## Coverage Testing

```shell
cargo tarpaulin --features enable_covtest -o html
```

## Property Testing

```shell
cargo test --features enable_proptest
```
