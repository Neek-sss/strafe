# Documentation

## Commands to output R help files

List out all packages

``` R
library()
```

List out functions in package

```R
ls("package:stats")
```

Or

```R
print_funcs = function(package) {
  funcs = ls(paste("package:", package, sep=""))
  for (i in 1:length(funcs)) {
    cat(paste("* [ ] ", funcs[i], "\n", sep=""))
  }
}
```

Get function docs

```R
file = help("<function name>")
```

If multiple entries in file, choose one with

```R
file = file[<index>]
```

then

```R
pkgname = basename(dirname(dirname(file)))
tools::Rd2txt(utils:::.getHelpFile(file), package = pkgname)
```

Or

```R
help_text <- function(...) {
  file <- help(...)
  path <- dirname(file)
  dirpath <- dirname(path)
  pkgname <- basename(dirpath)
  RdDB <- file.path(path, pkgname)
  rd <- tools:::fetchRdDB(RdDB, basename(file))
  text = capture.output(tools::Rd2txt(rd, out="", options=list(underline_titles=FALSE)))
  cat(text, sep="\n/// ")
}
```

Converting TimeSeries to DataFrame

```R
data.frame(<Column Name>=as.matrix(<Time Series>), date=zoo::as.Date(time(<Time Series>)))
```

or

```R
data.frame(<Column Name>=as.matrix(<Time Series>), time=time(<Time Series>))
```

Save R dataframes to CSV

```R
write.csv(<Dataframe>, "<File>.csv", row.names=FALSE)
```

## Generating LaTeX formulas in documentation

Bash

``` bash
RUSTDOCFLAGS="--html-in-header doc_script.html" cargo doc
```

Windows

``` Windows
set RUSTDOCFLAGS="--html-in-header doc_script.html"
cargo doc
```

Powershell

```PowerShell
$env:RUSTDOCFLAGS="--html-in-header doc_script.html"
cargo doc
```

For Docs.rs add

```
[package.metadata.docs.rs]
rustdoc-args = [ "--html-in-header", "../doc_script.html" ]
```

to Cargo.toml
