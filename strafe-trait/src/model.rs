use std::error::Error;

use nalgebra::DMatrix;
use strafe_type::{Alpha64, ModelMatrix};

use crate::{manipulator::Manipulator, Statistic, StatisticalEstimate};

pub trait ModelBuilder {
    type Model;
    fn with_x(self, x: &ModelMatrix) -> Self;
    fn with_y(self, y: &ModelMatrix) -> Self;
    fn with_weights(self, weights: &ModelMatrix) -> Self;
    fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self;
    fn build(self) -> Self::Model;
}

pub trait Model {
    fn get_x(&self) -> ModelMatrix;
    fn get_x1(&self) -> ModelMatrix;
    fn get_y(&self) -> ModelMatrix;
    fn get_weights(&self) -> ModelMatrix;
    fn get_intercept(&self) -> bool;
    fn manipulator(
        &self,
    ) -> Result<
        Box<dyn StatisticalEstimate<Manipulator<ModelMatrix, ModelMatrix>, ModelMatrix>>,
        Box<dyn Error>,
    >;
    fn determination(&self) -> Result<Box<dyn Statistic>, Box<dyn Error>>;
    fn parameters(
        &self,
    ) -> Result<Vec<Box<dyn StatisticalEstimate<f64, (f64, f64)>>>, Box<dyn Error>>;
    fn predictions(
        &self,
    ) -> Result<Box<dyn StatisticalEstimate<ModelMatrix, ModelMatrix>>, Box<dyn Error>>;
    fn residuals(&self) -> Result<ModelMatrix, Box<dyn Error>>;
    fn studentized_residuals(&self) -> Result<ModelMatrix, Box<dyn Error>>;
    fn standardized_residuals(&self) -> Result<ModelMatrix, Box<dyn Error>>;
    fn variance(&self) -> Result<DMatrix<f64>, Box<dyn Error>>;
    fn leverage(&self) -> Vec<f64> {
        let mut x = self.get_x1().matrix();
        let w = self.get_weights().matrix();
        for (mut row, w) in x.row_iter_mut().zip(w.iter()) {
            for x in row.iter_mut() {
                *x *= w.sqrt();
            }
        }

        (x.clone()
            * (x.transpose() * x.clone())
                .pseudo_inverse(f64::EPSILON)
                .unwrap()
            * x.transpose())
        .diagonal()
        .as_slice()
        .to_vec()
    }
}
