use std::any::TypeId;

pub trait Concept {
    fn comparator(&self) -> TypeId
    where
        Self: 'static,
    {
        TypeId::of::<Self>()
    }
    fn new() -> Self
    where
        Self: Sized;
}

impl dyn Concept {
    pub fn is<C: 'static + Concept>(&self) -> bool {
        self.comparator() == C::new().comparator()
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Normal {}

impl Concept for Normal {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct NotNormal {}

impl Concept for NotNormal {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct NormalResiduals {}

impl Concept for NormalResiduals {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct NotNormalResiduals {}

impl Concept for NotNormalResiduals {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct SomeSignficantFeature {}

impl Concept for SomeSignficantFeature {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct NoSignificantFeature {}

impl Concept for NoSignificantFeature {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct AccurateModel {}

impl Concept for AccurateModel {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct InaccurateModel {}

impl Concept for InaccurateModel {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct SignificantCoefficient {}

impl Concept for SignificantCoefficient {
    fn new() -> Self {
        Self {}
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct InsignificantCoefficient {}

impl Concept for InsignificantCoefficient {
    fn new() -> Self {
        Self {}
    }
}
