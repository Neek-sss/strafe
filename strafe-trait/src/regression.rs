// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::sync::Arc;

use nalgebra::DMatrix;

use crate::{Statistic, StatisticalEstimate};

pub type LinearEstimatorClosure = Arc<dyn Fn(&DMatrix<f64>) -> DMatrix<f64>>;

pub trait Regression {
    fn coefficients(&self) -> Vec<Box<dyn StatisticalEstimate<f64, f64>>>;
    fn estimate_function(
        &self,
    ) -> Box<dyn StatisticalEstimate<LinearEstimatorClosure, DMatrix<f64>>>;
    fn residuals(&self) -> DMatrix<f64>;
    fn predictions(&self) -> Box<dyn StatisticalEstimate<DMatrix<f64>, DMatrix<f64>>>;
    fn variance_covariance_matrix(&self) -> DMatrix<f64>;
    fn determination(&self) -> Box<dyn Statistic>;
}
