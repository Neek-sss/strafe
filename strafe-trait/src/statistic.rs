use std::fmt::Debug;

use strafe_type::Alpha64;

use crate::{Assumption, Conclusion};

#[derive(Debug)]
pub enum RejectionStatus {
    FailToReject(Box<dyn Conclusion>),
    RejectInFavorOf(Box<dyn Conclusion>),
}

impl RejectionStatus {
    pub fn unwrap(self) -> Box<dyn Conclusion> {
        match self {
            RejectionStatus::FailToReject(c) => c,
            RejectionStatus::RejectInFavorOf(c) => c,
        }
    }

    pub fn is<C: 'static + Conclusion>(self) -> bool {
        self.unwrap().is::<C>()
    }
}

/// The general form of a statistical evaluation
pub trait StatisticalTest {
    /// The data to feed into the testing function
    type Input;
    /// The response from the testing function
    type Output;
    /// Provide the required assumptions for the statistic to be calculated
    fn assumptions() -> Vec<Box<dyn Assumption>>;
    /// The hypotheses that is being tested
    fn null_hypotheses() -> Vec<Box<dyn Conclusion>>;
    /// The hypotheses that could be true if the null hypotheses is not
    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>>;
    /// The actual testing function
    fn test(&mut self, input: &Self::Input) -> Self::Output;
}

/// The general form of a statistical conclusion
pub trait Statistic {
    /// Get the alpha level associated with this statistical run
    fn alpha(&self) -> Alpha64;
    /// Get the result of the statistic
    fn statistic(&self) -> f64;
    /// Get the p-value of the result (usually, p < alpha
    /// means the model assumption is rejected)
    fn probability_value(&self) -> f64;
    /// A conclusion on whether the null hypothesis was passed or not
    fn conclusion(&self) -> RejectionStatus;
}

/// The general form of a statistical estimation
pub trait StatisticalEstimator {
    /// The data to feed into the estimation function
    type Input;
    /// The response from the estimation function
    type Output;
    /// Provide the required assumptions for the statistic to be calculated
    fn assumptions() -> Vec<Box<dyn Assumption>>;
    /// The actual estimation function
    fn estimate(&self, input: &Self::Input) -> Self::Output;
}

/// The general form of a statistic that generates an estimate
pub trait StatisticalEstimate<E, CI> {
    /// Get the alpha level associated with this statistical run
    fn alpha(&self) -> Alpha64;
    /// Get the estimated value of the statistic
    fn estimate(&self) -> E;
    /// Get the confidence interval of the estimate
    fn confidence_interval(&self) -> CI;
}
