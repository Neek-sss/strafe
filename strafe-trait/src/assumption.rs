use std::{any::TypeId, fmt::Debug};

use crate::{Concept, Normal, NormalResiduals, NotNormal, NotNormalResiduals};

pub trait Assumption: Debug + Concept {
    fn to_comparator(&self) -> TypeId
    where
        Self: 'static,
    {
        self.comparator()
    }
}

impl dyn Assumption {
    pub fn is<C: 'static + Concept>(&self) -> bool {
        self.comparator() == C::new().comparator()
    }
}

impl Assumption for Normal {}

impl Assumption for NotNormal {}

impl Assumption for NormalResiduals {}

impl Assumption for NotNormalResiduals {}
