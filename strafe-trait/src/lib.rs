#[cfg(not(tarpaulin_include))]
mod assumption;
#[cfg(not(tarpaulin_include))]
mod concept;
#[cfg(not(tarpaulin_include))]
mod conclusion;
#[cfg(not(tarpaulin_include))]
mod manipulator;
#[cfg(not(tarpaulin_include))]
mod model;
#[cfg(not(tarpaulin_include))]
mod regression;
#[cfg(not(tarpaulin_include))]
mod statistic;

pub use self::{
    assumption::*, concept::*, conclusion::*, manipulator::*, model::*, regression::*, statistic::*,
};
