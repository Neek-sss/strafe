use std::sync::Arc;

#[derive(Clone)]
pub struct Manipulator<I, O> {
    manipulator: Arc<dyn Fn(&I) -> O>,
}

impl<I, O> Manipulator<I, O> {
    pub fn new(f: Arc<dyn Fn(&I) -> O>) -> Self {
        Self { manipulator: f }
    }

    pub fn manipulate(&self, input: &I) -> O {
        (*self.manipulator)(input)
    }
}
