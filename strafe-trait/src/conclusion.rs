use std::{any::TypeId, fmt::Debug};

use crate::{
    AccurateModel, Concept, InaccurateModel, InsignificantCoefficient, NoSignificantFeature,
    Normal, NormalResiduals, NotNormal, NotNormalResiduals, SignificantCoefficient,
    SomeSignficantFeature,
};

pub trait Conclusion: Debug + Concept {
    fn to_comparator(&self) -> TypeId
    where
        Self: 'static,
    {
        self.comparator()
    }
}

impl dyn Conclusion {
    pub fn is<C: 'static + Concept>(&self) -> bool {
        self.comparator() == C::new().comparator()
    }
}

impl Conclusion for Normal {}

impl Conclusion for NotNormal {}

impl Conclusion for NormalResiduals {}

impl Conclusion for NotNormalResiduals {}

impl Conclusion for SomeSignficantFeature {}

impl Conclusion for NoSignificantFeature {}

impl Conclusion for AccurateModel {}

impl Conclusion for InaccurateModel {}

impl Conclusion for SignificantCoefficient {}

impl Conclusion for InsignificantCoefficient {}
