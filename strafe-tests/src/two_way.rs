// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub use r2rs_mass::robust_linear_model::*;
pub use r2rs_rfit::{
    rank_fit::{RankFit, RankFitBuilder, RankFitTest},
    scores,
};
pub use r2rs_stats::regression::{
    family, link, GeneralizedLinearRegression, GeneralizedLinearRegressionBuilder,
    GeneralizedRegressionTest, LeastSquaresRegression, LeastSquaresRegressionBuilder,
    LeastSquaresRegressionError, LeastSquaresRegressionTest,
};
