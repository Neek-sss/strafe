use bitvec::{prelude::*, vec::BitVec};

use crate::{signed::SignedIntegerExtendable, unsigned::UnsignedIntegerExtendable};

#[derive(Clone, Debug)]
pub struct DecimalExtendable(pub(crate) BitVec, usize);

impl DecimalExtendable {
    pub fn from_signed(s: SignedIntegerExtendable) -> Self {
        Self(s.0, 0)
    }

    pub fn from_unsigned(u: UnsignedIntegerExtendable) -> Self {
        let mut bits = u.0;
        bits.insert(0, false);
        Self(bits, 0)
    }
}
