#![allow(dead_code)]

#[cfg(feature = "f64")]
#[cfg(not(tarpaulin_include))]
mod fake_f128;

#[cfg(feature = "f64")]
#[cfg(not(tarpaulin_include))]
pub use fake_f128::f128;

#[cfg(feature = "extendable")]
#[cfg(not(tarpaulin_include))]
mod decimal;
#[cfg(feature = "extendable")]
#[cfg(not(tarpaulin_include))]
mod float;
#[cfg(feature = "extendable")]
#[cfg(not(tarpaulin_include))]
mod signed;
#[cfg(feature = "extendable")]
#[cfg(not(tarpaulin_include))]
mod softfloat;
#[cfg(feature = "extendable")]
#[cfg(not(tarpaulin_include))]
mod unsigned;
