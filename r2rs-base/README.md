# r2rs-base

## Supporting

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/F1F6VM74P)
[![librepay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/YarshTechnologies/donate)

If you like what you see here, consider supporting the development of Strafe.
I would love to develop this project full time, but I do have bills to pay.
With your support I hope to be able to transition more of time to this project
and make statistics in Rust a reality. Thank you so much for supporting!

## License

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license
  ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.