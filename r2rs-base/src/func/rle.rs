// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

#[derive(Debug, Clone, PartialEq)]
pub struct RunLengthEncoding<T: Clone + PartialEq> {
    pub length: usize,
    pub value: T,
}

/// Run Length Encoding
///
/// ## Description:
///
/// Compute the lengths and values of runs of equal values in a vector
/// - or the reverse operation.
///
/// ## Usage:
///
/// rle(x)
/// inverse.rle(x, ...)
///
/// #### S3 method for class 'rle'
/// print(x, digits = getOption("digits"), prefix = "", ...)
///
/// ## Arguments:
///
/// * x: a vector (atomic, not a list) for ‘rle()’; an object of class
///   ‘"rle"’ for ‘inverse.rle()’.
/// * ...: further arguments; ignored here.
/// * digits: number of significant digits for printing, see ‘print.default’.
/// * prefix: character string, prepended to each printed line.
///
/// ## Details:
///
/// ‘vector’ is used in the sense of ‘is.vector’.
///
/// Missing values are regarded as unequal to the previous value, even
/// if that is also missing.
///
/// ‘inverse.rle()’ is the inverse function of ‘rle()’, reconstructing
/// ‘x’ from the runs.
///
/// ## Value:
///
/// ‘rle()’ returns an object of class ‘"rle"’ which is a list with
/// components:
///
/// lengths: an integer vector containing the length of each run.
///
/// values: a vector of the same length as ‘lengths’ with the corresponding values.
/// ‘inverse.rle()’ returns an atomic vector.
///
/// ## Examples:
///
/// ```r
/// x <- rev(rep(6:10, 1:5))
/// rle(x)
/// ## lengths [1:5]  5 4 3 2 1
/// ## values  [1:5] 10 9 8 7 6
///
/// z <- c(TRUE, TRUE, FALSE, FALSE, TRUE, FALSE, TRUE, TRUE, TRUE)
/// rle(z)
/// rle(as.character(z))
/// print(rle(z), prefix = "..| ")
/// ```
pub fn rle<T: Clone + PartialEq>(x: &[T]) -> Vec<RunLengthEncoding<T>> {
    x.iter().fold(Vec::new(), |mut v, x| {
        if v.is_empty() {
            v.push(RunLengthEncoding {
                length: 1,
                value: x.clone(),
            });
            v
        } else {
            let len = v.len();
            let last = &mut v[len - 1];
            if last.value == x.clone() {
                last.length += 1;
            } else {
                v.push(RunLengthEncoding {
                    length: 1,
                    value: x.clone(),
                });
            }
            v
        }
    })
}

/// Run Length Encoding
///
/// ## Description:
///
/// Compute the lengths and values of runs of equal values in a vector
/// - or the reverse operation.
///
/// ## Usage:
///
/// rle(x)
/// inverse.rle(x, ...)
///
/// ## S3 method for class 'rle'
/// print(x, digits = getOption("digits"), prefix = "", ...)
///
/// ## Arguments:
///
/// * x: a vector (atomic, not a list) for ‘rle()’; an object of class
///   ‘"rle"’ for ‘inverse.rle()’.
/// * ...: further arguments; ignored here.
/// * digits: number of significant digits for printing, see
///   print.default’.
/// * prefix: character string, prepended to each printed line.
///
/// ## Details:
///
/// ‘vector’ is used in the sense of ‘is.vector’.
///
/// Missing values are regarded as unequal to the previous value, even
/// if that is also missing.
///
/// ‘inverse.rle()’ is the inverse function of ‘rle()’, reconstructing
/// ‘x’ from the runs.
///
/// ## Value:
///
/// ‘rle()’ returns an object of class ‘"rle"’ which is a list with
/// components:
///
/// lengths: an integer vector containing the length of each run.
///
/// values: a vector of the same length as ‘lengths’ with the
/// corresponding values.
///
/// ‘inverse.rle()’ returns an atomic vector.
///
/// ## Examples:
///
/// ```r
/// x <- rev(rep(6:10, 1:5))
/// rle(x)
/// ## lengths [1:5]  5 4 3 2 1
/// ## values  [1:5] 10 9 8 7 6
///
/// z <- c(TRUE, TRUE, FALSE, FALSE, TRUE, FALSE, TRUE, TRUE, TRUE)
/// rle(z)
/// rle(as.character(z))
/// print(rle(z), prefix = "..| ")
///
/// N <- integer(0)
/// stopifnot(x == inverse.rle(rle(x)),
/// identical(N, inverse.rle(rle(N))),
/// z == inverse.rle(rle(z)))
/// ```
pub fn inverse_rle<T: Clone + PartialEq>(x: &[RunLengthEncoding<T>]) -> Vec<T> {
    x.iter()
        .flat_map(|RunLengthEncoding { length, value }| vec![value.clone(); *length])
        .collect()
}
