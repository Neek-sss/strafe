// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

/// Lagged Differences
///
/// ## Description:
///
/// Returns suitably lagged and iterated differences.
///
/// ## Usage:
///
/// ```r
/// diff(x, ...)
///
/// ## Default S3 method:
/// diff(x, lag = 1, differences = 1, ...)
///
/// ## S3 method for class 'POSIXt'
/// diff(x, lag = 1, differences = 1, ...)
///
/// ## S3 method for class 'Date'
/// diff(x, lag = 1, differences = 1, ...)
/// ```
///
/// ## Arguments:
///
/// * x: a numeric vector or matrix containing the values to be
/// differenced.
///
/// * lag: an integer indicating which lag to use.
///
/// * differences: an integer indicating the order of the difference.
///
/// * ...: further arguments to be passed to or from methods.
///
/// ## Details:
///
/// ‘diff’ is a generic function with a default method and ones for
/// classes ‘"ts"’, ‘"POSIXt"’ and ‘"Date"’.
///
/// ‘NA’'s propagate.
///
/// ## Value:
///
/// If ‘x’ is a vector of length ‘n’ and ‘differences = 1’, then the
/// computed result is equal to the successive differences
/// ‘x\[(1+lag):n\] - x\[1:(n-lag)\]’.
///
/// If ‘difference’ is larger than one this algorithm is applied
/// recursively to ‘x’.Note that the returned value is a vector
/// which is shorter than ‘x’.
///
/// If ‘x’ is a matrix then the difference operations are carried out
/// on each column separately.
///
/// ## References:
///
/// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
/// Language_.Wadsworth & Brooks/Cole.
///
/// ## See Also:
///
/// ‘diff.ts’, ‘diffinv’.
///
/// ## Examples:
///
/// ```r
/// diff(1:10, 2)
/// diff(1:10, 2, 2)
/// x <- cumsum(cumsum(1:10))
/// diff(x, lag = 2)
/// diff(x, differences = 2)
///
/// diff(.leap.seconds)
/// ```
pub fn diff(x: &[f64]) -> Vec<f64> {
    x.windows(2).map(|win| win[1] - win[0]).collect()
}
