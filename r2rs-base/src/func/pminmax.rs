// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub fn pmin(x: &[f64], y: &[f64]) -> Vec<f64> {
    let max_length = x.len().max(y.len());
    x.iter()
        .cycle()
        .zip(y.iter().cycle())
        .take(max_length)
        .map(|(&x, &y)| x.min(y))
        .collect::<Vec<_>>()
}

pub fn pmax(x: &[f64], y: &[f64]) -> Vec<f64> {
    let max_length = x.len().max(y.len());
    x.iter()
        .cycle()
        .zip(y.iter().cycle())
        .take(max_length)
        .map(|(&x, &y)| x.max(y))
        .collect::<Vec<_>>()
}
