// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use r2rs_nmath::traits::RNG;
use strafe_type::{FloatConstraint, Real64};

/// Random Samples and Permutations
///
/// ## Description:
///
/// ‘sample’ takes a sample of the specified size from the elements of
/// ‘x’ using either with or without replacement.
///
/// ## Usage:
///
/// sample(x, size, replace = FALSE, prob = NULL)
///
/// sample.int(n, size = n, replace = FALSE, prob = NULL,
///   useHash = (n > 1e+07 && !replace && is.null(prob) && size <= n/2))
///
/// ## Arguments:
///
/// * x: either a vector of one or more elements from which to choose,
///   or a positive integer.  See ‘Details.’
/// * n: a positive number, the number of items to choose from.  See
///   Details.’
/// * size: a non-negative integer giving the number of items to choose.
/// * replace: should sampling be with replacement?
/// * prob: a vector of probability weights for obtaining the elements of
///   the vector being sampled.
/// * useHash: ‘logical’ indicating if the hash-version of the algorithm
///   should be used.  Can only be used for ‘replace = FALSE’,
///   ‘prob = NULL’, and ‘size <= n/2’, and really should be used
///   for large ‘n’, as ‘useHash=FALSE’ will use memory
///   proportional to ‘n’.
///
/// ## Details:
///
/// If ‘x’ has length 1, is numeric (in the sense of ‘is.numeric’) and
/// ‘x >= 1’, sampling _via_ ‘sample’ takes place from ‘1:x’.  _Note_
/// that this convenience feature may lead to undesired behaviour when
/// ‘x’ is of varying length in calls such as ‘sample(x)’.  See the
/// examples.
///
/// Otherwise ‘x’ can be any R object for which ‘length’ and
/// subsetting by integers make sense: S3 or S4 methods for these
/// operations will be dispatched as appropriate.
///
/// For ‘sample’ the default for ‘size’ is the number of items
/// inferred from the first argument, so that ‘sample(x)’ generates a
/// random permutation of the elements of ‘x’ (or ‘1:x’).
///
/// It is allowed to ask for ‘size = 0’ samples with ‘n = 0’ or a
/// length-zero ‘x’, but otherwise ‘n > 0’ or positive ‘length(x)’ is
/// required.
///
/// Non-integer positive numerical values of ‘n’ or ‘x’ will be
/// truncated to the next smallest integer, which has to be no larger
/// than ‘.Machine$integer.max’.
///
/// The optional ‘prob’ argument can be used to give a vector of
/// weights for obtaining the elements of the vector being sampled.
/// They need not sum to one, but they should be non-negative and not
/// all zero.  If ‘replace’ is true, Walker's alias method (Ripley,
/// 1987) is used when there are more than 200 reasonably probable
/// values: this gives results incompatible with those from R < 2.2.0.
///
/// If ‘replace’ is false, these probabilities are applied
/// sequentially, that is the probability of choosing the next item is
/// proportional to the weights amongst the remaining items.  The
/// number of nonzero weights must be at least ‘size’ in this case.
///
/// ‘sample.int’ is a bare interface in which both ‘n’ and ‘size’ must
/// be supplied as integers.
///
/// Argument ‘n’ can be larger than the largest integer of type
/// ‘integer’, up to the largest representable integer in type
/// ‘double’.  Only uniform sampling is supported.  Two random numbers
/// are used to ensure uniform sampling of large integers.
///
/// ## Value:
///
/// For ‘sample’ a vector of length ‘size’ with elements drawn from
/// either ‘x’ or from the integers ‘1:x’.
///
/// For ‘sample.int’, an integer vector of length ‘size’ with elements
/// from ‘1:n’, or a double vector if n >= 2^31.
///
/// ## References:
///
/// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
/// Language_.  Wadsworth & Brooks/Cole.
///
/// Ripley, B. D. (1987) _Stochastic Simulation_. Wiley.
///
/// ## See Also:
///
/// ‘RNGkind(sample.kind = ..)’ about random number generation,
/// notably the change of ‘sample()’ results with R version 3.6.0.
///
/// CRAN package ‘sampling’ for other methods of weighted sampling
/// without replacement.
///
/// ## Examples:
///
/// ```r
/// x <- 1:12
/// # a random permutation
/// sample(x)
/// # bootstrap resampling -- only if length(x) > 1 !
/// sample(x, replace = TRUE)
///
/// # 100 Bernoulli trials
/// sample(c(0,1), 100, replace = TRUE)
///
/// ## More careful bootstrapping --  Consider this when using sample()
/// ## programmatically (i.e., in your function or simulation)!
///
/// # sample()'s surprise -- example
/// x <- 1:10
///     sample(x[x >  8]) # length 2
///     sample(x[x >  9]) # oops -- length 10!
///     sample(x[x > 10]) # length 0
///
/// ## safer version:
/// resample <- function(x, ...) x[sample.int(length(x), ...)]
/// resample(x[x >  8]) # length 2
/// resample(x[x >  9]) # length 1
/// resample(x[x > 10]) # length 0
///
/// ## R 3.0.0 and later
/// sample.int(1e10, 12, replace = TRUE)
/// sample.int(1e10, 12) # not that there is much chance of duplicates
/// ```
pub fn sample<T: Clone, R: RNG>(
    x: &[T],
    size: usize,
    replace: bool,
    prob_arg: Option<&[Real64]>,
    rng: &mut R,
) -> Vec<T> {
    if size > x.len() {
        panic!("Length bad")
    }

    let calc_probs = |probs: &[f64]| -> Vec<(f64, f64)> {
        let prob_sum = probs.iter().sum::<f64>();
        let mut last_prob = 0.0;
        probs
            .iter()
            .map(|f| {
                let this_prob = last_prob + (*f / prob_sum);
                let ret = (last_prob, this_prob);
                last_prob = this_prob;
                ret
            })
            .collect()
    };

    let unwrapped_probs = prob_arg
        .unwrap_or(&vec![(1.0 / (x.len() as f64)).into(); x.len()])
        .iter()
        .map(|p| p.unwrap())
        .collect::<Vec<_>>();
    let mut probs = calc_probs(&unwrapped_probs);

    let mut x = x.to_vec();
    let mut num_misses = 0;

    (0..size)
        .map(|_| {
            let mut index = None;
            while index.is_none() {
                let rand = rng.unif_rand();
                match probs.binary_search_by(|(_, high)| high.partial_cmp(&rand).unwrap()) {
                    Ok(possible_index) | Err(possible_index) => {
                        if possible_index < probs.len() {
                            let prob = &probs[possible_index];
                            if prob.0 < rand {
                                index = Some(possible_index);
                            }
                        }
                    }
                }
                if index.is_none() {
                    num_misses += 1;
                }
                if num_misses >= 10 {
                    num_misses = 0;
                    let current_probs = probs
                        .iter()
                        .map(|(prob_low, prob_high)| prob_high - prob_low)
                        .collect::<Vec<_>>();
                    probs = calc_probs(&current_probs);
                }
            }
            let ret: &T = &x[index.unwrap()];
            let ret2 = ret.clone();
            if !replace {
                x.remove(index.unwrap());
                probs.remove(index.unwrap());
            }
            ret2
        })
        .collect()
}
