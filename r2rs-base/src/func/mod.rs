mod as_factor;
mod bootstrap;
mod diff;
mod order;
mod pminmax;
mod polyroot;
mod psort;
mod rle;
mod sample;
mod unique;
mod zapsmall;

pub use self::{
    as_factor::*, bootstrap::bootstrap_ci, diff::diff, order::order, pminmax::*,
    polyroot::polyroot, psort::*, rle::*, sample::sample, unique::unique, zapsmall::zapsmall,
};

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;
