// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use itertools::Itertools;
use nalgebra::DMatrix;

pub fn as_independent_factor_matrix<T: PartialEq + Clone>(x: &[T]) -> (Vec<T>, DMatrix<f64>) {
    let mut column_names = Vec::new();
    for value in x {
        if !column_names.contains(value) {
            column_names.push(value.clone());
        }
    }

    let mut matrix = DMatrix::<f64>::zeros(x.len(), column_names.len());

    for (row_index, value) in x.iter().enumerate() {
        let column_index = column_names
            .iter()
            .find_position(|&v| v == value)
            .unwrap()
            .0;
        matrix[(row_index, column_index)] = 1.0;
    }

    (column_names, matrix)
}

pub fn as_level_factor_matrix<T: PartialEq>(x: &[T], levels: &[T]) -> DMatrix<f64> {
    let mut matrix = DMatrix::<f64>::zeros(x.len(), 1);

    for (row_index, value) in x.iter().enumerate() {
        let level = levels.iter().find_position(|&v| v == value).unwrap().0;
        matrix[(1, row_index)] = level as f64;
    }

    matrix
}
