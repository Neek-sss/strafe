use log::error;
use num_complex::Complex64;
use num_traits::{Float, FloatConst, Zero};

/// Find Zeros of a Real or Complex Polynomial
///
/// ## Description:
///
/// Find zeros of a real or complex polynomial.
///
/// ## Usage:
///
/// polyroot(z)
///      
/// ## Arguments:
///
/// z: the vector of polynomial coefficients in increasing order.
///
/// ## Details:
///
/// A polynomial of degree n - 1,
///
/// $p(x) = z1 + z2 * x + ... + z\[n\] * x^{n-1}$
///
/// is given by its coefficient vector $z\[1:n\]$.  ‘polyroot’ returns
/// the n-1 complex zeros of p(x) using the Jenkins-Traub algorithm.
///
/// If the coefficient vector ‘z’ has zeroes for the highest powers,
/// these are discarded.
///
/// There is no maximum degree, but numerical stability may be an
/// issue for all but low-degree polynomials.
///
/// ## Value:
///
/// A complex vector of length n - 1, where n is the position of the
/// largest non-zero element of ‘z’.
///
/// ## Source:
///
/// C translation by Ross Ihaka of Fortran code in the reference, with
/// modifications by the R Core Team.
///
/// ## References:
///
/// Jenkins, M. A. and Traub, J. F. (1972).  Algorithm 419: zeros of a
/// complex polynomial.  _Communications of the ACM_, *15*(2), 97-99.
/// doi:10.1145/361254.361262 <https://doi.org/10.1145/361254.361262>.
///
/// ## See Also:
///
/// ‘uniroot’ for numerical root finding of arbitrary functions;
/// ‘complex’ and the ‘zero’ example in the demos directory.
///
/// ## Examples:
///
/// ```r
/// polyroot(c(1, 2, 1))
/// round(polyroot(choose(8, 0:8)), 11) # guess what!
/// for (n1 in 1:4) print(polyroot(1:n1), digits = 4)
/// polyroot(c(1, 2, 1, 0, 0)) # same as the first
/// ```
pub fn polyroot(x: &[Complex64]) -> Vec<Complex64> {
    let mut op = x.to_vec();

    // Get rid of leading zeros
    while op[0].is_zero() {
        op.remove(0);
    }

    let mut ret = vec![Complex64::zero(); op.len() - 1];

    // Get rid of trailing zeros
    while op.last().unwrap().is_zero() {
        op.pop();
    }

    // Find the degree of the remaining polynomial
    let mut degree = 0;
    for (i, op) in op.iter().enumerate() {
        if !op.is_zero() {
            degree = i;
        }
    }

    if degree != 0 && degree != 1 {
        const COSR: f64 = -0.06975647374412529990; // cos 94
        const SINR: f64 = 0.99756405025982424767; // sin 94

        let mut xx = f64::FRAC_1_SQRT_2();
        let mut yy = -f64::FRAC_1_SQRT_2();

        let mut nn = degree;
        let d1 = nn - 1;

        let mut p = vec![Complex64::zero(); nn];
        let mut h = vec![Complex64::zero(); nn];

        let mut qp = vec![Complex64::zero(); nn];
        let mut qh = vec![Complex64::zero(); nn];
        let mut pv = Complex64::zero();

        let mut sh = vec![Complex64::zero(); nn];

        // make a copy of the coefficients and shr[] = | p[] |
        for i in 0..nn {
            p[i] = op[i];
            sh[i].re = p[i].re.hypot(p[i].im);
        }

        // HERE
        // scale the polynomial with factor 'bnd'.
        let mut bnd = poly_scale(nn, &sh, f64::EPSILON, f64::MAX, f64::MIN, f64::RADIX as f64);
        if bnd != 1.0 {
            for p_i in &mut p[0..nn] {
                p_i.re *= bnd;
                p_i.im *= bnd;
            }
        }

        /* start the algorithm for one zero */
        let mut t = Complex64::zero();
        let mut omp = 0.0;
        let mut relstp = 0.0;

        while nn > 2 {
            let mut conv = false;
            let mut z = Complex64::zero();

            /* calculate bnd, a lower bound on the modulus of the zeros. */
            for i in 0..nn {
                sh[i].re = p[i].re.hypot(p[i].im);
            }
            bnd = poly_cauchy(nn, &mut sh);

            /* outer loop to control 2 major passes */
            /* with different sequences of shifts */
            for _ in 1..=2 {
                /* first stage calculation, no shift */
                noshift(5, nn, &mut h, &p);

                /* inner loop to select a shift */
                for i2 in 1..=9 {
                    /* shift is chosen with modulus bnd */
                    /* and amplitude rotated by 94 degrees */
                    /* from the previous shift */
                    let xxx = COSR * xx - SINR * yy;
                    yy = SINR * xx + COSR * yy;
                    xx = xxx;
                    let mut s = Complex64::new(bnd * xx, bnd * yy);

                    /*  second stage calculation, fixed shift */
                    z = Complex64::zero();
                    conv = fxshift(
                        i2 * 10,
                        &mut z,
                        &mut s,
                        &mut h,
                        &mut sh,
                        &mut p,
                        &mut qp,
                        &mut qh,
                        &mut pv,
                        &mut t,
                        nn,
                        &mut omp,
                        &mut relstp,
                    );
                    if conv {
                        break;
                    }
                }
                if conv {
                    break;
                }
            }
            if conv {
                let d_n = d1 + 2 - nn;
                ret[d_n].re = z.re;
                ret[d_n].im = z.im;
                nn -= 1;
                for i in 0..nn {
                    p[i].re = qp[i].re;
                    p[i].im = qp[i].im;
                }
                continue;
            } else {
                error!("polyroot has failed twice in a row");
                return Vec::new();
            }
        }
    }

    ret
}

/// Computes the derivative polynomial as the initial
/// polynomial and computes l1 no-shift h polynomials.
fn noshift(l1: i32, nn: usize, h: &mut [Complex64], p: &[Complex64]) {
    let n = nn - 1;
    let nm1 = n - 1;

    for i in 0..n {
        let xni = (nn - i - 1) as f64;
        h[i].re = xni * p[i].re / n as f64;
        h[i].im = xni * p[i].im / n as f64;
    }

    for _ in 1..=l1 {
        if h[n - 1].re.hypot(h[n - 1].im) <= f64::EPSILON * 10.0 * p[n - 1].re.hypot(p[n - 1].im) {
            /* If the constant term is essentially zero, */
            /* shift h coefficients. */
            for i in 1..=nm1 {
                let j = nn - i;
                h[j - 1].re = h[j - 2].re;
                h[j - 1].im = h[j - 2].im;
            }
            h[0].re = 0.;
            h[0].im = 0.;
        } else {
            let t = -p[nn - 1] / h[n - 1];
            for i in 1..=nm1 {
                let j = nn - i;
                let t1 = h[j - 2].re;
                let t2 = h[j - 2].im;
                h[j - 1].re = t.re * t1 - t.im * t2 + p[j - 1].re;
                h[j - 1].im = t.re * t2 + t.im * t1 + p[j - 1].im;
            }
            h[0].re = p[0].re;
            h[0].im = p[0].im;
        }
    }
}

/// Computes l2 fixed-shift h polynomials and tests for convergence.
///  initiates a variable-shift iteration and returns with the
///  approximate zero if successful.
///
/// l2 - limit of fixed shift steps
/// zr,zi - approximate zero if convergence (result TRUE)
///
/// Return value indicates convergence of stage 3 iteration
///
/// Uses global (sr,si), nn, pr\[\], pi\[\], .. (all args of polyev() !)
fn fxshift(
    l2: usize,
    z: &mut Complex64,
    s: &mut Complex64,
    h: &mut [Complex64],
    sh: &mut [Complex64],
    p: &mut [Complex64],
    qp: &mut [Complex64],
    qh: &mut [Complex64],
    pv: &mut Complex64,
    t: &mut Complex64,
    nn: usize,
    omp: &mut f64,
    relstp: &mut f64,
) -> bool {
    let n = nn - 1;

    /* evaluate p at s. */
    polyev(nn, s, p, qp, pv);

    let mut test = true;
    let mut pasd = false;

    /* calculate first t = -p(s)/h(s). */
    let mut is_zero = false;
    calct(pv, h, &mut is_zero, t, nn, s, qh);

    /* main loop for one second stage step. */
    for j in 1..=l2 {
        let ot = Complex64::new(t.re, t.im);

        /* compute next h polynomial and new t. */
        nexth(is_zero, qh, qp, h, t, nn);
        calct(pv, h, &mut is_zero, t, nn, s, qh);
        z.re = s.re + t.re;
        z.im = s.im + t.im;

        /* test for convergence unless stage 3 has */
        /* failed once or this is the last h polynomial. */
        if !is_zero && test && j != l2 {
            if (t.re - ot.re).hypot(t.im - ot.im) >= z.re.hypot(z.im) * 0.5 {
                pasd = false;
            } else if !pasd {
                pasd = true;
            } else {
                /* the weak convergence test has been */
                /* passed twice, start the third stage */
                /* iteration, after saving the current */
                /* h polynomial and shift. */
                for i in 0..n {
                    sh[i].re = h[i].re;
                    sh[i].im = h[i].im;
                }
                let svs = *s;
                if vrshift(10, z, nn, s, p, h, qh, qp, pv, t, omp, relstp) {
                    return true;
                }

                /* the iteration failed to converge. */
                /* turn off testing and restore */
                /* h, s, pv and t. */
                test = false;
                for i in 1..=n {
                    h[i - 1].re = sh[i - 1].re;
                    h[i - 1].im = sh[i - 1].im;
                }
                s.re = svs.re;
                s.im = svs.im;
                polyev(nn, s, p, qp, pv);
                calct(pv, h, &mut is_zero, t, nn, s, qh);
            }
        }
    }

    /* attempt an iteration with final h polynomial */
    /* from second stage. */
    vrshift(10, z, nn, s, p, h, qh, qp, pv, t, omp, relstp)
}

/// carries out the third stage iteration.
///
/// l3 - limit of steps in stage 3.
/// zr,zi - on entry contains the initial iterate;
///      if the iteration converges it contains
///      the final iterate on exit.
/// Returns TRUE if iteration converges
///
/// Assign and uses  GLOBAL sr, si
fn vrshift(
    l3: u32,
    z: &mut Complex64,
    nn: usize,
    s: &mut Complex64,
    p: &[Complex64],
    h: &mut [Complex64],
    qh: &mut [Complex64],
    qp: &mut [Complex64],
    pv: &mut Complex64,
    t: &mut Complex64,
    omp: &mut f64,
    relstp: &mut f64,
) -> bool {
    let mut b = false;
    s.re = z.re;
    s.im = z.im;

    /* main loop for stage three */
    for i in 1..l3 {
        /* evaluate p at s and test for convergence. */
        polyev(nn, s, p, qp, pv);

        let mp = pv.re.hypot(pv.im);
        let ms = s.re.hypot(s.im);
        if mp
            <= 20.0
                * errev(
                    nn,
                    qp,
                    ms,
                    mp,
                    f64::EPSILON,
                    2.0 * f64::SQRT_2() * f64::EPSILON,
                )
        {
            z.re = s.re;
            z.im = s.im;
            return true;
        }

        /* polynomial value is smaller in value than */
        /* a bound on the error in evaluating p, */
        /* terminate the iteration. */
        if i != 1 {
            if !b && mp >= *omp && *relstp < 0.05 {
                /* iteration has stalled. probably a */
                /* cluster of zeros. do 5 fixed shift */
                /* steps into the cluster to force */
                /* one zero to dominate. */

                let mut tp = *relstp;
                b = true;
                if *relstp < f64::EPSILON {
                    tp = f64::EPSILON;
                }
                let r1 = tp.sqrt();
                let r2 = s.re * (r1 + 1.0) - s.im * r1;
                s.im = s.re * r1 + s.im * (r1 + 1.0);
                s.re = r2;
                polyev(nn, s, p, qp, pv);
                for _ in 1..=5 {
                    let mut is_zero = false;
                    calct(pv, h, &mut is_zero, t, nn, s, qh);
                    nexth(is_zero, qh, qp, h, t, nn);
                }
                *omp = f64::infinity();
                /* calculate next iterate. */

                let mut is_zero = false;
                calct(pv, h, &mut is_zero, t, nn, s, qh);
                nexth(is_zero, qh, qp, h, t, nn);
                calct(pv, h, &mut is_zero, t, nn, s, qh);
                if !is_zero {
                    *relstp = t.re.hypot(t.im) / s.re.hypot(s.im);
                    s.re += t.re;
                    s.im += t.im;
                }
                continue;
            } else {
                /* exit if polynomial value */
                /* increases significantly. */

                if mp * 0.1 > *omp {
                    return false;
                }
            }
        }
        *omp = mp;

        /* calculate next iterate. */
        let mut is_zero = false;
        calct(pv, h, &mut is_zero, t, nn, s, qh);
        nexth(is_zero, qh, qp, h, t, nn);
        calct(pv, h, &mut is_zero, t, nn, s, qh);
        if !is_zero {
            *relstp = t.re.hypot(t.im) / s.re.hypot(s.im);
            s.re += t.re;
            s.im += t.im;
        }
    }
    false
}

/// Returns a scale factor to multiply the coefficients of the polynomial.
/// The scaling is done to avoid overflow and to avoid
/// undetected underflow interfering with the convergence criterion.
/// The factor is a power of the base.
///
/// pot\[1:n\] : modulus of coefficients of p
/// eps, big, small, base - constants describing the floating point arithmetic.
fn poly_scale(n: usize, pot: &[Complex64], eps: f64, big: f64, small: f64, base: f64) -> f64 {
    /* find largest and smallest moduli of coefficients. */
    let high = big.sqrt();
    let lo = small / eps;
    let mut max_ = 0.0;
    let mut min_ = big;
    for pot_i in &pot[0..n] {
        let x = pot_i.re;
        if x > max_ {
            max_ = x;
        }
        if x != 0.0 && x < min_ {
            min_ = x;
        }
    }

    /* scale only if there are very large or very small components. */

    if min_ < lo || max_ > high {
        let x = lo / min_;
        let sc = if x <= 1.0 {
            1.0 / (max_.sqrt() * min_.sqrt())
        } else if big / x > max_ {
            1.0
        } else {
            x
        };
        let ell = sc.ln() / base.ln() + 0.5;
        base.powi(ell as i32)
    } else {
        1.0
    }
}

/// computes  t = -p(s)/h(s).
/// is_zero   - logical, set true if h(s) is essentially zero.
fn calct(
    pv: &Complex64,
    h: &[Complex64],
    is_zero: &mut bool,
    t: &mut Complex64,
    nn: usize,
    s: &Complex64,
    qh: &mut [Complex64],
) {
    let n = nn - 1;

    /* evaluate h(s). */
    let mut hv = Complex64::zero();
    polyev(n, s, h, qh, &mut hv);

    *is_zero = hv.re.hypot(hv.im) <= f64::EPSILON * 10.0 * h[n - 1].re.hypot(h[n - 1].im);
    if !*is_zero {
        *t = -pv / hv;
    } else {
        *t = Complex64::zero();
    }
}

/// calculates the next shifted h polynomial.
/// is_zero : if TRUE  h(s) is essentially zero
fn nexth(
    is_zero: bool,
    qh: &[Complex64],
    qp: &[Complex64],
    h: &mut [Complex64],
    t: &Complex64,
    nn: usize,
) {
    let n = nn - 1;

    if !is_zero {
        for j in 1..n {
            let t1 = qh[j - 1].re;
            let t2 = qh[j - 1].im;
            h[j].re = t.re * t1 - t.im * t2 + qp[j].re;
            h[j].im = t.re * t2 + t.im * t1 + qp[j].im;
        }
        h[0].re = qp[0].re;
        h[0].im = qp[0].im;
    } else {
        /* if h(s) is zero replace h with qh. */

        for j in 1..n {
            h[j].re = qh[j - 1].re;
            h[j].im = qh[j - 1].im;
        }
        h[0].re = 0.0;
        h[0].im = 0.0;
    }
}

/// evaluates a polynomial  p  at  s  by the horner recurrence
/// placing the partial sums in q and the computed value in v_.
fn polyev(n: usize, s: &Complex64, p: &[Complex64], q: &mut [Complex64], v: &mut Complex64) {
    q[0].re = p[0].re;
    q[0].im = p[0].im;
    v.re = q[0].re;
    v.im = q[0].im;
    for i in 1..n {
        let t = v.re * s.re - v.im * s.im + p[i].re;
        v.im = v.re * s.im + v.im * s.re + p[i].im;
        q[i].im = v.im;
        v.re = t;
        q[i].re = v.re;
    }
}

/// bounds the error in evaluating the polynomial by the horner
/// recurrence.
///
/// qr,qi  - the partial sum vectors
/// ms  - modulus of the point
/// mp  - modulus of polynomial value
/// a_re,m_re - error bounds on complex addition and multiplication
fn errev(n: usize, q: &[Complex64], ms: f64, mp: f64, a_re: f64, m_re: f64) -> f64 {
    let mut e = q[0].re.hypot(q[0].im) * m_re / (a_re + m_re);
    for q_i in &q[0..n] {
        e = e * ms + q_i.re.hypot(q_i.im);
    }

    e * (a_re + m_re) - mp * m_re
}

/// Computes a lower bound on the moduli of the zeros of a polynomial
/// pot\[1:nn\] is the modulus of the coefficients.
fn poly_cauchy(n: usize, sh: &mut [Complex64]) -> f64 {
    let n1 = n - 1;

    sh[n1].re = -sh[n1].re;

    /* compute upper estimate of bound. */
    let mut x = (((-sh[n1].re).ln() - sh[0].re.ln()) / n1 as f64).exp();

    /* if newton step at the origin is better, use it. */
    if sh[n1 - 1].re != 0. {
        let xm = -sh[n1].re / sh[n1 - 1].re;
        if xm < x {
            x = xm;
        }
    }

    /* chop the interval (0,x) unitl f le 0. */
    loop {
        let xm = x * 0.1;
        let mut f = sh[0].re;
        for sh_i in &sh[1..n] {
            f = f * xm + sh_i.re;
        }
        if f <= 0.0 {
            break;
        }
        x = xm;
    }

    let mut dx = x;

    /* do Newton iteration until x converges to two decimal places. */
    while (dx / x).abs() > 0.005 {
        sh[0].im = sh[0].re;
        for i in 1..n {
            sh[i].im = sh[i - 1].im * x + sh[i].re;
        }
        let f = sh[n1].im;
        let mut delf = sh[0].im;
        for sh_i in &sh[1..n1] {
            delf = delf * x + sh_i.im;
        }
        dx = f / delf;
        x -= dx;
    }

    x
}
