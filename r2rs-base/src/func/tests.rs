// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use num_complex::Complex64;
use num_traits::ToPrimitive;
use r2rs_nmath::{rng::MersenneTwister, traits::RNG};
use strafe_testing::{
    r::{RGenerator, RString, RTester},
    r_assert_relative_equal_result,
};

use crate::func::{
    diff::diff,
    polyroot::polyroot,
    psort::partial_sort,
    rle::{inverse_rle, rle, RunLengthEncoding},
    sample::sample,
    unique::unique,
    zapsmall::zapsmall,
};

pub fn polyroot_test_inner(coefficients: Vec<Complex64>) {
    let rust_ret = polyroot(&coefficients);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = polyroot({});",
            RString::from_complex64_slice(&coefficients),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    for (rust, &r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(r), &rust.re);
    }
}

pub fn unique_test_inner(seed: u16, size: f64) {
    let x = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = trunc(runif({}, 0, 10));",
            RString::from_f64(size)
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let rust_ret = unique(&x);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = unique(c({}));",
            RString::from_f64_slice(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn zapsmall_positive_test_inner(seed: u16, size: f64) {
    let x = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = c(1, runif({}, 0, 1e-6));",
            RString::from_f64(size)
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let rust_ret = zapsmall(&x, Some(14));
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = unique(c({}));",
            RString::from_f64_slice(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn zapsmall_negative_test_inner(seed: u16, size: f64) {
    let x = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = c(-1, runif({}, 0, -1e-6));",
            RString::from_f64(size)
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let rust_ret = zapsmall(&x, Some(14));
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = unique(c({}));",
            RString::from_f64_slice(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn rle_test_inner(seed: u16, size: f64) {
    let x = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = trunc(runif({}, 0, 2));",
            RString::from_f64(size)
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let rust_ret = rle(&x);
    let r_ret_lengths = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = rle({});",
            RString::from_f64_slice(&x),
        )))
        .set_display(&RString::from_str("ret[['lengths']]").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let r_ret_values = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = rle({});",
            RString::from_f64_slice(&x),
        )))
        .set_display(&RString::from_str("ret[['values']]").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let r_ret = r_ret_lengths
        .into_iter()
        .zip(r_ret_values.into_iter())
        .map(|(length, value)| RunLengthEncoding {
            length: length as usize,
            value,
        })
        .collect::<Vec<_>>();
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        assert_eq!(r, rust);
    }
}

pub fn inverse_rle_test_inner(seed: u16, size: f64) {
    let x_lengths = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = trunc(runif({}, 0, 10));",
            RString::from_f64(size)
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let x_values = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = trunc(runif({}));",
            RString::from_f64(size)
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let x = x_lengths
        .iter()
        .zip(x_values.iter())
        .map(|(length, value)| RunLengthEncoding {
            length: length.clone() as usize,
            value: value.clone(),
        })
        .collect::<Vec<_>>();
    let rust_ret = inverse_rle(&x);
    let r_ret = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = inverse.rle(list('lengths'={},'values'={}));",
            RString::from_f64_slice(&x_lengths),
            RString::from_f64_slice(&x_values),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn sample_replace_test_inner(seed: u16, size: f64) {
    let mut high_disp = 0;
    for i in 0..10 {
        let seed = seed.wrapping_add(i);
        let x = RGenerator::new()
            .set_seed(seed)
            .generate_uniform(size, (0, 1));
        let probs = RGenerator::new()
            .set_seed(seed + 1)
            .generate_uniform(size, (0, 1));
        let mut rng = MersenneTwister::new();
        rng.set_seed(seed as u32);
        let mut rust_ret = sample(
            &x,
            size.to_usize().unwrap(),
            true,
            Some(&probs.iter().map(|f| f.into()).collect::<Vec<_>>()),
            &mut rng,
        );
        let mut r_ret = RTester::new()
            .set_seed(seed)
            .set_script(&RString::from_string(format!(
                "ret = sample({}, {}, prob={}, replace=F);",
                RString::from_f64_slice(&x),
                RString::from_f64(size),
                RString::from_f64_slice(&probs)
            )))
            .set_display(&RString::from_str("ret").unwrap())
            .run()
            .expect("R running error")
            .as_f64_vec()
            .expect("R running error");
        rust_ret.sort_by(|f1, f2| f1.partial_cmp(f2).unwrap());
        r_ret.sort_by(|f1, f2| f1.partial_cmp(f2).unwrap());
        let disp = rust_ret
            .iter()
            .zip(r_ret.iter())
            .map(|(rs, r)| (rs - r).abs())
            .sum::<f64>();
        if disp > 20.0 {
            high_disp += 1;
        }
    }
    if high_disp > 1 {
        panic!("High Dispersion: {:?} runs with high dispersion", high_disp);
    }
}

pub fn sample_no_replace_test_inner(seed: u16, size: f64) {
    let mut high_disp = 0;
    for i in 0..10 {
        let seed = seed.wrapping_add(i);
        let x = RGenerator::new()
            .set_seed(seed)
            .generate_uniform(size, (0, 1));
        let probs = RGenerator::new()
            .set_seed(seed + 1)
            .generate_uniform(size, (0, 1));
        let mut rng = MersenneTwister::new();
        rng.set_seed(seed as u32);
        let mut rust_ret = sample(
            &x,
            size.to_usize().unwrap(),
            false,
            Some(&probs.iter().map(|f| f.into()).collect::<Vec<_>>()),
            &mut rng,
        );
        let mut r_ret = RTester::new()
            .set_seed(seed)
            .set_script(&RString::from_string(format!(
                "ret = sample({}, {}, prob={}, replace=F);",
                RString::from_f64_slice(&x),
                RString::from_f64(size),
                RString::from_f64_slice(&probs)
            )))
            .set_display(&RString::from_str("ret").unwrap())
            .run()
            .expect("R running error")
            .as_f64_vec()
            .expect("R running error");
        rust_ret.sort_by(|f1, f2| f1.partial_cmp(f2).unwrap());
        r_ret.sort_by(|f1, f2| f1.partial_cmp(f2).unwrap());
        let disp = rust_ret
            .iter()
            .zip(r_ret.iter())
            .map(|(rs, r)| (rs - r).abs())
            .sum::<f64>();
        if disp > 20.0 {
            high_disp += 1;
        }
    }
    if high_disp > 1 {
        panic!("High Dispersion: {:?} runs with high dispersion", high_disp);
    }
}

pub fn partial_sort_test_inner(seed: u16, size: f64) {
    let x = RGenerator::new()
        .set_seed(seed)
        .generate_uniform(size, (0, 1));
    let indices = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = sample(0:{}, {}, replace = FALSE);",
            RString::from_f64(size - 1.0),
            RString::from_f64((size / 10.0).max(1.0)),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = sort({}, partial={});",
            RString::from_f64_slice(&x),
            RString::from_f64_slice(&indices.iter().map(|x_i| x_i + 1.0).collect::<Vec<_>>())
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let mut rust_ret = x.clone();
    partial_sort(
        &mut rust_ret,
        &indices.iter().map(|x_i| *x_i as usize).collect::<Vec<_>>(),
    )
    .expect("Error running partial sort");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn diff_test_inner(seed: u16, size: f64) {
    let x = RGenerator::new()
        .set_seed(seed)
        .generate_uniform(size, (0, 1));
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = diff({});",
            RString::from_f64_slice(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    let rust_ret = diff(&x);
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}
