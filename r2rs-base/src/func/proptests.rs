// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_complex::Complex64;
use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_r64, stat_w64, strafe_default_proptest_options},
};
use strafe_type::tof64;

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn polyroot_test(
        a in stat_r64(None, None, None),
        b in stat_r64(None, None, None),
        c in stat_r64(None, None, None),
    ) {
        polyroot_test_inner(vec![
            Complex64::new(tof64!(a), 0.0),
            Complex64::new(tof64!(b), 0.0),
            Complex64::new(tof64!(c), 0.0),
        ]);
    }

    #[test]
    fn unique_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(None, Some(50.0), None),
    ) {
        unique_test_inner(seed, size);
    }

    #[test]
    fn zapsmall_positive_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(None, Some(50.0), None),
    ) {
        zapsmall_positive_test_inner(seed, size);
    }

    #[test]
    fn zapsmall_negative_positive_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(None, Some(50.0), None),
    ) {
        zapsmall_negative_test_inner(seed, size);
    }

    #[test]
    fn rle_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(Some(1.0), Some(50.0), None),
    ) {
        rle_test_inner(seed, size);
    }

    #[test]
    fn inverse_rle_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(Some(1.0), Some(50.0), None),
    ) {
        inverse_rle_test_inner(seed, size);
    }

    #[test]
    fn sample_replace_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(Some(2.0), Some(50.0), None),
    ) {
        sample_replace_test_inner(seed, size);
    }

    #[test]
    fn sample_no_replace_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(Some(2.0), Some(50.0), None),
    ) {
        sample_no_replace_test_inner(seed, size);
    }

    #[test]
    fn partial_sort_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(Some(2.0), Some(50.0), None),
    ) {
        partial_sort_test_inner(seed, size);
    }

    #[test]
    fn diff_test(
        seed in 0..std::u16::MAX,
        size in stat_w64(Some(2.0), Some(50.0), None),
    ) {
        diff_test_inner(seed, size);
    }
}
