use r2rs_nmath::traits::RNG;
use strafe_type::{Alpha64, FloatConstraint};

use crate::func::sample::sample;

pub fn bootstrap_ci<O: PartialOrd + Clone, R: RNG>(
    func: &dyn Fn(&[usize]) -> O,
    sample_length: usize,
    alpha: Alpha64,
    bootstrap_amount: Option<usize>,
    rng: &mut R,
) -> (O, O) {
    let mut values = Vec::new();
    for _ in 0..bootstrap_amount.unwrap_or(100) {
        let resample = sample(
            &(0..sample_length).collect::<Vec<_>>(),
            sample_length,
            true,
            None,
            rng,
        );

        values.push(func(&resample));
    }
    values.sort_by(|v1, v2| v1.partial_cmp(v2).unwrap());

    (
        values[(values.len() as f64 * (alpha.unwrap() / 2.0)).floor() as usize].clone(),
        values[(values.len() as f64 * (1.0 - (alpha.unwrap() / 2.0))).floor() as usize].clone(),
    )
}
