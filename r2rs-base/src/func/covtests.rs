// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_complex::Complex64;
use r2rs_nmath::rng::MersenneTwister;

use super::tests::*;
use crate::func::{psort::partial_sort, sample::sample};

#[test]
fn polyroot_test_1() {
    polyroot_test_inner(vec![
        Complex64 { re: 0.0, im: 0.0 },
        Complex64 { re: 1.0, im: 0.0 },
    ]);
}

#[test]
fn polyroot_test_2() {
    polyroot_test_inner(vec![Complex64 { re: 1.0, im: 0.0 }]);
}

#[test]
fn polyroot_test_3() {
    polyroot_test_inner(vec![
        Complex64 { re: 1.0, im: 0.0 },
        Complex64 { re: 2.0, im: 0.0 },
        Complex64 { re: 1.0, im: 0.0 },
    ]);
}

#[test]
fn polyroot_test_4() {
    polyroot_test_inner(vec![
        Complex64 { re: 1.0, im: 0.0 },
        Complex64 { re: 2.0, im: 0.0 },
        Complex64 { re: 2.0, im: 0.0 },
        Complex64 { re: 3.0, im: 0.0 },
    ]);
}

#[test]
fn polyroot_test_5() {
    polyroot_test_inner(vec![
        Complex64 { re: 4.8, im: 1.0 },
        Complex64 { re: 2.6, im: 0.0 },
        Complex64 { re: 2.0, im: -6.0 },
        Complex64 { re: 3.1, im: 0.0 },
        Complex64 { re: -3.1, im: 7.0 },
    ]);
}

#[test]
fn polyroot_test_6() {
    polyroot_test_inner(vec![
        Complex64 { re: 4.8, im: 1.0 },
        Complex64 { re: 2.6, im: 0.0 },
        Complex64 {
            re: 2e300,
            im: -6.0,
        },
        Complex64 { re: 3.1, im: 0.0 },
        Complex64 { re: -3.1, im: 7.0 },
    ]);
}

#[test]
fn polyroot_test_7() {
    polyroot_test_inner(vec![
        Complex64 { re: 1.0, im: 0.0 },
        Complex64 { re: 0.0, im: 0.0 },
    ]);
}

#[test]
fn polyroot_test_8() {
    polyroot_test_inner(vec![
        Complex64 { re: 1.0, im: 0.0 },
        Complex64 { re: 0.0, im: 0.0 },
        Complex64 {
            re: 1e-200,
            im: 0.0,
        },
        Complex64 { re: 1.0, im: 0.0 },
    ]);
}

#[test]
fn unique_test_1() {
    unique_test_inner(1, 10.0);
}

#[test]
fn zapsmall_positive_test_1() {
    zapsmall_positive_test_inner(1, 10.0);
}

#[test]
fn zapsmall_negative_test_1() {
    zapsmall_negative_test_inner(1, 10.0);
}

#[test]
fn rle_test_1() {
    rle_test_inner(1, 10.0);
}

#[test]
fn inverse_rle_test_1() {
    inverse_rle_test_inner(1, 10.0);
}

#[test]
fn sample_replace_test_1() {
    sample_replace_test_inner(1, 10.0);
}

#[test]
#[should_panic]
fn sample_bad_size() {
    let x = vec![0.0; 1];
    let mut rng = MersenneTwister::new();
    sample(&x, 2, true, None, &mut rng);
}

#[test]
fn sample_no_replace_test_1() {
    sample_no_replace_test_inner(1, 10.0);
}

#[test]
fn partial_sort_test_1() {
    partial_sort_test_inner(1, 10.0);
}

#[test]
fn partial_sort_2() {
    let mut x = vec![0.0, -1.0, 5.4, 5.4, 5.2, 1.1];
    let i = vec![2, 4];
    partial_sort(&mut x, &i).expect("Error running partial sort");
}

#[test]
#[should_panic]
fn partial_sort_too_many() {
    let mut x = vec![0.0; 1];
    let i = vec![2; 1];
    partial_sort(&mut x, &i).expect("Error running partial sort");
}

#[test]
#[should_panic]
fn partial_sort_out_of_bounds() {
    let mut x = vec![0.0; 1];
    let i = vec![0; 2];
    partial_sort(&mut x, &i).expect("Error running partial sort");
}

#[test]
fn diff_test_1() {
    diff_test_inner(1, 10.0);
}
