// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;
use r2rs_nmath::traits::DigitPrec;

/// Rounding of Numbers: Zapping Small Ones to Zero
///
/// ## Description:
///
/// ‘zapsmall’ determines a ‘digits’ argument ‘dr’ for calling
/// ‘round(x, digits = dr)’ such that values close to zero (compared
/// with the maximal absolute value) are ‘zapped’, i.e., replaced by
/// ‘0’.
///
/// ## Usage:
///
/// zapsmall(x, digits = getOption("digits"))
///
/// ## Arguments:
///
/// * x: a numeric or complex vector or any R number-like object which
///   has a ‘round’ method and basic arithmetic methods including
///   ‘log10()’.
/// * digits: integer indicating the precision to be used.
///
/// ## References:
///
/// Chambers, J. M. (1998) _Programming with Data.  A Guide to the S
/// Language_.  Springer.
///
/// ## Examples:
///
/// ```r
/// x2 <- pi * 100^(-1:3)
/// print(x2 / 1000, digits = 4)
/// zapsmall(x2 / 1000, digits = 4)
///
/// zapsmall(exp(1i*0:4*pi/2))
/// ```
pub fn zapsmall<T: Float + DigitPrec>(x: &[T], digits_arg: Option<isize>) -> Vec<T> {
    let digits = digits_arg.unwrap_or(7);
    let max = x
        .iter()
        .filter(|f| !f.is_nan())
        .max_by(|f1, f2| f1.partial_cmp(f2).unwrap())
        .and_then(|m| m.to_f64())
        .unwrap();
    let d = if max > 0.0 {
        0.max(digits - max.log10() as isize)
    } else {
        digits
    };
    x.iter().map(|x_i| x_i.round_to(d)).collect()
}
