#[cfg(test)]
#[macro_use]
extern crate approx;
extern crate core;

pub mod func;
pub mod traits;
