// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use r2rs_nmath::{rng::WichmannHill, traits::RNG};
use strafe_testing::r::{RString, RTester};

use super::tests::*;
use crate::traits::stat_slice::{QuantileType, StatisticalSlice};

#[test]
fn mean_test_1() {
    mean_inner(1);
}

#[test]
fn median_test_1() {
    median_inner(1);
}

#[test]
fn median_test_2() {
    assert_relative_eq!(1.0, vec![1.0].median());
}

#[test]
fn mode_test_1() {
    mode_inner(1);
}

#[test]
fn range_test_1() {
    range_inner(1);
}

#[test]
fn rank_test_1() {
    rank_inner(1);
}

#[test]
fn rank_test_2() {
    for (i, j) in vec![0.0, 5.0, 5.0, 10.0]
        .rank()
        .into_iter()
        .zip(vec![1.0, 2.5, 2.5, 4.0].into_iter())
    {
        assert_relative_eq!(i, j);
    }
}

#[test]
fn scale_test_1() {
    scale_inner(1);
}

#[test]
fn scale_by_test_1() {
    scale_by_inner(1, 5.0);
}

#[test]
fn scale_robust_test_1() {
    scale_robust_inner(1);
}

#[test]
fn center_test_1() {
    center_inner(1);
}

#[test]
fn center_by_test_1() {
    center_by_inner(1, 5.0);
}

#[test]
fn center_robust_test_1() {
    center_robust_inner(1);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1, QuantileType::InverseEmpiricalDistribution);
}

#[test]
fn quantile_test_2() {
    quantile_inner(1, QuantileType::InverseEmpiricalDistributionWithAveraging);
}

#[test]
fn quantile_test_3() {
    quantile_inner(1, QuantileType::NearestEvenOrder);
}

#[test]
fn quantile_test_4() {
    quantile_inner(1, QuantileType::LinearInterpolation);
}

#[test]
fn quantile_test_5() {
    quantile_inner(1, QuantileType::PiecewiseLinear);
}

#[test]
fn quantile_test_6() {
    quantile_inner(1, QuantileType::Minitab);
}

#[test]
fn quantile_test_7() {
    quantile_inner(1, QuantileType::S);
}

#[test]
fn quantile_test_8() {
    quantile_inner(1, QuantileType::MedianUnbiased);
}

#[test]
fn quantile_test_9() {
    quantile_inner(1, QuantileType::UnbiasedNormal);
}

#[test]
fn quantile_test_10() {
    let x = [5.0, 5.0, 5.0, 5.0, 5.0];
    let seed = 1;

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "quantile({}, type = {})",
            RString::from_f64_slice(&x),
            RString::from_f64(3),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rng = WichmannHill::new();
    rng.set_seed(seed);

    let rust_ret = x.quantile(&[0.0, 0.25, 0.5, 0.75, 1.0], QuantileType::NearestEvenOrder);

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

#[test]
fn jitter_test_1() {
    random_jitter_inner(1, 5.0, Some(5.0));
}

#[test]
fn jitter_test_2() {
    set_jitter_inner(&[0.0, 0.0], 1, 5.0, Some(0.0));
}

#[test]
fn jitter_test_3() {
    random_jitter_inner(1, 5.0, None);
}

#[test]
fn jitter_test_4() {
    set_jitter_inner(&[1.0], 1, 5.0, None);
}

#[test]
fn jitter_test_5() {
    set_jitter_inner(&[0.0], 1, 5.0, None);
}
