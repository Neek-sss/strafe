pub trait StatisticalSlice {
    type Item;

    /// Arithmetic Mean
    ///
    /// ## Description:
    ///
    /// Generic function for the (trimmed) arithmetic mean.
    ///
    /// ## Usage:
    ///
    /// mean(x, ...)
    ///
    /// ## Default S3 method:
    /// mean(x, trim = 0, na.rm = FALSE, ...)
    ///
    /// ## Arguments:
    ///
    /// * x: An R object.  Currently there are methods for numeric/logical
    ///vectors and date, date-time and time interval objects.
    ///Complex vectors are allowed for ‘trim = 0’, only.
    /// * trim: the fraction (0 to 0.5) of observations to be trimmed from
    ///each end of ‘x’ before the mean is computed.  Values of trim
    ///outside that range are taken as the nearest endpoint.
    /// * na.rm: a logical evaluating to ‘TRUE’ or ‘FALSE’ indicating whether
    ///‘NA’ values should be stripped before the computation
    ///proceeds.
    /// * ...: further arguments passed to or from other methods.
    ///
    /// ## Value:
    ///
    /// If ‘trim’ is zero (the default), the arithmetic mean of the values
    /// in ‘x’ is computed, as a numeric or complex vector of length one.
    /// If ‘x’ is not logical (coerced to numeric), numeric (including
    /// integer) or complex, ‘NA_real_’ is returned, with a warning.
    ///
    /// If ‘trim’ is non-zero, a symmetrically trimmed mean is computed
    /// with a fraction of ‘trim’ observations deleted from each end
    /// before the mean is computed.
    ///
    /// ## References:
    ///
    /// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
    /// Language_.  Wadsworth & Brooks/Cole.
    ///
    /// ## See Also:
    ///
    /// ‘weighted.mean’, ‘mean.POSIXct’, ‘colMeans’ for row and column
    /// means.
    ///
    /// ## Examples:
    ///
    /// ```r
    /// x <- c(0:10, 50)
    /// xm <- mean(x)
    /// c(xm, mean(x, trim = 0.10))
    /// ```
    fn mean(&self) -> Self::Item;

    /// Median Value
    ///
    /// ## Description:
    ///
    /// Compute the sample median.
    ///
    /// ## Usage:
    ///
    /// median(x, na.rm = FALSE, ...)
    ///
    /// ## Arguments:
    ///
    /// * x: an object for which a method has been defined, or a numeric
    ///vector containing the values whose median is to be computed.
    /// * na.rm: a logical value indicating whether ‘NA’ values should be
    ///stripped before the computation proceeds.
    /// * ...: potentially further arguments for methods; not used in the
    ///default method.
    ///
    /// ## Details:
    ///
    /// This is a generic function for which methods can be written.
    /// However, the default method makes use of ‘is.na’, ‘sort’ and
    /// ‘mean’ from package ‘base’ all of which are generic, and so the
    /// default method will work for most classes (e.g., ‘"Date"’) for
    /// which a median is a reasonable concept.
    ///
    /// ## Value:
    ///
    /// The default method returns a length-one object of the same type as
    /// ‘x’, except when ‘x’ is logical or integer of even length, when
    /// the result will be double.
    ///
    /// If there are no values or if ‘na.rm = FALSE’ and there are ‘NA’
    /// values the result is ‘NA’ of the same type as ‘x’ (or more
    /// generally the result of ‘x\[NA_integer_\]’).
    ///
    /// ## References:
    ///
    /// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
    /// Language_.  Wadsworth & Brooks/Cole.
    ///
    /// ## See Also:
    ///
    /// ‘quantile’ for general quantiles.
    ///
    /// ## Examples:
    ///
    /// ```r
    /// median(1:4)  # = 2.5 [even number]
    /// median(c(1:3, 100, 1000))  # = 3 [odd, robust]
    /// ```
    fn median(&self) -> Self::Item;
    fn mode(&self) -> Self::Item;

    /// Range of Values
    ///
    /// ## Description:
    ///
    /// ‘range’ returns a vector containing the minimum and maximum of all
    /// the given arguments.
    ///
    /// ## Usage:
    ///
    /// range(..., na.rm = FALSE)
    ///
    /// ## Default S3 method:
    /// range(..., na.rm = FALSE, finite = FALSE)
    ///
    /// ## Arguments:
    ///
    /// * ...: any ‘numeric’ or character objects.
    /// * na.rm: logical, indicating if ‘NA’'s should be omitted.
    /// * finite: logical, indicating if all non-finite elements should be
    ///omitted.
    ///
    /// ## Details:
    ///
    /// ‘range’ is a generic function: methods can be defined for it
    /// directly or via the ‘Summary’ group generic.  For this to work
    /// properly, the arguments ‘...’ should be unnamed, and dispatch is
    /// on the first argument.
    ///
    /// If ‘na.rm’ is ‘FALSE’, ‘NA’ and ‘NaN’ values in any of the
    /// arguments will cause ‘NA’ values to be returned, otherwise ‘NA’
    /// values are ignored.
    ///
    /// If ‘finite’ is ‘TRUE’, the minimum and maximum of all finite
    /// values is computed, i.e., ‘finite = TRUE’ _includes_ ‘na.rm =
    /// TRUE’.
    ///
    /// A special situation occurs when there is no (after omission of
    /// ‘NA’s) nonempty argument left, see ‘min’.
    ///
    /// ## S4 methods:
    ///
    /// This is part of the S4 ‘Summary’ group generic.  Methods for it
    /// must use the signature ‘x, ..., na.rm’.
    ///
    /// ## References:
    ///
    /// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
    /// Language_.  Wadsworth & Brooks/Cole.
    ///
    /// ## See Also:
    ///
    /// ‘min’, ‘max’.
    ///
    /// The ‘extendrange()’ utility in package ‘grDevices’.
    ///
    /// ## Examples:
    ///
    /// ```r
    /// (r.x <- range(stats::rnorm(100)))
    /// diff(r.x) # the SAMPLE range
    ///
    /// x <- c(NA, 1:3, -1:1/0); x
    /// range(x)
    /// range(x, na.rm = TRUE)
    /// range(x, finite = TRUE)
    /// ```
    fn range(&self) -> (Self::Item, Self::Item);

    /// Sample Ranks
    ///
    /// ## Description:
    ///
    /// Returns the sample ranks of the values in a vector.  Ties (i.e.,
    /// equal values) and missing values can be handled in several ways.
    ///
    /// ## Usage:
    ///
    /// rank(x, na.last = TRUE,
    /// ties.method = c("average", "first", "last", "random", "max", "min"))
    ///
    /// ## Arguments:
    ///
    ///x: a numeric, complex, character or logical vector.
    ///
    /// * na.last: a logical or character string controlling the treatment of
    ///‘NA’s. If ‘TRUE’, missing values in the data are put last; if
    ///‘FALSE’, they are put first; if ‘NA’, they are removed; if
    ///‘"keep"’ they are kept with rank ‘NA’.
    /// * ties.method: a character string specifying how ties are treated, see
    ///‘Details’; can be abbreviated.
    ///
    /// ## Details:
    ///
    /// If all components are different (and no ‘NA’s), the ranks are well
    /// defined, with values in ‘seq_along(x)’.  With some values equal
    /// (called ‘ties’), the argument ‘ties.method’ determines the result
    /// at the corresponding indices.  The ‘"first"’ method results in a
    /// permutation with increasing values at each index set of ties, and
    /// analogously ‘"last"’ with decreasing values.  The ‘"random"’
    /// method puts these in random order whereas the default,
    /// ‘"average"’, replaces them by their mean, and ‘"max"’ and ‘"min"’
    /// replaces them by their maximum and minimum respectively, the
    /// latter being the typical sports ranking.
    ///
    /// ‘NA’ values are never considered to be equal: for ‘na.last = TRUE’
    /// and ‘na.last = FALSE’ they are given distinct ranks in the order
    /// in which they occur in ‘x’.
    ///
    /// *NB*: ‘rank’ is not itself generic but ‘xtfrm’ is, and
    /// ‘rank(xtfrm(x), ....)’ will have the desired result if there is a
    /// ‘xtfrm’ method.  Otherwise, ‘rank’ will make use of ‘==’, ‘>’,
    /// ‘is.na’ and extraction methods for classed objects, possibly
    /// rather slowly.
    ///
    /// ## Value:
    ///
    /// A numeric vector of the same length as ‘x’ with names copied from
    /// ‘x’ (unless ‘na.last = NA’, when missing values are removed).  The
    /// vector is of integer type unless ‘x’ is a long vector or
    /// ‘ties.method = "average"’ when it is of double type (whether or
    /// not there are any ties).
    ///
    /// ## References:
    ///
    /// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
    /// Language_.  Wadsworth & Brooks/Cole.
    ///
    /// ## See Also:
    ///
    /// ‘order’ and ‘sort’; ‘xtfrm’, see above.
    ///
    /// ## Examples:
    ///
    /// ```r
    /// (r1 <- rank(x1 <- c(3, 1, 4, 15, 92)))
    /// x2 <- c(3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5)
    /// names(x2) <- letters[1:11]
    /// (r2 <- rank(x2)) # ties are averaged
    /// ```
    ///
    /// #### rank() is "idempotent": rank(rank(x)) == rank(x) :
    /// ```r
    /// stopifnot(rank(r1) == r1, rank(r2) == r2)
    /// ```
    ///
    /// #### ranks without averaging
    /// ```r
    /// rank(x2, ties.method= "first")  # first occurrence wins
    /// rank(x2, ties.method= "last")#  last occurrence wins
    /// rank(x2, ties.method= "random") # ties broken at random
    /// rank(x2, ties.method= "random") # and again
    /// ```
    ///
    /// #### keep ties ties, no average
    /// ```r
    /// (rma <- rank(x2, ties.method= "max"))  # as used classically
    /// (rmi <- rank(x2, ties.method= "min"))  # as in Sports
    /// stopifnot(rma + rmi == round(r2 + r2))
    /// ```
    ///
    /// #### Comparing all tie.methods:
    /// ```r
    /// tMeth <- eval(formals(rank)$ties.method)
    /// rx2 <- sapply(tMeth, function(M) rank(x2, ties.method=M))
    /// cbind(x2, rx2)
    /// ```
    ///
    /// #### ties.method's does not matter w/o ties:
    /// ```r
    /// x <- sample(47)
    /// rx <- sapply(tMeth, function(MM) rank(x, ties.method=MM))
    /// stopifnot(all(rx[,1] == rx))
    /// ```
    fn rank(&self) -> Vec<f64>;

    /// Scaling and Centering of Matrix-like Objects
    ///
    /// Description:
    ///
    /// ‘scale’ is generic function whose default method centers and/or
    /// scales the columns of a numeric matrix.
    ///
    /// Usage:
    ///
    /// scale(x, center = TRUE, scale = TRUE)
    ///
    /// Arguments:
    ///
    /// * x: a numeric matrix(like object).
    /// * center: either a logical value or numeric-alike vector of length
    ///equal to the number of columns of ‘x’, where ‘numeric-alike’
    ///means that ‘as.numeric(.)’ will be applied successfully if
    ///‘is.numeric(.)’ is not true.
    /// * scale: either a logical value or a numeric-alike vector of length
    ///equal to the number of columns of ‘x’.
    ///
    /// Details:
    ///
    /// The value of ‘center’ determines how column centering is
    /// performed.  If ‘center’ is a numeric-alike vector with length
    /// equal to the number of columns of ‘x’, then each column of ‘x’ has
    /// the corresponding value from ‘center’ subtracted from it.  If
    /// ‘center’ is ‘TRUE’ then centering is done by subtracting the
    /// column means (omitting ‘NA’s) of ‘x’ from their corresponding
    /// columns, and if ‘center’ is ‘FALSE’, no centering is done.
    ///
    /// The value of ‘scale’ determines how column scaling is performed
    /// (after centering).  If ‘scale’ is a numeric-alike vector with
    /// length equal to the number of columns of ‘x’, then each column of
    /// ‘x’ is divided by the corresponding value from ‘scale’.  If
    /// ‘scale’ is ‘TRUE’ then scaling is done by dividing the (centered)
    /// columns of ‘x’ by their standard deviations if ‘center’ is ‘TRUE’,
    /// and the root mean square otherwise.  If ‘scale’ is ‘FALSE’, no
    /// scaling is done.
    ///
    /// The root-mean-square for a (possibly centered) column is defined
    /// as $\sqrt{\frac{\sum{x^2}}{n-1}}$, where x is a vector of the non-missing
    /// values and n is the number of non-missing values.  In the case
    /// ‘center = TRUE’, this is the same as the standard deviation, but
    /// in general it is not.  (To scale by the standard deviations
    /// without centering, use ‘scale(x, center = FALSE, scale = apply(x,
    /// 2, sd, na.rm = TRUE))’.)
    ///
    /// Value:
    ///
    /// For ‘scale.default’, the centered, scaled matrix.  The numeric
    /// centering and scalings used (if any) are returned as attributes
    /// ‘"scaled:center"’ and ‘"scaled:scale"’
    ///
    /// References:
    ///
    /// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
    /// Language_.  Wadsworth & Brooks/Cole.
    ///
    /// See Also:
    ///
    /// ‘sweep’ which allows centering (and scaling) with arbitrary
    /// statistics.
    ///
    /// For working with the scale of a plot, see ‘par’.
    ///
    /// Examples:
    ///
    /// ```r
    /// require(stats)
    /// x <- matrix(1:10, ncol = 2)
    /// (centered.x <- scale(x, scale = FALSE))
    /// cov(centered.scaled.x <- scale(x)) # all 1
    /// ```
    fn scale(&mut self);
    fn scale_by(&mut self, scale: Self::Item);
    fn scale_robust(&mut self);
    fn center(&mut self);
    fn center_by(&mut self, center: Self::Item);
    fn center_robust(&mut self);

    /// Sample Quantiles
    ///
    /// ## Description:
    ///
    /// The generic function ‘quantile’ produces sample quantiles
    /// corresponding to the given probabilities.  The smallest
    /// observation corresponds to a probability of 0 and the largest to a
    /// probability of 1.
    ///
    /// ## Usage:
    ///
    /// quantile(x, ...)
    ///
    /// ## Default S3 method:
    /// quantile(x, probs = seq(0, 1, 0.25), na.rm = FALSE,
    ///  names = TRUE, type = 7, digits = 7, ...)
    ///
    /// ## Arguments:
    ///
    /// * x: numeric vector whose sample quantiles are wanted, or an
    /// object of a class for which a method has been defined (see
    /// also ‘details’). ‘NA’ and ‘NaN’ values are not allowed in
    /// numeric vectors unless ‘na.rm’ is ‘TRUE’.
    ///
    /// * probs: numeric vector of probabilities with values in \[0,1\].
    /// (Values up to ‘2e-14’ outside that range are accepted and
    /// moved to the nearby endpoint.)
    ///
    /// * na.rm: logical; if true, any ‘NA’ and ‘NaN’'s are removed from ‘x’
    /// before the quantiles are computed.
    ///
    /// * names: logical; if true, the result has a ‘names’ attribute.  Set to
    /// ‘FALSE’ for speedup with many ‘probs’.
    ///
    /// * type: an integer between 1 and 9 selecting one of the nine quantile
    /// algorithms detailed below to be used.
    ///
    /// * digits: used only when ‘names’ is true: the precision to use when
    /// formatting the percentages.  In R versions up to 4.0.x, this
    /// had been set to ‘max(2, getOption("digits"))’, internally.
    ///
    /// * ...: further arguments passed to or from other methods.
    ///
    /// ## Details:
    ///
    /// A vector of length ‘length(probs)’ is returned; if ‘names = TRUE’,
    /// it has a ‘names’ attribute.
    ///
    /// ‘NA’ and ‘NaN’ values in ‘probs’ are propagated to the result.
    ///
    /// The default method works with classed objects sufficiently like
    /// numeric vectors that ‘sort’ and (not needed by types 1 and 3)
    /// addition of elements and multiplication by a number work
    /// correctly.  Note that as this is in a namespace, the copy of
    /// ‘sort’ in ‘base’ will be used, not some S4 generic of that name.
    /// Also note that that is no check on the ‘correctly’, and so e.g.
    /// ‘quantile’ can be applied to complex vectors which (apart from
    /// ties) will be ordered on their real parts.
    ///
    /// There is a method for the date-time classes (see ‘"POSIXt"’).
    /// Types 1 and 3 can be used for class ‘"Date"’ and for ordered
    /// factors.
    ///
    /// ## Types:
    ///
    /// ‘quantile’ returns estimates of underlying distribution quantiles
    /// based on one or two order statistics from the supplied elements in
    /// ‘x’ at probabilities in ‘probs’.  One of the nine quantile
    /// algorithms discussed in Hyndman and Fan (1996), selected by
    /// ‘type’, is employed.
    ///
    /// All sample quantiles are defined as weighted averages of
    /// consecutive order statistics. Sample quantiles of type i are
    /// defined by:
    ///
    ///$Q\[i\](p) = (1 - \gamma) x\[j\] + \gamma x\[j+1\]$,
    ///
    /// where $1 <= i <= 9$, $(j-m)/n <= p < (j-m+1)/n$, $x\[j\]$ is the $j$th order
    /// statistic, $n$ is the sample size, the value of gamma is a function
    /// of $j = \text{floor}(np + m)$ and $g = np + m - j$, and $m$ is a constant
    /// determined by the sample quantile type.
    ///
    /// *Discontinuous sample quantile types 1, 2, and 3*
    ///
    /// For types 1, 2 and 3, $Q\[i\](p)$ is a discontinuous function of p,
    /// with $m = 0$ when $i = 1$ and $i = 2$, and $m = -1/2$ when $i = 3$.
    ///
    /// * Type 1 Inverse of empirical distribution function.  $\gamma = 0$ if $g = 0$,
    ///and 1 otherwise.
    ///
    /// * Type 2 Similar to type 1 but with averaging at discontinuities.
    ///$\gamma = 0.5$ if $g = 0$, and 1 otherwise (SAS default, see
    ///Wicklin(2017)).
    ///
    /// * Type 3 Nearest even order statistic (SAS default till ca. 2010).
    ///$\gamma = 0$ if $g = 0$ and $j$ is even, and 1 otherwise.
    ///
    /// *Continuous sample quantile types 4 through 9*
    ///
    /// For types 4 through 9, $Q\[i\](p)$ is a continuous function of $p$, with
    /// $\gamma = g$ and $m$ given below. The sample quantiles can be obtained
    /// equivalently by linear interpolation between the points
    /// $(p\[k\],x\[k\])$ where $x\[k\]$ is the $k$th order statistic.  Specific
    /// expressions for $p\[k\]$ are given below.
    ///
    /// * Type 4 $m = 0$. $p\[k\] = k / n$.  That is, linear interpolation of the
    ///empirical cdf.
    ///
    /// * Type 5 $m = 1/2$.  $p\[k\] = (k - 0.5) / n$.  That is a piecewise linear
    ///function where the knots are the values midway through the
    ///steps of the empirical cdf.  This is popular amongst
    ///hydrologists.
    ///
    /// * Type 6 $m = p$. $p\[k\] = k / (n + 1)$.  Thus $p\[k\] = E\[F(x\[k\])\]$.  This
    ///is used by Minitab and by SPSS.
    ///
    /// * Type 7 $m = 1-p$.  $p\[k\] = (k - 1) / (n - 1)$.  In this case,
    ///$p\[k\] = \text{mode}\[F(x\[k\])\]$.  This is used by S.
    ///
    /// * Type 8 $m = (p+1)/3$.  $p\[k\] = (k - 1/3) / (n + 1/3)$.  Then
    ///$p\[k\] =~ \text{median}\[F(x\[k\])\]$. The resulting quantile estimates are
    ///approximately median-unbiased regardless of the distribution
    ///of ‘x’.
    ///
    /// * Type 9 $m = p/4 + 3/8$.  $p\[k\] = (k - 3/8) / (n + 1/4)$.  The
    ///resulting quantile estimates are approximately unbiased for
    ///the expected order statistics if ‘x’ is normally distributed.
    ///
    /// Further details are provided in Hyndman and Fan (1996) who
    /// recommended type 8.  The default method is type 7, as used by S
    /// and by R < 2.0.0.  Makkonen argues for type 6, also as already
    /// proposed by Weibull in 1939.  The Wikipedia page contains further
    /// information about availability of these 9 types in software.
    ///
    /// ## Author(s):
    ///
    /// of the version used in R >= 2.0.0, Ivan Frohne and Rob J Hyndman.
    ///
    /// ## References:
    ///
    /// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
    /// Language_.  Wadsworth & Brooks/Cole.
    ///
    /// Hyndman, R. J. and Fan, Y. (1996) Sample quantiles in statistical
    /// packages, _American Statistician_ *50*, 361-365.
    /// doi:10.2307/2684934 <https://doi.org/10.2307/2684934>.
    ///
    /// Wicklin, R. (2017) Sample quantiles: A comparison of 9
    /// definitions; SAS Blog.
    /// <https://blogs.sas.com/content/iml/2017/05/24/definitions-sample-quantiles.html>
    ///
    /// Wikipedia:
    /// <https://en.wikipedia.org/wiki/Quantile#Estimating_quantiles_from_a_sample>
    ///
    /// ## See Also:
    ///
    /// ‘ecdf’ for empirical distributions of which ‘quantile’ is an
    /// inverse; ‘boxplot.stats’ and ‘fivenum’ for computing other
    /// versions of quartiles, etc.
    ///
    /// ## Examples:
    ///
    /// ```r
    /// quantile(x <- rnorm(1001)) # Extremes & Quartiles by default
    /// quantile(x,  probs = c(0.1, 0.5, 1, 2, 5, 10, 50, NA)/100)
    ///
    /// ### Compare different types
    /// quantAll <- function(x, prob, ...)
    /// t(vapply(1:9, function(typ) quantile(x, probs = prob, type = typ, ...),
    ///  quantile(x, prob, type=1, ...)))
    /// p <- c(0.1, 0.5, 1, 2, 5, 10, 50)/100
    /// signif(quantAll(x, p), 4)
    ///
    /// ## 0% and 100% are equal to min(), max() for all types:
    /// stopifnot(t(quantAll(x, prob=0:1)) == range(x))
    ///
    /// ## for complex numbers:
    /// z <- complex(real = x, imaginary = -10*x)
    /// signif(quantAll(z, p), 4)
    /// ```
    fn quantile(&self, percentages: &[f64], rtype: QuantileType) -> Vec<f64>;

    /// # The Interquartile Range
    ///
    /// ## Description:
    ///
    /// computes interquartile range of the ‘x’ values.
    ///
    /// ## Usage:
    ///
    /// IQR(x, na.rm = FALSE, type = 7)
    ///
    /// ## Arguments:
    ///
    /// * x: a numeric vector.
    /// * na.rm: logical. Should missing values be removed?
    /// * type: an integer selecting one of the many quantile algorithms, see
    /// ‘quantile’.
    ///
    /// ## Details:
    ///
    /// Note that this function computes the quartiles using the
    /// ‘quantile’ function rather than following Tukey's recommendations,
    /// i.e., ‘IQR(x) = quantile(x, 3/4) - quantile(x, 1/4)’.
    ///
    /// For normally N(m,1) distributed X, the expected value of ‘IQR(X)’
    /// is ‘2*qnorm(3/4) = 1.3490’, i.e., for a normal-consistent estimate
    /// of the standard deviation, use ‘IQR(x) / 1.349’.
    ///
    /// ## References:
    ///
    /// Tukey, J. W. (1977).  _Exploratory Data Analysis._ Reading:
    /// Addison-Wesley.
    ///
    /// ## See Also:
    ///
    /// ‘fivenum’, ‘mad’ which is more robust, ‘range’, ‘quantile’.
    ///
    /// ## Examples:
    ///
    /// ```r
    /// IQR(rivers)
    /// ```
    fn iqr(&self, qtype: QuantileType) -> f64;

    /// 'Jitter' (Add Noise) to Numbers
    ///
    /// ## Description:
    ///
    /// Add a small amount of noise to a numeric vector.
    ///
    /// ## Usage:
    ///
    /// jitter(x, factor = 1, amount = NULL)
    ///
    /// ## Arguments:
    ///
    /// * x: numeric vector to which _jitter_ should be added.
    /// *factor: numeric.
    /// *amount: numeric; if positive, used as _amount_ (see below),
    ///otherwise, if ‘= 0’ the default is ‘factor * z/50’.
    ///
    /// Default (‘NULL’): ‘factor * d/5’ where ‘d’ is about the
    /// smallest difference between ‘x’ values.
    ///
    /// ## Details:
    ///
    /// The result, say ‘r’, is ‘r <- x + runif(n, -a, a)’ where ‘n <-
    /// length(x)’ and ‘a’ is the ‘amount’ argument (if specified).
    ///
    /// Let ‘z <- max(x) - min(x)’ (assuming the usual case).  The amount
    /// ‘a’ to be added is either provided as _positive_ argument ‘amount’
    /// or otherwise computed from ‘z’, as follows:
    ///
    /// If ‘amount == 0’, we set ‘a <- factor * z/50’ (same as S).
    ///
    /// If ‘amount’ is ‘NULL’ (_default_), we set ‘a <- factor * d/5’
    /// where _d_ is the smallest difference between adjacent unique
    /// (apart from fuzz) ‘x’ values.
    ///
    /// ## Value:
    ///
    /// ‘jitter(x, ...)’ returns a numeric of the same length as ‘x’, but
    /// with an ‘amount’ of noise added in order to break ties.
    ///
    /// ## Author(s):
    ///
    /// Werner Stahel and Martin Maechler, ETH Zurich
    ///
    /// ## References:
    ///
    /// Chambers, J. M., Cleveland, W. S., Kleiner, B. and Tukey, P.A.
    /// (1983) _Graphical Methods for Data Analysis._ Wadsworth; figures
    /// 2.8, 4.22, 5.4.
    ///
    /// Chambers, J. M. and Hastie, T. J. (1992) _Statistical Models in
    /// S._ Wadsworth & Brooks/Cole.
    ///
    /// ## See Also:
    ///
    /// ‘rug’ which you may want to combine with ‘jitter’.
    ///
    /// ## Examples:
    ///
    /// ```r
    /// round(jitter(c(rep(1, 3), rep(1.2, 4), rep(3, 3))), 3)
    /// ## These two 'fail' with S-plus 3.x:
    /// jitter(rep(0, 7))
    /// jitter(rep(10000, 5))
    /// ```
    fn jitter<R: NMathRNG + RNG>(
        &mut self,
        factor: Option<f64>,
        amount: Option<Positive64>,
        rng: &mut R,
    );
    fn sequence(start: Self::Item, end: Self::Item, length: usize) -> Vec<Self::Item>;
    fn sequence_by(start: Self::Item, end: Self::Item, by: Self::Item) -> Vec<Self::Item>;
}

use std::{cmp::PartialOrd, mem::swap};

use itertools::Itertools;
use num_traits::{Float, FromPrimitive, Num, NumAssign, ToPrimitive};
use r2rs_nmath::{
    distribution::UniformBuilder,
    rng::NMathRNG,
    traits::{DigitPrec, Distribution, RNG},
};
use strafe_type::{FloatConstraint, Positive64};

use crate::func::diff;

pub enum QuantileType {
    InverseEmpiricalDistribution,
    InverseEmpiricalDistributionWithAveraging,
    NearestEvenOrder,
    LinearInterpolation,
    PiecewiseLinear,
    Minitab,
    S,
    MedianUnbiased,
    UnbiasedNormal,
}

impl<I> StatisticalSlice for [I]
where
    I: Num
        + NumAssign
        + Float
        + FromPrimitive
        + ToPrimitive
        + PartialOrd
        + Clone
        + std::fmt::Debug
        + std::iter::Sum,
{
    type Item = I;

    fn mean(&self) -> Self::Item {
        self.iter().cloned().sum::<Self::Item>() / Self::Item::from_usize(self.len()).unwrap()
    }

    fn median(&self) -> Self::Item {
        let mut s = (0..self.len()).collect::<Vec<_>>();
        s.sort_by(|&i1, &i2| self[i1].partial_cmp(&self[i2]).unwrap());

        let l = s.len();
        if l == 1 {
            self[0]
        } else {
            let left = self[s[((l as f64 - 1.0) / 2.0).floor() as usize]];
            let right = self[s[((l as f64 - 1.0) / 2.0).ceil() as usize]];
            (left + right) / Self::Item::from_u8(2).unwrap()
        }
    }

    fn mode(&self) -> Self::Item {
        let mut s = (0..self.len()).collect::<Vec<_>>();
        s.sort_by(|&i1, &i2| self[i1].partial_cmp(&self[i2]).unwrap());

        let mut stored_num = &self[s[0]];
        let mut current_num = &self[s[0]];
        let mut stored_count = 0;
        let mut current_count = 0;
        s.iter().for_each(|&i| {
            if &self[i] == current_num {
                current_count += 1
            } else {
                current_count = 1;
                current_num = &self[i];
            }
            if current_count > stored_count {
                stored_count = current_count;
                stored_num = current_num;
            }
        });

        *stored_num
    }

    fn range(&self) -> (Self::Item, Self::Item) {
        let min = *self
            .iter()
            .min_by(|i1, i2| i1.partial_cmp(i2).unwrap())
            .unwrap();
        let max = *self
            .iter()
            .max_by(|i1, i2| i1.partial_cmp(i2).unwrap())
            .unwrap();
        (min, max)
    }

    fn rank(&self) -> Vec<f64> {
        let mut ret = vec![0.0; self.len()];

        for i in 0..self.len() {
            let mut r = 1.0;
            let mut s = 1.0;

            for j in 0..self.len() {
                if j != i && self[j] < self[i] {
                    r += 1.0;
                }

                if j != i && self[j] == self[i] {
                    s += 1.0;
                }
            }

            ret[i] = r + (s - 1.0) / 2.0;
        }

        ret
    }

    fn scale(&mut self) {
        let numerator = self.iter().cloned().map(|i| i.powi(2)).sum::<Self::Item>();
        let denominator = Self::Item::from_usize((self.len() - 1).max(1)).unwrap();
        let scale = (numerator / denominator).sqrt();

        self.scale_by(scale);
    }

    fn scale_by(&mut self, scale: Self::Item) {
        self.iter_mut().for_each(|i| *i /= scale);
    }

    fn scale_robust(&mut self) {
        let quantile = self.quantile(&[0.25, 0.75], QuantileType::S);
        let iqr = quantile[1] - quantile[0];

        self.scale_by(I::from(iqr).unwrap());
    }

    fn center(&mut self) {
        let center = self.mean();

        self.center_by(center);
    }

    fn center_by(&mut self, center: Self::Item) {
        self.iter_mut().for_each(|i| *i -= center);
    }

    fn center_robust(&mut self) {
        let center = self.median();

        self.center_by(center);
    }

    fn quantile(&self, percentages: &[f64], qtype: QuantileType) -> Vec<f64> {
        let ordered_self = self
            .iter()
            .map(|x| x.to_f64().unwrap())
            .sorted_by(|x1, x2| x1.partial_cmp(x2).unwrap())
            .collect::<Vec<_>>();
        let n = self.len();

        let (h, j) = if let QuantileType::InverseEmpiricalDistribution
        | QuantileType::InverseEmpiricalDistributionWithAveraging
        | QuantileType::NearestEvenOrder = qtype
        {
            let nppm = percentages
                .iter()
                .map(|p| {
                    if let QuantileType::NearestEvenOrder = qtype {
                        n as f64 * p - 0.5
                    } else {
                        n as f64 * p
                    }
                })
                .collect::<Vec<_>>();
            let j = nppm.iter().map(|nppm| nppm.floor()).collect::<Vec<_>>();

            let h = match qtype {
                QuantileType::InverseEmpiricalDistribution => nppm
                    .iter()
                    .zip(j.iter())
                    .map(|(nppm, j)| if nppm > j { 1.0 } else { 0.0 })
                    .collect::<Vec<_>>(),
                QuantileType::InverseEmpiricalDistributionWithAveraging => nppm
                    .iter()
                    .zip(j.iter())
                    .map(|(nppm, j)| if nppm > j { 1.0 } else { 0.5 })
                    .collect::<Vec<_>>(),
                QuantileType::NearestEvenOrder => nppm
                    .iter()
                    .zip(j.iter())
                    .map(|(nppm, j)| {
                        if (nppm != j) || ((j % 2.0) == 1.0) {
                            1.0
                        } else {
                            0.0
                        }
                    })
                    .collect::<Vec<_>>(),
                _ => unreachable!(),
            };

            (h, j)
        } else {
            let (a, b) = match qtype {
                QuantileType::LinearInterpolation => (0.0, 1.0),
                QuantileType::PiecewiseLinear => (0.5, 0.5),
                QuantileType::Minitab => (0.0, 0.0),
                QuantileType::S => (1.0, 1.0),
                QuantileType::MedianUnbiased => (1.0 / 3.0, 1.0 / 3.0),
                QuantileType::UnbiasedNormal => (3.0 / 8.0, 3.0 / 8.0),
                _ => unreachable!(),
            };
            let eps = 2.220446e-16;
            let fuzz = 4.0 * eps;

            let nppm = percentages
                .iter()
                .map(|p| a + p * (n as f64 + 1.0 - a - b))
                .collect::<Vec<_>>();
            let j = nppm
                .iter()
                .map(|nppm| (nppm + fuzz).floor())
                .collect::<Vec<_>>();
            let h = nppm
                .iter()
                .zip(j.iter())
                .map(|(nppm, j)| {
                    let h = nppm - j;
                    if h.abs() < fuzz {
                        0.0
                    } else {
                        h
                    }
                })
                .collect::<Vec<_>>();

            (h, j)
        };

        let mut temp_x = ordered_self;
        temp_x.insert(0, temp_x[0]);
        temp_x.insert(0, temp_x[0]);
        temp_x.push(*temp_x.last().unwrap());
        temp_x.push(*temp_x.last().unwrap());

        j.iter()
            .zip(h.iter())
            .map(|(j, h)| {
                if *h == 0.0 {
                    temp_x[(j + 1.0) as usize]
                } else if *h == 1.0 {
                    temp_x[(j + 2.0) as usize]
                } else {
                    (1.0 - h) * temp_x[(j + 1.0) as usize] + h * temp_x[(j + 2.0) as usize]
                }
            })
            .collect()
    }

    fn iqr(&self, qtype: QuantileType) -> f64 {
        let quantiles = self.quantile(&[0.25, 0.75], qtype);
        quantiles[1] - quantiles[0]
    }

    fn jitter<R: NMathRNG + RNG>(
        &mut self,
        factor: Option<f64>,
        amount: Option<Positive64>,
        rng: &mut R,
    ) {
        let factor = factor.unwrap_or(1.0);

        let (r1, r2) = self
            .iter()
            .filter(|x| x.is_finite())
            .cloned()
            .dedup_by(|f1, f2| f1 == f2)
            .collect::<Vec<_>>()
            .range();
        let mut z = r2 - r1;

        if z == Self::Item::zero() {
            z = self[0];
            if z == Self::Item::zero() {
                z = Self::Item::one();
            }
        }

        let amount = if let Some(am) = amount {
            if am.unwrap() == 0.0 {
                factor * (z.to_f64().unwrap() / 50.0)
            } else {
                am.unwrap()
            }
        } else {
            let round_to = (3.0 - z.to_f64().unwrap().log10().floor()) as isize;
            let xx = self
                .iter()
                .map(|xi| xi.to_f64().unwrap().round_to(round_to))
                .sorted_by(|f1, f2| f1.partial_cmp(f2).unwrap())
                .dedup_by(|f1, f2| f1 == f2)
                .collect::<Vec<_>>();
            let d = diff(&xx);

            if !d.is_empty() {
                *d.iter()
                    .min_by(|f1, f2| f1.partial_cmp(f2).unwrap())
                    .unwrap()
            } else if xx[0] != 0.0 {
                xx[0] / 10.0
            } else {
                z.to_f64().unwrap() / 10.0
            }
        };

        let mut unif_builder = UniformBuilder::new();
        unif_builder.with_bounds(-amount, amount);
        let uniform = unif_builder.build();

        self.iter_mut().for_each(|xi| {
            *xi = Self::Item::from(
                xi.to_f64().unwrap() + uniform.random_sample(rng).to_f64().unwrap(),
            )
            .unwrap()
        })
    }

    fn sequence(mut start: Self::Item, mut end: Self::Item, length: usize) -> Vec<Self::Item> {
        let mut reverse = false;
        if start < end {
            swap(&mut start, &mut end);
            reverse = true;
        }
        let mut ret = (0..=length)
            .map(|i| {
                (Self::Item::from_usize(i).unwrap() / Self::Item::from_usize(length).unwrap()
                    * (end - start))
                    + start
            })
            .collect::<Vec<_>>();

        if reverse {
            ret = ret.into_iter().rev().collect();
        }

        ret
    }

    fn sequence_by(start: Self::Item, end: Self::Item, by: Self::Item) -> Vec<Self::Item> {
        Self::sequence(start, end, ((end - start).abs() / by).to_usize().unwrap())
    }
}

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
