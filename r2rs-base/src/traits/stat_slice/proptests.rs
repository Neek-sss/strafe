// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_f64, stat_pos64, strafe_default_proptest_options},
};

use super::tests::*;
use crate::traits::stat_slice::QuantileType;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn mean(
        seed in prop::num::u16::ANY,
    ) {
        mean_inner(seed);
    }

    #[test]
    fn median(
        seed in prop::num::u16::ANY,
    ) {
        median_inner(seed);
    }

    #[test]
    fn mode(
        seed in prop::num::u16::ANY,
    ) {
        mode_inner(seed);
    }

    #[test]
    fn range(
        seed in prop::num::u16::ANY,
    ) {
        range_inner(seed);
    }

    #[test]
    fn rank(
        seed in prop::num::u16::ANY,
    ) {
        rank_inner(seed);
    }

    #[test]
    fn scale(
        seed in prop::num::u16::ANY,
    ) {
        scale_inner(seed);
    }

    #[test]
    fn scale_by(
        seed in prop::num::u16::ANY,
        scale in stat_f64(None, None, None),
    ) {
        scale_by_inner(seed, scale);
    }

    #[test]
    fn scale_robust(
        seed in prop::num::u16::ANY,
    ) {
        scale_robust_inner(seed);
    }

    #[test]
    fn center(
        seed in prop::num::u16::ANY,
    ) {
        center_inner(seed);
    }

    #[test]
    fn center_by(
        seed in prop::num::u16::ANY,
        center in stat_f64(None, None, None),
    ) {
        center_by_inner(seed, center);
    }

    #[test]
    fn center_robust(
        seed in prop::num::u16::ANY,
    ) {
        center_robust_inner(seed);
    }

    #[test]
    fn quantile(
        seed in prop::num::u16::ANY,
        qtype in 0..9,
    ) {
        let qtype = match qtype {
            0 => QuantileType::InverseEmpiricalDistribution,
            1 => QuantileType::InverseEmpiricalDistributionWithAveraging,
            2 => QuantileType::NearestEvenOrder,
            3 => QuantileType::LinearInterpolation,
            4 => QuantileType::PiecewiseLinear,
            5 => QuantileType::Minitab,
            6 => QuantileType::S,
            7 => QuantileType::MedianUnbiased,
            8 => QuantileType::UnbiasedNormal,
            _ => unreachable!(),
        };
        quantile_inner(seed, qtype);
    }

    #[test]
    fn jitter(
        seed in prop::num::u16::ANY,
        factor in stat_f64(None, None, None),
        amount in stat_pos64(None, None, None),
    ) {
        jitter_inner(seed, factor, amount);
    }
}
