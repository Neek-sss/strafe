// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::{collections::HashMap, str::FromStr};

use r2rs_nmath::{rng::WichmannHill, traits::RNG};
use strafe_testing::r::{RString, RTester};

use crate::traits::stat_slice::{QuantileType, StatisticalSlice};

pub fn mean_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "mean({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = random_nums.mean();

    assert_relative_eq!(r_ret, rust_ret, max_relative = 0.0001);
}

pub fn median_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "median({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = random_nums.median();

    assert_relative_eq!(r_ret, rust_ret, max_relative = 0.0001);
}

pub fn mode_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("rbinom({}, 5, .3)", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = {
        let full_list = RTester::new()
            .set_script(&RString::from_str(
                "getmode <- function(v) {d = data.frame(summary(as.factor(v)));c(as.numeric(row.names(d)), d[,1])};"
            ).unwrap())
            .set_display(&RString::from_string(format!(
                "getmode({})",
                RString::from_f64_slice(&random_nums))
            ))
            .run()
            .unwrap()
            .as_f64_vec()
            .unwrap();
        let headers = &full_list[..(full_list.len() / 2)];
        let data = &full_list[(full_list.len() / 2)..];
        let map = headers
            .iter()
            .zip(data.iter())
            .map(|(h, d)| (format!("{}", *h as usize), *d as usize))
            .collect::<HashMap<_, _>>();
        let max = *map
            .iter()
            .max_by(|(_, d1), (_, d2)| d1.partial_cmp(d2).unwrap())
            .unwrap()
            .1;
        let map2 = map
            .into_iter()
            .filter(|(_, d)| *d >= max)
            .collect::<HashMap<_, _>>();
        map2
    };

    let rust_ret = random_nums.mode();

    assert!(r_ret.contains_key(&format!("{}", rust_ret as usize)));
}

pub fn range_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "range({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let rust_ret = random_nums.range();

    assert_relative_eq!(r_ret[0], rust_ret.0, max_relative = 0.0001);
    assert_relative_eq!(r_ret[1], rust_ret.1, max_relative = 0.0001);
}

pub fn rank_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "rank({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let rust_ret = random_nums.rank();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn scale_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "scale({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rust_ret = random_nums.clone();
    rust_ret.center();
    rust_ret.scale();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn scale_by_inner(seed: u16, scale: f64) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "scale({}, scale = {})",
            RString::from_f64_slice(&random_nums),
            RString::from_f64(scale),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rust_ret = random_nums.clone();
    rust_ret.center();
    rust_ret.scale_by(scale);

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn scale_robust_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "scale({0}, scale = IQR({0}))",
            RString::from_f64_slice(&random_nums),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rust_ret = random_nums.clone();
    rust_ret.center();
    rust_ret.scale_robust();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn center_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "scale({})",
            RString::from_f64_slice(&random_nums),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rust_ret = random_nums.clone();
    rust_ret.center();
    rust_ret.scale();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn center_by_inner(seed: u16, center: f64) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "scale({}, center = {})",
            RString::from_f64_slice(&random_nums),
            RString::from_f64(center),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rust_ret = random_nums.clone();
    rust_ret.center_by(center);
    rust_ret.scale();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn center_robust_inner(seed: u16) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "scale({0}, center = median({0}))",
            RString::from_f64_slice(&random_nums),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rust_ret = random_nums.clone();
    rust_ret.center_robust();
    rust_ret.scale();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn quantile_inner(seed: u16, qtype: QuantileType) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "quantile({}, type = {})",
            RString::from_f64_slice(&random_nums),
            RString::from_f64(match qtype {
                QuantileType::InverseEmpiricalDistribution => 1,
                QuantileType::InverseEmpiricalDistributionWithAveraging => 2,
                QuantileType::NearestEvenOrder => 3,
                QuantileType::LinearInterpolation => 4,
                QuantileType::PiecewiseLinear => 5,
                QuantileType::Minitab => 6,
                QuantileType::S => 7,
                QuantileType::MedianUnbiased => 8,
                QuantileType::UnbiasedNormal => 9,
            }),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rng = WichmannHill::new();
    rng.set_seed(seed);

    let rust_ret = random_nums.quantile(&[0.0, 0.25, 0.5, 0.75, 1.0], qtype);

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn random_jitter_inner(seed: u16, factor: f64, amount: Option<f64>) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(if let Some(amount) = amount {
            format!(
                "jitter({}, factor = {}, amount = {})",
                RString::from_f64_slice(&random_nums),
                RString::from_f64(factor),
                RString::from_f64(amount),
            )
        } else {
            format!(
                "jitter({}, factor = {})",
                RString::from_f64_slice(&random_nums),
                RString::from_f64(factor),
            )
        }))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rng = WichmannHill::new();
    rng.set_seed(seed);

    let mut rust_ret = random_nums.clone();
    if let Some(amount) = amount {
        rust_ret.jitter(Some(factor), Some(amount.into()), &mut rng);
    } else {
        rust_ret.jitter(Some(factor), None, &mut rng);
    }

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}

pub fn set_jitter_inner(x: &[f64], seed: u16, factor: f64, amount: Option<f64>) {
    let seed = seed as u32;

    let r_ret = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(if let Some(amount) = amount {
            format!(
                "jitter({}, factor = {}, amount = {})",
                RString::from_f64_slice(x),
                RString::from_f64(factor),
                RString::from_f64(amount),
            )
        } else {
            format!(
                "jitter({}, factor = {})",
                RString::from_f64_slice(x),
                RString::from_f64(factor),
            )
        }))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut rng = WichmannHill::new();
    rng.set_seed(seed);

    let mut rust_ret = x.to_vec();
    if let Some(amount) = amount {
        rust_ret.jitter(Some(factor), Some(amount.into()), &mut rng);
    } else {
        rust_ret.jitter(Some(factor), None, &mut rng);
    }

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        assert_relative_eq!(r, rust, max_relative = 0.0001);
    }
}
