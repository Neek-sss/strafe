mod constraint;

pub use self::constraint::*;

pub type Finite64 = Finite<f64>;

pub type Real64 = NonNan<f64>;

pub type Positive64 = GreaterThanEqualZero<Real64>;

pub type PositiveInteger64 = GreaterThanEqualZero<Integer<Real64>>;

pub type Natural64 = GreaterThanZero<Integer<Real64>>;

pub type Rational64 = GreaterThanZero<Real64>;

pub type Probability64 = GreaterThanEqualZero<LessThanEqualOne<Real64>>;

pub type LogProbability64 = LessThanEqualZero<Real64>;

pub type Alpha64 = GreaterThanEqualZero<LessThanOne<Real64>>;
