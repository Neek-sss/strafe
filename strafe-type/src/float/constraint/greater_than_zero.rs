use std::fmt::{Debug, Display, Formatter};

use num_traits::{FromPrimitive, ToPrimitive};

use crate::float::constraint::float_constraint::FloatConstraint;

#[derive(Debug, Copy, Clone)]
pub struct GreaterThanZero<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>(
    pub(crate) T,
);

impl<T> Display for GreaterThanZero<T>
where
    T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint + Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> FloatConstraint
    for GreaterThanZero<T>
{
    #[inline]
    fn get_f64(&self) -> f64 {
        self.0.get_f64()
    }

    #[inline]
    fn constraint(&self) -> bool {
        self.0.constraint() && self.0.get_f64() > 0.0
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<isize>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: isize) -> Self {
        Self::from_isize(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&isize>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &isize) -> Self {
        Self::from_isize(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut isize>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut isize) -> Self {
        Self::from_isize(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for isize
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_isize().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<i8>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: i8) -> Self {
        Self::from_i8(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&i8>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &i8) -> Self {
        Self::from_i8(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut i8>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut i8) -> Self {
        Self::from_i8(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for i8
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_i8().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<i16>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: i16) -> Self {
        Self::from_i16(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&i16>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &i16) -> Self {
        Self::from_i16(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut i16>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut i16) -> Self {
        Self::from_i16(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for i16
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_i16().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<i32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: i32) -> Self {
        Self::from_i32(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&i32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &i32) -> Self {
        Self::from_i32(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut i32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut i32) -> Self {
        Self::from_i32(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for i32
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_i32().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<i64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: i64) -> Self {
        Self::from_i64(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&i64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &i64) -> Self {
        Self::from_i64(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut i64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut i64) -> Self {
        Self::from_i64(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for i64
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_i64().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<i128>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: i128) -> Self {
        Self::from_i128(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&i128>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &i128) -> Self {
        Self::from_i128(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut i128>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut i128) -> Self {
        Self::from_i128(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for i128
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_i128().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<usize>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: usize) -> Self {
        Self::from_usize(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&usize>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &usize) -> Self {
        Self::from_usize(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut usize>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut usize) -> Self {
        Self::from_usize(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for usize
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_usize().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<u8>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: u8) -> Self {
        Self::from_u8(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&u8>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &u8) -> Self {
        Self::from_u8(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut u8>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut u8) -> Self {
        Self::from_u8(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for u8
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_u8().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<u16>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: u16) -> Self {
        Self::from_u16(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&u16>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &u16) -> Self {
        Self::from_u16(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut u16>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut u16) -> Self {
        Self::from_u16(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for u16
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_u16().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<u32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: u32) -> Self {
        Self::from_u32(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&u32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &u32) -> Self {
        Self::from_u32(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut u32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut u32) -> Self {
        Self::from_u32(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for u32
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_u32().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<u64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: u64) -> Self {
        Self::from_u64(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&u64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &u64) -> Self {
        Self::from_u64(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut u64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut u64) -> Self {
        Self::from_u64(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for u64
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_u64().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<u128>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: u128) -> Self {
        Self::from_u128(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&u128>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &u128) -> Self {
        Self::from_u128(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut u128>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut u128) -> Self {
        Self::from_u128(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for u128
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_u128().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<f32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: f32) -> Self {
        Self::from_f32(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&f32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &f32) -> Self {
        Self::from_f32(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut f32>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut f32) -> Self {
        Self::from_f32(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for f32
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_f32().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<f64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: f64) -> Self {
        Self::from_f64(n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&f64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &f64) -> Self {
        Self::from_f64(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> From<&mut f64>
    for GreaterThanZero<T>
{
    #[inline]
    fn from(n: &mut f64) -> Self {
        Self::from_f64(*n).unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint>
    From<GreaterThanZero<T>> for f64
{
    #[inline]
    fn from(n: GreaterThanZero<T>) -> Self {
        n.to_f64().unwrap()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> ToPrimitive
    for GreaterThanZero<T>
{
    #[inline]
    fn to_isize(&self) -> Option<isize> {
        self.0.to_isize()
    }

    #[inline]
    fn to_i8(&self) -> Option<i8> {
        self.0.to_i8()
    }

    #[inline]
    fn to_i16(&self) -> Option<i16> {
        self.0.to_i16()
    }

    #[inline]
    fn to_i32(&self) -> Option<i32> {
        self.0.to_i32()
    }

    #[inline]
    fn to_i64(&self) -> Option<i64> {
        self.0.to_i64()
    }

    #[inline]
    fn to_i128(&self) -> Option<i128> {
        self.0.to_i128()
    }

    #[inline]
    fn to_usize(&self) -> Option<usize> {
        self.0.to_usize()
    }

    #[inline]
    fn to_u8(&self) -> Option<u8> {
        self.0.to_u8()
    }

    #[inline]
    fn to_u16(&self) -> Option<u16> {
        self.0.to_u16()
    }

    #[inline]
    fn to_u32(&self) -> Option<u32> {
        self.0.to_u32()
    }

    #[inline]
    fn to_u64(&self) -> Option<u64> {
        self.0.to_u64()
    }

    #[inline]
    fn to_u128(&self) -> Option<u128> {
        self.0.to_u128()
    }

    #[inline]
    fn to_f32(&self) -> Option<f32> {
        self.0.to_f32()
    }

    #[inline]
    fn to_f64(&self) -> Option<f64> {
        self.0.to_f64()
    }
}

impl<T: Debug + Copy + Clone + ToPrimitive + FromPrimitive + FloatConstraint> FromPrimitive
    for GreaterThanZero<T>
{
    #[inline]
    fn from_isize(n: isize) -> Option<Self> {
        let ret = T::from_isize(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_i8(n: i8) -> Option<Self> {
        let ret = T::from_i8(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_i16(n: i16) -> Option<Self> {
        let ret = T::from_i16(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_i32(n: i32) -> Option<Self> {
        let ret = T::from_i32(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_i64(n: i64) -> Option<Self> {
        let ret = T::from_i64(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_i128(n: i128) -> Option<Self> {
        let ret = T::from_i128(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_usize(n: usize) -> Option<Self> {
        let ret = T::from_usize(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_u8(n: u8) -> Option<Self> {
        let ret = T::from_u8(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_u16(n: u16) -> Option<Self> {
        let ret = T::from_u16(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_u32(n: u32) -> Option<Self> {
        let ret = T::from_u32(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_u64(n: u64) -> Option<Self> {
        let ret = T::from_u64(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_u128(n: u128) -> Option<Self> {
        let ret = T::from_u128(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_f32(n: f32) -> Option<Self> {
        let ret = T::from_f32(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }

    #[inline]
    fn from_f64(n: f64) -> Option<Self> {
        let ret = T::from_f64(n).map(|t| Self(t));
        #[cfg(debug_assertions)]
        if !ret.clone().unwrap().constraint() {
            panic!("Greater than Zero failed: {:?}", ret);
        }
        ret
    }
}
