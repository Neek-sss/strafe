use crate::float::constraint::float_constraint::FloatConstraint;

impl FloatConstraint for f64 {
    #[inline]
    fn get_f64(&self) -> f64 {
        *self
    }

    #[inline]
    fn constraint(&self) -> bool {
        true
    }
}
