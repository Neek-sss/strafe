use std::fmt::Debug;

use num_traits::ToPrimitive;

pub trait FloatConstraint: Sized + ToPrimitive + Debug {
    fn get_f64(&self) -> f64;
    fn constraint(&self) -> bool;

    #[inline]
    fn unwrap(self) -> f64 {
        self.to_f64().unwrap()
    }
}
