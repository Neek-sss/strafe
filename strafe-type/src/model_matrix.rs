use std::fmt::{Display, Formatter, Result as FmtResult};

use nalgebra::DMatrix;
use polars::{datatypes::DataType, frame::DataFrame};

use crate::display_table::DisplayTable;

#[derive(Clone, Debug)]
pub struct ModelMatrix {
    data: ModelMatrixData,
    schema: Vec<ModelMatrixSchema>,
}

#[derive(Clone, Debug)]
pub enum ModelMatrixData {
    Vec {
        name: String,
        data: Vec<f64>,
    },
    Matrix {
        names: Vec<String>,
        data: DMatrix<f64>,
    },
    DataFrame(DataFrame),
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum ModelMatrixSchema {
    Level,
    Factor,
    Value,
    RowName,
}

fn write_table_bars(
    column_widths: &[usize],
    formatter: &mut Formatter,
    left_bar: &str,
    middle_blank: &str,
    middle_bar: &str,
    right_bar: &str,
) -> FmtResult {
    write!(formatter, "{left_bar}")?;
    for (index, &len) in column_widths.iter().enumerate() {
        if len > 0 {
            for _ in 0..len + 2 {
                write!(formatter, "{middle_blank}")?;
            }
            if index != column_widths.len() - 1 {
                write!(formatter, "{middle_bar}")?;
            }
        }
    }
    writeln!(formatter, "{right_bar}")?;

    Ok(())
}

fn write_data(
    column_widths: &[usize],
    formatter: &mut Formatter,
    bar: &str,
    row_names: &[String],
    data: &Vec<Vec<String>>,
) -> FmtResult {
    for (row_index, row) in data.into_iter().enumerate() {
        for (column_index, value) in row.iter().enumerate() {
            write!(formatter, "{bar} ")?;
            let column_index = if !row_names.is_empty() && column_index == 0 {
                row_names[row_index].fmt(formatter)?;
                for _ in 0..column_widths[column_index] - row_names[row_index].len() {
                    write!(formatter, " ")?;
                }
                write!(formatter, " {bar} ")?;
                value.fmt(formatter)?;
                column_index + 1
            } else {
                value.fmt(formatter)?;
                if !row_names.is_empty() {
                    column_index + 1
                } else {
                    column_index
                }
            };
            if column_widths[column_index] > value.to_string().len() {
                for _ in 0..column_widths[column_index] - value.to_string().len() {
                    write!(formatter, " ")?;
                }
            }
            write!(formatter, " ")?;
        }
        writeln!(formatter, "{bar}")?;
    }

    Ok(())
}

fn as_factor<T: PartialOrd + PartialEq + ToString + Clone + Display>(
    name: &str,
    v: &[T],
) -> (Vec<String>, DMatrix<f64>) {
    let mut ret_names = Vec::new();
    let mut ret_matrix = DMatrix::<f64>::zeros(v.len(), 0);

    let mut unique_c = v.to_vec();
    unique_c.sort_by(|u_c1, u_c2| u_c1.partial_cmp(u_c2).unwrap());
    unique_c = unique_c.into_iter().fold(Vec::new(), |mut acc, v| {
        if let Some(a_last) = acc.last() {
            if a_last == &v {
                acc
            } else {
                acc.push(v);
                acc
            }
        } else {
            acc.push(v);
            acc
        }
    });
    unique_c.sort_by(|a, b| a.partial_cmp(b).unwrap());

    for j in 0..unique_c.len() {
        ret_matrix = ret_matrix.insert_column(j, 0.0);
        ret_names.push(format!("{} ({})", name, unique_c[j]));
    }

    for (j, c_j) in v.iter().enumerate() {
        let column_index = unique_c
            .binary_search_by(|a| a.partial_cmp(c_j).unwrap())
            .unwrap();
        ret_matrix[(j, column_index)] = 1.0;
    }

    (ret_names, ret_matrix)
}

fn as_level<T: PartialOrd + PartialEq + ToString + Clone>(v: &[T]) -> DMatrix<f64> {
    let mut ret = DMatrix::<f64>::zeros(v.len(), 1);

    let mut unique_c = v.to_vec();
    unique_c.sort_by(|u_c1, u_c2| u_c1.partial_cmp(u_c2).unwrap());
    unique_c = unique_c.into_iter().fold(Vec::new(), |mut acc, v| {
        if let Some(a_last) = acc.last() {
            if a_last == &v {
                acc
            } else {
                acc.push(v);
                acc
            }
        } else {
            acc.push(v);
            acc
        }
    });
    unique_c.sort_by(|a, b| a.partial_cmp(b).unwrap());

    for (i, c_t) in v.iter().enumerate() {
        let index = unique_c
            .binary_search_by(|u_c| u_c.partial_cmp(c_t).unwrap())
            .unwrap();
        ret[(i, 0)] = index as f64 + 1.0;
    }

    ret
}

impl ModelMatrix {
    pub fn matrix(&self) -> DMatrix<f64> {
        match &self.data {
            ModelMatrixData::Vec { data, .. } => match self.schema[0] {
                ModelMatrixSchema::RowName => DMatrix::<f64>::zeros(data.len(), 0),
                ModelMatrixSchema::Level => as_level(data),
                ModelMatrixSchema::Factor => as_factor("", data).1,
                ModelMatrixSchema::Value => DMatrix::<f64>::from_column_slice(data.len(), 1, data),
            },
            ModelMatrixData::Matrix { data, .. } => {
                let mut matrix = DMatrix::<f64>::zeros(data.nrows(), 0);

                let schemas = &self.schema;

                for i in 0..data.ncols() {
                    let column = data.column(i);
                    let schema = &schemas[i];

                    match schema {
                        ModelMatrixSchema::RowName => {}
                        ModelMatrixSchema::Level => {
                            let mut ncols = matrix.ncols();
                            matrix = matrix.insert_column(ncols, 0.0);
                            let x = column.as_slice();
                            for (j, &v) in as_level(x).iter().enumerate() {
                                ncols = matrix.ncols();
                                matrix[(j, ncols - 1)] = v;
                            }
                        }
                        ModelMatrixSchema::Factor => {
                            let x = column.as_slice();
                            let factor_matrix = as_factor("", x).1;
                            for ii in 0..factor_matrix.ncols() {
                                let mut ncols = matrix.ncols();
                                matrix = matrix.insert_column(ncols, 0.0);
                                for j in 0..factor_matrix.nrows() {
                                    ncols = matrix.ncols();
                                    matrix[(j, ncols - 1)] = factor_matrix[(j, ii)];
                                }
                            }
                        }
                        ModelMatrixSchema::Value => {
                            let mut ncols = matrix.ncols();
                            matrix = matrix.insert_column(ncols, 0.0);
                            for (j, &v) in column.iter().enumerate() {
                                ncols = matrix.ncols();
                                matrix[(j, ncols - 1)] = v;
                            }
                        }
                    }
                }

                matrix
            }
            ModelMatrixData::DataFrame(df) => {
                let mut matrix = DMatrix::<f64>::zeros(df.height(), 0);

                let data_names = df.get_column_names();
                let data_types = df.dtypes();
                let schemas = &self.schema;

                for i in 0..df.width() {
                    let column = &df[data_names[i]];
                    let data_type = &data_types[i];
                    let schema = &schemas[i];

                    match schema {
                        ModelMatrixSchema::RowName => {}
                        ModelMatrixSchema::Level => match data_type {
                            DataType::Boolean => {
                                let mut c = Vec::new();
                                column
                                    .bool()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let mat = as_level(&c);

                                let column_index_start = matrix.ncols();
                                for column_number in 0..mat.ncols() {
                                    let ncols = matrix.ncols();
                                    matrix = matrix.insert_column(ncols, 0.0);
                                    for j in 0..mat.nrows() {
                                        matrix[(j, column_number + column_index_start)] =
                                            mat[(j, column_number)];
                                    }
                                }
                            }
                            DataType::UInt8
                            | DataType::UInt16
                            | DataType::UInt32
                            | DataType::UInt64
                            | DataType::Int8
                            | DataType::Int16
                            | DataType::Int32
                            | DataType::Int64
                            | DataType::Float32
                            | DataType::Float64 => {
                                let mut c = Vec::new();
                                column
                                    .cast(&DataType::Float64)
                                    .unwrap()
                                    .f64()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let mat = as_level(&c);

                                let column_index_start = matrix.ncols();
                                for column_number in 0..mat.ncols() {
                                    let ncols = matrix.ncols();
                                    matrix = matrix.insert_column(ncols, 0.0);
                                    for j in 0..mat.nrows() {
                                        matrix[(j, column_number + column_index_start)] =
                                            mat[(j, column_number)];
                                    }
                                }
                            }
                            DataType::String => {
                                let mut c = Vec::new();
                                column
                                    .str()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let mat = as_level(&c);

                                let column_index_start = matrix.ncols();
                                for column_number in 0..mat.ncols() {
                                    let ncols = matrix.ncols();
                                    matrix = matrix.insert_column(ncols, 0.0);
                                    for j in 0..mat.nrows() {
                                        matrix[(j, column_number + column_index_start)] =
                                            mat[(j, column_number)];
                                    }
                                }
                            }
                            DataType::Binary | DataType::BinaryOffset => {}
                            DataType::Date
                            | DataType::Datetime(_, _)
                            | DataType::Duration(_)
                            | DataType::Time => {}
                            DataType::List(_) | DataType::Null | DataType::Unknown => {}
                        },
                        ModelMatrixSchema::Factor => match data_type {
                            DataType::Boolean => {
                                let mut c = Vec::new();
                                column
                                    .bool()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let (_, mat) = as_factor("", &c);

                                let column_index_start = matrix.ncols();
                                for column_number in 0..mat.ncols() {
                                    let ncols = matrix.ncols();
                                    matrix = matrix.insert_column(ncols, 0.0);
                                    for j in 0..mat.nrows() {
                                        matrix[(j, column_number + column_index_start)] =
                                            mat[(j, column_number)];
                                    }
                                }
                            }
                            DataType::UInt8
                            | DataType::UInt16
                            | DataType::UInt32
                            | DataType::UInt64
                            | DataType::Int8
                            | DataType::Int16
                            | DataType::Int32
                            | DataType::Int64
                            | DataType::Float32
                            | DataType::Float64 => {
                                let mut c = Vec::new();
                                column
                                    .cast(&DataType::Float64)
                                    .unwrap()
                                    .f64()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let (_, mat) = as_factor("", &c);

                                let column_index_start = matrix.ncols();
                                for column_number in 0..mat.ncols() {
                                    let ncols = matrix.ncols();
                                    matrix = matrix.insert_column(ncols, 0.0);
                                    for j in 0..mat.nrows() {
                                        matrix[(j, column_number + column_index_start)] =
                                            mat[(j, column_number)];
                                    }
                                }
                            }
                            DataType::String => {
                                let mut c = Vec::new();
                                column
                                    .str()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let (_, mat) = as_factor("", &c);

                                let column_index_start = matrix.ncols();
                                for column_number in 0..mat.ncols() {
                                    let ncols = matrix.ncols();
                                    matrix = matrix.insert_column(ncols, 0.0);
                                    for j in 0..mat.nrows() {
                                        matrix[(j, column_number + column_index_start)] =
                                            mat[(j, column_number)];
                                    }
                                }
                            }
                            DataType::Binary | DataType::BinaryOffset => {}
                            DataType::Date
                            | DataType::Datetime(_, _)
                            | DataType::Duration(_)
                            | DataType::Time => {}
                            DataType::List(_) | DataType::Null | DataType::Unknown => {}
                        },
                        ModelMatrixSchema::Value => {
                            let column_index = matrix.ncols();
                            matrix = matrix.insert_column(column_index, 0.0);

                            let mut c = Vec::new();
                            match data_type {
                                DataType::Boolean => {
                                    column.bool().unwrap().for_each(|c_opt| {
                                        c.push(if c_opt.unwrap() { 1.0 } else { 0.0 })
                                    });
                                }
                                DataType::UInt8
                                | DataType::UInt16
                                | DataType::UInt32
                                | DataType::UInt64
                                | DataType::Int8
                                | DataType::Int16
                                | DataType::Int32
                                | DataType::Int64
                                | DataType::Float32
                                | DataType::Float64 => {
                                    column
                                        .cast(&DataType::Float64)
                                        .unwrap()
                                        .f64()
                                        .unwrap()
                                        .for_each(|c_opt| c.push(c_opt.unwrap()));
                                }
                                DataType::String => {
                                    column.str().unwrap().for_each(|c_opt| {
                                        let c_unwrapped = c_opt.unwrap().to_string();
                                        c.push(c_unwrapped.parse::<f64>().unwrap());
                                    });
                                }
                                DataType::Binary | DataType::BinaryOffset => {}
                                DataType::Date
                                | DataType::Datetime(_, _)
                                | DataType::Duration(_)
                                | DataType::Time => {}
                                DataType::List(_) | DataType::Null | DataType::Unknown => {}
                            }

                            for (j, c_j) in c.into_iter().enumerate() {
                                matrix[(j, column_index)] = c_j;
                            }
                        }
                    }
                }

                matrix
            }
        }
    }

    pub fn set_schema_index(&mut self, index: usize, schema: ModelMatrixSchema) {
        self.schema[index] = schema;
    }

    pub fn set_schema_name(&mut self, name: &str, schema: ModelMatrixSchema) {
        match &self.data {
            ModelMatrixData::Vec { .. } => self.schema[0] = schema,
            ModelMatrixData::Matrix { .. } => {
                let index = name.replace("x", "").parse::<usize>().unwrap();
                self.schema[index] = schema;
            }
            ModelMatrixData::DataFrame(df) => {
                let name_index = df
                    .get_column_names()
                    .iter()
                    .enumerate()
                    .find(|(_, &n)| n == name)
                    .unwrap()
                    .0;
                self.schema[name_index] = schema;
            }
        }
    }

    pub fn set_name(&mut self, old_name: &str, new_name: &str) {
        match &mut self.data {
            ModelMatrixData::Vec { name, .. } => {
                *name = new_name.to_string();
            }
            ModelMatrixData::Matrix { names, .. } => {
                for name in names {
                    if name == old_name {
                        *name = new_name.to_string();
                    }
                }
            }
            ModelMatrixData::DataFrame(df) => {
                df.rename(old_name, new_name).unwrap();
            }
        }
    }

    pub fn set_name_index(&mut self, index: usize, name: &str) {
        match &mut self.data {
            ModelMatrixData::Vec { name, .. } => {
                *name = name.to_string();
            }
            ModelMatrixData::Matrix { names, .. } => {
                names[index] = name.to_string();
            }
            ModelMatrixData::DataFrame(df) => {
                let old_name = df.get_column_names()[index].to_string();
                df.rename(&old_name, name).unwrap();
            }
        }
    }

    pub fn column_names(&self) -> Vec<String> {
        match &self.data {
            ModelMatrixData::Vec { name, data } => match self.schema[0] {
                ModelMatrixSchema::RowName => Vec::new(),
                ModelMatrixSchema::Level | ModelMatrixSchema::Value => vec![name.clone()],
                ModelMatrixSchema::Factor => as_factor(name, data).0,
            },
            ModelMatrixData::Matrix {
                names: base_names,
                data,
            } => {
                let mut names = Vec::new();
                for i in 0..data.ncols() {
                    match self.schema[i] {
                        ModelMatrixSchema::RowName => {}
                        ModelMatrixSchema::Level | ModelMatrixSchema::Value => {
                            names.push(base_names[i].clone())
                        }
                        ModelMatrixSchema::Factor => {
                            for name in as_factor(&base_names[i], data.column(i).as_slice()).0 {
                                names.push(name);
                            }
                        }
                    }
                }
                names
            }
            ModelMatrixData::DataFrame(df) => {
                let mut names = Vec::new();

                let data_names = df.get_column_names();
                let data_types = df.dtypes();
                let schemas = &self.schema;

                for i in 0..df.width() {
                    let column_name = data_names[i];
                    let column = &df[column_name];
                    let data_type = &data_types[i];
                    let schema = &schemas[i];

                    match schema {
                        ModelMatrixSchema::RowName => {}
                        ModelMatrixSchema::Value | ModelMatrixSchema::Level => {
                            names.push(data_names[i].to_string())
                        }
                        ModelMatrixSchema::Factor => match data_type {
                            DataType::Boolean => {
                                let mut c = Vec::new();
                                column
                                    .bool()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let (mut new_names, _) = as_factor(column_name, &c);

                                names.append(&mut new_names);
                            }
                            DataType::UInt8
                            | DataType::UInt16
                            | DataType::UInt32
                            | DataType::UInt64
                            | DataType::Int8
                            | DataType::Int16
                            | DataType::Int32
                            | DataType::Int64
                            | DataType::Float32
                            | DataType::Float64 => {
                                let mut c = Vec::new();
                                column
                                    .cast(&DataType::Float64)
                                    .unwrap()
                                    .f64()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let (mut new_names, _) = as_factor(column_name, &c);

                                names.append(&mut new_names);
                            }
                            DataType::String => {
                                let mut c = Vec::new();
                                column
                                    .str()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                let (mut new_names, _) = as_factor(column_name, &c);

                                names.append(&mut new_names);
                            }
                            DataType::Binary | DataType::BinaryOffset => {}
                            DataType::Date
                            | DataType::Datetime(_, _)
                            | DataType::Duration(_)
                            | DataType::Time => {}
                            DataType::List(_) | DataType::Null | DataType::Unknown => {}
                        },
                    }
                }

                names
            }
        }
    }

    pub fn row_names(&self) -> Vec<String> {
        match &self.data {
            ModelMatrixData::Vec { data, .. } => {
                if self.schema[0] == ModelMatrixSchema::RowName {
                    data.iter().map(|d| d.to_string()).collect::<Vec<_>>()
                } else {
                    Vec::new()
                }
            }
            ModelMatrixData::Matrix { data, .. } => {
                let mut names = Vec::new();
                for i in 0..data.ncols() {
                    if self.schema[i] == ModelMatrixSchema::RowName {
                        names = data.column(i).iter().map(|d| d.to_string()).collect();
                        break;
                    }
                }
                names
            }
            ModelMatrixData::DataFrame(df) => {
                let mut names = Vec::new();

                let data_names = df.get_column_names();
                let data_types = df.dtypes();
                let schemas = &self.schema;

                for i in 0..df.width() {
                    let column_name = data_names[i];
                    let column = &df[column_name];
                    let data_type = &data_types[i];
                    let schema = schemas[i];

                    if schema == ModelMatrixSchema::RowName {
                        match data_type {
                            DataType::Boolean => {
                                let mut c = Vec::new();
                                column
                                    .bool()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                names = c.into_iter().map(|b| b.to_string()).collect();
                            }
                            DataType::UInt8
                            | DataType::UInt16
                            | DataType::UInt32
                            | DataType::UInt64
                            | DataType::Int8
                            | DataType::Int16
                            | DataType::Int32
                            | DataType::Int64
                            | DataType::Float32
                            | DataType::Float64 => {
                                let mut c = Vec::new();
                                column
                                    .cast(&DataType::Float64)
                                    .unwrap()
                                    .f64()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                names = c.into_iter().map(|f| f.to_string()).collect();
                            }
                            DataType::String => {
                                let mut c = Vec::new();
                                column
                                    .str()
                                    .unwrap()
                                    .for_each(|c_opt| c.push(c_opt.unwrap()));
                                names = c.into_iter().map(|s| s.to_string()).collect();
                            }
                            DataType::Binary | DataType::BinaryOffset => {}
                            DataType::Date
                            | DataType::Datetime(_, _)
                            | DataType::Duration(_)
                            | DataType::Time => {}
                            DataType::List(_) | DataType::Null | DataType::Unknown => {}
                        }
                        break;
                    }
                }

                names
            }
        }
    }
}

impl Display for ModelMatrix {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        writeln!(
            formatter,
            "({}, {})",
            self.matrix().nrows(),
            self.matrix().ncols()
        )?;

        let dp = DisplayTable::new(
            self.column_names(),
            self.row_names(),
            self.matrix()
                .row_iter()
                .map(|r| r.iter().cloned().collect())
                .collect(),
            None,
        );

        writeln!(formatter, "{dp}")
    }
}

impl From<&DataFrame> for ModelMatrix {
    fn from(value: &DataFrame) -> Self {
        Self::from(value.clone())
    }
}

impl From<DataFrame> for ModelMatrix {
    fn from(value: DataFrame) -> Self {
        Self {
            schema: value
                .dtypes()
                .into_iter()
                .enumerate()
                .filter_map(|(i, dtype)| {
                    if i == 0 && value.get_column_names()[i].is_empty() {
                        Some(ModelMatrixSchema::RowName)
                    } else {
                        match dtype {
                            DataType::Boolean => Some(ModelMatrixSchema::Factor),
                            DataType::UInt8
                            | DataType::UInt16
                            | DataType::UInt32
                            | DataType::UInt64
                            | DataType::Int8
                            | DataType::Int16
                            | DataType::Int32
                            | DataType::Int64
                            | DataType::Float32
                            | DataType::Float64 => Some(ModelMatrixSchema::Value),
                            DataType::String => Some(ModelMatrixSchema::Factor),
                            DataType::Binary | DataType::BinaryOffset => None,
                            DataType::Date
                            | DataType::Datetime(_, _)
                            | DataType::Duration(_)
                            | DataType::Time => None,
                            DataType::List(_) | DataType::Null | DataType::Unknown => None,
                        }
                    }
                })
                .collect(),
            data: ModelMatrixData::DataFrame(value),
        }
    }
}

impl From<&DMatrix<f64>> for ModelMatrix {
    fn from(value: &DMatrix<f64>) -> Self {
        Self::from(value.clone())
    }
}

impl From<DMatrix<f64>> for ModelMatrix {
    fn from(value: DMatrix<f64>) -> Self {
        Self {
            schema: (0..value.ncols())
                .map(|_| ModelMatrixSchema::Value)
                .collect(),
            data: ModelMatrixData::Matrix {
                names: (0..value.ncols()).map(|i| format!("x{i}")).collect(),
                data: value,
            },
        }
    }
}

impl<T: Clone> From<Vec<T>> for ModelMatrix
where
    f64: From<T>,
{
    fn from(value: Vec<T>) -> Self {
        Self::from(value.as_slice())
    }
}

impl<T: Clone> From<&Vec<T>> for ModelMatrix
where
    f64: From<T>,
{
    fn from(value: &Vec<T>) -> Self {
        Self::from(value.as_slice())
    }
}

impl<T: Clone> From<&[T]> for ModelMatrix
where
    f64: From<T>,
{
    fn from(value: &[T]) -> Self {
        Self {
            data: ModelMatrixData::Vec {
                name: "x".to_string(),
                data: value.iter().map(|t| f64::from(t.clone())).collect(),
            },
            schema: vec![ModelMatrixSchema::Value],
        }
    }
}
