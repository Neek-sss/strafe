use std::{
    error::Error,
    fmt::{Display, Error as FmtError, Formatter},
};

#[derive(Debug)]
pub enum DistributionError {
    BadX,
    BadQuantile,
    BadProbability,
}

impl Display for DistributionError {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError> {
        match &self {
            DistributionError::BadX => write!(fmt, "x was not a valid input"),
            DistributionError::BadQuantile => write!(fmt, "q was not a valid input"),
            DistributionError::BadProbability => write!(fmt, "p was not a valid input"),
        }
    }
}

impl Error for DistributionError {}
