use std::{
    error::Error,
    fmt::{Display, Error as FmtError, Formatter},
};

#[derive(Copy, Clone, Debug)]
pub enum FloatError {
    UnderflowError,
    PrecisionError,
    ProbabilityRangeError,
    NaturalRangeError,
    RealRangeError,
    BadNumber,
}

impl Display for FloatError {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError> {
        match self {
            FloatError::UnderflowError => write!(fmt, "Calculation underflowed"),
            FloatError::PrecisionError => write!(fmt, "Precision cannot be maintained"),
            FloatError::ProbabilityRangeError => write!(fmt, "Not a probability"),
            FloatError::NaturalRangeError => write!(fmt, "Not a natural number"),
            FloatError::RealRangeError => write!(fmt, "Not real a number"),
            FloatError::BadNumber => write!(fmt, "Number does not meet constraint"),
        }
    }
}

impl Error for FloatError {}
