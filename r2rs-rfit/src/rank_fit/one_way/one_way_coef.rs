// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::{collections::HashMap, hash::Hash};

use r2rs_nmath::{distribution::TBuilder, traits::Distribution};
use r2rs_stats::funcs::p_adjust;
use strafe_type::{Alpha64, FloatConstraint};

use crate::rank_fit::one_way::func::confintadjust;

pub struct RankOneWayCoefBuilder {
    alpha: Alpha64,
}

impl Default for RankOneWayCoefBuilder {
    fn default() -> Self {
        Self { alpha: 0.05.into() }
    }
}

impl RankOneWayCoefBuilder {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
        }
    }

    fn get_ind(kp: usize) -> (Vec<usize>, Vec<usize>) {
        let ind1 = (0..kp)
            .map(|n| vec![n; kp - n])
            .flatten()
            .collect::<Vec<_>>();

        let ind2 = (0..kp)
            .map(|p| (0..=p).rev().collect::<Vec<_>>())
            .flatten()
            .rev()
            .zip(ind1.iter())
            .map(|(i1, i2)| i1 + i2 + 1)
            .collect::<Vec<_>>();

        (ind1, ind2)
    }

    pub(crate) fn build<T: Eq + Hash + ToString>(
        &self,
        (x, deltahat, tauhat, column_names, nvec): &(&[f64], &[f64], f64, &[T], &HashMap<T, usize>),
    ) -> Result<Vec<RankOneWayCoef>, Box<dyn std::error::Error>> {
        let kp = deltahat.len() - 1;
        let (ind1, ind2) = Self::get_ind(kp);
        let mut est = deltahat.to_vec();
        for i in 0..ind1.len() {
            est.push(deltahat[ind1[i]] - deltahat[ind2[i]]);
        }

        let kp = deltahat.len();
        let (ind1, ind2) = Self::get_ind(kp);
        let se = ind1
            .iter()
            .zip(ind2.iter())
            .map(|(&ind1, &ind2)| {
                tauhat
                    * ((1.0 / nvec[&column_names[ind1]] as f64)
                        + (1.0 / nvec[&column_names[ind2]] as f64))
                        .sqrt()
            })
            .collect::<Vec<_>>();
        let cv = confintadjust(x.len(), nvec.len(), self.alpha, None)?.unwrap();

        let mut t_builder = TBuilder::new();
        t_builder.with_df(x.len() - (kp + 1))?;
        let t = t_builder.build();
        let pval = p_adjust(
            &est.iter()
                .zip(se.iter())
                .map(|(&est, &se)| t.probability((est / se).abs(), false).unwrap())
                .collect::<Vec<_>>(),
            None,
        );

        Ok((0..ind1.len())
            .map(|i| RankOneWayCoef {
                i: column_names[ind1[i]].to_string(),
                j: column_names[ind2[i]].to_string(),
                estimate: est[i],
                confidence_interval: (est[i] - cv * se[i], est[i] + cv * se[i]),
                alpha: self.alpha,
                p: pval[i],
            })
            .collect::<Vec<_>>())
    }
}

#[derive(Clone, Debug)]
pub struct RankOneWayCoef {
    pub(crate) i: String,
    pub(crate) j: String,
    pub(crate) estimate: f64,
    pub(crate) confidence_interval: (f64, f64),
    pub(crate) alpha: Alpha64,
    pub(crate) p: f64,
}
