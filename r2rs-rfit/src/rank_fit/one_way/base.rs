// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::{collections::HashMap, fmt::Debug, hash::Hash};

/// # Rank-based Oneway Analysis of Variance
///
/// ## Description:
///
/// Carries out a robust analysis of variance for a one factor design.
/// Analysis is based on the R estimates.
///
/// ## Usage:
///
/// oneway.rfit(y, g, scores = Rfit::wscores, p.adjust = "none")
///
/// ## Arguments:
///
/// * y: n by 1 response vector
/// * g: n by 1 vector representing group membership
/// * scores: an object of class 'scores'
/// * p.adjust: adjustment to the p-values, argument passed to p.adjust
///
/// ## Details:
///
/// Carries out a robust one-way analysis of variance based on full
/// model r fit.
///
/// Value:
///
/// * fit : full model fit from rfit
/// * est : Estimates
/// * se : Standard Errors
/// * I : First Index
/// * J : Second Index
/// * p.value: p-values
/// * y: response vector
/// * g: vector denoting group membership
///
/// ## Author(s):
///
/// Joseph McKean, John Kloke
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## See Also:
///
/// rfit
///
/// ## Examples:
///
/// ```r
///  data(quail)
///  oneway.rfit(quail$ldl,quail$treat)
/// ```
#[derive(Clone, Debug)]
pub struct OneWayRankFit<T: Eq + Hash + Clone + ToString> {
    pub(crate) x: Vec<f64>,
    pub(crate) tauhat: f64,
    pub(crate) deltahat: Vec<f64>,
    pub(crate) x_column_names: Vec<T>,
    pub(crate) nvec: HashMap<T, usize>,
}
