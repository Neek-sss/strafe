// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::error::Error;

use r2rs_nmath::{
    distribution::{TBuilder, TukeyBuilder},
    func::choose,
    traits::Distribution,
};
use strafe_type::{Alpha64, FloatConstraint, Real64};

pub enum ConfIntAdjustMethod {
    Bonferroni,
    Tukey,
}

pub fn confintadjust<A: Into<Alpha64>>(
    n: usize,
    k: usize,
    alpha: A,
    method: Option<ConfIntAdjustMethod>,
) -> Result<Real64, Box<dyn Error>> {
    Ok(match method {
        Some(ConfIntAdjustMethod::Bonferroni) => {
            let mut builder = TBuilder::new();
            builder.with_df(n - k)?;
            builder
                .build()
                .quantile(1.0 - alpha.into().unwrap() / choose(k, 2).unwrap(), true)
        }
        Some(ConfIntAdjustMethod::Tukey) => {
            let mut builder = TukeyBuilder::new();
            builder.with_means(k);
            builder.with_df(n - k);
            (builder
                .build()
                .quantile(1.0 - alpha.into().unwrap(), true)
                .unwrap()
                / 2.0_f64.sqrt())
            .into()
        }
        None => {
            let mut builder = TBuilder::new();
            builder.with_df(n - k)?;
            builder
                .build()
                .quantile(1.0 - alpha.into().unwrap() / 2.0, true)
        }
    })
}
