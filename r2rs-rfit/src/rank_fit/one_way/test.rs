// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::hash::Hash;

use strafe_trait::{Assumption, Conclusion, StatisticalTest};

use crate::rank_fit::one_way::{
    one_way_coef::{RankOneWayCoef, RankOneWayCoefBuilder},
    OneWayRankFit,
};

impl<T: Eq + Hash + Clone + ToString> StatisticalTest for OneWayRankFit<T> {
    type Input = ();
    type Output = Result<Vec<RankOneWayCoef>, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        vec![]
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![]
    }

    fn test(&mut self, _: &Self::Input) -> Self::Output {
        let coefs = RankOneWayCoefBuilder::new().build(&(
            &self.x,
            &self.deltahat,
            self.tauhat,
            &self.x_column_names,
            &self.nvec,
        ))?;

        Ok(coefs)
    }
}
