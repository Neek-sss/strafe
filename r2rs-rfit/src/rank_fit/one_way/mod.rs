// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

mod base;
mod builder;
pub(crate) mod func;
mod one_way_coef;
mod test;

pub use base::*;
pub use builder::*;
