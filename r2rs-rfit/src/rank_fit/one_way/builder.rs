// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::{collections::HashMap, error::Error, hash::Hash};

use nalgebra::DMatrix;
use r2rs_base::func::as_independent_factor_matrix;
use r2rs_nmath::rng::MarsagliaMulticarry;
use strafe_trait::{Model, ModelBuilder};
use strafe_type::{Alpha64, ModelMatrix};

use crate::{
    rank_fit::{one_way::OneWayRankFit, two_way::RankFitBuilder},
    scores::Scores,
};

pub struct OneWayRankFitBuilder<S: Scores, T: Eq + Hash + Clone + ToString> {
    pub(crate) x: Option<Vec<f64>>,
    pub(crate) groups: Option<Vec<T>>,
    pub(crate) alpha: Alpha64,
    pub(crate) scores: S,
}

impl<S: Scores, T: Eq + Hash + Clone + ToString> OneWayRankFitBuilder<S, T> {
    pub fn new() -> Self {
        Self {
            x: None,
            groups: None,
            alpha: 0.05.into(),
            scores: S::default(),
        }
    }

    pub fn with_x(self, x: &[f64]) -> Self {
        Self {
            x: Some(x.to_vec()),
            ..self
        }
    }

    pub fn with_groups(self, groups: &[T]) -> Self {
        Self {
            groups: Some(groups.to_vec()),
            ..self
        }
    }

    pub fn with_alpha(self, alpha: Alpha64) -> Self {
        Self { alpha, ..self }
    }

    pub fn with_scores(self, scores: &S) -> Self {
        Self {
            scores: scores.clone(),
            ..self
        }
    }

    pub fn build(&mut self) -> Result<OneWayRankFit<T>, Box<dyn Error>> {
        let mut nvec = HashMap::new();
        for g in self.groups.as_ref().unwrap() {
            *nvec.entry(g.clone()).or_insert(0) += 1;
        }
        let ug = nvec.keys().collect::<Vec<_>>();

        if ug.len() < 3 {
            panic!("requires K > 2");
        }

        let y = DMatrix::<f64>::from_row_slice(
            self.x.as_ref().unwrap().len(),
            1,
            self.x.as_ref().unwrap(),
        );

        let (x_column_names, mut x) = as_independent_factor_matrix(self.groups.as_ref().unwrap());
        x = x.remove_column(0);
        let fit = RankFitBuilder::<S, MarsagliaMulticarry>::new()
            .with_bootstrap_coefficient_ci(false)
            .with_x(&ModelMatrix::from(x))
            .with_y(&ModelMatrix::from(y))
            .build();

        let deltahat = fit
            .parameters()?
            .into_iter()
            .skip(1)
            .map(|b| b.estimate())
            .collect();

        Ok(OneWayRankFit {
            x: self.x.as_ref().unwrap().clone(),
            tauhat: fit.tauhat,
            deltahat,
            x_column_names,
            nvec,
        })
    }
}
