// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

mod anova;
mod one_way;
pub(crate) mod prediction;

pub(crate) mod two_way;

pub use self::{
    anova::raov,
    one_way::{OneWayRankFit, OneWayRankFitBuilder},
    two_way::{RankFit, RankFitBuilder, RankFitTest},
};

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;
#[cfg(test)]
mod tests;
