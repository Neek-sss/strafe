// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::error::Error;

use strafe_type::ModelMatrix;

use crate::{
    rank_fit::anova::{
        kwayr::{kwayr, RAnova},
        subsets::subsets,
    },
    scores::Scores,
};

mod cellx;
mod khmat;
mod kwayr;
mod paste_col_r_fit;
mod redmod;
mod subsets;

/// # R ANOVA
///
/// ## Description:
///
/// Returns full model fit and robust ANOVA table for all main effects
/// and interactions.
///
/// ## Usage:
///
/// raov(f, data = list(), ...)
///
/// ## Arguments:
///
/// * f: an object of class formula
/// * data: an optional data frame
/// * ...: additional arguments
///
/// ## Details:
///
/// Based on reduction in dispersion tests for testing main effects
/// and interaction. Uses an algorithm described in Hocking (1985).
///
/// ## Value:
///
/// * table: Description of 'comp1'
/// * fit: full model fit returned from rfit
/// * residuals: the residuals, i.e. y-yhat
/// * fitted.values: yhat = x betahat
/// * call: Call to the function
///
/// ## Author(s):
///
/// Joseph McKean, John Kloke
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Hocking, R. R. (1985), _The Analysis of Linear Models_, Monterey,
/// California: Brooks/Cole.
///
/// ## See Also:
///
/// ‘rfit’, ‘oneway.rfit’
///
/// ## Examples:
///
/// ```r
/// raov(logSurv~Poison+Treatment,data=BoxCox)
/// ```
pub fn raov<S: Scores>(
    x: &ModelMatrix,
    names: &[String],
    y: &ModelMatrix,
) -> Result<Vec<RAnova>, Box<dyn Error>> {
    let mut fit = kwayr::<S>(x, y)?;

    let ind = subsets(names.len());
    let row_names = ind
        .row_iter()
        .map(|row| {
            row.iter()
                .enumerate()
                .filter_map(|(i, &choose)| if choose { Some(names[i].clone()) } else { None })
                .fold(String::new(), |acc, name| {
                    if acc.is_empty() {
                        name
                    } else {
                        format!("{acc}:{name}")
                    }
                })
        })
        .collect::<Vec<_>>();

    for i in 0..row_names.len() {
        fit[i].label = row_names[i].clone();
    }

    Ok(fit)
}
