// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

pub fn paste_cols_r_fit(x: &DMatrix<f64>) -> Vec<String> {
    x.row_iter()
        .map(|row| {
            row.iter().fold(String::new(), |accumulator, r_i| {
                format!("{accumulator}{r_i}")
            })
        })
        .collect::<Vec<_>>()
}
