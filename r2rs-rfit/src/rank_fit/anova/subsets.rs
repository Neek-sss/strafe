// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::{DMatrix, RowDVector};
use r2rs_base::func::order;

pub fn subsets(n: usize) -> DMatrix<bool> {
    let mut subsets = DMatrix::<usize>::zeros(2.0_f64.powi(n as i32) as usize, n);
    for i in 0..subsets.ncols() {
        let mut skip = true;
        for j_slice in (0..subsets.nrows())
            .collect::<Vec<_>>()
            .chunks(2.0_f64.powi(i as i32) as usize)
        {
            if skip {
                skip = false;
            } else {
                for &j in j_slice {
                    subsets[(j, i)] = 1;
                }
                skip = true;
            }
        }
    }
    let ad = order(
        &subsets
            .row_iter()
            .map(|row| row.iter().sum::<usize>())
            .collect::<Vec<_>>(),
    );
    DMatrix::<bool>::from_rows(
        &ad.into_iter()
            .skip(1)
            .map(|i| {
                RowDVector::<bool>::from_iterator(
                    subsets.ncols(),
                    subsets.row(i).iter().map(|&s| s == 1),
                )
            })
            .collect::<Vec<_>>(),
    )
}
