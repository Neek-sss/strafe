// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use r2rs_base::func::unique;

fn avea(a: usize) -> DMatrix<f64> {
    DMatrix::<f64>::repeat(1, a, 1.0 / a as f64)
}

fn deltaa(a: usize) -> DMatrix<f64> {
    let am1 = a - 1;
    let r = DMatrix::<f64>::from_diagonal_element(am1, am1, 1.0);
    r.insert_column(a - 1, -1.0)
}

pub fn khmat(levsind: &DMatrix<f64>, permh: &[bool]) -> DMatrix<f64> {
    let k = levsind.ncols();
    let mut numba = unique(&levsind.column(0).iter().cloned().collect::<Vec<_>>()).len();
    let mut khmat = if permh[0] { deltaa(numba) } else { avea(numba) };
    for i in 1..k {
        numba = unique(&levsind.column(i).iter().cloned().collect::<Vec<_>>()).len();
        if permh[i] {
            khmat = khmat.kronecker(&deltaa(numba))
        } else {
            khmat = khmat.kronecker(&avea(numba))
        }
    }
    khmat
}
