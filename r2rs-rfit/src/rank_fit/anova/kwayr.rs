// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::error::Error;

use nalgebra::DMatrix;
use r2rs_nmath::{distribution::FBuilder, rng::MarsagliaMulticarry, traits::Distribution};
use strafe_trait::{Model, ModelBuilder};
use strafe_type::{FloatConstraint, ModelMatrix};

use crate::{
    funcs::dispersion,
    rank_fit::{
        anova::{cellx::cellx, khmat::khmat, redmod::redmod, subsets::subsets},
        two_way::RankFitBuilder,
    },
    scores::Scores,
};

#[derive(Clone, Debug)]
pub struct RAnova {
    pub label: String,
    pub degrees_of_freedom: usize,
    pub residual_deviation: f64,
    pub mean_residual_deviation: f64,
    pub f: f64,
    pub p: f64,
}

pub fn kwayr<S: Scores>(x: &ModelMatrix, y: &ModelMatrix) -> Result<Vec<RAnova>, Box<dyn Error>> {
    let nf = x.matrix().ncols();
    let levsind = x.matrix();
    let n = y.matrix().len();
    let (_, xfull) = cellx(&x.matrix());
    let listtests = subsets(nf);
    let p = xfull.ncols();
    let xuse = xfull.clone();

    let fit_f = RankFitBuilder::<S, MarsagliaMulticarry>::new()
        .with_intercept(false)
        .with_bootstrap_coefficient_ci(false)
        .with_x(&ModelMatrix::from(&xfull))
        .with_y(&y)
        .build();

    let pars = fit_f
        .parameters()?
        .into_iter()
        .map(|p| p.estimate())
        .collect::<Vec<_>>();
    let drf = dispersion(
        &DMatrix::<f64>::from_iterator(pars.len(), 1, pars.into_iter()),
        &xuse,
        &y.matrix(),
        &fit_f.scores,
    );
    let dfull = n - p;
    let nt = listtests.ncols();
    let mut ret = Vec::new();
    for i in 0..=nt {
        let permh = listtests.row(i).iter().cloned().collect::<Vec<_>>();
        let hmat = khmat(&levsind, &permh);

        let q = hmat.column(1).len();
        let xred = redmod(&xfull, &hmat);

        let fitr = RankFitBuilder::<S, MarsagliaMulticarry>::new()
            .with_intercept(false)
            .with_bootstrap_coefficient_ci(false)
            .with_x(&ModelMatrix::from(&xred))
            .with_y(&y)
            .build();

        let pars = fitr
            .parameters()?
            .into_iter()
            .map(|p| p.estimate())
            .collect::<Vec<_>>();
        let drr = dispersion(
            &DMatrix::<f64>::from_iterator(pars.len(), 1, pars.into_iter()),
            &xred,
            &y.matrix(),
            &fitr.scores,
        );
        let rd = drr - drf;
        let ft = (rd / q as f64) / (fit_f.tauhat / 2.0);

        let mut f_builder = FBuilder::new();
        f_builder.with_df1(q);
        f_builder.with_df2(dfull);
        let f = f_builder.build();
        let pv = 1.0 - f.probability(ft, true).unwrap();

        ret.push(RAnova {
            label: "".to_string(),
            degrees_of_freedom: q,
            residual_deviation: rd,
            mean_residual_deviation: rd / q as f64,
            f: ft,
            p: pv,
        });
    }

    Ok(ret)
}
