// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use r2rs_base::func::unique;

use crate::rank_fit::anova::paste_col_r_fit::paste_cols_r_fit;

pub fn cellx(x: &DMatrix<f64>) -> (Vec<String>, DMatrix<f64>) {
    let x = paste_cols_r_fit(x);
    let column_names = unique(&x);
    let mut ret = DMatrix::<f64>::zeros(x.len(), column_names.len());
    for i in 0..x.len() {
        ret[(
            i,
            column_names
                .iter()
                .enumerate()
                .find(|(_, name)| &x[i] == *name)
                .unwrap()
                .0,
        )] = 1.0;
    }
    (column_names, ret)
}
