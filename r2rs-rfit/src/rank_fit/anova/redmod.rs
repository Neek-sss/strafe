// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

pub fn redmod(xmat: &DMatrix<f64>, amat: &DMatrix<f64>) -> DMatrix<f64> {
    let q = amat.nrows();
    let p = xmat.ncols();
    let temp = amat.transpose().qr();
    if temp.q().ncols() != q {
        panic!("redmod:  The hypothesis matrix is not full row rank.")
    } else {
        let mut zed = xmat.transpose();
        temp.q_tr_mul(&mut zed);
        let redmod = DMatrix::<f64>::from_row_iterator(
            p - q,
            zed.ncols(),
            zed.row_iter()
                .skip(q)
                .take(p - q)
                .map(|r| r.iter().cloned().collect::<Vec<_>>())
                .flatten(),
        );
        redmod.transpose()
    }
}
