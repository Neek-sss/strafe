// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_trait::{Assumption, Conclusion, StatisticalTest};

use crate::{
    rank_fit::two_way::base::RankFit,
    scores::Scores,
    tests::{
        DropInDispersionStatistic, DropInDispersionTest, RankTCoefficient, RankTCoefficientBuilder,
        WaldOverallStatistic, WaldOverallTest,
    },
};

#[derive(Clone, Debug)]
pub struct RankFitTest {
    pub coefs: Vec<RankTCoefficient>,
    pub wald: WaldOverallStatistic,
    pub drop: DropInDispersionStatistic,
}

impl<S: Scores> StatisticalTest for RankFit<S> {
    type Input = ();
    type Output = Result<RankFitTest, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        vec![]
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![]
    }

    fn test(&mut self, _: &Self::Input) -> Self::Output {
        // Significance of Coefs
        let mut coefs = RankTCoefficientBuilder::new()
            .with_alpha(self.alpha)
            .test(&(
                self.x1.clone(),
                self.b.clone(),
                self.get_tauhat(),
                self.get_taushat(),
            ))?;
        if !self.bootstrap_b_ci.is_empty() {
            for (coef, ci) in coefs.iter_mut().zip(self.bootstrap_b_ci.iter()) {
                coef.confidence_interval = ci.clone();
            }
        }

        // Significance of Regression
        let wald = WaldOverallTest::new().with_alpha(self.alpha).test(&(
            self.x1.clone(),
            self.b.clone(),
            self.get_tauhat(),
            self.get_taushat(),
        ))?;

        // Drop in Dispersion Test
        let drop = DropInDispersionTest::new()
            .with_alpha(self.alpha)
            .with_fit1_scores(&self.scores)
            .test(&(
                self.x1.clone(),
                self.y.matrix(),
                self.b.clone(),
                self.get_tauhat(),
                None,
            ))?;

        Ok(RankFitTest { coefs, wald, drop })
    }
}
