// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::error::Error;

use nalgebra::DMatrix;
use r2rs_stats::{
    funcs::hat,
    traits::{DeviationType, StatArray},
};
use strafe_trait::{
    Manipulator, Model, Statistic, StatisticalEstimate, StatisticalEstimator, StatisticalTest,
};
use strafe_type::ModelMatrix;

use crate::{
    funcs::dispersion,
    rank_fit::two_way::{func::rfit_vcov, RankFit},
    scores::Scores,
    tests::{LenientRobustRSquaredTest, RankLinearEstimator, RankTCoefficientBuilder},
};

impl<S: Scores> Model for RankFit<S> {
    fn get_x(&self) -> ModelMatrix {
        self.x.clone()
    }

    fn get_x1(&self) -> ModelMatrix {
        self.x1.clone()
    }

    fn get_y(&self) -> ModelMatrix {
        self.y.clone()
    }

    fn get_weights(&self) -> ModelMatrix {
        ModelMatrix::from(vec![1.0; self.y.matrix().len()])
    }

    fn get_intercept(&self) -> bool {
        self.intercept
    }

    fn manipulator(
        &self,
    ) -> Result<
        Box<dyn StatisticalEstimate<Manipulator<ModelMatrix, ModelMatrix>, ModelMatrix>>,
        Box<dyn Error>,
    > {
        Ok(Box::new(
            RankLinearEstimator::new()
                .with_alpha(self.alpha)
                .estimate(&(
                    self.x1.clone(),
                    self.b.clone(),
                    self.residuals.clone(),
                    self.get_tauhat(),
                    self.get_taushat(),
                ))?,
        ))
    }

    fn determination(&self) -> Result<Box<dyn Statistic>, Box<dyn Error>> {
        Ok(Box::new(
            LenientRobustRSquaredTest::new()
                .with_alpha(self.alpha)
                .with_scores(&self.scores)
                .test(&(
                    self.x1.clone(),
                    self.y.matrix(),
                    self.b.clone(),
                    self.get_tauhat(),
                ))?,
        ))
    }

    fn parameters(
        &self,
    ) -> Result<Vec<Box<dyn StatisticalEstimate<f64, (f64, f64)>>>, Box<dyn Error>> {
        Ok(RankTCoefficientBuilder::new()
            .with_alpha(self.alpha)
            .estimate(&(
                self.x1.clone(),
                self.b.clone(),
                self.get_tauhat(),
                self.get_taushat(),
            ))?
            .into_iter()
            .enumerate()
            .map(|(i, mut c)| {
                if !self.bootstrap_b_ci.is_empty() {
                    c.confidence_interval = self.bootstrap_b_ci[i];
                }
                let x: Box<dyn StatisticalEstimate<f64, (f64, f64)>> = Box::new(c);
                x
            })
            .collect())
    }

    fn predictions(
        &self,
    ) -> Result<Box<dyn StatisticalEstimate<ModelMatrix, ModelMatrix>>, Box<dyn Error>> {
        Ok(Box::new(
            RankLinearEstimator::new()
                .with_alpha(self.alpha)
                .estimate(&(
                    self.x1.clone(),
                    self.b.clone(),
                    self.residuals.clone(),
                    self.get_tauhat(),
                    self.get_taushat(),
                ))?,
        ))
    }

    fn residuals(&self) -> Result<ModelMatrix, Box<dyn Error>> {
        Ok(ModelMatrix::from(self.residuals.clone()))
    }

    fn studentized_residuals(&self) -> Result<ModelMatrix, Box<dyn Error>> {
        let mut ehat = self.residuals()?.matrix();
        let n = ehat.nrows();
        let p = self.x.matrix().ncols();

        let sigmahat = ehat.as_slice().mad(DeviationType::Median(None));
        let deltas = ehat.as_slice().iter().map(|e| e.abs()).sum::<f64>() / (n as f64 - p as f64);
        let delta = dispersion(&self.b, &self.x1.matrix(), &self.y.matrix(), &self.scores)
            / (n as f64 - p as f64);

        let k2 = (self.tauhat / sigmahat).powi(2) * (2.0 * delta / self.tauhat - 1.0);

        let s = if self.symmetric {
            let h = hat(&self.x.matrix(), true);
            h.into_iter()
                .map(|h| {
                    let temp = 1.0 - k2 * h;
                    sigmahat
                        * if temp.is_sign_negative() {
                            1.0 - h
                        } else {
                            temp
                        }
                        .sqrt()
                })
                .collect::<Vec<_>>()
        } else {
            let hc = hat(
                &self
                    .x
                    .matrix()
                    .insert_column(0, 1.0)
                    .qr()
                    .q()
                    .remove_column(0),
                false,
            );
            let k1 = (self.taushat / sigmahat).powi(2) * (2.0 * deltas / self.taushat - 1.0);

            hc.into_iter()
                .map(|hc| {
                    let temp = 1.0 - k1 / n as f64 - k2 * hc;
                    sigmahat
                        * if temp.is_sign_negative() {
                            1.0 - hc
                        } else {
                            temp
                        }
                        .sqrt()
                })
                .collect::<Vec<_>>()
        };

        ehat.iter_mut()
            .zip(s.into_iter())
            .for_each(|(ehat, s)| *ehat /= s);

        Ok(ehat.into())
    }

    fn standardized_residuals(&self) -> Result<ModelMatrix, Box<dyn Error>> {
        // TODO: I know this is not the same as standardized, but there isn't and standardized
        //  residuals implemented in Rfit and studentized will serve a similar function in plots
        self.studentized_residuals()
    }

    fn variance(&self) -> Result<DMatrix<f64>, Box<dyn Error>> {
        let tauhat = self.get_tauhat();
        let taushat = self.get_taushat();
        Ok(rfit_vcov(&self.x1.matrix(), tauhat, taushat))
    }
}
