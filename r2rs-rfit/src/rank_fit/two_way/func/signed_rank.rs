// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use r2rs_base::traits::StatisticalSlice;

use crate::rank_fit::two_way::func::walsh::walsh;

/// Signed-Rank Estimate of Location (Intercept)
///
/// ## Description:
///
/// Returns the signed-rank estimate of intercept with is equivalent
/// to the Hodges-Lehmann estimate of the residuals.
///
/// ## Usage:
///
/// signedrank(x)
///
/// ## Arguments:
///
/// * x: numeric vector
///
/// ## Value:
///
/// Returns the median of the Walsh averages.
///
/// ## Author(s):
///
/// John Kloke, Joseph McKean
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Hollander, M. and Wolfe, D.A. (1999), _Nonparametric Statistical
/// Methods_, New York: Wiley.
///
/// ## See Also:
///
/// ‘walsh’
///
/// ## Examples:
///
/// ```r
/// ## The function is currently defined as
/// function (x)
/// median(walsh(x))
/// ```
pub fn signed_rank(x: &[f64]) -> f64 {
    walsh(x).median()
}
