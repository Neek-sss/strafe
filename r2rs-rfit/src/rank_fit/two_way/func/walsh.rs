// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

/// Walsh Averages
///
/// ## Description:
///
/// Given a list of n numbers, the Walsh averages are the n(n+1)/2
/// pairwise averages.
///
/// ## Usage:
///
/// walsh(x)
///
/// ## Arguments:
///
///  x: A numeric vector
///
/// ## Value:
///
/// The Walsh averages.
///
/// ## Author(s):
///
/// John Kloke, Joseph McKean
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Hollander, M. and Wolfe, D.A. (1999), _Nonparametric Statistical
/// Methods_, New York: Wiley.
///
/// ## See Also:
///
///‘signedrank’
///
/// ## Examples:
///
/// ```r
/// median(walsh(rnorm(100)))  # Hodges-Lehmann estimate of location
///
/// ## The function is currently defined as
/// function (x)
/// {
///     n <- length(x)
///     w <- vector(n * (n + 1)/2, mode = "numeric")
///     ind <- 0
///     for (i in 1:n) {
///         for (j in i:n) {
///             ind <- ind + 1
///             w[ind] <- 0.5 * (x[i] + x[j])
///         }
///     }
///     return(w)
/// }
/// ```
pub fn walsh(x: &[f64]) -> Vec<f64> {
    let n = x.len();
    let mut w = vec![0.0; n * (n + 1) / 2];
    let mut ind = 0;
    for i in 0..n {
        for j in i..n {
            w[ind] = 0.5 * (x[i] + x[j]);
            ind += 1;
        }
    }
    w
}
