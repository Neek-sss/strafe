// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

pub fn rfit_vcov(x: &DMatrix<f64>, tauhat: f64, taushat: f64) -> DMatrix<f64> {
    let q = -x.clone().qr().q();
    let q1 = q.column(0);
    let q2 = q.clone().remove_column(0);

    let xxpxi = x * (x.transpose() * x).pseudo_inverse(f64::EPSILON).unwrap();
    let a1 = q1.transpose() * xxpxi.clone();
    let a2 = q2.transpose() * xxpxi;
    taushat.powi(2) * (a1.transpose() * a1) + tauhat.powi(2) * (a2.transpose() * a2)
}
