// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::ops::Mul;

use nalgebra::DMatrix;
use num_traits::Float;
use r2rs_stats::funcs::{
    opt::{Optim, BFGS},
    sd,
};

use crate::scores::{Scores, WilcoxScores};

fn jaeckel_grad(
    x: &DMatrix<f64>,
    y: &DMatrix<f64>,
    beta: &DMatrix<f64>,
    scrs_mat: &DMatrix<f64>,
) -> Vec<f64> {
    let e = y - x * beta;

    let mut intermediate = x.row_iter().zip(e.into_iter()).collect::<Vec<_>>();
    intermediate.sort_by(|(_, e1), (_, e2)| e1.total_cmp(e2));
    let ordered_x =
        DMatrix::from_rows(&intermediate.into_iter().map(|(r, _)| r).collect::<Vec<_>>());

    let scrs = scrs_mat.mul(-1.0);
    (ordered_x.transpose() * scrs).as_slice().to_vec()
}

fn jaeckel_disp(
    x: &DMatrix<f64>,
    y: &DMatrix<f64>,
    beta: &DMatrix<f64>,
    scrs_mat: &DMatrix<f64>,
) -> f64 {
    let mut e = (y - x * beta).as_slice().to_vec();
    e.sort_by(|f1, f2| f1.partial_cmp(f2).unwrap());
    let e_mat = DMatrix::from_vec(e.len(), 1, e);
    (e_mat.transpose() * scrs_mat)[(0, 0)]
}

/// Function to Minimize Jaeckel's Dispersion Function
///
/// ## Description:
///
/// Uses the built-in function ‘optim’ to minimize Jaeckel's
/// dispersion function.
///
/// ## Usage:
///
/// ```r
/// jaeckel(x, y, beta0 = lm(y ~ x)$coef[2:(ncol(x) + 1)],
///  scores = Rfit::wscores, control = NULL,...)
/// ```
///
/// ## Arguments:
///
/// * x: n by p design matrix
/// * y: n by 1 response vector
/// * beta0: intial estimate
/// * scores: object of class 'scores'
/// * control: control passed to fitting routine
/// * ...: addtional arguments to be passed to fitting routine
///
/// ## Details:
///
/// Function uses ‘optim’ with method set to BFGS to minimize
/// Jaeckel's dispersion function. If control is not specified at the
/// function call, the relative tolerance (reltol) is set to
/// .Machine$double.eps^(3/4) maximum number of iterations is set to
/// 200. See ‘optim’.
///
/// ## Value:
///
/// Results of ‘optim’ are returned.
///
/// ## Author(s):
///
/// John Kloke
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Jaeckel, L. A. (1972), Estimating regression coefficients by
/// minimizing the dispersion of residuals. _Annals of Mathematical
/// Statistics_, 43, 1449 - 1458.
///
/// Kapenga, J. A., McKean, J. W., and Vidmar, T. J. (1988), _RGLM:
/// Users Manual_, Statist. Assoc. Short Course on Robust Statistical
/// Procedures for the Analysis of Linear and Nonlinear Models, New
/// Orleans.
///
/// ## See Also:
///
/// ‘optim’, ‘rfit’
///
/// ## Examples:
///
/// ```r
/// ##  This is a internal function.  See rfit for user-level examples.
/// ```
pub fn jaeckel(x: &DMatrix<f64>, y: &DMatrix<f64>, beta0: &DMatrix<f64>) -> Vec<f64> {
    let maxit = 200;
    let reltol = f64::epsilon().powf(0.75);

    let scores = WilcoxScores::new();
    let y_len = y.shape().0;
    let scrs = scores.get_scores(
        &(0..y.shape().0)
            .map(|i| i as f64 / (y_len as f64 + 1.0))
            .collect::<Vec<_>>(),
    );
    let scrs_mat_1 = DMatrix::from_vec(scrs.len(), 1, scrs);
    let scrs_mat_2 = scrs_mat_1.clone();
    let scrs_mat_3 = scrs_mat_1.clone();
    let scrs_mat_4 = scrs_mat_1.clone();

    let y_1 = y.clone();

    let y_sd = sd(y.as_slice());
    let y_star = y.iter().map(|y| y / y_sd).collect::<Vec<_>>();

    let y_star_temp_1 = DMatrix::from_vec(y_star.len(), 1, y_star);
    let y_star_temp_2 = y_star_temp_1.clone();

    let x_1 = x.clone();
    let x_2 = x.clone();
    let x_3 = x.clone();
    let x_4 = x.clone();

    let mut bfgs = BFGS {
        grad_func: Some(Box::new(move |beta: Vec<f64>| -> Vec<f64> {
            jaeckel_grad(
                &x_1,
                &y_star_temp_1.clone(),
                &DMatrix::from_vec(beta.len(), 1, beta.to_vec()),
                &scrs_mat_1.clone(),
            )
        })),
        ..Default::default()
    };
    bfgs.optim_options.maxit = maxit;
    bfgs.optim_options.reltol = reltol;
    let fit_0 = bfgs
        .optim((beta0 / y_sd).as_slice().to_vec(), &move |beta: Vec<
            f64,
        >|
              -> f64 {
            jaeckel_disp(
                &x_2,
                &y_star_temp_2.clone(),
                &DMatrix::from_vec(beta.len(), 1, beta.to_vec()),
                &scrs_mat_2.clone(),
            )
        })
        .expect("Could not optimize");
    let fit_beta = fit_0
        .parameters
        .iter()
        .map(|fit| fit * y_sd)
        .collect::<Vec<_>>();

    let mut bfgs = BFGS {
        grad_func: Some(Box::new(move |beta: Vec<f64>| -> Vec<f64> {
            jaeckel_grad(
                &x_3,
                &y_1,
                &DMatrix::from_vec(beta.len(), 1, beta.to_vec()),
                &scrs_mat_3.clone(),
            )
        })),
        ..Default::default()
    };
    bfgs.optim_options.maxit = maxit;
    bfgs.optim_options.reltol = reltol;
    let opt = bfgs
        .optim(fit_beta, &move |beta: Vec<f64>| -> f64 {
            jaeckel_disp(
                &x_4,
                &y,
                &DMatrix::from_vec(beta.len(), 1, beta.to_vec()),
                &scrs_mat_4.clone(),
            )
        })
        .expect("Could not optimize");
    opt.parameters
}
