// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use r2rs_base::traits::StatisticalSlice;
use r2rs_stats::traits::{DeviationType, StatArray};

use crate::{
    rank_fit::two_way::func::{hstar::hstar, hstarreadyscr::hstarrreadyscr, looptau::looptau},
    scores::Scores,
};

/// Estimate of the scale parameter tau
///
/// ## Description:
///
/// An estimate of the scale parameter tau is needed for the standard
/// errors of the coefficents in rank-based regression.
///
/// ## Usage:
///
/// gettau(ehat, p, scores = Rfit::wscores, delta = 0.8, hparm = 2, ...)
///
/// ## Arguments:
///
/// * ehat: full model residuals
/// * p: number of regression coefficents
/// * scores: object of class scores, defaults to Wilcoxon scores
/// * delta: confidence level
/// * hparm: Joe's hparm
/// * ...: additional arguments. currently unused
///
/// ## Details:
///
/// This is the confidence interval type estimate of the scale
/// parameter tau developed my Koul, Sievers, and McKean (1987). This
/// estimate is also discussed in Section 3.7.1 of Hettmansperger and
/// McKean (1998). One of these function is called in rfit.  The
/// default is to use the faster FORTRAN version. The R version can be
/// more precise in small samples, but also can be much slower
/// especially when sample sizes are large.
///
/// ## Value:
///
/// Length one numeric object.
///
/// ## Author(s):
///
/// Joseph McKean, John Kloke
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Koul, H.L., Sievers, G.L., and McKean, J.W. (1987) An esimator of
/// the scale parameter for the rank analysis of linear models under
/// general score functions, _Scandinavian Journal of Statistics_, 14,
/// 131-141.
///
/// ## See Also:
///
/// ‘rfit’
pub fn gettau(
    ehat: &[f64],
    p: usize,
    scores: &impl Scores,
    delta: Option<f64>,
    hparm: Option<usize>,
) -> f64 {
    let delta = delta.unwrap_or(0.8);
    let hparm = hparm.unwrap_or(2);
    let n = ehat.len();

    let sc_vec = (1..=n)
        .map(|i| i as f64 / (n as f64 + 1.0))
        .collect::<Vec<_>>();
    let asc = scores.get_scores(&sc_vec);
    let ascpr = scores.get_scores_deriv(&sc_vec);

    let (abdord, wtord, cons) = hstarrreadyscr(ehat, &asc, &ascpr);
    let temp1 = looptau(delta, &abdord, &wtord, cons, n);
    let tn = temp1.0 / (n as f64).sqrt();
    let pn = hstar(&abdord, &wtord, cons, n, tn);
    let mut tauscr = ((asc[n - 1] - asc[0]) * pn) / (2.0 * tn);
    tauscr = (n as f64 / (n as f64 - p as f64)).sqrt() * (1.0 / tauscr);

    let mut w = vec![0.0; n];
    let stan = ehat
        .iter()
        .map(|e_i| (e_i - ehat.median()) / ehat.mad(DeviationType::Median(None)))
        .collect::<Vec<_>>();
    w.iter_mut().zip(stan.iter()).for_each(|(w_i, stan_i)| {
        if stan_i.abs() < hparm as f64 {
            *w_i = 1.0
        }
    });
    let mut hubcor = w.iter().sum::<f64>() / n as f64;
    if hubcor < 0.000001 {
        hubcor = 0.000001;
    }

    let fincor = 1.0 + ((p as f64 / n as f64) * ((1.0 - hubcor) / hubcor));
    tauscr *= fincor;
    tauscr
}
