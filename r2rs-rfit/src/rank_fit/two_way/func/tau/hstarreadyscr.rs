// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::rank_fit::two_way::func::pairup::{pairup, PairType};

/// Internal Functions for Estimating tau
///
/// ## Description:
///
/// These are internal functions used for calculating the scale
/// parameter tau necessary for estimating the standard errors of
/// coefficients for rank-regression.
///
/// ## Usage:
///
/// hstarreadyscr(ehat,asc,ascpr)
/// hstar(abdord, wtord, const, n, y)
/// looptau(delta, abdord, wtord, const, n)
/// pairup(x,type="less")
///
/// ## Arguments:
///
/// * ehat: Full model residals
/// * delta: Window parameter (proportion) used in the Koul et al.
/// estimator of tau.  Default value is 0.80.  If the ratio of
/// sample size to number of regression parameters (n to p) is
/// less than 5, larger values such as 0.90 to 0.95 are more
/// approporiate.
/// * y: Argument of function hstar
/// * abdord: Ordered absolute differences of residuals
/// * wtord: Standardized (by const) ordered absolute differences of
/// residuals
/// * const: Range of score function
/// * n: Sample size
/// * x: Argument for pairup
/// * type: Argument for the function pairup
/// * asc: scores
/// * ascpr: derivative of the scores
///
/// ## Author(s):
///
/// Joseph McKean, John Kloke
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Koul, H.L., Sievers, G.L., and McKean, J.W. (1987) An esimator of
/// the scale parameter for the rank analysis of linear models under
/// general score functions, _Scandinavian Journal of Statistics_, 14,
/// 131-141.
///
/// ## See Also:
///
/// ‘gettau’, ‘rfit’
pub fn hstarrreadyscr(ehat: &[f64], asc: &[f64], ascpr: &[f64]) -> (Vec<f64>, Vec<f64>, f64) {
    let n = ehat.len();
    let mut orde = ehat.to_vec();
    orde.sort_by(|f1, f2| f1.partial_cmp(f2).unwrap());

    let cons = asc[n - 1] - asc[0];

    let mut vphi = vec![0.0; n];

    let mut cn = 0.0;
    for i in 0..n {
        vphi[i] = ascpr[i];
        cn += ascpr[i] / cons;
    }

    let vad = pairup(&orde, PairType::Less);
    let vwt = pairup(&vphi, PairType::Less) / cons;
    let vad2 = (vad.column(0) - vad.column(1))
        .into_iter()
        .map(|f| f.abs())
        .collect::<Vec<_>>();
    let vwt2 = (vwt.column(0) + vwt.column(1))
        .into_iter()
        .cloned()
        .collect::<Vec<_>>();

    let mut ords = vad2.into_iter().zip(vwt2.into_iter()).collect::<Vec<_>>();
    ords.sort_by(|(vad1, _), (vad2, _)| vad1.partial_cmp(vad2).unwrap());

    let (absdifford, wtsord): (Vec<_>, Vec<_>) = ords.into_iter().unzip();

    (absdifford, wtsord, cn)
}
