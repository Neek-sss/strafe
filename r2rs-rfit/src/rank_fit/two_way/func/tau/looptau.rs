// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::rank_fit::two_way::func::hstar::hstar;

/// Internal Functions for Estimating tau
///
/// ## Description:
///
/// These are internal functions used for calculating the scale
/// parameter tau necessary for estimating the standard errors of
/// coefficients for rank-regression.
///
/// ## Usage:
///
/// hstarreadyscr(ehat,asc,ascpr)
/// hstar(abdord, wtord, const, n, y)
/// looptau(delta, abdord, wtord, const, n)
/// pairup(x,type="less")
///
/// ## Arguments:
///
/// * ehat: Full model residals
/// * delta: Window parameter (proportion) used in the Koul et al.
/// estimator of tau.  Default value is 0.80.  If the ratio of
/// sample size to number of regression parameters (n to p) is
/// less than 5, larger values such as 0.90 to 0.95 are more
/// approporiate.
/// * y: Argument of function hstar
/// * abdord: Ordered absolute differences of residuals
/// * wtord: Standardized (by const) ordered absolute differences of
/// residuals
/// * const: Range of score function
/// * n: Sample size
/// * x: Argument for pairup
/// * type: Argument for the function pairup
/// * asc: scores
/// * ascpr: derivative of the scores
///
/// ## Author(s):
///
/// Joseph McKean, John Kloke
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Koul, H.L., Sievers, G.L., and McKean, J.W. (1987) An esimator of
/// the scale parameter for the rank analysis of linear models under
/// general score functions, _Scandinavian Journal of Statistics_, 14,
/// 131-141.
///
/// ## See Also:
///
/// ‘gettau’, ‘rfit’
pub fn looptau(
    delta: f64,
    abdord: &[f64],
    wtord: &[f64],
    cons: f64,
    n: usize,
) -> (f64, f64, i32, usize, i32) {
    let m = abdord.len();
    let mut icmax = 0;
    let inddelta = (delta * m as f64).round() as usize;

    let mut guess = abdord[inddelta - 1];
    let itmax = m;
    let mut y = hstar(abdord, wtord, cons, n, guess);
    let mut ikeep = inddelta;
    let mut ic = 0.0;
    let mut ierror = 0;
    let mut gohome = 0;
    let mut istage = 1;

    // Good Guess
    if y == delta {
        ic = 1.0;
        gohome = -1;
    }
    if gohome == 0 && y > delta {
        istage = -1;
    }

    // Set the Stage
    while gohome == 0 {
        // Iterate
        while ic < 0.5 {
            icmax += 1;
            if istage == -1 {
                if y > delta {
                    ikeep -= 1;
                    if ikeep < 1 {
                        ic = 1.0;
                        ierror = 1;
                    } else {
                        guess = abdord[ikeep - 1];
                        y = hstar(abdord, wtord, cons, n, guess);
                    }
                } else {
                    ic = 1.0;
                    gohome = 1;
                }
            }
            if istage == 1 {
                if y < delta {
                    ikeep += 1;
                    if ikeep > m {
                        ic = 1.0;
                        ierror = 1;
                    } else {
                        guess = abdord[ikeep - 1];
                        y = hstar(abdord, wtord, cons, n, guess);
                    }
                } else {
                    ic = 1.0;
                    gohome = 1;
                }
            }
            icmax += 1;
            if icmax > itmax {
                ic = 1.0;
                ierror = 2;
                gohome = 1;
            }
        }
    }

    (guess, y, ierror, icmax, gohome)
}
