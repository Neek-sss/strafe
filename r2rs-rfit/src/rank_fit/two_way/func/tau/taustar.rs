// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use r2rs_base::func::partial_sort;
use r2rs_nmath::{distribution::NormalBuilder, traits::Distribution};
use strafe_type::FloatConstraint;

pub fn taustar(e: &[f64], p: usize, conf: Option<f64>) -> f64 {
    let conf = conf.unwrap_or(0.95);

    let n = e.len();
    let zc = NormalBuilder::new()
        .build()
        .quantile((1.0 + conf) / 2.0, true)
        .unwrap();

    let mut c1 = (0.5 * (n as f64) - 0.5 * (n as f64).sqrt() * zc - 0.5).floor();

    if c1 < 0.0 {
        c1 = 0.0;
    }

    let low_ind = c1 as usize;
    let high_ind = n - c1 as usize - 1;
    let mut z_vec = e.to_vec();
    partial_sort(&mut z_vec, &[low_ind, high_ind]).unwrap();
    let z = z_vec[high_ind] - z_vec[low_ind];

    (n as f64 / (n as f64 - p as f64 - 1.0)).sqrt() * (n as f64).sqrt() * z / (2.0 * zc)
}
