// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use r2rs_base::{func::order, traits::StatisticalSlice};
use r2rs_stats::funcs::linear_regression_qr;

use crate::{
    funcs::dispersion,
    rank_fit::two_way::func::{
        jaeckel::jaeckel,
        signed_rank::signed_rank,
        tau::{gettau::gettau, taustar::taustar},
    },
    scores::Scores,
};

pub fn rank_fit(
    x: &DMatrix<f64>,
    y: &DMatrix<f64>,
    intercept: bool,
    symmetric: bool,
    scores: &impl Scores,
) -> (DMatrix<f64>, DMatrix<f64>, f64, f64) {
    let xq1 = x.clone().insert_column(0, 1.0).qr().q();
    let xq = if intercept {
        xq1.remove_column(0)
    } else {
        xq1.remove_column(x.ncols())
    };

    let x1 = if intercept {
        x.clone().insert_column(0, 1.0)
    } else {
        x.clone()
    };

    let starting_model = {
        // Least Squares fit without intercept
        let least_squares_model = xq.transpose() * y;

        // Null model (all betas = 0)
        let null_model = DMatrix::from_iterator(
            least_squares_model.nrows(),
            1,
            (0..least_squares_model.nrows()).map(|_| 0.0),
        );

        // Set starting model to the model with lower dispersion
        if dispersion(&least_squares_model, &xq, y, scores)
            < dispersion(&null_model, &xq, y, scores)
        {
            least_squares_model
        } else {
            null_model
        }
    };

    let ord = order(
        &(y - &xq * &starting_model)
            .iter()
            .cloned()
            .collect::<Vec<_>>(),
    );
    let ordered_xq = DMatrix::<f64>::from_row_iterator(
        xq.nrows(),
        xq.ncols(),
        ord.iter()
            .map(|&i| xq.row(i).iter().cloned().collect::<Vec<_>>())
            .flatten(),
    );

    let ordered_y = DMatrix::<f64>::from_row_iterator(
        y.nrows(),
        y.ncols(),
        ord.iter()
            .map(|&i| y.row(i).iter().cloned().collect::<Vec<_>>())
            .flatten(),
    );

    let jaeckel_betas = jaeckel(&ordered_xq, &ordered_y, &starting_model);
    let betahat = DMatrix::from_vec(jaeckel_betas.len(), 1, jaeckel_betas);

    let mut yhat = xq * betahat;
    let mut ehat = y - &yhat;
    let alphahat = if symmetric {
        signed_rank(ehat.as_slice())
    } else {
        ehat.column(0).as_slice().median()
    };
    ehat.iter_mut().for_each(|ei| *ei -= alphahat);
    yhat.iter_mut().for_each(|yi| *yi += alphahat);

    let mut bhat = linear_regression_qr(&x1, &yhat);

    let alphahat0 = if symmetric {
        signed_rank(y.as_slice())
    } else {
        y.as_slice().median()
    };
    let mut bhat0 = DMatrix::<f64>::zeros(bhat.nrows(), bhat.ncols());
    bhat0[(0, 0)] = alphahat0;

    if dispersion(&bhat, &x1, y, scores) > dispersion(&bhat0, &x1, y, scores) {
        bhat = bhat0;
        ehat = y.clone();
        ehat.iter_mut().for_each(|ehat| *ehat -= alphahat0);
        yhat.iter_mut().for_each(|yhat| *yhat = alphahat0);
    }

    let tauhat = gettau(ehat.as_slice(), x.ncols(), scores, None, None);
    let taushat = if symmetric {
        tauhat
    } else {
        taustar(ehat.as_slice(), x.ncols() + 1, None)
    };

    (bhat, ehat, tauhat, taushat)
}
