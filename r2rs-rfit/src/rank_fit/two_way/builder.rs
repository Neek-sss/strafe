// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use r2rs_base::func::bootstrap_ci;
use r2rs_nmath::traits::RNG;
use strafe_trait::ModelBuilder;
use strafe_type::{Alpha64, ModelMatrix};

use crate::{
    rank_fit::two_way::{rank_fit, RankFit},
    scores::Scores,
};

pub struct RankFitBuilder<S: Scores, R: RNG + Default> {
    pub(crate) x: Option<ModelMatrix>,
    pub(crate) y: Option<ModelMatrix>,
    pub(crate) w: Option<ModelMatrix>,
    pub(crate) intercept: bool,
    pub(crate) bootstrap: bool,
    pub(crate) bootstrap_amount: Option<usize>,
    pub(crate) bootstrap_rng: R,
    pub(crate) alpha: Alpha64,
    pub(crate) symmetric: bool,
    pub(crate) scores: S,
}

impl<S: Scores, R: RNG + Default> RankFitBuilder<S, R> {
    pub fn new() -> Self {
        Self {
            x: None,
            y: None,
            intercept: true,
            bootstrap: true,
            bootstrap_amount: None,
            bootstrap_rng: R::default(),
            w: None,
            alpha: 0.05.into(),
            symmetric: false,
            scores: S::default(),
        }
    }

    pub fn with_symmetric(self, symmetric: bool) -> Self {
        Self { symmetric, ..self }
    }

    pub fn with_bootstrap_coefficient_ci(self, bootstrap: bool) -> Self {
        Self { bootstrap, ..self }
    }

    pub fn with_bootstrap_rng(self, rng: &R) -> Self {
        Self {
            bootstrap_rng: rng.clone(),
            ..self
        }
    }

    pub fn with_scores(self, scores: &S) -> Self {
        Self {
            scores: scores.clone(),
            ..self
        }
    }

    pub fn with_intercept(self, intercept: bool) -> Self {
        Self { intercept, ..self }
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
            ..self
        }
    }
}

impl<S: Scores, R: RNG + Default> ModelBuilder for RankFitBuilder<S, R> {
    type Model = RankFit<S>;

    fn with_x(self, x: &ModelMatrix) -> Self {
        Self {
            x: Some(x.clone()),
            ..self
        }
    }

    fn with_y(self, y: &ModelMatrix) -> Self {
        Self {
            y: Some(y.clone()),
            ..self
        }
    }

    fn with_weights(self, weights: &ModelMatrix) -> Self {
        Self {
            w: Some(weights.clone()),
            ..self
        }
    }

    fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
            ..self
        }
    }

    fn build(mut self) -> Self::Model {
        let (beta, residuals, tauhat, taushat) = rank_fit(
            &self.x.as_ref().unwrap().matrix(),
            &self.y.as_ref().unwrap().matrix(),
            self.intercept,
            self.symmetric,
            &self.scores,
        );

        let x1 = if self.intercept {
            ModelMatrix::from(self.x.as_ref().unwrap().matrix().insert_column(0, 1.0))
        } else {
            ModelMatrix::from(self.x.as_ref().unwrap().clone())
        };

        let mut ret = RankFit {
            x: self.x.as_ref().unwrap().clone(),
            y: self.y.as_ref().unwrap().clone(),
            bootstrap: self.bootstrap,
            bootstrap_b_ci: vec![],
            intercept: false,
            x1,
            b: beta.clone(),
            alpha: self.alpha,
            symmetric: self.symmetric,
            scores: self.scores.clone(),
            tauhat,
            taushat,
            residuals,
        };

        if self.bootstrap {
            let x = self.x.as_ref().unwrap().matrix();
            let y = self.y.as_ref().unwrap().matrix();
            for i in 0..beta.len() {
                let ci = bootstrap_ci(
                    &|resample: &[usize]| {
                        let mut x_clone = x.clone();
                        for (mut row, &i) in x_clone.row_iter_mut().zip(resample.iter()) {
                            for (val, &new_val) in row.iter_mut().zip(x.row(i).iter()) {
                                *val = new_val;
                            }
                        }

                        let mut y_clone = y.clone();
                        for (mut row, &i) in y_clone.row_iter_mut().zip(resample.iter()) {
                            for (val, &new_val) in row.iter_mut().zip(y.row(i).iter()) {
                                *val = new_val;
                            }
                        }

                        let (beta, _, _, _) = rank_fit(
                            &x_clone,
                            &y_clone,
                            self.intercept,
                            self.symmetric,
                            &self.scores,
                        );
                        beta.as_slice()[i]
                    },
                    x.nrows(),
                    self.alpha,
                    self.bootstrap_amount,
                    &mut self.bootstrap_rng,
                );
                ret.bootstrap_b_ci.push(ci);
            }
        }

        ret
    }
}
