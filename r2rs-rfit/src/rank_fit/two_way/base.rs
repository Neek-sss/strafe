// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::fmt::{Debug, Display, Formatter};

use nalgebra::DMatrix;
use r2rs_base::traits::{QuantileType, StatisticalSlice};
use strafe_trait::{Model, Statistic, StatisticalEstimate, StatisticalTest};
use strafe_type::{Alpha64, DisplayTable, FloatConstraint, ModelMatrix};

use crate::scores::Scores;

/// Rank-based Estimates of Regression Coefficients
///
/// ## Description:
///
/// Minimizes Jaeckel's dispersion function to obtain a rank-based
/// solution for linear models.
///
/// ## Usage:
///
/// rfit(formula, data = list(), ...)
///
/// *Default S3 method:*
/// rfit(formula, data, subset, yhat0 = NULL,
/// scores = Rfit::wscores, symmetric = FALSE, TAU = "F0", ...)
///
/// ## Arguments:
///
/// * formula: an object of class formula
/// * data: an optional data frame
/// * subset: an optional argument specifying the subset of observations to
/// be used
/// * yhat0: an n by vector of initial fitted values, default is NULL
/// * scores: an object of class 'scores'
/// * symmetric: logical.  If 'FALSE' uses median of residuals as estimate of
/// intercept
/// * TAU: version of estimation routine for scale parameter.  F0 for
/// Fortran, R for (slower) R, N for none
/// * ...: additional arguments to be passed to fitting routines
///
/// ## Details:
///
/// Rank-based estimation involves replacing the L2 norm of least
/// squares estimation with a pseudo-norm which is a function of the
/// ranks of the residuals. That is, in rank estimation, the usual
/// notion of Euclidean distance is replaced with another measure of
/// distance which is referred to as Jaeckel's (1972) dispersion
/// function. Jaeckel's dispersion function depends on a score
/// function and a library of commonly used score functions is
/// included.  e.g. linear (Wilcoxon) and normal (Gaussian) scores. If
/// an inital fit is not supplied (i.e. yhat0 = NULL) then inital fit
/// is based on a LS fit.
///
/// ## Value:
///
/// * coefficients: estimated regression coefficents with intercept
/// * residuals: the residuals, i.e. y-yhat
/// * fitted.values: yhat = x betahat
/// * xc: centered design matrix
/// * tauhat: estimated value of the scale parameter tau
/// * taushat: estimated value of the scale parameter tau_s
/// * betahat: estimated regression coefficents
/// * call: Call to the function
///
/// ## Author(s):
///
/// John Kloke, Joesph McKean
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Jaeckel, L. A. (1972). Estimating regression coefficients by
/// minimizing the dispersion of residuals. _Annals of Mathematical
/// Statistics_, 43, 1449 - 1458.
///
/// Jureckova, J. (1971). Nonparametric estimate of regression
/// coefficients. _Annals of Mathematical Statistics_, 42, 1328 -
/// 1338.
///
/// ## See Also:
///
/// ‘summary.rfit’ ‘drop.test’ ‘rstudent.rfit’
///
/// ## Examples:
///
/// ```r
/// data(baseball)
/// data(wscores)
/// fit<-rfit(weight~height,data=baseball)
/// summary(fit)
///
/// ### set the starting value
/// x1 <- runif(47); x2 <- runif(47); y <- 1 + 0.5*x1 + rnorm(47)
/// # based on a fit to a sub-model
/// rfit(y~x1+x2,yhat0=fitted.values(rfit(y~x1)))
/// ```
#[derive(Clone, Debug)]
pub struct RankFit<S: Scores> {
    pub(crate) x: ModelMatrix,
    pub(crate) y: ModelMatrix,
    pub(crate) bootstrap: bool,
    pub(crate) bootstrap_b_ci: Vec<(f64, f64)>,
    pub(crate) intercept: bool,
    pub(crate) x1: ModelMatrix,
    pub(crate) b: DMatrix<f64>,
    pub(crate) alpha: Alpha64,
    pub(crate) symmetric: bool,
    pub(crate) scores: S,
    pub(crate) tauhat: f64,
    pub(crate) taushat: f64,
    pub(crate) residuals: DMatrix<f64>,
}

impl<S: Scores> RankFit<S> {
    pub(crate) fn get_tauhat(&self) -> f64 {
        self.tauhat
    }

    pub(crate) fn get_taushat(&self) -> f64 {
        self.taushat
    }
}

impl<S: Scores> Display for RankFit<S> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut s = self.clone();

        // Write residuals table
        writeln!(f, "Residuals:")?;
        let headers = vec![
            "Minimum".to_string(),
            "1st Quantile".to_string(),
            "Median".to_string(),
            "3rd Quantile".to_string(),
            "Maximum".to_string(),
        ];
        let lines = vec![s
            .residuals()
            .unwrap()
            .matrix()
            .as_slice()
            .quantile(&[0.0, 0.25, 0.5, 0.75, 1.0], QuantileType::S)
            .into_iter()
            .map(|f| f)
            .collect::<Vec<_>>()];
        let row_names = Vec::new();
        writeln!(f, "{}", DisplayTable::new(headers, row_names, lines, None))?;

        // Write coefficients table
        writeln!(f, "Coefficients:")?;
        let headers = if self.bootstrap {
            vec![
                "Estimate".to_string(),
                "Confidence Interval (L)".to_string(),
                "Confidence Interval (U)".to_string(),
                "T-Value".to_string(),
                "P-Value".to_string(),
            ]
        } else {
            vec![
                "Estimate".to_string(),
                "T-Value".to_string(),
                "P-Value".to_string(),
            ]
        };
        let mut row_names = Vec::new();
        let mut lines = Vec::new();
        for coef in s.clone().test(&()).unwrap().coefs {
            row_names.push(coef.name.clone());
            if self.bootstrap {
                lines.push(vec![
                    coef.estimate(),
                    coef.confidence_interval().0,
                    coef.confidence_interval().1,
                    coef.statistic(),
                    coef.probability_value(),
                ])
            } else {
                lines.push(vec![coef.estimate, coef.t, coef.p])
            };
        }
        writeln!(f, "{}", DisplayTable::new(headers, row_names, lines, None))?;

        // Write tests table
        writeln!(f, "Tests:")?;
        let headers = vec![
            "Statistic".to_string(),
            "P-Value".to_string(),
            "Alpha".to_string(),
        ];
        let mut row_names = Vec::new();
        let mut lines = Vec::new();

        let rsq = s.determination().unwrap();
        row_names.push("Multiple R-squared (Robust)".to_string());
        lines.push(vec![
            rsq.statistic(),
            rsq.probability_value(),
            rsq.alpha().unwrap(),
        ]);

        let drop = s.test(&()).unwrap().drop;
        row_names.push("Reduction in Dispersion".to_string());
        lines.push(vec![
            drop.statistic(),
            drop.probability_value(),
            drop.alpha().unwrap(),
        ]);

        let wald = s.test(&()).unwrap().wald;
        row_names.push("Overall Wald".to_string());
        lines.push(vec![
            wald.statistic(),
            wald.probability_value(),
            wald.alpha().unwrap(),
        ]);

        writeln!(f, "{}", DisplayTable::new(headers, row_names, lines, None))?;

        Ok(())
    }
}
