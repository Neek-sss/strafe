// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use r2rs_nmath::{distribution::TBuilder, traits::Distribution};
use r2rs_stats::{
    funcs::predict,
    traits::{DeviationType, StatArray},
};
use strafe_type::{Alpha64, FloatConstraint, Real64};

use crate::rank_fit::two_way::func::rfit_vcov;

fn interval_inner<A: Into<Alpha64>>(
    x: &DMatrix<f64>,
    b: &DMatrix<f64>,
    alpha: A,
    tauhat: f64,
    taushat: f64,
) -> (DMatrix<f64>, Real64, Vec<f64>) {
    let variance = (x * rfit_vcov(x, tauhat, taushat) * x.transpose())
        .diagonal()
        .as_slice()
        .to_vec();

    let eta = predict(x, b);

    let mut t_builder = TBuilder::new();
    t_builder.with_df(x.nrows() - x.ncols() - 1).unwrap();
    let t_dist = t_builder.build();
    let t = t_dist.quantile(alpha.into().unwrap() / 2.0, true);

    (eta, t, variance)
}

pub fn confidence_interval<A: Into<Alpha64>>(
    x: &DMatrix<f64>,
    b: &DMatrix<f64>,
    alpha: A,
    tauhat: f64,
    taushat: f64,
) -> (DMatrix<f64>, DMatrix<f64>) {
    let (eta, t, variance) = interval_inner(x, b, alpha, tauhat, taushat);

    (
        DMatrix::from_iterator(
            variance.len(),
            1,
            eta.iter()
                .zip(variance.iter())
                .map(|(eta_i, var_i)| eta_i - (t.unwrap() * var_i.sqrt())),
        ),
        DMatrix::from_iterator(
            variance.len(),
            1,
            eta.iter()
                .zip(variance.iter())
                .map(|(eta_i, var_i)| eta_i + (t.unwrap() * var_i.sqrt())),
        ),
    )
}

/// According to _Nonparametric Statistical Methods Using R_ (2015) by *John Kloke* and *Joseph W.
/// McKean* one can use the Median Absolute Deviance along with the calculation of standard error to
/// estimate the prediction interval, albeit giving a wider estimate of the prediction interval than
/// is true.  We use this method as there does not seem to be a better alternative at the time of
/// writing, and this is erring on the side of caution.
pub fn prediction_interval<A: Into<Alpha64>>(
    x: &DMatrix<f64>,
    b: &DMatrix<f64>,
    alpha: A,
    tauhat: f64,
    taushat: f64,
    resids: &[f64],
) -> (DMatrix<f64>, DMatrix<f64>) {
    let (eta, t, variance) = interval_inner(x, b, alpha, tauhat, taushat);
    let mad = resids.mad(DeviationType::Median(None));

    (
        DMatrix::from_iterator(
            variance.len(),
            1,
            eta.iter()
                .zip(variance.iter())
                .map(|(eta_i, var_i)| eta_i - (t.unwrap() * (var_i + mad).sqrt())),
        ),
        DMatrix::from_iterator(
            variance.len(),
            1,
            eta.iter()
                .zip(variance.iter())
                .map(|(eta_i, var_i)| eta_i + (t.unwrap() * (var_i + mad).sqrt())),
        ),
    )
}
