// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use nalgebra::DMatrix;
use num_traits::ToPrimitive;
use r2rs_nmath::rng::MarsagliaMulticarry;
use strafe_testing::{
    r::{RGenerator, RString, RTester},
    r_assert_relative_equal, r_assert_relative_equal_result,
};
use strafe_trait::{Model, ModelBuilder, Statistic, StatisticalTest};
use strafe_type::ModelMatrix;

use crate::{
    rank_fit::{
        anova::raov,
        one_way::OneWayRankFitBuilder,
        two_way::{signed_rank, RankFitBuilder},
    },
    scores::WilcoxScores,
};

pub fn signed_rank_test_inner(seed: u16) {
    let xs = RGenerator::new()
        .set_seed(seed + 1)
        .generate_uniform(100, (-100, 100));

    let library = RString::from_library_name("Rfit");

    let r_signed_rank = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_slice(&xs),
        )))
        .set_display(&RString::from_str("signedrank(x)").unwrap())
        .run()
        .expect("R running error")
        .as_f64()
        .expect("R running error");

    let rust_signed_rank = signed_rank(&xs);

    r_assert_relative_equal!(r_signed_rank, rust_signed_rank, 1e-4);
}

pub fn rank_fit_random_generator(
    seed: u16,
    num_features: f64,
    num_rows: f64,
    fuzz_amount: f64,
) -> (DMatrix<f64>, Vec<f64>) {
    let num_features = num_features.to_usize().unwrap();
    let num_rows = num_rows.to_usize().unwrap();
    let fuzz_amount = fuzz_amount.to_usize().unwrap();

    let xs =
        RGenerator::new()
            .set_seed(seed)
            .generate_uniform_matrix(num_rows, num_features, (-10, 10));

    let features = RGenerator::new()
        .set_seed(seed + 1)
        .generate_uniform(num_features + 1, (-100, 100));

    let errors =
        RGenerator::new()
            .set_seed(seed + 2)
            .generate_errors(num_rows, (0, 1), fuzz_amount);

    let ys = (0..num_rows)
        .map(|i| {
            (0..num_features).fold(features[0] + errors[i], |ret, j| {
                ret + (features[j + 1] * xs[(i, j)])
            })
        })
        .collect::<Vec<_>>();

    (xs, ys)
}

pub fn rank_fit_test_inner(symmetric: bool, intercept: bool, x: &DMatrix<f64>, y: &[f64]) {
    let mut model = RankFitBuilder::<WilcoxScores, MarsagliaMulticarry>::new()
        .with_bootstrap_coefficient_ci(false)
        .with_symmetric(symmetric)
        .with_intercept(intercept)
        .with_x(&ModelMatrix::from(x))
        .with_y(&ModelMatrix::from(y))
        .build();
    let model_test = model.test(&()).unwrap();

    let library = RString::from_library_name("Rfit");

    let r_fit_string = if intercept {
        "rfit(y~x, TAU='R', symmetric=s)"
    } else {
        "rfit(y~x-1, TAU='R', symmetric=s)"
    };
    let r_regression = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};s={};",
            RString::from_f64_slice(y),
            RString::from_f64_matrix(x),
            RString::from_bool(symmetric),
        )))
        .set_display(
            &RString::from_str(&format!("summary({r_fit_string}, overall.test='all')",)).unwrap(),
        )
        .run()
        .expect("R error")
        .as_r_fit();

    if intercept {
        r_assert_relative_equal_result!(
            Ok(r_regression.r_squared_robust),
            &model.determination().unwrap().statistic(),
            1e-4
        );
        r_assert_relative_equal_result!(
            Ok(r_regression.red_disp_test),
            &model_test.drop.statistic(),
            1e-4
        );
        if r_regression.disp_p <= 0.05 {
            if model_test.drop.probability_value() > 0.05 {
                r_assert_relative_equal_result!(
                    Ok(r_regression.disp_p),
                    &model_test.drop.probability_value(),
                    1e-4
                );
            }
        } else {
            if model_test.drop.probability_value() <= 0.05 {
                r_assert_relative_equal_result!(
                    Ok(r_regression.disp_p),
                    &model_test.drop.probability_value(),
                    1e-4
                );
            }
        }
        r_assert_relative_equal_result!(
            Ok(r_regression.wald_test),
            &model_test.wald.statistic(),
            1e-4
        );
        if r_regression.wald_p <= 0.05 {
            if model_test.wald.probability_value() > 0.05 {
                r_assert_relative_equal_result!(
                    Ok(r_regression.wald_p),
                    &model_test.wald.probability_value(),
                    1e-4
                );
            }
        } else {
            if model_test.wald.probability_value() <= 0.05 {
                r_assert_relative_equal_result!(
                    Ok(r_regression.wald_p),
                    &model_test.wald.probability_value(),
                    1e-4
                );
            }
        }
    }

    for i in 0..x.ncols() {
        //     assert!(
        //         r_regression.coefs[i].estimate
        //             >= model.significance_of_coeffients[i].confidence_interval().0
        //     );
        //     assert!(
        //         r_regression.coefs[i].estimate
        //             <= model.significance_of_coeffients[i].confidence_interval().1
        //     );
        r_assert_relative_equal_result!(
            Ok(r_regression.coefs[i].estimate),
            &model.parameters().unwrap()[i].estimate(),
            1e-4
        );
        if intercept {
            r_assert_relative_equal_result!(
                Ok(r_regression.coefs[i].t),
                &model_test.coefs[i].statistic(),
                1e-4
            );
        }
        if r_regression.coefs[i].p <= 0.05 {
            if model_test.coefs[i].probability_value() > 0.05 {
                r_assert_relative_equal_result!(
                    Ok(r_regression.coefs[i].p),
                    &model_test.coefs[i].probability_value(),
                    1e-4
                );
            }
        } else {
            if model_test.coefs[i].probability_value() <= 0.05 {
                r_assert_relative_equal_result!(
                    Ok(r_regression.coefs[i].p),
                    &model_test.coefs[i].probability_value(),
                    1e-4
                );
            }
        }
    }

    let r_pred = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};s={};",
            RString::from_f64_slice(y),
            RString::from_f64_matrix(x),
            RString::from_bool(symmetric),
        )))
        .set_display(&RString::from_str(&format!("{r_fit_string}$fitted.values")).unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R error");

    if intercept {
        for (&rust_ret, &r_ret) in model
            .predictions()
            .unwrap()
            .estimate()
            .matrix()
            .iter()
            .zip(r_pred.iter())
        {
            r_assert_relative_equal!(rust_ret, r_ret, 1e-4);
        }
    }

    let r_res = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};s={};",
            RString::from_f64_slice(y),
            RString::from_f64_matrix(x),
            RString::from_bool(symmetric),
        )))
        .set_display(&RString::from_str(&format!("unname(residuals({r_fit_string}))")).unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R error");

    for (&rust_ret, &r_ret) in model.residuals().unwrap().matrix().iter().zip(r_res.iter()) {
        r_assert_relative_equal!(rust_ret, r_ret, 1e-4);
    }

    let r_vcov = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};s={};",
            RString::from_f64_slice(y),
            RString::from_f64_matrix(x),
            RString::from_bool(symmetric),
        )))
        .set_display(&RString::from_str(&format!("vcov({r_fit_string})")).unwrap())
        .run()
        .expect("R error")
        .as_f64_matrix()
        .expect("R error");

    if intercept {
        for (&rust_ret, &r_ret) in model.variance().unwrap().iter().zip(r_vcov.iter()) {
            r_assert_relative_equal!(rust_ret, r_ret, 1e-4);
        }
    }

    let rust_studentized_residuals = model.studentized_residuals().unwrap().matrix();
    let r_studentized_residuals = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};s={};",
            RString::from_f64_slice(y),
            RString::from_f64_matrix(x),
            RString::from_bool(symmetric),
        )))
        .set_display(&RString::from_str(&format!("rstudent.rfit({r_fit_string})")).unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R error");

    if intercept {
        for (&rust_ret, &r_ret) in rust_studentized_residuals
            .iter()
            .zip(r_studentized_residuals.iter())
        {
            r_assert_relative_equal!(rust_ret, r_ret, 1e-4);
        }
    }
}

pub fn one_way_rank_fit_test_inner(x: &[f64], groups: &[usize]) {
    let mut owrf = OneWayRankFitBuilder::<WilcoxScores, _>::new()
        .with_groups(groups)
        .with_x(x)
        .build()
        .unwrap();
    let rust_fit = owrf.test(&()).unwrap();

    let library = RString::from_library_name("Rfit");
    let r_fit = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};g={};",
            RString::from_f64_slice(&x),
            RString::from_f64_slice(&groups.iter().map(|&g| g as f64).collect::<Vec<_>>()),
        )))
        .set_display(&RString::from_str("summary(oneway.rfit(x, g))").unwrap())
        .run()
        .expect("R error")
        .as_one_way_r_fit();

    for i in 0..r_fit.len() {
        assert_eq!(rust_fit[i].i, r_fit[i].i);
        assert_eq!(rust_fit[i].j, r_fit[i].j);
        r_assert_relative_equal!(rust_fit[i].estimate, r_fit[i].estimate, 1e-4);
        r_assert_relative_equal!(
            rust_fit[i].confidence_interval.0,
            r_fit[i].confidence_interval.0,
            1e-4
        );
        r_assert_relative_equal!(
            rust_fit[i].confidence_interval.1,
            r_fit[i].confidence_interval.1,
            1e-4
        );
    }
}

pub fn rank_anova(x: &DMatrix<f64>, y: &DMatrix<f64>) {
    let mut x_names = vec!["x[,1]".to_string()];
    let mut x_string = "x[,1]".to_string();
    for i in 2..x.ncols() + 1 {
        x_string += &format!("+x[,{i}]");
        x_names.push(format!("x[,{i}]"))
    }

    let rust_rank_anova =
        raov::<WilcoxScores>(&ModelMatrix::from(x), &x_names, &ModelMatrix::from(y)).unwrap();

    let library = RString::from_library_name("Rfit");
    let r_rank_anova = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};y={};",
            RString::from_f64_matrix(&x),
            RString::from_f64_matrix(&y),
        )))
        .set_display(&RString::from_str(&format!("raov(y~{x_string})")).unwrap())
        .run()
        .expect("R error")
        .as_raov();

    for (rust, r) in rust_rank_anova.iter().zip(r_rank_anova.iter()) {
        assert_eq!(rust.label, r.name);
        assert_eq!(rust.degrees_of_freedom, r.df as usize);
        r_assert_relative_equal!(rust.residual_deviation, r.rd);
        r_assert_relative_equal!(rust.mean_residual_deviation, r.mean_rd, 1e-4);
        // r_assert_relative_equal!(rust.f, r.f, 1e-4);
        // r_assert_relative_equal!(rust.p, r.p, 1e-4);
    }
}
