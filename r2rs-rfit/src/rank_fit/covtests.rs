// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use polars::datatypes::DataType;

use super::tests::*;
use crate::data::{baseball, boxcox, quail};

#[test]
fn signed_rank_test() {
    signed_rank_test_inner(1);
}

#[test]
fn rank_fit_test_1() {
    let (x, y) = rank_fit_random_generator(1, 1.0, 12.0, 8.0);
    rank_fit_test_inner(false, true, &x, &y);
}

#[test]
fn rank_fit_test_2() {
    let (x, y) = rank_fit_random_generator(1, 2.0, 12.0, 8.0);
    rank_fit_test_inner(true, true, &x, &y);
}

#[test]
fn rank_fit_test_3() {
    let baseball = baseball().unwrap();

    let x = DMatrix::<f64>::from_iterator(
        baseball.height(),
        2,
        baseball
            .column("height")
            .unwrap()
            .iter()
            .chain(baseball.column("weight").unwrap().iter())
            .map(|f| {
                let ret: f64 = f.cast(&DataType::Float64).try_extract().unwrap();
                ret
            }),
    );
    let y = baseball
        .column("average")
        .unwrap()
        .iter()
        .map(|f| {
            let ret: f64 = f.cast(&DataType::Float64).try_extract().unwrap();
            ret
        })
        .collect::<Vec<_>>();

    rank_fit_test_inner(false, true, &x, &y);
}

#[test]
fn rank_fit_test_4() {
    let baseball = baseball().unwrap();

    let x = DMatrix::<f64>::from_iterator(
        baseball.height(),
        2,
        baseball
            .column("height")
            .unwrap()
            .iter()
            .chain(baseball.column("weight").unwrap().iter())
            .map(|f| {
                let ret: f64 = f.cast(&DataType::Float64).try_extract().unwrap();
                ret
            }),
    );
    let y = baseball
        .column("average")
        .unwrap()
        .iter()
        .map(|f| {
            let ret: f64 = f.cast(&DataType::Float64).try_extract().unwrap();
            ret
        })
        .collect::<Vec<_>>();

    rank_fit_test_inner(false, false, &x, &y);
}

#[test]
fn one_way_rank_fit_test_1() {
    let quail = quail().unwrap();
    let treat = quail
        .column("treat")
        .unwrap()
        .iter()
        .map(|f| {
            let ret: u32 = f.cast(&DataType::UInt32).try_extract().unwrap();
            ret as usize
        })
        .collect::<Vec<_>>();

    let ldl = quail
        .column("ldl")
        .unwrap()
        .iter()
        .map(|f| {
            let ret: f64 = f.cast(&DataType::Float64).try_extract().unwrap();
            ret
        })
        .collect::<Vec<_>>();
    one_way_rank_fit_test_inner(&ldl, &treat);
}

#[test]
fn raov() {
    let box_cox = boxcox().unwrap();
    let y = DMatrix::<f64>::from_iterator(
        box_cox.shape().0,
        1,
        box_cox["logSurv"]
            .cast(&DataType::Float64)
            .unwrap()
            .f64()
            .unwrap()
            .to_vec()
            .into_iter()
            .map(|x| x.unwrap()),
    );
    let x = DMatrix::<f64>::from_iterator(
        box_cox.shape().0,
        2,
        box_cox["Poison"]
            .cast(&DataType::Float64)
            .unwrap()
            .f64()
            .unwrap()
            .to_vec()
            .into_iter()
            .map(|x| x.unwrap())
            .chain(
                box_cox["Treatment"]
                    .cast(&DataType::Float64)
                    .unwrap()
                    .f64()
                    .unwrap()
                    .to_vec()
                    .into_iter()
                    .map(|x| x.unwrap()),
            ),
    );

    rank_anova(&x, &y);
}
