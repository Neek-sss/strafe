use nalgebra::DMatrix;
use r2rs_stats::traits::StatArray;

use crate::scores::Scores;

/// Jaeckel's Dispersion Function
///
/// ## Description:
///
/// Returns the value of Jaeckel's dispersion function for given
/// values of the regression coefficents.
///
/// ## Usage:
///
/// disp(beta, x, y, scores)
///
/// ## Arguments:
///
/// * beta: p by 1 vector of regression coefficents
/// *  x: n by p design matrix
/// *  y: n by 1 response vector
/// * scores: an object of class scores
///
/// ## Details:
///
/// Returns the value of Jaeckel's disperion function evaluated at the
/// value of the parameters in the function call. That is,
/// $\sum_i=1^n a(R(e_i)) * e_i$ where $R$ denotes rank and
/// $a(1) <= a(2) <= ... <= a(n)$ are the scores. The residuals
/// $(e_i i=1,...n)$ are calculated $y - x \beta$.
///
/// ## Author(s):
///
/// John Kloke, Joseph McKean
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// Jaeckel, L. A. (1972). Estimating regression coefficients by
/// minimizing the dispersion of residuals. _Annals of Mathematical
/// Statistics_, 43, 1449 - 1458.
///
/// ## See Also:
///
/// ‘rfit’ ‘drop.test’ ‘summary.rfit’
pub fn dispersion(
    beta: &DMatrix<f64>,
    x: &DMatrix<f64>,
    y: &DMatrix<f64>,
    scores: &impl Scores,
) -> f64 {
    let error = y - (x * beta);

    let rank: Vec<_> = error
        .iter()
        .cloned()
        .collect::<Vec<_>>()
        .ranks()
        .iter()
        .map(|ri| ri / (error.len() + 1) as f64)
        .collect();

    (DMatrix::from_iterator(1, rank.len(), scores.get_scores(&rank).into_iter()) * error)[(0, 0)]
}
