// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use nalgebra::DMatrix;
use strafe_testing::{
    r::{RGenerator, RString, RTester},
    r_assert_relative_equal,
};

use crate::{funcs::dispersion::dispersion, scores::WilcoxScores};

pub fn dispersion_test_inner(seed: u16, num_features: usize, num_rows: usize, fuzz_amount: f64) {
    let xs = RGenerator::new()
        .set_seed(seed + 1)
        .generate_uniform_matrix(num_rows, num_features + 1, (-10, 10));

    let features = RGenerator::new()
        .set_seed(seed + 1)
        .generate_uniform(num_features + 1, (-100, 100));
    let betas = DMatrix::from_iterator(features.len(), 1, features.iter().copied());

    let errors =
        RGenerator::new()
            .set_seed(seed + 2)
            .generate_errors(num_rows, (0, 1), fuzz_amount);

    let ys = (0..num_rows)
        .map(|i| {
            (0..num_features).fold(features[0] + errors[i], |ret, j| {
                ret + (features[j + 1] * xs[(i, j)])
            })
        })
        .collect::<Vec<_>>();
    let y = DMatrix::from_iterator(ys.len(), 1, ys.iter().copied());

    let library = RString::from_library_name("Rfit");

    let r_disp = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};y={};beta={};",
            RString::from_f64_matrix(&xs),
            RString::from_f64_matrix(&y),
            RString::from_f64_matrix(&betas),
        )))
        .set_display(&RString::from_str("disp(beta, x, y, wscores)").unwrap())
        .run()
        .expect("R running error")
        .as_f64()
        .expect("R running error");

    let rust_disp = dispersion(&betas, &xs, &y, &WilcoxScores::new());

    r_assert_relative_equal!(r_disp, rust_disp, 1e-4);
}
