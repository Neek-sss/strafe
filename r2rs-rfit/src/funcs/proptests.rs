// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_w64, strafe_default_proptest_options},
};
use strafe_type::tof64;

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn dispersion_test(
        seed in 0..std::u16::MAX,
        num_features in stat_w64(Some(1.0), Some(5.0), None),
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
        fuzz_amount in stat_w64(None, Some(100.0), None),
    ) {
        dispersion_test_inner(
            seed,
            tof64!(num_features) as usize,
            tof64!(num_rows) as usize,
            tof64!(fuzz_amount)
        );
    }
}
