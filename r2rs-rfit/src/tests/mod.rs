// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)
mod drop;
mod rank_linear_estimator;
mod rank_t_coefficient;
mod robust_r_squared;
mod wald_overall;

pub use self::{
    drop::*, rank_linear_estimator::*, rank_t_coefficient::*, robust_r_squared::*, wald_overall::*,
};

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
