// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::{
    fmt::{Debug, Formatter},
    sync::Arc,
};

use nalgebra::DMatrix;
use r2rs_stats::funcs::predict;
use strafe_trait::{Assumption, Manipulator, StatisticalEstimate, StatisticalEstimator};
use strafe_type::{Alpha64, ModelMatrix};

use crate::rank_fit::prediction::{confidence_interval, prediction_interval};

#[derive(Copy, Clone, Debug)]
pub struct RankLinearEstimator {
    alpha: Alpha64,
}

impl StatisticalEstimator for RankLinearEstimator {
    type Input = (ModelMatrix, DMatrix<f64>, DMatrix<f64>, f64, f64);
    type Output = Result<RankLinearEstimate, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        todo!()
    }

    fn estimate(&self, (x, b, residuals, tauhat, taushat): &Self::Input) -> Self::Output {
        let b_temp = b.clone();
        let x = x.matrix();

        Ok(RankLinearEstimate {
            predictor_estimate: Manipulator::new(Arc::new(move |x: &ModelMatrix| {
                ModelMatrix::from(predict(&x.matrix().insert_column(0, 1.0), &b_temp))
            })),
            predictor_confidence_interval: {
                let (lower, upper) = prediction_interval(
                    &x,
                    &b,
                    self.alpha,
                    *tauhat,
                    *taushat,
                    residuals.as_slice(),
                );
                let mut pi: ModelMatrix = DMatrix::<f64>::from_iterator(
                    x.nrows(),
                    2,
                    lower.into_iter().chain(upper.into_iter()).cloned(),
                )
                .into();
                pi.set_name_index(0, "Lower Prediction Interval");
                pi.set_name_index(1, "Upper Prediction Interval");

                pi
            },
            predictions_estimate: ModelMatrix::from(predict(&x, b)),
            predictions_confidence_interval: {
                let (lower, upper) = confidence_interval(&x, &b, self.alpha, *tauhat, *taushat);
                let mut pi: ModelMatrix = DMatrix::<f64>::from_iterator(
                    x.nrows(),
                    2,
                    lower.into_iter().chain(upper.into_iter()).cloned(),
                )
                .into();
                pi.set_name_index(0, "Lower Confidence Interval");
                pi.set_name_index(1, "Upper Confidence Interval");

                pi
            },
            alpha: self.alpha,
        })
    }
}

impl Default for RankLinearEstimator {
    fn default() -> Self {
        Self { alpha: 0.05.into() }
    }
}

impl RankLinearEstimator {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
            ..self
        }
    }
}

#[derive(Clone)]
pub struct RankLinearEstimate {
    predictor_estimate: Manipulator<ModelMatrix, ModelMatrix>,
    predictor_confidence_interval: ModelMatrix,
    predictions_estimate: ModelMatrix,
    predictions_confidence_interval: ModelMatrix,
    alpha: Alpha64,
}

impl Debug for RankLinearEstimate {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("RankLinearEstimate")
            // .field("predictor_estimate", &self.predictor_estimate)
            .field(
                "predictor_confidence_interval",
                &self.predictor_confidence_interval,
            )
            .field("predictions_estimate", &self.predictions_estimate)
            .field(
                "predictions_confidence_interval",
                &self.predictions_confidence_interval,
            )
            .field("alpha", &self.alpha)
            .finish()
    }
}

impl StatisticalEstimate<Manipulator<ModelMatrix, ModelMatrix>, ModelMatrix>
    for RankLinearEstimate
{
    fn alpha(&self) -> Alpha64 {
        self.alpha
    }

    fn estimate(&self) -> Manipulator<ModelMatrix, ModelMatrix> {
        self.predictor_estimate.clone()
    }

    fn confidence_interval(&self) -> ModelMatrix {
        self.predictor_confidence_interval.clone()
    }
}

impl StatisticalEstimate<ModelMatrix, ModelMatrix> for RankLinearEstimate {
    fn alpha(&self) -> Alpha64 {
        self.alpha
    }

    fn estimate(&self) -> ModelMatrix {
        self.predictions_estimate.clone()
    }

    fn confidence_interval(&self) -> ModelMatrix {
        self.predictions_confidence_interval.clone()
    }
}
