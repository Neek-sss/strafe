// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use strafe_trait::{
    Assumption, Concept, Conclusion, InsignificantCoefficient, SignificantCoefficient,
    StatisticalTest,
};
use strafe_type::ModelMatrix;

use crate::tests::rank_t_coefficient::{base::RankTCoefficientBuilder, response::RankTCoefficient};

impl StatisticalTest for RankTCoefficientBuilder {
    type Input = (ModelMatrix, DMatrix<f64>, f64, f64);
    type Output = Result<Vec<RankTCoefficient>, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        Vec::new()
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![Box::new(InsignificantCoefficient::new())]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![Box::new(SignificantCoefficient::new())]
    }

    fn test(&mut self, input: &Self::Input) -> Self::Output {
        self.build(input)
    }
}
