// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use r2rs_nmath::{distribution::TBuilder, traits::Distribution};
use strafe_type::{Alpha64, FloatConstraint, ModelMatrix};

use crate::{rank_fit::two_way::rfit_vcov, tests::rank_t_coefficient::response::RankTCoefficient};

#[derive(Copy, Clone, Debug)]
pub struct RankTCoefficientBuilder {
    pub(crate) alpha: Alpha64,
}

impl Default for RankTCoefficientBuilder {
    fn default() -> Self {
        Self { alpha: 0.05.into() }
    }
}

impl RankTCoefficientBuilder {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
        }
    }

    pub(crate) fn build(
        &self,
        (x, b, tauhat, taushat): &(ModelMatrix, DMatrix<f64>, f64, f64),
    ) -> Result<Vec<RankTCoefficient>, Box<dyn std::error::Error>> {
        let names = x.column_names();
        let x = x.matrix();
        let n = x.nrows();
        let pp1 = x.ncols();

        let mut standard_errors = rfit_vcov(&x, *tauhat, *taushat).diagonal();
        standard_errors.iter_mut().for_each(|x| *x = x.sqrt());

        let t_stats = b.component_div(&standard_errors);

        let mut t_builder = TBuilder::new();
        t_builder.with_df(n - pp1)?;
        let t = t_builder.build();

        let mut p_vals = t_stats.clone();
        p_vals
            .iter_mut()
            .for_each(|t_stat| *t_stat = 2.0 * t.probability(-t_stat.abs(), true).unwrap());
        let coefs = b
            .iter()
            .zip(t_stats.iter())
            .zip(p_vals.iter())
            .zip(names.into_iter())
            .map(|(((&b, &t), &p), name)| RankTCoefficient {
                name,
                estimate: b,
                alpha: self.alpha,
                confidence_interval: (0.0, 0.0),
                t,
                p,
            })
            .collect::<Vec<_>>();

        Ok(coefs)
    }
}
