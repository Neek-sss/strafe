// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use r2rs_nmath::{distribution::FBuilder, traits::Distribution};
use strafe_trait::{Assumption, Conclusion, RejectionStatus, Statistic, StatisticalTest};
use strafe_type::{Alpha64, FloatConstraint, ModelMatrix, Probability64};

use crate::{funcs::dispersion, scores::Scores};

/// Drop (Reduction) in Dispersion Test
///
/// ## Description:
///
/// Given two full model fits, this function performs a reduction in
/// dispersion test.
///
/// ## Usage:
///
/// drop.test(fitF, fitR = NULL)
///
/// ## Arguments:
///
/// * fitF: An object of class rfit.  The full model fit.
/// * fitR: An object of class rfit.  The reduced model fit.
///
/// ## Details:
///
/// Rank-based inference procedure analogous to the traditional (LS)
/// reduced model test.
///
/// The full and reduced model dispersions are calculated. The
/// reduction in dispersion test, or drop test for short, has an
/// asymptotic chi-sq distribution. Simulation studies suggest using F
/// critical values. The p-value returned is based on a F-distribution
/// with df1 and df2 degrees of freedom where df1 is the difference in
/// the number of parameters in the fits of fitF and fitR and df2 is
/// the residual degrees of freedom in the fit fitF.
///
/// Both fits are based on a minimization routine. It is possible that
/// resulting solutions are such that the fitF$disp > fitRdisp. We
/// recommend starting the full model at the reduced model fit as a
/// way to avoid this situation. See examples.
///
/// ## Value:
///
/// * F: Value of the F test statistic
/// * p.value: The observed significance level of the test (using an F
/// quantile)
/// * RD: Reduced model dispersion minus Full model dispersion
/// * tauhat: Estimate of the scale parameter (using the full model
/// residuals)
/// * df1: numerator degrees of freedom
/// * df2: denominator degrees of freedom
///
/// ## Author(s):
///
/// John Kloke, Joseph McKean
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## See Also:
///
/// ‘rfit’
///
/// Examples:
///
/// ```r
/// y<-rnorm(47)
/// x1<-rnorm(47)
/// x2<-rnorm(47)
/// fitF<-rfit(y~x1+x2)
/// fitR<-rfit(y~x1)
/// drop.test(fitF,fitR)
///
/// ## try starting the full model at the reduced model fit
/// fitF<-rfit(y~x1+x2,yhat0=fitR$fitted)
/// drop.test(fitF,fitR)
/// ```
#[derive(Copy, Clone, Debug)]
pub struct DropInDispersionTest<S: Scores> {
    alpha: Alpha64,
    scores1: S,
    scores2: S,
}

impl<S: Scores> StatisticalTest for DropInDispersionTest<S> {
    type Input = (
        ModelMatrix,
        DMatrix<f64>,
        DMatrix<f64>,
        f64,
        Option<(DMatrix<f64>, DMatrix<f64>, DMatrix<f64>)>,
    );
    type Output = Result<DropInDispersionStatistic, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        Vec::new()
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![]
    }

    fn test(&mut self, (x1, y1, b1, tauhat, fit2): &Self::Input) -> Self::Output {
        let x1 = x1.matrix();
        let pp1 = b1.nrows();

        let (rd, df1) = if let Some((x2, y2, b2)) = fit2 {
            // Compare the two fits
            let d1 = dispersion(b1, &x1, y1, &self.scores1);
            let d2 = dispersion(b2, x2, y2, &self.scores2);
            (d2 - d1, b1.nrows() - b2.nrows())
        } else {
            // Compare fit to null betas
            let mut b0 = b1.clone();
            b0.iter_mut().for_each(|b| *b = 0.0);
            let d0 = dispersion(&b0, &x1, y1, &self.scores1);
            let d1 = dispersion(b1, &x1, y1, &self.scores1);
            (d0 - d1, pp1 - 1)
        };

        let df2 = y1.nrows() - pp1;

        let test = (rd / df1 as f64) / (tauhat / 2.0);

        let mut f_builder = FBuilder::new();
        f_builder.with_df1(df1);
        f_builder.with_df2(df2);
        let f = f_builder.build();

        let p_val = 1.0 - f.probability(test, true).unwrap();

        Ok(DropInDispersionStatistic {
            test,
            p: p_val.into(),
            alpha: self.alpha,
        })
    }
}

#[derive(Clone, Debug)]
pub struct DropInDispersionStatistic {
    test: f64,
    p: Probability64,
    alpha: Alpha64,
}

impl Statistic for DropInDispersionStatistic {
    fn alpha(&self) -> Alpha64 {
        self.alpha
    }

    fn statistic(&self) -> f64 {
        self.test
    }

    fn probability_value(&self) -> f64 {
        self.p.unwrap()
    }

    fn conclusion(&self) -> RejectionStatus {
        unimplemented!()
    }
}

impl<S: Scores> Default for DropInDispersionTest<S> {
    fn default() -> Self {
        Self {
            alpha: 0.05.into(),
            scores1: S::default(),
            scores2: S::default(),
        }
    }
}

impl<S: Scores> DropInDispersionTest<S> {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
            ..self
        }
    }

    pub fn with_fit1_scores(self, scores: &S) -> Self {
        Self {
            scores1: scores.clone(),
            ..self
        }
    }

    pub fn with_fit2_scores(self, scores: &S) -> Self {
        Self {
            scores2: scores.clone(),
            ..self
        }
    }
}
