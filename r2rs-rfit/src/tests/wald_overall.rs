// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::fmt::{Debug, Formatter};

use nalgebra::DMatrix;
use r2rs_nmath::{distribution::FBuilder, traits::Distribution};
use strafe_trait::{Assumption, Conclusion, RejectionStatus, Statistic, StatisticalTest};
use strafe_type::{Alpha64, FloatConstraint, ModelMatrix};

use crate::rank_fit::two_way::func::rfit_vcov;

/// Overall Wald test
///
/// ## Description:
///
/// Conducts a Wald test of all regression parameters are zero
///
/// ## Usage:
///
/// wald.test.overall(fit)
///
/// ## Arguments:
///
/// * fit: result from a rfit
///
/// ## Author(s):
///
/// John Kloke
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## Examples:
///
/// ```r
/// x <- rnorm(47)
/// y <- rnorm(47)
/// wald.test.overall(rfit(y~x))
/// ```
#[derive(Copy, Clone, Debug)]
pub struct WaldOverallTest {
    alpha: Alpha64,
}

impl StatisticalTest for WaldOverallTest {
    type Input = (ModelMatrix, DMatrix<f64>, f64, f64);
    type Output = Result<WaldOverallStatistic, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        Vec::new()
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![]
    }

    fn test(&mut self, (x, b, tauhat, taushat): &Self::Input) -> Self::Output {
        let x = x.matrix();
        let p = b.nrows();
        let beta_hat = b.clone().remove_row(0);
        let v = rfit_vcov(&x, *tauhat, *taushat)
            .remove_row(0)
            .remove_column(0);

        let df1 = p - 1;
        let df2 = x.nrows() - b.nrows();

        let test_stat =
            (beta_hat.transpose() * v.try_inverse().unwrap() * beta_hat / (df1 as f64))[(0, 0)];

        let mut f_builder = FBuilder::new();
        f_builder.with_df1(df1);
        f_builder.with_df2(df2);
        let f = f_builder.build();

        let p_val = f.probability(test_stat, false).unwrap();

        Ok(WaldOverallStatistic {
            wald: test_stat,
            p: p_val,
            alpha: self.alpha,
        })
    }
}

#[derive(Clone)]
pub struct WaldOverallStatistic {
    alpha: Alpha64,
    wald: f64,
    p: f64,
}

impl Debug for WaldOverallStatistic {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("WaldOverallStatistic")
            .field("alpha", &self.alpha)
            .field("wald", &self.wald)
            .field("p", &self.p)
            .finish()
    }
}

impl Statistic for WaldOverallStatistic {
    fn alpha(&self) -> Alpha64 {
        self.alpha
    }

    fn statistic(&self) -> f64 {
        self.wald
    }

    fn probability_value(&self) -> f64 {
        self.p
    }

    fn conclusion(&self) -> RejectionStatus {
        unimplemented!()
    }
}

impl Default for WaldOverallTest {
    fn default() -> Self {
        Self { alpha: 0.05.into() }
    }
}

impl WaldOverallTest {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
        }
    }
}
