// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use nalgebra::DMatrix;
use num_traits::ToPrimitive;
use r2rs_nmath::rng::MarsagliaMulticarry;
use strafe_testing::{
    r::{RGenerator, RString, RTester},
    r_assert_relative_equal_result,
};
use strafe_trait::{Model, ModelBuilder, Statistic, StatisticalTest};
use strafe_type::ModelMatrix;

use crate::{
    rank_fit::two_way::RankFitBuilder, scores::WilcoxScores, tests::drop::DropInDispersionTest,
};

pub fn drop_test_inner(seed: u16, num_features: f64, num_rows: f64, fuzz_amount: f64) {
    let num_features = num_features.to_usize().unwrap();
    let num_rows = num_rows.to_usize().unwrap();
    let fuzz_amount = fuzz_amount.to_usize().unwrap();

    let xs_1 =
        RGenerator::new()
            .set_seed(seed)
            .generate_uniform_matrix(num_rows, num_features, (-10, 10));

    let xs_2 = RGenerator::new().set_seed(seed).generate_uniform_matrix(
        num_rows,
        num_features + 1,
        (-10, 10),
    );

    let features = RGenerator::new()
        .set_seed(seed + 1)
        .generate_uniform(num_features + 1, (-100, 100));

    let errors =
        RGenerator::new()
            .set_seed(seed + 2)
            .generate_errors(num_rows, (0, 1), fuzz_amount);

    let ys = (0..num_rows)
        .map(|i| {
            (0..num_features).fold(features[0] + errors[i], |ret, j| {
                ret + (features[j + 1] * xs_1[(i, j)])
            })
        })
        .collect::<Vec<_>>();

    let y = DMatrix::from_iterator(ys.len(), 1, ys.iter().copied());

    let library = RString::from_library_name("Rfit");

    let rust_regression_1 = RankFitBuilder::<WilcoxScores, MarsagliaMulticarry>::new()
        .with_bootstrap_coefficient_ci(false)
        .with_x(&ModelMatrix::from(&xs_1))
        .with_y(&ModelMatrix::from(&y))
        .build();

    let b_1 = DMatrix::from_iterator(
        rust_regression_1.parameters().unwrap().len(),
        1,
        rust_regression_1
            .parameters()
            .unwrap()
            .iter()
            .map(|c| c.estimate()),
    );

    let rust_regression_2 = RankFitBuilder::<WilcoxScores, MarsagliaMulticarry>::new()
        .with_bootstrap_coefficient_ci(false)
        .with_x(&ModelMatrix::from(&xs_2))
        .with_y(&ModelMatrix::from(&y))
        .build();

    let b_2 = DMatrix::from_iterator(
        rust_regression_2.parameters().unwrap().len(),
        1,
        rust_regression_2
            .parameters()
            .unwrap()
            .iter()
            .map(|c| c.estimate()),
    );

    let r_slice = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x1={};x2={};",
            RString::from_f64_slice(&ys),
            RString::from_f64_matrix(&xs_1),
            RString::from_f64_matrix(&xs_2),
        )))
        .set_display(
            &RString::from_str("drop.test(rfit(y~x2, TAU='R'), rfit(y~x1, TAU='R'))").unwrap(),
        )
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("Error running R");

    let drop_ret = DropInDispersionTest::<WilcoxScores>::new()
        .test(&(
            ModelMatrix::from(xs_2.clone().insert_column(0, 1.0)),
            y.clone(),
            b_2.clone(),
            rust_regression_2.tauhat,
            Some((xs_1.clone().insert_column(0, 1.0), y, b_1)),
        ))
        .expect("Error in test");

    r_assert_relative_equal_result!(Ok(r_slice[0]), &drop_ret.statistic(), 1e-4);

    r_assert_relative_equal_result!(Ok(r_slice[1]), &drop_ret.probability_value(), 1e-4);
}
