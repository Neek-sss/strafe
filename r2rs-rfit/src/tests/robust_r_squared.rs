// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use strafe_trait::{
    AccurateModel, Assumption, Concept, Conclusion, InaccurateModel, RejectionStatus, Statistic,
    StatisticalTest,
};
use strafe_type::{Alpha64, FloatConstraint, ModelMatrix};

use crate::{scores::Scores, tests::drop::DropInDispersionTest};

#[derive(Copy, Clone, Debug)]
pub struct LenientRobustRSquaredTest<S: Scores> {
    alpha: Alpha64,
    scores: S,
}

impl<S: Scores> StatisticalTest for LenientRobustRSquaredTest<S> {
    type Input = (ModelMatrix, DMatrix<f64>, DMatrix<f64>, f64);
    type Output = Result<LenientRobustRSquaredStatistic, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        Vec::new()
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![Box::new(InaccurateModel::new())]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![Box::new(AccurateModel::new())]
    }

    fn test(&mut self, (x, y, b, tauhat): &Self::Input) -> Self::Output {
        let drop = DropInDispersionTest::<S>::new()
            .with_fit1_scores(&self.scores)
            .test(&(x.clone(), y.clone(), b.clone(), *tauhat, None))?;

        let x = x.matrix();

        let df1 = (x.ncols() - 1) as f64;
        let df2 = (y.nrows() - x.ncols()) as f64;
        let r_2 = (df1 / df2 * drop.statistic()) / (1.0 + df1 / df2 * drop.statistic());

        Ok(LenientRobustRSquaredStatistic {
            r_2,
            p: 1.0 - r_2,
            alpha: (self.alpha.unwrap() * 3.0).into(),
        })
    }
}

#[derive(Clone, Debug)]
pub struct LenientRobustRSquaredStatistic {
    r_2: f64,
    alpha: Alpha64,
    p: f64,
}

impl Statistic for LenientRobustRSquaredStatistic {
    fn alpha(&self) -> Alpha64 {
        self.alpha
    }

    fn statistic(&self) -> f64 {
        self.r_2
    }

    fn probability_value(&self) -> f64 {
        self.p
    }

    fn conclusion(&self) -> RejectionStatus {
        if self.p < self.alpha.unwrap() {
            RejectionStatus::RejectInFavorOf(Box::new(AccurateModel {}))
        } else {
            RejectionStatus::FailToReject(Box::new(InaccurateModel {}))
        }
    }
}

impl<S: Scores> Default for LenientRobustRSquaredTest<S> {
    fn default() -> Self {
        Self {
            alpha: 0.05.into(),
            scores: S::default(),
        }
    }
}

impl<S: Scores> LenientRobustRSquaredTest<S> {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
            ..self
        }
    }

    pub fn with_scores(self, scores: &S) -> Self {
        Self {
            scores: scores.clone(),
            ..self
        }
    }
}
