use std::io::Cursor;

use polars::prelude::*;

/// # Baseball Card Data
///
/// ## Description:
///
///  These data come from the back-side of 59 baseball cards that
///  Carrie had.
///
/// ## Usage:
///
/// data(baseball)
///      
/// ## Format:
///
/// A data frame with 59 observations on the following 6 variables.
///
/// * ‘height’ Height in inches
/// * ‘weight’ Weight in pounds
/// * ‘bat’ a factor with levels ‘L’ ‘R’ ‘S’
/// * ‘throw’ a factor with levels ‘L’ ‘R’
/// * ‘field’ a factor with levels ‘0’ ‘1’
/// * ‘average’ ERA if the player is a pitcher and his batting average
///     if the player is a fielder
///
/// ## Source:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## Examples:
///
/// ```r
/// data(baseball)
/// wilcox.test(height~field,data=baseball)
/// rfit(weight~height,data=baseball)
/// ```
pub fn baseball() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("baseball.csv"))).finish()
}

/// # Baseball Salaries
///
/// ## Description:
///
/// Salaries of 176 professional baseball players for the 1987 season.
///
/// ## Usage:
///
/// data(bbsalaries)
///      
/// ## Format:
///
/// A data frame with 176 observations on the following 8 variables.
///
/// * ‘logYears’ Log of the number of years experience
/// * ‘aveWins’ Average wins per year
/// * ‘aveLosses’ Average losses per year
/// * ‘era’ Earned Run Average
/// * ‘aveGames’ Average games pitched in per year
/// * ‘aveInnings’ Average number of innings pitched per year
/// * ‘aveSaves’ Average number of saves per year
/// * ‘logSalary’ Log of the base salary in dollars
///
/// ## Source:
///
/// <http://lib.stat.cmu.edu/datasets/baseball.data>
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## Examples:
///
/// ```r
/// data(bbsalaries)
/// summary(rfit(logSalary~logYears+aveWins+aveLosses+era+aveGames+aveInnings+aveSaves,data=bbsalaries))
/// ```
pub fn bbsalaries() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("bbsalaries.csv"))).finish()
}

/// # Box and Cox (1964) data.
///
/// ## Description:
///
/// The data are the results of a 3 * 4 two-way design, where
/// forty-eight animals were exposed to three different poisons and
/// four different treatments. The design is balanced with four
/// replications per cell. The response was the log survival time of
/// the animal.
///
/// ## Usage:
///
/// data(BoxCox)
///      
/// ## Format:
///
/// A data frame with 48 observations on the following 3 variables.
///
/// * ‘logSurv’ log Survival Time
/// * ‘Poison’ a factor indicating poison level
/// * ‘Treatment’ a factor indicating treatment level
///
/// ## Source:
///
/// Box, G.E.P. and Cox, D.R. (1964), An analysis of transformations,
/// _ Journal of the Royal Statistical Society, Series B,
/// Methodological_, 26, 211-252.
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## Examples:
///
/// ```r
/// data(BoxCox)
/// with(BoxCox,interaction.plot(Treatment,Poison,logSurv,median))
/// raov(logSurv~Poison+Treatment,data=BoxCox)
/// ```
pub fn boxcox() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("BoxCox.csv"))).finish()
}

/// # Cardiovascular risk factors
///
/// ## Description:
///
/// Data from a study to investigate assocation between uric acid and
/// various cardiovascular risk factors in developing countries
/// (Heritier et. al. 2009). There are 474 men and 524 women aged
/// 25-64.
///
/// ## Usage:
///
/// data(CardioRiskFactors)
///      
/// ## Format:
///
/// A data frame with 998 observations on the following 14 variables.
///
/// * ‘age’ Age of subject
/// * ‘bmi’ Body Mass Index
/// * ‘waisthip’ waist/hip ratio(?)
/// * ‘smok’ indicator for regular smoker
/// * ‘choles’ total cholesterol
/// * ‘trig’ triglycerides level in body fat
/// * ‘hdl’ high-density lipoprotien(?)
/// * ‘ldl’ low-density lipoprotein
/// * ‘sys’ systolic blood pressure
/// * ‘dia’ diastolic blood pressure(?)
/// * ‘Uric’ serum uric
/// * ‘sex’ indicator for male
/// * ‘alco’ alcohol intake (mL/day)
/// * ‘apoa’ apoprotein A
///
/// ## Details:
///
/// Data set and description taken from Heritier et. al. (2009) (c.f.
/// Conen et. al. 2004).
///
/// ## Source:
///
/// Heritier, S., Cantoni, E., Copt, S., and Victoria-Feser, M.
/// (2009), _Robust Methods in Biostatistics_, New York: John Wiley
/// and Sons.
///
/// Conen, D., Wietlisbach, V., Bovet, P., Shamlaye, C., Riesen, W.,
/// Paccaud, F., and Burnier, M. (2004), Prevalence of hyperuricemia
/// and relation of serum uric acid with cardiovascular risk factors
/// in a developing country. _BMC Public Health_.
///
/// ## Examples:
///
/// ```r
/// data(CardioRiskFactors)
/// fitF<-rfit(Uric~bmi+sys+choles+ldl+sex+smok+alco+apoa+trig+age,data=CardioRiskFactors)
/// fitR<-rfit(Uric~bmi+sys+choles+ldl+sex,data=CardioRiskFactors)
/// drop.test(fitF,fitR)
/// summary(fitR)
/// ```
pub fn cardioriskfactors() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("CardioRiskFactors.csv"))).finish()
}

/// # Free Fatty Acid Data
///
/// ## Description:
///
/// The response variable is level of free fatty acid in a sample of
/// prepubescent boys. The explanatory variables are age (in months),
/// weight (in lbs), and skin fold thickness.
///
/// ## Usage:
///
/// data(ffa)
///      
/// ## Format:
///
/// A data frame with 41 rows and 4 columns.
///
/// * ‘age’ age in years
/// * ‘weight’ weight in lbs
/// * ‘skin’ skin fold thinkness
/// * ‘ffa’ free fatty acid
///
/// ## Source:
///
/// Morrison, D.F. (1983), _Applied Linear Statistical Models_,
/// Englewood Cliffs, NJ:Prentice Hall.
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## Examples:
///
/// ```r
/// data(ffa)
/// summary(rfit(ffa~age+weight+skin,data=ffa))  #using the default (Wilcoxon scores)
/// summary(rfit(ffa~age+weight+skin,data=ffa,scores=bentscores1))
/// ```
pub fn ffa() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("ffa.csv"))).finish()
}

/// # Quail Data
///
/// ## Description:
///
/// Thirty-nine quail were randomized to one of for treatments for
/// lowering cholesterol.
///
/// ## Usage:
///
/// data(quail)
///      
/// ## Format:
///
/// A data frame with 39 observations on the following 2 variables.
///
/// * ‘treat’ a factor with levels ‘1’ ‘2’ ‘3’ ‘4’
/// * ‘ldl’ a numeric vector
///
/// ## Source:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## Examples:
///
/// ```r
/// data(quail)
/// boxplot(ldl~treat,data=quail)
/// ```
pub fn quail() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("quail.csv"))).finish()
}

/// # Serum Level of luteinizing hormone (LH)
///
/// ## Description:
///
/// Hollander and Wolfe (1999) discuss a 2 by 5 factorial design for a
/// study to determine the effect of light on the release of
/// luteinizing hormone (LH). The factors in the design are: light
/// regimes at two levels (constant light and 14 hours of light
/// followed by 10 hours of darkness) and a luteinizing release factor
/// (LRF) at 5 different dosage levels. The response is the level of
/// luteinizing hormone (LH), nanograms per ml of serum in blood
/// samples. Sixty rats were put on test under these 10 treatment
/// combinations, six rats per combination.
///
/// ## Usage:
///
/// data(serumLH)
///      
/// ## Format:
///
/// A data frame with 60 observations on the following 3 variables.
///
/// * ‘serum’ a numeric vector
/// * ‘light.regime’ a factor with levels ‘Constant’ ‘Intermittent’
/// * ‘LRF.dose’ a factor with levels ‘0’ ‘10’ ‘1250’ ‘250’ ‘50’
///
/// ## Source:
///
/// Hollander, M. and Wolfe, D.A. (1999), _Nonparametric Statistical
/// Methods_, New York: Wiley.
///
/// ## References:
///
/// Hollander, M. and Wolfe, D.A. (1999), _Nonparametric Statistical
/// Methods_, New York: Wiley.
///
/// ## Examples:
///
/// ```r
/// data(serumLH)
/// raov(serum~light.regime + LRF.dose + light.regime*LRF.dose, data = serumLH)
/// ```
pub fn serumlh() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("serumLH.csv"))).finish()
}

/// # Telephone Data
///
/// ## Description:
///
/// The number of telephone calls (in tens of millions) made in
/// Belgium from 1950-1973.
///
/// ## Usage:
///
/// data(telephone)
///      
/// ## Format:
///
/// A data frame with 24 observations on the following 2 variables.
///
/// * ‘year’ years since 1950 AD
/// * ‘calls’ number of telephone calls in tens of millions
///
/// ## Source:
///
/// Rousseeuw, P.J. and Leroy, A.M. (1987), _Robust Regression and
/// Outlier Detection_, New York: Wiley.
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## Examples:
///
/// ```r
/// data(telephone)
/// plot(telephone)
/// abline(rfit(calls~year,data=telephone))
/// ```
pub fn telephone() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("telephone.csv"))).finish()
}
