// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::scores::Scores;

#[derive(Copy, Clone, Debug)]
pub struct HeavyTailScores {
    pub lower_tail_barrier: f64,
    pub lower_tail_value: f64,
    pub upper_tail_barrier: f64,
    pub upper_tail_value: f64,
}

impl HeavyTailScores {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for HeavyTailScores {
    fn default() -> Self {
        Self {
            lower_tail_barrier: 0.25,
            lower_tail_value: -1.0,
            upper_tail_barrier: 0.75,
            upper_tail_value: 1.0,
        }
    }
}

impl Scores for HeavyTailScores {
    fn phi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if ui < self.lower_tail_barrier {
                    self.lower_tail_value
                } else if ui > self.upper_tail_barrier {
                    self.upper_tail_value
                } else {
                    (self.upper_tail_value - self.lower_tail_value)
                        / (self.upper_tail_barrier - self.lower_tail_barrier)
                        * (ui - self.lower_tail_barrier)
                        + self.lower_tail_value
                }
            })
            .collect()
    }

    fn dphi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if ui < self.lower_tail_barrier || ui > self.upper_tail_barrier {
                    0.0
                } else {
                    (self.upper_tail_value - self.lower_tail_value)
                        / (self.upper_tail_barrier - self.lower_tail_barrier)
                }
            })
            .collect()
    }
}
