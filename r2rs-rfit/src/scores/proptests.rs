// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_w64, strafe_default_proptest_options},
};
use strafe_type::tof64;

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn heavy_tail_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        scores_test_inner(seed, tof64!(num_rows), ScoreOptions::HeavyTail);
    }

    #[test]
    fn left_skew_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        scores_test_inner(seed, tof64!(num_rows), ScoreOptions::LeftSkew);
    }

    #[test]
    fn light_tail_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        scores_test_inner(seed, tof64!(num_rows), ScoreOptions::LightTail);
    }

    #[test]
    fn log_gf_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        scores_test_inner(seed, tof64!(num_rows), ScoreOptions::LogGF);
    }

    #[test]
    fn logrank_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        scores_test_inner(seed, tof64!(num_rows), ScoreOptions::LogRank);
    }

    #[test]
    fn normal_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        scores_test_inner(seed, tof64!(num_rows), ScoreOptions::Normal);
    }

    #[test]
    fn right_skew_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        scores_test_inner(seed, tof64!(num_rows), ScoreOptions::RightSkew);
    }

    #[test]
    fn wilcox_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        scores_test_inner(seed, tof64!(num_rows), ScoreOptions::Wilcox);
    }
}
