// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::scores::Scores;

#[derive(Copy, Clone, Debug)]
pub struct LightTailScores {
    pub s1: f64,
    pub s2: f64,
    pub s3: f64,
    pub s4: f64,
    pub s5: f64,
}

impl LightTailScores {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for LightTailScores {
    fn default() -> Self {
        Self {
            s1: 0.25,
            s2: 0.75,
            s3: -1.0,
            s4: 1.0,
            s5: 0.0,
        }
    }
}

impl Scores for LightTailScores {
    fn phi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if ui < self.s1 {
                    (self.s5 - self.s3) / self.s1 * ui + self.s3
                } else if ui > self.s2 {
                    (self.s4 - self.s5) / (1.0 - self.s2) * (ui - self.s2) + self.s5
                } else {
                    self.s5
                }
            })
            .collect()
    }

    fn dphi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if ui < self.s1 {
                    (self.s5 - self.s3) / self.s1
                } else if ui > self.s2 {
                    (self.s4 - self.s5) / (1.0 - self.s2)
                } else {
                    self.s5
                }
            })
            .collect()
    }
}
