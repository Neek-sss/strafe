// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;
use r2rs_nmath::{distribution::NormalBuilder, traits::Distribution};
use strafe_type::FloatConstraint;

use crate::scores::Scores;

#[derive(Copy, Clone, Debug, Default)]
pub struct NormalScores {}

impl NormalScores {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Scores for NormalScores {
    fn phi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if (0.0..=1.0).contains(&ui) {
                    let normal = NormalBuilder::new().build();
                    normal.quantile(ui, true).unwrap()
                } else {
                    f64::nan()
                }
            })
            .collect()
    }

    fn dphi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if (0.0..=1.0).contains(&ui) {
                    let normal = NormalBuilder::new().build();
                    1.0 / normal.density(normal.quantile(ui, true)).unwrap()
                } else {
                    f64::nan()
                }
            })
            .collect()
    }
}
