//! All Scores
//!
//! ## Description:
//!
//! An object of class scores which includes the score function and
//! it's derivative for rank-based regression inference.
//!
//! ## Usage:
//!
//! data(wscores)
//!
//! ## Format:
//!
//! The format is: Formal class 'scores' \[package ".GlobalEnv"\] with 2
//! slots ..@ phi :function (u) ..@ Dphi:function (u)
//!
//! ## Details:
//!
//! Using Wilcoxon (linear) scores leads to inference which has ARE of
//! 0.955 to least squares (ML) when the data are normal. Wilcoxon
//! scores are optimal when the underlying error distribution is
//! logistic. Normal scores are optimal when the data are normally
//! distributed. Log-rank scores are optimal when the data are from an
//! exponential distribution, e.g. in a proportional hazards model.
//! Log-Generalized F scores can also be used in the analysis of
//! survival data (see Hettmansperger and McKean p. 233).
//!
//! bentscores1 are recommended for right-skewed distributions.
//! bentscores2 are recommended for light-tailed distributions.
//! bentscores3 are recommended for left-skewed distributions.
//! bentscores4 are recommended for heavy-tailed distributions.
//!
//! ## References:
//!
//! Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
//! Statistical Methods, 2nd ed._, New York: Chapman-Hall.
//!
//! ## Examples:
//!
//! ```r
//! u <- seq(0.01,0.99,by=0.01)
//! plot(u,getScores(wscores,u),type='l',main='Wilcoxon Scores')
//! plot(u,getScores(nscores,u),type='l',main='Normal Scores')
//!
//! data(wscores)
//! x<-runif(50)
//! y<-rlogis(50)
//! rfit(y~x,scores=wscores)
//!
//! x<-rnorm(50)
//! y<-rnorm(50)
//! rfit(y~x,scores=nscores)
//! ```

use std::fmt::Debug;

use r2rs_base::traits::StatisticalSlice;

mod heavy_tail;
mod left_skew;
mod light_tail;
mod log_gf;
mod logrank;
mod normal;
mod right_skew;
mod wilcox;

pub use self::{
    heavy_tail::HeavyTailScores, left_skew::LeftSkewScores, light_tail::LightTailScores,
    log_gf::LogGFScores, logrank::LogRankScores, normal::NormalScores, right_skew::RightSkewScores,
    wilcox::WilcoxScores,
};

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;

/// Class "scores"
///
/// ## Description:
///
/// A score function and it's corresponding derivative is required for
/// rank-based estimation.This object puts them together.
///
/// ## Objects from the Class:
///
/// Objects can be created by calls of the form ‘new("scores", ...)’.
///
/// ## Slots:
///
/// * ‘phi’: Object of class ‘"function"’ the score function
///
/// * ‘Dphi’: Object of class ‘"function"’ the first derivative of the
///score function
///
/// * ‘param’: Object of class ‘"param"’
///
/// ## Author(s):
///
/// John Kloke
///
/// ## References:
///
/// Hettmansperger, T.P. and McKean J.W. (2011), _Robust Nonparametric
/// Statistical Methods, 2nd ed._, New York: Chapman-Hall.
///
/// ## See Also:
///
/// ‘param’
///
/// ## Examples:
///
/// ```r
/// showClass("scores")
/// ```
pub trait Scores: Debug + Clone + Default {
    fn phi(&self, x: &[f64]) -> Vec<f64>;
    fn dphi(&self, x: &[f64]) -> Vec<f64>;
    fn get_scores(&self, x: &[f64]) -> Vec<f64> {
        let a = self.phi(x);

        let ac: Vec<f64> = a.iter().map(|ai| ai - a.mean()).collect();
        let sigma: f64 = ac.iter().map(|aci| aci.powi(2)).sum::<f64>() / ((ac.len() + 1) as f64);
        ac.iter().map(|aci| aci / sigma.sqrt()).collect()
    }
    fn get_scores_deriv(&self, x: &[f64]) -> Vec<f64> {
        let ap = self.dphi(x);
        let a = self.phi(x);

        let ac: Vec<f64> = a.iter().map(|ai| ai - a.mean()).collect();
        let sigma: f64 = ac.iter().map(|aci| aci.powi(2)).sum::<f64>() / ((ac.len() + 1) as f64);
        ap.iter().map(|api| api / sigma.sqrt()).collect()
    }
}
