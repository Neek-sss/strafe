// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::scores::Scores;

#[derive(Copy, Clone, Debug, Default)]
pub struct LogRankScores {}

impl LogRankScores {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Scores for LogRankScores {
    fn phi(&self, x: &[f64]) -> Vec<f64> {
        x.iter().map(|ui| -1.0 - (1.0 - ui).ln()).collect()
    }

    fn dphi(&self, x: &[f64]) -> Vec<f64> {
        x.iter().map(|ui| 1.0 / (1.0 - ui)).collect()
    }
}
