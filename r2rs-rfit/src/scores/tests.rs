// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal,
};

use crate::scores::{
    heavy_tail::HeavyTailScores, left_skew::LeftSkewScores, light_tail::LightTailScores,
    log_gf::LogGFScores, logrank::LogRankScores, normal::NormalScores, right_skew::RightSkewScores,
    wilcox::WilcoxScores, Scores,
};

pub enum ScoreOptions {
    HeavyTail,
    LeftSkew,
    LightTail,
    LogGF,
    LogRank,
    Normal,
    RightSkew,
    Wilcox,
}

impl ScoreOptions {
    fn to_phi_string(&self) -> String {
        match self {
            ScoreOptions::HeavyTail => "bentscores4@phi(x, bentscores4@param)".to_string(),
            ScoreOptions::LeftSkew => "bentscores3@phi(x, bentscores3@param)".to_string(),
            ScoreOptions::LightTail => "bentscores2@phi(x, bentscores2@param)".to_string(),
            ScoreOptions::LogGF => "logGFscores@phi(x, logGFscores@param)".to_string(),
            ScoreOptions::LogRank => "logrank.scores@phi(x)".to_string(),
            ScoreOptions::Normal => "nscores@phi(x)".to_string(),
            ScoreOptions::RightSkew => "bentscores1@phi(x, bentscores1@param)".to_string(),
            ScoreOptions::Wilcox => "wscores@phi(x)".to_string(),
        }
    }

    fn to_dphi_string(&self) -> String {
        match self {
            ScoreOptions::HeavyTail => "bentscores4@Dphi(x, bentscores4@param)".to_string(),
            ScoreOptions::LeftSkew => "bentscores3@Dphi(x, bentscores3@param)".to_string(),
            ScoreOptions::LightTail => "bentscores2@Dphi(x, bentscores2@param)".to_string(),
            ScoreOptions::LogGF => "logGFscores@Dphi(x, logGFscores@param)".to_string(),
            ScoreOptions::LogRank => "logrank.scores@Dphi(x)".to_string(),
            ScoreOptions::Normal => "nscores@Dphi(x)".to_string(),
            ScoreOptions::RightSkew => "bentscores1@Dphi(x, bentscores1@param)".to_string(),
            ScoreOptions::Wilcox => "wscores@Dphi(x)".to_string(),
        }
    }

    fn phi(&self, x: &[f64]) -> Vec<f64> {
        match self {
            ScoreOptions::HeavyTail => HeavyTailScores::new().phi(x),
            ScoreOptions::LeftSkew => LeftSkewScores::new().phi(x),
            ScoreOptions::LightTail => LightTailScores::new().phi(x),
            ScoreOptions::LogGF => LogGFScores::new().phi(x),
            ScoreOptions::LogRank => LogRankScores::new().phi(x),
            ScoreOptions::Normal => NormalScores::new().phi(x),
            ScoreOptions::RightSkew => RightSkewScores::new().phi(x),
            ScoreOptions::Wilcox => WilcoxScores::new().phi(x),
        }
    }

    fn dphi(&self, x: &[f64]) -> Vec<f64> {
        match self {
            ScoreOptions::HeavyTail => HeavyTailScores::new().dphi(x),
            ScoreOptions::LeftSkew => LeftSkewScores::new().dphi(x),
            ScoreOptions::LightTail => LightTailScores::new().dphi(x),
            ScoreOptions::LogGF => LogGFScores::new().dphi(x),
            ScoreOptions::LogRank => LogRankScores::new().dphi(x),
            ScoreOptions::Normal => NormalScores::new().dphi(x),
            ScoreOptions::RightSkew => RightSkewScores::new().dphi(x),
            ScoreOptions::Wilcox => WilcoxScores::new().dphi(x),
        }
    }
}

pub fn scores_test_inner(seed: u16, num_rows: f64, min: f64, max: f64, score_option: ScoreOptions) {
    let num_rows = num_rows - (num_rows % 2.0);
    let xs = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_string(format!(
            "c(runif({0}/2, {1}, {2}), rnorm({0}/2, 0, {2}))",
            num_rows, min, max
        )))
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R array error");

    let library = RString::from_library_name("Rfit");

    let r_phi = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_slice(&xs),
        )))
        .set_display(&RString::from_str(&score_option.to_phi_string()).unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");

    let rust_phi = score_option.phi(&xs);

    for (&r_val, &rust_val) in r_phi.iter().zip(rust_phi.iter()) {
        r_assert_relative_equal!(r_val, rust_val, 1e-4);
    }

    let r_dphi = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_slice(&xs),
        )))
        .set_display(&RString::from_str(&score_option.to_dphi_string()).unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");

    let rust_dphi = score_option.dphi(&xs);

    for (&r_val, &rust_val) in r_dphi.iter().zip(rust_dphi.iter()) {
        r_assert_relative_equal!(r_val, rust_val, 1e-4);
    }
}
