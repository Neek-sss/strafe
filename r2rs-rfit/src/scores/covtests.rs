// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn heavy_tail_test_1() {
    scores_test_inner(1, 12.0, -10.0, 10.0, ScoreOptions::HeavyTail);
}

#[test]
fn heavy_tail_test_2() {
    scores_test_inner(1, 12.0, 0.0, 1.0, ScoreOptions::HeavyTail);
}

#[test]
fn left_skew_test() {
    scores_test_inner(1, 12.0, -10.0, 10.0, ScoreOptions::LeftSkew);
}

#[test]
fn light_tail_test_1() {
    scores_test_inner(1, 12.0, -10.0, 10.0, ScoreOptions::LightTail);
}

#[test]
fn light_tail_test_2() {
    scores_test_inner(1, 12.0, 0.0, 1.0, ScoreOptions::LightTail);
}

#[test]
fn log_gf_test_1() {
    scores_test_inner(1, 12.0, -10.0, 10.0, ScoreOptions::LogGF);
}

#[test]
fn log_gf_test_2() {
    scores_test_inner(1, 12.0, 0.0, 1.0, ScoreOptions::LogGF);
}

#[test]
fn logrank_test() {
    scores_test_inner(1, 12.0, -10.0, 10.0, ScoreOptions::LogRank);
}

#[test]
fn normal_test_1() {
    scores_test_inner(1, 12.0, -10.0, 10.0, ScoreOptions::Normal);
}

#[test]
fn normal_test_2() {
    scores_test_inner(1, 12.0, 0.0, 1.0, ScoreOptions::Normal);
}

#[test]
fn right_skew_test() {
    scores_test_inner(1, 12.0, -10.0, 10.0, ScoreOptions::RightSkew);
}

#[test]
fn wilcox_test() {
    scores_test_inner(1, 12.0, -10.0, 10.0, ScoreOptions::Wilcox);
}
