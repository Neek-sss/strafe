// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;
use r2rs_nmath::{distribution::FBuilder, traits::Distribution};
use strafe_type::FloatConstraint;

use crate::scores::Scores;

#[derive(Copy, Clone, Debug)]
pub struct LogGFScores {
    pub m1: f64,
    pub m2: f64,
}

impl LogGFScores {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for LogGFScores {
    fn default() -> Self {
        Self { m1: 1.0, m2: 1.0 }
    }
}

impl Scores for LogGFScores {
    fn phi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if (0.0..=1.0).contains(&ui) {
                    let mut f_builder = FBuilder::new();
                    f_builder.with_df1(2.0 * self.m1);
                    f_builder.with_df2(2.0 * self.m2);
                    let f = f_builder.build();
                    let f1 = f.quantile(ui, true).unwrap();

                    let top = self.m1 * self.m2 * (f1 - 1.0);
                    let bottom = self.m2 + self.m1 * f1;
                    top / bottom
                } else {
                    f64::nan()
                }
            })
            .collect()
    }

    fn dphi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if (0.0..=1.0).contains(&ui) {
                    let mut f_builder = FBuilder::new();
                    f_builder.with_df1(2.0 * self.m1);
                    f_builder.with_df2(2.0 * self.m2);
                    let f = f_builder.build();
                    let f1 = f.quantile(ui, true).unwrap();

                    let top = self.m1 * self.m2 * (self.m1 + self.m2);
                    let bottom = (self.m2 + self.m1 * f1).powi(2) * f.density(f1).unwrap();
                    top / bottom
                } else {
                    f64::nan()
                }
            })
            .collect()
    }
}
