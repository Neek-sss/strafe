// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::scores::Scores;

#[derive(Copy, Clone, Debug)]
pub struct RightSkewScores {
    pub tail_barrier: f64,
    pub s2: f64,
    pub tail_value: f64,
}

impl RightSkewScores {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for RightSkewScores {
    fn default() -> Self {
        Self {
            tail_barrier: 0.5,
            s2: -2.0,
            tail_value: 1.0,
        }
    }
}

impl Scores for RightSkewScores {
    fn phi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if ui < self.tail_barrier {
                    (self.tail_value - self.s2) / self.tail_barrier * ui + self.s2
                } else {
                    self.tail_value
                }
            })
            .collect()
    }

    fn dphi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if ui < self.tail_barrier {
                    (self.tail_value - self.s2) / self.tail_barrier
                } else {
                    0.0
                }
            })
            .collect()
    }
}
