// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::scores::Scores;

#[derive(Copy, Clone, Debug)]
pub struct LeftSkewScores {
    pub tail_barrier: f64,
    pub s2: f64,
    pub s3: f64,
}

impl LeftSkewScores {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for LeftSkewScores {
    fn default() -> Self {
        Self {
            tail_barrier: 0.5,
            s2: -1.0,
            s3: 2.0,
        }
    }
}

impl Scores for LeftSkewScores {
    fn phi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if ui < self.tail_barrier {
                    self.s2
                } else {
                    (self.s3 - self.s2) / (1.0 - self.tail_barrier) * (ui - self.tail_barrier)
                        + self.s2
                }
            })
            .collect()
    }

    fn dphi(&self, x: &[f64]) -> Vec<f64> {
        x.iter()
            .map(|&ui| {
                if ui < self.tail_barrier {
                    0.0
                } else {
                    (self.s3 - self.s2) / (1.0 - self.tail_barrier)
                }
            })
            .collect()
    }
}
