# Function List

* [x] baseball
* [x] bbsalaries
* [x] bentscores1
* [x] bentscores2
* [x] bentscores3
* [x] bentscores4
* [x] BoxCox
* [x] CardioRiskFactors
* [x] cellx
* [x] confintadjust
* [x] confintadjust.methods
* [x] disp
* [x] drop.test
* [x] ffa
* [x] getScores
* [x] getScoresDeriv
* [x] gettau
* [x] hstar
* [x] hstarreadyscr
* [x] jaeckel
* [x] khmat
* [x] kwayr
* [x] logGFscores
* [x] logrank.scores
* [x] looptau
* [x] nscores
* [x] oneway.rfit
* [x] pairup
* [x] pasteColsRfit
* [x] print.drop.test
* [x] print.oneway.rfit
* [x] print.raov
* [x] print.rfit
* [x] print.summary.oneway.rfit
* [x] print.summary.rfit
* [x] quail
* [x] raov
* [x] redmod
* [x] rfit
* [x] rfit.default
* [x] rstudent.rfit
* [x] serumLH
* [x] signedrank
* [x] subsets
* [x] summary.oneway.rfit
* [x] summary.rfit
* [x] taustar
* [x] telephone
* [x] vcov.rfit
* [x] wald.test.overall
* [x] walsh
* [x] wscores

## Skipped

* [ ] gettauF0
* [ ] grad
* [ ] nscale