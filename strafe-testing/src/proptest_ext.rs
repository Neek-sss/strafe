use num::ToPrimitive;
use proptest::prelude::*;
use r2rs_nmath::traits::DigitPrec;
use strafe_type::{
    alpha64, fin64, lp64, n64, p64, pos64, r64, rat64, tof64, w64, Alpha64, Checked, Finite64,
    LogProbability64, Natural64, Positive64, PositiveInteger64, Probability64, Rational64, Real64,
};

pub fn strafe_default_proptest_options() -> ProptestConfig {
    ProptestConfig {
        cases: 5,
        // timeout: 2 /* min */ * 60 /* sec in a min */ * 1_000, /* ms in a sec */
        verbose: 1,
        max_shrink_iters: 10,
        ..ProptestConfig::default()
    }
}

pub const FLOAT_MIN: f64 = 1e-30;
pub const FLOAT_MAX: f64 = 1e30;
pub const FLOAT_PREC: usize = f64::DIGITS as usize;

prop_compose! {
    pub fn stat_f64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> f64 {
        crate::f64_limit!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! f64_limit {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::f64_limit_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::f64_limit_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::f64_limit_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn f64_limit_<F: ToPrimitive>(x: F, min: f64, max: f64, prec: usize) -> f64 {
    let x = tof64!(x);
    x.signum() * ((x.abs() % (max - min)) + min).precision_round_to(prec)
}

prop_compose! {
    pub fn stat_p64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<Probability64> {
        crate::num_to_p64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_p64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_p64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_p64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_p64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_p64_<F: ToPrimitive + std::fmt::Display>(
    x: F,
    min: f64,
    max: f64,
    prec: usize,
) -> Checked<Probability64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    if f.is_nan() {
        f = 0.0;
    }

    if f.is_sign_negative() {
        f = f.abs();
    }

    if f > 1.0 {
        f = f.recip();
        f = f.max(max.recip())
    }

    p64!(f)
}

prop_compose! {
    pub fn stat_alpha64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<Alpha64> {
        crate::num_to_alpha64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_alpha64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_alpha64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_alpha64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_alpha64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_alpha64_<F: ToPrimitive + std::fmt::Display>(
    x: F,
    min: f64,
    max: f64,
    prec: usize,
) -> Checked<Alpha64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    if f.is_nan() {
        f = 0.0;
    }

    if f.is_sign_negative() {
        f = f.abs();
    }

    if f >= 1.0 {
        f = f.recip();
        f = f.max(max.recip())
    }

    alpha64!(f)
}

prop_compose! {
    pub fn stat_lp64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<LogProbability64> {
        crate::num_to_lp64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_lp64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_lp64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_lp64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_lp64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_lp64_<F: ToPrimitive + std::fmt::Display>(
    x: F,
    min: f64,
    max: f64,
    prec: usize,
) -> Checked<LogProbability64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    if f.is_nan() {
        f = 0.0;
    }

    if f.is_sign_positive() {
        f = -f;
    }

    lp64!(f)
}

prop_compose! {
    pub fn stat_r64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<Real64> {
        crate::num_to_r64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_r64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_r64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_r64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_r64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_r64_<F: ToPrimitive>(x: F, min: f64, max: f64, prec: usize) -> Checked<Real64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    if f.is_nan() {
        f = 0.0;
    }

    r64!(f)
}

prop_compose! {
    pub fn stat_pos64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<Positive64> {
        crate::num_to_pos64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_pos64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_pos64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_pos64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_pos64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_pos64_<F: ToPrimitive>(x: F, min: f64, max: f64, prec: usize) -> Checked<Positive64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    if f.is_nan() {
        f = 0.0;
    }

    if f <= 0.0 {
        f = f.abs();
    }

    pos64!(f)
}

prop_compose! {
    pub fn stat_rat64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<Rational64> {
        crate::num_to_rat64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_rat64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_rat64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_rat64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_rat64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_rat64_<F: ToPrimitive>(x: F, min: f64, max: f64, prec: usize) -> Checked<Rational64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    if f.is_nan() || f <= 0.0 {
        f = 1.0;
    }

    f = f.abs();

    rat64!(f)
}

prop_compose! {
    pub fn stat_w64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<PositiveInteger64> {
        crate::num_to_w64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_w64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_w64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_w64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_w64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_w64_<F: ToPrimitive>(
    x: F,
    min: f64,
    max: f64,
    prec: usize,
) -> Checked<PositiveInteger64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    f = f.round();

    if f.is_nan() {
        f = 0.0;
    }

    let min = min.round().max(0.0);

    if f < min {
        f = min
    }

    w64!(f)
}

prop_compose! {
    pub fn stat_n64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<Natural64> {
        crate::num_to_n64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_n64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_n64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_n64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_n64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_n64_<F: ToPrimitive>(x: F, min: f64, max: f64, prec: usize) -> Checked<Natural64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    f = f.round();

    if f.is_nan() {
        f = 1.0;
    }

    if f <= 0.0 {
        f = 1.0
    }

    let min = min.round().max(0.0);

    if f < min {
        f = min
    }

    n64!(f)
}

prop_compose! {
    pub fn stat_fin64(min: Option<f64>, max: Option<f64>, prec: Option<usize>)(x in any::<f64>()) -> Checked<Finite64> {
        crate::num_to_fin64!(
            x,
            min.unwrap_or(FLOAT_MIN),
            max.unwrap_or(FLOAT_MAX),
            prec.unwrap_or(FLOAT_PREC)
        )
    }
}

#[macro_export]
macro_rules! num_to_fin64 {
    ($x:expr, $min:expr, $max:expr, $prec:expr) => {{
        $crate::proptest_ext::num_to_fin64_($x, $min, $max, $prec)
    }};
    ($x:expr, $min:expr, $max:expr) => {{
        $crate::proptest_ext::num_to_fin64_($x, $min, $max, $crate::proptest::FLOAT_PREC)
    }};
    ($x:expr) => {{
        $crate::proptest_ext::num_to_fin64_(
            $x,
            $crate::proptest_ext::FLOAT_MIN,
            $crate::proptest_ext::FLOAT_MAX,
            $crate::proptest_ext::FLOAT_PREC,
        )
    }};
}

pub fn num_to_fin64_<F: ToPrimitive>(x: F, min: f64, max: f64, prec: usize) -> Checked<Finite64> {
    let mut f = f64_limit!(tof64!(x), min, max, prec);

    if !f.is_finite() {
        f = 0.0;
    }

    fin64!(f)
}
