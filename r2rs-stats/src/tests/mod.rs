// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

mod adjusted_r_squared;
mod linear_estimator;
mod residuals;
mod significance_of_regression;
mod swilk;
mod t_coefficient;
mod z_coefficient;

pub use self::{
    adjusted_r_squared::*,
    linear_estimator::*,
    residuals::{NormalResidualStatistic, NormalResidualTest},
    significance_of_regression::*,
    swilk::{ShapiroWilkError, ShapiroWilkStatistic, ShapiroWilkTest},
    t_coefficient::*,
    z_coefficient::*,
};

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
