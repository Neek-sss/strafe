// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use strafe_trait::{
    AccurateModel, Assumption, Concept, Conclusion, InaccurateModel, RejectionStatus, Statistic,
    StatisticalTest,
};
use strafe_type::{Alpha64, FloatConstraint, ModelMatrix, Rational64};

use crate::funcs::{residual_sum_of_squares, total_sum_of_squares};

#[derive(Copy, Clone, Debug)]
pub struct LenientAdjustedRSquaredTest {
    alpha: Alpha64,
    scale: Option<f64>,
    df: Option<Rational64>,
}

impl StatisticalTest for LenientAdjustedRSquaredTest {
    type Input = (ModelMatrix, DMatrix<f64>, DMatrix<f64>, DMatrix<f64>);
    type Output = Result<LenientAdjustedRSquaredStatistic, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        Vec::new()
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![Box::new(InaccurateModel::new())]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![Box::new(AccurateModel::new())]
    }

    fn test(&mut self, (x, y, b, w): &Self::Input) -> Self::Output {
        let x = x.matrix();
        let mut weighted_x = x.clone();
        weighted_x
            .column_iter_mut()
            .for_each(|mut row| row.component_mul_assign(w));
        let weighted_y = y.component_mul(w);

        let n = x.shape().0;
        let k = x.shape().1 - 1;

        let residual_mean_square =
            residual_sum_of_squares(&weighted_x, &weighted_y, b) / (n - k - 1) as f64;
        let total_mean_square = total_sum_of_squares(&weighted_x, &weighted_y) / (n - 1) as f64;

        let adj_r_sq = 1.0 - (residual_mean_square / total_mean_square);

        let p = 1.0 - adj_r_sq;

        Ok(LenientAdjustedRSquaredStatistic {
            adj_r_sq,
            p,
            alpha: (self.alpha.unwrap() * 3.0).into(),
        })
    }
}

#[derive(Clone, Debug)]
pub struct LenientAdjustedRSquaredStatistic {
    alpha: Alpha64,
    adj_r_sq: f64,
    p: f64,
}

impl Statistic for LenientAdjustedRSquaredStatistic {
    fn alpha(&self) -> Alpha64 {
        self.alpha
    }

    fn statistic(&self) -> f64 {
        self.adj_r_sq
    }

    fn probability_value(&self) -> f64 {
        self.p
    }

    fn conclusion(&self) -> RejectionStatus {
        if self.p < self.alpha.unwrap() {
            RejectionStatus::RejectInFavorOf(Box::new(AccurateModel {}))
        } else {
            RejectionStatus::FailToReject(Box::new(InaccurateModel {}))
        }
    }
}

impl Default for LenientAdjustedRSquaredTest {
    fn default() -> Self {
        Self {
            alpha: 0.05.into(),
            scale: None,
            df: None,
        }
    }
}

impl LenientAdjustedRSquaredTest {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
            ..self
        }
    }

    pub fn with_scale(self, scale: f64) -> Self {
        Self {
            scale: Some(scale),
            ..self
        }
    }

    pub fn with_df<R: Into<Rational64>>(self, df: R) -> Self {
        Self {
            df: Some(df.into()),
            ..self
        }
    }
}

//pub fn r_sq(x: &DMatrix<f64>, y: &DMatrix<f64>, b: &DMatrix<f64>) -> f64 {
//    let n = x.shape().0;
//
//    1.0 - (residual_sum_of_squares(x, y, b) / total_sum_of_squares(x, y))
//}
