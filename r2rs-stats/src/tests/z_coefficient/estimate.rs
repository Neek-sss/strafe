// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use strafe_trait::{Assumption, StatisticalEstimator};
use strafe_type::ModelMatrix;

use crate::tests::z_coefficient::{ZCoefficient, ZCoefficientBuilder};

impl StatisticalEstimator for ZCoefficientBuilder {
    type Input = (ModelMatrix, DMatrix<f64>, DMatrix<f64>, DMatrix<f64>);
    type Output = Result<Vec<ZCoefficient>, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        Vec::new()
    }

    fn estimate(&self, input: &Self::Input) -> Self::Output {
        self.build(input)
    }
}
