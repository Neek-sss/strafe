use nalgebra::DMatrix;
use strafe_trait::{
    Assumption, Concept, Conclusion, Normal, NormalResiduals, NotNormalResiduals, RejectionStatus,
    Statistic, StatisticalTest,
};
use strafe_type::{Alpha64, ModelMatrix};

use crate::{
    funcs::residuals,
    tests::swilk::{ShapiroWilkStatistic, ShapiroWilkTest},
};

#[derive(Copy, Clone, Debug)]
pub struct NormalResidualTest {
    alpha: Alpha64,
}

impl StatisticalTest for NormalResidualTest {
    type Input = (ModelMatrix, DMatrix<f64>, DMatrix<f64>, DMatrix<f64>);
    type Output = Result<NormalResidualStatistic, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        ShapiroWilkTest::assumptions()
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![Box::new(NormalResiduals::new())]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![Box::new(NotNormalResiduals::new())]
    }

    fn test(&mut self, (x, y, model, w): &Self::Input) -> Self::Output {
        let x = x.matrix();
        let mut weighted_x = x.clone();
        weighted_x
            .column_iter_mut()
            .for_each(|mut row| row.component_mul_assign(w));
        let weighted_y = y.component_mul(w);

        let shapiro_wilk_statistic = ShapiroWilkTest::new().with_alpha(self.alpha).test(
            &residuals(&weighted_y, &weighted_x, model)
                .as_slice()
                .to_vec(),
        )?;
        Ok(NormalResidualStatistic {
            shapiro_wilk_statistic,
        })
    }
}

#[derive(Copy, Clone, Debug)]
pub struct NormalResidualStatistic {
    shapiro_wilk_statistic: ShapiroWilkStatistic,
}

impl Statistic for NormalResidualStatistic {
    fn alpha(&self) -> Alpha64 {
        self.shapiro_wilk_statistic.alpha
    }

    fn statistic(&self) -> f64 {
        self.shapiro_wilk_statistic.w
    }

    fn probability_value(&self) -> f64 {
        self.shapiro_wilk_statistic.p
    }

    fn conclusion(&self) -> RejectionStatus {
        let shapiro_wilk_conclusion = self.shapiro_wilk_statistic.conclusion();
        if shapiro_wilk_conclusion.unwrap().is::<Normal>() {
            RejectionStatus::RejectInFavorOf(Box::new(NormalResiduals {}))
        } else {
            RejectionStatus::FailToReject(Box::new(NotNormalResiduals {}))
        }
    }
}

impl Default for NormalResidualTest {
    fn default() -> Self {
        Self { alpha: 0.05.into() }
    }
}

impl NormalResidualTest {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
        }
    }
}
