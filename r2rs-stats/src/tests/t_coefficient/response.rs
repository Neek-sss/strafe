// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_trait::{
    InsignificantCoefficient, RejectionStatus, SignificantCoefficient, Statistic,
    StatisticalEstimate,
};
use strafe_type::{Alpha64, FloatConstraint};

#[derive(Clone, Debug)]
pub struct TCoefficient {
    pub name: String,
    pub(crate) estimate: f64,
    pub(crate) confidence_interval: (f64, f64),
    pub(crate) alpha: Alpha64,
    pub(crate) t: f64,
    pub(crate) p: f64,
}

impl StatisticalEstimate<f64, (f64, f64)> for TCoefficient {
    fn alpha(&self) -> Alpha64 {
        self.alpha
    }

    fn estimate(&self) -> f64 {
        self.estimate
    }

    fn confidence_interval(&self) -> (f64, f64) {
        self.confidence_interval
    }
}

impl Statistic for TCoefficient {
    fn alpha(&self) -> Alpha64 {
        self.alpha
    }

    fn statistic(&self) -> f64 {
        self.t
    }

    fn probability_value(&self) -> f64 {
        self.p
    }

    fn conclusion(&self) -> RejectionStatus {
        if self.p < self.alpha.unwrap() {
            RejectionStatus::RejectInFavorOf(Box::new(SignificantCoefficient {}))
        } else {
            RejectionStatus::FailToReject(Box::new(InsignificantCoefficient {}))
        }
    }
}
