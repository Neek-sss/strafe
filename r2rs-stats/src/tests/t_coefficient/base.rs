// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use r2rs_nmath::{distribution::TBuilder, traits::Distribution};
use strafe_type::{Alpha64, FloatConstraint, ModelMatrix};

use crate::{funcs::residual_sum_of_squares, tests::t_coefficient::TCoefficient};

#[derive(Copy, Clone, Debug)]
pub struct TCoefficientBuilder {
    alpha: Alpha64,
}

impl Default for TCoefficientBuilder {
    fn default() -> Self {
        Self { alpha: 0.05.into() }
    }
}

impl TCoefficientBuilder {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_alpha(self, alpha: Alpha64) -> Self {
        Self { alpha }
    }

    pub(crate) fn build(
        &self,
        (x, y, b, w): &(ModelMatrix, DMatrix<f64>, DMatrix<f64>, DMatrix<f64>),
    ) -> Result<Vec<TCoefficient>, Box<dyn std::error::Error>> {
        let names = x.column_names();
        let x = x.matrix();
        let mut weighted_x = x.clone();
        weighted_x
            .column_iter_mut()
            .for_each(|mut row| row.component_mul_assign(w));
        let weighted_y = y.component_mul(w);

        let n = x.shape().0;
        let k = x.shape().1 - 1;

        let num_coef = x.shape().1;
        let covariance = (weighted_x.transpose() * &weighted_x)
            .pseudo_inverse(f64::EPSILON)
            .expect("Inverse error");
        let sigma_squared =
            residual_sum_of_squares(&weighted_x, &weighted_y, b) / (n - k - 1) as f64;

        let mut ret = Vec::new();
        for j in 0..num_coef {
            let estimate = b[(j, 0)];
            let standard_error = (sigma_squared * covariance[(j, j)]).sqrt();
            let t = estimate / standard_error;

            let mut t_distr_builder = TBuilder::new();
            t_distr_builder.with_df(n - k - 1)?;
            let t_distr = t_distr_builder.build();

            let confidence_value = t_distr
                .quantile(self.alpha.unwrap() / (2.0 * (n - k - 1) as f64), false)
                .unwrap();

            let lower_bound = estimate - (confidence_value * standard_error);
            let upper_bound = estimate + (confidence_value * standard_error);

            let p = t_distr.probability(t.abs(), false).unwrap() * 2.0;

            ret.push(TCoefficient {
                name: names[j].clone(),
                alpha: (self.alpha.unwrap() / (k + 1) as f64).into(),
                estimate,
                confidence_interval: (lower_bound, upper_bound),
                t,
                p,
            });
        }

        Ok(ret)
    }
}
