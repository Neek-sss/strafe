// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use num_traits::ToPrimitive;
use strafe_testing::{
    r::{RGenerator, RString, RTester},
    r_assert_relative_equal_result,
};
use strafe_trait::{Statistic, StatisticalTest};

use crate::tests::swilk::ShapiroWilkTest;

pub fn shapiro_wilk_test_inner(seed: u16, num_rows: f64, fuzz_amount: f64) {
    let num_rows = num_rows.to_usize().unwrap();
    let fuzz_amount = fuzz_amount.to_usize().unwrap();

    let xs = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = rnorm({});",
            RString::from_f64(num_rows),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R array error");

    let errors =
        RGenerator::new()
            .set_seed(seed + 1)
            .generate_errors(num_rows, (0, 100), fuzz_amount);

    let y = (0..num_rows).map(|i| xs[i] + errors[i]).collect::<Vec<_>>();

    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "sw=shapiro.test({});",
            RString::from_f64_slice(&y),
        )))
        .set_display(&RString::from_str("c(sw$statistic, sw$p)").unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R vec error");

    let r_swilk = (r_ret[0], r_ret[1]);

    let swilk_ret = ShapiroWilkTest::new().test(&y).expect("Shapiro test error");

    r_assert_relative_equal_result!(Ok(r_swilk.0), &swilk_ret.statistic(), 1e-4);
    r_assert_relative_equal_result!(Ok(r_swilk.1), &swilk_ret.probability_value(), 1e-4);
}
