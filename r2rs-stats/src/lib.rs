#![allow(dead_code)]
#![allow(mutable_transmutes)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(unused_assignments)]
#![allow(unused_mut)]

pub mod distance;
pub mod funcs;
pub mod regression;
pub mod tests;
pub mod traits;

#[cfg(not(tarpaulin_include))]
mod unfinished;
