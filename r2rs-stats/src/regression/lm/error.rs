// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::{
    error::Error,
    fmt::{Display, Formatter, Result as FmtResult},
};

use crate::tests::NormalResidualStatistic;

#[derive(Clone, Debug)]
pub enum LeastSquaresRegressionError {
    NotNormalResiduals(NormalResidualStatistic),
}

impl Display for LeastSquaresRegressionError {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        write!(fmt, "Residuals are not normally distributed")
    }
}

impl Error for LeastSquaresRegressionError {}
