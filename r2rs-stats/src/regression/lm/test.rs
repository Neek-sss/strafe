// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::error::Error;

use strafe_trait::{
    Assumption, Concept, Conclusion, InsignificantCoefficient, NoSignificantFeature,
    NormalResiduals, SignificantCoefficient, SomeSignficantFeature, StatisticalTest,
};

use crate::{
    regression::lm::LeastSquaresRegression,
    tests::{
        NormalResidualStatistic, NormalResidualTest, SignificanceOfRegressionStatistic,
        SignificanceOfRegressionTest, TCoefficient, TCoefficientBuilder,
    },
};

pub struct LeastSquaresRegressionTest {
    pub residual_test: NormalResidualStatistic,
    pub significance_of_regression: SignificanceOfRegressionStatistic,
    pub significance_of_coefficients: Vec<TCoefficient>,
}

impl StatisticalTest for LeastSquaresRegression {
    type Input = ();
    type Output = Result<LeastSquaresRegressionTest, Box<dyn Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        vec![Box::new(NormalResiduals::new())]
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![
            Box::new(NoSignificantFeature::new()),
            Box::new(InsignificantCoefficient::new()),
        ]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![
            Box::new(SomeSignficantFeature::new()),
            Box::new(SignificantCoefficient::new()),
        ]
    }

    fn test(&mut self, _: &Self::Input) -> Self::Output {
        let residual_test = NormalResidualTest::new()
            .with_alpha(self.alpha)
            .test(&self.model_data)?;

        let significance_of_regression = SignificanceOfRegressionTest::new()
            .with_alpha(self.alpha)
            .test(&self.model_data)?;

        let significance_of_coefficients = TCoefficientBuilder::new()
            .with_alpha(self.alpha)
            .test(&self.model_data)?;

        Ok(LeastSquaresRegressionTest {
            residual_test,
            significance_of_regression,
            significance_of_coefficients,
        })
    }
}
