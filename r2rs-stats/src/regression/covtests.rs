// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use polars::datatypes::DataType;
use strafe_datasets::baseball;
use strafe_type::ModelMatrix;

use super::tests::*;
use crate::regression::glm::family::gaussian::Gaussian;

#[test]
fn linear_regression_test() {
    linear_regression_test_inner(1, 1.0, 12.0, 8.0);
}

#[test]
fn generalized_linear_regression_test() {
    generalized_linear_regression_test_inner(1, 1.0, 12.0, 8.0);
}

#[test]
fn generalized_linear_regression_2() {
    let baseball = baseball().unwrap();

    let mut x_in = Vec::new();
    baseball
        .column("height")
        .unwrap()
        .cast(&DataType::Float64)
        .unwrap()
        .f64()
        .unwrap()
        .for_each(|f| x_in.push(f.unwrap()));
    let mut x_mm = ModelMatrix::from(&x_in);
    x_mm.set_name("x", "height");

    let mut y_in = Vec::new();
    baseball
        .column("weight")
        .unwrap()
        .cast(&DataType::Float64)
        .unwrap()
        .f64()
        .unwrap()
        .for_each(|f| y_in.push(f.unwrap()));
    let mut y_mm = ModelMatrix::from(&y_in);
    y_mm.set_name("x", "weight");

    fit_models(
        x_mm.matrix(),
        y_mm.matrix(),
        ModelMatrix::from(vec![1.0; x_in.len()]).matrix(),
        Gaussian::default(),
        &family_option_string("Gaussian"),
    );
}
