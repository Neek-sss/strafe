// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

mod glm;
mod lm;

pub use self::{
    glm::{
        family, link, GeneralizedLinearRegression, GeneralizedLinearRegressionBuilder,
        GeneralizedRegressionTest,
    },
    lm::{
        LeastSquaresRegression, LeastSquaresRegressionBuilder, LeastSquaresRegressionError,
        LeastSquaresRegressionTest,
    },
};

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
