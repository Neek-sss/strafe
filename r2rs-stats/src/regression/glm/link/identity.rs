// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::regression::glm::link::core::Link;

#[derive(Default, Clone, Debug)]
pub struct Identity {}

impl Identity {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Link for Identity {
    fn link_func(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        mu.clone()
    }

    fn link_inverse(&self, eta: &DMatrix<f64>) -> DMatrix<f64> {
        eta.clone()
    }

    fn mu_eta(&self, eta: &DMatrix<f64>) -> DMatrix<f64> {
        DMatrix::from_iterator(eta.shape().0, eta.shape().1, eta.iter().map(|_| 1.0))
    }

    fn valid_eta(&self, _eta: &DMatrix<f64>) -> bool {
        true
    }
}
