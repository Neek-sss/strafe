// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::regression::glm::link::core::Link;

#[derive(Default, Clone, Debug)]
pub struct OneDivMuSqrd {}

impl OneDivMuSqrd {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Link for OneDivMuSqrd {
    fn link_func(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        mu.map(|mu| 1.0 / mu.powi(2))
    }

    fn link_inverse(&self, eta: &DMatrix<f64>) -> DMatrix<f64> {
        eta.map(|eta| 1.0 / eta.sqrt())
    }

    fn mu_eta(&self, eta: &DMatrix<f64>) -> DMatrix<f64> {
        eta.map(|eta| -1.0 / (2.0 * eta.powf(1.5)))
    }

    fn valid_eta(&self, eta: &DMatrix<f64>) -> bool {
        eta.iter().all(|&eta| eta.is_finite() && eta > 0.0)
    }
}
