// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_r64, stat_w64, strafe_default_proptest_options},
};
use strafe_type::tof64;

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn cauchit_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::Cauchit);
    }

    #[test]
    fn cloglog_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::CLogLog);
    }

    #[test]
    fn identity_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::Identity);
    }

    #[test]
    fn inverse_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::Inverse);
    }

    #[test]
    fn log_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::Log);
    }

    #[test]
    fn logit_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::Logit);
    }

    #[test]
    fn one_div_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::OneDivMuSqrd);
    }

    #[test]
    fn power_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
        lambda in stat_r64(Some(-1.0), Some(100.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::Power(tof64!(lambda)));
    }

    #[test]
    fn probit_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::Probit);
    }

    #[test]
    fn sqrt_link_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        link_test_inner(seed, num_rows, LinkOptions::Sqrt);
    }
}
