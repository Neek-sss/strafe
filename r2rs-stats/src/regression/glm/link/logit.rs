// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::regression::glm::{
    family::family_c_translation::{logit_link, logit_linkinv, logit_mu_eta},
    link::core::Link,
};

#[derive(Default, Clone, Debug)]
pub struct Logit {}

impl Logit {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Link for Logit {
    fn link_func(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        DMatrix::from_iterator(
            mu.shape().0,
            mu.shape().1,
            logit_link(mu.as_slice()).into_iter(),
        )
    }

    fn link_inverse(&self, eta: &DMatrix<f64>) -> DMatrix<f64> {
        DMatrix::from_iterator(
            eta.shape().0,
            eta.shape().1,
            logit_linkinv(eta.as_slice()).into_iter(),
        )
    }

    fn mu_eta(&self, eta: &DMatrix<f64>) -> DMatrix<f64> {
        DMatrix::from_iterator(
            eta.shape().0,
            eta.shape().1,
            logit_mu_eta(eta.as_slice()).into_iter(),
        )
    }

    fn valid_eta(&self, _eta: &DMatrix<f64>) -> bool {
        true
    }
}
