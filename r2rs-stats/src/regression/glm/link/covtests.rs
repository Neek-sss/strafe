// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn cauchit_link_test() {
    link_test_inner(1, 12.0, LinkOptions::Cauchit);
}

#[test]
fn cloglog_link_test() {
    link_test_inner(1, 12.0, LinkOptions::CLogLog);
}

#[test]
fn identity_link_test() {
    link_test_inner(1, 12.0, LinkOptions::Identity);
}

#[test]
fn inverse_link_test() {
    link_test_inner(1, 12.0, LinkOptions::Inverse);
}

#[test]
fn log_link_test() {
    link_test_inner(1, 12.0, LinkOptions::Log);
}

#[test]
fn logit_link_test() {
    link_test_inner(1, 12.0, LinkOptions::Logit);
}

#[test]
fn one_div_link_test() {
    link_test_inner(1, 12.0, LinkOptions::OneDivMuSqrd);
}

#[test]
fn power_link_test() {
    link_test_inner(1, 12.0, LinkOptions::Power(-1.0));
}

#[test]
fn probit_link_test() {
    link_test_inner(1, 12.0, LinkOptions::Probit);
}

#[test]
fn sqrt_link_test() {
    link_test_inner(1, 12.0, LinkOptions::Sqrt);
}
