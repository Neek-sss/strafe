// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::regression::glm::link::core::Link;

#[derive(Default, Clone, Debug)]
pub struct CLogLog {}

impl CLogLog {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Link for CLogLog {
    fn link_func(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        mu.map(|mu| (-(1.0 - mu).ln()).ln())
    }

    fn link_inverse(&self, eta: &DMatrix<f64>) -> DMatrix<f64> {
        eta.map(|eta| ((-(-eta.exp()).exp_m1()).min(1.0 - f64::EPSILON)).max(f64::EPSILON))
    }

    fn mu_eta(&self, eta: &DMatrix<f64>) -> DMatrix<f64> {
        eta.map(|eta| {
            let eta = eta.min(700.0);
            (eta.exp() * (-eta.exp()).exp()).max(f64::EPSILON)
        })
    }

    fn valid_eta(&self, _eta: &DMatrix<f64>) -> bool {
        true
    }
}
