pub mod cauchit;
pub mod cloglog;
pub mod core;
pub mod identity;
pub mod inverse;
pub mod log;
pub mod logit;
pub mod one_div_mu_sqrd;
pub mod power;
pub mod probit;
pub mod sqrt;

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
