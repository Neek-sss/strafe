use ::libc;
use log::error;

/*, MAYBE */
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2005-2016  The R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 *
 *  Quartz Quartz device module header file
 *
 */
static THRESH: libc::c_double = 30.0f64;
static MTHRESH: libc::c_double = -30.0f64;
static INVEPS: libc::c_double = 1.0 / 2.2204460492503131e-16f64;
/* *
 * Evaluate x/(1 - x). An inline function is used so that x is
 * evaluated once only.
 *
 * @param x input in the range (0, 1)
 *
 * @return x/(1 - x)
 */
#[inline]
fn x_d_omx(x: libc::c_double) -> libc::c_double {
    if !(0.0..=1.0).contains(&x) {
        error!("Value {} out of range (0, 1)", x);
    }
    x / (1.0 - x)
}
/* *
 * Evaluate x/(1 + x). An inline function is used so that x is
 * evaluated once only. [but inlining is optional!]
 *
 * @param x input
 *
 * @return x/(1 + x)
 */
#[inline]
extern "C" fn x_d_opx(x: f64) -> f64 {
    x / (1.0 + x)
}
/* Declarations for .Call entry points */
pub fn logit_link(mu: &[f64]) -> Vec<f64> {
    let mut i = 0;
    let mut n = mu.len();
    let mut ans = mu.to_vec();
    if n == 0 {
        error!("Argument mu must be a nonempty numeric vector");
    }
    i = 0;
    while i < n {
        ans[i] = (x_d_omx(mu[i])).ln();
        i += 1
    }
    ans
}
pub fn logit_linkinv(eta: &[f64]) -> Vec<f64> {
    let mut ans = eta.to_vec();
    let mut i = 0;
    let mut n = eta.len();
    if n == 0 {
        error!("Argument eta must be a nonempty numeric vector");
    }
    i = 0;
    while i < n {
        let mut etai = eta[i];
        let mut tmp = 0.0;
        tmp = if etai < MTHRESH {
            2.2204460492503131e-16f64
        } else if etai > THRESH {
            INVEPS
        } else {
            etai.exp()
        };
        ans[i] = x_d_opx(tmp);
        i += 1
    }
    ans
}
pub fn logit_mu_eta(mut eta: &[f64]) -> Vec<f64> {
    let mut ans = eta.to_vec();
    let mut i = 0;
    let mut n = eta.len();
    if n == 0 {
        error!("Argument eta must be a nonempty numeric vector");
    }
    i = 0;
    while i < n {
        let mut etai = eta[i];
        let mut opexp = 1.0 + etai.exp();
        ans[i] = if etai > THRESH || etai < MTHRESH {
            2.2204460492503131e-16f64
        } else {
            (etai.exp()) / (opexp * opexp)
        };
        i += 1
    }
    ans
}
#[inline]
fn y_log_y(y: f64, mu: f64) -> f64 {
    if y != 0.0f64 {
        (y) * (y / mu).ln()
    } else {
        0.0
    }
}
pub fn binomial_dev_resids(mut y: &[f64], mut mu: &[f64], mut wt: &[f64]) -> Vec<f64> {
    let mut i = 0;
    let mut n = y.len();
    let mut lmu = mu.len();
    let mut lwt = wt.len();
    let mut ans = y.to_vec();
    let mut mui = 0.0;
    let mut yi = 0.0;
    if lmu != n && lmu != 1 {
        error!(
            "argument eta must be a numeric vector of length 1 or length {}",
            n
        );
    }
    if lwt != n && lwt != 1 {
        error!(
            "argument wt must be a numeric vector of length 1 or length {}",
            n
        );
    }
    /* Written separately to avoid an optimization bug on Solaris cc */
    if lmu > 1 {
        i = 0;
        while i < n {
            mui = mu[i];
            yi = y[i];
            ans[i] = 2.0
                * wt[if lwt > 1 { i } else { 0 }]
                * (y_log_y(yi, mui) + y_log_y(1.0 - yi, 1.0 - mui));
            i += 1
        }
    } else {
        mui = mu[0];
        i = 0;
        while i < n {
            yi = y[i];
            ans[i] = 2.0
                * wt[if lwt > 1 { i } else { 0 }]
                * (y_log_y(yi, mui) + y_log_y(1.0 - yi, 1.0 - mui));
            i += 1
        }
    }
    ans
}
