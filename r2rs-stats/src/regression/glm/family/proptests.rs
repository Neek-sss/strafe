// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_w64, strafe_default_proptest_options},
};

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn binomial_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Binomial);
    }

    #[test]
    fn gamma_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Gamma);
    }

    #[test]
    fn gaussian_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Gaussian);
    }

    #[test]
    fn inverse_gaussian_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::InverseGaussian);
    }

    #[test]
    fn poisson_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Poisson);
    }

    #[test]
    fn quasi_constant_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Quasi(Variance::Constant));
    }

    #[test]
    fn quasi_mu_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Quasi(Variance::Mu));
    }

    #[test]
    fn quasi_mu_sqrd_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Quasi(Variance::MuSqrd));
    }

    #[test]
    fn quasi_mu_cubed_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Quasi(Variance::MuCubed));
    }

    #[test]
    fn quasi_mu_one_minus_one_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::Quasi(Variance::MuOneMinusMu));
    }

    #[test]
    fn quasi_binomial_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::QuasiBinomial);
    }

    #[test]
    fn quasi_poisson_family_test(
        seed in 0..std::u16::MAX,
        num_rows in stat_w64(Some(12.0), Some(50.0), None),
    ) {
        family_test_inner(seed, num_rows, FamilyOptions::QuasiPoisson);
    }
}
