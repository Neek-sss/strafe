// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use num_traits::Float;

use crate::regression::glm::{
    family::core::Family,
    link::{core::Link, log::Log},
};

#[derive(Clone, Debug)]
pub struct QuasiPoisson<L: Link> {
    link: L,
}

impl Default for QuasiPoisson<Log> {
    fn default() -> Self {
        Self { link: Log::new() }
    }
}

impl<L: Link + Clone> QuasiPoisson<L> {
    pub fn new(link: &L) -> Self {
        Self { link: link.clone() }
    }
}

impl<L: Link + Clone> Family<L> for QuasiPoisson<L> {
    fn variance(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        mu.clone()
    }

    fn valid_mu(&self, mu: &DMatrix<f64>) -> bool {
        mu.iter().all(|mu| mu.is_finite() && *mu > 0.0)
    }

    fn residual_deviance(
        &self,
        y: &DMatrix<f64>,
        mu: &DMatrix<f64>,
        weights: &DMatrix<f64>,
    ) -> DMatrix<f64> {
        let mut ret = y.clone();
        ret.iter_mut()
            .zip(mu.iter())
            .zip(weights.iter())
            .for_each(|((ret, mu), weight)| {
                let y = *ret;
                let mu = *mu;
                let weight = *weight;
                let mut r = mu * weight;
                if y > 0.0 {
                    r = weight * (y * (y / mu).ln() - (y - mu));
                }
                *ret = 2.0 * r;
            });
        ret
    }

    fn aic(
        &self,
        _y: &[f64],
        _n: &[f64],
        _mu: &[f64],
        _weights: &[f64],
        _residual_deviance: f64,
    ) -> f64 {
        f64::nan()
    }

    fn initialize(
        &self,
        y: &mut DMatrix<f64>,
        _weights: &mut DMatrix<f64>,
    ) -> (DMatrix<f64>, DMatrix<f64>) {
        let mut n = y.clone();
        n.iter_mut().for_each(|n| *n = 1.0);
        let mut mu_start = y.clone();
        mu_start.iter_mut().for_each(|y| *y += 0.1);
        (n, mu_start)
    }

    fn link(&self) -> L {
        self.link.clone()
    }

    fn set_dispersion(&self) -> Option<f64> {
        None
    }
}
