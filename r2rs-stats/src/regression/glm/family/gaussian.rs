// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::{DMatrix, RealField};

use crate::regression::glm::{
    family::core::Family,
    link::{core::Link, identity::Identity},
};

#[derive(Clone, Debug)]
pub struct Gaussian<L: Link> {
    link: L,
}

impl Default for Gaussian<Identity> {
    fn default() -> Self {
        Self {
            link: Identity::new(),
        }
    }
}

impl<L: Link + Clone> Gaussian<L> {
    pub fn new(link: &L) -> Self {
        Self { link: link.clone() }
    }
}

impl<L: Link + Clone> Family<L> for Gaussian<L> {
    fn variance(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = mu.clone();
        ret.iter_mut().for_each(|mu| *mu = 1.0);
        ret
    }

    fn valid_mu(&self, _mu: &DMatrix<f64>) -> bool {
        true
    }

    fn residual_deviance(
        &self,
        y: &DMatrix<f64>,
        mu: &DMatrix<f64>,
        weights: &DMatrix<f64>,
    ) -> DMatrix<f64> {
        let mut ret = y.clone();
        ret.iter_mut()
            .zip(mu.iter())
            .zip(weights.iter())
            .for_each(|((ret, mu), weight)| {
                let y = *ret;
                let mu = *mu;
                let weight = *weight;
                *ret = weight * (y - mu).powi(2);
            });
        ret
    }

    fn aic(
        &self,
        _y: &[f64],
        _n: &[f64],
        _mu: &[f64],
        weights: &[f64],
        residual_deviance: f64,
    ) -> f64 {
        let nobs = weights.len() as f64;
        let ret = nobs * ((residual_deviance / nobs * 2.0 * f64::pi()).ln() + 1.0) + 2.0
            - weights.iter().map(|w| w.ln()).sum::<f64>();
        ret
    }

    fn initialize(
        &self,
        y: &mut DMatrix<f64>,
        _weights: &mut DMatrix<f64>,
    ) -> (DMatrix<f64>, DMatrix<f64>) {
        let mut n = y.clone();
        n.iter_mut().for_each(|n| *n = 1.0);
        let mu_start = y.clone();
        (n, mu_start)
    }

    fn link(&self) -> L {
        self.link.clone()
    }

    fn set_dispersion(&self) -> Option<f64> {
        None
    }
}
