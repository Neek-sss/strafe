pub mod binomial;
pub mod core;
pub mod gamma;
pub mod gaussian;
pub mod inverse_gaussian;
pub mod poisson;
pub mod quasi;
pub mod quasi_binomial;
pub mod quasi_poisson;
pub mod quasi_variance;

pub(crate) mod family_c_translation;

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
