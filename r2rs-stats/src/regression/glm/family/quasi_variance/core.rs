// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::fmt::Debug;

use nalgebra::DMatrix;

pub trait Variance: Clone + Debug {
    fn var_fun(&self, mu: &DMatrix<f64>) -> DMatrix<f64>;
    fn dev_resids(&self, y: &DMatrix<f64>, mu: &DMatrix<f64>, wt: &DMatrix<f64>) -> DMatrix<f64>;
    fn valid_mu(&self, mu: &DMatrix<f64>) -> bool;
    fn initialize(&self, y: &DMatrix<f64>) -> (DMatrix<f64>, DMatrix<f64>);
}
