// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::regression::glm::family::{
    family_c_translation::binomial_dev_resids, quasi_variance::core::Variance,
};

#[derive(Default, Debug, Clone)]
pub struct MuOneMinusMu {}

impl MuOneMinusMu {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Variance for MuOneMinusMu {
    fn var_fun(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = mu.clone();
        ret.iter_mut().for_each(|mu| *mu = *mu * (1.0 - *mu));
        ret
    }

    fn dev_resids(&self, y: &DMatrix<f64>, mu: &DMatrix<f64>, wt: &DMatrix<f64>) -> DMatrix<f64> {
        DMatrix::from_iterator(
            y.shape().0,
            y.shape().1,
            binomial_dev_resids(y.as_slice(), mu.as_slice(), wt.as_slice()).into_iter(),
        )
    }

    fn valid_mu(&self, mu: &DMatrix<f64>) -> bool {
        mu.iter().all(|mu| *mu > 0.0 && *mu < 1.0)
    }

    fn initialize(&self, y: &DMatrix<f64>) -> (DMatrix<f64>, DMatrix<f64>) {
        let mut n = y.clone();
        n.iter_mut().for_each(|n| *n = 1.0);
        let mut mu_start = y.clone();
        mu_start
            .iter_mut()
            .for_each(|y| *y = y.min(0.999).max(0.001));
        (n, mu_start)
    }
}
