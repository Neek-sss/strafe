// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::regression::glm::family::quasi_variance::core::Variance;

#[derive(Default, Debug, Clone)]
pub struct Constant {}

impl Constant {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Variance for Constant {
    fn var_fun(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = mu.clone();
        ret.iter_mut().for_each(|r| *r = 1.0);
        ret
    }

    fn dev_resids(&self, y: &DMatrix<f64>, mu: &DMatrix<f64>, wt: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = y.clone();
        ret.iter_mut()
            .zip(mu.iter())
            .zip(wt.iter())
            .for_each(|((ret, mu), wt)| {
                let y = *ret;
                let mu = *mu;
                let wt = *wt;
                *ret = wt * (y - mu).powi(2)
            });
        ret
    }

    fn valid_mu(&self, _mu: &DMatrix<f64>) -> bool {
        true
    }

    fn initialize(&self, y: &DMatrix<f64>) -> (DMatrix<f64>, DMatrix<f64>) {
        let mut n = y.clone();
        n.iter_mut().for_each(|n| *n = 1.0);
        (n, y.clone())
    }
}
