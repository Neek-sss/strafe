// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::regression::glm::family::quasi_variance::core::Variance;

#[derive(Default, Debug, Clone)]
pub struct MuSqrd {}

impl MuSqrd {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Variance for MuSqrd {
    fn var_fun(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = mu.clone();
        ret.iter_mut().for_each(|mu| *mu = mu.powi(2));
        ret
    }

    fn dev_resids(&self, y: &DMatrix<f64>, mu: &DMatrix<f64>, wt: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = y.clone();
        ret.iter_mut()
            .zip(mu.iter())
            .zip(wt.iter())
            .for_each(|((ret, mu), wt)| {
                let y = *ret;
                let mu = *mu;
                let wt = *wt;
                *ret = (-2.0 * wt * ((if y == 0.0 { 1.0 } else { y } / mu).ln() - (y - mu) / mu))
                    .max(0.0)
            });
        ret
    }

    fn valid_mu(&self, mu: &DMatrix<f64>) -> bool {
        mu.iter().all(|mu| *mu > 0.0)
    }

    fn initialize(&self, y: &DMatrix<f64>) -> (DMatrix<f64>, DMatrix<f64>) {
        let mut n = y.clone();
        n.iter_mut().for_each(|n| *n = 1.0);
        let mut mu_start = y.clone();
        mu_start
            .iter_mut()
            .for_each(|y| *y = *y + 0.1 * if *y == 0.0 { 1.0 } else { 0.0 });
        (n, mu_start)
    }
}
