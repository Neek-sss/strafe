// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use num_traits::Float;

use crate::regression::glm::{
    family::{
        core::Family,
        quasi_variance::{constant::Constant, core::Variance},
    },
    link::{core::Link, identity::Identity},
};

#[derive(Clone, Debug)]
pub struct Quasi<L: Link, V: Variance> {
    link: L,
    variance: V,
}

impl Default for Quasi<Identity, Constant> {
    fn default() -> Self {
        Self {
            link: Identity::new(),
            variance: Constant::new(),
        }
    }
}

impl<L: Link + Clone, V: Variance> Quasi<L, V> {
    pub fn new(link: &L, variance: &V) -> Self {
        Self {
            link: link.clone(),
            variance: variance.clone(),
        }
    }
}

impl<L: Link + Clone, V: Variance> Family<L> for Quasi<L, V> {
    fn variance(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        self.variance.var_fun(mu)
    }

    fn valid_mu(&self, mu: &DMatrix<f64>) -> bool {
        self.variance.valid_mu(mu)
    }

    fn residual_deviance(
        &self,
        y: &DMatrix<f64>,
        mu: &DMatrix<f64>,
        weights: &DMatrix<f64>,
    ) -> DMatrix<f64> {
        self.variance.dev_resids(y, mu, weights)
    }

    fn aic(
        &self,
        _y: &[f64],
        _n: &[f64],
        _mu: &[f64],
        _weights: &[f64],
        _residual_deviance: f64,
    ) -> f64 {
        f64::nan()
    }

    fn initialize(
        &self,
        y: &mut DMatrix<f64>,
        _weights: &mut DMatrix<f64>,
    ) -> (DMatrix<f64>, DMatrix<f64>) {
        self.variance.initialize(y)
    }

    fn link(&self) -> L {
        self.link.clone()
    }

    fn set_dispersion(&self) -> Option<f64> {
        None
    }
}
