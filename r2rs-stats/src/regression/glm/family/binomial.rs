// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use log::{error, warn};
use nalgebra::DMatrix;
use r2rs_nmath::{distribution::BinomialBuilder, traits::Distribution};
use strafe_type::FloatConstraint;

use crate::regression::glm::{
    family::{core::Family, family_c_translation::binomial_dev_resids},
    link::{core::Link, logit::Logit},
};

#[derive(Clone, Debug)]
pub struct Binomial<L: Link> {
    link: L,
}

pub fn binomial_default() -> Binomial<Logit> {
    Binomial::default()
}

impl Default for Binomial<Logit> {
    fn default() -> Self {
        Self { link: Logit::new() }
    }
}

impl<L: Link + Clone> Binomial<L> {
    pub fn new(link: &L) -> Self {
        Self { link: link.clone() }
    }
}

impl<L: Link + Clone> Family<L> for Binomial<L> {
    fn variance(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        DMatrix::from_iterator(
            mu.shape().0,
            mu.shape().1,
            mu.iter().map(|&mu_i| mu_i * (1.0 - mu_i)),
        )
    }

    fn valid_mu(&self, mu: &DMatrix<f64>) -> bool {
        mu.iter()
            .all(|&mu_i| mu_i.is_finite() && mu_i > 0.0 && mu_i < 1.0)
    }

    fn residual_deviance(
        &self,
        y: &DMatrix<f64>,
        mu: &DMatrix<f64>,
        weights: &DMatrix<f64>,
    ) -> DMatrix<f64> {
        DMatrix::from_iterator(
            y.shape().0,
            y.shape().1,
            binomial_dev_resids(y.as_slice(), mu.as_slice(), weights.as_slice()).into_iter(),
        )
    }

    fn aic(
        &self,
        y: &[f64],
        n: &[f64],
        mu: &[f64],
        weights: &[f64],
        _residual_deviance: f64,
    ) -> f64 {
        let m = if n.iter().any(|&n_i| n_i > 1.0) {
            n
        } else {
            weights
        };
        -2.0 * (0..y.len())
            .map(|i| {
                let temp = if m[i] > 0.0 { weights[i] / m[i] } else { 0.0 };
                let mut binom_builder = BinomialBuilder::new();
                binom_builder.with_size(m[i].round());
                binom_builder.with_success_probability(mu[i]);
                let binom = binom_builder.build();
                temp * binom.log_density((m[i] * y[i]).round()).unwrap()
            })
            .sum::<f64>()
    }

    fn initialize(
        &self,
        y: &mut DMatrix<f64>,
        weights: &mut DMatrix<f64>,
    ) -> (DMatrix<f64>, DMatrix<f64>) {
        let n = (0..y.len()).map(|_| 1.0).collect::<Vec<_>>();
        y.iter_mut()
            .zip(weights.iter())
            .for_each(|(y_i, &weight_i)| {
                if weight_i == 0.0 {
                    *y_i = 0.0
                }
            });
        if y.iter().any(|&y_i| !(0.0..=1.0).contains(&y_i)) {
            error!("y values must be 0 <= y <= 1")
        }
        let mu_start = (0..y.len())
            .map(|i| (weights[i] * y[i] + 0.5) / (weights[i] + 1.0))
            .collect::<Vec<_>>();
        let m = weights
            .iter()
            .zip(y.iter())
            .map(|(&weight_i, &y_i)| weight_i * y_i)
            .collect::<Vec<_>>();
        if m.iter().any(|&m_i| m_i - m_i.round() > 1e-3) {
            warn!("non-integer #successes in a binomial glm!");
        }
        (
            DMatrix::from_iterator(weights.shape().0, weights.shape().1, n.into_iter()),
            DMatrix::from_iterator(weights.shape().0, weights.shape().1, mu_start.into_iter()),
        )
    }

    fn link(&self) -> L {
        self.link.clone()
    }

    fn set_dispersion(&self) -> Option<f64> {
        Some(1.0)
    }
}
