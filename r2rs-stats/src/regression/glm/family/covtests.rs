// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;
use crate::regression::glm::{
    family::{
        binomial::Binomial,
        gamma::Gamma,
        gaussian::Gaussian,
        inverse_gaussian::InverseGaussian,
        poisson::Poisson,
        quasi::Quasi,
        quasi_binomial::QuasiBinomial,
        quasi_poisson::QuasiPoisson,
        quasi_variance::{
            constant::Constant, mu::Mu, mu_cubed::MuCubed, mu_one_minus_mu::MuOneMinusMu,
            mu_sqrd::MuSqrd,
        },
    },
    link::identity::Identity,
};

#[test]
fn binomial_family_test() {
    family_test_inner(
        1,
        12.0,
        Binomial::default(),
        &family_option_string("Binomial"),
    );
}

#[test]
fn gamma_family_test() {
    family_test_inner(1, 12.0, Gamma::default(), &family_option_string("Gamma"));
}

#[test]
fn gaussian_family_test() {
    family_test_inner(
        1,
        12.0,
        Gaussian::default(),
        &family_option_string("Gaussian"),
    );
}

#[test]
fn inverse_gaussian_family_test() {
    family_test_inner(
        1,
        12.0,
        InverseGaussian::default(),
        &family_option_string("InverseGaussian"),
    );
}

#[test]
fn poisson_family_test() {
    family_test_inner(
        1,
        12.0,
        Poisson::default(),
        &family_option_string("Poisson"),
    );
}

#[test]
fn quasi_constant_family_test() {
    family_test_inner(
        1,
        12.0,
        Quasi::new(&Identity::default(), &Constant::default()),
        &family_option_string("Quasi(Constant)"),
    );
}

#[test]
fn quasi_mu_family_test() {
    family_test_inner(
        1,
        12.0,
        Quasi::new(&Identity::default(), &Mu::default()),
        &family_option_string("Quasi(Mu)"),
    );
}

#[test]
fn quasi_mu_sqrd_family_test() {
    family_test_inner(
        1,
        12.0,
        Quasi::new(&Identity::default(), &MuSqrd::default()),
        &family_option_string("Quasi(MuSqrd)"),
    );
}

#[test]
fn quasi_mu_cubed_family_test() {
    family_test_inner(
        1,
        12.0,
        Quasi::new(&Identity::default(), &MuCubed::default()),
        &family_option_string("Quasi(MuCubed)"),
    );
}

#[test]
fn quasi_mu_one_minus_one_family_test() {
    family_test_inner(
        1,
        12.0,
        Quasi::new(&Identity::default(), &MuOneMinusMu::default()),
        &family_option_string("Quasi(MuOneMinusMu)"),
    );
}

#[test]
fn quasi_binomial_family_test() {
    family_test_inner(
        1,
        12.0,
        QuasiBinomial::default(),
        &family_option_string("QuasiBinomial"),
    );
}

#[test]
fn quasi_poisson_family_test() {
    family_test_inner(
        1,
        12.0,
        QuasiPoisson::default(),
        &family_option_string("QuasiPoisson"),
    );
}
