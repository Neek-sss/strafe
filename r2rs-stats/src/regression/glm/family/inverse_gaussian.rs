// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::{DMatrix, RealField};

use crate::regression::glm::{
    family::core::Family,
    link::{core::Link, one_div_mu_sqrd::OneDivMuSqrd},
};

#[derive(Clone, Debug)]
pub struct InverseGaussian<L: Link> {
    link: L,
}

impl Default for InverseGaussian<OneDivMuSqrd> {
    fn default() -> Self {
        Self {
            link: OneDivMuSqrd::new(),
        }
    }
}

impl<L: Link + Clone> InverseGaussian<L> {
    pub fn new(link: &L) -> Self {
        Self { link: link.clone() }
    }
}

impl<L: Link + Clone> Family<L> for InverseGaussian<L> {
    fn variance(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = mu.clone();
        ret.iter_mut().for_each(|mu| *mu = mu.powi(3));
        ret
    }

    fn valid_mu(&self, _mu: &DMatrix<f64>) -> bool {
        true
    }

    fn residual_deviance(
        &self,
        y: &DMatrix<f64>,
        mu: &DMatrix<f64>,
        weights: &DMatrix<f64>,
    ) -> DMatrix<f64> {
        let mut ret = y.clone();
        ret.iter_mut()
            .zip(mu.iter())
            .zip(weights.iter())
            .for_each(|((ret, mu), weight)| {
                let y = *ret;
                let mu = *mu;
                let weight = *weight;
                *ret = weight * (y - mu).powi(2) / (y * mu.powi(2));
            });
        ret
    }

    fn aic(
        &self,
        y: &[f64],
        _n: &[f64],
        _mu: &[f64],
        weights: &[f64],
        residual_deviance: f64,
    ) -> f64 {
        let weight_sum = weights.iter().sum::<f64>();
        let weighted_y_sum = y
            .iter()
            .zip(weights.iter())
            .map(|(y, weight)| y.ln() * weight)
            .sum::<f64>();
        weight_sum * ((residual_deviance / weight_sum * 2.0 * f64::pi()).ln() + 1.0)
            + 3.0 * weighted_y_sum
            + 2.0
    }

    fn initialize(
        &self,
        y: &mut DMatrix<f64>,
        _weights: &mut DMatrix<f64>,
    ) -> (DMatrix<f64>, DMatrix<f64>) {
        let mut n = y.clone();
        n.iter_mut().for_each(|n| *n = 1.0);
        let mu_start = y.clone();
        (n, mu_start)
    }

    fn link(&self) -> L {
        self.link.clone()
    }

    fn set_dispersion(&self) -> Option<f64> {
        None
    }
}
