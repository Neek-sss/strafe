// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use num_traits::Float;

use crate::regression::glm::{
    family::{core::Family, family_c_translation::binomial_dev_resids},
    link::{core::Link, logit::Logit},
};

#[derive(Clone, Debug)]
pub struct QuasiBinomial<L: Link> {
    link: L,
}

impl Default for QuasiBinomial<Logit> {
    fn default() -> Self {
        Self { link: Logit::new() }
    }
}

impl<L: Link + Clone> QuasiBinomial<L> {
    pub fn new(link: &L) -> Self {
        Self { link: link.clone() }
    }
}

impl<L: Link + Clone> Family<L> for QuasiBinomial<L> {
    fn variance(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = mu.clone();
        ret.iter_mut().for_each(|mu| *mu = *mu * (1.0 - *mu));
        ret
    }

    fn valid_mu(&self, mu: &DMatrix<f64>) -> bool {
        mu.iter().all(|mu| mu.is_finite() && *mu > 0.0 && *mu < 1.0)
    }

    fn residual_deviance(
        &self,
        y: &DMatrix<f64>,
        mu: &DMatrix<f64>,
        weights: &DMatrix<f64>,
    ) -> DMatrix<f64> {
        DMatrix::from_iterator(
            y.shape().0,
            y.shape().1,
            binomial_dev_resids(y.as_slice(), mu.as_slice(), weights.as_slice()).into_iter(),
        )
    }

    fn aic(
        &self,
        _y: &[f64],
        _n: &[f64],
        _mu: &[f64],
        _weights: &[f64],
        _residual_deviance: f64,
    ) -> f64 {
        f64::nan()
    }

    fn initialize(
        &self,
        y: &mut DMatrix<f64>,
        weights: &mut DMatrix<f64>,
    ) -> (DMatrix<f64>, DMatrix<f64>) {
        let mut n = y.clone();
        n.iter_mut().for_each(|n| *n = 1.0);
        let mut mu_start = y.clone();
        mu_start
            .iter_mut()
            .zip(weights.iter())
            .for_each(|(y, weights)| *y = (*weights * *y + 0.5) / (*weights + 1.0));
        (n, mu_start)
    }

    fn link(&self) -> L {
        self.link.clone()
    }

    fn set_dispersion(&self) -> Option<f64> {
        None
    }
}
