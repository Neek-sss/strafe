// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;
use num_traits::Float;
use r2rs_nmath::{distribution::GammaBuilder, traits::Distribution};
use strafe_type::FloatConstraint;

use crate::regression::glm::{
    family::core::Family,
    link::{core::Link, inverse::Inverse},
};

#[derive(Clone, Debug)]
pub struct Gamma<L: Link> {
    link: L,
}

impl Default for Gamma<Inverse> {
    fn default() -> Self {
        Self {
            link: Inverse::new(),
        }
    }
}

impl<L: Link + Clone> Gamma<L> {
    pub fn new(link: &L) -> Self {
        Self { link: link.clone() }
    }
}

impl<L: Link + Clone> Family<L> for Gamma<L> {
    fn variance(&self, mu: &DMatrix<f64>) -> DMatrix<f64> {
        let mut ret = mu.clone();
        ret.iter_mut().for_each(|mu| *mu = mu.powi(2));
        ret
    }

    fn valid_mu(&self, mu: &DMatrix<f64>) -> bool {
        mu.iter().all(|mu| mu.is_finite() && *mu > 0.0)
    }

    fn residual_deviance(
        &self,
        y: &DMatrix<f64>,
        mu: &DMatrix<f64>,
        weights: &DMatrix<f64>,
    ) -> DMatrix<f64> {
        let mut ret = y.clone();
        ret.iter_mut()
            .zip(mu.iter())
            .zip(weights.iter())
            .for_each(|((ret, mu), weight)| {
                let y = *ret;
                let mu = *mu;
                let weight = *weight;
                *ret = -2.0 * weight * (if y == 0.0 { 1.0 } else { y / mu }.ln() - (y - mu) / mu);
            });
        ret
    }

    fn aic(
        &self,
        y: &[f64],
        _n: &[f64],
        mu: &[f64],
        weights: &[f64],
        residual_deviance: f64,
    ) -> f64 {
        let n = weights.iter().sum::<f64>();
        let disp = residual_deviance / n;
        if disp == 0.0 {
            f64::nan()
        } else {
            -2.0 * y
                .iter()
                .zip(mu.iter())
                .zip(weights.iter())
                .map(|((y, mu), weight)| {
                    let mut gamma_builder = GammaBuilder::new();
                    gamma_builder.with_shape(1.0 / disp);
                    gamma_builder.with_scale(mu * disp);
                    let gamma = gamma_builder.build();
                    gamma.log_density(y).unwrap() * weight
                })
                .sum::<f64>()
                + 2.0
        }
    }

    fn initialize(
        &self,
        y: &mut DMatrix<f64>,
        _weights: &mut DMatrix<f64>,
    ) -> (DMatrix<f64>, DMatrix<f64>) {
        let mut n = y.clone();
        n.iter_mut().for_each(|n| *n = 1.0);
        let mu_start = y.clone();
        (n, mu_start)
    }

    fn link(&self) -> L {
        self.link.clone()
    }

    fn set_dispersion(&self) -> Option<f64> {
        None
    }
}
