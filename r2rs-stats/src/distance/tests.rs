// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use strafe_testing::{
    r::{RGenerator, RString, RTester},
    r_assert_relative_equal_result,
};

use crate::distance::{
    binary::binary, canberra::canberra, euclidean::euclidean, manhattan::manhattan,
    maximum::maximum, minkowski::minkowski, row_distances,
};

pub fn binary_test_inner(seed: u16, row_size: f64, column_size: f64) {
    let x = RTester::new()
        .set_seed(seed)
        .set_script(&RString::from_string(format!(
            "ret = round(matrix(runif({0}*{1}), ncol={1}, nrow={0}), digits=3);",
            RString::from_f64(row_size),
            RString::from_f64(column_size),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_matrix()
        .expect("R running error");
    let rust_ret = row_distances(&x, &binary);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = dist({}, 'binary', upper=T, diag=T);",
            RString::from_f64_matrix(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_matrix()
        .expect("R running error");
    for (&rust, &r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(r), &rust);
    }
}

pub fn canberra_test_inner(seed: u16, row_size: f64, column_size: f64) {
    let x = RGenerator::new()
        .set_seed(seed)
        .generate_uniform_matrix(row_size, column_size, (0, 1));
    let rust_ret = row_distances(&x, &canberra);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = dist({}, 'canberra', upper=T, diag=T);",
            RString::from_f64_matrix(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_matrix()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn euclidean_test_inner(seed: u16, row_size: f64, column_size: f64) {
    let x = RGenerator::new()
        .set_seed(seed)
        .generate_uniform_matrix(row_size, column_size, (0, 1));
    let rust_ret = row_distances(&x, &euclidean);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = dist({}, 'euclidean', upper=T, diag=T);",
            RString::from_f64_matrix(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_matrix()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn manhattan_test_inner(seed: u16, row_size: f64, column_size: f64) {
    let x = RGenerator::new()
        .set_seed(seed)
        .generate_uniform_matrix(row_size, column_size, (0, 1));
    let rust_ret = row_distances(&x, &manhattan);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = dist({}, 'manhattan', upper=T, diag=T);",
            RString::from_f64_matrix(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_matrix()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn maximum_test_inner(seed: u16, row_size: f64, column_size: f64) {
    let x = RGenerator::new()
        .set_seed(seed)
        .generate_uniform_matrix(row_size, column_size, (0, 1));
    let rust_ret = row_distances(&x, &maximum);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = dist({}, 'maximum', upper=T, diag=T);",
            RString::from_f64_matrix(&x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_matrix()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}

pub fn minkowski_test_inner(seed: u16, p: f64, row_size: f64, column_size: f64) {
    let x = RGenerator::new()
        .set_seed(seed)
        .generate_uniform_matrix(row_size, column_size, (0, 1));
    let rust_ret = row_distances(&x, &|x1, x2| minkowski(x1, x2, p));
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = dist({}, p={}, 'minkowski', upper=T, diag=T);",
            RString::from_f64_matrix(&x),
            RString::from_f64(p),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64_matrix()
        .expect("R running error");
    for (rust, r) in rust_ret.iter().zip(r_ret.iter()) {
        r_assert_relative_equal_result!(Ok(*r), rust);
    }
}
