// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Zero;

pub fn binary(row1: &[&f64], row2: &[&f64]) -> f64 {
    let mut distance = 0.0;
    for (&x1, &x2) in row1.iter().zip(row2.iter()) {
        if (x2.is_zero() || x1.is_zero()) && (!x2.is_zero() || !x1.is_zero()) {
            distance += 1.0;
        }
    }
    distance /= row1.len() as f64;
    distance
}
