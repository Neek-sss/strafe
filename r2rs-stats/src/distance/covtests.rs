// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn binary_test() {
    binary_test_inner(1, 2.0, 2.0);
}

#[test]
fn canberra_test() {
    canberra_test_inner(1, 2.0, 2.0);
}

#[test]
fn euclidean_test() {
    euclidean_test_inner(1, 2.0, 2.0);
}

#[test]
fn manhattan_test() {
    manhattan_test_inner(1, 2.0, 2.0);
}

#[test]
fn maximum_test() {
    maximum_test_inner(1, 2.0, 2.0);
}

#[test]
fn minkowski_test() {
    minkowski_test_inner(1, 1.0, 2.0, 2.0);
}
