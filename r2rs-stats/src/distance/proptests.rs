// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_rat64, stat_w64, strafe_default_proptest_options},
};

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn binary_test(
        seed in 0..std::u16::MAX,
        row_size in stat_w64(Some(2.0), Some(12.0), None),
        column_size in stat_w64(Some(2.0), Some(12.0), None),
    ) {
        binary_test_inner(seed, row_size, column_size);
    }

    #[test]
    fn canberra_test(
        seed in 0..std::u16::MAX,
        row_size in stat_w64(Some(2.0), Some(12.0), None),
        column_size in stat_w64(Some(2.0), Some(12.0), None),
    ) {
        canberra_test_inner(seed, row_size, column_size);
    }

    #[test]
    fn euclidean_test(
        seed in 0..std::u16::MAX,
        row_size in stat_w64(Some(2.0), Some(12.0), None),
        column_size in stat_w64(Some(2.0), Some(12.0), None),
    ) {
        euclidean_test_inner(seed, row_size, column_size);
    }

    #[test]
    fn manhattan_test(
        seed in 0..std::u16::MAX,
        row_size in stat_w64(Some(2.0), Some(12.0), None),
        column_size in stat_w64(Some(2.0), Some(12.0), None),
    ) {
        manhattan_test_inner(seed, row_size, column_size);
    }

    #[test]
    fn maximum_test(
        seed in 0..std::u16::MAX,
        row_size in stat_w64(Some(2.0), Some(12.0), None),
        column_size in stat_w64(Some(2.0), Some(12.0), None),
    ) {
        maximum_test_inner(seed, row_size, column_size);
    }

    #[test]
    fn minkowski_test(
        seed in 0..std::u16::MAX,
        p in stat_rat64(None, Some(10.0), None),
        row_size in stat_w64(Some(2.0), Some(12.0), None),
        column_size in stat_w64(Some(2.0), Some(12.0), None),
    ) {
        minkowski_test_inner(seed, p, row_size, column_size);
    }
}
