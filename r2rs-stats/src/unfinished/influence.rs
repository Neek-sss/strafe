use crate::{
    optim::getListElement,
    sexprec::{SEXP, SEXPTYPE},
};
use ::libc;
extern "C" {
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn SET_STRING_ELT(x: SEXP, i: R_xlen_t, v: SEXP);
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    #[no_mangle]
    static mut R_NamesSymbol: SEXP;
    #[no_mangle]
    fn asLogical(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn alloc3DArray(_: SEXPTYPE, _: libc::c_int, _: libc::c_int, _: libc::c_int) -> SEXP;
    #[no_mangle]
    fn allocMatrix(_: SEXPTYPE, _: libc::c_int, _: libc::c_int) -> SEXP;
    #[no_mangle]
    fn mkChar(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn ncols(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn nrows(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn setAttrib(_: SEXP, _: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 2012   The R Core Team.
     *
     *  This program is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* auxiliary */

    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 2005-2019  The R Core Team
     *
     *  This program is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* definitions not involving SEXPs, plus _() */
    #[no_mangle]
    fn lminfl_(
        x: *mut libc::c_double,
        ldx: *mut libc::c_int,
        n: *mut libc::c_int,
        k: *mut libc::c_int,
        q: *mut libc::c_int,
        docoef: *mut libc::c_int,
        qraux: *mut libc::c_double,
        resid: *mut libc::c_double,
        hat: *mut libc::c_double,
        coef: *mut libc::c_double,
        sigma: *mut libc::c_double,
        tol: *mut libc::c_double,
    );
}
pub type ptrdiff_t = libc::c_long;
pub type R_xlen_t = ptrdiff_t;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2012--2018 The R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
#[no_mangle]
pub unsafe extern "C" fn influence(
    mut mqr: SEXP,
    mut do_coef: SEXP,
    mut e: SEXP,
    mut stol: SEXP,
) -> SEXP {
    let mut qr: SEXP = getListElement(
        mqr,
        b"qr\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
    ); // <- numeric(0)
    let mut qraux: SEXP = getListElement(
        mqr,
        b"qraux\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
    );
    let mut n: libc::c_int = nrows(qr);
    let mut k: libc::c_int = asInteger(getListElement(
        mqr,
        b"rank\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
    ));
    let mut q: libc::c_int = ncols(e);
    let mut docoef: libc::c_int = asLogical(do_coef);
    let mut tol: libc::c_double = asReal(stol);
    let mut hat: SEXP = protect(allocVector(14 as libc::c_int as SEXPTYPE, n as R_xlen_t));
    let mut rh: *mut libc::c_double = REAL(hat);
    let mut coefficients: SEXP = protect(if docoef != 0 {
        alloc3DArray(14 as libc::c_int as SEXPTYPE, n, k, q)
    } else {
        allocVector(14 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t)
    });
    let mut sigma: SEXP = protect(allocMatrix(14 as libc::c_int as SEXPTYPE, n, q));
    lminfl_(
        REAL(qr),
        &mut n,
        &mut n,
        &mut k,
        &mut q,
        &mut docoef,
        REAL(qraux),
        REAL(e),
        rh,
        REAL(coefficients),
        REAL(sigma),
        &mut tol,
    );
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < n {
        if *rh.offset(i as isize) > 1.0f64 - tol {
            *rh.offset(i as isize) = 1.0f64
        }
        i += 1
    }
    let mut ans: SEXP = protect(allocVector(
        19 as libc::c_int as SEXPTYPE,
        if docoef != 0 {
            3 as libc::c_int
        } else {
            2 as libc::c_int
        } as R_xlen_t,
    ));
    let mut nm: SEXP = allocVector(
        16 as libc::c_int as SEXPTYPE,
        if docoef != 0 {
            3 as libc::c_int
        } else {
            2 as libc::c_int
        } as R_xlen_t,
    );
    setAttrib(ans, R_NamesSymbol, nm);
    let mut m: libc::c_int = 0 as libc::c_int;
    SET_VECTOR_ELT(ans, m as R_xlen_t, hat);
    let fresh0 = m;
    m = m + 1;
    SET_STRING_ELT(
        nm,
        fresh0 as R_xlen_t,
        mkChar(b"hat\x00" as *const u8 as *const libc::c_char),
    );
    if docoef != 0 {
        SET_VECTOR_ELT(ans, m as R_xlen_t, coefficients);
        let fresh1 = m;
        m = m + 1;
        SET_STRING_ELT(
            nm,
            fresh1 as R_xlen_t,
            mkChar(b"coefficients\x00" as *const u8 as *const libc::c_char),
        );
    }
    SET_VECTOR_ELT(ans, m as R_xlen_t, sigma);
    let fresh2 = m;
    m = m + 1;
    SET_STRING_ELT(
        nm,
        fresh2 as R_xlen_t,
        mkChar(b"sigma\x00" as *const u8 as *const libc::c_char),
    );
    /* unneeded :
    SET_VECTOR_ELT(ans, m, e);
    SET_STRING_ELT(nm, m, mkChar("wt.res"));
    */
    unprotect(4 as libc::c_int);
    return ans;
}
