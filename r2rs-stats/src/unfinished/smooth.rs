use crate::sexprec::{SEXP, SEXPTYPE};
use ::libc;
extern "C" {

    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn XLENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn SET_STRING_ELT(x: SEXP, i: R_xlen_t, v: SEXP);
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    #[no_mangle]
    static mut R_NamesSymbol: SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn mkChar(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn setAttrib(_: SEXP, _: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn ScalarInteger(_: libc::c_int) -> SEXP;
    #[no_mangle]
    fn ScalarLogical(_: libc::c_int) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
pub type R_xlen_t = ptrdiff_t;
pub type C2RustUnnamed = libc::c_uint;
pub const sm_TUKEY_ENDRULE: C2RustUnnamed = 2;
pub const sm_COPY_ENDRULE: C2RustUnnamed = 1;
pub const sm_NO_ENDRULE: C2RustUnnamed = 0;
unsafe extern "C" fn med3(
    mut u: libc::c_double,
    mut v: libc::c_double,
    mut w: libc::c_double,
) -> libc::c_double {
    /*, MAYBE */
    /* Median(u,v,w): */
    if u <= v && v <= w || u >= v && v >= w {
        return v;
    }
    if u <= w && w <= v || u >= w && w >= v {
        return w;
    }
    /* else */
    return u;
}
/* Note: Velleman & Hoaglin use a smarter version,  which returns "change"
   ----
   and change = TRUE, when  med3(u,v,w) != v   ==> makes "R" (in "3R") faster
*/
unsafe extern "C" fn imed3(
    mut u: libc::c_double,
    mut v: libc::c_double,
    mut w: libc::c_double,
) -> libc::c_int {
    /* Return (Index-1) of  median(u,v,w) , i.e.,
       -1 : u
    0 : v
    1 : w
     */
    if u <= v && v <= w || u >= v && v >= w {
        return 0 as libc::c_int;
    }
    if u <= w && w <= v || u >= w && w >= v {
        return 1 as libc::c_int;
    }
    /* else */
    return -(1 as libc::c_int);
}
unsafe extern "C" fn sm_3(
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut n: R_xlen_t,
    mut end_rule: libc::c_int,
) -> Rboolean {
    /* y[] := Running Median of three (x) = "3 (x[])" with "copy ends"
     * ---  return chg := ( y != x ) */
    let mut i: R_xlen_t = 0;
    let mut j: libc::c_int = 0;
    let mut chg: Rboolean = FALSE;
    if n <= 2 as libc::c_int as libc::c_long {
        i = 0 as libc::c_int as R_xlen_t;
        while i < n {
            *y.offset(i as isize) = *x.offset(i as isize);
            i += 1
        }
        return FALSE;
    }
    i = 1 as libc::c_int as R_xlen_t;
    while i < n - 1 as libc::c_int as libc::c_long {
        j = imed3(
            *x.offset((i - 1 as libc::c_int as libc::c_long) as isize),
            *x.offset(i as isize),
            *x.offset((i + 1 as libc::c_int as libc::c_long) as isize),
        );
        *y.offset(i as isize) = *x.offset((i + j as libc::c_long) as isize);
        chg = (chg as libc::c_uint != 0 || j != 0) as libc::c_int as Rboolean;
        i += 1
    }
    /* [y, chg]  :=  sm_DO_ENDRULE(x, y, end_rule, chg) : */
    /* do nothing : don't even assign them */
    /* 1 */
    /* 2 */
    match end_rule {
        0 => {}
        1 => {
            *y.offset(0 as libc::c_int as isize) = *x.offset(0 as libc::c_int as isize);
            *y.offset((n - 1 as libc::c_int as libc::c_long) as isize) =
                *x.offset((n - 1 as libc::c_int as libc::c_long) as isize)
        }
        2 => {
            *y.offset(0 as libc::c_int as isize) = med3(
                3 as libc::c_int as libc::c_double * *y.offset(1 as libc::c_int as isize)
                    - 2 as libc::c_int as libc::c_double * *y.offset(2 as libc::c_int as isize),
                *x.offset(0 as libc::c_int as isize),
                *y.offset(1 as libc::c_int as isize),
            );
            chg = (chg as libc::c_uint != 0
                || *y.offset(0 as libc::c_int as isize) != *x.offset(0 as libc::c_int as isize))
                as libc::c_int as Rboolean;
            *y.offset((n - 1 as libc::c_int as libc::c_long) as isize) = med3(
                *y.offset((n - 2 as libc::c_int as libc::c_long) as isize),
                *x.offset((n - 1 as libc::c_int as libc::c_long) as isize),
                3 as libc::c_int as libc::c_double
                    * *y.offset((n - 2 as libc::c_int as libc::c_long) as isize)
                    - 2 as libc::c_int as libc::c_double
                        * *y.offset((n - 3 as libc::c_int as libc::c_long) as isize),
            );
            chg = (chg as libc::c_uint != 0
                || *y.offset((n - 1 as libc::c_int as libc::c_long) as isize)
                    != *x.offset((n - 1 as libc::c_int as libc::c_long) as isize))
                as libc::c_int as Rboolean
        }
        _ => {
            error(
                dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"invalid end-rule for running median of 3: %d\x00" as *const u8
                        as *const libc::c_char,
                    5 as libc::c_int,
                ),
                end_rule,
            );
        }
    }
    return chg;
}
unsafe extern "C" fn sm_3R(
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut z: *mut libc::c_double,
    mut n: R_xlen_t,
    mut end_rule: libc::c_int,
) -> libc::c_int {
    /* y[] := "3R"(x) ; 3R = Median of three, repeated until convergence */
    let mut iter: libc::c_int = 0;
    let mut chg: Rboolean = FALSE;
    chg = sm_3(x, y, n, sm_COPY_ENDRULE as libc::c_int);
    iter = chg as libc::c_int;
    while chg as u64 != 0 {
        chg = sm_3(y, z, n, sm_NO_ENDRULE as libc::c_int);
        if chg as u64 != 0 {
            iter += 1;
            let mut i: R_xlen_t = 1 as libc::c_int as R_xlen_t;
            while i < n - 1 as libc::c_int as libc::c_long {
                *y.offset(i as isize) = *z.offset(i as isize);
                i += 1
            }
        }
    }
    if n > 2 as libc::c_int as libc::c_long {
        match end_rule {
            0 => {}
            1 => {
                *y.offset(0 as libc::c_int as isize) = *x.offset(0 as libc::c_int as isize);
                *y.offset((n - 1 as libc::c_int as libc::c_long) as isize) =
                    *x.offset((n - 1 as libc::c_int as libc::c_long) as isize)
            }
            2 => {
                *y.offset(0 as libc::c_int as isize) = med3(
                    3 as libc::c_int as libc::c_double * *y.offset(1 as libc::c_int as isize)
                        - 2 as libc::c_int as libc::c_double * *y.offset(2 as libc::c_int as isize),
                    *x.offset(0 as libc::c_int as isize),
                    *y.offset(1 as libc::c_int as isize),
                );
                chg = (chg as libc::c_uint != 0
                    || *y.offset(0 as libc::c_int as isize) != *x.offset(0 as libc::c_int as isize))
                    as libc::c_int as Rboolean;
                *y.offset((n - 1 as libc::c_int as libc::c_long) as isize) = med3(
                    *y.offset((n - 2 as libc::c_int as libc::c_long) as isize),
                    *x.offset((n - 1 as libc::c_int as libc::c_long) as isize),
                    3 as libc::c_int as libc::c_double
                        * *y.offset((n - 2 as libc::c_int as libc::c_long) as isize)
                        - 2 as libc::c_int as libc::c_double
                            * *y.offset((n - 3 as libc::c_int as libc::c_long) as isize),
                );
                chg = (chg as libc::c_uint != 0
                    || *y.offset((n - 1 as libc::c_int as libc::c_long) as isize)
                        != *x.offset((n - 1 as libc::c_int as libc::c_long) as isize))
                    as libc::c_int as Rboolean
            }
            _ => {
                error(
                    dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"invalid end-rule for running median of 3: %d\x00" as *const u8
                            as *const libc::c_char,
                        5 as libc::c_int,
                    ),
                    end_rule,
                );
            }
        }
    }
    /* =>  chg = TRUE  iff  ends changed */
    return if iter != 0 {
        iter as libc::c_uint
    } else {
        chg as libc::c_uint
    } as libc::c_int;
    /* = 0   <==>  only one "3" w/o any change
       = 1   <==>  either ["3" w/o change + endchange]
           or   [two "3"s, 2nd w/o change  ]
    */
}
unsafe extern "C" fn sptest(mut x: *mut libc::c_double, mut i: R_xlen_t) -> Rboolean {
    /* Split test:
      Are we at a /-\ or \_/ location => split should be made ?
    */
    if *x.offset(i as isize) != *x.offset((i + 1 as libc::c_int as libc::c_long) as isize) {
        return FALSE;
    }
    if *x.offset((i - 1 as libc::c_int as libc::c_long) as isize) <= *x.offset(i as isize)
        && *x.offset((i + 1 as libc::c_int as libc::c_long) as isize)
            <= *x.offset((i + 2 as libc::c_int as libc::c_long) as isize)
        || *x.offset((i - 1 as libc::c_int as libc::c_long) as isize) >= *x.offset(i as isize)
            && *x.offset((i + 1 as libc::c_int as libc::c_long) as isize)
                >= *x.offset((i + 2 as libc::c_int as libc::c_long) as isize)
    {
        return FALSE;
    }
    /* else */
    return TRUE;
}
unsafe extern "C" fn sm_split3(
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut n: R_xlen_t,
    mut do_ends: Rboolean,
) -> Rboolean {
    /* y[] := S(x[])  where S() = "sm_split3"  */
    let mut i: R_xlen_t = 0;
    let mut chg: Rboolean = FALSE;
    i = 0 as libc::c_int as R_xlen_t;
    while i < n {
        *y.offset(i as isize) = *x.offset(i as isize);
        i += 1
    }
    if n <= 4 as libc::c_int as libc::c_long {
        return FALSE;
    }
    /* Colin Goodall doesn't do splits near ends
    in spl() in Statlib's "smoother" code !! */
    if do_ends as libc::c_uint != 0 && sptest(x, 1 as libc::c_int as R_xlen_t) as libc::c_uint != 0
    {
        chg = TRUE;
        *y.offset(1 as libc::c_int as isize) = *x.offset(0 as libc::c_int as isize);
        *y.offset(2 as libc::c_int as isize) = med3(
            *x.offset(2 as libc::c_int as isize),
            *x.offset(3 as libc::c_int as isize),
            3 as libc::c_int as libc::c_double * *x.offset(3 as libc::c_int as isize)
                - 2 as libc::c_int as libc::c_double * *x.offset(4 as libc::c_int as isize),
        )
    }
    i = 2 as libc::c_int as R_xlen_t;
    while i < n - 3 as libc::c_int as libc::c_long {
        if sptest(x, i) as u64 != 0 {
            /* plateau at x[i] == x[i+1] */
            let mut j: libc::c_int = 0;
            /* at left : */
            j = imed3(
                *x.offset(i as isize),
                *x.offset((i - 1 as libc::c_int as libc::c_long) as isize),
                3 as libc::c_int as libc::c_double
                    * *x.offset((i - 1 as libc::c_int as libc::c_long) as isize)
                    - 2 as libc::c_int as libc::c_double
                        * *x.offset((i - 2 as libc::c_int as libc::c_long) as isize),
            );
            if (-(1 as libc::c_int)) < j {
                *y.offset(i as isize) = if j == 0 as libc::c_int {
                    *x.offset((i - 1 as libc::c_int as libc::c_long) as isize)
                } else {
                    (3 as libc::c_int as libc::c_double
                        * *x.offset((i - 1 as libc::c_int as libc::c_long) as isize))
                        - 2 as libc::c_int as libc::c_double
                            * *x.offset((i - 2 as libc::c_int as libc::c_long) as isize)
                };
                chg = (*y.offset(i as isize) != *x.offset(i as isize)) as libc::c_int as Rboolean
            }
            /* at right : */
            j = imed3(
                *x.offset((i + 1 as libc::c_int as libc::c_long) as isize),
                *x.offset((i + 2 as libc::c_int as libc::c_long) as isize),
                3 as libc::c_int as libc::c_double
                    * *x.offset((i + 2 as libc::c_int as libc::c_long) as isize)
                    - 2 as libc::c_int as libc::c_double
                        * *x.offset((i + 3 as libc::c_int as libc::c_long) as isize),
            );
            if (-(1 as libc::c_int)) < j {
                *y.offset((i + 1 as libc::c_int as libc::c_long) as isize) =
                    if j == 0 as libc::c_int {
                        *x.offset((i + 2 as libc::c_int as libc::c_long) as isize)
                    } else {
                        (3 as libc::c_int as libc::c_double
                            * *x.offset((i + 2 as libc::c_int as libc::c_long) as isize))
                            - 2 as libc::c_int as libc::c_double
                                * *x.offset((i + 3 as libc::c_int as libc::c_long) as isize)
                    };
                chg = (*y.offset((i + 1 as libc::c_int as libc::c_long) as isize)
                    != *x.offset((i + 1 as libc::c_int as libc::c_long) as isize))
                    as libc::c_int as Rboolean
            }
        }
        i += 1
    }
    if do_ends as libc::c_uint != 0
        && sptest(x, n - 3 as libc::c_int as libc::c_long) as libc::c_uint != 0
    {
        chg = TRUE;
        *y.offset((n - 2 as libc::c_int as libc::c_long) as isize) =
            *x.offset((n - 1 as libc::c_int as libc::c_long) as isize);
        *y.offset((n - 3 as libc::c_int as libc::c_long) as isize) = med3(
            *x.offset((n - 3 as libc::c_int as libc::c_long) as isize),
            *x.offset((n - 4 as libc::c_int as libc::c_long) as isize),
            3 as libc::c_int as libc::c_double
                * *x.offset((n - 4 as libc::c_int as libc::c_long) as isize)
                - 2 as libc::c_int as libc::c_double
                    * *x.offset((n - 5 as libc::c_int as libc::c_long) as isize),
        )
    }
    return chg;
}
unsafe extern "C" fn sm_3RS3R(
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut z: *mut libc::c_double,
    mut w: *mut libc::c_double,
    mut n: R_xlen_t,
    mut end_rule: libc::c_int,
    mut split_ends: Rboolean,
) -> libc::c_int {
    /* y[1:n] := "3R S 3R"(x[1:n]);  z = "work"; */
    let mut iter: libc::c_int = 0;
    let mut chg: Rboolean = FALSE;
    iter = sm_3R(x, y, z, n, end_rule);
    chg = sm_split3(y, z, n, split_ends);
    if chg as u64 != 0 {
        iter += sm_3R(z, y, w, n, end_rule)
    }
    /* else y == z already */
    return iter + chg as libc::c_int;
}
unsafe extern "C" fn sm_3RSS(
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut z: *mut libc::c_double,
    mut n: R_xlen_t,
    mut end_rule: libc::c_int,
    mut split_ends: Rboolean,
) -> libc::c_int {
    /* y[1:n] := "3RSS"(x[1:n]);  z = "work"; */
    let mut iter: libc::c_int = 0;
    let mut chg: Rboolean = FALSE;
    iter = sm_3R(x, y, z, n, end_rule);
    chg = sm_split3(y, z, n, split_ends);
    if chg as u64 != 0 {
        sm_split3(z, y, n, split_ends);
    }
    /* else  y == z already */
    return iter + chg as libc::c_int;
}
unsafe extern "C" fn sm_3RSR(
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut z: *mut libc::c_double,
    mut w: *mut libc::c_double,
    mut n: R_xlen_t,
    mut end_rule: libc::c_int,
    mut split_ends: Rboolean,
) -> libc::c_int {
    /* y[1:n] := "3RSR"(x[1:n]);  z := residuals; w = "work"; */
    /*== "SR" (as follows) is stupid ! (MM) ==*/
    let mut i: R_xlen_t = 0; /* INF.LOOP stopper */
    let mut iter: libc::c_int = 0;
    let mut chg: Rboolean = FALSE;
    let mut ch2: Rboolean = FALSE;
    iter = sm_3R(x, y, z, n, end_rule);
    loop {
        iter += 1;
        chg = sm_split3(y, z, n, split_ends);
        ch2 = sm_3R(z, y, w, n, end_rule) as Rboolean;
        chg = (chg as libc::c_uint != 0 || ch2 as libc::c_uint != 0) as libc::c_int as Rboolean;
        if chg as u64 == 0 {
            break;
        }
        if iter as libc::c_long > 2 as libc::c_int as libc::c_long * n {
            break;
        }
        i = 0 as libc::c_int as R_xlen_t;
        while i < n {
            *z.offset(i as isize) = *x.offset(i as isize) - *y.offset(i as isize);
            i += 1
        }
        if !(chg as u64 != 0) {
            break;
        }
    }
    return iter;
}
/*-------- These are  called from R : -----------*/
#[no_mangle]
pub unsafe extern "C" fn Rsm(mut x: SEXP, mut stype: SEXP, mut send: SEXP) -> SEXP {
    let mut iend: libc::c_int = asInteger(send);
    let mut type_0: libc::c_int = asInteger(stype);
    let mut n: R_xlen_t = XLENGTH(x);
    let mut ans: SEXP = protect(allocVector(
        19 as libc::c_int as SEXPTYPE,
        2 as libc::c_int as R_xlen_t,
    ));
    let mut y: SEXP = allocVector(14 as libc::c_int as SEXPTYPE, n);
    SET_VECTOR_ELT(ans, 0 as libc::c_int as R_xlen_t, y);
    let mut nm: SEXP = allocVector(16 as libc::c_int as SEXPTYPE, 2 as libc::c_int as R_xlen_t);
    setAttrib(ans, R_NamesSymbol, nm);
    SET_STRING_ELT(
        nm,
        0 as libc::c_int as R_xlen_t,
        mkChar(b"y\x00" as *const u8 as *const libc::c_char),
    );
    if type_0 <= 5 as libc::c_int {
        /* -Wall */
        let mut iter: libc::c_int = 0 as libc::c_int;
        match type_0 {
            1 => {
                let mut z: *mut libc::c_double = R_alloc(
                    n as size_t,
                    ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
                ) as *mut libc::c_double;
                let mut w: *mut libc::c_double = R_alloc(
                    n as size_t,
                    ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
                ) as *mut libc::c_double;
                iter = sm_3RS3R(
                    REAL(x),
                    REAL(y),
                    z,
                    w,
                    n,
                    iend.abs(),
                    if iend < 0 as libc::c_int {
                        TRUE as libc::c_int
                    } else {
                        FALSE as libc::c_int
                    } as Rboolean,
                )
            }
            2 => {
                let mut z_0: *mut libc::c_double = R_alloc(
                    n as size_t,
                    ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
                ) as *mut libc::c_double;
                iter = sm_3RSS(
                    REAL(x),
                    REAL(y),
                    z_0,
                    n,
                    iend.abs(),
                    if iend < 0 as libc::c_int {
                        TRUE as libc::c_int
                    } else {
                        FALSE as libc::c_int
                    } as Rboolean,
                )
            }
            3 => {
                let mut z_1: *mut libc::c_double = R_alloc(
                    n as size_t,
                    ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
                ) as *mut libc::c_double;
                let mut w_0: *mut libc::c_double = R_alloc(
                    n as size_t,
                    ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
                ) as *mut libc::c_double;
                iter = sm_3RSR(
                    REAL(x),
                    REAL(y),
                    z_1,
                    w_0,
                    n,
                    iend.abs(),
                    if iend < 0 as libc::c_int {
                        TRUE as libc::c_int
                    } else {
                        FALSE as libc::c_int
                    } as Rboolean,
                )
            }
            4 => {
                // "3R"
                let mut z_2: *mut libc::c_double = R_alloc(
                    n as size_t,
                    ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
                ) as *mut libc::c_double;
                iter = sm_3R(REAL(x), REAL(y), z_2, n, iend)
            }
            5 => {
                // "3"
                iter = sm_3(REAL(x), REAL(y), n, iend) as libc::c_int
            }
            _ => {}
        }
        SET_VECTOR_ELT(ans, 1 as libc::c_int as R_xlen_t, ScalarInteger(iter));
        SET_STRING_ELT(
            nm,
            1 as libc::c_int as R_xlen_t,
            mkChar(b"iter\x00" as *const u8 as *const libc::c_char),
        );
    } else {
        let mut changed: libc::c_int =
            sm_split3(REAL(x), REAL(y), n, iend as Rboolean) as libc::c_int;
        SET_VECTOR_ELT(ans, 1 as libc::c_int as R_xlen_t, ScalarLogical(changed));
        SET_STRING_ELT(
            nm,
            1 as libc::c_int as R_xlen_t,
            mkChar(b"changed\x00" as *const u8 as *const libc::c_char),
        );
    }
    unprotect(1 as libc::c_int);
    return ans;
}
