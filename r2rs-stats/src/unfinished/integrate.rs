use crate::sexprec::{SEXP, SEXPREC, SEXPTYPE};
use ::libc;
extern "C" {
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn TYPEOF(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn INTEGER(x: SEXP) -> *mut libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn VECTOR_ELT(x: SEXP, i: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn SET_STRING_ELT(x: SEXP, i: R_xlen_t, v: SEXP);
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    #[no_mangle]
    fn CAR(e: SEXP) -> SEXP;
    #[no_mangle]
    fn CDR(e: SEXP) -> SEXP;
    #[no_mangle]
    static mut R_NamesSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn eval(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn mkChar(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn setAttrib(_: SEXP, _: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn lang2(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn length(_: SEXP) -> R_len_t;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
    #[no_mangle]
    fn Rdqags(
        f: Option<integr_fn>,
        ex: *mut libc::c_void,
        a: *mut libc::c_double,
        b: *mut libc::c_double,
        epsabs: *mut libc::c_double,
        epsrel: *mut libc::c_double,
        result: *mut libc::c_double,
        abserr: *mut libc::c_double,
        neval: *mut libc::c_int,
        ier: *mut libc::c_int,
        limit: *mut libc::c_int,
        lenw: *mut libc::c_int,
        last: *mut libc::c_int,
        iwork: *mut libc::c_int,
        work: *mut libc::c_double,
    );
    #[no_mangle]
    fn Rdqagi(
        f: Option<integr_fn>,
        ex: *mut libc::c_void,
        bound: *mut libc::c_double,
        inf: *mut libc::c_int,
        epsabs: *mut libc::c_double,
        epsrel: *mut libc::c_double,
        result: *mut libc::c_double,
        abserr: *mut libc::c_double,
        neval: *mut libc::c_int,
        ier: *mut libc::c_int,
        limit: *mut libc::c_int,
        lenw: *mut libc::c_int,
        last: *mut libc::c_int,
        iwork: *mut libc::c_int,
        work: *mut libc::c_double,
    );
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
pub type R_len_t = libc::c_int;
pub type R_xlen_t = ptrdiff_t;
pub type integr_fn =
    unsafe extern "C" fn(_: *mut libc::c_double, _: libc::c_int, _: *mut libc::c_void) -> ();
#[derive(Copy, Clone)]
#[repr(C)]
pub struct int_struct {
    pub f: SEXP,
    pub env: SEXP,
}
pub type IntStruct = *mut int_struct;
/* This is *the* ``integr_fn f'' used when called from R : */
unsafe extern "C" fn Rint(
    mut x: *mut libc::c_double,
    mut n: libc::c_int,
    mut ex: *mut libc::c_void,
) {
    let mut args: SEXP = 0 as *mut SEXPREC;
    let mut resultsxp: SEXP = 0 as *mut SEXPREC;
    let mut tmp: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut IS: IntStruct = ex as IntStruct;
    args = allocVector(14 as libc::c_int as SEXPTYPE, n as R_xlen_t);
    protect(args);
    i = 0 as libc::c_int;
    while i < n {
        *REAL(args).offset(i as isize) = *x.offset(i as isize);
        i += 1
    }
    tmp = lang2((*IS).f, args);
    protect(tmp);
    resultsxp = eval(tmp, (*IS).env);
    protect(resultsxp);
    if length(resultsxp) != n {
        error(
            b"evaluation of function gave a result of wrong length\x00" as *const u8
                as *const libc::c_char,
        );
    }
    if TYPEOF(resultsxp) == 13 as libc::c_int {
        resultsxp = coerceVector(resultsxp, 14 as libc::c_int as SEXPTYPE)
    } else if TYPEOF(resultsxp) != 14 as libc::c_int {
        error(
            b"evaluation of function gave a result of wrong type\x00" as *const u8
                as *const libc::c_char,
        );
    }
    i = 0 as libc::c_int;
    while i < n {
        *x.offset(i as isize) = *REAL(resultsxp).offset(i as isize);
        if (*x.offset(i as isize)).is_finite() as i32 == 0 {
            error(b"non-finite function value\x00" as *const u8 as *const libc::c_char);
        }
        i += 1
    }
    unprotect(3 as libc::c_int);
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2001-2016  the R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
// for isfinite
/* called via .External(.) :*/
#[no_mangle]
pub unsafe extern "C" fn call_dqags(mut args: SEXP) -> SEXP {
    let mut is: int_struct = int_struct {
        f: 0 as *mut SEXPREC,
        env: 0 as *mut SEXPREC,
    };
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut ansnames: SEXP = 0 as *mut SEXPREC;
    let mut lower: libc::c_double = 0.;
    let mut upper: libc::c_double = 0.;
    let mut epsabs: libc::c_double = 0.;
    let mut epsrel: libc::c_double = 0.;
    let mut result: libc::c_double = 0.;
    let mut abserr: libc::c_double = 0.;
    let mut work: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut neval: libc::c_int = 0;
    let mut ier: libc::c_int = 0;
    let mut limit: libc::c_int = 0;
    let mut lenw: libc::c_int = 0;
    let mut last: libc::c_int = 0;
    let mut iwork: *mut libc::c_int = 0 as *mut libc::c_int;
    args = CDR(args);
    is.f = CAR(args);
    args = CDR(args);
    is.env = CAR(args);
    args = CDR(args);
    if length(CAR(args)) > 1 as libc::c_int {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"\'%s\' must be of length one\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            b"lower\x00" as *const u8 as *const libc::c_char,
        );
    }
    lower = asReal(CAR(args));
    args = CDR(args);
    if length(CAR(args)) > 1 as libc::c_int {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"\'%s\' must be of length one\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            b"upper\x00" as *const u8 as *const libc::c_char,
        );
    }
    upper = asReal(CAR(args));
    args = CDR(args);
    epsabs = asReal(CAR(args));
    args = CDR(args);
    epsrel = asReal(CAR(args));
    args = CDR(args);
    limit = asInteger(CAR(args));
    args = CDR(args);
    lenw = 4 as libc::c_int * limit;
    iwork = R_alloc(
        limit as size_t,
        ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_int;
    work = R_alloc(
        lenw as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    Rdqags(
        Some(
            Rint as unsafe extern "C" fn(
                _: *mut libc::c_double,
                _: libc::c_int,
                _: *mut libc::c_void,
            ) -> (),
        ),
        &mut is as *mut int_struct as *mut libc::c_void,
        &mut lower,
        &mut upper,
        &mut epsabs,
        &mut epsrel,
        &mut result,
        &mut abserr,
        &mut neval,
        &mut ier,
        &mut limit,
        &mut lenw,
        &mut last,
        iwork,
        work,
    );
    ans = allocVector(19 as libc::c_int as SEXPTYPE, 4 as libc::c_int as R_xlen_t);
    protect(ans);
    ansnames = allocVector(16 as libc::c_int as SEXPTYPE, 4 as libc::c_int as R_xlen_t);
    protect(ansnames);
    SET_STRING_ELT(
        ansnames,
        0 as libc::c_int as R_xlen_t,
        mkChar(b"value\x00" as *const u8 as *const libc::c_char),
    );
    SET_VECTOR_ELT(
        ans,
        0 as libc::c_int as R_xlen_t,
        allocVector(14 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    *REAL(VECTOR_ELT(ans, 0 as libc::c_int as R_xlen_t)).offset(0 as libc::c_int as isize) = result;
    SET_STRING_ELT(
        ansnames,
        1 as libc::c_int as R_xlen_t,
        mkChar(b"abs.error\x00" as *const u8 as *const libc::c_char),
    );
    SET_VECTOR_ELT(
        ans,
        1 as libc::c_int as R_xlen_t,
        allocVector(14 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    *REAL(VECTOR_ELT(ans, 1 as libc::c_int as R_xlen_t)).offset(0 as libc::c_int as isize) = abserr;
    SET_STRING_ELT(
        ansnames,
        2 as libc::c_int as R_xlen_t,
        mkChar(b"subdivisions\x00" as *const u8 as *const libc::c_char),
    );
    SET_VECTOR_ELT(
        ans,
        2 as libc::c_int as R_xlen_t,
        allocVector(13 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    *INTEGER(VECTOR_ELT(ans, 2 as libc::c_int as R_xlen_t)).offset(0 as libc::c_int as isize) =
        last;
    SET_STRING_ELT(
        ansnames,
        3 as libc::c_int as R_xlen_t,
        mkChar(b"ierr\x00" as *const u8 as *const libc::c_char),
    );
    SET_VECTOR_ELT(
        ans,
        3 as libc::c_int as R_xlen_t,
        allocVector(13 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    *INTEGER(VECTOR_ELT(ans, 3 as libc::c_int as R_xlen_t)).offset(0 as libc::c_int as isize) = ier;
    setAttrib(ans, R_NamesSymbol, ansnames);
    unprotect(2 as libc::c_int);
    return ans;
}
#[no_mangle]
pub unsafe extern "C" fn call_dqagi(mut args: SEXP) -> SEXP {
    let mut is: int_struct = int_struct {
        f: 0 as *mut SEXPREC,
        env: 0 as *mut SEXPREC,
    };
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut ansnames: SEXP = 0 as *mut SEXPREC;
    let mut bound: libc::c_double = 0.;
    let mut epsabs: libc::c_double = 0.;
    let mut epsrel: libc::c_double = 0.;
    let mut result: libc::c_double = 0.;
    let mut abserr: libc::c_double = 0.;
    let mut work: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut inf: libc::c_int = 0;
    let mut neval: libc::c_int = 0;
    let mut ier: libc::c_int = 0;
    let mut limit: libc::c_int = 0;
    let mut lenw: libc::c_int = 0;
    let mut last: libc::c_int = 0;
    let mut iwork: *mut libc::c_int = 0 as *mut libc::c_int;
    args = CDR(args);
    is.f = CAR(args);
    args = CDR(args);
    is.env = CAR(args);
    args = CDR(args);
    if length(CAR(args)) > 1 as libc::c_int {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"\'%s\' must be of length one\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            b"bound\x00" as *const u8 as *const libc::c_char,
        );
    }
    bound = asReal(CAR(args));
    args = CDR(args);
    inf = asInteger(CAR(args));
    args = CDR(args);
    epsabs = asReal(CAR(args));
    args = CDR(args);
    epsrel = asReal(CAR(args));
    args = CDR(args);
    limit = asInteger(CAR(args));
    args = CDR(args);
    lenw = 4 as libc::c_int * limit;
    iwork = R_alloc(
        limit as size_t,
        ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_int;
    work = R_alloc(
        lenw as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    Rdqagi(
        Some(
            Rint as unsafe extern "C" fn(
                _: *mut libc::c_double,
                _: libc::c_int,
                _: *mut libc::c_void,
            ) -> (),
        ),
        &mut is as *mut int_struct as *mut libc::c_void,
        &mut bound,
        &mut inf,
        &mut epsabs,
        &mut epsrel,
        &mut result,
        &mut abserr,
        &mut neval,
        &mut ier,
        &mut limit,
        &mut lenw,
        &mut last,
        iwork,
        work,
    );
    ans = allocVector(19 as libc::c_int as SEXPTYPE, 4 as libc::c_int as R_xlen_t);
    protect(ans);
    ansnames = allocVector(16 as libc::c_int as SEXPTYPE, 4 as libc::c_int as R_xlen_t);
    protect(ansnames);
    SET_STRING_ELT(
        ansnames,
        0 as libc::c_int as R_xlen_t,
        mkChar(b"value\x00" as *const u8 as *const libc::c_char),
    );
    SET_VECTOR_ELT(
        ans,
        0 as libc::c_int as R_xlen_t,
        allocVector(14 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    *REAL(VECTOR_ELT(ans, 0 as libc::c_int as R_xlen_t)).offset(0 as libc::c_int as isize) = result;
    SET_STRING_ELT(
        ansnames,
        1 as libc::c_int as R_xlen_t,
        mkChar(b"abs.error\x00" as *const u8 as *const libc::c_char),
    );
    SET_VECTOR_ELT(
        ans,
        1 as libc::c_int as R_xlen_t,
        allocVector(14 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    *REAL(VECTOR_ELT(ans, 1 as libc::c_int as R_xlen_t)).offset(0 as libc::c_int as isize) = abserr;
    SET_STRING_ELT(
        ansnames,
        2 as libc::c_int as R_xlen_t,
        mkChar(b"subdivisions\x00" as *const u8 as *const libc::c_char),
    );
    SET_VECTOR_ELT(
        ans,
        2 as libc::c_int as R_xlen_t,
        allocVector(13 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    *INTEGER(VECTOR_ELT(ans, 2 as libc::c_int as R_xlen_t)).offset(0 as libc::c_int as isize) =
        last;
    SET_STRING_ELT(
        ansnames,
        3 as libc::c_int as R_xlen_t,
        mkChar(b"ierr\x00" as *const u8 as *const libc::c_char),
    );
    SET_VECTOR_ELT(
        ans,
        3 as libc::c_int as R_xlen_t,
        allocVector(13 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    *INTEGER(VECTOR_ELT(ans, 3 as libc::c_int as R_xlen_t)).offset(0 as libc::c_int as isize) = ier;
    setAttrib(ans, R_NamesSymbol, ansnames);
    unprotect(2 as libc::c_int);
    return ans;
}
