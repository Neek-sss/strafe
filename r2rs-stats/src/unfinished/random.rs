use crate::{
    rcont::rcont2,
    sexprec::{SEXP, SEXPREC, SEXPREC_ALIGN, SEXPTYPE, VECSEXP},
};
use ::libc;
extern "C" {
    pub type R_allocator;
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    #[no_mangle]
    fn ALTREP_LENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn ALTVEC_DATAPTR(x: SEXP) -> *mut libc::c_void;
    #[no_mangle]
    fn ALTSTRING_ELT(_: SEXP, _: R_xlen_t) -> SEXP;
    /* IEEE -Inf */
    #[no_mangle]
    static mut R_NaReal: libc::c_double;
    /* NA_REAL: IEEE */
    #[no_mangle]
    static mut R_NaInt: libc::c_int;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2016  The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     *
     *
     * Memory Allocation (garbage collected) --- INCLUDING S compatibility ---
     */
    /* Included by R.h: API */
    /* for size_t */
    #[no_mangle]
    fn vmaxget() -> *mut libc::c_void;
    #[no_mangle]
    fn vmaxset(_: *const libc::c_void);
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn R_BadLongVector(_: SEXP, _: *const libc::c_char, _: libc::c_int) -> !;
    #[no_mangle]
    static mut R_NilValue: SEXP;
    #[no_mangle]
    static mut R_ClassSymbol: SEXP;
    #[no_mangle]
    static mut R_DimNamesSymbol: SEXP;
    #[no_mangle]
    static mut R_NamesSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn allocMatrix(_: SEXPTYPE, _: libc::c_int, _: libc::c_int) -> SEXP;
    #[no_mangle]
    fn allocVector3(_: SEXPTYPE, _: R_xlen_t, _: *mut R_allocator_t) -> SEXP;
    #[no_mangle]
    fn duplicate(_: SEXP) -> SEXP;
    #[no_mangle]
    fn getAttrib(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn setAttrib(_: SEXP, _: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn R_signal_protect_error() -> !;
    #[no_mangle]
    fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    #[no_mangle]
    static mut R_PPStackSize: libc::c_int;
    #[no_mangle]
    static mut R_PPStackTop: libc::c_int;
    #[no_mangle]
    static mut R_PPStack: *mut SEXP;
    /* INLINE_PROTECT */
    /* from dstruct.c */
    /*  length - length of objects  */
    #[no_mangle]
    fn envlength(rho: SEXP) -> libc::c_int;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    #[no_mangle]
    fn GetRNGstate();
    #[no_mangle]
    fn PutRNGstate();
    #[no_mangle]
    fn rnorm(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn runif(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rgamma(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rbeta(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rlnorm(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rchisq(_: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rnchisq(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rf(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rt(_: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rbinom(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    /* Multnomial Distribution */
    #[no_mangle]
    fn rmultinom(_: libc::c_int, _: *mut libc::c_double, _: libc::c_int, _: *mut libc::c_int);
    #[no_mangle]
    fn rcauchy(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rexp(_: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rgeom(_: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rhyper(_: libc::c_double, _: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rnbinom(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rnbinom_mu(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rpois(_: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rweibull(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rlogis(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rwilcox(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn rsignrank(_: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn lgamma(_: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn __errno_location() -> *mut libc::c_int;

}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
/*, MAYBE */
/* type for length of (standard, not long) vectors etc */
pub type R_len_t = libc::c_int;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
pub type R_allocator_t = R_allocator;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1997--2012  The R Core Team
 *  Copyright (C) 2003--2008  The R Foundation
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* interval at which to check interrupts */
pub type ran1 = Option<unsafe extern "C" fn(_: libc::c_double) -> libc::c_double>;
pub type ran2 =
    Option<unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double>;
pub type ran3 = Option<
    unsafe extern "C" fn(_: libc::c_double, _: libc::c_double, _: libc::c_double) -> libc::c_double,
>;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1999-2017  The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Internal header, not installed */
/* this header is always to be included from others.
  It is only called if COMPILING_R is defined (in util.c) or
  from GNU C systems.

  There are different conventions for inlining across compilation units.
  See http://www.greenend.org.uk/rjk/2003/03/inline.html
*/
/* Probably not able to use C99 semantics in gcc < 4.3.0 */
/* Apple's gcc build >5400 (since Xcode 3.0) doesn't support GNU inline in C99 mode */
/* This section is normally only used for versions of gcc which do not
   support C99 semantics.  __GNUC_STDC_INLINE__ is defined if
   GCC is following C99 inline semantics by default: we
   switch R's usage to the older GNU semantics via attributes.
   Do this even for __GNUC_GNUC_INLINE__ to shut up warnings in 4.2.x.
   __GNUC_STDC_INLINE__ and __GNUC_GNU_INLINE__ were added in gcc 4.2.0.
*/
/* ifdef COMPILING_R */
/* C99_INLINE_SEMANTICS */
/* for strlen, strcmp */
/* define inline-able functions */
#[inline]
unsafe extern "C" fn DATAPTR(mut x: SEXP) -> *mut libc::c_void {
    if (*x).sxpinfo.alt() != 0 {
        return ALTVEC_DATAPTR(x);
    } else {
        return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void;
    };
}
#[inline]
unsafe extern "C" fn XLENGTH_EX(mut x: SEXP) -> R_xlen_t {
    return if (*x).sxpinfo.alt() as libc::c_int != 0 {
        ALTREP_LENGTH(x)
    } else {
        (*(x as VECSEXP)).vecsxp.length
    };
}
#[inline]
unsafe extern "C" fn LENGTH_EX(
    mut x: SEXP,
    mut file: *const libc::c_char,
    mut line: libc::c_int,
) -> libc::c_int {
    if x == R_NilValue {
        return 0 as libc::c_int;
    }
    let mut len: R_xlen_t = XLENGTH_EX(x);
    if len > 2147483647 as libc::c_int as libc::c_long {
        R_BadLongVector(x, file, line);
    }
    return len as libc::c_int;
}
/* if not inlining use version in memory.c with more error checking */
#[inline]
unsafe extern "C" fn STRING_ELT(mut x: SEXP, mut i: R_xlen_t) -> SEXP {
    if (*x).sxpinfo.alt() != 0 {
        return ALTSTRING_ELT(x, i);
    } else {
        let mut ps: *mut SEXP = (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize)
            as *mut libc::c_void as *mut SEXP;
        return *ps.offset(i as isize);
    };
}
#[inline]
unsafe extern "C" fn protect(mut s: SEXP) -> SEXP {
    if R_PPStackTop < R_PPStackSize {
        let fresh0 = R_PPStackTop;
        R_PPStackTop = R_PPStackTop + 1;
        let ref mut fresh1 = *R_PPStack.offset(fresh0 as isize);
        *fresh1 = s
    } else {
        R_signal_protect_error();
    }
    return s;
}
#[inline]
unsafe extern "C" fn unprotect(mut l: libc::c_int) {
    R_PPStackTop -= l;
}
/* TODO: a  Length(.) {say} which is  length() + dispatch (S3 + S4) if needed
         for one approach, see do_seq_along() in ../main/seq.c
*/
#[inline]
unsafe extern "C" fn length(mut s: SEXP) -> R_len_t {
    match (*s).sxpinfo.type_0() as libc::c_int {
        0 => return 0 as libc::c_int,
        10 | 13 | 14 | 15 | 16 | 9 | 19 | 20 | 24 => {
            return LENGTH_EX(
                s,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                522 as libc::c_int,
            )
        }
        2 | 6 | 17 => {
            let mut i: libc::c_int = 0 as libc::c_int;
            while !s.is_null() && s != R_NilValue {
                i += 1;
                s = (*s).u.listsxp.cdrval
            }
            return i;
        }
        4 => return envlength(s),
        _ => return 1 as libc::c_int,
    };
}
/* regular allocVector() as a special case of allocVector3() with no custom allocator */
#[inline]
unsafe extern "C" fn allocVector(mut type_0: SEXPTYPE, mut length: R_xlen_t) -> SEXP {
    return allocVector3(type_0, length, 0 as *mut R_allocator_t);
}
/* NOTE: R's inherits() is based on inherits3() in ../main/objects.c
 * Here, use char / CHAR() instead of the slower more general translateChar()
 */
#[inline]
unsafe extern "C" fn inherits(mut s: SEXP, mut name: *const libc::c_char) -> Rboolean {
    let mut klass: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut nclass: libc::c_int = 0;
    if (*s).sxpinfo.obj() != 0 {
        klass = getAttrib(s, R_ClassSymbol);
        nclass = length(klass);
        i = 0 as libc::c_int;
        while i < nclass {
            if strcmp(
                (STRING_ELT(klass, i as R_xlen_t) as *mut SEXPREC_ALIGN)
                    .offset(1 as libc::c_int as isize) as *mut libc::c_void
                    as *const libc::c_char,
                name,
            ) == 0
            {
                return TRUE;
            }
            i += 1
        }
    }
    return FALSE;
}
#[inline]
unsafe extern "C" fn isVector(mut s: SEXP) -> Rboolean
/* === isVectorList() or isVectorAtomic() */ {
    match (*s).sxpinfo.type_0() as libc::c_int {
        10 | 13 | 14 | 15 | 16 | 24 | 19 | 20 => return TRUE,
        _ => return FALSE,
    };
}
#[inline]
unsafe extern "C" fn isInteger(mut s: SEXP) -> Rboolean {
    return ((*s).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
        && inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as u64 == 0)
        as libc::c_int as Rboolean;
}
/* Is an object of numeric type. */
/* FIXME:  the LGLSXP case should be excluded here
 * (really? in many places we affirm they are treated like INTs)*/
#[inline]
unsafe extern "C" fn isNumeric(mut s: SEXP) -> Rboolean {
    match (*s).sxpinfo.type_0() as libc::c_int {
        13 => {
            if inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as u64 != 0 {
                return FALSE;
            }
        }
        10 | 14 => {}
        _ => return FALSE,
    } /* REALSXP */
    return TRUE;
}
unsafe extern "C" fn fillWithNAs(mut x: SEXP, mut n: R_xlen_t, mut type_0: SEXPTYPE) {
    let mut i: R_xlen_t = 0;
    if type_0 == 13 as libc::c_int as libc::c_uint {
        i = 0 as libc::c_int as R_xlen_t;
        while i < n {
            *(DATAPTR(x) as *mut libc::c_int).offset(i as isize) = R_NaInt;
            i += 1
        }
    } else {
        i = 0 as libc::c_int as R_xlen_t;
        while i < n {
            *(DATAPTR(x) as *mut libc::c_double).offset(i as isize) = R_NaReal;
            i += 1
        }
    }
    warn!("NAs produced");
}
#[inline]
unsafe extern "C" fn resultLength(mut lengthArgument: SEXP) -> R_xlen_t {
    let mut n: R_xlen_t = 0;
    if isVector(lengthArgument) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid arguments\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if XLENGTH_EX(lengthArgument) == 1 as libc::c_int as libc::c_long {
        let mut dn: libc::c_double = asReal(lengthArgument);
        if dn.is_nan() as i32 != 0 as libc::c_int
            || dn < 0 as libc::c_int as libc::c_double
            || dn > 4503599627370496.0
        {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"invalid arguments\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        n = dn as R_xlen_t
    } else {
        n = XLENGTH_EX(lengthArgument)
    }
    return n;
}
/* random sampling from 1 parameter families. */
#[inline]
unsafe extern "C" fn random1(
    mut sn: SEXP,
    mut sa: SEXP,
    mut fn_0: ran1,
    mut type_0: SEXPTYPE,
) -> SEXP {
    let mut x: SEXP = 0 as *mut SEXPREC;
    let mut a: SEXP = 0 as *mut SEXPREC;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    if isNumeric(sa) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid arguments\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    n = resultLength(sn);
    x = allocVector(type_0, n);
    protect(x);
    if n == 0 as libc::c_int as libc::c_long {
        unprotect(1 as libc::c_int);
        return x;
    }
    na = XLENGTH_EX(sa);
    if na < 1 as libc::c_int as libc::c_long {
        fillWithNAs(x, n, type_0);
    } else {
        let mut naflag: Rboolean = FALSE;
        a = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
        protect(a);
        GetRNGstate();
        let mut ra: *mut libc::c_double = DATAPTR(a) as *mut libc::c_double;
        *__errno_location() = 0 as libc::c_int;
        if type_0 == 13 as libc::c_int as libc::c_uint {
            let mut rx: libc::c_double = 0.;
            let mut ix: *mut libc::c_int = DATAPTR(x) as *mut libc::c_int;
            let mut i: R_xlen_t = 0 as libc::c_int as R_xlen_t;
            while i < n {
                //  if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
                rx = fn_0.expect("non-null function pointer")(*ra.offset((i % na) as isize));
                if rx.is_nan() as i32 != 0 as libc::c_int
                    || rx > 2147483647 as libc::c_int as libc::c_double
                    || rx <= (-(2147483647 as libc::c_int) - 1 as libc::c_int) as libc::c_double
                {
                    naflag = TRUE;
                    *ix.offset(i as isize) = R_NaInt
                } else {
                    *ix.offset(i as isize) = rx as libc::c_int
                }
                i += 1
            }
        } else {
            let mut rx_0: *mut libc::c_double = DATAPTR(x) as *mut libc::c_double;
            let mut i_0: R_xlen_t = 0 as libc::c_int as R_xlen_t;
            while i_0 < n {
                //  if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
                *rx_0.offset(i_0 as isize) =
                    fn_0.expect("non-null function pointer")(*ra.offset((i_0 % na) as isize));
                if (*rx_0.offset(i_0 as isize)).is_nan() as i32 != 0 as libc::c_int {
                    naflag = TRUE
                }
                i_0 += 1
            }
        }
        if naflag as u64 != 0 {
            warning(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"NAs produced\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        PutRNGstate();
        unprotect(1 as libc::c_int);
    }
    unprotect(1 as libc::c_int);
    return x;
}
#[no_mangle]
pub unsafe extern "C" fn do_rchisq(mut sn: SEXP, mut sa: SEXP) -> SEXP {
    return random1(
        sn,
        sa,
        Some(rchisq as unsafe extern "C" fn(_: libc::c_double) -> libc::c_double),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rexp(mut sn: SEXP, mut sa: SEXP) -> SEXP {
    return random1(
        sn,
        sa,
        Some(rexp as unsafe extern "C" fn(_: libc::c_double) -> libc::c_double),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rgeom(mut sn: SEXP, mut sa: SEXP) -> SEXP {
    return random1(
        sn,
        sa,
        Some(rgeom as unsafe extern "C" fn(_: libc::c_double) -> libc::c_double),
        13 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rpois(mut sn: SEXP, mut sa: SEXP) -> SEXP {
    return random1(
        sn,
        sa,
        Some(rpois as unsafe extern "C" fn(_: libc::c_double) -> libc::c_double),
        13 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rt(mut sn: SEXP, mut sa: SEXP) -> SEXP {
    return random1(
        sn,
        sa,
        Some(rt as unsafe extern "C" fn(_: libc::c_double) -> libc::c_double),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rsignrank(mut sn: SEXP, mut sa: SEXP) -> SEXP {
    return random1(
        sn,
        sa,
        Some(rsignrank as unsafe extern "C" fn(_: libc::c_double) -> libc::c_double),
        13 as libc::c_int as SEXPTYPE,
    );
}
/* random sampling from 2 parameter families. */
#[inline]
unsafe extern "C" fn random2(
    mut sn: SEXP,
    mut sa: SEXP,
    mut sb: SEXP,
    mut fn_0: ran2,
    mut type_0: SEXPTYPE,
) -> SEXP {
    let mut x: SEXP = 0 as *mut SEXPREC;
    let mut a: SEXP = 0 as *mut SEXPREC;
    let mut b: SEXP = 0 as *mut SEXPREC;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    let mut nb: R_xlen_t = 0;
    if isNumeric(sa) as u64 == 0 || isNumeric(sb) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid arguments\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    n = resultLength(sn);
    x = allocVector(type_0, n);
    protect(x);
    if n == 0 as libc::c_int as libc::c_long {
        unprotect(1 as libc::c_int);
        return x;
    }
    na = XLENGTH_EX(sa);
    nb = XLENGTH_EX(sb);
    if na < 1 as libc::c_int as libc::c_long || nb < 1 as libc::c_int as libc::c_long {
        fillWithNAs(x, n, type_0);
    } else {
        let mut naflag: Rboolean = FALSE;
        a = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
        protect(a);
        b = coerceVector(sb, 14 as libc::c_int as SEXPTYPE);
        protect(b);
        GetRNGstate();
        let mut ra: *mut libc::c_double = DATAPTR(a) as *mut libc::c_double;
        let mut rb: *mut libc::c_double = DATAPTR(b) as *mut libc::c_double;
        if type_0 == 13 as libc::c_int as libc::c_uint {
            let mut ix: *mut libc::c_int = DATAPTR(x) as *mut libc::c_int;
            let mut rx: libc::c_double = 0.;
            *__errno_location() = 0 as libc::c_int;
            let mut i: R_xlen_t = 0 as libc::c_int as R_xlen_t;
            while i < n {
                //  if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
                rx = fn_0.expect("non-null function pointer")(
                    *ra.offset((i % na) as isize),
                    *rb.offset((i % nb) as isize),
                );
                if rx.is_nan() as i32 != 0 as libc::c_int
                    || rx > 2147483647 as libc::c_int as libc::c_double
                    || rx <= (-(2147483647 as libc::c_int) - 1 as libc::c_int) as libc::c_double
                {
                    naflag = TRUE;
                    *ix.offset(i as isize) = R_NaInt
                } else {
                    *ix.offset(i as isize) = rx as libc::c_int
                }
                i += 1
            }
        } else {
            let mut rx_0: *mut libc::c_double = DATAPTR(x) as *mut libc::c_double;
            *__errno_location() = 0 as libc::c_int;
            let mut i_0: R_xlen_t = 0 as libc::c_int as R_xlen_t;
            while i_0 < n {
                //  if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
                *rx_0.offset(i_0 as isize) = fn_0.expect("non-null function pointer")(
                    *ra.offset((i_0 % na) as isize),
                    *rb.offset((i_0 % nb) as isize),
                );
                if (*rx_0.offset(i_0 as isize)).is_nan() as i32 != 0 as libc::c_int {
                    naflag = TRUE
                }
                i_0 += 1
            }
        }
        if naflag as u64 != 0 {
            warning(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"NAs produced\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        PutRNGstate();
        unprotect(2 as libc::c_int);
    }
    unprotect(1 as libc::c_int);
    return x;
}
#[no_mangle]
pub unsafe extern "C" fn do_rbeta(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(rbeta as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rbinom(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rbinom as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        13 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rcauchy(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rcauchy as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rf(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(rf as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rgamma(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rgamma as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rlnorm(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rlnorm as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rlogis(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rlogis as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rnbinom(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rnbinom as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        13 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rnorm(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(rnorm as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_runif(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(runif as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rweibull(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rweibull
                as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rwilcox(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rwilcox as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        13 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rnchisq(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rnchisq as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        14 as libc::c_int as SEXPTYPE,
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_rnbinom_mu(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP) -> SEXP {
    return random2(
        sn,
        sa,
        sb,
        Some(
            rnbinom_mu
                as unsafe extern "C" fn(_: libc::c_double, _: libc::c_double) -> libc::c_double,
        ),
        14 as libc::c_int as SEXPTYPE,
    );
}
/* random sampling from 3 parameter families. */
#[inline]
unsafe extern "C" fn random3(
    mut sn: SEXP,
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut fn_0: ran3,
    mut type_0: SEXPTYPE,
) -> SEXP {
    let mut x: SEXP = 0 as *mut SEXPREC;
    let mut a: SEXP = 0 as *mut SEXPREC;
    let mut b: SEXP = 0 as *mut SEXPREC;
    let mut c: SEXP = 0 as *mut SEXPREC;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    let mut nb: R_xlen_t = 0;
    let mut nc: R_xlen_t = 0;
    if isNumeric(sa) as u64 == 0 || isNumeric(sb) as u64 == 0 || isNumeric(sc) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid arguments\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    n = resultLength(sn);
    x = allocVector(type_0, n);
    protect(x);
    if n == 0 as libc::c_int as libc::c_long {
        unprotect(1 as libc::c_int);
        return x;
    }
    na = XLENGTH_EX(sa);
    nb = XLENGTH_EX(sb);
    nc = XLENGTH_EX(sc);
    if na < 1 as libc::c_int as libc::c_long
        || nb < 1 as libc::c_int as libc::c_long
        || nc < 1 as libc::c_int as libc::c_long
    {
        fillWithNAs(x, n, type_0);
    } else {
        let mut naflag: Rboolean = FALSE;
        a = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
        protect(a);
        b = coerceVector(sb, 14 as libc::c_int as SEXPTYPE);
        protect(b);
        c = coerceVector(sc, 14 as libc::c_int as SEXPTYPE);
        protect(c);
        GetRNGstate();
        let mut ra: *mut libc::c_double = DATAPTR(a) as *mut libc::c_double;
        let mut rb: *mut libc::c_double = DATAPTR(b) as *mut libc::c_double;
        let mut rc: *mut libc::c_double = DATAPTR(c) as *mut libc::c_double;
        *__errno_location() = 0 as libc::c_int;
        if type_0 == 13 as libc::c_int as libc::c_uint {
            let mut ix: *mut libc::c_int = DATAPTR(x) as *mut libc::c_int;
            let mut rx: libc::c_double = 0.;
            *__errno_location() = 0 as libc::c_int;
            let mut i: R_xlen_t = 0 as libc::c_int as R_xlen_t;
            while i < n {
                //         if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
                rx = fn_0.expect("non-null function pointer")(
                    *ra.offset((i % na) as isize),
                    *rb.offset((i % nb) as isize),
                    *rc.offset((i % nc) as isize),
                );
                if rx.is_nan() as i32 != 0 as libc::c_int
                    || rx > 2147483647 as libc::c_int as libc::c_double
                    || rx <= (-(2147483647 as libc::c_int) - 1 as libc::c_int) as libc::c_double
                {
                    naflag = TRUE;
                    *ix.offset(i as isize) = R_NaInt
                } else {
                    *ix.offset(i as isize) = rx as libc::c_int
                }
                i += 1
            }
        } else {
            let mut rx_0: *mut libc::c_double = DATAPTR(x) as *mut libc::c_double;
            *__errno_location() = 0 as libc::c_int;
            let mut i_0: R_xlen_t = 0 as libc::c_int as R_xlen_t;
            while i_0 < n {
                //         if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
                *rx_0.offset(i_0 as isize) = fn_0.expect("non-null function pointer")(
                    *ra.offset((i_0 % na) as isize),
                    *rb.offset((i_0 % nb) as isize),
                    *rc.offset((i_0 % nc) as isize),
                ); /* n= #{samples} */
                if (*rx_0.offset(i_0 as isize)).is_nan() as i32 != 0 as libc::c_int {
                    naflag = TRUE
                } /* X ~ Multi(size, prob) */
                i_0 += 1
            }
        } /* k = #{components or classes} = X-vector length */
        if naflag as u64 != 0 {
            warning(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"NAs produced\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )); /*as `do_sample' -- need this line? */
        }
        PutRNGstate();
        unprotect(3 as libc::c_int);
    }
    unprotect(1 as libc::c_int);
    return x;
}
#[no_mangle]
pub unsafe extern "C" fn do_rhyper(mut sn: SEXP, mut sa: SEXP, mut sb: SEXP, mut sc: SEXP) -> SEXP {
    return random3(
        sn,
        sa,
        sb,
        sc,
        Some(
            rhyper
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                ) -> libc::c_double,
        ),
        13 as libc::c_int as SEXPTYPE,
    );
}
unsafe extern "C" fn FixupProb(mut p: *mut libc::c_double, mut n: libc::c_int) {
    let mut sum: libc::c_double = 0.0f64;
    let mut npos: libc::c_int = 0 as libc::c_int;
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < n {
        if (*p.offset(i as isize)).is_finite() as i32 == 0 {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"NA in probability vector\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        if *p.offset(i as isize) < 0.0f64 {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"negative probability\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        if *p.offset(i as isize) > 0.0f64 {
            npos += 1;
            sum += *p.offset(i as isize)
        }
        i += 1
    }
    if npos == 0 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"no positive probabilities\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    let mut i_0: libc::c_int = 0 as libc::c_int;
    while i_0 < n {
        *p.offset(i_0 as isize) /= sum;
        i_0 += 1
    }
}
#[no_mangle]
pub unsafe extern "C" fn do_rmultinom(mut sn: SEXP, mut ssize: SEXP, mut prob: SEXP) -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut nms: SEXP = 0 as *mut SEXPREC;
    let mut n: libc::c_int = 0;
    let mut size: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut ik: libc::c_int = 0;
    n = asInteger(sn);
    size = asInteger(ssize);
    if n == R_NaInt || n < 0 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid first argument \'n\'\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if size == R_NaInt || size < 0 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid second argument \'size\'\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    prob = coerceVector(prob, 14 as libc::c_int as SEXPTYPE);
    k = length(prob);
    if !((*prob).sxpinfo.named() as libc::c_int == 0 as libc::c_int) {
        prob = duplicate(prob)
    }
    protect(prob);
    /* check and make sum = 1: */
    FixupProb(DATAPTR(prob) as *mut libc::c_double, k); /* k x n : natural for columnwise store */
    GetRNGstate();
    ans = allocMatrix(13 as libc::c_int as SEXPTYPE, k, n);
    protect(ans);
    ik = 0 as libc::c_int;
    i = ik;
    while i < n {
        // if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
        rmultinom(
            size,
            DATAPTR(prob) as *mut libc::c_double,
            k,
            &mut *((DATAPTR as unsafe extern "C" fn(_: SEXP) -> *mut libc::c_void)(ans)
                as *mut libc::c_int)
                .offset(ik as isize),
        );
        i += 1;
        ik += k
    }
    PutRNGstate();
    nms = getAttrib(prob, R_NamesSymbol);
    if !((*nms).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int) {
        let mut dimnms: SEXP = 0 as *mut SEXPREC;
        protect(nms);
        dimnms = allocVector(19 as libc::c_int as SEXPTYPE, 2 as libc::c_int as R_xlen_t);
        protect(dimnms);
        SET_VECTOR_ELT(dimnms, 0 as libc::c_int as R_xlen_t, nms);
        setAttrib(ans, R_DimNamesSymbol, dimnms);
        unprotect(2 as libc::c_int);
    }
    unprotect(2 as libc::c_int);
    return ans;
}
#[no_mangle]
pub unsafe extern "C" fn r2dtable(mut n: SEXP, mut r: SEXP, mut c: SEXP) -> SEXP {
    let mut nr: libc::c_int = 0;
    let mut nc: libc::c_int = 0;
    let mut row_sums: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut col_sums: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut i: libc::c_int = 0;
    let mut jwork: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut n_of_samples: libc::c_int = 0;
    let mut n_of_cases: libc::c_int = 0;
    let mut fact: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut tmp: SEXP = 0 as *mut SEXPREC;
    let mut vmax: *const libc::c_void = vmaxget();
    nr = length(r);
    nc = length(c);
    /* Note that the R code in r2dtable() also checks for missing and
       negative values.
       Should maybe do the same here ...
    */
    if isInteger(n) as u64 == 0
        || length(n) == 0 as libc::c_int
        || isInteger(r) as u64 == 0
        || nr <= 1 as libc::c_int
        || isInteger(c) as u64 == 0
        || nc <= 1 as libc::c_int
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid arguments\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    n_of_samples = *(DATAPTR(n) as *mut libc::c_int).offset(0 as libc::c_int as isize);
    row_sums = DATAPTR(r) as *mut libc::c_int;
    col_sums = DATAPTR(c) as *mut libc::c_int;
    /* Compute total number of cases as the sum of the row sums.
       Note that the R code in r2dtable() also checks whether this is
       the same as the sum of the col sums.
       Should maybe do the same here ...
    */
    n_of_cases = 0 as libc::c_int;
    jwork = row_sums;
    i = 0 as libc::c_int;
    while i < nr {
        let fresh2 = jwork;
        jwork = jwork.offset(1);
        n_of_cases += *fresh2;
        i += 1
    }
    /* Log-factorials from 0 to n_of_cases.
       (I.e., lgamma(1), ..., lgamma(n_of_cases + 1).)
    */
    fact = R_alloc(
        (n_of_cases + 1 as libc::c_int) as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    *fact.offset(0 as libc::c_int as isize) = 0.0f64;
    i = 1 as libc::c_int;
    while i <= n_of_cases {
        *fact.offset(i as isize) = lgamma((i + 1 as libc::c_int) as libc::c_double);
        i += 1
    }
    jwork = R_alloc(
        nc as size_t,
        ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_int;
    ans = allocVector(19 as libc::c_int as SEXPTYPE, n_of_samples as R_xlen_t);
    protect(ans);
    GetRNGstate();
    i = 0 as libc::c_int;
    while i < n_of_samples {
        tmp = allocMatrix(13 as libc::c_int as SEXPTYPE, nr, nc);
        protect(tmp);
        rcont2(
            &mut nr,
            &mut nc,
            row_sums,
            col_sums,
            &mut n_of_cases,
            fact,
            jwork,
            DATAPTR(tmp) as *mut libc::c_int,
        );
        SET_VECTOR_ELT(ans, i as R_xlen_t, tmp);
        unprotect(1 as libc::c_int);
        i += 1
    }
    PutRNGstate();
    unprotect(1 as libc::c_int);
    vmaxset(vmax);
    return ans;
}
