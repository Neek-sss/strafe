use crate::sexprec::{SEXP, SEXPREC, SEXPTYPE};
use ::libc;
extern "C" {
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn TYPEOF(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn INTEGER(x: SEXP) -> *mut libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn SET_STRING_ELT(x: SEXP, i: R_xlen_t, v: SEXP);
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    /* "name" */
    #[no_mangle]
    static mut R_NamesSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn duplicate(_: SEXP) -> SEXP;
    #[no_mangle]
    fn mkChar(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn ncols(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn setAttrib(_: SEXP, _: SEXP, _: SEXP) -> SEXP;
    /* Defining NO_RINLINEDFUNS disables use to simulate platforms where
    this is not available */
    /* need remapped names here for use with R_NO_REMAP */
    /*
       These are the inlinable functions that are provided in Rinlinedfuns.h
       It is *essential* that these do not appear in any other header file,
       with or without the Rf_ prefix.
    */
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn length(_: SEXP) -> R_len_t;
    #[no_mangle]
    fn ScalarInteger(_: libc::c_int) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
}
pub type ptrdiff_t = libc::c_long;
/* type for length of (standard, not long) vectors etc */
pub type R_len_t = libc::c_int;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
/* string vectors */
/* dot-dot-dot object */
/* make "any" args work.
Used in specifying types for symbol
registration to mean anything is okay  */
/* generic vectors */
/* expressions vectors */
/* byte code */
/* external pointer */
/* weak reference */
/* raw bytes */
/* S4, non-vector */
/* used for detecting PROTECT issues in memory.c */
/* fresh node created in new page */
/* node released by GC */
/* Closure or Builtin or Special */
/* NOT YET */
/* These are also used with the write barrier on, in attrib.c and util.c */
/* Table of constant values */
unsafe extern "C" fn loglin(
    mut nvar: libc::c_int,
    mut dim: *mut libc::c_int,
    mut ncon: libc::c_int,
    mut config: *mut libc::c_int,
    mut ntab: libc::c_int,
    mut table: *mut libc::c_double,
    mut fit: *mut libc::c_double,
    mut locmar: *mut libc::c_int,
    mut nmar: libc::c_int,
    mut marg: *mut libc::c_double,
    mut nu: libc::c_int,
    mut u: *mut libc::c_double,
    mut maxdev: libc::c_double,
    mut maxit: libc::c_int,
    mut dev: *mut libc::c_double,
    mut nlast: *mut libc::c_int,
    mut ifault: *mut libc::c_int,
) {
    let mut current_block: u64;
    // nvar could be zero (no-segfault test)
    if nvar == 0 {
        error(b"no variables\x00" as *const u8 as *const libc::c_char); // not translated
    }
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut point: libc::c_int = 0;
    let mut size: libc::c_int = 0;
    let vla = nvar as usize;
    let mut check: Vec<libc::c_int> = ::std::vec::from_elem(0, vla);
    let vla_0 = nvar as usize;
    let mut icon: Vec<libc::c_int> = ::std::vec::from_elem(0, vla_0);
    let mut x: libc::c_double = 0.;
    let mut y: libc::c_double = 0.;
    let mut xmax: libc::c_double = 0.;
    /* Parameter adjustments */
    dim = dim.offset(-1);
    locmar = locmar.offset(-1);
    config = config.offset(-((nvar + 1 as libc::c_int) as isize));
    fit = fit.offset(-1);
    table = table.offset(-1);
    marg = marg.offset(-1);
    u = u.offset(-1);
    dev = dev.offset(-1);
    /* Function body */
    *ifault = 0 as libc::c_int;
    *nlast = 0 as libc::c_int;
    /* Check validity of NVAR, the number of variables, and of maxit,
    the maximum number of iterations */
    if nvar > 0 as libc::c_int && maxit > 0 as libc::c_int {
        /* Look at table and fit constants */
        size = 1 as libc::c_int;
        j = 1 as libc::c_int;
        loop {
            if !(j <= nvar) {
                current_block = 12124785117276362961;
                break;
            }
            if *dim.offset(j as isize) <= 0 as libc::c_int {
                current_block = 2629672494974161066;
                break;
            }
            size *= *dim.offset(j as isize);
            j += 1
        }
        match current_block {
            2629672494974161066 => {}
            _ => {
                if size <= ntab {
                    x = 0.0f64;
                    y = 0.0f64;
                    i = 1 as libc::c_int;
                    loop {
                        if !(i <= size) {
                            current_block = 11932355480408055363;
                            break;
                        }
                        if *table.offset(i as isize) < 0.0f64 || *fit.offset(i as isize) < 0.0f64 {
                            current_block = 2629672494974161066;
                            break;
                        }
                        x += *table.offset(i as isize);
                        y += *fit.offset(i as isize);
                        i += 1
                    }
                    match current_block {
                        2629672494974161066 => {}
                        _ =>
                        /* Make a preliminary adjustment to obtain the fit to an empty
                        configuration list */
                        {
                            if y == 0.0f64 {
                                current_block = 2629672494974161066;
                            } else {
                                x /= y;
                                i = 1 as libc::c_int;
                                while i <= size {
                                    *fit.offset(i as isize) = x * *fit.offset(i as isize);
                                    i += 1
                                }
                                if ncon <= 0 as libc::c_int
                                    || *config.offset((nvar + 1 as libc::c_int) as isize)
                                        == 0 as libc::c_int
                                {
                                    return;
                                }
                                /* Allocate marginal tables */
                                point = 1 as libc::c_int;
                                i = 1 as libc::c_int;
                                loop {
                                    if !(i <= ncon) {
                                        current_block = 4741994311446740739;
                                        break;
                                    }
                                    /* A zero beginning a configuration indicates that the list is
                                    completed */
                                    if *config.offset((i * nvar + 1 as libc::c_int) as isize)
                                        == 0 as libc::c_int
                                    {
                                        current_block = 14955198446389028284;
                                        break;
                                    }
                                    /* Get marginal table size.  While doing this task, see if the
                                    configuration list contains duplications or elements out of
                                    range. */
                                    size = 1 as libc::c_int;
                                    j = 0 as libc::c_int;
                                    while j < nvar {
                                        *check.as_mut_ptr().offset(j as isize) = 0 as libc::c_int;
                                        j += 1
                                    }
                                    j = 1 as libc::c_int;
                                    while j <= nvar {
                                        k = *config.offset((j + i * nvar) as isize);
                                        /* A zero indicates the end of the string. */
                                        if k == 0 as libc::c_int {
                                            break;
                                        }
                                        /* See if element is valid. */
                                        if k >= 0 as libc::c_int && k <= nvar {
                                            /* Check for duplication */
                                            if !(*check
                                                .as_mut_ptr()
                                                .offset((k - 1 as libc::c_int) as isize)
                                                != 0)
                                            {
                                                *check
                                                    .as_mut_ptr()
                                                    .offset((k - 1 as libc::c_int) as isize) =
                                                    1 as libc::c_int;
                                                /* Get size */
                                                size *= *dim.offset(k as isize);
                                                j += 1;
                                                continue;
                                            }
                                        }
                                        *ifault = 1 as libc::c_int;
                                        return;
                                    }
                                    /* Since U is used to store fitted marginals, size must not
                                    exceed NU */
                                    if size > nu {
                                        current_block = 9435919032613155258;
                                        break;
                                    }
                                    /* LOCMAR points to marginal tables to be placed in MARG */
                                    *locmar.offset(i as isize) = point;
                                    point += size;
                                    i += 1
                                }
                                match current_block {
                                    9435919032613155258 => {}
                                    _ => {
                                        match current_block {
                                            4741994311446740739 => {
                                                /* Get N, number of valid configurations */
                                                i = ncon + 1 as libc::c_int
                                            }
                                            _ => {}
                                        }
                                        n = i - 1 as libc::c_int;
                                        /* See if MARG can hold all marginal tables */
                                        if point > nmar + 1 as libc::c_int {
                                            current_block = 9435919032613155258;
                                        } else {
                                            /* Obtain marginal tables */
                                            i = 1 as libc::c_int;
                                            while i <= n {
                                                j = 1 as libc::c_int;
                                                while j <= nvar {
                                                    *icon
                                                        .as_mut_ptr()
                                                        .offset((j - 1 as libc::c_int) as isize) =
                                                        *config.offset((j + i * nvar) as isize);
                                                    j += 1
                                                }
                                                collap(
                                                    nvar,
                                                    &mut *table.offset(1 as libc::c_int as isize),
                                                    &mut *marg.offset(1 as libc::c_int as isize),
                                                    *locmar.offset(i as isize),
                                                    &mut *dim.offset(1 as libc::c_int as isize),
                                                    icon.as_mut_ptr(),
                                                );
                                                i += 1
                                            }
                                            /* Perform iterations */
                                            k = 1 as libc::c_int;
                                            loop {
                                                if !(k <= maxit) {
                                                    current_block = 14294131666767243020;
                                                    break;
                                                }
                                                /* XMAX is maximum deviation observed between fitted and true
                                                marginal during a cycle */
                                                xmax = 0.0f64;
                                                i = 1 as libc::c_int;
                                                while i <= n {
                                                    j = 1 as libc::c_int;
                                                    while j <= nvar {
                                                        *icon.as_mut_ptr().offset(
                                                            (j - 1 as libc::c_int) as isize,
                                                        ) = *config.offset((j + i * nvar) as isize);
                                                        j += 1
                                                    }
                                                    collap(
                                                        nvar,
                                                        &mut *fit.offset(1 as libc::c_int as isize),
                                                        &mut *u.offset(1 as libc::c_int as isize),
                                                        1 as libc::c_int,
                                                        &mut *dim.offset(1 as libc::c_int as isize),
                                                        icon.as_mut_ptr(),
                                                    );
                                                    adjust(
                                                        nvar,
                                                        &mut *fit.offset(1 as libc::c_int as isize),
                                                        &mut *u.offset(1 as libc::c_int as isize),
                                                        &mut *marg
                                                            .offset(1 as libc::c_int as isize),
                                                        &mut *locmar.offset(i as isize),
                                                        &mut *dim.offset(1 as libc::c_int as isize),
                                                        icon.as_mut_ptr(),
                                                        &mut xmax,
                                                    );
                                                    i += 1
                                                }
                                                /* Test convergence */
                                                *dev.offset(k as isize) = xmax;
                                                if xmax < maxdev {
                                                    current_block = 2854605741447102226;
                                                    break;
                                                }
                                                k += 1
                                            }
                                            match current_block {
                                                2854605741447102226 =>
                                                /* Normal termination */
                                                {
                                                    *nlast = k;
                                                    return;
                                                }
                                                _ => {
                                                    if maxit > 1 as libc::c_int {
                                                        /* No convergence */
                                                        *ifault = 3 as libc::c_int;
                                                        *nlast = maxit;
                                                        return;
                                                    } else {
                                                        *nlast = 1 as libc::c_int;
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    current_block = 9435919032613155258;
                }
                match current_block {
                    2629672494974161066 => {}
                    _ => {
                        *ifault = 2 as libc::c_int;
                        return;
                    }
                }
            }
        }
    }
    *ifault = 4 as libc::c_int;
}
/* Algorithm AS 51 Appl. Statist. (1972), vol. 21, p. 218
   original (C) Royal Statistical Society 1972

   Performs an iterative proportional fit of the marginal totals of a
   contingency table.
*/
/* Algorithm AS 51.1 Appl. Statist. (1972), vol. 21, p. 218

   Computes a marginal table from a complete table.
   All parameters are assumed valid without test.

   The larger table is X and the smaller one is Y.
*/
unsafe extern "C" fn collap(
    mut nvar: libc::c_int,
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut locy: libc::c_int,
    mut dim: *mut libc::c_int,
    mut config: *mut libc::c_int,
) {
    let mut current_block: u64;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut l: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut locu: libc::c_int = 0;
    let vla = (nvar + 1 as libc::c_int) as usize;
    let mut size: Vec<libc::c_int> = ::std::vec::from_elem(0, vla);
    let vla_0 = nvar as usize;
    let mut coord: Vec<libc::c_int> = ::std::vec::from_elem(0, vla_0);
    /* Parameter adjustments */
    config = config.offset(-1);
    dim = dim.offset(-1);
    x = x.offset(-1);
    y = y.offset(-1);
    /* Initialize arrays */
    *size.as_mut_ptr().offset(0 as libc::c_int as isize) = 1 as libc::c_int;
    k = 1 as libc::c_int;
    loop {
        if !(k <= nvar) {
            current_block = 11812396948646013369;
            break;
        }
        l = *config.offset(k as isize);
        if l == 0 as libc::c_int {
            current_block = 18046297923229789857;
            break;
        }
        *size.as_mut_ptr().offset(k as isize) =
            *size.as_mut_ptr().offset((k - 1 as libc::c_int) as isize) * *dim.offset(l as isize);
        k += 1
    }
    match current_block {
        11812396948646013369 => {
            /* Find number of variables in configuration */
            k = nvar + 1 as libc::c_int
        }
        _ => {}
    }
    n = k - 1 as libc::c_int;
    /* Initialize Y.  First cell of marginal table is at Y(LOCY) and
    table has SIZE(K) elements */
    locu = locy + *size.as_mut_ptr().offset((k - 1 as libc::c_int) as isize) - 1 as libc::c_int;
    j = locy;
    while j <= locu {
        *y.offset(j as isize) = 0.0f64;
        j += 1
    }
    /* Initialize coordinates */
    k = 0 as libc::c_int;
    while k < nvar {
        *coord.as_mut_ptr().offset(k as isize) = 0 as libc::c_int;
        k += 1
    }
    /* Find locations in tables */
    i = 1 as libc::c_int;
    'c_5701: loop {
        j = locy;
        k = 1 as libc::c_int;
        while k <= n {
            l = *config.offset(k as isize);
            j += *coord.as_mut_ptr().offset((l - 1 as libc::c_int) as isize)
                * *size.as_mut_ptr().offset((k - 1 as libc::c_int) as isize);
            k += 1
        }
        *y.offset(j as isize) += *x.offset(i as isize);
        /* Update coordinates */
        i += 1;
        k = 1 as libc::c_int;
        loop {
            if !(k <= nvar) {
                break 'c_5701;
            }
            let ref mut fresh0 = *coord.as_mut_ptr().offset((k - 1 as libc::c_int) as isize);
            *fresh0 += 1;
            if *coord.as_mut_ptr().offset((k - 1 as libc::c_int) as isize) < *dim.offset(k as isize)
            {
                break;
            }
            *coord.as_mut_ptr().offset((k - 1 as libc::c_int) as isize) = 0 as libc::c_int;
            k += 1
        }
    }
}
/* Algorithm AS 51.2 Appl. Statist. (1972), vol. 21, p. 218

Makes proportional adjustment corresponding to CONFIG.
All parameters are assumed valid without test.
*/
unsafe extern "C" fn adjust(
    mut nvar: libc::c_int,
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut z: *mut libc::c_double,
    mut locz: *mut libc::c_int,
    mut dim: *mut libc::c_int,
    mut config: *mut libc::c_int,
    mut d: *mut libc::c_double,
) {
    let mut current_block: u64;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut l: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let vla = (nvar + 1 as libc::c_int) as usize;
    let mut size: Vec<libc::c_int> = ::std::vec::from_elem(0, vla);
    let vla_0 = nvar as usize;
    let mut coord: Vec<libc::c_int> = ::std::vec::from_elem(0, vla_0);
    let mut e: libc::c_double = 0.;
    /* Parameter adjustments */
    config = config.offset(-1);
    dim = dim.offset(-1);
    x = x.offset(-1);
    y = y.offset(-1);
    z = z.offset(-1);
    /* Set size array */
    *size.as_mut_ptr().offset(0 as libc::c_int as isize) = 1 as libc::c_int;
    k = 1 as libc::c_int;
    loop {
        if !(k <= nvar) {
            current_block = 7746791466490516765;
            break;
        }
        l = *config.offset(k as isize);
        if l == 0 as libc::c_int {
            current_block = 11289147888405429654;
            break;
        }
        *size.as_mut_ptr().offset(k as isize) =
            *size.as_mut_ptr().offset((k - 1 as libc::c_int) as isize) * *dim.offset(l as isize);
        k += 1
    }
    match current_block {
        7746791466490516765 => {
            /* Find number of variables in configuration */
            k = nvar + 1 as libc::c_int
        }
        _ => {}
    }
    n = k - 1 as libc::c_int;
    /* Test size of deviation */
    l = *size.as_mut_ptr().offset((k - 1 as libc::c_int) as isize);
    j = 1 as libc::c_int;
    k = *locz;
    i = 1 as libc::c_int;
    while i <= l {
        e = if *z.offset(k as isize) - *y.offset(j as isize) >= 0 as libc::c_int as libc::c_double {
            (*z.offset(k as isize)) - *y.offset(j as isize)
        } else {
            -(*z.offset(k as isize) - *y.offset(j as isize))
        };
        if e > *d {
            *d = e
        }
        j += 1;
        k += 1;
        i += 1
    }
    /* Initialize coordinates */
    k = 0 as libc::c_int;
    while k < nvar {
        *coord.as_mut_ptr().offset(k as isize) = 0 as libc::c_int;
        k += 1
    }
    i = 1 as libc::c_int;
    'c_5992: loop
    /* Perform adjustment */
    {
        j = 0 as libc::c_int;
        k = 1 as libc::c_int;
        while k <= n {
            l = *config.offset(k as isize);
            j += *coord.as_mut_ptr().offset((l - 1 as libc::c_int) as isize)
                * *size.as_mut_ptr().offset((k - 1 as libc::c_int) as isize);
            k += 1
        }
        k = j + *locz;
        j += 1;
        /* Note that Y(J) should be non-negative */
        if *y.offset(j as isize) <= 0.0f64 {
            *x.offset(i as isize) = 0.0f64
        }
        if *y.offset(j as isize) > 0.0f64 {
            *x.offset(i as isize) =
                *x.offset(i as isize) * *z.offset(k as isize) / *y.offset(j as isize)
        }
        /* Update coordinates */
        i += 1;
        k = 1 as libc::c_int;
        loop {
            if !(k <= nvar) {
                break 'c_5992;
            }
            let ref mut fresh1 = *coord.as_mut_ptr().offset((k - 1 as libc::c_int) as isize);
            *fresh1 += 1;
            if *coord.as_mut_ptr().offset((k - 1 as libc::c_int) as isize) < *dim.offset(k as isize)
            {
                break;
            }
            *coord.as_mut_ptr().offset((k - 1 as libc::c_int) as isize) = 0 as libc::c_int;
            k += 1
        }
    }
}
#[no_mangle]
pub unsafe extern "C" fn LogLin(
    mut dtab: SEXP,
    mut conf: SEXP,
    mut table: SEXP,
    mut start: SEXP,
    mut snmar: SEXP,
    mut eps: SEXP,
    mut iter: SEXP,
) -> SEXP {
    let mut nvar: libc::c_int = length(dtab);
    let mut ncon: libc::c_int = ncols(conf);
    let mut ntab: libc::c_int = length(table);
    let mut nmar: libc::c_int = asInteger(snmar);
    let mut maxit: libc::c_int = asInteger(iter);
    let mut nlast: libc::c_int = 0;
    let mut ifault: libc::c_int = 0;
    let mut maxdev: libc::c_double = asReal(eps);
    let mut fit: SEXP = protect(if TYPEOF(start) == 14 as libc::c_int {
        duplicate(start)
    } else {
        coerceVector(start, 14 as libc::c_int as SEXPTYPE)
    });
    let mut locmar: SEXP = protect(allocVector(13 as libc::c_int as SEXPTYPE, ncon as R_xlen_t));
    let mut marg: SEXP = protect(allocVector(14 as libc::c_int as SEXPTYPE, nmar as R_xlen_t));
    let mut u: SEXP = protect(allocVector(14 as libc::c_int as SEXPTYPE, ntab as R_xlen_t));
    let mut dev: SEXP = protect(allocVector(
        14 as libc::c_int as SEXPTYPE,
        maxit as R_xlen_t,
    ));
    dtab = protect(coerceVector(dtab, 13 as libc::c_int as SEXPTYPE));
    conf = protect(coerceVector(conf, 13 as libc::c_int as SEXPTYPE));
    table = protect(coerceVector(table, 14 as libc::c_int as SEXPTYPE));
    loglin(
        nvar,
        INTEGER(dtab),
        ncon,
        INTEGER(conf),
        ntab,
        REAL(table),
        REAL(fit),
        INTEGER(locmar),
        nmar,
        REAL(marg),
        ntab,
        REAL(u),
        maxdev,
        maxit,
        REAL(dev),
        &mut nlast,
        &mut ifault,
    );
    match ifault {
        1 | 2 => {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"this should not happen\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        3 => {
            warning(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"algorithm did not converge\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        4 => {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"incorrect specification of \'table\' or \'start\'\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        _ => {}
    }
    let mut ans: SEXP = protect(allocVector(
        19 as libc::c_int as SEXPTYPE,
        3 as libc::c_int as R_xlen_t,
    ));
    let mut nm: SEXP = 0 as *mut SEXPREC;
    SET_VECTOR_ELT(ans, 0 as libc::c_int as R_xlen_t, fit);
    SET_VECTOR_ELT(ans, 1 as libc::c_int as R_xlen_t, dev);
    SET_VECTOR_ELT(ans, 2 as libc::c_int as R_xlen_t, ScalarInteger(nlast));
    nm = allocVector(16 as libc::c_int as SEXPTYPE, 3 as libc::c_int as R_xlen_t);
    setAttrib(ans, R_NamesSymbol, nm);
    SET_STRING_ELT(
        nm,
        0 as libc::c_int as R_xlen_t,
        mkChar(b"fit\x00" as *const u8 as *const libc::c_char),
    );
    SET_STRING_ELT(
        nm,
        1 as libc::c_int as R_xlen_t,
        mkChar(b"dev\x00" as *const u8 as *const libc::c_char),
    );
    SET_STRING_ELT(
        nm,
        2 as libc::c_int as R_xlen_t,
        mkChar(b"nlast\x00" as *const u8 as *const libc::c_char),
    );
    unprotect(9 as libc::c_int);
    return ans;
}
