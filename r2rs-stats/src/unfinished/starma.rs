use ::libc;
extern "C" {

    #[no_mangle]
    fn sqrt(_: libc::c_double) -> libc::c_double;
    /* IEEE -Inf */
    #[no_mangle]
    static mut R_NaReal: libc::c_double;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn R_chk_free(_: *mut libc::c_void);
    #[no_mangle]
    fn R_chk_calloc(_: size_t, _: size_t) -> *mut libc::c_void;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
}
pub type size_t = libc::c_ulong;
#[derive(Copy, Clone)]
#[repr(C)]
pub struct starma_struct {
    pub p: libc::c_int,
    pub q: libc::c_int,
    pub r: libc::c_int,
    pub np: libc::c_int,
    pub nrbar: libc::c_int,
    pub n: libc::c_int,
    pub ncond: libc::c_int,
    pub m: libc::c_int,
    pub trans: libc::c_int,
    pub method: libc::c_int,
    pub nused: libc::c_int,
    pub mp: libc::c_int,
    pub mq: libc::c_int,
    pub msp: libc::c_int,
    pub msq: libc::c_int,
    pub ns: libc::c_int,
    pub delta: libc::c_double,
    pub s2: libc::c_double,
    pub params: *mut libc::c_double,
    pub phi: *mut libc::c_double,
    pub theta: *mut libc::c_double,
    pub a: *mut libc::c_double,
    pub P: *mut libc::c_double,
    pub V: *mut libc::c_double,
    pub thetab: *mut libc::c_double,
    pub xnext: *mut libc::c_double,
    pub xrow: *mut libc::c_double,
    pub rbar: *mut libc::c_double,
    pub w: *mut libc::c_double,
    pub wkeep: *mut libc::c_double,
    pub resid: *mut libc::c_double,
    pub reg: *mut libc::c_double,
}
pub type Starma = *mut starma_struct;
/*  R : A Computer Language for Statistical Data Analysis
 *
 *  Copyright (C) 1999-2002 The R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/.
 */
/* Code in this file based on Applied Statistics algorithms AS154/182
(C) Royal Statistical Society 1980, 1982 */
unsafe extern "C" fn inclu2(
    mut np: libc::c_int,
    mut xnext: *mut libc::c_double,
    mut xrow: *mut libc::c_double,
    mut ynext: libc::c_double,
    mut d: *mut libc::c_double,
    mut rbar: *mut libc::c_double,
    mut thetab: *mut libc::c_double,
) {
    let mut cbar: libc::c_double = 0.;
    let mut sbar: libc::c_double = 0.;
    let mut di: libc::c_double = 0.;
    let mut xi: libc::c_double = 0.;
    let mut xk: libc::c_double = 0.;
    let mut rbthis: libc::c_double = 0.;
    let mut dpi: libc::c_double = 0.;
    let mut i: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut ithisr: libc::c_int = 0;
    /*   This subroutine updates d, rbar, thetab by the inclusion
    of xnext and ynext. */
    i = 0 as libc::c_int;
    while i < np {
        *xrow.offset(i as isize) = *xnext.offset(i as isize);
        i += 1
    }
    ithisr = 0 as libc::c_int;
    i = 0 as libc::c_int;
    while i < np {
        if *xrow.offset(i as isize) != 0.0f64 {
            xi = *xrow.offset(i as isize);
            di = *d.offset(i as isize);
            dpi = di + xi * xi;
            *d.offset(i as isize) = dpi;
            cbar = di / dpi;
            sbar = xi / dpi;
            k = i + 1 as libc::c_int;
            while k < np {
                xk = *xrow.offset(k as isize);
                rbthis = *rbar.offset(ithisr as isize);
                *xrow.offset(k as isize) = xk - xi * rbthis;
                let fresh0 = ithisr;
                ithisr = ithisr + 1;
                *rbar.offset(fresh0 as isize) = cbar * rbthis + sbar * xk;
                k += 1
            }
            xk = ynext;
            ynext = xk - xi * *thetab.offset(i as isize);
            *thetab.offset(i as isize) = cbar * *thetab.offset(i as isize) + sbar * xk;
            if di == 0.0f64 {
                return;
            }
        } else {
            ithisr = ithisr + np - i - 1 as libc::c_int
        }
        i += 1
    }
}
#[no_mangle]
pub unsafe extern "C" fn starma(mut G: Starma, mut ifault: *mut libc::c_int) {
    let mut p: libc::c_int = (*G).p;
    let mut q: libc::c_int = (*G).q;
    let mut r: libc::c_int = (*G).r;
    let mut np: libc::c_int = (*G).np;
    let mut nrbar: libc::c_int = (*G).nrbar;
    let mut phi: *mut libc::c_double = (*G).phi;
    let mut theta: *mut libc::c_double = (*G).theta;
    let mut a: *mut libc::c_double = (*G).a;
    let mut P: *mut libc::c_double = (*G).P;
    let mut V: *mut libc::c_double = (*G).V;
    let mut thetab: *mut libc::c_double = (*G).thetab;
    let mut xnext: *mut libc::c_double = (*G).xnext;
    let mut xrow: *mut libc::c_double = (*G).xrow;
    let mut rbar: *mut libc::c_double = (*G).rbar;
    let mut indi: libc::c_int = 0;
    let mut indj: libc::c_int = 0;
    let mut indn: libc::c_int = 0;
    let mut phii: libc::c_double = 0.;
    let mut phij: libc::c_double = 0.;
    let mut ynext: libc::c_double = 0.;
    let mut vj: libc::c_double = 0.;
    let mut bi: libc::c_double = 0.;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut ithisr: libc::c_int = 0;
    let mut ind: libc::c_int = 0;
    let mut npr: libc::c_int = 0;
    let mut ind1: libc::c_int = 0;
    let mut ind2: libc::c_int = 0;
    let mut npr1: libc::c_int = 0;
    let mut im: libc::c_int = 0;
    let mut jm: libc::c_int = 0;
    /*      Invoking this subroutine sets the values of v and phi, and
    obtains the initial values of a and p. */
    /*     Check if ar(1) */
    if !(q > 0 as libc::c_int || p > 1 as libc::c_int) {
        *V.offset(0 as libc::c_int as isize) = 1.0f64;
        *a.offset(0 as libc::c_int as isize) = 0.0f64;
        *P.offset(0 as libc::c_int as isize) = 1.0f64
            / (1.0f64
                - *phi.offset(0 as libc::c_int as isize) * *phi.offset(0 as libc::c_int as isize));
        return;
    }
    /*        Check for failure indication. */
    *ifault = 0 as libc::c_int;
    if p < 0 as libc::c_int {
        *ifault = 1 as libc::c_int
    }
    if q < 0 as libc::c_int {
        *ifault += 2 as libc::c_int
    }
    if p == 0 as libc::c_int && q == 0 as libc::c_int {
        *ifault = 4 as libc::c_int
    }
    k = q + 1 as libc::c_int;
    if k < p {
        k = p
    }
    if r != k {
        *ifault = 5 as libc::c_int
    }
    if np != r * (r + 1 as libc::c_int) / 2 as libc::c_int {
        *ifault = 6 as libc::c_int
    }
    if nrbar != np * (np - 1 as libc::c_int) / 2 as libc::c_int {
        *ifault = 7 as libc::c_int
    }
    if r == 1 as libc::c_int {
        *ifault = 8 as libc::c_int
    }
    if *ifault != 0 as libc::c_int {
        return;
    }
    /*        Now set a(0), V and phi. */
    i = 1 as libc::c_int;
    while i < r {
        *a.offset(i as isize) = 0.0f64;
        if i >= p {
            *phi.offset(i as isize) = 0.0f64
        }
        *V.offset(i as isize) = 0.0f64;
        if i < q + 1 as libc::c_int {
            *V.offset(i as isize) = *theta.offset((i - 1 as libc::c_int) as isize)
        }
        i += 1
    }
    *a.offset(0 as libc::c_int as isize) = 0.0f64;
    if p == 0 as libc::c_int {
        *phi.offset(0 as libc::c_int as isize) = 0.0f64
    }
    *V.offset(0 as libc::c_int as isize) = 1.0f64;
    ind = r;
    j = 1 as libc::c_int;
    while j < r {
        vj = *V.offset(j as isize);
        i = j;
        while i < r {
            let fresh1 = ind;
            ind = ind + 1;
            *V.offset(fresh1 as isize) = *V.offset(i as isize) * vj;
            i += 1
        }
        j += 1
    }
    /*        Now find p(0). */
    if p > 0 as libc::c_int {
        /*      The set of equations s * vec(p(0)) = vec(v) is solved for
        vec(p(0)).  s is generated row by row in the array xnext.  The
        order of elements in p is changed, so as to bring more leading
        zeros into the rows of s. */
        i = 0 as libc::c_int;
        while i < nrbar {
            *rbar.offset(i as isize) = 0.0f64;
            i += 1
        }
        i = 0 as libc::c_int;
        while i < np {
            *P.offset(i as isize) = 0.0f64;
            *thetab.offset(i as isize) = 0.0f64;
            *xnext.offset(i as isize) = 0.0f64;
            i += 1
        }
        ind = 0 as libc::c_int;
        ind1 = -(1 as libc::c_int);
        npr = np - r;
        npr1 = npr + 1 as libc::c_int;
        indj = npr;
        ind2 = npr - 1 as libc::c_int;
        j = 0 as libc::c_int;
        while j < r {
            phij = *phi.offset(j as isize);
            let fresh2 = indj;
            indj = indj + 1;
            *xnext.offset(fresh2 as isize) = 0.0f64;
            indi = npr1 + j;
            i = j;
            while i < r {
                let fresh3 = ind;
                ind = ind + 1;
                ynext = *V.offset(fresh3 as isize);
                phii = *phi.offset(i as isize);
                if j != r - 1 as libc::c_int {
                    *xnext.offset(indj as isize) = -phii;
                    if i != r - 1 as libc::c_int {
                        *xnext.offset(indi as isize) -= phij;
                        ind1 += 1;
                        *xnext.offset(ind1 as isize) = -1.0f64
                    }
                }
                *xnext.offset(npr as isize) = -phii * phij;
                ind2 += 1;
                if ind2 >= np {
                    ind2 = 0 as libc::c_int
                }
                *xnext.offset(ind2 as isize) += 1.0f64;
                inclu2(np, xnext, xrow, ynext, P, rbar, thetab);
                *xnext.offset(ind2 as isize) = 0.0f64;
                if i != r - 1 as libc::c_int {
                    let fresh4 = indi;
                    indi = indi + 1;
                    *xnext.offset(fresh4 as isize) = 0.0f64;
                    *xnext.offset(ind1 as isize) = 0.0f64
                }
                i += 1
            }
            j += 1
        }
        ithisr = nrbar - 1 as libc::c_int;
        im = np - 1 as libc::c_int;
        i = 0 as libc::c_int;
        while i < np {
            bi = *thetab.offset(im as isize);
            jm = np - 1 as libc::c_int;
            j = 0 as libc::c_int;
            while j < i {
                let fresh5 = ithisr;
                ithisr = ithisr - 1;
                let fresh6 = jm;
                jm = jm - 1;
                bi -= *rbar.offset(fresh5 as isize) * *P.offset(fresh6 as isize);
                j += 1
            }
            let fresh7 = im;
            im = im - 1;
            *P.offset(fresh7 as isize) = bi;
            i += 1
        }
        /*        now re-order p. */
        ind = npr;
        i = 0 as libc::c_int;
        while i < r {
            let fresh8 = ind;
            ind = ind + 1;
            *xnext.offset(i as isize) = *P.offset(fresh8 as isize);
            i += 1
        }
        ind = np - 1 as libc::c_int;
        ind1 = npr - 1 as libc::c_int;
        i = 0 as libc::c_int;
        while i < npr {
            let fresh9 = ind1;
            ind1 = ind1 - 1;
            let fresh10 = ind;
            ind = ind - 1;
            *P.offset(fresh10 as isize) = *P.offset(fresh9 as isize);
            i += 1
        }
        i = 0 as libc::c_int;
        while i < r {
            *P.offset(i as isize) = *xnext.offset(i as isize);
            i += 1
        }
    } else {
        /* P(0) is obtained by backsubstitution for a moving average process. */
        indn = np;
        ind = np;
        i = 0 as libc::c_int;
        while i < r {
            j = 0 as libc::c_int;
            while j <= i {
                ind -= 1;
                *P.offset(ind as isize) = *V.offset(ind as isize);
                if j != 0 as libc::c_int {
                    indn -= 1;
                    *P.offset(ind as isize) += *P.offset(indn as isize)
                }
                j += 1
            }
            i += 1
        }
    };
}
#[no_mangle]
pub unsafe extern "C" fn karma(
    mut G: Starma,
    mut sumlog: *mut libc::c_double,
    mut ssq: *mut libc::c_double,
    mut iupd: libc::c_int,
    mut nit: *mut libc::c_int,
) {
    let mut p: libc::c_int = (*G).p;
    let mut q: libc::c_int = (*G).q;
    let mut r: libc::c_int = (*G).r;
    let mut n: libc::c_int = (*G).n;
    let mut nu: libc::c_int = 0 as libc::c_int;
    let mut phi: *mut libc::c_double = (*G).phi;
    let mut theta: *mut libc::c_double = (*G).theta;
    let mut a: *mut libc::c_double = (*G).a;
    let mut P: *mut libc::c_double = (*G).P;
    let mut V: *mut libc::c_double = (*G).V;
    let mut w: *mut libc::c_double = (*G).w;
    let mut resid: *mut libc::c_double = (*G).resid;
    let mut work: *mut libc::c_double = (*G).xnext;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut l: libc::c_int = 0;
    let mut ii: libc::c_int = 0;
    let mut ind: libc::c_int = 0;
    let mut indn: libc::c_int = 0;
    let mut indw: libc::c_int = 0;
    let mut a1: libc::c_double = 0.;
    let mut dt: libc::c_double = 0.;
    let mut et: libc::c_double = 0.;
    let mut ft: libc::c_double = 0.;
    let mut g: libc::c_double = 0.;
    let mut ut: libc::c_double = 0.;
    let mut phij: libc::c_double = 0.;
    let mut phijdt: libc::c_double = 0.;
    let mut current_block_82: u64;
    /*  Invoking this subroutine updates a, P, sumlog and ssq by inclusion
    of data values w(1) to w(n). the corresponding values of resid are
    also obtained.  When ft is less than (1 + delta), quick recursions
    are used. */
    /*        for non-zero values of nit, perform quick recursions. */
    if *nit == 0 as libc::c_int {
        i = 0 as libc::c_int;
        loop {
            if !(i < n) {
                current_block_82 = 7189308829251266000;
                break;
            }
            /*        prediction. */
            if iupd != 1 as libc::c_int || i > 0 as libc::c_int {
                /*        here dt = ft - 1.0 */
                dt = if r > 1 as libc::c_int {
                    *P.offset(r as isize)
                } else {
                    0.0f64
                };
                if dt < (*G).delta {
                    current_block_82 = 14008132735080916920;
                    break;
                }
                a1 = *a.offset(0 as libc::c_int as isize);
                j = 0 as libc::c_int;
                while j < r - 1 as libc::c_int {
                    *a.offset(j as isize) = *a.offset((j + 1 as libc::c_int) as isize);
                    j += 1
                }
                *a.offset((r - 1 as libc::c_int) as isize) = 0.0f64;
                j = 0 as libc::c_int;
                while j < p {
                    *a.offset(j as isize) += *phi.offset(j as isize) * a1;
                    j += 1
                }
                if *P.offset(0 as libc::c_int as isize) == 0.0f64 {
                    /* last obs was available */
                    ind = -(1 as libc::c_int);
                    indn = r;
                    j = 0 as libc::c_int;
                    while j < r {
                        l = j;
                        while l < r {
                            ind += 1;
                            *P.offset(ind as isize) = *V.offset(ind as isize);
                            if l < r - 1 as libc::c_int {
                                let fresh11 = indn;
                                indn = indn + 1;
                                *P.offset(ind as isize) += *P.offset(fresh11 as isize)
                            }
                            l += 1
                        }
                        j += 1
                    }
                } else {
                    j = 0 as libc::c_int;
                    while j < r {
                        *work.offset(j as isize) = *P.offset(j as isize);
                        j += 1
                    }
                    ind = -(1 as libc::c_int);
                    indn = r;
                    dt = *P.offset(0 as libc::c_int as isize);
                    j = 0 as libc::c_int;
                    while j < r {
                        phij = *phi.offset(j as isize);
                        phijdt = phij * dt;
                        l = j;
                        while l < r {
                            ind += 1;
                            *P.offset(ind as isize) =
                                *V.offset(ind as isize) + *phi.offset(l as isize) * phijdt;
                            if j < r - 1 as libc::c_int {
                                *P.offset(ind as isize) += *work
                                    .offset((j + 1 as libc::c_int) as isize)
                                    * *phi.offset(l as isize)
                            }
                            if l < r - 1 as libc::c_int {
                                let fresh12 = indn;
                                indn = indn + 1;
                                *P.offset(ind as isize) +=
                                    *work.offset((l + 1 as libc::c_int) as isize) * phij
                                        + *P.offset(fresh12 as isize)
                            }
                            l += 1
                        }
                        j += 1
                    }
                }
            }
            /*        updating. */
            ft = *P.offset(0 as libc::c_int as isize);
            if !((*w.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int) {
                ut = *w.offset(i as isize) - *a.offset(0 as libc::c_int as isize);
                if r > 1 as libc::c_int {
                    j = 1 as libc::c_int;
                    ind = r;
                    while j < r {
                        g = *P.offset(j as isize) / ft;
                        *a.offset(j as isize) += g * ut;
                        l = j;
                        while l < r {
                            let fresh13 = ind;
                            ind = ind + 1;
                            *P.offset(fresh13 as isize) -= g * *P.offset(l as isize);
                            l += 1
                        }
                        j += 1
                    }
                }
                *a.offset(0 as libc::c_int as isize) = *w.offset(i as isize);
                *resid.offset(i as isize) = ut / sqrt(ft);
                *ssq += ut * ut / ft;
                *sumlog += ft.ln();
                nu += 1;
                l = 0 as libc::c_int;
                while l < r {
                    *P.offset(l as isize) = 0.0f64;
                    l += 1
                }
            } else {
                *resid.offset(i as isize) = R_NaReal
            }
            i += 1
        }
        match current_block_82 {
            14008132735080916920 => {}
            _ => {
                *nit = n;
                current_block_82 = 12705158477165241210;
            }
        }
    } else {
        /*        quick recursions: never used with missing values */
        i = 0 as libc::c_int;
        current_block_82 = 14008132735080916920;
    }
    match current_block_82 {
        14008132735080916920 => {
            *nit = i;
            ii = i;
            while ii < n {
                et = *w.offset(ii as isize);
                indw = ii;
                j = 0 as libc::c_int;
                while j < p {
                    indw -= 1;
                    if indw < 0 as libc::c_int {
                        break;
                    }
                    et -= *phi.offset(j as isize) * *w.offset(indw as isize);
                    j += 1
                }
                j = 0 as libc::c_int;
                while j < (if ii > q { q } else { ii }) {
                    et -= *theta.offset(j as isize)
                        * *resid.offset((ii - j - 1 as libc::c_int) as isize);
                    j += 1
                }
                *resid.offset(ii as isize) = et;
                *ssq += et * et;
                nu += 1;
                ii += 1
            }
        }
        _ => {}
    }
    (*G).nused = nu;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2001-2017 The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/*  start of AS 182 */
#[no_mangle]
pub unsafe extern "C" fn forkal(
    mut G: Starma,
    mut d: libc::c_int,
    mut il: libc::c_int,
    mut delta: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut amse: *mut libc::c_double,
    mut ifault: *mut libc::c_int,
) {
    let mut p: libc::c_int = (*G).p;
    let mut q: libc::c_int = (*G).q;
    let mut r: libc::c_int = (*G).r;
    let mut n: libc::c_int = (*G).n;
    let mut np: libc::c_int = (*G).np;
    let mut phi: *mut libc::c_double = (*G).phi;
    let mut V: *mut libc::c_double = (*G).V;
    let mut w: *mut libc::c_double = (*G).w;
    let mut xrow: *mut libc::c_double = (*G).xrow;
    let mut a: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut P: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut store: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut rd: libc::c_int = r + d;
    let mut rz: libc::c_int = rd * (rd + 1 as libc::c_int) / 2 as libc::c_int;
    let mut phii: libc::c_double = 0.;
    let mut phij: libc::c_double = 0.;
    let mut sigma2: libc::c_double = 0.;
    let mut a1: libc::c_double = 0.;
    let mut aa: libc::c_double = 0.;
    let mut dt: libc::c_double = 0.;
    let mut phijdt: libc::c_double = 0.;
    let mut ams: libc::c_double = 0.;
    let mut tmp: libc::c_double = 0.;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut l: libc::c_int = 0;
    let mut nu: libc::c_int = 0 as libc::c_int;
    let mut k1: libc::c_int = 0;
    let mut i45: libc::c_int = 0;
    let mut jj: libc::c_int = 0;
    let mut kk: libc::c_int = 0;
    let mut lk: libc::c_int = 0;
    let mut ll: libc::c_int = 0;
    let mut nt: libc::c_int = 0;
    let mut kk1: libc::c_int = 0;
    let mut lk1: libc::c_int = 0;
    let mut ind: libc::c_int = 0;
    let mut jkl: libc::c_int = 0;
    let mut kkk: libc::c_int = 0;
    let mut ind1: libc::c_int = 0;
    let mut ind2: libc::c_int = 0;
    /*  Finite sample prediction from ARIMA processes. */
    /*  This routine will calculate the finite sample predictions
    and their conditional mean square errors for any ARIMA process. */
    /*     invoking this routine will calculate the finite sample predictions */
    /*     and their conditional mean square errors for any arima process. */
    store = R_alloc(
        rd as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    R_chk_free((*G).a as *mut libc::c_void);
    (*G).a = 0 as *mut libc::c_double;
    a = R_chk_calloc(
        rd as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).a = a;
    R_chk_free((*G).P as *mut libc::c_void);
    (*G).P = 0 as *mut libc::c_double;
    P = R_chk_calloc(
        rz as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).P = P;
    /*     check for input faults. */
    *ifault = 0 as libc::c_int;
    if p < 0 as libc::c_int {
        *ifault = 1 as libc::c_int
    }
    if q < 0 as libc::c_int {
        *ifault += 2 as libc::c_int
    }
    if p * p + q * q == 0 as libc::c_int {
        *ifault = 4 as libc::c_int
    }
    if r != (if p < q + 1 as libc::c_int {
        (q) + 1 as libc::c_int
    } else {
        p
    }) {
        *ifault = 5 as libc::c_int
    }
    if np != r * (r + 1 as libc::c_int) / 2 as libc::c_int {
        *ifault = 6 as libc::c_int
    }
    if d < 0 as libc::c_int {
        *ifault = 8 as libc::c_int
    }
    if il < 1 as libc::c_int {
        *ifault = 11 as libc::c_int
    }
    if *ifault != 0 as libc::c_int {
        return;
    }
    /*     Find initial likelihood conditions. */
    if r == 1 as libc::c_int {
        *a.offset(0 as libc::c_int as isize) = 0.0f64;
        *V.offset(0 as libc::c_int as isize) = 1.0f64;
        *P.offset(0 as libc::c_int as isize) = 1.0f64
            / (1.0f64
                - *phi.offset(0 as libc::c_int as isize) * *phi.offset(0 as libc::c_int as isize))
    } else {
        starma(G, ifault);
    }
    /*     Calculate data transformations */
    nt = n - d;
    if d > 0 as libc::c_int {
        j = 0 as libc::c_int;
        while j < d {
            *store.offset(j as isize) = *w.offset((n - j - 2 as libc::c_int) as isize);
            if (*store.offset(j as isize)).is_nan() as i32 != 0 as libc::c_int {
                error(
                    dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"missing value in last %d observations\x00" as *const u8
                            as *const libc::c_char,
                        5 as libc::c_int,
                    ),
                    d,
                );
            }
            j += 1
        }
        i = 0 as libc::c_int;
        while i < nt {
            aa = 0.0f64;
            k = 0 as libc::c_int;
            while k < d {
                aa -=
                    *delta.offset(k as isize) * *w.offset((d + i - k - 1 as libc::c_int) as isize);
                k += 1
            }
            *w.offset(i as isize) = *w.offset((i + d) as isize) + aa;
            i += 1
        }
    }
    /*     Evaluate likelihood to obtain final Kalman filter conditions */
    let mut sumlog: libc::c_double = 0.0f64;
    let mut ssq: libc::c_double = 0.0f64;
    let mut nit: libc::c_int = 0 as libc::c_int;
    (*G).n = nt;
    karma(G, &mut sumlog, &mut ssq, 1 as libc::c_int, &mut nit);
    /*     Calculate m.l.e. of sigma squared */
    sigma2 = 0.0f64;
    j = 0 as libc::c_int;
    while j < nt {
        /* macOS/gcc 3.5 didn't have isnan defined properly */
        tmp = *(*G).resid.offset(j as isize);
        if !(tmp.is_nan() as i32 != 0 as libc::c_int) {
            nu += 1;
            sigma2 += tmp * tmp
        }
        j += 1
    }
    sigma2 /= nu as libc::c_double;
    /*     reset the initial a and P when differencing occurs */
    if d > 0 as libc::c_int {
        i = 0 as libc::c_int;
        while i < np {
            *xrow.offset(i as isize) = *P.offset(i as isize);
            i += 1
        }
        i = 0 as libc::c_int;
        while i < rz {
            *P.offset(i as isize) = 0.0f64;
            i += 1
        }
        ind = 0 as libc::c_int;
        j = 0 as libc::c_int;
        while j < r {
            k = j * (rd + 1 as libc::c_int) - j * (j + 1 as libc::c_int) / 2 as libc::c_int;
            i = j;
            while i < r {
                let fresh14 = ind;
                ind = ind + 1;
                let fresh15 = k;
                k = k + 1;
                *P.offset(fresh15 as isize) = *xrow.offset(fresh14 as isize);
                i += 1
            }
            j += 1
        }
        j = 0 as libc::c_int;
        while j < d {
            *a.offset((r + j) as isize) = *store.offset(j as isize);
            j += 1
        }
    }
    i45 = 2 as libc::c_int * rd + 1 as libc::c_int;
    jkl = r * (2 as libc::c_int * d + r + 1 as libc::c_int) / 2 as libc::c_int;
    l = 0 as libc::c_int;
    while l < il {
        /*     predict a */
        a1 = *a.offset(0 as libc::c_int as isize);
        i = 0 as libc::c_int;
        while i < r - 1 as libc::c_int {
            *a.offset(i as isize) = *a.offset((i + 1 as libc::c_int) as isize);
            i += 1
        }
        *a.offset((r - 1 as libc::c_int) as isize) = 0.0f64;
        j = 0 as libc::c_int;
        while j < p {
            *a.offset(j as isize) += *phi.offset(j as isize) * a1;
            j += 1
        }
        if d > 0 as libc::c_int {
            j = 0 as libc::c_int;
            while j < d {
                a1 += *delta.offset(j as isize) * *a.offset((r + j) as isize);
                j += 1
            }
            i = rd - 1 as libc::c_int;
            while i > r {
                *a.offset(i as isize) = *a.offset((i - 1 as libc::c_int) as isize);
                i -= 1
            }
            *a.offset(r as isize) = a1
        }
        /*     predict P */
        if d > 0 as libc::c_int {
            i = 0 as libc::c_int;
            while i < d {
                *store.offset(i as isize) = 0.0f64;
                j = 0 as libc::c_int;
                while j < d {
                    ll = if i < j { j } else { i };
                    k = if i > j { j } else { i };
                    jj = jkl
                        + (ll - k)
                        + k * (2 as libc::c_int * d + 2 as libc::c_int - k - 1 as libc::c_int)
                            / 2 as libc::c_int;
                    *store.offset(i as isize) += *delta.offset(j as isize) * *P.offset(jj as isize);
                    j += 1
                }
                i += 1
            }
            if d > 1 as libc::c_int {
                j = 0 as libc::c_int;
                while j < d - 1 as libc::c_int {
                    jj = d - j - 1 as libc::c_int;
                    lk = (jj - 1 as libc::c_int) * (2 as libc::c_int * d + 2 as libc::c_int - jj)
                        / 2 as libc::c_int
                        + jkl;
                    lk1 = jj * (2 as libc::c_int * d + 1 as libc::c_int - jj) / 2 as libc::c_int
                        + jkl;
                    i = 0 as libc::c_int;
                    while i <= j {
                        let fresh16 = lk;
                        lk = lk + 1;
                        let fresh17 = lk1;
                        lk1 = lk1 + 1;
                        *P.offset(fresh17 as isize) = *P.offset(fresh16 as isize);
                        i += 1
                    }
                    j += 1
                }
                j = 0 as libc::c_int;
                while j < d - 1 as libc::c_int {
                    *P.offset((jkl + j + 1 as libc::c_int) as isize) =
                        *store.offset(j as isize) + *P.offset((r + j) as isize);
                    j += 1
                }
            }
            *P.offset(jkl as isize) = *P.offset(0 as libc::c_int as isize);
            i = 0 as libc::c_int;
            while i < d {
                *P.offset(jkl as isize) += *delta.offset(i as isize)
                    * (*store.offset(i as isize) + 2.0f64 * *P.offset((r + i) as isize));
                i += 1
            }
            i = 0 as libc::c_int;
            while i < d {
                *store.offset(i as isize) = *P.offset((r + i) as isize);
                i += 1
            }
            j = 0 as libc::c_int;
            while j < r {
                kk1 = (j + 1 as libc::c_int) * (2 as libc::c_int * rd - j - 2 as libc::c_int)
                    / 2 as libc::c_int
                    + r;
                k1 = j * (2 as libc::c_int * rd - j - 1 as libc::c_int) / 2 as libc::c_int + r;
                i = 0 as libc::c_int;
                while i < d {
                    kk = kk1 + i;
                    k = k1 + i;
                    *P.offset(k as isize) = *phi.offset(j as isize) * *store.offset(i as isize);
                    if j < r - 1 as libc::c_int {
                        *P.offset(k as isize) += *P.offset(kk as isize)
                    }
                    i += 1
                }
                j += 1
            }
            j = 0 as libc::c_int;
            while j < r {
                *store.offset(j as isize) = 0.0f64;
                kkk = (j + 1 as libc::c_int) * (i45 - j - 1 as libc::c_int) / 2 as libc::c_int - d;
                i = 0 as libc::c_int;
                while i < d {
                    let fresh18 = kkk;
                    kkk = kkk + 1;
                    *store.offset(j as isize) +=
                        *delta.offset(i as isize) * *P.offset(fresh18 as isize);
                    i += 1
                }
                j += 1
            }
            j = 0 as libc::c_int;
            while j < r {
                k = (j + 1 as libc::c_int) * (rd + 1 as libc::c_int)
                    - (j + 1 as libc::c_int) * (j + 2 as libc::c_int) / 2 as libc::c_int;
                i = 0 as libc::c_int;
                while i < d - 1 as libc::c_int {
                    k -= 1;
                    *P.offset(k as isize) = *P.offset((k - 1 as libc::c_int) as isize);
                    i += 1
                }
                j += 1
            }
            j = 0 as libc::c_int;
            while j < r {
                k = j * (2 as libc::c_int * rd - j - 1 as libc::c_int) / 2 as libc::c_int + r;
                *P.offset(k as isize) = *store.offset(j as isize)
                    + *phi.offset(j as isize) * *P.offset(0 as libc::c_int as isize);
                if j < r - 1 as libc::c_int {
                    *P.offset(k as isize) += *P.offset((j + 1 as libc::c_int) as isize)
                }
                j += 1
            }
        }
        i = 0 as libc::c_int;
        while i < r {
            *store.offset(i as isize) = *P.offset(i as isize);
            i += 1
        }
        ind = 0 as libc::c_int;
        dt = *P.offset(0 as libc::c_int as isize);
        j = 0 as libc::c_int;
        while j < r {
            phij = *phi.offset(j as isize);
            phijdt = phij * dt;
            ind2 = j * (2 as libc::c_int * rd - j + 1 as libc::c_int) / 2 as libc::c_int
                - 1 as libc::c_int;
            ind1 = (j + 1 as libc::c_int) * (i45 - j - 1 as libc::c_int) / 2 as libc::c_int
                - 1 as libc::c_int;
            i = j;
            while i < r {
                ind2 += 1;
                phii = *phi.offset(i as isize);
                let fresh19 = ind;
                ind = ind + 1;
                *P.offset(ind2 as isize) = *V.offset(fresh19 as isize) + phii * phijdt;
                if j < r - 1 as libc::c_int {
                    *P.offset(ind2 as isize) +=
                        *store.offset((j + 1 as libc::c_int) as isize) * phii
                }
                if i < r - 1 as libc::c_int {
                    ind1 += 1;
                    *P.offset(ind2 as isize) += *store.offset((i + 1 as libc::c_int) as isize)
                        * phij
                        + *P.offset(ind1 as isize)
                }
                i += 1
            }
            j += 1
        }
        /*     predict y */
        *y.offset(l as isize) = *a.offset(0 as libc::c_int as isize);
        j = 0 as libc::c_int;
        while j < d {
            *y.offset(l as isize) += *a.offset((r + j) as isize) * *delta.offset(j as isize);
            j += 1
        }
        /*     calculate m.s.e. of y */
        ams = *P.offset(0 as libc::c_int as isize);
        if d > 0 as libc::c_int {
            j = 0 as libc::c_int;
            while j < d {
                k = r * (i45 - r) / 2 as libc::c_int
                    + j * (2 as libc::c_int * d + 1 as libc::c_int - j) / 2 as libc::c_int;
                tmp = *delta.offset(j as isize);
                ams +=
                    2.0f64 * tmp * *P.offset((r + j) as isize) + *P.offset(k as isize) * tmp * tmp;
                j += 1
            }
            j = 0 as libc::c_int;
            while j < d - 1 as libc::c_int {
                k = r * (i45 - r) / 2 as libc::c_int
                    + 1 as libc::c_int
                    + j * (2 as libc::c_int * d + 1 as libc::c_int - j) / 2 as libc::c_int;
                i = j + 1 as libc::c_int;
                while i < d {
                    let fresh20 = k;
                    k = k + 1;
                    ams += 2.0f64
                        * *delta.offset(i as isize)
                        * *delta.offset(j as isize)
                        * *P.offset(fresh20 as isize);
                    i += 1
                }
                j += 1
            }
        }
        *amse.offset(l as isize) = ams * sigma2;
        l += 1
    }
}
