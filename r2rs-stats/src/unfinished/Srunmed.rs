use crate::sexprec::{SEXP, SEXPTYPE};
use ::libc;
extern "C" {
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2016    The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn Rprintf(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn REprintf(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn TYPEOF(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn XLENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn IS_LONG_VEC(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    /* Defining NO_RINLINEDFUNS disables use to simulate platforms where
    this is not available */
    /* need remapped names here for use with R_NO_REMAP */
    /*
       These are the inlinable functions that are provided in Rinlinedfuns.h
       It is *essential* that these do not appear in any other header file,
       with or without the Rf_ prefix.
    */
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
pub type R_xlen_t = ptrdiff_t;
/*
* Copyright (C) 2012-2019  The R Core Team
* Copyright (C) 2003 ff.   The R Foundation
* Copyright (C) 2000-2 Martin Maechler <maechler@stat.math.ethz.ch>
* Copyright (C) 1995   Berwin A. Turlach <berwin@alphasun.anu.edu.au>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, a copy is available at
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* These routines implement a running median smoother according to the
 * algorithm described in Haerdle und Steiger (1995).
 *
 * A tech-report of that paper is available under
 * ftp://amadeus.wiwi.hu-berlin.de/pub/papers/sfb/sfb1994/dpsfb940015.ps.Z
 *
 * The current implementation does not use any global variables!
 */
/* Changes for R port by Martin Maechler ((C) above):
 *
 *  s/long/int/   R uses int, not long (as S does)
 *  s/void/static void/  most routines are internal
 *
 * Added  print_level  and end_rule  arguments
 */
/* Variable name descri- | Identities from paper
 * name here paper ption   | (1-indexing)
 * ---------    ----- -----------------------------------
 * window[]      H      the array containing the double heap
 * data[]        X      the data (left intact)
 * ...   i 1st permuter  H[i[m]]    == X[i + m]
 * ...   j 2nd permuter  X[i +j[m]] == H[m]
 */
unsafe extern "C" fn swap(
    mut l: libc::c_int,
    mut r: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    /* swap positions `l' and `r' in window[] and nrlist[]
     *
     * ---- Used in R_heapsort() and many other routines
     */
    let mut nl: libc::c_int = 0;
    let mut nr: libc::c_int = 0;
    let mut tmp: libc::c_double = 0.;
    if print_level >= 3 as libc::c_int {
        Rprintf(b"SW(%d,%d) \x00" as *const u8 as *const libc::c_char, l, r);
    }
    tmp = *window.offset(l as isize);
    *window.offset(l as isize) = *window.offset(r as isize);
    *window.offset(r as isize) = tmp;
    nl = *nrlist.offset(l as isize);
    nr = *nrlist.offset(r as isize);
    *nrlist.offset(l as isize) = nr;
    *nrlist.offset(r as isize) = nl;
    *outlist.offset(nl as isize) = r;
    *outlist.offset(nr as isize) = l;
}
unsafe extern "C" fn siftup(
    mut l: libc::c_int,
    mut r: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    /* Used only in R_heapsort() */
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut nrold: libc::c_int = 0;
    let mut x: libc::c_double = 0.;
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"siftup(%d,%d) \x00" as *const u8 as *const libc::c_char,
            l,
            r,
        );
    }
    i = l;
    j = 2 as libc::c_int * i;
    x = *window.offset(i as isize);
    nrold = *nrlist.offset(i as isize);
    while j <= r {
        if j < r {
            if *window.offset(j as isize) < *window.offset((j + 1 as libc::c_int) as isize) {
                j += 1
            }
        }
        if x >= *window.offset(j as isize) {
            break;
        }
        *window.offset(i as isize) = *window.offset(j as isize);
        *outlist.offset(*nrlist.offset(j as isize) as isize) = i;
        *nrlist.offset(i as isize) = *nrlist.offset(j as isize);
        i = j;
        j = 2 as libc::c_int * i
    }
    *window.offset(i as isize) = x;
    *outlist.offset(nrold as isize) = i;
    *nrlist.offset(i as isize) = nrold;
}
unsafe extern "C" fn R_heapsort(
    mut low: libc::c_int,
    mut up: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    let mut l: libc::c_int = 0;
    let mut u: libc::c_int = 0;
    l = up / 2 as libc::c_int + 1 as libc::c_int;
    u = up;
    while l > low {
        l -= 1;
        siftup(l, u, window, outlist, nrlist, print_level);
    }
    while u > low {
        swap(l, u, window, outlist, nrlist, print_level);
        u -= 1;
        siftup(l, u, window, outlist, nrlist, print_level);
    }
}
unsafe extern "C" fn inittree(
    mut n: R_xlen_t,
    mut k: libc::c_int,
    mut k2: libc::c_int,
    mut data: *const libc::c_double,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    let mut i: libc::c_int = 0;
    let mut k2p1: libc::c_int = 0;
    let mut big: libc::c_double = 0.;
    i = 1 as libc::c_int;
    while i <= k {
        /* use 1-indexing for our three arrays !*/
        *window.offset(i as isize) = *data.offset((i - 1 as libc::c_int) as isize);
        let ref mut fresh0 = *outlist.offset(i as isize);
        *fresh0 = i;
        *nrlist.offset(i as isize) = *fresh0;
        i += 1
    }
    /* sort the window[] -- sort *only* called here */
    R_heapsort(1 as libc::c_int, k, window, outlist, nrlist, print_level);
    big = (*window.offset(k as isize)).abs();
    if big < (*window.offset(1 as libc::c_int as isize)).abs() {
        big = (*window.offset(1 as libc::c_int as isize)).abs()
    }
    /* big := max | X[1..k] | */
    i = k;
    while (i as libc::c_long) < n {
        if big < (*data.offset(i as isize)).abs() {
            big = (*data.offset(i as isize)).abs()
        }
        i += 1
    }
    /* big == max(|data_i|,  i = 1,..,n) */
    big = 1 as libc::c_int as libc::c_double + 2.0f64 * big; /* such that -big < data[] < +big (strictly !) */
    i = k;
    while i > 0 as libc::c_int {
        *window.offset((i + k2) as isize) = *window.offset(i as isize);
        *nrlist.offset((i + k2) as isize) = *nrlist.offset(i as isize) - 1 as libc::c_int;
        i -= 1
    }
    i = 0 as libc::c_int;
    while i < k {
        *outlist.offset(i as isize) = *outlist.offset((i + 1 as libc::c_int) as isize) + k2;
        i += 1
    }
    k2p1 = k2 + 1 as libc::c_int;
    i = 0 as libc::c_int;
    while i < k2p1 {
        *window.offset(i as isize) = -big;
        *window.offset((k + k2p1 + i) as isize) = big;
        i += 1
    }
}
/* inittree*/
unsafe extern "C" fn toroot(
    mut outvirt: libc::c_int,
    mut k: libc::c_int,
    mut nrnew: R_xlen_t,
    mut outnext: libc::c_int,
    mut data: *const libc::c_double,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    let mut father: libc::c_int = 0;
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"toroot(%d, %d,%d) \x00" as *const u8 as *const libc::c_char,
            k,
            nrnew as libc::c_int,
            outnext,
        );
    }
    loop {
        father = outvirt / 2 as libc::c_int;
        *window.offset((outvirt + k) as isize) = *window.offset((father + k) as isize);
        *outlist.offset(*nrlist.offset((father + k) as isize) as isize) = outvirt + k;
        *nrlist.offset((outvirt + k) as isize) = *nrlist.offset((father + k) as isize);
        outvirt = father;
        if !(father != 0 as libc::c_int) {
            break;
        }
    }
    *window.offset(k as isize) = *data.offset(nrnew as isize);
    *outlist.offset(outnext as isize) = k;
    *nrlist.offset(k as isize) = outnext;
}
unsafe extern "C" fn downtoleave(
    mut outvirt: libc::c_int,
    mut k: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    let mut childl: libc::c_int = 0;
    let mut childr: libc::c_int = 0;
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"\n downtoleave(%d, %d)\n   \x00" as *const u8 as *const libc::c_char,
            outvirt,
            k,
        );
    }
    loop {
        childl = outvirt * 2 as libc::c_int;
        childr = childl - 1 as libc::c_int;
        if *window.offset((childl + k) as isize) < *window.offset((childr + k) as isize) {
            childl = childr
        }
        if *window.offset((outvirt + k) as isize) >= *window.offset((childl + k) as isize) {
            break;
        }
        /* seg.fault happens here: invalid outvirt/childl ? */
        swap(
            outvirt + k,
            childl + k,
            window,
            outlist,
            nrlist,
            print_level,
        );
        outvirt = childl
    }
}
unsafe extern "C" fn uptoleave(
    mut outvirt: libc::c_int,
    mut k: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    let mut childl: libc::c_int = 0;
    let mut childr: libc::c_int = 0;
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"\n uptoleave(%d, %d)\n   \x00" as *const u8 as *const libc::c_char,
            outvirt,
            k,
        );
    }
    loop {
        childl = outvirt * 2 as libc::c_int;
        childr = childl + 1 as libc::c_int;
        if *window.offset((childl + k) as isize) > *window.offset((childr + k) as isize) {
            childl = childr
        }
        if *window.offset((outvirt + k) as isize) <= *window.offset((childl + k) as isize) {
            break;
        }
        swap(
            outvirt + k,
            childl + k,
            window,
            outlist,
            nrlist,
            print_level,
        );
        outvirt = childl
    }
}
unsafe extern "C" fn upperoutupperin(
    mut outvirt: libc::c_int,
    mut k: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    let mut father: libc::c_int = 0;
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"\nUpperoutUPPERin(%d, %d)\n  \x00" as *const u8 as *const libc::c_char,
            outvirt,
            k,
        );
    }
    uptoleave(outvirt, k, window, outlist, nrlist, print_level);
    father = outvirt / 2 as libc::c_int;
    while *window.offset((outvirt + k) as isize) < *window.offset((father + k) as isize) {
        swap(
            outvirt + k,
            father + k,
            window,
            outlist,
            nrlist,
            print_level,
        );
        outvirt = father;
        father = outvirt / 2 as libc::c_int
    }
    if print_level >= 2 as libc::c_int {
        Rprintf(b"\n\x00" as *const u8 as *const libc::c_char);
    };
}
unsafe extern "C" fn upperoutdownin(
    mut outvirt: libc::c_int,
    mut k: libc::c_int,
    mut nrnew: R_xlen_t,
    mut outnext: libc::c_int,
    mut data: *const libc::c_double,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"\n__upperoutDOWNin(%d, %d)\n  \x00" as *const u8 as *const libc::c_char,
            outvirt,
            k,
        );
    }
    toroot(
        outvirt,
        k,
        nrnew,
        outnext,
        data,
        window,
        outlist,
        nrlist,
        print_level,
    );
    if *window.offset(k as isize) < *window.offset((k - 1 as libc::c_int) as isize) {
        swap(
            k,
            k - 1 as libc::c_int,
            window,
            outlist,
            nrlist,
            print_level,
        );
        downtoleave(-(1 as libc::c_int), k, window, outlist, nrlist, print_level);
    };
}
unsafe extern "C" fn downoutdownin(
    mut outvirt: libc::c_int,
    mut k: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    let mut father: libc::c_int = 0;
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"\nDownoutDOWNin(%d, %d)\n  \x00" as *const u8 as *const libc::c_char,
            outvirt,
            k,
        );
    }
    downtoleave(outvirt, k, window, outlist, nrlist, print_level);
    father = outvirt / 2 as libc::c_int;
    while *window.offset((outvirt + k) as isize) > *window.offset((father + k) as isize) {
        swap(
            outvirt + k,
            father + k,
            window,
            outlist,
            nrlist,
            print_level,
        );
        outvirt = father;
        father = outvirt / 2 as libc::c_int
    }
    if print_level >= 2 as libc::c_int {
        Rprintf(b"\n\x00" as *const u8 as *const libc::c_char);
    };
}
unsafe extern "C" fn downoutupperin(
    mut outvirt: libc::c_int,
    mut k: libc::c_int,
    mut nrnew: R_xlen_t,
    mut outnext: libc::c_int,
    mut data: *const libc::c_double,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"\n__downoutUPPERin(%d, %d)\n  \x00" as *const u8 as *const libc::c_char,
            outvirt,
            k,
        );
    }
    toroot(
        outvirt,
        k,
        nrnew,
        outnext,
        data,
        window,
        outlist,
        nrlist,
        print_level,
    );
    if *window.offset(k as isize) > *window.offset((k + 1 as libc::c_int) as isize) {
        swap(
            k,
            k + 1 as libc::c_int,
            window,
            outlist,
            nrlist,
            print_level,
        );
        uptoleave(1 as libc::c_int, k, window, outlist, nrlist, print_level);
    };
}
unsafe extern "C" fn wentoutone(
    mut k: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"\nwentOUT_1(%d)\n  \x00" as *const u8 as *const libc::c_char,
            k,
        );
    }
    swap(
        k,
        k + 1 as libc::c_int,
        window,
        outlist,
        nrlist,
        print_level,
    );
    uptoleave(1 as libc::c_int, k, window, outlist, nrlist, print_level);
}
unsafe extern "C" fn wentouttwo(
    mut k: libc::c_int,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut print_level: libc::c_int,
) {
    if print_level >= 2 as libc::c_int {
        Rprintf(
            b"\nwentOUT_2(%d)\n  \x00" as *const u8 as *const libc::c_char,
            k,
        );
    }
    swap(
        k,
        k - 1 as libc::c_int,
        window,
        outlist,
        nrlist,
        print_level,
    );
    downtoleave(-(1 as libc::c_int), k, window, outlist, nrlist, print_level);
}
/* For Printing `diagnostics' : */
unsafe extern "C" fn runmedint(
    mut n: R_xlen_t,
    mut k: libc::c_int,
    mut k2: libc::c_int,
    mut data: *const libc::c_double,
    mut median: *mut libc::c_double,
    mut window: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut end_rule: libc::c_int,
    mut print_level: libc::c_int,
) {
    /* Running Median of `k' ,  k == 2*k2 + 1 *
     * end_rule == 0: leave values at the end,
     *          otherwise: "constant" end values
     */
    let mut outnext: libc::c_int = 0;
    let mut out: libc::c_int = 0;
    let mut outvirt: libc::c_int = 0;
    if end_rule != 0 {
        let mut i: libc::c_int = 0 as libc::c_int;
        while i <= k2 {
            let fresh1 = i;
            i = i + 1;
            *median.offset(fresh1 as isize) = *window.offset(k as isize)
        }
    } else {
        let mut i_0: libc::c_int = 0 as libc::c_int;
        while i_0 < k2 {
            *median.offset(i_0 as isize) = *data.offset(i_0 as isize);
            i_0 += 1
        }
        *median.offset(k2 as isize) = *window.offset(k as isize)
    }
    outnext = 0 as libc::c_int;
    let mut i_1: R_xlen_t = (k2 + 1 as libc::c_int) as R_xlen_t;
    while i_1 < n - k2 as libc::c_long {
        /* compute (0-index) median[i] == X*_{i+1} */
        out = *outlist.offset(outnext as isize);
        let mut nrnew: R_xlen_t = i_1 + k2 as libc::c_long;
        *window.offset(out as isize) = *data.offset(nrnew as isize);
        outvirt = out - k;
        if out > k {
            if *data.offset(nrnew as isize) >= *window.offset(k as isize) {
                upperoutupperin(outvirt, k, window, outlist, nrlist, print_level);
            } else {
                upperoutdownin(
                    outvirt,
                    k,
                    nrnew,
                    outnext,
                    data,
                    window,
                    outlist,
                    nrlist,
                    print_level,
                );
            }
        } else if out < k {
            if *data.offset(nrnew as isize) < *window.offset(k as isize) {
                downoutdownin(outvirt, k, window, outlist, nrlist, print_level);
            } else {
                downoutupperin(
                    outvirt,
                    k,
                    nrnew,
                    outnext,
                    data,
                    window,
                    outlist,
                    nrlist,
                    print_level,
                );
            }
        } else if *window.offset(k as isize) > *window.offset((k + 1 as libc::c_int) as isize) {
            wentoutone(k, window, outlist, nrlist, print_level);
        } else if *window.offset(k as isize) < *window.offset((k - 1 as libc::c_int) as isize) {
            wentouttwo(k, window, outlist, nrlist, print_level);
        }
        *median.offset(i_1 as isize) = *window.offset(k as isize);
        outnext = (outnext + 1 as libc::c_int) % k;
        i_1 += 1
    }
    if end_rule != 0 {
        let mut i_2: R_xlen_t = n - k2 as libc::c_long;
        while i_2 < n {
            let fresh2 = i_2;
            i_2 = i_2 + 1;
            *median.offset(fresh2 as isize) = *window.offset(k as isize)
        }
    } else {
        let mut i_3: R_xlen_t = n - k2 as libc::c_long;
        while i_3 < n {
            *median.offset(i_3 as isize) = *data.offset(i_3 as isize);
            i_3 += 1
        }
    };
}
/* runmedint() */
// Main function called from runmed() in ./Srunmed.c :
unsafe extern "C" fn Trunmed(
    mut n: R_xlen_t,
    mut k: libc::c_int,
    mut data: *const libc::c_double,
    mut median: *mut libc::c_double,
    mut outlist: *mut libc::c_int,
    mut nrlist: *mut libc::c_int,
    mut window: *mut libc::c_double,
    mut end_rule: libc::c_int,
    mut print_level: libc::c_int,
) {
    let mut k2: libc::c_int = (k - 1 as libc::c_int) / 2 as libc::c_int;
    let mut j: libc::c_int = 0;
    inittree(n, k, k2, data, window, outlist, nrlist, print_level);
    /* window[], outlist[], and nrlist[] are all 1-based (indices) */
    if print_level != 0 {
        Rprintf(b"After inittree():\n\x00" as *const u8 as *const libc::c_char);
        Rprintf(
            b" %9s: \x00" as *const u8 as *const libc::c_char,
            b"j\x00" as *const u8 as *const libc::c_char,
        );
        j = 0 as libc::c_int;
        while j <= 2 as libc::c_int * k {
            Rprintf(b"%6d\x00" as *const u8 as *const libc::c_char, j);
            j += 1
        }
        Rprintf(b"\n\x00" as *const u8 as *const libc::c_char);
        Rprintf(
            b" %9s: \x00" as *const u8 as *const libc::c_char,
            b"window []\x00" as *const u8 as *const libc::c_char,
        );
        j = 0 as libc::c_int;
        while j <= 2 as libc::c_int * k {
            Rprintf(
                b"%6g\x00" as *const u8 as *const libc::c_char,
                *window.offset(j as isize),
            );
            j += 1
        }
        Rprintf(b"\n\x00" as *const u8 as *const libc::c_char);
        Rprintf(
            b" %9s: \x00" as *const u8 as *const libc::c_char,
            b" nrlist[]\x00" as *const u8 as *const libc::c_char,
        );
        j = 0 as libc::c_int;
        while j <= 2 as libc::c_int * k {
            Rprintf(
                b"%6d\x00" as *const u8 as *const libc::c_char,
                *nrlist.offset(j as isize),
            );
            j += 1
        }
        Rprintf(b"\n\x00" as *const u8 as *const libc::c_char);
        Rprintf(
            b" %9s: \x00" as *const u8 as *const libc::c_char,
            b"outlist[]\x00" as *const u8 as *const libc::c_char,
        );
        j = 0 as libc::c_int;
        while j <= 2 as libc::c_int * k {
            Rprintf(
                b"%6d\x00" as *const u8 as *const libc::c_char,
                if j <= k2 || j > k + k2 {
                    -(9 as libc::c_int)
                } else {
                    *outlist.offset((j - k2) as isize)
                },
            );
            j += 1
        }
        Rprintf(b"\n\x00" as *const u8 as *const libc::c_char);
    }
    runmedint(
        n,
        k,
        k2,
        data,
        median,
        window,
        outlist,
        nrlist,
        end_rule,
        print_level,
    );
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995--2002 Martin Maechler <maechler@stat.math.ethz.ch>
 *  Copyright (C) 2003       The R Foundation
 *  Copyright (C) 2012-2016  The R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
unsafe extern "C" fn Srunmed(
    mut y: *mut libc::c_double,
    mut smo: *mut libc::c_double,
    mut n: R_xlen_t,
    mut bw: libc::c_int,
    mut end_rule: libc::c_int,
    mut debug: libc::c_int,
) {
    /*
    *  Computes "Running Median" smoother with medians of 'band'

    *  Input:
    * y(n) - responses in order of increasing predictor values
    * n - number of observations
    * bw - span of running medians (MUST be ODD !!)
    * end_rule -- 0: Keep original data at ends {j; j < b2 | j > n-b2}
    *   -- 1: Constant ends = median(y[1],..,y[bw]) "robust"
    *  Output:
    * smo(n) - smoothed responses

    * NOTE:  The 'end' values are just copied !! this is fast but not too nice !
    */
    /* Local variables */
    let mut rmed: libc::c_double = 0.;
    let mut rmin: libc::c_double = 0.;
    let mut temp: libc::c_double = 0.;
    let mut rnew: libc::c_double = 0.;
    let mut yout: libc::c_double = 0.;
    let mut yi: libc::c_double = 0.;
    let mut rbe: libc::c_double = 0.;
    let mut rtb: libc::c_double = 0.;
    let mut rse: libc::c_double = 0.;
    let mut yin: libc::c_double = 0.;
    let mut rts: libc::c_double = 0.;
    let mut imin: libc::c_int = 0;
    let mut ismo: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut first: libc::c_int = 0;
    let mut last: libc::c_int = 0;
    let mut band2: libc::c_int = 0;
    let mut kminus: libc::c_int = 0;
    let mut kplus: libc::c_int = 0;
    let mut scrat: *mut libc::c_double = R_alloc(
        bw as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    /*was  malloc( (unsigned) bw * sizeof(double));*/
    if bw as libc::c_long > n {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bandwidth/span of running medians is larger than n\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    /* 1. Compute  'rmed' := Median of the first 'band' values
    ======================================================== */
    let mut i_0: libc::c_int = 0 as libc::c_int;
    while i_0 < bw {
        *scrat.offset(i_0 as isize) = *y.offset(i_0 as isize);
        i_0 += 1
    }
    /* find minimal value  rmin = scrat[imin] <= scrat[j] */
    rmin = *scrat.offset(0 as libc::c_int as isize);
    imin = 0 as libc::c_int;
    let mut i_1: libc::c_int = 1 as libc::c_int;
    while i_1 < bw {
        if *scrat.offset(i_1 as isize) < rmin {
            rmin = *scrat.offset(i_1 as isize);
            imin = i_1
        }
        i_1 += 1
    }
    /* swap scrat[0] <-> scrat[imin] */
    temp = *scrat.offset(0 as libc::c_int as isize);
    *scrat.offset(0 as libc::c_int as isize) = rmin;
    *scrat.offset(imin as isize) = temp;
    /* sort the rest of of scrat[] by bubble (?) sort -- */
    let mut i_2: libc::c_int = 2 as libc::c_int;
    while i_2 < bw {
        if *scrat.offset(i_2 as isize) < *scrat.offset((i_2 - 1 as libc::c_int) as isize) {
            /* find the proper place for scrat[i] */
            temp = *scrat.offset(i_2 as isize); /* now:  scrat[j-1] <= temp */
            j = i_2; /* == Median( y[(1:band2)-1] ) */
            loop {
                *scrat.offset(j as isize) = *scrat.offset((j - 1 as libc::c_int) as isize);
                j -= 1;
                if !(*scrat.offset((j - 1 as libc::c_int) as isize) > temp) {
                    break;
                }
            }
            *scrat.offset(j as isize) = temp
        }
        i_2 += 1
    }
    band2 = bw / 2 as libc::c_int;
    rmed = *scrat.offset(band2 as isize);
    /* "malloc" had  free( (char*) scrat);*/
    /*-- release scratch memory --*/
    if end_rule == 0 as libc::c_int {
        /*-- keep DATA at end values */
        i = 0 as libc::c_int;
        while i < band2 {
            *smo.offset(i as isize) = *y.offset(i as isize);
            i += 1
        }
    } else {
        /* if(end_rule == 1)  copy median to CONSTANT end values */
        i = 0 as libc::c_int;
        while i < band2 {
            *smo.offset(i as isize) = rmed;
            i += 1
        }
    }
    *smo.offset(band2 as isize) = rmed;
    band2 += 1;
    /* = bw / 2 + 1*/
    if debug != 0 {
        REprintf(
            b"(bw,b2)= (%d,%d)\n\x00" as *const u8 as *const libc::c_char,
            bw,
            band2,
        );
    }
    /* Big FOR Loop: RUNNING median, update the median 'rmed'
    ======================================================= */
    first = 1 as libc::c_int; /*     END FOR ------------ big Loop -------------------- */
    last = bw; /* New median = old one   in all the simple cases --*/
    ismo = band2; /* else: yin == rmed -- nothing to do .... */
    while (last as libc::c_long) < n {
        yin = *y.offset(last as isize);
        yout = *y.offset((first - 1 as libc::c_int) as isize);
        if debug != 0 {
            REprintf(
                b" is=%d, y(in/out)= %10g, %10g\x00" as *const u8 as *const libc::c_char,
                ismo,
                yin,
                yout,
            );
        }
        rnew = rmed;
        if yin < rmed {
            if yout >= rmed {
                kminus = 0 as libc::c_int;
                if yout > rmed {
                    /* --- yin < rmed < yout --- */
                    if debug != 0 {
                        REprintf(b": yin < rmed < yout \x00" as *const u8 as *const libc::c_char);
                        /* was -rinf */
                    }
                    rnew = yin;
                    i = first;
                    while i <= last {
                        yi = *y.offset(i as isize);
                        if yi < rmed {
                            kminus += 1;
                            if yi > rnew {
                                rnew = yi
                            }
                        }
                        i += 1
                    }
                    if kminus < band2 {
                        rnew = rmed
                    }
                } else {
                    /*  --- yin < rmed = yout --- */
                    if debug != 0 {
                        REprintf(b": yin < rmed == yout \x00" as *const u8 as *const libc::c_char);
                        /* was -rinf */
                    }
                    rts = yin;
                    rse = rts;
                    i = first;
                    while i <= last {
                        yi = *y.offset(i as isize);
                        if yi <= rmed {
                            if yi < rmed {
                                kminus += 1;
                                if yi > rts {
                                    rts = yi
                                }
                                if yi > rse {
                                    rse = yi
                                }
                            } else {
                                rse = yi
                            }
                        }
                        i += 1
                    }
                    rnew = if kminus == band2 { rts } else { rse };
                    if debug != 0 {
                        REprintf(b"k- : %d,\x00" as *const u8 as *const libc::c_char, kminus);
                    }
                }
            }
        /* else: both  yin, yout < rmed -- nothing to do .... */
        } else if yin != rmed {
            /* yin > rmed */
            if yout <= rmed {
                kplus = 0 as libc::c_int; /* -- yout = rmed < yin --- */
                if yout < rmed {
                    /* -- yout < rmed < yin --- */
                    if debug != 0 {
                        REprintf(b": yout < rmed < yin \x00" as *const u8 as *const libc::c_char);
                        /* was rinf */
                    } /* was rinf */
                    rnew = yin;
                    i = first;
                    while i <= last {
                        yi = *y.offset(i as isize);
                        if yi > rmed {
                            kplus += 1;
                            if yi < rnew {
                                rnew = yi
                            }
                        }
                        i += 1
                    }
                    if kplus < band2 {
                        rnew = rmed
                    }
                } else {
                    if debug != 0 {
                        REprintf(b": yout == rmed < yin \x00" as *const u8 as *const libc::c_char);
                    }
                    rtb = yin;
                    rbe = rtb;
                    i = first;
                    while i <= last {
                        yi = *y.offset(i as isize);
                        if yi >= rmed {
                            if yi > rmed {
                                kplus += 1;
                                if yi < rtb {
                                    rtb = yi
                                }
                                if yi < rbe {
                                    rbe = yi
                                }
                            } else {
                                rbe = yi
                            }
                        }
                        i += 1
                    }
                    rnew = if kplus == band2 { rtb } else { rbe };
                    if debug != 0 {
                        REprintf(b"k+ : %d,\x00" as *const u8 as *const libc::c_char, kplus);
                    }
                }
            }
            /* else: both  yin, yout > rmed --> nothing to do */
        }
        if debug != 0 {
            REprintf(
                b"=> %12g, %12g\n\x00" as *const u8 as *const libc::c_char,
                rmed,
                rnew,
            );
        }
        rmed = rnew;
        *smo.offset(ismo as isize) = rmed;
        first += 1;
        last += 1;
        ismo += 1
    }
    if end_rule == 0 as libc::c_int {
        /*-- keep DATA at end values */
        i = ismo;
        while (i as libc::c_long) < n {
            *smo.offset(i as isize) = *y.offset(i as isize);
            i += 1
        }
    } else {
        /* if(end_rule == 1)  copy median to CONSTANT end values */
        i = ismo;
        while (i as libc::c_long) < n {
            *smo.offset(i as isize) = rmed;
            i += 1
        }
    };
}
/* Srunmed */
#[no_mangle]
pub unsafe extern "C" fn runmed(
    mut x: SEXP,
    mut stype: SEXP,
    mut sk: SEXP,
    mut end: SEXP,
    mut print_level: SEXP,
) -> SEXP {
    if TYPEOF(x) != 14 as libc::c_int {
        error(b"numeric \'x\' required\x00" as *const u8 as *const libc::c_char);
    }
    let mut n: R_xlen_t = XLENGTH(x);
    let mut type_0: libc::c_int = asInteger(stype);
    let mut k: libc::c_int = asInteger(sk);
    let mut iend: libc::c_int = asInteger(end);
    let mut pl: libc::c_int = asInteger(print_level);
    let mut ans: SEXP = protect(allocVector(14 as libc::c_int as SEXPTYPE, n));
    if type_0 == 1 as libc::c_int {
        if IS_LONG_VEC(x) != 0 {
            error(
                b"long vectors are not supported for algorithm = \"Turlach\"\x00" as *const u8
                    as *const libc::c_char,
            );
        }
        let mut i1: *mut libc::c_int = R_alloc(
            (k + 1 as libc::c_int) as size_t,
            ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_int;
        let mut i2: *mut libc::c_int = R_alloc(
            (2 as libc::c_int * k + 1 as libc::c_int) as size_t,
            ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_int;
        let mut d1: *mut libc::c_double = R_alloc(
            (2 as libc::c_int * k + 1 as libc::c_int) as size_t,
            ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_double;
        Trunmed(n, k, REAL(x), REAL(ans), i1, i2, d1, iend, pl);
    } else {
        Srunmed(
            REAL(x),
            REAL(ans),
            n,
            k,
            iend,
            (pl > 0 as libc::c_int) as libc::c_int,
        );
    }
    unprotect(1 as libc::c_int);
    return ans;
}
