use crate::sexprec::SEXP;
use ::libc;
extern "C" {
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn nrows(_: SEXP) -> libc::c_int;
}
pub type size_t = libc::c_ulong;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1997-2002   The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Double Centering for Classical Multidimensional Scaling */
/* NB: this does not duplicate A */
#[no_mangle]
pub unsafe extern "C" fn DoubleCentre(mut A: SEXP) -> SEXP {
    let mut n: libc::c_int = nrows(A); /* avoid integer overflow with long vectors */
    let mut a: *mut libc::c_double = REAL(A);
    let mut N: size_t = n as size_t;
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < n {
        let mut sum: libc::c_double = 0 as libc::c_int as libc::c_double;
        let mut j: libc::c_int = 0 as libc::c_int;
        while j < n {
            sum += *a.offset(
                (i as libc::c_ulong).wrapping_add((j as libc::c_ulong).wrapping_mul(N)) as isize,
            );
            j += 1
        }
        sum /= n as libc::c_double;
        let mut j_0: libc::c_int = 0 as libc::c_int;
        while j_0 < n {
            *a.offset(
                (i as libc::c_ulong).wrapping_add((j_0 as libc::c_ulong).wrapping_mul(N)) as isize,
            ) -= sum;
            j_0 += 1
        }
        i += 1
    }
    let mut j_1: libc::c_int = 0 as libc::c_int;
    while j_1 < n {
        let mut sum_0: libc::c_double = 0 as libc::c_int as libc::c_double;
        let mut i_0: libc::c_int = 0 as libc::c_int;
        while i_0 < n {
            sum_0 += *a.offset(
                (i_0 as libc::c_ulong).wrapping_add((j_1 as libc::c_ulong).wrapping_mul(N))
                    as isize,
            );
            i_0 += 1
        }
        sum_0 /= n as libc::c_double;
        let mut i_1: libc::c_int = 0 as libc::c_int;
        while i_1 < n {
            *a.offset(
                (i_1 as libc::c_ulong).wrapping_add((j_1 as libc::c_ulong).wrapping_mul(N))
                    as isize,
            ) -= sum_0;
            i_1 += 1
        }
        j_1 += 1
    }
    return A;
}
