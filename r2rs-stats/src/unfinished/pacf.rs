use crate::{
    sexprec::{SEXP, SEXPREC, SEXPTYPE},
    starma::forkal,
    starma::karma,
    starma::starma,
    starma::{starma_struct, Starma},
};
use ::libc;
extern "C" {
    #[no_mangle]
    fn R_chk_free(_: *mut libc::c_void);
    /* NA_REAL: IEEE */
    #[no_mangle]
    static mut R_NaInt: libc::c_int;
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn R_chk_calloc(_: size_t, _: size_t) -> *mut libc::c_void;
    #[no_mangle]
    fn TYPEOF(x: SEXP) -> libc::c_int;
    /* Vector Access Functions */
    #[no_mangle]
    fn LENGTH(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn INTEGER(x: SEXP) -> *mut libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    /* Current srcref, for debuggers */
    /* Special Values */
    #[no_mangle]
    static mut R_NilValue: SEXP;
    /* "dimnames" */
    #[no_mangle]
    static mut R_DimSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn allocMatrix(_: SEXPTYPE, _: libc::c_int, _: libc::c_int) -> SEXP;
    #[no_mangle]
    fn install(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn lengthgets(_: SEXP, _: R_len_t) -> SEXP;
    #[no_mangle]
    fn setAttrib(_: SEXP, _: SEXP, _: SEXP) -> SEXP;
    /* External pointer interface */
    #[no_mangle]
    fn R_MakeExternalPtr(p: *mut libc::c_void, tag: SEXP, prot: SEXP) -> SEXP;
    #[no_mangle]
    fn R_ExternalPtrAddr(s: SEXP) -> *mut libc::c_void;
    #[no_mangle]
    fn R_ExternalPtrTag(s: SEXP) -> SEXP;
    /* Defining NO_RINLINEDFUNS disables use to simulate platforms where
    this is not available */
    /* need remapped names here for use with R_NO_REMAP */
    /*
       These are the inlinable functions that are provided in Rinlinedfuns.h
       It is *essential* that these do not appear in any other header file,
       with or without the Rf_ prefix.
    */
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn ScalarReal(_: libc::c_double) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;

}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
/* type for length of (standard, not long) vectors etc */
pub type R_len_t = libc::c_int;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
/* external pointer */
/* weak reference */
/* raw bytes */
/* S4, non-vector */
/* used for detecting PROTECT issues in memory.c */
/* fresh node created in new page */
/* node released by GC */
/* Closure or Builtin or Special */
/* NOT YET */
/* These are also used with the write barrier on, in attrib.c and util.c */
/* cor is the autocorrelations starting from 0 lag*/
unsafe extern "C" fn uni_pacf(
    mut cor: *mut libc::c_double,
    mut p: *mut libc::c_double,
    mut nlag: libc::c_int,
) {
    let mut a: libc::c_double = 0.;
    let mut b: libc::c_double = 0.;
    let mut c: libc::c_double = 0.;
    let mut v: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut w: *mut libc::c_double = 0 as *mut libc::c_double;
    v = R_alloc(
        nlag as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    w = R_alloc(
        nlag as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    let ref mut fresh0 = *p.offset(0 as libc::c_int as isize);
    *fresh0 = *cor.offset(1 as libc::c_int as isize);
    *w.offset(0 as libc::c_int as isize) = *fresh0;
    let mut ll: libc::c_int = 1 as libc::c_int;
    while ll < nlag {
        a = *cor.offset((ll + 1 as libc::c_int) as isize);
        b = 1.0f64;
        let mut i: libc::c_int = 0 as libc::c_int;
        while i < ll {
            a -= *w.offset(i as isize) * *cor.offset((ll - i) as isize);
            b -= *w.offset(i as isize) * *cor.offset((i + 1 as libc::c_int) as isize);
            i += 1
        }
        c = a / b;
        *p.offset(ll as isize) = c;
        if ll + 1 as libc::c_int == nlag {
            break;
        }
        *w.offset(ll as isize) = c;
        let mut i_0: libc::c_int = 0 as libc::c_int;
        while i_0 < ll {
            *v.offset((ll - i_0 - 1 as libc::c_int) as isize) = *w.offset(i_0 as isize);
            i_0 += 1
        }
        let mut i_1: libc::c_int = 0 as libc::c_int;
        while i_1 < ll {
            *w.offset(i_1 as isize) -= c * *v.offset(i_1 as isize);
            i_1 += 1
        }
        ll += 1
    }
}
#[no_mangle]
pub unsafe extern "C" fn pacf1(mut acf: SEXP, mut lmax: SEXP) -> SEXP {
    let mut lagmax: libc::c_int = asInteger(lmax);
    acf = protect(coerceVector(acf, 14 as libc::c_int as SEXPTYPE));
    let mut ans: SEXP = protect(allocVector(
        14 as libc::c_int as SEXPTYPE,
        lagmax as R_xlen_t,
    ));
    uni_pacf(REAL(acf), REAL(ans), lagmax);
    let mut d: SEXP = protect(allocVector(
        13 as libc::c_int as SEXPTYPE,
        3 as libc::c_int as R_xlen_t,
    ));
    *INTEGER(d).offset(0 as libc::c_int as isize) = lagmax;
    let ref mut fresh1 = *INTEGER(d).offset(2 as libc::c_int as isize);
    *fresh1 = 1 as libc::c_int;
    *INTEGER(d).offset(1 as libc::c_int as isize) = *fresh1;
    setAttrib(ans, R_DimSymbol, d);
    unprotect(3 as libc::c_int);
    return ans;
}
/* Use an external reference to store the structure we keep allocated
memory in */
static mut Starma_tag: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
#[no_mangle]
pub unsafe extern "C" fn setup_starma(
    mut na: SEXP,
    mut x: SEXP,
    mut pn: SEXP,
    mut xreg: SEXP,
    mut pm: SEXP,
    mut dt: SEXP,
    mut ptrans: SEXP,
    mut sncond: SEXP,
) -> SEXP {
    let mut G: Starma = 0 as *mut starma_struct; /* AIX can't calloc 0 items */
    let mut i: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut m: libc::c_int = 0;
    let mut ip: libc::c_int = 0;
    let mut iq: libc::c_int = 0;
    let mut ir: libc::c_int = 0;
    let mut np: libc::c_int = 0;
    let mut res: SEXP = 0 as *mut SEXPREC;
    let mut rx: *mut libc::c_double = REAL(x);
    let mut rxreg: *mut libc::c_double = REAL(xreg);
    G = R_chk_calloc(
        1 as libc::c_int as size_t,
        ::std::mem::size_of::<starma_struct>() as libc::c_ulong,
    ) as *mut starma_struct;
    (*G).mp = *INTEGER(na).offset(0 as libc::c_int as isize);
    (*G).mq = *INTEGER(na).offset(1 as libc::c_int as isize);
    (*G).msp = *INTEGER(na).offset(2 as libc::c_int as isize);
    (*G).msq = *INTEGER(na).offset(3 as libc::c_int as isize);
    (*G).ns = *INTEGER(na).offset(4 as libc::c_int as isize);
    n = asInteger(pn);
    (*G).n = n;
    (*G).ncond = asInteger(sncond);
    m = asInteger(pm);
    (*G).m = m;
    (*G).params = R_chk_calloc(
        ((*G).mp + (*G).mq + (*G).msp + (*G).msq + (*G).m) as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    ip = (*G).ns * (*G).msp + (*G).mp;
    (*G).p = ip;
    iq = (*G).ns * (*G).msq + (*G).mq;
    (*G).q = iq;
    ir = if ip < iq + 1 as libc::c_int {
        (iq) + 1 as libc::c_int
    } else {
        ip
    };
    (*G).r = ir;
    np = ir * (ir + 1 as libc::c_int) / 2 as libc::c_int;
    (*G).np = np;
    (*G).nrbar = if (1 as libc::c_int) < np * (np - 1 as libc::c_int) / 2 as libc::c_int {
        (np * (np - 1 as libc::c_int)) / 2 as libc::c_int
    } else {
        1 as libc::c_int
    };
    (*G).trans = asInteger(ptrans);
    (*G).a = R_chk_calloc(
        ir as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).P = R_chk_calloc(
        np as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).V = R_chk_calloc(
        np as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).thetab = R_chk_calloc(
        np as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).xnext = R_chk_calloc(
        np as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).xrow = R_chk_calloc(
        np as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).rbar = R_chk_calloc(
        (*G).nrbar as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).w = R_chk_calloc(
        n as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).wkeep = R_chk_calloc(
        n as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).resid = R_chk_calloc(
        n as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).phi = R_chk_calloc(
        ir as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).theta = R_chk_calloc(
        ir as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).reg = R_chk_calloc(
        (1 as libc::c_int + n * m) as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    (*G).delta = asReal(dt);
    i = 0 as libc::c_int;
    while i < n {
        let ref mut fresh2 = *(*G).wkeep.offset(i as isize);
        *fresh2 = *rx.offset(i as isize);
        *(*G).w.offset(i as isize) = *fresh2;
        i += 1
    }
    i = 0 as libc::c_int;
    while i < n * m {
        *(*G).reg.offset(i as isize) = *rxreg.offset(i as isize);
        i += 1
    }
    Starma_tag = install(b"STARMA_TAG\x00" as *const u8 as *const libc::c_char);
    res = R_MakeExternalPtr(G as *mut libc::c_void, Starma_tag, R_NilValue);
    return res;
}
#[no_mangle]
pub unsafe extern "C" fn free_starma(mut pG: SEXP) -> SEXP {
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    R_chk_free((*G).params as *mut libc::c_void);
    (*G).params = 0 as *mut libc::c_double;
    R_chk_free((*G).a as *mut libc::c_void);
    (*G).a = 0 as *mut libc::c_double;
    R_chk_free((*G).P as *mut libc::c_void);
    (*G).P = 0 as *mut libc::c_double;
    R_chk_free((*G).V as *mut libc::c_void);
    (*G).V = 0 as *mut libc::c_double;
    R_chk_free((*G).thetab as *mut libc::c_void);
    (*G).thetab = 0 as *mut libc::c_double;
    R_chk_free((*G).xnext as *mut libc::c_void);
    (*G).xnext = 0 as *mut libc::c_double;
    R_chk_free((*G).xrow as *mut libc::c_void);
    (*G).xrow = 0 as *mut libc::c_double;
    R_chk_free((*G).rbar as *mut libc::c_void);
    (*G).rbar = 0 as *mut libc::c_double;
    R_chk_free((*G).w as *mut libc::c_void);
    (*G).w = 0 as *mut libc::c_double;
    R_chk_free((*G).wkeep as *mut libc::c_void);
    (*G).wkeep = 0 as *mut libc::c_double;
    R_chk_free((*G).resid as *mut libc::c_void);
    (*G).resid = 0 as *mut libc::c_double;
    R_chk_free((*G).phi as *mut libc::c_void);
    (*G).phi = 0 as *mut libc::c_double;
    R_chk_free((*G).theta as *mut libc::c_void);
    (*G).theta = 0 as *mut libc::c_double;
    R_chk_free((*G).reg as *mut libc::c_void);
    (*G).reg = 0 as *mut libc::c_double;
    R_chk_free(G as *mut libc::c_void);
    G = 0 as Starma;
    return R_NilValue;
}
#[no_mangle]
pub unsafe extern "C" fn Starma_method(mut pG: SEXP, mut method: SEXP) -> SEXP {
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    (*G).method = asInteger(method);
    return R_NilValue;
}
#[no_mangle]
pub unsafe extern "C" fn Dotrans(mut pG: SEXP, mut x: SEXP) -> SEXP {
    let mut y: SEXP = allocVector(14 as libc::c_int as SEXPTYPE, LENGTH(x) as R_xlen_t);
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    dotrans(G, REAL(x), REAL(y), 1 as libc::c_int);
    return y;
}
#[no_mangle]
pub unsafe extern "C" fn set_trans(mut pG: SEXP, mut ptrans: SEXP) -> SEXP {
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    (*G).trans = asInteger(ptrans);
    return R_NilValue;
}
#[no_mangle]
pub unsafe extern "C" fn arma0fa(mut pG: SEXP, mut inparams: SEXP) -> SEXP {
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut ifault: libc::c_int = 0 as libc::c_int;
    let mut it: libc::c_int = 0;
    let mut streg: libc::c_int = 0;
    let mut sumlog: libc::c_double = 0.;
    let mut ssq: libc::c_double = 0.;
    let mut tmp: libc::c_double = 0.;
    let mut ans: libc::c_double = 0.;
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    dotrans(G, REAL(inparams), (*G).params, (*G).trans);
    if (*G).ns > 0 as libc::c_int {
        /* expand out seasonal ARMA models */
        i = 0 as libc::c_int;
        while i < (*G).mp {
            *(*G).phi.offset(i as isize) = *(*G).params.offset(i as isize);
            i += 1
        }
        i = 0 as libc::c_int;
        while i < (*G).mq {
            *(*G).theta.offset(i as isize) = *(*G).params.offset((i + (*G).mp) as isize);
            i += 1
        }
        i = (*G).mp;
        while i < (*G).p {
            *(*G).phi.offset(i as isize) = 0.0f64;
            i += 1
        }
        i = (*G).mq;
        while i < (*G).q {
            *(*G).theta.offset(i as isize) = 0.0f64;
            i += 1
        }
        j = 0 as libc::c_int;
        while j < (*G).msp {
            *(*G)
                .phi
                .offset(((j + 1 as libc::c_int) * (*G).ns - 1 as libc::c_int) as isize) +=
                *(*G).params.offset((j + (*G).mp + (*G).mq) as isize);
            i = 0 as libc::c_int;
            while i < (*G).mp {
                *(*G)
                    .phi
                    .offset(((j + 1 as libc::c_int) * (*G).ns + i) as isize) -=
                    *(*G).params.offset(i as isize)
                        * *(*G).params.offset((j + (*G).mp + (*G).mq) as isize);
                i += 1
            }
            j += 1
        }
        j = 0 as libc::c_int;
        while j < (*G).msq {
            *(*G)
                .theta
                .offset(((j + 1 as libc::c_int) * (*G).ns - 1 as libc::c_int) as isize) += *(*G)
                .params
                .offset((j + (*G).mp + (*G).mq + (*G).msp) as isize);
            i = 0 as libc::c_int;
            while i < (*G).mq {
                *(*G)
                    .theta
                    .offset(((j + 1 as libc::c_int) * (*G).ns + i) as isize) +=
                    *(*G).params.offset((i + (*G).mp) as isize)
                        * *(*G)
                            .params
                            .offset((j + (*G).mp + (*G).mq + (*G).msp) as isize);
                i += 1
            }
            j += 1
        }
    } else {
        i = 0 as libc::c_int;
        while i < (*G).mp {
            *(*G).phi.offset(i as isize) = *(*G).params.offset(i as isize);
            i += 1
        }
        i = 0 as libc::c_int;
        while i < (*G).mq {
            *(*G).theta.offset(i as isize) = *(*G).params.offset((i + (*G).mp) as isize);
            i += 1
        }
    }
    streg = (*G).mp + (*G).mq + (*G).msp + (*G).msq;
    if (*G).m > 0 as libc::c_int {
        i = 0 as libc::c_int;
        while i < (*G).n {
            tmp = *(*G).wkeep.offset(i as isize);
            j = 0 as libc::c_int;
            while j < (*G).m {
                tmp -= *(*G).reg.offset((i + (*G).n * j) as isize)
                    * *(*G).params.offset((streg + j) as isize);
                j += 1
            }
            *(*G).w.offset(i as isize) = tmp;
            i += 1
        }
    }
    if (*G).method == 1 as libc::c_int {
        let mut p: libc::c_int = (*G).mp + (*G).ns * (*G).msp;
        let mut q: libc::c_int = (*G).mq + (*G).ns * (*G).msq;
        let mut nu: libc::c_int = 0 as libc::c_int;
        ssq = 0.0f64;
        i = 0 as libc::c_int;
        while i < (*G).ncond {
            *(*G).resid.offset(i as isize) = 0.0f64;
            i += 1
        }
        i = (*G).ncond;
        while i < (*G).n {
            tmp = *(*G).w.offset(i as isize);
            j = 0 as libc::c_int;
            while j
                < (if i - (*G).ncond > p {
                    p
                } else {
                    (i) - (*G).ncond
                })
            {
                tmp -= *(*G).phi.offset(j as isize)
                    * *(*G).w.offset((i - j - 1 as libc::c_int) as isize);
                j += 1
            }
            j = 0 as libc::c_int;
            while j
                < (if i - (*G).ncond > q {
                    q
                } else {
                    (i) - (*G).ncond
                })
            {
                tmp -= *(*G).theta.offset(j as isize)
                    * *(*G).resid.offset((i - j - 1 as libc::c_int) as isize);
                j += 1
            }
            *(*G).resid.offset(i as isize) = tmp;
            if !(tmp.is_nan() as i32 != 0 as libc::c_int) {
                nu += 1;
                ssq += tmp * tmp
            }
            i += 1
        }
        (*G).s2 = ssq / nu as libc::c_double;
        ans = 0.5f64 * ((*G).s2).ln()
    } else {
        starma(G, &mut ifault);
        if ifault != 0 {
            error(
                dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"starma error code %d\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ),
                ifault,
            );
        }
        sumlog = 0.0f64;
        ssq = 0.0f64;
        it = 0 as libc::c_int;
        karma(G, &mut sumlog, &mut ssq, 1 as libc::c_int, &mut it);
        (*G).s2 = ssq / (*G).nused as libc::c_double;
        ans = 0.5f64
            * ((ssq / (*G).nused as libc::c_double).ln() + sumlog / (*G).nused as libc::c_double)
    }
    return ScalarReal(ans);
}
#[no_mangle]
pub unsafe extern "C" fn get_s2(mut pG: SEXP) -> SEXP {
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    return ScalarReal((*G).s2);
}
#[no_mangle]
pub unsafe extern "C" fn get_resid(mut pG: SEXP) -> SEXP {
    let mut res: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut rres: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    res = allocVector(14 as libc::c_int as SEXPTYPE, (*G).n as R_xlen_t);
    rres = REAL(res);
    i = 0 as libc::c_int;
    while i < (*G).n {
        *rres.offset(i as isize) = *(*G).resid.offset(i as isize);
        i += 1
    }
    return res;
}
#[no_mangle]
pub unsafe extern "C" fn arma0_kfore(
    mut pG: SEXP,
    mut pd: SEXP,
    mut psd: SEXP,
    mut nahead: SEXP,
) -> SEXP {
    let mut dd: libc::c_int = asInteger(pd);
    let mut d: libc::c_int = 0;
    let mut il: libc::c_int = asInteger(nahead);
    let mut ifault: libc::c_int = 0 as libc::c_int;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut del: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut del2: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut res: SEXP = 0 as *mut SEXPREC;
    let mut x: SEXP = 0 as *mut SEXPREC;
    let mut var: SEXP = 0 as *mut SEXPREC;
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    res = allocVector(19 as libc::c_int as SEXPTYPE, 2 as libc::c_int as R_xlen_t);
    protect(res);
    x = allocVector(14 as libc::c_int as SEXPTYPE, il as R_xlen_t);
    SET_VECTOR_ELT(res, 0 as libc::c_int as R_xlen_t, x);
    var = allocVector(14 as libc::c_int as SEXPTYPE, il as R_xlen_t);
    SET_VECTOR_ELT(res, 1 as libc::c_int as R_xlen_t, var);
    d = dd + (*G).ns * asInteger(psd);
    del = R_alloc(
        (d + 1 as libc::c_int) as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    del2 = R_alloc(
        (d + 1 as libc::c_int) as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    *del.offset(0 as libc::c_int as isize) = 1 as libc::c_int as libc::c_double;
    i = 1 as libc::c_int;
    while i <= d {
        *del.offset(i as isize) = 0 as libc::c_int as libc::c_double;
        i += 1
    }
    j = 0 as libc::c_int;
    while j < dd {
        i = 0 as libc::c_int;
        while i <= d {
            *del2.offset(i as isize) = *del.offset(i as isize);
            i += 1
        }
        i = 0 as libc::c_int;
        while i <= d - 1 as libc::c_int {
            *del.offset((i + 1 as libc::c_int) as isize) -= *del2.offset(i as isize);
            i += 1
        }
        j += 1
    }
    j = 0 as libc::c_int;
    while j < asInteger(psd) {
        i = 0 as libc::c_int;
        while i <= d {
            *del2.offset(i as isize) = *del.offset(i as isize);
            i += 1
        }
        i = 0 as libc::c_int;
        while i <= d - (*G).ns {
            *del.offset((i + (*G).ns) as isize) -= *del2.offset(i as isize);
            i += 1
        }
        j += 1
    }
    i = 1 as libc::c_int;
    while i <= d {
        *del.offset(i as isize) *= -(1 as libc::c_int) as libc::c_double;
        i += 1
    }
    forkal(
        G,
        d,
        il,
        del.offset(1 as libc::c_int as isize),
        REAL(x),
        REAL(var),
        &mut ifault,
    );
    if ifault != 0 {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"forkal error code %d\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            ifault,
        );
    }
    unprotect(1 as libc::c_int);
    return res;
}
unsafe extern "C" fn artoma(
    mut p: libc::c_int,
    mut phi: *mut libc::c_double,
    mut psi: *mut libc::c_double,
    mut npsi: libc::c_int,
) {
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    i = 0 as libc::c_int;
    while i < p {
        *psi.offset(i as isize) = *phi.offset(i as isize);
        i += 1
    }
    i = p;
    while i < npsi {
        *psi.offset(i as isize) = 0.0f64;
        i += 1
    }
    i = 0 as libc::c_int;
    while i < npsi - p - 1 as libc::c_int {
        j = 0 as libc::c_int;
        while j < p {
            *psi.offset((i + j + 1 as libc::c_int) as isize) +=
                *phi.offset(j as isize) * *psi.offset(i as isize);
            j += 1
        }
        i += 1
    }
}
#[no_mangle]
pub unsafe extern "C" fn ar2ma(mut ar: SEXP, mut npsi: SEXP) -> SEXP {
    ar = protect(coerceVector(ar, 14 as libc::c_int as SEXPTYPE));
    let mut p: libc::c_int = LENGTH(ar);
    let mut ns: libc::c_int = asInteger(npsi);
    let mut ns1: libc::c_int = ns + p + 1 as libc::c_int;
    let mut psi: SEXP = protect(allocVector(14 as libc::c_int as SEXPTYPE, ns1 as R_xlen_t));
    artoma(p, REAL(ar), REAL(psi), ns1);
    let mut ans: SEXP = lengthgets(psi, ns);
    unprotect(2 as libc::c_int);
    return ans;
}
/*  R : A Computer Language for Statistical Data Analysis
 *
 *  Copyright (C) 1999-2016 The R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/.
 */
/* Internal */
unsafe extern "C" fn partrans(
    mut p: libc::c_int,
    mut raw: *mut libc::c_double,
    mut new: *mut libc::c_double,
) {
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut a: libc::c_double = 0.;
    let mut work: [libc::c_double; 100] = [0.; 100];
    if p > 100 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"can only transform 100 pars in arima0\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    /* Step one: map (-Inf, Inf) to (-1, 1) via tanh
    The parameters are now the pacf phi_{kk} */
    j = 0 as libc::c_int;
    while j < p {
        let ref mut fresh3 = *new.offset(j as isize);
        *fresh3 = (*raw.offset(j as isize)).tanh();
        work[j as usize] = *fresh3;
        j += 1
    }
    /* Step two: run the Durbin-Levinson recursions to find phi_{j.},
    j = 2, ..., p and phi_{p.} are the autoregression coefficients */
    j = 1 as libc::c_int;
    while j < p {
        a = *new.offset(j as isize);
        k = 0 as libc::c_int;
        while k < j {
            work[k as usize] -= a * *new.offset((j - k - 1 as libc::c_int) as isize);
            k += 1
        }
        k = 0 as libc::c_int;
        while k < j {
            *new.offset(k as isize) = work[k as usize];
            k += 1
        }
        j += 1
    }
}
unsafe extern "C" fn dotrans(
    mut G: Starma,
    mut raw: *mut libc::c_double,
    mut new: *mut libc::c_double,
    mut trans: libc::c_int,
) {
    let mut i: libc::c_int = 0;
    let mut v: libc::c_int = 0;
    let mut n: libc::c_int = (*G).mp + (*G).mq + (*G).msp + (*G).msq + (*G).m;
    i = 0 as libc::c_int;
    while i < n {
        *new.offset(i as isize) = *raw.offset(i as isize);
        i += 1
    }
    if trans != 0 {
        partrans((*G).mp, raw, new);
        v = (*G).mp;
        partrans((*G).mq, raw.offset(v as isize), new.offset(v as isize));
        v += (*G).mq;
        partrans((*G).msp, raw.offset(v as isize), new.offset(v as isize));
        v += (*G).msp;
        partrans((*G).msq, raw.offset(v as isize), new.offset(v as isize));
    };
}
unsafe extern "C" fn invpartrans(
    mut p: libc::c_int,
    mut phi: *mut libc::c_double,
    mut new: *mut libc::c_double,
) {
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut a: libc::c_double = 0.;
    let mut work: [libc::c_double; 100] = [0.; 100];
    if p > 100 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"can only transform 100 pars in arima0\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    j = 0 as libc::c_int;
    while j < p {
        let ref mut fresh4 = *new.offset(j as isize);
        *fresh4 = *phi.offset(j as isize);
        work[j as usize] = *fresh4;
        j += 1
    }
    /* Run the Durbin-Levinson recursions backwards
    to find the PACF phi_{j.} from the autoregression coefficients */
    j = p - 1 as libc::c_int;
    while j > 0 as libc::c_int {
        a = *new.offset(j as isize);
        k = 0 as libc::c_int;
        while k < j {
            work[k as usize] = (*new.offset(k as isize)
                + a * *new.offset((j - k - 1 as libc::c_int) as isize))
                / (1 as libc::c_int as libc::c_double - a * a);
            k += 1
        }
        k = 0 as libc::c_int;
        while k < j {
            *new.offset(k as isize) = work[k as usize];
            k += 1
        }
        j -= 1
    }
    j = 0 as libc::c_int;
    while j < p {
        *new.offset(j as isize) = (*new.offset(j as isize)).atanh();
        j += 1
    }
}
#[no_mangle]
pub unsafe extern "C" fn Invtrans(mut pG: SEXP, mut x: SEXP) -> SEXP {
    let mut y: SEXP = allocVector(14 as libc::c_int as SEXPTYPE, LENGTH(x) as R_xlen_t);
    let mut i: libc::c_int = 0;
    let mut v: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut raw: *mut libc::c_double = REAL(x);
    let mut new: *mut libc::c_double = REAL(y);
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    n = (*G).mp + (*G).mq + (*G).msp + (*G).msq;
    v = 0 as libc::c_int;
    invpartrans((*G).mp, raw.offset(v as isize), new.offset(v as isize));
    v += (*G).mp;
    invpartrans((*G).mq, raw.offset(v as isize), new.offset(v as isize));
    v += (*G).mq;
    invpartrans((*G).msp, raw.offset(v as isize), new.offset(v as isize));
    v += (*G).msp;
    invpartrans((*G).msq, raw.offset(v as isize), new.offset(v as isize));
    i = n;
    while i < n + (*G).m {
        *new.offset(i as isize) = *raw.offset(i as isize);
        i += 1
    }
    return y;
}
#[no_mangle]
pub unsafe extern "C" fn Gradtrans(mut pG: SEXP, mut x: SEXP) -> SEXP {
    let mut y: SEXP = allocMatrix(14 as libc::c_int as SEXPTYPE, LENGTH(x), LENGTH(x));
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut v: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut raw: *mut libc::c_double = REAL(x);
    let mut A: *mut libc::c_double = REAL(y);
    let mut w1: [libc::c_double; 100] = [0.; 100];
    let mut w2: [libc::c_double; 100] = [0.; 100];
    let mut w3: [libc::c_double; 100] = [0.; 100];
    let mut G: Starma = 0 as *mut starma_struct;
    if TYPEOF(pG) != 22 as libc::c_int || R_ExternalPtrTag(pG) != Starma_tag {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"bad Starma struct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    G = R_ExternalPtrAddr(pG) as Starma;
    n = (*G).mp + (*G).mq + (*G).msp + (*G).msq + (*G).m;
    i = 0 as libc::c_int;
    while i < n {
        j = 0 as libc::c_int;
        while j < n {
            *A.offset((i + j * n) as isize) = (i == j) as libc::c_int as libc::c_double;
            j += 1
        }
        i += 1
    }
    if (*G).mp > 0 as libc::c_int {
        i = 0 as libc::c_int;
        while i < (*G).mp {
            w1[i as usize] = *raw.offset(i as isize);
            i += 1
        }
        partrans((*G).mp, w1.as_mut_ptr(), w2.as_mut_ptr());
        i = 0 as libc::c_int;
        while i < (*G).mp {
            w1[i as usize] += 1e-3f64;
            partrans((*G).mp, w1.as_mut_ptr(), w3.as_mut_ptr());
            j = 0 as libc::c_int;
            while j < (*G).mp {
                *A.offset((i + j * n) as isize) = (w3[j as usize] - w2[j as usize]) / 1e-3f64;
                j += 1
            }
            w1[i as usize] -= 1e-3f64;
            i += 1
        }
    }
    if (*G).mq > 0 as libc::c_int {
        v = (*G).mp;
        i = 0 as libc::c_int;
        while i < (*G).mq {
            w1[i as usize] = *raw.offset((i + v) as isize);
            i += 1
        }
        partrans((*G).mq, w1.as_mut_ptr(), w2.as_mut_ptr());
        i = 0 as libc::c_int;
        while i < (*G).mq {
            w1[i as usize] += 1e-3f64;
            partrans((*G).mq, w1.as_mut_ptr(), w3.as_mut_ptr());
            j = 0 as libc::c_int;
            while j < (*G).mq {
                *A.offset((i + v + j * n) as isize) = (w3[j as usize] - w2[j as usize]) / 1e-3f64;
                j += 1
            }
            w1[i as usize] -= 1e-3f64;
            i += 1
        }
    }
    if (*G).msp > 0 as libc::c_int {
        v = (*G).mp + (*G).mq;
        i = 0 as libc::c_int;
        while i < (*G).msp {
            w1[i as usize] = *raw.offset((i + v) as isize);
            i += 1
        }
        partrans((*G).msp, w1.as_mut_ptr(), w2.as_mut_ptr());
        i = 0 as libc::c_int;
        while i < (*G).msp {
            w1[i as usize] += 1e-3f64;
            partrans((*G).msp, w1.as_mut_ptr(), w3.as_mut_ptr());
            j = 0 as libc::c_int;
            while j < (*G).msp {
                *A.offset((i + v + (j + v) * n) as isize) =
                    (w3[j as usize] - w2[j as usize]) / 1e-3f64;
                j += 1
            }
            w1[i as usize] -= 1e-3f64;
            i += 1
        }
    }
    if (*G).msq > 0 as libc::c_int {
        v = (*G).mp + (*G).mq + (*G).msp;
        i = 0 as libc::c_int;
        while i < (*G).msq {
            w1[i as usize] = *raw.offset((i + v) as isize);
            i += 1
        }
        partrans((*G).msq, w1.as_mut_ptr(), w2.as_mut_ptr());
        i = 0 as libc::c_int;
        while i < (*G).msq {
            w1[i as usize] += 1e-3f64;
            partrans((*G).msq, w1.as_mut_ptr(), w3.as_mut_ptr());
            j = 0 as libc::c_int;
            while j < (*G).msq {
                *A.offset((i + v + (j + v) * n) as isize) =
                    (w3[j as usize] - w2[j as usize]) / 1e-3f64;
                j += 1
            }
            w1[i as usize] -= 1e-3f64;
            i += 1
        }
    }
    return y;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2001-2017 The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
#[no_mangle]
pub unsafe extern "C" fn ARMAtoMA(mut ar: SEXP, mut ma: SEXP, mut lag_max: SEXP) -> SEXP {
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut p: libc::c_int = LENGTH(ar);
    let mut q: libc::c_int = LENGTH(ma);
    let mut m: libc::c_int = asInteger(lag_max);
    let mut phi: *mut libc::c_double = REAL(ar);
    let mut theta: *mut libc::c_double = REAL(ma);
    let mut psi: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut tmp: libc::c_double = 0.;
    let mut res: SEXP = 0 as *mut SEXPREC;
    if m <= 0 as libc::c_int || m == R_NaInt {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid value of lag.max\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    res = allocVector(14 as libc::c_int as SEXPTYPE, m as R_xlen_t);
    protect(res);
    psi = REAL(res);
    i = 0 as libc::c_int;
    while i < m {
        tmp = if i < q {
            *theta.offset(i as isize)
        } else {
            0.0f64
        };
        j = 0 as libc::c_int;
        while j
            < (if i + 1 as libc::c_int > p {
                p
            } else {
                (i) + 1 as libc::c_int
            })
        {
            tmp += *phi.offset(j as isize)
                * (if i - j - 1 as libc::c_int >= 0 as libc::c_int {
                    *psi.offset((i - j - 1 as libc::c_int) as isize)
                } else {
                    1.0f64
                });
            j += 1
        }
        *psi.offset(i as isize) = tmp;
        i += 1
    }
    unprotect(1 as libc::c_int);
    return res;
}
