use ::libc;
extern "C" {

    #[no_mangle]
    fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    #[no_mangle]
    fn printf(_: *const libc::c_char, _: ...) -> libc::c_int;
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2016  The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     *
     *
     * Memory Allocation (garbage collected) --- INCLUDING S compatibility ---
     */
    /* Included by R.h: API */
    /* for size_t */
    #[no_mangle]
    fn vmaxget() -> *mut libc::c_void;
    #[no_mangle]
    fn vmaxset(_: *const libc::c_void);
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn dqrcf_(
        x: *mut libc::c_double,
        n: *mut libc::c_int,
        k: *mut libc::c_int,
        qraux: *mut libc::c_double,
        y: *mut libc::c_double,
        ny: *mut libc::c_int,
        b: *mut libc::c_double,
        info: *mut libc::c_int,
    );
    /* find qr decomposition, dqrdc2() is basis of R's qr(),
    also used by nlme and many other packages. */
    #[no_mangle]
    fn dqrdc2_(
        x: *mut libc::c_double,
        ldx: *mut libc::c_int,
        n: *mut libc::c_int,
        p: *mut libc::c_int,
        tol: *mut libc::c_double,
        rank: *mut libc::c_int,
        qraux: *mut libc::c_double,
        pivot: *mut libc::c_int,
        work: *mut libc::c_double,
    );
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
}
pub type size_t = libc::c_ulong;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type C2RustUnnamed = libc::c_uint;
pub const TRUE: C2RustUnnamed = 1;
pub const FALSE: C2RustUnnamed = 0;
pub type Array = array;
#[derive(Copy, Clone)]
#[repr(C)]
pub struct array {
    pub vec: *mut libc::c_double,
    pub mat: *mut *mut libc::c_double,
    pub arr3: *mut *mut *mut libc::c_double,
    pub arr4: *mut *mut *mut *mut libc::c_double,
    pub dim: [libc::c_int; 4],
    pub ndim: libc::c_int,
}
/*, MAYBE */
/* Functions for dynamically allocating arrays

   The Array structure contains pointers to arrays which are allocated
   using the R_alloc function. Although the .C() interface cleans up
   all memory assigned with R_alloc, judicious use of vmaxget() vmaxset()
   to free this memory is probably wise. See memory.c in R core.

*/
unsafe extern "C" fn assert(mut bool: libc::c_int) {
    if bool == 0 {
        error(
            b"assert failed in src/library/ts/src/carray.c\x00" as *const u8 as *const libc::c_char,
        );
    };
}
unsafe extern "C" fn init_array() -> Array {
    let mut i: libc::c_int = 0;
    let mut a: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    /* Initialize everything to zero.  Useful for debugging */
    a.vec = 0 as *mut libc::c_double;
    a.mat = 0 as *mut *mut libc::c_double;
    a.arr3 = 0 as *mut *mut *mut libc::c_double;
    a.arr4 = 0 as *mut *mut *mut *mut libc::c_double;
    i = 0 as libc::c_int;
    while i < 4 as libc::c_int {
        a.dim[i as usize] = 0 as libc::c_int;
        i += 1
    }
    a.ndim = 0 as libc::c_int;
    return a;
}
unsafe extern "C" fn vector_length(mut a: Array) -> libc::c_int {
    let mut i: libc::c_int = 0;
    let mut len: libc::c_int = 0;
    i = 0 as libc::c_int;
    len = 1 as libc::c_int;
    while i < a.ndim {
        len *= a.dim[i as usize];
        i += 1
    }
    return len;
}
unsafe extern "C" fn make_array(
    mut vec: *mut libc::c_double,
    mut dim: *mut libc::c_int,
    mut ndim: libc::c_int,
) -> Array {
    let mut d: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut len: [libc::c_int; 5] = [0; 5];
    let mut a: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    assert((ndim <= 4 as libc::c_int) as libc::c_int);
    a = init_array();
    len[ndim as usize] = 1 as libc::c_int;
    d = ndim;
    while d >= 1 as libc::c_int {
        len[(d - 1 as libc::c_int) as usize] = len[d as usize] * *dim.offset((ndim - d) as isize);
        d -= 1
    }
    d = 1 as libc::c_int;
    while d <= ndim {
        match d {
            1 => a.vec = vec,
            2 => {
                a.mat = R_alloc(
                    len[(2 as libc::c_int - 1 as libc::c_int) as usize] as size_t,
                    ::std::mem::size_of::<*mut libc::c_double>() as libc::c_ulong as libc::c_int,
                ) as *mut *mut libc::c_double;
                i = 0 as libc::c_int;
                j = 0 as libc::c_int;
                while i < len[(2 as libc::c_int - 1 as libc::c_int) as usize] {
                    let ref mut fresh0 = *a.mat.offset(i as isize);
                    *fresh0 = a.vec.offset(j as isize);
                    i += 1;
                    j += *dim.offset((ndim - 2 as libc::c_int + 1 as libc::c_int) as isize)
                }
            }
            3 => {
                a.arr3 = R_alloc(
                    len[(3 as libc::c_int - 1 as libc::c_int) as usize] as size_t,
                    ::std::mem::size_of::<*mut *mut libc::c_double>() as libc::c_ulong
                        as libc::c_int,
                ) as *mut *mut *mut libc::c_double;
                i = 0 as libc::c_int;
                j = 0 as libc::c_int;
                while i < len[(3 as libc::c_int - 1 as libc::c_int) as usize] {
                    let ref mut fresh1 = *a.arr3.offset(i as isize);
                    *fresh1 = a.mat.offset(j as isize);
                    i += 1;
                    j += *dim.offset((ndim - 3 as libc::c_int + 1 as libc::c_int) as isize)
                }
            }
            4 => {
                a.arr4 = R_alloc(
                    len[(4 as libc::c_int - 1 as libc::c_int) as usize] as size_t,
                    ::std::mem::size_of::<*mut *mut *mut libc::c_double>() as libc::c_ulong
                        as libc::c_int,
                ) as *mut *mut *mut *mut libc::c_double;
                i = 0 as libc::c_int;
                j = 0 as libc::c_int;
                while i < len[(4 as libc::c_int - 1 as libc::c_int) as usize] {
                    let ref mut fresh2 = *a.arr4.offset(i as isize);
                    *fresh2 = a.arr3.offset(j as isize);
                    i += 1;
                    j += *dim.offset((ndim - 4 as libc::c_int + 1 as libc::c_int) as isize)
                }
            }
            _ => {}
        }
        d += 1
    }
    i = 0 as libc::c_int;
    while i < ndim {
        a.dim[i as usize] = *dim.offset(i as isize);
        i += 1
    }
    a.ndim = ndim;
    return a;
}
unsafe extern "C" fn make_zero_array(mut dim: *mut libc::c_int, mut ndim: libc::c_int) -> Array {
    let mut i: libc::c_int = 0;
    let mut len: libc::c_int = 0;
    let mut vec: *mut libc::c_double = 0 as *mut libc::c_double;
    i = 0 as libc::c_int;
    len = 1 as libc::c_int;
    while i < ndim {
        len *= *dim.offset(i as isize);
        i += 1
    }
    vec = R_alloc(
        len as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    i = 0 as libc::c_int;
    while i < len {
        *vec.offset(i as isize) = 0.0f64;
        i += 1
    }
    return make_array(vec, dim, ndim);
}
unsafe extern "C" fn make_matrix(
    mut vec: *mut libc::c_double,
    mut nrow: libc::c_int,
    mut ncol: libc::c_int,
) -> Array {
    let mut dim: [libc::c_int; 2] = [0; 2];
    dim[0 as libc::c_int as usize] = nrow;
    dim[1 as libc::c_int as usize] = ncol;
    return make_array(vec, dim.as_mut_ptr(), 2 as libc::c_int);
}
unsafe extern "C" fn make_zero_matrix(mut nrow: libc::c_int, mut ncol: libc::c_int) -> Array {
    let mut dim: [libc::c_int; 2] = [0; 2];
    let mut a: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    dim[0 as libc::c_int as usize] = nrow;
    dim[1 as libc::c_int as usize] = ncol;
    a = make_zero_array(dim.as_mut_ptr(), 2 as libc::c_int);
    return a;
}
unsafe extern "C" fn subarray(mut a: Array, mut index: libc::c_int) -> Array
/* Return subarray of array a in the form of an Array
   structure so it can be manipulated by other functions
   NB The data are not copied, so any changes made to the
      subarray will affect the original array.
*/ {
    let mut i: libc::c_int = 0;
    let mut offset: libc::c_int = 0;
    let mut b: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    b = init_array();
    /* is index in range? */
    assert((index >= 0 as libc::c_int && index < a.dim[0 as libc::c_int as usize]) as libc::c_int);
    offset = index;
    let mut current_block_8: u64;
    match a.ndim {
        4 => {
            /* NB Falling through here */
            offset *= a.dim[(a.ndim - 4 as libc::c_int + 1 as libc::c_int) as usize];
            b.arr3 = a.arr3.offset(offset as isize);
            current_block_8 = 18185463511693606631;
        }
        3 => {
            current_block_8 = 18185463511693606631;
        }
        2 => {
            current_block_8 = 13374446449343346977;
        }
        _ => {
            current_block_8 = 2968425633554183086;
        }
    }
    match current_block_8 {
        18185463511693606631 => {
            offset *= a.dim[(a.ndim - 3 as libc::c_int + 1 as libc::c_int) as usize];
            b.mat = a.mat.offset(offset as isize);
            current_block_8 = 13374446449343346977;
        }
        _ => {}
    }
    match current_block_8 {
        13374446449343346977 => {
            offset *= a.dim[(a.ndim - 2 as libc::c_int + 1 as libc::c_int) as usize];
            b.vec = a.vec.offset(offset as isize)
        }
        _ => {}
    }
    b.ndim = a.ndim - 1 as libc::c_int;
    i = 0 as libc::c_int;
    while i < b.ndim {
        b.dim[i as usize] = a.dim[(i + 1 as libc::c_int) as usize];
        i += 1
    }
    return b;
}
unsafe extern "C" fn test_array_conform(mut a1: Array, mut a2: Array) -> libc::c_int {
    let mut i: libc::c_int = 0;
    let mut ans: libc::c_int = FALSE as libc::c_int;
    if a1.ndim != a2.ndim {
        return FALSE as libc::c_int;
    } else {
        i = 0 as libc::c_int;
        while i < a1.ndim {
            if a1.dim[i as usize] == a2.dim[i as usize] {
                ans = TRUE as libc::c_int
            } else {
                return FALSE as libc::c_int;
            }
            i += 1
        }
    }
    return ans;
}
unsafe extern "C" fn copy_array(mut orig: Array, mut ans: Array)
/* copy matrix orig to ans */
{
    let mut i: libc::c_int = 0;
    assert(test_array_conform(orig, ans));
    i = 0 as libc::c_int;
    while i < vector_length(orig) {
        *ans.vec.offset(i as isize) = *orig.vec.offset(i as isize);
        i += 1
    }
}
unsafe extern "C" fn transpose_matrix(mut mat: Array, mut ans: Array) {
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut vmax: *const libc::c_void = 0 as *const libc::c_void;
    let mut tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    tmp = init_array();
    assert((mat.ndim == 2 as libc::c_int && ans.ndim == 2 as libc::c_int) as libc::c_int);
    assert(
        (mat.dim[1 as libc::c_int as usize] == ans.dim[0 as libc::c_int as usize]) as libc::c_int,
    );
    assert(
        (mat.dim[0 as libc::c_int as usize] == ans.dim[1 as libc::c_int as usize]) as libc::c_int,
    );
    vmax = vmaxget();
    tmp = make_zero_matrix(
        ans.dim[0 as libc::c_int as usize],
        ans.dim[1 as libc::c_int as usize],
    );
    i = 0 as libc::c_int;
    while i < mat.dim[0 as libc::c_int as usize] {
        j = 0 as libc::c_int;
        while j < mat.dim[1 as libc::c_int as usize] {
            *(*tmp.mat.offset(j as isize)).offset(i as isize) =
                *(*mat.mat.offset(i as isize)).offset(j as isize);
            j += 1
        }
        i += 1
    }
    copy_array(tmp, ans);
    vmaxset(vmax);
}
unsafe extern "C" fn array_op(
    mut arr1: Array,
    mut arr2: Array,
    mut op: libc::c_char,
    mut ans: Array,
)
/* Element-wise array operations */
{
    let mut i: libc::c_int = 0;
    assert(test_array_conform(arr1, arr2));
    assert(test_array_conform(arr2, ans));
    match op as libc::c_int {
        42 => {
            i = 0 as libc::c_int;
            while i < vector_length(ans) {
                *ans.vec.offset(i as isize) =
                    *arr1.vec.offset(i as isize) * *arr2.vec.offset(i as isize);
                i += 1
            }
        }
        43 => {
            i = 0 as libc::c_int;
            while i < vector_length(ans) {
                *ans.vec.offset(i as isize) =
                    *arr1.vec.offset(i as isize) + *arr2.vec.offset(i as isize);
                i += 1
            }
        }
        47 => {
            i = 0 as libc::c_int;
            while i < vector_length(ans) {
                *ans.vec.offset(i as isize) =
                    *arr1.vec.offset(i as isize) / *arr2.vec.offset(i as isize);
                i += 1
            }
        }
        45 => {
            i = 0 as libc::c_int;
            while i < vector_length(ans) {
                *ans.vec.offset(i as isize) =
                    *arr1.vec.offset(i as isize) - *arr2.vec.offset(i as isize);
                i += 1
            }
        }
        _ => {
            printf(b"Unknown op in array_op\x00" as *const u8 as *const libc::c_char);
        }
    };
}
unsafe extern "C" fn scalar_op(
    mut arr: Array,
    mut s: libc::c_double,
    mut op: libc::c_char,
    mut ans: Array,
)
/* Elementwise scalar operations */
{
    let mut i: libc::c_int = 0;
    assert(test_array_conform(arr, ans));
    match op as libc::c_int {
        42 => {
            i = 0 as libc::c_int;
            while i < vector_length(ans) {
                *ans.vec.offset(i as isize) = *arr.vec.offset(i as isize) * s;
                i += 1
            }
        }
        43 => {
            i = 0 as libc::c_int;
            while i < vector_length(ans) {
                *ans.vec.offset(i as isize) = *arr.vec.offset(i as isize) + s;
                i += 1
            }
        }
        47 => {
            i = 0 as libc::c_int;
            while i < vector_length(ans) {
                *ans.vec.offset(i as isize) = *arr.vec.offset(i as isize) / s;
                i += 1
            }
        }
        45 => {
            i = 0 as libc::c_int;
            while i < vector_length(ans) {
                *ans.vec.offset(i as isize) = *arr.vec.offset(i as isize) - s;
                i += 1
            }
        }
        _ => {
            printf(b"Unknown op in array_op\x00" as *const u8 as *const libc::c_char);
        }
    };
}
unsafe extern "C" fn matrix_prod(
    mut mat1: Array,
    mut mat2: Array,
    mut trans1: libc::c_int,
    mut trans2: libc::c_int,
    mut ans: Array,
)
/*
    General matrix product between mat1 and mat2. Put answer in ans.
    trans1 and trans2 are logical flags which indicate if the matrix is
    to be transposed. Normal matrix multiplication has trans1 = trans2 = 0.
*/
{
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut K1: libc::c_int = 0;
    let mut K2: libc::c_int = 0;
    let mut vmax: *const libc::c_void = 0 as *const libc::c_void;
    let mut m1: libc::c_double = 0.;
    let mut m2: libc::c_double = 0.;
    let mut tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    /* Test whether everything is a matrix */
    assert(
        (mat1.ndim == 2 as libc::c_int
            && mat2.ndim == 2 as libc::c_int
            && ans.ndim == 2 as libc::c_int) as libc::c_int,
    );
    /* Test whether matrices conform. K is the dimension that is
    lost by multiplication */
    if trans1 != 0 {
        assert(
            (mat1.dim[1 as libc::c_int as usize] == ans.dim[0 as libc::c_int as usize])
                as libc::c_int,
        );
        K1 = mat1.dim[0 as libc::c_int as usize]
    } else {
        assert(
            (mat1.dim[0 as libc::c_int as usize] == ans.dim[0 as libc::c_int as usize])
                as libc::c_int,
        );
        K1 = mat1.dim[1 as libc::c_int as usize]
    }
    if trans2 != 0 {
        assert(
            (mat2.dim[0 as libc::c_int as usize] == ans.dim[1 as libc::c_int as usize])
                as libc::c_int,
        );
        K2 = mat2.dim[1 as libc::c_int as usize]
    } else {
        assert(
            (mat2.dim[1 as libc::c_int as usize] == ans.dim[1 as libc::c_int as usize])
                as libc::c_int,
        );
        K2 = mat2.dim[0 as libc::c_int as usize]
    }
    assert((K1 == K2) as libc::c_int);
    tmp = init_array();
    /* In case ans is the same as mat1 or mat2, we create a temporary
       matrix to hold the answer, then copy it to ans
    */
    vmax = vmaxget();
    tmp = make_zero_matrix(
        ans.dim[0 as libc::c_int as usize],
        ans.dim[1 as libc::c_int as usize],
    );
    i = 0 as libc::c_int;
    while i < tmp.dim[0 as libc::c_int as usize] {
        j = 0 as libc::c_int;
        while j < tmp.dim[1 as libc::c_int as usize] {
            k = 0 as libc::c_int;
            while k < K1 {
                m1 = if trans1 != 0 {
                    *(*mat1.mat.offset(k as isize)).offset(i as isize)
                } else {
                    *(*mat1.mat.offset(i as isize)).offset(k as isize)
                };
                m2 = if trans2 != 0 {
                    *(*mat2.mat.offset(j as isize)).offset(k as isize)
                } else {
                    *(*mat2.mat.offset(k as isize)).offset(j as isize)
                };
                *(*tmp.mat.offset(i as isize)).offset(j as isize) += m1 * m2;
                k += 1
            }
            j += 1
        }
        i += 1
    }
    copy_array(tmp, ans);
    vmaxset(vmax);
}
unsafe extern "C" fn set_array_to_zero(mut arr: Array) {
    let mut i: libc::c_int = 0;
    i = 0 as libc::c_int;
    while i < vector_length(arr) {
        *arr.vec.offset(i as isize) = 0.0f64;
        i += 1
    }
}
unsafe extern "C" fn make_identity_matrix(mut n: libc::c_int) -> Array {
    let mut i: libc::c_int = 0;
    let mut a: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    a = make_zero_matrix(n, n);
    i = 0 as libc::c_int;
    while i < n {
        *(*a.mat.offset(i as isize)).offset(i as isize) = 1.0f64;
        i += 1
    }
    return a;
}
unsafe extern "C" fn qr_solve(mut x: Array, mut y: Array, mut coef: Array)
/* Translation of the R function qr.solve into pure C
   NB We have to transpose the matrices since the ordering of an array is different in Fortran
   NB2 We have to copy x to avoid it being overwritten.
*/
{
    let mut i: libc::c_int = 0;
    let mut info: libc::c_int = 0 as libc::c_int;
    let mut rank: libc::c_int = 0;
    let mut pivot: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut n: libc::c_int = 0;
    let mut p: libc::c_int = 0;
    let mut vmax: *const libc::c_void = 0 as *const libc::c_void;
    let mut tol: libc::c_double = 1.0E-7f64;
    let mut qraux: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut work: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut xt: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut yt: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut coeft: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    assert((x.dim[0 as libc::c_int as usize] == y.dim[0 as libc::c_int as usize]) as libc::c_int);
    assert(
        (coef.dim[1 as libc::c_int as usize] == y.dim[1 as libc::c_int as usize]) as libc::c_int,
    );
    assert(
        (x.dim[1 as libc::c_int as usize] == coef.dim[0 as libc::c_int as usize]) as libc::c_int,
    );
    vmax = vmaxget();
    qraux = R_alloc(
        x.dim[1 as libc::c_int as usize] as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    pivot = R_alloc(
        x.dim[1 as libc::c_int as usize] as size_t,
        ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_int;
    work = R_alloc(
        (2 as libc::c_int * x.dim[1 as libc::c_int as usize]) as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    i = 0 as libc::c_int;
    while i < x.dim[1 as libc::c_int as usize] {
        *pivot.offset(i as isize) = i + 1 as libc::c_int;
        i += 1
    }
    xt = make_zero_matrix(
        x.dim[1 as libc::c_int as usize],
        x.dim[0 as libc::c_int as usize],
    );
    transpose_matrix(x, xt);
    n = x.dim[0 as libc::c_int as usize];
    p = x.dim[1 as libc::c_int as usize];
    dqrdc2_(
        xt.vec, &mut n, &mut n, &mut p, &mut tol, &mut rank, qraux, pivot, work,
    );
    if rank != p {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Singular matrix in qr_solve\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    yt = make_zero_matrix(
        y.dim[1 as libc::c_int as usize],
        y.dim[0 as libc::c_int as usize],
    );
    coeft = make_zero_matrix(
        coef.dim[1 as libc::c_int as usize],
        coef.dim[0 as libc::c_int as usize],
    );
    transpose_matrix(y, yt);
    dqrcf_(
        xt.vec,
        &mut *x.dim.as_mut_ptr().offset(0 as libc::c_int as isize),
        &mut rank,
        qraux,
        yt.vec,
        &mut *y.dim.as_mut_ptr().offset(1 as libc::c_int as isize),
        coeft.vec,
        &mut info,
    );
    transpose_matrix(coeft, coef);
    vmaxset(vmax);
}
unsafe extern "C" fn ldet(mut x: Array) -> libc::c_double
/* Log determinant of square matrix */ {
    let mut i: libc::c_int = 0; /* is x a matrix? */
    let mut rank: libc::c_int = 0; /* is x square? */
    let mut pivot: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut n: libc::c_int = 0;
    let mut p: libc::c_int = 0;
    let mut vmax: *const libc::c_void = 0 as *const libc::c_void;
    let mut ll: libc::c_double = 0.;
    let mut tol: libc::c_double = 1.0E-7f64;
    let mut qraux: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut work: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut xtmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    assert((x.ndim == 2 as libc::c_int) as libc::c_int);
    assert((x.dim[0 as libc::c_int as usize] == x.dim[1 as libc::c_int as usize]) as libc::c_int);
    vmax = vmaxget();
    qraux = R_alloc(
        x.dim[1 as libc::c_int as usize] as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    pivot = R_alloc(
        x.dim[1 as libc::c_int as usize] as size_t,
        ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_int;
    work = R_alloc(
        (2 as libc::c_int * x.dim[1 as libc::c_int as usize]) as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    xtmp = make_zero_matrix(
        x.dim[0 as libc::c_int as usize],
        x.dim[1 as libc::c_int as usize],
    );
    copy_array(x, xtmp);
    i = 0 as libc::c_int;
    while i < x.dim[1 as libc::c_int as usize] {
        *pivot.offset(i as isize) = i + 1 as libc::c_int;
        i += 1
    }
    n = x.dim[0 as libc::c_int as usize];
    p = n;
    dqrdc2_(
        xtmp.vec, &mut n, &mut n, &mut p, &mut tol, &mut rank, qraux, pivot, work,
    );
    if rank != p {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Singular matrix in ldet\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    i = 0 as libc::c_int;
    ll = 0.0f64;
    while i < rank {
        ll += ((*(*xtmp.mat.offset(i as isize)).offset(i as isize)).abs()).ln();
        i += 1
    }
    vmaxset(vmax);
    return ll;
}
#[no_mangle]
pub unsafe extern "C" fn multi_burg(
    mut pn: *mut libc::c_int,
    mut x: *mut libc::c_double,
    mut pomax: *mut libc::c_int,
    mut pnser: *mut libc::c_int,
    mut coef: *mut libc::c_double,
    mut pacf: *mut libc::c_double,
    mut var: *mut libc::c_double,
    mut aic: *mut libc::c_double,
    mut porder: *mut libc::c_int,
    mut useaic: *mut libc::c_int,
    mut vmethod: *mut libc::c_int,
) {
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut m: libc::c_int = 0;
    let mut omax: libc::c_int = *pomax;
    let mut n: libc::c_int = *pn;
    let mut nser: libc::c_int = *pnser;
    let mut order: libc::c_int = *porder;
    let mut dim1: [libc::c_int; 3] = [0; 3];
    let mut aicmin: libc::c_double = 0.;
    let mut xarr: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut resid_f: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut resid_b: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut resid_f_tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut A: *mut Array = 0 as *mut Array;
    let mut B: *mut Array = 0 as *mut Array;
    let mut P: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut V: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    dim1[0 as libc::c_int as usize] = omax + 1 as libc::c_int;
    dim1[2 as libc::c_int as usize] = nser;
    dim1[1 as libc::c_int as usize] = dim1[2 as libc::c_int as usize];
    A = R_alloc(
        (omax + 1 as libc::c_int) as size_t,
        ::std::mem::size_of::<Array>() as libc::c_ulong as libc::c_int,
    ) as *mut Array;
    B = R_alloc(
        (omax + 1 as libc::c_int) as size_t,
        ::std::mem::size_of::<Array>() as libc::c_ulong as libc::c_int,
    ) as *mut Array;
    i = 0 as libc::c_int;
    while i <= omax {
        *A.offset(i as isize) = make_zero_array(dim1.as_mut_ptr(), 3 as libc::c_int);
        *B.offset(i as isize) = make_zero_array(dim1.as_mut_ptr(), 3 as libc::c_int);
        i += 1
    }
    P = make_array(pacf, dim1.as_mut_ptr(), 3 as libc::c_int);
    V = make_array(var, dim1.as_mut_ptr(), 3 as libc::c_int);
    xarr = make_matrix(x, nser, n);
    resid_f = make_zero_matrix(nser, n);
    resid_b = make_zero_matrix(nser, n);
    set_array_to_zero(resid_b);
    copy_array(xarr, resid_f);
    copy_array(xarr, resid_b);
    resid_f_tmp = make_zero_matrix(nser, n);
    burg0(omax, resid_f, resid_b, A, B, P, V, *vmethod);
    /* Model order selection */
    i = 0 as libc::c_int;
    while i <= omax {
        *aic.offset(i as isize) = n as libc::c_double * ldet(subarray(V, i))
            + (2 as libc::c_int * i * nser * nser) as libc::c_double;
        i += 1
    }
    if *useaic != 0 {
        order = 0 as libc::c_int;
        aicmin = *aic.offset(0 as libc::c_int as isize);
        i = 1 as libc::c_int;
        while i <= omax {
            if *aic.offset(i as isize) < aicmin {
                aicmin = *aic.offset(i as isize);
                order = i
            }
            i += 1
        }
    } else {
        order = omax
    }
    *porder = order;
    i = 0 as libc::c_int;
    while i < vector_length(*A.offset(order as isize)) {
        *coef.offset(i as isize) = *(*A.offset(order as isize)).vec.offset(i as isize);
        i += 1
    }
    if *useaic != 0 {
        /* Recalculate residuals for chosen model */
        set_array_to_zero(resid_f); /* Update K */
        set_array_to_zero(resid_f_tmp);
        m = 0 as libc::c_int;
        while m <= order {
            i = 0 as libc::c_int;
            while i < resid_f_tmp.dim[0 as libc::c_int as usize] {
                j = 0 as libc::c_int;
                while j < resid_f_tmp.dim[1 as libc::c_int as usize] - order {
                    *(*resid_f_tmp.mat.offset(i as isize)).offset((j + order) as isize) =
                        *(*xarr.mat.offset(i as isize)).offset((j + order - m) as isize);
                    j += 1
                }
                i += 1
            }
            matrix_prod(
                subarray(*A.offset(order as isize), m),
                resid_f_tmp,
                0 as libc::c_int,
                0 as libc::c_int,
                resid_f_tmp,
            );
            array_op(resid_f_tmp, resid_f, '+' as i32 as libc::c_char, resid_f);
            m += 1
        }
    }
    copy_array(resid_f, xarr);
}
unsafe extern "C" fn burg0(
    mut omax: libc::c_int,
    mut resid_f: Array,
    mut resid_b: Array,
    mut A: *mut Array,
    mut B: *mut Array,
    mut P: Array,
    mut V: Array,
    mut vmethod: libc::c_int,
) {
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut m: libc::c_int = 0;
    let mut n: libc::c_int = resid_f.dim[1 as libc::c_int as usize];
    let mut nser: libc::c_int = resid_f.dim[0 as libc::c_int as usize];
    let mut ss_ff: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut ss_bb: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut ss_fb: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut resid_f_tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut resid_b_tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut KA: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut KB: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut E: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut id: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    ss_ff = make_zero_matrix(nser, nser);
    ss_fb = make_zero_matrix(nser, nser);
    ss_bb = make_zero_matrix(nser, nser);
    resid_f_tmp = make_zero_matrix(nser, n);
    resid_b_tmp = make_zero_matrix(nser, n);
    id = make_identity_matrix(nser);
    tmp = make_zero_matrix(nser, nser);
    E = make_zero_matrix(nser, nser);
    KA = make_zero_matrix(nser, nser);
    KB = make_zero_matrix(nser, nser);
    set_array_to_zero(*A.offset(0 as libc::c_int as isize));
    set_array_to_zero(*B.offset(0 as libc::c_int as isize));
    copy_array(
        id,
        subarray(*A.offset(0 as libc::c_int as isize), 0 as libc::c_int),
    );
    copy_array(
        id,
        subarray(*B.offset(0 as libc::c_int as isize), 0 as libc::c_int),
    );
    matrix_prod(resid_f, resid_f, 0 as libc::c_int, 1 as libc::c_int, E);
    scalar_op(E, n as libc::c_double, '/' as i32 as libc::c_char, E);
    copy_array(E, subarray(V, 0 as libc::c_int));
    m = 0 as libc::c_int;
    while m < omax {
        i = 0 as libc::c_int;
        while i < nser {
            j = n - 1 as libc::c_int;
            while j > m {
                *(*resid_b.mat.offset(i as isize)).offset(j as isize) =
                    *(*resid_b.mat.offset(i as isize)).offset((j - 1 as libc::c_int) as isize);
                j -= 1
            }
            *(*resid_f.mat.offset(i as isize)).offset(m as isize) = 0.0f64;
            *(*resid_b.mat.offset(i as isize)).offset(m as isize) = 0.0f64;
            i += 1
        }
        matrix_prod(resid_f, resid_f, 0 as libc::c_int, 1 as libc::c_int, ss_ff);
        matrix_prod(resid_b, resid_b, 0 as libc::c_int, 1 as libc::c_int, ss_bb);
        matrix_prod(resid_f, resid_b, 0 as libc::c_int, 1 as libc::c_int, ss_fb);
        burg2(ss_ff, ss_bb, ss_fb, E, KA, KB);
        i = 0 as libc::c_int;
        while i <= m + 1 as libc::c_int {
            matrix_prod(
                KA,
                subarray(*B.offset(m as isize), m + 1 as libc::c_int - i),
                0 as libc::c_int,
                0 as libc::c_int,
                tmp,
            );
            array_op(
                subarray(*A.offset(m as isize), i),
                tmp,
                '-' as i32 as libc::c_char,
                subarray(*A.offset((m + 1 as libc::c_int) as isize), i),
            );
            matrix_prod(
                KB,
                subarray(*A.offset(m as isize), m + 1 as libc::c_int - i),
                0 as libc::c_int,
                0 as libc::c_int,
                tmp,
            );
            array_op(
                subarray(*B.offset(m as isize), i),
                tmp,
                '-' as i32 as libc::c_char,
                subarray(*B.offset((m + 1 as libc::c_int) as isize), i),
            );
            i += 1
        }
        matrix_prod(KA, resid_b, 0 as libc::c_int, 0 as libc::c_int, resid_f_tmp);
        matrix_prod(KB, resid_f, 0 as libc::c_int, 0 as libc::c_int, resid_b_tmp);
        array_op(resid_f, resid_f_tmp, '-' as i32 as libc::c_char, resid_f);
        array_op(resid_b, resid_b_tmp, '-' as i32 as libc::c_char, resid_b);
        if vmethod == 1 as libc::c_int {
            matrix_prod(KA, KB, 0 as libc::c_int, 0 as libc::c_int, tmp);
            array_op(id, tmp, '-' as i32 as libc::c_char, tmp);
            matrix_prod(tmp, E, 0 as libc::c_int, 0 as libc::c_int, E);
        } else if vmethod == 2 as libc::c_int {
            matrix_prod(resid_f, resid_f, 0 as libc::c_int, 1 as libc::c_int, E);
            matrix_prod(resid_b, resid_b, 0 as libc::c_int, 1 as libc::c_int, tmp);
            array_op(E, tmp, '+' as i32 as libc::c_char, E);
            scalar_op(
                E,
                2.0f64 * (n - m - 1 as libc::c_int) as libc::c_double,
                '/' as i32 as libc::c_char,
                E,
            );
        } else {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"Invalid vmethod\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        copy_array(E, subarray(V, m + 1 as libc::c_int));
        copy_array(KA, subarray(P, m + 1 as libc::c_int));
        m += 1
    }
}
unsafe extern "C" fn burg2(
    mut ss_ff: Array,
    mut ss_bb: Array,
    mut ss_fb: Array,
    mut E: Array,
    mut KA: Array,
    mut KB: Array,
)
/*
   Estimate partial correlation by minimizing (1/2)*det(s).ln() where
   "s" is the the sum of the forward and backward prediction errors.

   In the multivariate case, the forward (KA) and backward (KB) partial
   correlation coefficients are related by

      KA = solve(E) %*% t(KB) %*% E

   where E is the prediction variance.

*/
{
    let mut i: libc::c_int = 0; /* theta in vector form */
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut l: libc::c_int = 0;
    let mut nser: libc::c_int = ss_ff.dim[0 as libc::c_int as usize];
    let mut iter: libc::c_int = 0;
    let mut ss_bf: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut s: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut d1: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut D1: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut D2: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut THETA: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut THETAOLD: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut THETADIFF: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut TMP: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut obj: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut e: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut f: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut g: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut h: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut sg: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut sh: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut theta: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    ss_bf = make_zero_matrix(nser, nser);
    transpose_matrix(ss_fb, ss_bf);
    s = make_zero_matrix(nser, nser);
    tmp = make_zero_matrix(nser, nser);
    d1 = make_zero_matrix(nser, nser);
    e = make_zero_matrix(nser, nser);
    f = make_zero_matrix(nser, nser);
    g = make_zero_matrix(nser, nser);
    h = make_zero_matrix(nser, nser);
    sg = make_zero_matrix(nser, nser);
    sh = make_zero_matrix(nser, nser);
    theta = make_zero_matrix(nser, nser);
    D1 = make_zero_matrix(nser * nser, 1 as libc::c_int);
    D2 = make_zero_matrix(nser * nser, nser * nser);
    THETA = make_zero_matrix(nser * nser, 1 as libc::c_int);
    THETAOLD = make_zero_matrix(nser * nser, 1 as libc::c_int);
    THETADIFF = make_zero_matrix(nser * nser, 1 as libc::c_int);
    TMP = make_zero_matrix(nser * nser, 1 as libc::c_int);
    obj = make_zero_matrix(1 as libc::c_int, 1 as libc::c_int);
    /* utility matrices e,f,g,h */
    qr_solve(E, ss_bf, e);
    qr_solve(E, ss_fb, f);
    qr_solve(E, ss_bb, tmp);
    transpose_matrix(tmp, tmp);
    qr_solve(E, tmp, g);
    qr_solve(E, ss_ff, tmp);
    transpose_matrix(tmp, tmp);
    qr_solve(E, tmp, h);
    iter = 0 as libc::c_int;
    while iter < 20 as libc::c_int {
        /* Forward and backward partial correlation coefficients */
        transpose_matrix(theta, tmp);
        qr_solve(E, tmp, tmp);
        transpose_matrix(tmp, KA);
        qr_solve(E, theta, tmp);
        transpose_matrix(tmp, KB);
        /* Sum of forward and backward prediction errors ... */
        set_array_to_zero(s);
        /* Forward */
        array_op(s, ss_ff, '+' as i32 as libc::c_char, s);
        matrix_prod(KA, ss_bf, 0 as libc::c_int, 0 as libc::c_int, tmp);
        array_op(s, tmp, '-' as i32 as libc::c_char, s);
        transpose_matrix(tmp, tmp);
        array_op(s, tmp, '-' as i32 as libc::c_char, s);
        matrix_prod(ss_bb, KA, 0 as libc::c_int, 1 as libc::c_int, tmp);
        matrix_prod(KA, tmp, 0 as libc::c_int, 0 as libc::c_int, tmp);
        array_op(s, tmp, '+' as i32 as libc::c_char, s);
        /* Backward */
        array_op(s, ss_bb, '+' as i32 as libc::c_char, s);
        matrix_prod(KB, ss_fb, 0 as libc::c_int, 0 as libc::c_int, tmp);
        array_op(s, tmp, '-' as i32 as libc::c_char, s);
        transpose_matrix(tmp, tmp);
        array_op(s, tmp, '-' as i32 as libc::c_char, s);
        matrix_prod(ss_ff, KB, 0 as libc::c_int, 1 as libc::c_int, tmp);
        matrix_prod(KB, tmp, 0 as libc::c_int, 0 as libc::c_int, tmp);
        array_op(s, tmp, '+' as i32 as libc::c_char, s);
        matrix_prod(s, f, 0 as libc::c_int, 0 as libc::c_int, d1);
        matrix_prod(e, s, 1 as libc::c_int, 0 as libc::c_int, tmp);
        array_op(d1, tmp, '+' as i32 as libc::c_char, d1);
        /*matrix_prod(g,s,0,0,sg);*/
        matrix_prod(s, g, 0 as libc::c_int, 0 as libc::c_int, sg);
        matrix_prod(s, h, 0 as libc::c_int, 0 as libc::c_int, sh);
        i = 0 as libc::c_int;
        while i < nser {
            j = 0 as libc::c_int;
            while j < nser {
                *(*D1.mat.offset((nser * i + j) as isize)).offset(0 as libc::c_int as isize) =
                    *(*d1.mat.offset(i as isize)).offset(j as isize);
                k = 0 as libc::c_int;
                while k < nser {
                    l = 0 as libc::c_int;
                    while l < nser {
                        *(*D2.mat.offset((nser * i + j) as isize))
                            .offset((nser * k + l) as isize) = (i == k) as libc::c_int
                            as libc::c_double
                            * *(*sg.mat.offset(j as isize)).offset(l as isize)
                            + *(*sh.mat.offset(i as isize)).offset(k as isize)
                                * (j == l) as libc::c_int as libc::c_double;
                        l += 1
                    }
                    k += 1
                }
                j += 1
            }
            i += 1
        }
        copy_array(THETA, THETAOLD);
        qr_solve(D2, D1, THETA);
        i = 0 as libc::c_int;
        while i < vector_length(theta) {
            *theta.vec.offset(i as isize) = *THETA.vec.offset(i as isize);
            i += 1
        }
        matrix_prod(D2, THETA, 0 as libc::c_int, 0 as libc::c_int, TMP);
        array_op(THETAOLD, THETA, '-' as i32 as libc::c_char, THETADIFF);
        matrix_prod(D2, THETADIFF, 0 as libc::c_int, 0 as libc::c_int, TMP);
        matrix_prod(THETADIFF, TMP, 1 as libc::c_int, 0 as libc::c_int, obj);
        if *obj.vec.offset(0 as libc::c_int as isize) < 1.0E-8f64 {
            break;
        }
        iter += 1
    }
    if iter == 20 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Burg\'s algorithm failed to find partial correlation\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    };
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2001-2017 The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Whittle's algorithm for autoregression estimation

   multi_yw  is the interface to R. It also handles model selection using AIC

   whittle,whittle2     implement Whittle's recursion for solving the multivariate
            Yule-Walker equations.

   Notation

   resid        residuals (forward and backward)
   A            Estimates of forward autocorrelation coefficients
   B            Estimates of backward autocorrelation coefficients
   EA,EB        Prediction Variance
   KA,KB        Partial correlation coefficient
*/
#[no_mangle]
pub unsafe extern "C" fn multi_yw(
    mut acf: *mut libc::c_double,
    mut pn: *mut libc::c_int,
    mut pomax: *mut libc::c_int,
    mut pnser: *mut libc::c_int,
    mut coef: *mut libc::c_double,
    mut pacf: *mut libc::c_double,
    mut var: *mut libc::c_double,
    mut aic: *mut libc::c_double,
    mut porder: *mut libc::c_int,
    mut useaic: *mut libc::c_int,
) {
    let mut i: libc::c_int = 0;
    let mut m: libc::c_int = 0;
    let mut omax: libc::c_int = *pomax;
    let mut n: libc::c_int = *pn;
    let mut nser: libc::c_int = *pnser;
    let mut order: libc::c_int = *porder;
    let mut aicmin: libc::c_double = 0.;
    let mut acf_array: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut p_forward: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut p_back: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut v_forward: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut v_back: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut A: *mut Array = 0 as *mut Array;
    let mut B: *mut Array = 0 as *mut Array;
    let mut dim: [libc::c_int; 3] = [0; 3];
    dim[0 as libc::c_int as usize] = omax + 1 as libc::c_int;
    dim[2 as libc::c_int as usize] = nser;
    dim[1 as libc::c_int as usize] = dim[2 as libc::c_int as usize];
    acf_array = make_array(acf, dim.as_mut_ptr(), 3 as libc::c_int);
    p_forward = make_array(pacf, dim.as_mut_ptr(), 3 as libc::c_int);
    v_forward = make_array(var, dim.as_mut_ptr(), 3 as libc::c_int);
    /* Backward equations (discarded) */
    p_back = make_zero_array(dim.as_mut_ptr(), 3 as libc::c_int);
    v_back = make_zero_array(dim.as_mut_ptr(), 3 as libc::c_int);
    A = R_alloc(
        (omax + 2 as libc::c_int) as size_t,
        ::std::mem::size_of::<Array>() as libc::c_ulong as libc::c_int,
    ) as *mut Array;
    B = R_alloc(
        (omax + 2 as libc::c_int) as size_t,
        ::std::mem::size_of::<Array>() as libc::c_ulong as libc::c_int,
    ) as *mut Array;
    i = 0 as libc::c_int;
    while i <= omax {
        *A.offset(i as isize) = make_zero_array(dim.as_mut_ptr(), 3 as libc::c_int);
        *B.offset(i as isize) = make_zero_array(dim.as_mut_ptr(), 3 as libc::c_int);
        i += 1
    }
    whittle(acf_array, omax, A, B, p_forward, v_forward, p_back, v_back);
    /* Model order selection */
    m = 0 as libc::c_int; /* prediction variance */
    while m <= omax {
        *aic.offset(m as isize) = n as libc::c_double * ldet(subarray(v_forward, m))
            + (2 as libc::c_int * m * nser * nser) as libc::c_double; /* partial correlation coefficient */
        m += 1
    }
    if *useaic != 0 {
        order = 0 as libc::c_int;
        aicmin = *aic.offset(0 as libc::c_int as isize);
        m = 0 as libc::c_int;
        while m <= omax {
            if *aic.offset(m as isize) < aicmin {
                aicmin = *aic.offset(m as isize);
                order = m
            }
            m += 1
        }
    } else {
        order = omax
    }
    *porder = order;
    i = 0 as libc::c_int;
    while i < vector_length(*A.offset(order as isize)) {
        *coef.offset(i as isize) = *(*A.offset(order as isize)).vec.offset(i as isize);
        i += 1
    }
}
unsafe extern "C" fn whittle(
    mut acf: Array,
    mut nlag: libc::c_int,
    mut A: *mut Array,
    mut B: *mut Array,
    mut p_forward: Array,
    mut v_forward: Array,
    mut p_back: Array,
    mut v_back: Array,
) {
    let mut lag: libc::c_int = 0;
    let mut nser: libc::c_int = acf.dim[1 as libc::c_int as usize];
    let mut vmax: *const libc::c_void = 0 as *const libc::c_void;
    let mut EA: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut EB: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut KA: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut KB: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut id: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    vmax = vmaxget();
    KA = make_zero_matrix(nser, nser);
    EA = make_zero_matrix(nser, nser);
    KB = make_zero_matrix(nser, nser);
    EB = make_zero_matrix(nser, nser);
    id = make_identity_matrix(nser);
    copy_array(
        id,
        subarray(*A.offset(0 as libc::c_int as isize), 0 as libc::c_int),
    );
    copy_array(
        id,
        subarray(*B.offset(0 as libc::c_int as isize), 0 as libc::c_int),
    );
    copy_array(id, subarray(p_forward, 0 as libc::c_int));
    copy_array(id, subarray(p_back, 0 as libc::c_int));
    lag = 1 as libc::c_int;
    while lag <= nlag {
        whittle2(
            acf,
            *A.offset((lag - 1 as libc::c_int) as isize),
            *B.offset((lag - 1 as libc::c_int) as isize),
            lag,
            b"forward\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
            *A.offset(lag as isize),
            KA,
            EB,
        );
        whittle2(
            acf,
            *B.offset((lag - 1 as libc::c_int) as isize),
            *A.offset((lag - 1 as libc::c_int) as isize),
            lag,
            b"back\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
            *B.offset(lag as isize),
            KB,
            EA,
        );
        copy_array(EA, subarray(v_forward, lag - 1 as libc::c_int));
        copy_array(EB, subarray(v_back, lag - 1 as libc::c_int));
        copy_array(KA, subarray(p_forward, lag));
        copy_array(KB, subarray(p_back, lag));
        lag += 1
    }
    tmp = make_zero_matrix(nser, nser);
    matrix_prod(KB, KA, 1 as libc::c_int, 1 as libc::c_int, tmp);
    array_op(id, tmp, '-' as i32 as libc::c_char, tmp);
    matrix_prod(
        EA,
        tmp,
        0 as libc::c_int,
        0 as libc::c_int,
        subarray(v_forward, nlag),
    );
    vmaxset(vmax);
}
unsafe extern "C" fn whittle2(
    mut acf: Array,
    mut Aold: Array,
    mut Bold: Array,
    mut lag: libc::c_int,
    mut direction: *mut libc::c_char,
    mut A: Array,
    mut K: Array,
    mut E: Array,
) {
    let mut d: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut nser: libc::c_int = acf.dim[1 as libc::c_int as usize];
    let mut vmax: *const libc::c_void = 0 as *const libc::c_void;
    let mut beta: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut tmp: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    let mut id: Array = Array {
        vec: 0 as *mut libc::c_double,
        mat: 0 as *mut *mut libc::c_double,
        arr3: 0 as *mut *mut *mut libc::c_double,
        arr4: 0 as *mut *mut *mut *mut libc::c_double,
        dim: [0; 4],
        ndim: 0,
    };
    d = (strcmp(
        direction,
        b"forward\x00" as *const u8 as *const libc::c_char,
    ) == 0 as libc::c_int) as libc::c_int;
    vmax = vmaxget();
    beta = make_zero_matrix(nser, nser);
    tmp = make_zero_matrix(nser, nser);
    id = make_identity_matrix(nser);
    set_array_to_zero(E);
    copy_array(id, subarray(A, 0 as libc::c_int));
    i = 0 as libc::c_int;
    while i < lag {
        matrix_prod(
            subarray(acf, lag - i),
            subarray(Aold, i),
            d,
            1 as libc::c_int,
            tmp,
        );
        array_op(beta, tmp, '+' as i32 as libc::c_char, beta);
        matrix_prod(
            subarray(acf, i),
            subarray(Bold, i),
            d,
            1 as libc::c_int,
            tmp,
        );
        array_op(E, tmp, '+' as i32 as libc::c_char, E);
        i += 1
    }
    qr_solve(E, beta, K);
    transpose_matrix(K, K);
    i = 1 as libc::c_int;
    while i <= lag {
        matrix_prod(
            K,
            subarray(Bold, lag - i),
            0 as libc::c_int,
            0 as libc::c_int,
            tmp,
        );
        array_op(
            subarray(Aold, i),
            tmp,
            '-' as i32 as libc::c_char,
            subarray(A, i),
        );
        i += 1
    }
    vmaxset(vmax);
}
