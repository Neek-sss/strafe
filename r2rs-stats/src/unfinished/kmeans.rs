use ::libc;
extern "C" {
    #[no_mangle]
    static mut R_PosInf: libc::c_double;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2016    The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn Rprintf(_: *const libc::c_char, _: ...);
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
/*, MAYBE */
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2004   The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
#[no_mangle]
pub unsafe extern "C" fn kmeans_Lloyd(
    mut x: *mut libc::c_double,
    mut pn: *mut libc::c_int,
    mut pp: *mut libc::c_int,
    mut cen: *mut libc::c_double,
    mut pk: *mut libc::c_int,
    mut cl: *mut libc::c_int,
    mut pmaxiter: *mut libc::c_int,
    mut nc: *mut libc::c_int,
    mut wss: *mut libc::c_double,
) {
    let mut n: libc::c_int = *pn;
    let mut k: libc::c_int = *pk;
    let mut p: libc::c_int = *pp;
    let mut maxiter: libc::c_int = *pmaxiter;
    let mut iter: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut c: libc::c_int = 0;
    let mut it: libc::c_int = 0;
    let mut inew: libc::c_int = 0 as libc::c_int;
    let mut best: libc::c_double = 0.;
    let mut dd: libc::c_double = 0.;
    let mut tmp: libc::c_double = 0.;
    let mut updated: Rboolean = FALSE;
    i = 0 as libc::c_int;
    while i < n {
        *cl.offset(i as isize) = -(1 as libc::c_int);
        i += 1
    }
    iter = 0 as libc::c_int;
    while iter < maxiter {
        updated = FALSE;
        i = 0 as libc::c_int;
        while i < n {
            /* find nearest centre for each point */
            best = R_PosInf;
            j = 0 as libc::c_int;
            while j < k {
                dd = 0.0f64;
                c = 0 as libc::c_int;
                while c < p {
                    tmp = *x.offset((i + n * c) as isize) - *cen.offset((j + k * c) as isize);
                    dd += tmp * tmp;
                    c += 1
                }
                if dd < best {
                    best = dd;
                    inew = j + 1 as libc::c_int
                }
                j += 1
            }
            if *cl.offset(i as isize) != inew {
                updated = TRUE;
                *cl.offset(i as isize) = inew
            }
            i += 1
        }
        if updated as u64 == 0 {
            break;
        }
        /* update each centre */
        j = 0 as libc::c_int;
        while j < k * p {
            *cen.offset(j as isize) = 0.0f64;
            j += 1
        }
        j = 0 as libc::c_int;
        while j < k {
            *nc.offset(j as isize) = 0 as libc::c_int;
            j += 1
        }
        i = 0 as libc::c_int;
        while i < n {
            it = *cl.offset(i as isize) - 1 as libc::c_int;
            let ref mut fresh0 = *nc.offset(it as isize);
            *fresh0 += 1;
            c = 0 as libc::c_int;
            while c < p {
                *cen.offset((it + c * k) as isize) += *x.offset((i + c * n) as isize);
                c += 1
            }
            i += 1
        }
        j = 0 as libc::c_int;
        while j < k * p {
            *cen.offset(j as isize) /= *nc.offset((j % k) as isize) as libc::c_double;
            j += 1
        }
        iter += 1
    }
    *pmaxiter = iter + 1 as libc::c_int;
    j = 0 as libc::c_int;
    while j < k {
        *wss.offset(j as isize) = 0.0f64;
        j += 1
    }
    i = 0 as libc::c_int;
    while i < n {
        it = *cl.offset(i as isize) - 1 as libc::c_int;
        c = 0 as libc::c_int;
        while c < p {
            tmp = *x.offset((i + n * c) as isize) - *cen.offset((it + k * c) as isize);
            *wss.offset(it as isize) += tmp * tmp;
            c += 1
        }
        i += 1
    }
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2001-2017 The R Core Team.
 *  Copyright (C) 2003-2016 The R Foundation
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* monoSpl.c : */
#[no_mangle]
pub unsafe extern "C" fn kmeans_MacQueen(
    mut x: *mut libc::c_double,
    mut pn: *mut libc::c_int,
    mut pp: *mut libc::c_int,
    mut cen: *mut libc::c_double,
    mut pk: *mut libc::c_int,
    mut cl: *mut libc::c_int,
    mut pmaxiter: *mut libc::c_int,
    mut nc: *mut libc::c_int,
    mut wss: *mut libc::c_double,
) {
    let mut n: libc::c_int = *pn;
    let mut k: libc::c_int = *pk;
    let mut p: libc::c_int = *pp;
    let mut maxiter: libc::c_int = *pmaxiter;
    let mut iter: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut c: libc::c_int = 0;
    let mut it: libc::c_int = 0;
    let mut inew: libc::c_int = 0 as libc::c_int;
    let mut iold: libc::c_int = 0;
    let mut best: libc::c_double = 0.;
    let mut dd: libc::c_double = 0.;
    let mut tmp: libc::c_double = 0.;
    let mut updated: Rboolean = FALSE;
    /* first assign each point to the nearest cluster centre */
    i = 0 as libc::c_int;
    while i < n {
        best = R_PosInf;
        j = 0 as libc::c_int;
        while j < k {
            dd = 0.0f64;
            c = 0 as libc::c_int;
            while c < p {
                tmp = *x.offset((i + n * c) as isize) - *cen.offset((j + k * c) as isize);
                dd += tmp * tmp;
                c += 1
            }
            if dd < best {
                best = dd;
                inew = j + 1 as libc::c_int
            }
            j += 1
        }
        if *cl.offset(i as isize) != inew {
            *cl.offset(i as isize) = inew
        }
        i += 1
    }
    /* and recompute centres as centroids */
    j = 0 as libc::c_int;
    while j < k * p {
        *cen.offset(j as isize) = 0.0f64;
        j += 1
    }
    j = 0 as libc::c_int;
    while j < k {
        *nc.offset(j as isize) = 0 as libc::c_int;
        j += 1
    }
    i = 0 as libc::c_int;
    while i < n {
        it = *cl.offset(i as isize) - 1 as libc::c_int;
        let ref mut fresh1 = *nc.offset(it as isize);
        *fresh1 += 1;
        c = 0 as libc::c_int;
        while c < p {
            *cen.offset((it + c * k) as isize) += *x.offset((i + c * n) as isize);
            c += 1
        }
        i += 1
    }
    j = 0 as libc::c_int;
    while j < k * p {
        *cen.offset(j as isize) /= *nc.offset((j % k) as isize) as libc::c_double;
        j += 1
    }
    iter = 0 as libc::c_int;
    while iter < maxiter {
        updated = FALSE;
        i = 0 as libc::c_int;
        while i < n {
            best = R_PosInf;
            j = 0 as libc::c_int;
            while j < k {
                dd = 0.0f64;
                c = 0 as libc::c_int;
                while c < p {
                    tmp = *x.offset((i + n * c) as isize) - *cen.offset((j + k * c) as isize);
                    dd += tmp * tmp;
                    c += 1
                }
                if dd < best {
                    best = dd;
                    inew = j
                }
                j += 1
            }
            iold = *cl.offset(i as isize) - 1 as libc::c_int;
            if iold != inew {
                updated = TRUE;
                *cl.offset(i as isize) = inew + 1 as libc::c_int;
                let ref mut fresh2 = *nc.offset(iold as isize);
                *fresh2 -= 1;
                let ref mut fresh3 = *nc.offset(inew as isize);
                *fresh3 += 1;
                /* update old and new cluster centres */
                c = 0 as libc::c_int;
                while c < p {
                    *cen.offset((iold + k * c) as isize) += (*cen.offset((iold + k * c) as isize)
                        - *x.offset((i + n * c) as isize))
                        / *nc.offset(iold as isize) as libc::c_double;
                    *cen.offset((inew + k * c) as isize) += (*x.offset((i + n * c) as isize)
                        - *cen.offset((inew + k * c) as isize))
                        / *nc.offset(inew as isize) as libc::c_double;
                    c += 1
                }
            }
            i += 1
        }
        if updated as u64 == 0 {
            break;
        }
        iter += 1
    }
    *pmaxiter = iter + 1 as libc::c_int;
    j = 0 as libc::c_int;
    while j < k {
        *wss.offset(j as isize) = 0.0f64;
        j += 1
    }
    i = 0 as libc::c_int;
    while i < n {
        it = *cl.offset(i as isize) - 1 as libc::c_int;
        c = 0 as libc::c_int;
        while c < p {
            tmp = *x.offset((i + n * c) as isize) - *cen.offset((it + k * c) as isize);
            *wss.offset(it as isize) += tmp * tmp;
            c += 1
        }
        i += 1
    }
}
// tracing for  kmeans() in  ./kmns.f
#[no_mangle]
pub unsafe extern "C" fn kmns1_(
    mut k: *mut libc::c_int,
    mut it: *mut libc::c_int,
    mut indx: *mut libc::c_int,
) {
    Rprintf(
        b"KMNS(*, k=%d): iter=%3d, indx=%d\n\x00" as *const u8 as *const libc::c_char,
        *k,
        *it,
        *indx,
    );
}
#[no_mangle]
pub unsafe extern "C" fn kmnsqpr_(
    mut istep: *mut libc::c_int,
    mut icoun: *mut libc::c_int,
    mut NCP: *mut libc::c_int,
    mut k: *mut libc::c_int,
    mut trace: *mut libc::c_int,
) {
    Rprintf(
        b" QTRAN(): istep=%d, icoun=%d\x00" as *const u8 as *const libc::c_char,
        *istep,
        *icoun,
    );
    if *trace >= 2 as libc::c_int {
        Rprintf(
            b", NCP[1:%d]=\x00" as *const u8 as *const libc::c_char,
            *k.offset(0 as libc::c_int as isize),
        );
        let mut i: libc::c_int = 0 as libc::c_int;
        while i < *k.offset(0 as libc::c_int as isize) {
            Rprintf(
                b" %d\x00" as *const u8 as *const libc::c_char,
                *NCP.offset(i as isize),
            );
            i += 1
        }
    }
    Rprintf(b"\n\x00" as *const u8 as *const libc::c_char);
}
