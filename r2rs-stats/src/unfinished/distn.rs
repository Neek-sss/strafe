use crate::sexprec::{SEXP, SEXPREC, SEXPREC_ALIGN, SEXPTYPE, VECSEXP};
use ::libc;
extern "C" {
    pub type R_allocator;
    #[no_mangle]
    fn SHALLOW_DUPLICATE_ATTRIB(to: SEXP, from: SEXP);
    #[no_mangle]
    fn ALTREP_LENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn ALTVEC_DATAPTR(x: SEXP) -> *mut libc::c_void;
    #[no_mangle]
    fn ALTSTRING_ELT(_: SEXP, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    #[no_mangle]
    static mut R_NilValue: SEXP;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
     *  Copyright (C) 1998--2016  The R Core Team.
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    /*
       This used to define _BSD_SOURCE to make declarations of isfinite
       and isnan visible in glibc.  But that was deprecated in glibc 2.20,
       and --std=c99 suffices nowadays.
    */
    /* needed for isnan and isfinite, neither of which are used under C++ */
    /* implementation of these : ../../main/arithmetic.c */
    #[no_mangle]
    static mut R_NaN: libc::c_double;
    /* IEEE -Inf */
    #[no_mangle]
    static mut R_NaReal: libc::c_double;
    /* NA_INTEGER:= INT_MIN currently */
    /* #define NA_FACTOR R_NaInt  unused */
    /* NA_STRING is a SEXP, so defined in Rinternals.h */
    #[no_mangle]
    fn R_IsNA(_: libc::c_double) -> libc::c_int;
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn R_BadLongVector(_: SEXP, _: *const libc::c_char, _: libc::c_int) -> !;
    #[no_mangle]
    static mut R_ClassSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn allocVector3(_: SEXPTYPE, _: R_xlen_t, _: *mut R_allocator_t) -> SEXP;
    #[no_mangle]
    fn getAttrib(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn R_signal_protect_error() -> !;
    #[no_mangle]
    fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    #[no_mangle]
    static mut R_PPStackSize: libc::c_int;
    #[no_mangle]
    static mut R_PPStackTop: libc::c_int;
    #[no_mangle]
    static mut R_PPStack: *mut SEXP;
    /* INLINE_PROTECT */
    /* from dstruct.c */
    /*  length - length of objects  */
    #[no_mangle]
    fn envlength(rho: SEXP) -> libc::c_int;
    /* Normal Distribution */
    #[no_mangle]
    fn dnorm(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pnorm(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qnorm(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* both tails */
    /* Uniform Distribution */
    #[no_mangle]
    fn dunif(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn punif(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qunif(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Gamma Distribution */
    #[no_mangle]
    fn dgamma(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pgamma(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qgamma(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Beta Distribution */
    #[no_mangle]
    fn dbeta(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pbeta(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qbeta(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Lognormal Distribution */
    #[no_mangle]
    fn dlnorm(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn plnorm(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qlnorm(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Chi-squared Distribution */
    #[no_mangle]
    fn dchisq(_: libc::c_double, _: libc::c_double, _: libc::c_int) -> libc::c_double;
    #[no_mangle]
    fn pchisq(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qchisq(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Non-central Chi-squared Distribution */
    #[no_mangle]
    fn dnchisq(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pnchisq(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qnchisq(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* F Distibution */
    #[no_mangle]
    fn df(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pf(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qf(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Student t Distibution */
    #[no_mangle]
    fn dt(_: libc::c_double, _: libc::c_double, _: libc::c_int) -> libc::c_double;
    #[no_mangle]
    fn pt(_: libc::c_double, _: libc::c_double, _: libc::c_int, _: libc::c_int) -> libc::c_double;
    #[no_mangle]
    fn qt(_: libc::c_double, _: libc::c_double, _: libc::c_int, _: libc::c_int) -> libc::c_double;
    #[no_mangle]
    fn dbinom(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pbinom(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qbinom(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Cauchy Distribution */
    #[no_mangle]
    fn dcauchy(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pcauchy(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qcauchy(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Exponential Distribution */
    #[no_mangle]
    fn dexp(_: libc::c_double, _: libc::c_double, _: libc::c_int) -> libc::c_double;
    #[no_mangle]
    fn pexp(_: libc::c_double, _: libc::c_double, _: libc::c_int, _: libc::c_int)
        -> libc::c_double;
    #[no_mangle]
    fn qexp(_: libc::c_double, _: libc::c_double, _: libc::c_int, _: libc::c_int)
        -> libc::c_double;
    /* Geometric Distribution */
    #[no_mangle]
    fn dgeom(_: libc::c_double, _: libc::c_double, _: libc::c_int) -> libc::c_double;
    #[no_mangle]
    fn pgeom(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qgeom(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Hypergeometric Distibution */
    #[no_mangle]
    fn dhyper(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn phyper(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qhyper(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Negative Binomial Distribution */
    #[no_mangle]
    fn dnbinom(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pnbinom(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qnbinom(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn dnbinom_mu(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pnbinom_mu(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qnbinom_mu(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn dpois(_: libc::c_double, _: libc::c_double, _: libc::c_int) -> libc::c_double;
    #[no_mangle]
    fn ppois(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qpois(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Weibull Distribution */
    #[no_mangle]
    fn dweibull(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pweibull(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qweibull(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Logistic Distribution */
    #[no_mangle]
    fn dlogis(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn plogis(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qlogis(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Non-central Beta Distribution */
    #[no_mangle]
    fn dnbeta(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pnbeta(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qnbeta(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Non-central F Distribution */
    #[no_mangle]
    fn dnf(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pnf(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qnf(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Non-central Student t Distribution */
    #[no_mangle]
    fn dnt(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pnt(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qnt(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Studentized Range Distribution */
    #[no_mangle]
    fn ptukey(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qtukey(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Wilcoxon Rank Sum Distribution */
    #[no_mangle]
    fn dwilcox(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn pwilcox(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qwilcox(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* Wilcoxon Signed Rank Distribution */
    #[no_mangle]
    fn dsignrank(_: libc::c_double, _: libc::c_double, _: libc::c_int) -> libc::c_double;
    #[no_mangle]
    fn psignrank(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn qsignrank(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* These are here to get them in the correct package */
    /* from src/nmath/wilcox.c */
    #[no_mangle]
    fn signrank_free();
    #[no_mangle]
    fn wilcox_free();
}
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
/*, MAYBE */
/* type for length of (standard, not long) vectors etc */
pub type R_len_t = libc::c_int;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
pub type R_allocator_t = R_allocator;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1999-2017  The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Internal header, not installed */
/* this header is always to be included from others.
  It is only called if COMPILING_R is defined (in util.c) or
  from GNU C systems.

  There are different conventions for inlining across compilation units.
  See http://www.greenend.org.uk/rjk/2003/03/inline.html
*/
/* Probably not able to use C99 semantics in gcc < 4.3.0 */
/* Apple's gcc build >5400 (since Xcode 3.0) doesn't support GNU inline in C99 mode */
/* This section is normally only used for versions of gcc which do not
   support C99 semantics.  __GNUC_STDC_INLINE__ is defined if
   GCC is following C99 inline semantics by default: we
   switch R's usage to the older GNU semantics via attributes.
   Do this even for __GNUC_GNUC_INLINE__ to shut up warnings in 4.2.x.
   __GNUC_STDC_INLINE__ and __GNUC_GNU_INLINE__ were added in gcc 4.2.0.
*/
/* ifdef COMPILING_R */
/* C99_INLINE_SEMANTICS */
/* for strlen, strcmp */
/* define inline-able functions */
#[inline]
unsafe extern "C" fn DATAPTR(mut x: SEXP) -> *mut libc::c_void {
    if (*x).sxpinfo.alt() != 0 {
        return ALTVEC_DATAPTR(x);
    } else {
        return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void;
    };
}
#[inline]
unsafe extern "C" fn XLENGTH_EX(mut x: SEXP) -> R_xlen_t {
    return if (*x).sxpinfo.alt() as libc::c_int != 0 {
        ALTREP_LENGTH(x)
    } else {
        (*(x as VECSEXP)).vecsxp.length
    };
}
#[inline]
unsafe extern "C" fn LENGTH_EX(
    mut x: SEXP,
    mut file: *const libc::c_char,
    mut line: libc::c_int,
) -> libc::c_int {
    if x == R_NilValue {
        return 0 as libc::c_int;
    }
    let mut len: R_xlen_t = XLENGTH_EX(x);
    if len > 2147483647 as libc::c_int as libc::c_long {
        R_BadLongVector(x, file, line);
    }
    return len as libc::c_int;
}
/* if not inlining use version in memory.c with more error checking */
#[inline]
unsafe extern "C" fn STRING_ELT(mut x: SEXP, mut i: R_xlen_t) -> SEXP {
    if (*x).sxpinfo.alt() != 0 {
        return ALTSTRING_ELT(x, i);
    } else {
        let mut ps: *mut SEXP = (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize)
            as *mut libc::c_void as *mut SEXP;
        return *ps.offset(i as isize);
    };
}
#[inline]
unsafe extern "C" fn protect(mut s: SEXP) -> SEXP {
    if R_PPStackTop < R_PPStackSize {
        let fresh0 = R_PPStackTop;
        R_PPStackTop = R_PPStackTop + 1;
        let ref mut fresh1 = *R_PPStack.offset(fresh0 as isize);
        *fresh1 = s
    } else {
        R_signal_protect_error();
    }
    return s;
}
#[inline]
unsafe extern "C" fn unprotect(mut l: libc::c_int) {
    R_PPStackTop -= l;
}
/* TODO: a  Length(.) {say} which is  length() + dispatch (S3 + S4) if needed
         for one approach, see do_seq_along() in ../main/seq.c
*/
#[inline]
unsafe extern "C" fn length(mut s: SEXP) -> R_len_t {
    match (*s).sxpinfo.type_0() as libc::c_int {
        0 => return 0 as libc::c_int,
        10 | 13 | 14 | 15 | 16 | 9 | 19 | 20 | 24 => {
            return LENGTH_EX(
                s,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                522 as libc::c_int,
            )
        }
        2 | 6 | 17 => {
            let mut i: libc::c_int = 0 as libc::c_int;
            while !s.is_null() && s != R_NilValue {
                i += 1;
                s = (*s).u.listsxp.cdrval
            }
            return i;
        }
        4 => return envlength(s),
        _ => return 1 as libc::c_int,
    };
}
/* regular allocVector() as a special case of allocVector3() with no custom allocator */
#[inline]
unsafe extern "C" fn allocVector(mut type_0: SEXPTYPE, mut length: R_xlen_t) -> SEXP {
    return allocVector3(type_0, length, 0 as *mut R_allocator_t);
}
/* NOTE: R's inherits() is based on inherits3() in ../main/objects.c
 * Here, use char / CHAR() instead of the slower more general translateChar()
 */
#[inline]
unsafe extern "C" fn inherits(mut s: SEXP, mut name: *const libc::c_char) -> Rboolean {
    let mut klass: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut nclass: libc::c_int = 0;
    if (*s).sxpinfo.obj() != 0 {
        klass = getAttrib(s, R_ClassSymbol);
        nclass = length(klass);
        i = 0 as libc::c_int;
        while i < nclass {
            if strcmp(
                (STRING_ELT(klass, i as R_xlen_t) as *mut SEXPREC_ALIGN)
                    .offset(1 as libc::c_int as isize) as *mut libc::c_void
                    as *const libc::c_char,
                name,
            ) == 0
            {
                return TRUE;
            }
            i += 1
        }
    }
    return FALSE;
}
/* Is an object of numeric type. */
/* FIXME:  the LGLSXP case should be excluded here
 * (really? in many places we affirm they are treated like INTs)*/
#[inline]
unsafe extern "C" fn isNumeric(mut s: SEXP) -> Rboolean {
    match (*s).sxpinfo.type_0() as libc::c_int {
        13 => {
            if inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as u64 != 0 {
                return FALSE;
            }
        }
        10 | 14 => {}
        _ => return FALSE,
    }
    return TRUE;
}
/*
*  R : A Computer Language for Statistical Data Analysis
*  Copyright (C) 1998--2018  The R Core Team
*  Copyright (C) 1995--1997  Robert Gentleman and Ross Ihaka
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, a copy is available at
*  https://www.R-project.org/Licenses/

* distn ==  [DIST]ributio[N]s, i.e. probability distributions
* -----                             notably R's   [dpq]<kind>()  functions
*/
/* interval at which to check interrupts */
//#define NINTERRUPT 1000000
/* Mathematical Functions of Two Numeric Arguments (plus 1 int) */
unsafe extern "C" fn math2_1(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sI: SEXP,
    mut f: Option<
        unsafe extern "C" fn(
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_int,
        ) -> libc::c_double,
    >,
) -> SEXP {
    let mut sy: SEXP = 0 as *mut SEXPREC;
    let mut i: R_xlen_t = 0;
    let mut ia: R_xlen_t = 0;
    let mut ib: R_xlen_t = 0;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    let mut nb: R_xlen_t = 0;
    let mut ai: libc::c_double = 0.;
    let mut bi: libc::c_double = 0.;
    let mut a: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut b: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut y: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut m_opt: libc::c_int = 0;
    let mut naflag: libc::c_int = 0;
    if isNumeric(sa) as u64 == 0 || isNumeric(sb) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Non-numeric argument to mathematical function\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    na = XLENGTH_EX(sa);
    nb = XLENGTH_EX(sb);
    if na == 0 as libc::c_int as libc::c_long || nb == 0 as libc::c_int as libc::c_long {
        sy = allocVector(14 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t);
        protect(sy);
        if na == 0 as libc::c_int as libc::c_long {
            SHALLOW_DUPLICATE_ATTRIB(sy, sa);
        }
        unprotect(1 as libc::c_int);
        return sy;
    }
    n = if na < nb { nb } else { na };
    sa = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
    protect(sa);
    sb = coerceVector(sb, 14 as libc::c_int as SEXPTYPE);
    protect(sb);
    sy = allocVector(14 as libc::c_int as SEXPTYPE, n);
    protect(sy);
    a = DATAPTR(sa) as *mut libc::c_double;
    b = DATAPTR(sb) as *mut libc::c_double;
    y = DATAPTR(sy) as *mut libc::c_double;
    naflag = 0 as libc::c_int;
    m_opt = asInteger(sI);
    ib = 0 as libc::c_int as R_xlen_t;
    ia = ib;
    i = ia;
    while i < n {
        // if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
        ai = *a.offset(ia as isize);
        bi = *b.offset(ib as isize);
        if R_IsNA(ai) != 0 || R_IsNA(bi) != 0 {
            *y.offset(i as isize) = R_NaReal
        } else if ai.is_nan() as i32 != 0 as libc::c_int || bi.is_nan() as i32 != 0 as libc::c_int {
            *y.offset(i as isize) = R_NaN
        } else {
            *y.offset(i as isize) = f.expect("non-null function pointer")(ai, bi, m_opt);
            if (*y.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                naflag = 1 as libc::c_int
            }
        }
        ia += 1;
        ia = if ia == na {
            0 as libc::c_int as libc::c_long
        } else {
            ia
        };
        ib += 1;
        ib = if ib == nb {
            0 as libc::c_int as libc::c_long
        } else {
            ib
        };
        i += 1
    }
    if naflag != 0 {
        warning(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"NaNs produced\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if n == na {
        SHALLOW_DUPLICATE_ATTRIB(sy, sa);
    } else if n == nb {
        SHALLOW_DUPLICATE_ATTRIB(sy, sb);
    }
    unprotect(3 as libc::c_int);
    return sy;
}
/* math2_1() */
unsafe extern "C" fn math2_2(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sI1: SEXP,
    mut sI2: SEXP,
    mut f: Option<
        unsafe extern "C" fn(
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_int,
            _: libc::c_int,
        ) -> libc::c_double,
    >,
) -> SEXP {
    let mut sy: SEXP = 0 as *mut SEXPREC;
    let mut i: R_xlen_t = 0;
    let mut ia: R_xlen_t = 0;
    let mut ib: R_xlen_t = 0;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    let mut nb: R_xlen_t = 0;
    let mut ai: libc::c_double = 0.;
    let mut bi: libc::c_double = 0.;
    let mut a: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut b: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut y: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i_1: libc::c_int = 0;
    let mut i_2: libc::c_int = 0;
    let mut naflag: libc::c_int = 0;
    if isNumeric(sa) as u64 == 0 || isNumeric(sb) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Non-numeric argument to mathematical function\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    na = XLENGTH_EX(sa);
    nb = XLENGTH_EX(sb);
    if na == 0 as libc::c_int as libc::c_long || nb == 0 as libc::c_int as libc::c_long {
        sy = allocVector(14 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t);
        protect(sy);
        if na == 0 as libc::c_int as libc::c_long {
            SHALLOW_DUPLICATE_ATTRIB(sy, sa);
        }
        unprotect(1 as libc::c_int);
        return sy;
    }
    n = if na < nb { nb } else { na };
    sa = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
    protect(sa);
    sb = coerceVector(sb, 14 as libc::c_int as SEXPTYPE);
    protect(sb);
    sy = allocVector(14 as libc::c_int as SEXPTYPE, n);
    protect(sy);
    a = DATAPTR(sa) as *mut libc::c_double;
    b = DATAPTR(sb) as *mut libc::c_double;
    y = DATAPTR(sy) as *mut libc::c_double;
    naflag = 0 as libc::c_int;
    i_1 = asInteger(sI1);
    i_2 = asInteger(sI2);
    ib = 0 as libc::c_int as R_xlen_t;
    ia = ib;
    i = ia;
    while i < n {
        // if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
        ai = *a.offset(ia as isize);
        bi = *b.offset(ib as isize);
        if R_IsNA(ai) != 0 || R_IsNA(bi) != 0 {
            *y.offset(i as isize) = R_NaReal
        } else if ai.is_nan() as i32 != 0 as libc::c_int || bi.is_nan() as i32 != 0 as libc::c_int {
            *y.offset(i as isize) = R_NaN
        } else {
            *y.offset(i as isize) = f.expect("non-null function pointer")(ai, bi, i_1, i_2);
            if (*y.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                naflag = 1 as libc::c_int
            }
        }
        ia += 1;
        ia = if ia == na {
            0 as libc::c_int as libc::c_long
        } else {
            ia
        };
        ib += 1;
        ib = if ib == nb {
            0 as libc::c_int as libc::c_long
        } else {
            ib
        };
        i += 1
    }
    if naflag != 0 {
        warning(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"NaNs produced\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if n == na {
        SHALLOW_DUPLICATE_ATTRIB(sy, sa);
    } else if n == nb {
        SHALLOW_DUPLICATE_ATTRIB(sy, sb);
    }
    unprotect(3 as libc::c_int);
    return sy;
}
/* math2_2() */
#[no_mangle]
pub unsafe extern "C" fn do_dchisq(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP) -> SEXP {
    return math2_1(
        sa,
        sb,
        sI,
        Some(
            dchisq
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dexp(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP) -> SEXP {
    return math2_1(
        sa,
        sb,
        sI,
        Some(
            dexp as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dgeom(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP) -> SEXP {
    return math2_1(
        sa,
        sb,
        sI,
        Some(
            dgeom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dpois(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP) -> SEXP {
    return math2_1(
        sa,
        sb,
        sI,
        Some(
            dpois
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dt(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP) -> SEXP {
    return math2_1(
        sa,
        sb,
        sI,
        Some(
            dt as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dsignrank(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP) -> SEXP {
    return math2_1(
        sa,
        sb,
        sI,
        Some(
            dsignrank
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pchisq(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            pchisq
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qchisq(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            qchisq
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pexp(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            pexp as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qexp(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            qexp as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pgeom(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            pgeom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qgeom(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            qgeom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_ppois(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            ppois
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qpois(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            qpois
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pt(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            pt as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qt(mut sa: SEXP, mut sb: SEXP, mut sI: SEXP, mut sJ: SEXP) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            qt as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_psignrank(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            psignrank
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qsignrank(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math2_2(
        sa,
        sb,
        sI,
        sJ,
        Some(
            qsignrank
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
/* Mathematical Functions of Three (Real) Arguments */
unsafe extern "C" fn math3_1(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut f: Option<
        unsafe extern "C" fn(
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_int,
        ) -> libc::c_double,
    >,
) -> SEXP {
    let mut sy: SEXP = 0 as *mut SEXPREC;
    let mut i: R_xlen_t = 0;
    let mut ia: R_xlen_t = 0;
    let mut ib: R_xlen_t = 0;
    let mut ic: R_xlen_t = 0;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    let mut nb: R_xlen_t = 0;
    let mut nc: R_xlen_t = 0;
    let mut ai: libc::c_double = 0.;
    let mut bi: libc::c_double = 0.;
    let mut ci: libc::c_double = 0.;
    let mut a: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut b: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut c: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut y: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i_1: libc::c_int = 0;
    let mut naflag: libc::c_int = 0;
    if isNumeric(sa) as u64 == 0 || isNumeric(sb) as u64 == 0 || isNumeric(sc) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Non-numeric argument to mathematical function\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    na = XLENGTH_EX(sa);
    nb = XLENGTH_EX(sb);
    nc = XLENGTH_EX(sc);
    if na == 0 as libc::c_int as libc::c_long
        || nb == 0 as libc::c_int as libc::c_long
        || nc == 0 as libc::c_int as libc::c_long
    {
        return allocVector(14 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t);
    }
    n = na;
    if n < nb {
        n = nb
    }
    if n < nc {
        n = nc
    }
    sa = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
    protect(sa);
    sb = coerceVector(sb, 14 as libc::c_int as SEXPTYPE);
    protect(sb);
    sc = coerceVector(sc, 14 as libc::c_int as SEXPTYPE);
    protect(sc);
    sy = allocVector(14 as libc::c_int as SEXPTYPE, n);
    protect(sy);
    a = DATAPTR(sa) as *mut libc::c_double;
    b = DATAPTR(sb) as *mut libc::c_double;
    c = DATAPTR(sc) as *mut libc::c_double;
    y = DATAPTR(sy) as *mut libc::c_double;
    naflag = 0 as libc::c_int;
    i_1 = asInteger(sI);
    ic = 0 as libc::c_int as R_xlen_t;
    ib = ic;
    ia = ib;
    i = ia;
    while i < n {
        // if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
        ai = *a.offset(ia as isize);
        bi = *b.offset(ib as isize);
        ci = *c.offset(ic as isize);
        if R_IsNA(ai) != 0 || R_IsNA(bi) != 0 || R_IsNA(ci) != 0 {
            *y.offset(i as isize) = R_NaReal
        } else if ai.is_nan() as i32 != 0 as libc::c_int
            || bi.is_nan() as i32 != 0 as libc::c_int
            || ci.is_nan() as i32 != 0 as libc::c_int
        {
            *y.offset(i as isize) = R_NaN
        } else {
            *y.offset(i as isize) = f.expect("non-null function pointer")(ai, bi, ci, i_1);
            if (*y.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                naflag = 1 as libc::c_int
            }
        }
        ia += 1;
        ia = if ia == na {
            0 as libc::c_int as libc::c_long
        } else {
            ia
        };
        ib += 1;
        ib = if ib == nb {
            0 as libc::c_int as libc::c_long
        } else {
            ib
        };
        ic += 1;
        ic = if ic == nc {
            0 as libc::c_int as libc::c_long
        } else {
            ic
        };
        i += 1
    }
    if naflag != 0 {
        warning(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"NaNs produced\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if n == na {
        SHALLOW_DUPLICATE_ATTRIB(sy, sa);
    } else if n == nb {
        SHALLOW_DUPLICATE_ATTRIB(sy, sb);
    } else if n == nc {
        SHALLOW_DUPLICATE_ATTRIB(sy, sc);
    }
    unprotect(4 as libc::c_int);
    return sy;
}
/* math3_1 */
unsafe extern "C" fn math3_2(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
    mut f: Option<
        unsafe extern "C" fn(
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_int,
            _: libc::c_int,
        ) -> libc::c_double,
    >,
) -> SEXP {
    let mut sy: SEXP = 0 as *mut SEXPREC;
    let mut i: R_xlen_t = 0;
    let mut ia: R_xlen_t = 0;
    let mut ib: R_xlen_t = 0;
    let mut ic: R_xlen_t = 0;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    let mut nb: R_xlen_t = 0;
    let mut nc: R_xlen_t = 0;
    let mut ai: libc::c_double = 0.;
    let mut bi: libc::c_double = 0.;
    let mut ci: libc::c_double = 0.;
    let mut a: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut b: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut c: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut y: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i_1: libc::c_int = 0;
    let mut i_2: libc::c_int = 0;
    let mut naflag: libc::c_int = 0;
    if isNumeric(sa) as u64 == 0 || isNumeric(sb) as u64 == 0 || isNumeric(sc) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Non-numeric argument to mathematical function\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    na = XLENGTH_EX(sa);
    nb = XLENGTH_EX(sb);
    nc = XLENGTH_EX(sc);
    if na == 0 as libc::c_int as libc::c_long
        || nb == 0 as libc::c_int as libc::c_long
        || nc == 0 as libc::c_int as libc::c_long
    {
        return allocVector(14 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t);
    }
    n = na;
    if n < nb {
        n = nb
    }
    if n < nc {
        n = nc
    }
    sa = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
    protect(sa);
    sb = coerceVector(sb, 14 as libc::c_int as SEXPTYPE);
    protect(sb);
    sc = coerceVector(sc, 14 as libc::c_int as SEXPTYPE);
    protect(sc);
    sy = allocVector(14 as libc::c_int as SEXPTYPE, n);
    protect(sy);
    a = DATAPTR(sa) as *mut libc::c_double;
    b = DATAPTR(sb) as *mut libc::c_double;
    c = DATAPTR(sc) as *mut libc::c_double;
    y = DATAPTR(sy) as *mut libc::c_double;
    naflag = 0 as libc::c_int;
    i_1 = asInteger(sI);
    i_2 = asInteger(sJ);
    ic = 0 as libc::c_int as R_xlen_t;
    ib = ic;
    ia = ib;
    i = ia;
    while i < n {
        // if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
        ai = *a.offset(ia as isize);
        bi = *b.offset(ib as isize);
        ci = *c.offset(ic as isize);
        if R_IsNA(ai) != 0 || R_IsNA(bi) != 0 || R_IsNA(ci) != 0 {
            *y.offset(i as isize) = R_NaReal
        } else if ai.is_nan() as i32 != 0 as libc::c_int
            || bi.is_nan() as i32 != 0 as libc::c_int
            || ci.is_nan() as i32 != 0 as libc::c_int
        {
            *y.offset(i as isize) = R_NaN
        } else {
            *y.offset(i as isize) = f.expect("non-null function pointer")(ai, bi, ci, i_1, i_2);
            if (*y.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                naflag = 1 as libc::c_int
            }
        }
        ia += 1;
        ia = if ia == na {
            0 as libc::c_int as libc::c_long
        } else {
            ia
        };
        ib += 1;
        ib = if ib == nb {
            0 as libc::c_int as libc::c_long
        } else {
            ib
        };
        ic += 1;
        ic = if ic == nc {
            0 as libc::c_int as libc::c_long
        } else {
            ic
        };
        i += 1
    }
    if naflag != 0 {
        warning(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"NaNs produced\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if n == na {
        SHALLOW_DUPLICATE_ATTRIB(sy, sa);
    } else if n == nb {
        SHALLOW_DUPLICATE_ATTRIB(sy, sb);
    } else if n == nc {
        SHALLOW_DUPLICATE_ATTRIB(sy, sc);
    }
    unprotect(4 as libc::c_int);
    return sy;
}
/* math3_2 */
#[no_mangle]
pub unsafe extern "C" fn do_dbeta(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dbeta
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dbinom(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dbinom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dcauchy(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dcauchy
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_df(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            df as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dgamma(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dgamma
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dlnorm(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dlnorm
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dlogis(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dlogis
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dnbinom(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dnbinom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dnbinom_mu(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dnbinom_mu
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dnorm(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dnorm
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dweibull(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dweibull
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dunif(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dunif
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dnt(mut sa: SEXP, mut sb: SEXP, mut sc: SEXP, mut sI: SEXP) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dnt as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dnchisq(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dnchisq
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dwilcox(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math3_1(
        sa,
        sb,
        sc,
        sI,
        Some(
            dwilcox
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pbeta(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pbeta
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qbeta(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qbeta
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pbinom(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pbinom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qbinom(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qbinom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pcauchy(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pcauchy
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qcauchy(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qcauchy
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pf(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pf as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qf(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qf as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pgamma(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pgamma
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qgamma(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qgamma
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_plnorm(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            plnorm
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qlnorm(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qlnorm
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_plogis(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            plogis
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qlogis(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qlogis
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pnbinom(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pnbinom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qnbinom(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qnbinom
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pnbinom_mu(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pnbinom_mu
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qnbinom_mu(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qnbinom_mu
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pnorm(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pnorm
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qnorm(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qnorm
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pweibull(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pweibull
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qweibull(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qweibull
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_punif(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            punif
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qunif(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qunif
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pnt(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pnt as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qnt(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qnt as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pnchisq(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pnchisq
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qnchisq(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qnchisq
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pwilcox(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            pwilcox
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qwilcox(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math3_2(
        sa,
        sb,
        sc,
        sI,
        sJ,
        Some(
            qwilcox
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
/* Mathematical Functions of Four (Real) Arguments */
unsafe extern "C" fn math4_1(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut f: Option<
        unsafe extern "C" fn(
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_int,
        ) -> libc::c_double,
    >,
) -> SEXP {
    let mut sy: SEXP = 0 as *mut SEXPREC;
    let mut i: R_xlen_t = 0;
    let mut ia: R_xlen_t = 0;
    let mut ib: R_xlen_t = 0;
    let mut ic: R_xlen_t = 0;
    let mut id: R_xlen_t = 0;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    let mut nb: R_xlen_t = 0;
    let mut nc: R_xlen_t = 0;
    let mut nd: R_xlen_t = 0;
    let mut ai: libc::c_double = 0.;
    let mut bi: libc::c_double = 0.;
    let mut ci: libc::c_double = 0.;
    let mut di: libc::c_double = 0.;
    let mut a: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut b: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut c: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut d: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut y: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i_1: libc::c_int = 0;
    let mut naflag: libc::c_int = 0;
    if isNumeric(sa) as u64 == 0
        || isNumeric(sb) as u64 == 0
        || isNumeric(sc) as u64 == 0
        || isNumeric(sd) as u64 == 0
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Non-numeric argument to mathematical function\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    na = XLENGTH_EX(sa);
    nb = XLENGTH_EX(sb);
    nc = XLENGTH_EX(sc);
    nd = XLENGTH_EX(sd);
    if na == 0 as libc::c_int as libc::c_long
        || nb == 0 as libc::c_int as libc::c_long
        || nc == 0 as libc::c_int as libc::c_long
        || nd == 0 as libc::c_int as libc::c_long
    {
        return allocVector(14 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t);
    }
    n = na;
    if n < nb {
        n = nb
    }
    if n < nc {
        n = nc
    }
    if n < nd {
        n = nd
    }
    sa = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
    protect(sa);
    sb = coerceVector(sb, 14 as libc::c_int as SEXPTYPE);
    protect(sb);
    sc = coerceVector(sc, 14 as libc::c_int as SEXPTYPE);
    protect(sc);
    sd = coerceVector(sd, 14 as libc::c_int as SEXPTYPE);
    protect(sd);
    sy = allocVector(14 as libc::c_int as SEXPTYPE, n);
    protect(sy);
    a = DATAPTR(sa) as *mut libc::c_double;
    b = DATAPTR(sb) as *mut libc::c_double;
    c = DATAPTR(sc) as *mut libc::c_double;
    d = DATAPTR(sd) as *mut libc::c_double;
    y = DATAPTR(sy) as *mut libc::c_double;
    naflag = 0 as libc::c_int;
    i_1 = asInteger(sI);
    id = 0 as libc::c_int as R_xlen_t;
    ic = id;
    ib = ic;
    ia = ib;
    i = ia;
    while i < n {
        // if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
        ai = *a.offset(ia as isize);
        bi = *b.offset(ib as isize);
        ci = *c.offset(ic as isize);
        di = *d.offset(id as isize);
        if R_IsNA(ai) != 0 || R_IsNA(bi) != 0 || R_IsNA(ci) != 0 || R_IsNA(di) != 0 {
            *y.offset(i as isize) = R_NaReal
        } else if ai.is_nan() as i32 != 0 as libc::c_int
            || bi.is_nan() as i32 != 0 as libc::c_int
            || ci.is_nan() as i32 != 0 as libc::c_int
            || di.is_nan() as i32 != 0 as libc::c_int
        {
            *y.offset(i as isize) = R_NaN
        } else {
            *y.offset(i as isize) = f.expect("non-null function pointer")(ai, bi, ci, di, i_1);
            if (*y.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                naflag = 1 as libc::c_int
            }
        }
        ia += 1;
        ia = if ia == na {
            0 as libc::c_int as libc::c_long
        } else {
            ia
        };
        ib += 1;
        ib = if ib == nb {
            0 as libc::c_int as libc::c_long
        } else {
            ib
        };
        ic += 1;
        ic = if ic == nc {
            0 as libc::c_int as libc::c_long
        } else {
            ic
        };
        id += 1;
        id = if id == nd {
            0 as libc::c_int as libc::c_long
        } else {
            id
        };
        i += 1
    }
    if naflag != 0 {
        warning(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"NaNs produced\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if n == na {
        SHALLOW_DUPLICATE_ATTRIB(sy, sa);
    } else if n == nb {
        SHALLOW_DUPLICATE_ATTRIB(sy, sb);
    } else if n == nc {
        SHALLOW_DUPLICATE_ATTRIB(sy, sc);
    } else if n == nd {
        SHALLOW_DUPLICATE_ATTRIB(sy, sd);
    }
    unprotect(5 as libc::c_int);
    return sy;
}
/* math4_1() */
unsafe extern "C" fn math4_2(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
    mut f: Option<
        unsafe extern "C" fn(
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_double,
            _: libc::c_int,
            _: libc::c_int,
        ) -> libc::c_double,
    >,
) -> SEXP {
    let mut sy: SEXP = 0 as *mut SEXPREC;
    let mut i: R_xlen_t = 0;
    let mut ia: R_xlen_t = 0;
    let mut ib: R_xlen_t = 0;
    let mut ic: R_xlen_t = 0;
    let mut id: R_xlen_t = 0;
    let mut n: R_xlen_t = 0;
    let mut na: R_xlen_t = 0;
    let mut nb: R_xlen_t = 0;
    let mut nc: R_xlen_t = 0;
    let mut nd: R_xlen_t = 0;
    let mut ai: libc::c_double = 0.;
    let mut bi: libc::c_double = 0.;
    let mut ci: libc::c_double = 0.;
    let mut di: libc::c_double = 0.;
    let mut a: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut b: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut c: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut d: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut y: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i_1: libc::c_int = 0;
    let mut i_2: libc::c_int = 0;
    let mut naflag: libc::c_int = 0;
    if isNumeric(sa) as u64 == 0
        || isNumeric(sb) as u64 == 0
        || isNumeric(sc) as u64 == 0
        || isNumeric(sd) as u64 == 0
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"Non-numeric argument to mathematical function\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    na = XLENGTH_EX(sa);
    nb = XLENGTH_EX(sb);
    nc = XLENGTH_EX(sc);
    nd = XLENGTH_EX(sd);
    if na == 0 as libc::c_int as libc::c_long
        || nb == 0 as libc::c_int as libc::c_long
        || nc == 0 as libc::c_int as libc::c_long
        || nd == 0 as libc::c_int as libc::c_long
    {
        return allocVector(14 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t);
    }
    n = na;
    if n < nb {
        n = nb
    }
    if n < nc {
        n = nc
    }
    if n < nd {
        n = nd
    }
    sa = coerceVector(sa, 14 as libc::c_int as SEXPTYPE);
    protect(sa);
    sb = coerceVector(sb, 14 as libc::c_int as SEXPTYPE);
    protect(sb);
    sc = coerceVector(sc, 14 as libc::c_int as SEXPTYPE);
    protect(sc);
    sd = coerceVector(sd, 14 as libc::c_int as SEXPTYPE);
    protect(sd);
    sy = allocVector(14 as libc::c_int as SEXPTYPE, n);
    protect(sy);
    a = DATAPTR(sa) as *mut libc::c_double;
    b = DATAPTR(sb) as *mut libc::c_double;
    c = DATAPTR(sc) as *mut libc::c_double;
    d = DATAPTR(sd) as *mut libc::c_double;
    y = DATAPTR(sy) as *mut libc::c_double;
    naflag = 0 as libc::c_int;
    i_1 = asInteger(sI);
    i_2 = asInteger(sJ);
    id = 0 as libc::c_int as R_xlen_t;
    ic = id;
    ib = ic;
    ia = ib;
    i = ia;
    while i < n {
        // if ((i+1) % NINTERRUPT) R_CheckUserInterrupt();
        ai = *a.offset(ia as isize);
        bi = *b.offset(ib as isize);
        ci = *c.offset(ic as isize);
        di = *d.offset(id as isize);
        if R_IsNA(ai) != 0 || R_IsNA(bi) != 0 || R_IsNA(ci) != 0 || R_IsNA(di) != 0 {
            *y.offset(i as isize) = R_NaReal
        } else if ai.is_nan() as i32 != 0 as libc::c_int
            || bi.is_nan() as i32 != 0 as libc::c_int
            || ci.is_nan() as i32 != 0 as libc::c_int
            || di.is_nan() as i32 != 0 as libc::c_int
        {
            *y.offset(i as isize) = R_NaN
        } else {
            *y.offset(i as isize) = f.expect("non-null function pointer")(ai, bi, ci, di, i_1, i_2);
            if (*y.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                naflag = 1 as libc::c_int
            }
        }
        ia += 1;
        ia = if ia == na {
            0 as libc::c_int as libc::c_long
        } else {
            ia
        };
        ib += 1;
        ib = if ib == nb {
            0 as libc::c_int as libc::c_long
        } else {
            ib
        };
        ic += 1;
        ic = if ic == nc {
            0 as libc::c_int as libc::c_long
        } else {
            ic
        };
        id += 1;
        id = if id == nd {
            0 as libc::c_int as libc::c_long
        } else {
            id
        };
        i += 1
    }
    if naflag != 0 {
        warning(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"NaNs produced\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if n == na {
        SHALLOW_DUPLICATE_ATTRIB(sy, sa);
    } else if n == nb {
        SHALLOW_DUPLICATE_ATTRIB(sy, sb);
    } else if n == nc {
        SHALLOW_DUPLICATE_ATTRIB(sy, sc);
    } else if n == nd {
        SHALLOW_DUPLICATE_ATTRIB(sy, sd);
    }
    unprotect(5 as libc::c_int);
    return sy;
}
/* math4_2() */
#[no_mangle]
pub unsafe extern "C" fn do_dhyper(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math4_1(
        sa,
        sb,
        sc,
        sd,
        sI,
        Some(
            dhyper
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dnbeta(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math4_1(
        sa,
        sb,
        sc,
        sd,
        sI,
        Some(
            dnbeta
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_dnf(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
) -> SEXP {
    return math4_1(
        sa,
        sb,
        sc,
        sd,
        sI,
        Some(
            dnf as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_phyper(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math4_2(
        sa,
        sb,
        sc,
        sd,
        sI,
        sJ,
        Some(
            phyper
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qhyper(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math4_2(
        sa,
        sb,
        sc,
        sd,
        sI,
        sJ,
        Some(
            qhyper
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pnbeta(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math4_2(
        sa,
        sb,
        sc,
        sd,
        sI,
        sJ,
        Some(
            pnbeta
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qnbeta(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math4_2(
        sa,
        sb,
        sc,
        sd,
        sI,
        sJ,
        Some(
            qnbeta
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_pnf(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math4_2(
        sa,
        sb,
        sc,
        sd,
        sI,
        sJ,
        Some(
            pnf as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qnf(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math4_2(
        sa,
        sb,
        sc,
        sd,
        sI,
        sJ,
        Some(
            qnf as unsafe extern "C" fn(
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_double,
                _: libc::c_int,
                _: libc::c_int,
            ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_ptukey(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math4_2(
        sa,
        sb,
        sc,
        sd,
        sI,
        sJ,
        Some(
            ptukey
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn do_qtukey(
    mut sa: SEXP,
    mut sb: SEXP,
    mut sc: SEXP,
    mut sd: SEXP,
    mut sI: SEXP,
    mut sJ: SEXP,
) -> SEXP {
    return math4_2(
        sa,
        sb,
        sc,
        sd,
        sI,
        sJ,
        Some(
            qtukey
                as unsafe extern "C" fn(
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_double,
                    _: libc::c_int,
                    _: libc::c_int,
                ) -> libc::c_double,
        ),
    );
}
#[no_mangle]
pub unsafe extern "C" fn stats_signrank_free() -> SEXP {
    signrank_free();
    return R_NilValue;
}
#[no_mangle]
pub unsafe extern "C" fn stats_wilcox_free() -> SEXP {
    wilcox_free();
    return R_NilValue;
}
