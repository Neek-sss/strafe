use crate::{
    fft::fft_factor,
    fft::fft_work,
    sexprec::{SEXP, SEXPREC, SEXPREC_ALIGN, SEXPTYPE, VECSEXP},
};
use ::libc;
extern "C" {
    pub type R_allocator;
    #[no_mangle]
    fn ALTREP_LENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn ALTVEC_DATAPTR(x: SEXP) -> *mut libc::c_void;
    #[no_mangle]
    fn R_BadLongVector(_: SEXP, _: *const libc::c_char, _: libc::c_int) -> !;
    /* Current srcref, for debuggers */
    /* Special Values */
    #[no_mangle]
    static mut R_NilValue: SEXP;
    /* "dimnames" */
    #[no_mangle]
    static mut R_DimSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asLogical(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn allocVector3(_: SEXPTYPE, _: R_xlen_t, _: *mut R_allocator_t) -> SEXP;
    #[no_mangle]
    fn duplicate(_: SEXP) -> SEXP;
    #[no_mangle]
    fn getAttrib(_: SEXP, _: SEXP) -> SEXP;
    /* IEEE -Inf */
    #[no_mangle]
    static mut R_NaReal: libc::c_double;
    /* NA_REAL: IEEE */
    #[no_mangle]
    static mut R_NaInt: libc::c_int;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn R_signal_protect_error() -> !;
    #[no_mangle]
    static mut R_PPStackSize: libc::c_int;
    #[no_mangle]
    static mut R_PPStackTop: libc::c_int;
    #[no_mangle]
    static mut R_PPStack: *mut SEXP;
    /* INLINE_PROTECT */
    /* from dstruct.c */
    /*  length - length of objects  */
    #[no_mangle]
    fn envlength(rho: SEXP) -> libc::c_int;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998--2018  The R Core Team
     *  Copyright (C) 1995--1997  Robert Gentleman and Ross Ihaka
     *
     *  This program is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* These are the R interface routines to the plain FFT code
    fft_factor() & fft_work() in fft.c. */
    // for PRIu64

}
pub type __uint64_t = libc::c_ulong;
pub type uint64_t = __uint64_t;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1998-2001   The R Core Team
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
#[derive(Copy, Clone)]
#[repr(C)]
pub struct Rcomplex {
    pub r: libc::c_double,
    pub i: libc::c_double,
}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
/*, MAYBE */
/* type for length of (standard, not long) vectors etc */
pub type R_len_t = libc::c_int;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */

pub type R_allocator_t = R_allocator;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1999-2017  The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Internal header, not installed */
/* this header is always to be included from others.
  It is only called if COMPILING_R is defined (in util.c) or
  from GNU C systems.

  There are different conventions for inlining across compilation units.
  See http://www.greenend.org.uk/rjk/2003/03/inline.html
*/
/* Probably not able to use C99 semantics in gcc < 4.3.0 */
/* Apple's gcc build >5400 (since Xcode 3.0) doesn't support GNU inline in C99 mode */
/* This section is normally only used for versions of gcc which do not
   support C99 semantics.  __GNUC_STDC_INLINE__ is defined if
   GCC is following C99 inline semantics by default: we
   switch R's usage to the older GNU semantics via attributes.
   Do this even for __GNUC_GNUC_INLINE__ to shut up warnings in 4.2.x.
   __GNUC_STDC_INLINE__ and __GNUC_GNU_INLINE__ were added in gcc 4.2.0.
*/
/* ifdef COMPILING_R */
/* C99_INLINE_SEMANTICS */
/* for strlen, strcmp */
/* define inline-able functions */
#[inline]
unsafe extern "C" fn DATAPTR(mut x: SEXP) -> *mut libc::c_void {
    if (*x).sxpinfo.alt() != 0 {
        return ALTVEC_DATAPTR(x);
    } else {
        return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void;
    };
}
#[inline]
unsafe extern "C" fn XLENGTH_EX(mut x: SEXP) -> R_xlen_t {
    return if (*x).sxpinfo.alt() as libc::c_int != 0 {
        ALTREP_LENGTH(x)
    } else {
        (*(x as VECSEXP)).vecsxp.length
    };
}
#[inline]
unsafe extern "C" fn LENGTH_EX(
    mut x: SEXP,
    mut file: *const libc::c_char,
    mut line: libc::c_int,
) -> libc::c_int {
    if x == R_NilValue {
        return 0 as libc::c_int;
    }
    let mut len: R_xlen_t = XLENGTH_EX(x);
    if len > 2147483647 as libc::c_int as libc::c_long {
        R_BadLongVector(x, file, line);
    }
    return len as libc::c_int;
}
#[inline]
unsafe extern "C" fn protect(mut s: SEXP) -> SEXP {
    if R_PPStackTop < R_PPStackSize {
        let fresh0 = R_PPStackTop;
        R_PPStackTop = R_PPStackTop + 1;
        let ref mut fresh1 = *R_PPStack.offset(fresh0 as isize);
        *fresh1 = s
    } else {
        R_signal_protect_error();
    }
    return s;
}
#[inline]
unsafe extern "C" fn unprotect(mut l: libc::c_int) {
    R_PPStackTop -= l;
}
/* TODO: a  Length(.) {say} which is  length() + dispatch (S3 + S4) if needed
         for one approach, see do_seq_along() in ../main/seq.c
*/
#[inline]
unsafe extern "C" fn length(mut s: SEXP) -> R_len_t {
    match (*s).sxpinfo.type_0() as libc::c_int {
        0 => return 0 as libc::c_int,
        10 | 13 | 14 | 15 | 16 | 9 | 19 | 20 | 24 => {
            return LENGTH_EX(
                s,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                522 as libc::c_int,
            )
        }
        2 | 6 | 17 => {
            let mut i: libc::c_int = 0 as libc::c_int;
            while !s.is_null() && s != R_NilValue {
                i += 1;
                s = (*s).u.listsxp.cdrval
            }
            return i;
        }
        4 => return envlength(s),
        _ => return 1 as libc::c_int,
    };
}
/* regular allocVector() as a special case of allocVector3() with no custom allocator */
#[inline]
unsafe extern "C" fn allocVector(mut type_0: SEXPTYPE, mut length: R_xlen_t) -> SEXP {
    return allocVector3(type_0, length, 0 as *mut R_allocator_t);
}
/* Fourier Transform for Univariate Spatial and Time Series */
#[no_mangle]
pub unsafe extern "C" fn fft(mut z: SEXP, mut inverse: SEXP) -> SEXP {
    let mut d: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut inv: libc::c_int = 0;
    let mut maxf: libc::c_int = 0;
    let mut maxmaxf: libc::c_int = 0;
    let mut maxmaxp: libc::c_int = 0;
    let mut maxp: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut ndims: libc::c_int = 0;
    let mut nseg: libc::c_int = 0;
    let mut nspn: libc::c_int = 0;
    let mut work: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut iwork: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut smaxf: size_t = 0;
    let mut maxsize: size_t =
        (-(1 as libc::c_int) as size_t).wrapping_div(4 as libc::c_int as libc::c_ulong);
    match (*z).sxpinfo.type_0() as libc::c_int {
        13 | 10 | 14 => z = coerceVector(z, 15 as libc::c_int as SEXPTYPE),
        15 => {
            if !((*z).sxpinfo.named() as libc::c_int == 0 as libc::c_int) {
                z = duplicate(z)
            }
        }
        _ => {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"non-numeric argument\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
    }
    protect(z);
    /* -2 for forward transform, complex values */
    /* +2 for backward transform, complex values */
    inv = asLogical(inverse);
    if inv == R_NaInt || inv == 0 as libc::c_int {
        inv = -(2 as libc::c_int)
    } else {
        inv = 2 as libc::c_int
    }
    if LENGTH_EX(
        z,
        b"fourier.c\x00" as *const u8 as *const libc::c_char,
        82 as libc::c_int,
    ) > 1 as libc::c_int
    {
        d = getAttrib(z, R_DimSymbol);
        if (*d).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int {
            /* temporal transform */
            n = length(z);
            fft_factor(n, &mut maxf, &mut maxp);
            if maxf == 0 as libc::c_int {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"fft factorization error\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
            smaxf = maxf as size_t;
            if smaxf > maxsize {
                error(b"fft too large\x00" as *const u8 as *const libc::c_char);
            }
            work = R_alloc(
                (4 as libc::c_int as libc::c_ulong).wrapping_mul(smaxf),
                ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
            ) as *mut libc::c_double;
            iwork = R_alloc(
                maxp as size_t,
                ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
            ) as *mut libc::c_int;
            fft_work(
                &mut (*((DATAPTR as unsafe extern "C" fn(_: SEXP) -> *mut libc::c_void)(z)
                    as *mut Rcomplex)
                    .offset(0 as libc::c_int as isize))
                .r,
                &mut (*((DATAPTR as unsafe extern "C" fn(_: SEXP) -> *mut libc::c_void)(z)
                    as *mut Rcomplex)
                    .offset(0 as libc::c_int as isize))
                .i,
                1 as libc::c_int,
                n,
                1 as libc::c_int,
                inv,
                work,
                iwork,
            );
        } else {
            /* spatial transform */
            maxmaxf = 1 as libc::c_int;
            maxmaxp = 1 as libc::c_int;
            ndims = LENGTH_EX(
                d,
                b"fourier.c\x00" as *const u8 as *const libc::c_char,
                99 as libc::c_int,
            );
            /* do whole loop just for error checking and maxmax[fp] .. */
            i = 0 as libc::c_int;
            while i < ndims {
                if *(DATAPTR(d) as *mut libc::c_int).offset(i as isize) > 1 as libc::c_int {
                    fft_factor(
                        *(DATAPTR(d) as *mut libc::c_int).offset(i as isize),
                        &mut maxf,
                        &mut maxp,
                    );
                    if maxf == 0 as libc::c_int {
                        error(dcgettext(
                            b"stats\x00" as *const u8 as *const libc::c_char,
                            b"fft factorization error\x00" as *const u8 as *const libc::c_char,
                            5 as libc::c_int,
                        ));
                    }
                    if maxf > maxmaxf {
                        maxmaxf = maxf
                    }
                    if maxp > maxmaxp {
                        maxmaxp = maxp
                    }
                }
                i += 1
            }
            smaxf = maxmaxf as size_t;
            if smaxf > maxsize {
                error(b"fft too large\x00" as *const u8 as *const libc::c_char);
            }
            work = R_alloc(
                (4 as libc::c_int as libc::c_ulong).wrapping_mul(smaxf),
                ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
            ) as *mut libc::c_double;
            iwork = R_alloc(
                maxmaxp as size_t,
                ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
            ) as *mut libc::c_int;
            nseg = LENGTH_EX(
                z,
                b"fourier.c\x00" as *const u8 as *const libc::c_char,
                117 as libc::c_int,
            );
            n = 1 as libc::c_int;
            nspn = 1 as libc::c_int;
            i = 0 as libc::c_int;
            while i < ndims {
                if *(DATAPTR(d) as *mut libc::c_int).offset(i as isize) > 1 as libc::c_int {
                    nspn *= n;
                    n = *(DATAPTR(d) as *mut libc::c_int).offset(i as isize);
                    nseg /= n;
                    fft_factor(n, &mut maxf, &mut maxp);
                    fft_work(
                        &mut (*((DATAPTR as unsafe extern "C" fn(_: SEXP) -> *mut libc::c_void)(z)
                            as *mut Rcomplex)
                            .offset(0 as libc::c_int as isize))
                        .r,
                        &mut (*((DATAPTR as unsafe extern "C" fn(_: SEXP) -> *mut libc::c_void)(z)
                            as *mut Rcomplex)
                            .offset(0 as libc::c_int as isize))
                        .i,
                        nseg,
                        n,
                        nspn,
                        inv,
                        work,
                        iwork,
                    );
                }
                i += 1
            }
        }
    }
    unprotect(1 as libc::c_int);
    return z;
}
/* Fourier Transform for Vector-Valued ("multivariate") Series */
/* Not to be confused with the spatial case (in do_fft). */
#[no_mangle]
pub unsafe extern "C" fn mvfft(mut z: SEXP, mut inverse: SEXP) -> SEXP {
    let mut d: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut inv: libc::c_int = 0;
    let mut maxf: libc::c_int = 0;
    let mut maxp: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut p: libc::c_int = 0;
    let mut work: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut iwork: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut smaxf: size_t = 0;
    let mut maxsize: size_t =
        (-(1 as libc::c_int) as size_t).wrapping_div(4 as libc::c_int as libc::c_ulong);
    d = getAttrib(z, R_DimSymbol);
    if d == R_NilValue || length(d) > 2 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"vector-valued (multivariate) series required\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    n = *(DATAPTR(d) as *mut libc::c_int).offset(0 as libc::c_int as isize);
    p = *(DATAPTR(d) as *mut libc::c_int).offset(1 as libc::c_int as isize);
    match (*z).sxpinfo.type_0() as libc::c_int {
        13 | 10 | 14 => z = coerceVector(z, 15 as libc::c_int as SEXPTYPE),
        15 => {
            if !((*z).sxpinfo.named() as libc::c_int == 0 as libc::c_int) {
                z = duplicate(z)
            }
        }
        _ => {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"non-numeric argument\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
    }
    protect(z);
    /* -2 for forward  transform, complex values */
    /* +2 for backward transform, complex values */
    inv = asLogical(inverse);
    if inv == R_NaInt || inv == 0 as libc::c_int {
        inv = -(2 as libc::c_int)
    } else {
        inv = 2 as libc::c_int
    }
    if n > 1 as libc::c_int {
        fft_factor(n, &mut maxf, &mut maxp);
        if maxf == 0 as libc::c_int {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"fft factorization error\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        smaxf = maxf as size_t;
        if smaxf > maxsize {
            error(b"fft too large\x00" as *const u8 as *const libc::c_char);
        }
        work = R_alloc(
            (4 as libc::c_int as libc::c_ulong).wrapping_mul(smaxf),
            ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_double;
        iwork = R_alloc(
            maxp as size_t,
            ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_int;
        i = 0 as libc::c_int;
        while i < p {
            fft_factor(n, &mut maxf, &mut maxp);
            fft_work(
                &mut (*((DATAPTR as unsafe extern "C" fn(_: SEXP) -> *mut libc::c_void)(z)
                    as *mut Rcomplex)
                    .offset((i * n) as isize))
                .r,
                &mut (*((DATAPTR as unsafe extern "C" fn(_: SEXP) -> *mut libc::c_void)(z)
                    as *mut Rcomplex)
                    .offset((i * n) as isize))
                .i,
                1 as libc::c_int,
                n,
                1 as libc::c_int,
                inv,
                work,
                iwork,
            );
            i += 1
        }
    }
    unprotect(1 as libc::c_int);
    return z;
}
unsafe extern "C" fn ok_n(
    mut n: libc::c_int,
    mut f: *const libc::c_int,
    mut nf: libc::c_int,
) -> Rboolean {
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < nf {
        while n % *f.offset(i as isize) == 0 as libc::c_int {
            n = n / *f.offset(i as isize);
            if n == 1 as libc::c_int {
                return TRUE;
            }
        }
        i += 1
    }
    return (n == 1 as libc::c_int) as libc::c_int as Rboolean;
}
unsafe extern "C" fn ok_n_64(
    mut n: uint64_t,
    mut f: *const libc::c_int,
    mut nf: libc::c_int,
) -> Rboolean {
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < nf {
        while n.wrapping_rem(*f.offset(i as isize) as libc::c_ulong)
            == 0 as libc::c_int as libc::c_ulong
        {
            n = n.wrapping_div(*f.offset(i as isize) as libc::c_ulong);
            if n == 1 as libc::c_int as libc::c_ulong {
                return TRUE;
            }
        }
        i += 1
    }
    return (n == 1 as libc::c_int as libc::c_ulong) as libc::c_int as Rboolean;
}
unsafe extern "C" fn nextn0(
    mut n: libc::c_int,
    mut f: *const libc::c_int,
    mut nf: libc::c_int,
) -> libc::c_int {
    while ok_n(n, f, nf) as u64 == 0 && n < 2147483647 as libc::c_int {
        n += 1
    }
    if n >= 2147483647 as libc::c_int {
        warning(dcgettext(b"stats\x00" as *const u8 as *const libc::c_char,
                             b"nextn() found no solution < %d = INT_MAX (the maximal integer); pass \'0+ n\' instead of \'n\'\x00"
                                 as *const u8 as *const libc::c_char,
                             5 as libc::c_int), 2147483647 as libc::c_int);
        return R_NaInt;
    } else {
        return n;
    };
}
unsafe extern "C" fn nextn0_64(
    mut n: uint64_t,
    mut f: *const libc::c_int,
    mut nf: libc::c_int,
) -> uint64_t {
    while ok_n_64(n, f, nf) as u64 == 0 && (n as u64) < 18446744073709551615 {
        n = n.wrapping_add(1)
    }
    if n as u64 >= 18446744073709551615 {
        // or give an error?  previously was much more problematic
        warning(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"nextn<64>() found no solution < %ld = UINT64_MAX (the maximal integer)\x00"
                    as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            18446744073709551615_u64,
        );
        return 0 as libc::c_int as uint64_t;
    // no NA for this type
    } else {
        return n;
    };
}
#[no_mangle]
pub unsafe extern "C" fn nextn(mut n: SEXP, mut f: SEXP) -> SEXP {
    if (*n).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int {
        // NULL <==> integer(0) :
        return allocVector(13 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t);
    }
    let mut nprot: libc::c_int = 0 as libc::c_int;
    if (*f).sxpinfo.type_0() as libc::c_int != 13 as libc::c_int {
        f = coerceVector(f, 13 as libc::c_int as SEXPTYPE);
        protect(f);
        nprot += 1
    }
    let mut nf: libc::c_int = LENGTH_EX(
        f,
        b"fourier.c\x00" as *const u8 as *const libc::c_char,
        246 as libc::c_int,
    );
    let mut f_: *mut libc::c_int = DATAPTR(f) as *mut libc::c_int;
    /* check the factors */
    if nf == 0 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"no factors\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        )); // < 0 : from integer overflow
    } // := max_i n[i]
    if nf < 0 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"too many factors\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < nf {
        if *f_.offset(i as isize) == R_NaInt || *f_.offset(i as isize) <= 1 as libc::c_int {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"invalid factors\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        i += 1
    }
    let mut use_int: Rboolean =
        ((*n).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int) as libc::c_int as Rboolean;
    if use_int as u64 == 0 && (*n).sxpinfo.type_0() as libc::c_int != 14 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"\'n\' must have typeof(.) \"integer\" or \"double\"\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    let mut nn: R_xlen_t = XLENGTH_EX(n);
    if use_int as u64 == 0 && nn != 0 {
        let mut d_n: *mut libc::c_double = DATAPTR(n) as *mut libc::c_double;
        let mut n_max: libc::c_double = -(1 as libc::c_int) as libc::c_double;
        let mut i_0: R_xlen_t = 0 as libc::c_int as R_xlen_t;
        while i_0 < nn {
            if !((*d_n.offset(i_0 as isize)).is_nan() as i32 != 0 as libc::c_int)
                && *d_n.offset(i_0 as isize) > n_max
            {
                n_max = *d_n.offset(i_0 as isize)
            }
            i_0 += 1
        }
        if n_max
            <= (2147483647 as libc::c_int / *f_.offset(0 as libc::c_int as isize)) as libc::c_double
        {
            // maximal n[] should not be too large to find "next n"
            use_int = TRUE; // use "double" (as R has no int64 ..)
            n = coerceVector(n, 13 as libc::c_int as SEXPTYPE); // = 2^53
            n = protect(n);
            nprot += 1
        }
    }
    let mut ans: SEXP = protect(allocVector(
        if use_int as libc::c_uint != 0 {
            13 as libc::c_int
        } else {
            14 as libc::c_int
        } as SEXPTYPE,
        nn,
    ));
    nprot += 1;
    if nn == 0 as libc::c_int as libc::c_long {
        return ans;
    }
    if use_int as u64 != 0 {
        let mut n_: *mut libc::c_int = DATAPTR(n) as *mut libc::c_int;
        let mut r: *mut libc::c_int = DATAPTR(ans) as *mut libc::c_int;
        let mut i_1: R_xlen_t = 0 as libc::c_int as R_xlen_t;
        while i_1 < nn {
            if *n_.offset(i_1 as isize) == R_NaInt {
                *r.offset(i_1 as isize) = R_NaInt
            } else if *n_.offset(i_1 as isize) <= 1 as libc::c_int {
                *r.offset(i_1 as isize) = 1 as libc::c_int
            } else {
                *r.offset(i_1 as isize) =
                    nextn0(*n_.offset(i_1 as isize), f_ as *const libc::c_int, nf)
            }
            i_1 += 1
        }
    } else {
        let mut n__0: *mut libc::c_double = DATAPTR(n) as *mut libc::c_double;
        let mut r_0: *mut libc::c_double = DATAPTR(ans) as *mut libc::c_double;
        let mut i_2: R_xlen_t = 0 as libc::c_int as R_xlen_t;
        while i_2 < nn {
            if (*n__0.offset(i_2 as isize)).is_nan() as i32 != 0 as libc::c_int {
                *r_0.offset(i_2 as isize) = R_NaReal
            } else if *n__0.offset(i_2 as isize) <= 1 as libc::c_int as libc::c_double {
                *r_0.offset(i_2 as isize) = 1 as libc::c_int as libc::c_double
            } else {
                let max_dbl_int = 9007199254740992;
                let mut n_n: uint64_t = nextn0_64(
                    *n__0.offset(i_2 as isize) as uint64_t,
                    f_ as *const libc::c_int,
                    nf,
                );
                if n_n as u64 > max_dbl_int {
                    warning(dcgettext(b"stats\x00" as *const u8 as
                                             *const libc::c_char,
                                         b"nextn() = %lu > 2^53 may not be exactly representable in R (as \"double\")\x00"
                                             as *const u8 as
                                             *const libc::c_char,
                                         5 as libc::c_int), n_n);
                }
                *r_0.offset(i_2 as isize) = n_n as libc::c_double
            }
            i_2 += 1
        }
    }
    unprotect(nprot);
    return ans;
}
