use ::libc;
extern "C" {
    #[no_mangle]
    fn sqrt(_: libc::c_double) -> libc::c_double;

    /* True for special NaN, *not* for NA */
    #[no_mangle]
    fn R_finite(_: libc::c_double) -> libc::c_int;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2016    The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn Rprintf(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn REprintf(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn sgram_(
        sg0: *mut libc::c_double,
        sg1: *mut libc::c_double,
        sg2: *mut libc::c_double,
        sg3: *mut libc::c_double,
        tb: *mut libc::c_double,
        nb: *mut libc::c_int,
    );
    #[no_mangle]
    fn stxwx_(
        x: *mut libc::c_double,
        z: *mut libc::c_double,
        w: *mut libc::c_double,
        k: *mut libc::c_int,
        xknot: *mut libc::c_double,
        n: *mut libc::c_int,
        y: *mut libc::c_double,
        hs0: *mut libc::c_double,
        hs1: *mut libc::c_double,
        hs2: *mut libc::c_double,
        hs3: *mut libc::c_double,
    );
    #[no_mangle]
    fn sslvrg_(
        penalt: *mut libc::c_double,
        dofoff: *mut libc::c_double,
        x: *mut libc::c_double,
        y: *mut libc::c_double,
        w: *mut libc::c_double,
        ssw: *mut libc::c_double,
        n: *mut libc::c_int,
        knot: *mut libc::c_double,
        nk: *mut libc::c_int,
        coef: *mut libc::c_double,
        sz: *mut libc::c_double,
        lev: *mut libc::c_double,
        crit: *mut libc::c_double,
        icrit: *mut libc::c_int,
        lambda: *mut libc::c_double,
        xwy: *mut libc::c_double,
        hs0: *mut libc::c_double,
        hs1: *mut libc::c_double,
        hs2: *mut libc::c_double,
        hs3: *mut libc::c_double,
        sg0: *mut libc::c_double,
        sg1: *mut libc::c_double,
        sg2: *mut libc::c_double,
        sg3: *mut libc::c_double,
        abd: *mut libc::c_double,
        p1ip: *mut libc::c_double,
        p2ip: *mut libc::c_double,
        ld4: *mut libc::c_int,
        ldnk: *mut libc::c_int,
        info: *mut libc::c_int,
    );
    /* R's versions with !R_FINITE checks */
    #[no_mangle]
    fn R_pow(x: libc::c_double, y: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn fsign(_: libc::c_double, _: libc::c_double) -> libc::c_double;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
/*, MAYBE */
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2001-2017 The R Core Team.
 *  Copyright (C) 2003-2016 The R Foundation
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* monoSpl.c : */
/* Fortran : */
/* sbart.f -- translated by f2c (version 20010821).
 * ------- and f2c-clean,v 1.9 2000/01/13
 *
 * According to the GAMFIT sources, this was derived from code by
 * Finbarr O'Sullivan.
 */
/* sbart() : The cubic spline smoother
   -------
 Calls  sgram (sg0,sg1,sg2,sg3,knot,nk)
     stxwx (xs,ys,ws,n,knot,nk,xwy,hs0,hs1,hs2,hs3)
     sslvrg (penalt,dofoff,xs,ys,ws,ssw,n,knot,nk, coef,sz,lev,crit,icrit,
         lambda, xwy, hs0,hs1,hs2,hs3, sg0,sg1,sg2,sg3,
         abd,p1ip,p2ip,ld4,ldnk,ier)

 is itself called from  qsbart() [./qsbart.f]  which has only one work array

 Now allows to pass 'lambda' (not just 'spar') via spar[0] == *spar  iff  *isetup = 2
*/
#[no_mangle]
pub unsafe extern "C" fn sbart_(
    mut penalt: *mut libc::c_double,
    mut dofoff: *mut libc::c_double,
    mut xs: *mut libc::c_double,
    mut ys: *mut libc::c_double,
    mut ws: *mut libc::c_double,
    mut ssw: *mut libc::c_double,
    mut n: *mut libc::c_int,
    mut knot: *mut libc::c_double,
    mut nk: *mut libc::c_int,
    mut coef: *mut libc::c_double,
    mut sz: *mut libc::c_double,
    mut lev: *mut libc::c_double,
    mut crit: *mut libc::c_double,
    mut icrit: *mut libc::c_int,
    mut spar: *mut libc::c_double,
    mut ispar: *mut libc::c_int,
    mut iter: *mut libc::c_int,
    mut lspar: *mut libc::c_double,
    mut uspar: *mut libc::c_double,
    mut tol: *mut libc::c_double,
    mut eps: *mut libc::c_double,
    mut Ratio: *mut libc::c_double,
    mut isetup: *mut libc::c_int,
    mut xwy: *mut libc::c_double,
    mut hs0: *mut libc::c_double,
    mut hs1: *mut libc::c_double,
    mut hs2: *mut libc::c_double,
    mut hs3: *mut libc::c_double,
    mut sg0: *mut libc::c_double,
    mut sg1: *mut libc::c_double,
    mut sg2: *mut libc::c_double,
    mut sg3: *mut libc::c_double,
    mut abd: *mut libc::c_double,
    mut p1ip: *mut libc::c_double,
    mut p2ip: *mut libc::c_double,
    mut ld4: *mut libc::c_int,
    mut ldnk: *mut libc::c_int,
    mut ier: *mut libc::c_int,
) {
    let mut current_block: u64;
    /* A Cubic B-spline Smoothing routine.

       The algorithm minimises:

        (1/n) * sum ws(i)^2 * (ys(i)-sz(i))^2 + lambda* int ( s"(x) )^2 dx

       lambda is a function of the spar which is assumed to be between 0 and 1

     INPUT
     -----
       penalt A penalty > 1 to be used in the gcv criterion
       dofoff either `df.offset' for GCV or `df' (to be matched).
       n  number of data points
       ys(n) vector of length n containing the observations
       ws(n) vector containing the weights given to each data point
            NB: the code alters the values here.
       xs(n) vector containing the ordinates of the observations
       ssw          `centered weighted sum of y^2'
       nk  number of b-spline coefficients to be estimated
            nk <= n+2
       knot(nk+4) vector of knot points defining the cubic b-spline basis.
            To obtain full cubic smoothing splines one might
            have (provided the xs-values are strictly increasing)
       spar  penalised likelihood smoothing parameter
       ispar indicating if spar is supplied (ispar=1) or to be estimated
       lspar, uspar lower and upper values for spar search;  0.,1. are good values
       tol, eps used in Golden Search routine
       isetup setup indicator initially 0 or 2 (if 'spar' is lambda)
        NB: this alters that, and it is a constant in the caller!
       icrit indicator saying which cross validation score is to be computed
            0: none ;  1: GCV ;  2: CV ;  3: 'df matching'
       ld4  the leading dimension of abd (ie ld4=4)
       ldnk  the leading dimension of p2ip (not referenced)

     OUTPUT
     ------
       coef(nk) vector of spline coefficients
       sz(n) vector of smoothed z-values
       lev(n) vector of leverages
       crit  either ordinary or generalized CV score
       spar         if ispar != 1
       lspar         == lambda (a function of spar and the design if(setup != 1)
       iter  number of iterations needed for spar search (if ispar != 1)
       ier  error indicator
            ier = 0 ___  everything fine
            ier = 1 ___  spar too small or too big
                problem in cholesky decomposition

     Working arrays/matrix
       xwy   X'Wy
       hs0,hs1,hs2,hs3 the non-zero diagonals of the X'WX matrix
       sg0,sg1,sg2,sg3 the non-zero diagonals of the Gram matrix SIGMA
       abd (ld4, nk) [ X'WX + lambda*SIGMA ] = R'R in banded form; output = R
       p1ip(ld4, nk) inner products between columns of R^{-1}
       p2ip(ldnk,nk) all inner products between columns of R inverse
                where  R'R = [X'WX + lambda*SIGMA]  NOT REFERENCED
    */
    // "Correct" ./sslvrg.f (line 129):   crit = 3 + (dofoff-df)**2
    /* cancellation in (3 + eps) - 3, but still...informative */
    /* c_Gold is the squared inverse of the golden ratio */

    static mut c_Gold: libc::c_double = 0.381966011250105151795413165634f64;
    /* == (3. - sqrt(5.)) / 2. */
    /* Local variables */
    static mut ratio: libc::c_double = 0.; /* must be static (not needed in R) */
    let mut a: libc::c_double = 0.;
    let mut b: libc::c_double = 0.;
    let mut d: libc::c_double = 0.;
    let mut e: libc::c_double = 0.;
    let mut p: libc::c_double = 0.;
    let mut q: libc::c_double = 0.;
    let mut r: libc::c_double = 0.;
    let mut u: libc::c_double = 0.;
    let mut v: libc::c_double = 0.;
    let mut w: libc::c_double = 0.;
    let mut x: libc::c_double = 0.;
    let mut ax: libc::c_double = 0.;
    let mut fu: libc::c_double = 0.;
    let mut fv: libc::c_double = 0.;
    let mut fw: libc::c_double = 0.;
    let mut fx: libc::c_double = 0.;
    let mut bx: libc::c_double = 0.;
    let mut xm: libc::c_double = 0.;
    let mut tol1: libc::c_double = 0.;
    let mut tol2: libc::c_double = 0.;
    let mut i: libc::c_int = 0;
    let mut maxit: libc::c_int = 0;
    let mut Fparabol: Rboolean = FALSE;
    let mut tracing: Rboolean = (*ispar < 0 as libc::c_int) as libc::c_int as Rboolean;
    let mut spar_is_lambda: Rboolean = FALSE;
    /* unnecessary initializations to keep  -Wall happy */
    d = 0.0f64;
    fu = 0.0f64;
    u = 0.0f64;
    // never computed if(spar_is_lambda)
    ratio = 1.0f64;
    /*  Compute SIGMA, X' W X, X' W z, trace ratio, s0, s1.

        SIGMA -> sg0,sg1,sg2,sg3   -- via sgram() in ./sgram.f
        X' W X -> hs0,hs1,hs2,hs3   \
        X' W Z -> xwy               _\ via stxwx() in ./stxwx.f
    */
    /* trevor fixed this 4/19/88
     * Note: sbart, i.e. stxwx() and sslvrg() {mostly, not always!}, use
     *  the square of the weights; the following rectifies that */
    i = 0 as libc::c_int;
    while i < *n {
        if *ws.offset(i as isize) > 0.0f64 {
            *ws.offset(i as isize) = sqrt(*ws.offset(i as isize))
        }
        i += 1
    }
    if *isetup < 0 as libc::c_int {
        spar_is_lambda = TRUE
    } else if *isetup != 1 as libc::c_int {
        // 0 or 2
        /* SIGMA[i,j] := Int  B''(i,t) B''(j,t) dt  {B(k,.) = k-th B-spline} */
        sgram_(sg0, sg1, sg2, sg3, knot, nk);
        stxwx_(xs, ys, ws, n, knot, nk, xwy, hs0, hs1, hs2, hs3);
        spar_is_lambda = (*isetup == 2 as libc::c_int) as libc::c_int as Rboolean;
        if spar_is_lambda as u64 == 0 {
            /* Compute ratio :=  tr(X' W X) / tr(SIGMA) */
            let mut t1: libc::c_double = 0.0f64;
            let mut t2: libc::c_double = 0.0f64;
            i = 3 as libc::c_int - 1 as libc::c_int;
            while i < *nk - 3 as libc::c_int {
                t1 += *hs0.offset(i as isize);
                t2 += *sg0.offset(i as isize);
                i += 1
            }
            ratio = t1 / t2
        }
        *isetup = 1 as libc::c_int
    }
    /*     Compute estimate */
    // Compute SSPLINE(SPAR), assign result to *crit (and the auxil.variables)
    if *ispar == 1 as libc::c_int {
        /* Value of spar supplied */
        *lspar = if spar_is_lambda as libc::c_uint != 0 {
            *spar
        } else {
            (ratio) * R_pow(16.0f64, *spar * 6.0f64 - 2.0f64)
        };
        sslvrg_(
            penalt, dofoff, xs, ys, ws, ssw, n, knot, nk, coef, sz, lev, crit, icrit, lspar, xwy,
            hs0, hs1, hs2, hs3, sg0, sg1, sg2, sg3, abd, p1ip, p2ip, ld4, ldnk, ier,
        );
        /* got through check 2 */
        *Ratio = ratio;
        return;
    }
    /* ELSE ---- spar not supplied --> compute it ! ---------------------------
     */
    ax = *lspar;
    bx = *uspar;
    /*
           Use Forsythe Malcom and Moler routine to MINIMIZE criterion
           f denotes the value of the criterion

           an approximation x  to the point where f  attains a minimum  on
           the interval  (ax,bx)  is determined.


       INPUT

       ax  left endpoint of initial interval
       bx  right endpoint of initial interval
       f  function subprogram which evaluates  f(x)  for any  x
         in the interval  (ax,bx)
       tol  desired length of the interval of uncertainty of the final
         result ( >= 0 )

       OUTPUT

       fmin  abcissa approximating the point where f  attains a minimum
    */
    /*
       The method used is a combination of  golden  section  search  and
       successive parabolic interpolation. convergence is never much slower
       than  that  for  a  fibonacci search.  if  f  has a continuous second
       derivative which is positive at the minimum (which is not  at  ax  or
       bx),  then  convergence  is superlinear, and usually of the order of
       about  1.324....
        the function  f  is never evaluated at two points closer together
       than  eps*abs(fmin) + (tol/3), where eps is approximately the square
       root  of  the  relative  machine  precision.   if   f   is a unimodal
       function and the computed values of  f   are  always  unimodal  when
       separated by at least  eps*abs(x) + (tol/3), then  fmin  approximates
       the abcissa of the global minimum of  f  on the interval  ax,bx  with
       an error less than  3*eps*abs(fmin) + tol.  if   f is not unimodal,
       then fmin may approximate a local, but perhaps non-global, minimum to
       the same accuracy.
        this function subprogram is a slightly modified version  of  the
       algol  60 procedure localmin  given in richard brent, algorithms for
       minimization without derivatives, prentice - hall, inc. (1973).

       Double  a,b,c,d,e,eps,xm,p,q,r,tol1,tol2,u,v,w
       Double  fu,fv,fw,fx,x
    */
    /*  eps is approximately the square root of the relative machine
        precision.

        -  eps = 1e0
        - 10  eps = eps/2e0
        -  tol1 = 1e0 + eps
        -  if (tol1 > 1e0) go to 10
        -  eps = sqrt(eps)
        R Version <= 1.3.x had
        eps = .000244     ( = sqrt(5.954 e-8) )
         -- now eps is passed as argument
    */
    /* initialization */
    maxit = *iter;
    *iter = 0 as libc::c_int;
    a = ax;
    b = bx;
    v = a + c_Gold * (b - a);
    w = v;
    x = v;
    e = 0.0f64;
    *lspar = if spar_is_lambda as libc::c_uint != 0 {
        x
    } else {
        (ratio) * R_pow(16.0f64, x * 6.0f64 - 2.0f64)
    };
    sslvrg_(
        penalt, dofoff, xs, ys, ws, ssw, n, knot, nk, coef, sz, lev, crit, icrit, lspar, xwy, hs0,
        hs1, hs2, hs3, sg0, sg1, sg2, sg3, abd, p1ip, p2ip, ld4, ldnk, ier,
    );
    fx = *crit;
    fv = fx;
    fw = fx;
    /* main loop
    --------- */
    while *ier == 0 as libc::c_int {
        /* end main loop -- goto L20; */
        /* L20: */
        xm = (a + b) * 0.5f64;
        tol1 = *eps * x.abs() + *tol / 3.0f64;
        tol2 = tol1 * 2.0f64;
        *iter += 1;
        if tracing as u64 != 0 {
            if *iter == 1 as libc::c_int {
                /* write header */
                Rprintf(b"sbart (ratio = %15.8g) iterations; initial tol1 = %12.6e :\n%11s %14s  %9s %11s  Kind %11s %12s\n%s\n\x00"
                            as *const u8 as *const libc::c_char, ratio, tol1,
                        b"spar\x00" as *const u8 as *const libc::c_char,
                        if *icrit == 1 as libc::c_int {
                            b"GCV\x00" as *const u8 as *const libc::c_char
                        } else if *icrit == 2 as libc::c_int {
                            b"CV\x00" as *const u8 as *const libc::c_char
                        } else if *icrit == 3 as libc::c_int {
                            b"(df0-df)^2\x00" as *const u8 as
                                *const libc::c_char
                        } else {
                            b"?f?\x00" as *const u8 as *const libc::c_char
                        }, b"b - a\x00" as *const u8 as *const libc::c_char,
                        b"e\x00" as *const u8 as *const libc::c_char,
                        b"NEW lspar\x00" as *const u8 as *const libc::c_char,
                        b"crit\x00" as *const u8 as *const libc::c_char,
                        b" -------------------------------------------------------------------------------\x00"
                            as *const u8 as *const libc::c_char);
            }
            Rprintf(
                b"%11.8f %14.9g %9.4e %11.5g\x00" as *const u8 as *const libc::c_char,
                x,
                if *icrit == 3 as libc::c_int {
                    (fx) - 3.0f64
                } else {
                    fx
                },
                b - a,
                e,
            );
            Fparabol = FALSE
        }
        /* Check the (somewhat peculiar) stopping criterion: note that
        the RHS is negative as long as the interval [a,b] is not small:*/
        if (x - xm).abs() <= tol2 - (b - a) * 0.5f64 || *iter > maxit {
            break;
        }
        /* is golden-section necessary */
        if e.abs() <= tol1 || fx >= 1e100f64 || fv >= 1e100f64 || fw >= 1e100f64 {
            current_block = 11783109083342941207;
        } else {
            /* Fit Parabola */
            if tracing as u64 != 0 {
                Rprintf(b" FP\x00" as *const u8 as *const libc::c_char);
                Fparabol = TRUE
            }
            r = (x - w) * (fx - fv);
            q = (x - v) * (fx - fw);
            p = (x - v) * q - (x - w) * r;
            q = (q - r) * 2.0f64;
            if q > 0.0f64 {
                p = -p
            }
            q = q.abs();
            r = e;
            e = d;
            /* is parabola acceptable?  Otherwise do golden-section */
            if p.abs() >= (0.5f64 * q * r).abs() || q == 0.0f64 {
                /* above line added by BDR;
                 * [the abs(.) >= abs() = 0 should have branched..]
                 * in FTN: COMMON above ensures q is NOT a register variable */
                current_block = 11783109083342941207;
            } else if p <= q * (a - x) || p >= q * (b - x) {
                current_block = 11783109083342941207;
            } else {
                /* Parabolic Interpolation step */
                if tracing as u64 != 0 {
                    Rprintf(b" PI \x00" as *const u8 as *const libc::c_char);
                }
                d = p / q;
                if R_finite(d) == 0 {
                    REprintf(
                        b" !FIN(d:=p/q): ier=%d, (v,w, p,q)= %g, %g, %g, %g\n\x00" as *const u8
                            as *const libc::c_char,
                        *ier,
                        v,
                        w,
                        p,
                        q,
                    );
                }
                u = x + d;
                /* f must not be evaluated too close to ax or bx */
                if u - a < tol2 || b - u < tol2 {
                    d = fsign(tol1, xm - x)
                }
                current_block = 388264277043112403;
            }
        }
        match current_block {
            11783109083342941207 =>
            /*------*/
            /* a golden-section step */
            {
                if tracing as u64 != 0 {
                    Rprintf(
                        b" GS%s \x00" as *const u8 as *const libc::c_char,
                        if Fparabol as libc::c_uint != 0 {
                            b"\x00" as *const u8 as *const libc::c_char
                        } else {
                            b" --\x00" as *const u8 as *const libc::c_char
                        },
                    );
                }
                if x >= xm {
                    e = a - x
                } else {
                    /* x < xm*/
                    e = b - x
                }
                d = c_Gold * e
            }
            _ => {}
        }
        u = x + (if d.abs() >= tol1 { d } else { fsign(tol1, d) });
        /*  tol1 check : f must not be evaluated too close to x */
        *lspar = if spar_is_lambda as libc::c_uint != 0 {
            u
        } else {
            (ratio) * R_pow(16.0f64, u * 6.0f64 - 2.0f64)
        };
        sslvrg_(
            penalt, dofoff, xs, ys, ws, ssw, n, knot, nk, coef, sz, lev, crit, icrit, lspar, xwy,
            hs0, hs1, hs2, hs3, sg0, sg1, sg2, sg3, abd, p1ip, p2ip, ld4, ldnk, ier,
        );
        fu = *crit;
        if tracing as u64 != 0 {
            Rprintf(
                b"%11g %12g\n\x00" as *const u8 as *const libc::c_char,
                *lspar,
                if *icrit == 3 as libc::c_int {
                    (fu) - 3.0f64
                } else {
                    fu
                },
            );
        }
        if R_finite(fu) == 0 {
            REprintf(
                b"spar-finding: non-finite value %g; using BIG value\n\x00" as *const u8
                    as *const libc::c_char,
                fu,
            );
            fu = 2.0f64 * 1e100f64
        }
        /*  update  a, b, v, w, and x */
        if fu <= fx {
            if u >= x {
                a = x
            } else {
                b = x
            }
            v = w;
            fv = fw;
            w = x;
            fw = fx;
            x = u;
            fx = fu
        } else {
            if u < x {
                a = u
            } else {
                b = u
            }
            if fu <= fw || w == x {
                /* L70: */
                v = w;
                fv = fw;
                w = u;
                fw = fu
            } else if fu <= fv || v == x || v == w {
                /* L80: */
                v = u;
                fv = fu
            }
        }
    }
    if tracing as u64 != 0 {
        Rprintf(
            b"  >>> %12g %12g\n\x00" as *const u8 as *const libc::c_char,
            *lspar,
            if *icrit == 3 as libc::c_int {
                (fx) - 3.0f64
            } else {
                fx
            },
        );
    }
    *Ratio = ratio;
    *spar = x;
    *crit = fx;
}
/* sbart */
