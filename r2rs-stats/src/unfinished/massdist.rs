use crate::sexprec::{SEXP, SEXPTYPE};
use ::libc;
extern "C" {
    #[no_mangle]
    static mut R_NaInt: libc::c_int;
    #[no_mangle]
    fn XLENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    /* Defining NO_RINLINEDFUNS disables use to simulate platforms where
    this is not available */
    /* need remapped names here for use with R_NO_REMAP */
    /*
       These are the inlinable functions that are provided in Rinlinedfuns.h
       It is *essential* that these do not appear in any other header file,
       with or without the Rf_ prefix.
    */
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
}
pub type ptrdiff_t = libc::c_long;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
/* real variables */
/* complex variables */
/* string vectors */
/* dot-dot-dot object */
/* make "any" args work.
Used in specifying types for symbol
registration to mean anything is okay  */
/* generic vectors */
/* expressions vectors */
/* byte code */
/* external pointer */
/* weak reference */
/* raw bytes */
/* S4, non-vector */
/* used for detecting PROTECT issues in memory.c */
/* fresh node created in new page */
/* node released by GC */
/* Closure or Builtin or Special */
/* NOT YET */
/* These are also used with the write barrier on, in attrib.c and util.c */
/*
*  R : A Computer Language for Statistical Data Analysis
*  Copyright (C) 1996-2012 The R Core Team
*  Copyright (C) 2005  The R Foundation

*  "HACKED" to allow weights by Adrian Baddeley
*  Changes indicated by 'AB'
* -------
*  FIXME   Does he want 'COPYRIGHT' ?
* -------
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, a copy is available at
*  https://www.R-project.org/Licenses/
*/
/* NB: this only works in the lower half of y, but pads with zeros. */
#[no_mangle]
pub unsafe extern "C" fn BinDist(
    mut sx: SEXP,
    mut sw: SEXP,
    mut slo: SEXP,
    mut shi: SEXP,
    mut sn: SEXP,
) -> SEXP {
    sx = coerceVector(sx, 14 as libc::c_int as SEXPTYPE);
    protect(sx);
    sw = coerceVector(sw, 14 as libc::c_int as SEXPTYPE);
    protect(sw);
    let mut n: libc::c_int = asInteger(sn);
    if n == R_NaInt || n <= 0 as libc::c_int {
        error(
            b"invalid \'%s\' argument\x00" as *const u8 as *const libc::c_char,
            b"n\x00" as *const u8 as *const libc::c_char,
        );
    }
    let mut ans: SEXP = allocVector(
        14 as libc::c_int as SEXPTYPE,
        (2 as libc::c_int * n) as R_xlen_t,
    );
    protect(ans);
    let mut xlo: libc::c_double = asReal(slo);
    let mut xhi: libc::c_double = asReal(shi);
    let mut x: *mut libc::c_double = REAL(sx);
    let mut w: *mut libc::c_double = REAL(sw);
    let mut y: *mut libc::c_double = REAL(ans);
    let mut ixmin: libc::c_int = 0 as libc::c_int;
    let mut ixmax: libc::c_int = n - 2 as libc::c_int;
    let mut xdelta: libc::c_double = (xhi - xlo) / (n - 1 as libc::c_int) as libc::c_double;
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < 2 as libc::c_int * n {
        *y.offset(i as isize) = 0 as libc::c_int as libc::c_double;
        i += 1
    }
    let mut i_0: R_xlen_t = 0 as libc::c_int as R_xlen_t;
    while i_0 < XLENGTH(sx) {
        if (*x.offset(i_0 as isize)).is_finite() as i32 != 0 {
            let mut xpos: libc::c_double = (*x.offset(i_0 as isize) - xlo) / xdelta;
            let mut ix: libc::c_int = xpos.floor() as libc::c_int;
            let mut fx: libc::c_double = xpos - ix as libc::c_double;
            let mut wi: libc::c_double = *w.offset(i_0 as isize);
            if ixmin <= ix && ix <= ixmax {
                *y.offset(ix as isize) += (1 as libc::c_int as libc::c_double - fx) * wi;
                *y.offset((ix + 1 as libc::c_int) as isize) += fx * wi
            } else if ix == -(1 as libc::c_int) {
                *y.offset(0 as libc::c_int as isize) += fx * wi
            } else if ix == ixmax + 1 as libc::c_int {
                *y.offset(ix as isize) += (1 as libc::c_int as libc::c_double - fx) * wi
            }
        }
        i_0 += 1
    }
    unprotect(3 as libc::c_int);
    return ans;
}
