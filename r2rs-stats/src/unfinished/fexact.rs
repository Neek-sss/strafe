use crate::sexprec::{SEXP, SEXPTYPE};
use ::libc;
extern "C" {
    #[no_mangle]
    fn sprintf(_: *mut libc::c_char, _: *const libc::c_char, _: ...) -> libc::c_int;

    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn REprintf(_: *const libc::c_char, _: ...);
    /*
    *  R : A Computer Language for Statistical Data Analysis
    *  Copyright (C) 1998-2017    The R Core Team
    *
    *  This header file is free software; you can redistribute it and/or modify
    *  it under the terms of the GNU Lesser General Public License as published by
    *  the Free Software Foundation; either version 2.1 of the License, or
    *  (at your option) any later version.

    *  This file is part of R. R is distributed under the terms of the
    *  GNU General Public License, either Version 2, June 1991 or Version 3,
    *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
    *
    *  This program is distributed in the hope that it will be useful,
    *  but WITHOUT ANY WARRANTY; without even the implied warranty of
    *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    *  GNU Lesser General Public License for more details.
    *
    *  You should have received a copy of the GNU Lesser General Public License
    *  along with this program; if not, a copy is available at
    *  https://www.R-project.org/Licenses/
    *
    *
    * Generally useful  UTILITIES  *NOT* relying on R internals (from Defn.h)
    */
    /* Included by R.h: API */
    /* ../../main/sort.c : */
    #[no_mangle]
    fn R_isort(_: *mut libc::c_int, _: libc::c_int);
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    #[no_mangle]
    fn pgamma(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
        _: libc::c_int,
    ) -> libc::c_double;
    /* General Support Functions */
    #[no_mangle]
    fn imax2(_: libc::c_int, _: libc::c_int) -> libc::c_int;
    #[no_mangle]
    fn imin2(_: libc::c_int, _: libc::c_int) -> libc::c_int;
    #[no_mangle]
    fn fmax2(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn fmin2(_: libc::c_double, _: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn INTEGER(x: SEXP) -> *mut libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn ncols(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn nrows(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn ScalarReal(_: libc::c_double) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
}
pub type size_t = libc::c_ulong;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
/*, MAYBE */
/* The only public function : */
#[no_mangle]
pub unsafe extern "C" fn fexact(
    mut nrow: libc::c_int,
    mut ncol: libc::c_int,
    mut table: *const libc::c_int,
    mut ldtabl: libc::c_int,
    mut expect: libc::c_double,
    mut percnt: libc::c_double,
    mut emin: libc::c_double,
    mut prt: *mut libc::c_double,
    mut pre: *mut libc::c_double,
    mut workspace: libc::c_int,
    mut mult: libc::c_int,
) {
    /*
    ALGORITHM 643, COLLECTED ALGORITHMS FROM ACM.
    THIS WORK PUBLISHED IN TRANSACTIONS ON MATHEMATICAL SOFTWARE,
    VOL. 19, NO. 4, DECEMBER, 1993, PP. 484-488.
    -----------------------------------------------------------------------
    Name:       FEXACT
    Purpose:    Computes Fisher's exact test probabilities and a hybrid
            approximation to Fisher exact test probabilities for a
            contingency table using the network algorithm.

    Arguments:
      NROW    - The number of rows in the table.   (Input)
      NCOL    - The number of columns in the table.  (Input)
      TABLE   - NROW by NCOL matrix containing the contingency
            table.      (Input)
      LDTABL  - Leading dimension of TABLE exactly as specified
            in the dimension statement in the calling
            program.      (Input)
      EXPECT  - Expected value used in the hybrid algorithm for
            deciding when to use asymptotic theory
            probabilities.     (Input)
            If EXPECT <= 0.0 then asymptotic theory probabilities
            are not used and Fisher exact test probabilities are
            computed.  Otherwise, if PERCNT or more of the cells in
            the remaining table have estimated expected values of
            EXPECT or more, with no remaining cell having expected
            value less than EMIN, then asymptotic chi-squared
            probabilities are used.  See the algorithm section of the
            manual document for details.
            Use EXPECT = 5.0 to obtain the 'Cochran' condition.
      PERCNT  - Percentage of remaining cells that must have
            estimated expected values greater than EXPECT
            before asymptotic probabilities can be used. (Input)
            See argument EXPECT for details.
            Use PERCNT = 80.0 to obtain the 'Cochran' condition.
      EMIN    - Minimum cell estimated expected value allowed for
            asymptotic chi-squared probabilities to be used. (Input)
            See argument EXPECT for details.
            Use EMIN = 1.0 to obtain the 'Cochran' condition.
      PRT     - Probability of the observed table for fixed
            marginal totals.     (Output)
      PRE     - Table p-value.     (Output)
            PRE is the probability of a more extreme table,
            where `extreme' is in a probabilistic sense.
            If EXPECT < 0 then the Fisher exact probability
            is returned.  Otherwise, an approximation to the
            Fisher exact probability is computed based upon
            asymptotic chi-squared probabilities for ``large''
            table expected values.  The user defines ``large''
            through the arguments EXPECT, PERCNT, and EMIN.

    Remarks:
    1. For many problems one megabyte or more of workspace can be
       required. If the environment supports it, the user should begin
       by increasing the workspace used to 200,000 units.
    2. In FEXACT, LDSTP = MULT*LDKEY.  The proportion of table space used
       by STP may be changed by changing the line MULT = 30 below to
       another value. --> MULT is now an __argument__ of the function
    3. FEXACT may be converted to single precision by setting IREAL = 3,
       and converting all DOUBLE PRECISION specifications (except the
       specifications for RWRK, IWRK, and DWRK) to REAL. This will
       require changing the names and specifications of the intrinsic
       functions ALOG, AMAX1, AMIN1, EXP, and REAL.  In addition, the
       machine specific constants will need to be changed, and the name
       DWRK will need to be changed to RWRK in the call to F2XACT.
    4. Machine specific constants are specified and documented in F2XACT.
       A missing value code is specified in both FEXACT and F2XACT.
    5. Although not a restriction, is is not generally practical to call
       this routine with large tables which are not sparse and in
       which the 'hybrid' algorithm has little effect.  For example,
       although it is feasible to compute exact probabilities for the
       table
          1 8 5 4 4 2 2
          5 3 3 4 3 1 0
         10 1 4 0 0 0 0,
       computing exact probabilities for a similar table which has been
       enlarged by the addition of an extra row (or column) may not be
       feasible.
    -----------------------------------------------------------------------
    */
    /* CONSTANT Parameters : */
    /* To increase the length of the table of past path lengths relative
       to the length of the hash table, increase MULT.
    */
    /* AMISS is a missing value indicator which is returned when the
       probability is not defined.
    */
    let _amiss: libc::c_double = -12345.0f64;
    /*
      Set IREAL = 4 for DOUBLE PRECISION
      Set IREAL = 3 for SINGLE PRECISION
    */
    /* Local variables */
    let mut nco: libc::c_int = 0;
    let mut nro: libc::c_int = 0;
    let mut ntot: libc::c_int = 0;
    let mut numb: libc::c_int = 0;
    let mut iiwk: libc::c_int = 0;
    let mut irwk: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut kk: libc::c_int = 0;
    let mut ldkey: libc::c_int = 0;
    let mut ldstp: libc::c_int = 0;
    let mut i1: libc::c_int = 0;
    let mut i2: libc::c_int = 0;
    let mut i3: libc::c_int = 0;
    let mut i4: libc::c_int = 0;
    let mut i5: libc::c_int = 0;
    let mut i6: libc::c_int = 0;
    let mut i7: libc::c_int = 0;
    let mut i8: libc::c_int = 0;
    let mut i9: libc::c_int = 0;
    let mut i10: libc::c_int = 0;
    let mut i3a: libc::c_int = 0;
    let mut i3b: libc::c_int = 0;
    let mut i3c: libc::c_int = 0;
    let mut i9a: libc::c_int = 0;
    /* Workspace Allocation (freed when returning to R) */
    let mut equiv: *mut libc::c_double = 0 as *mut libc::c_double; // previously was 200 hard wired
    let mut iwkmax: libc::c_int = 2 as libc::c_int * (workspace / 2 as libc::c_int);
    let mut iwkpt: libc::c_int = 0 as libc::c_int;
    let mut n2_stack: libc::c_int = imax2(200 as libc::c_int, iwkmax / 1000 as libc::c_int);
    equiv = R_alloc(
        (iwkmax / 2 as libc::c_int) as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    /* Function Body */
    if nrow > ldtabl {
        prterr(
            1 as libc::c_int,
            b"NROW must be less than or equal to LDTABL.\x00" as *const u8 as *const libc::c_char,
        );
    }
    ntot = 0 as libc::c_int;
    i = 0 as libc::c_int;
    while i < nrow {
        j = 0 as libc::c_int;
        while j < ncol {
            if *table.offset((i + j * ldtabl) as isize) < 0 as libc::c_int {
                prterr(
                    2 as libc::c_int,
                    b"All elements of TABLE must be nonnegative.\x00" as *const u8
                        as *const libc::c_char,
                );
            }
            ntot += *table.offset((i + j * ldtabl) as isize);
            j += 1
        }
        i += 1
    }
    if ntot == 0 as libc::c_int {
        prterr(
            3 as libc::c_int,
            b"All elements of TABLE are zero.\nPRT and PRE are set to missing values.\x00"
                as *const u8 as *const libc::c_char,
        );
    }
    /* nco := max(nrow, ncol)
     * nro := min(nrow, ncol) */
    if ncol > nrow {
        nco = ncol;
        nro = nrow
    } else {
        nco = nrow;
        nro = ncol
    }
    k = nrow + ncol + 1 as libc::c_int;
    kk = k * nco;
    i1 = iwork(
        iwkmax,
        &mut iwkpt,
        ntot + 1 as libc::c_int,
        4 as libc::c_int,
    );
    i2 = iwork(iwkmax, &mut iwkpt, nco, 2 as libc::c_int);
    i3 = iwork(iwkmax, &mut iwkpt, nco, 2 as libc::c_int);
    i3a = iwork(iwkmax, &mut iwkpt, nco, 2 as libc::c_int);
    i3b = iwork(iwkmax, &mut iwkpt, nro, 2 as libc::c_int);
    i3c = iwork(iwkmax, &mut iwkpt, nro, 2 as libc::c_int);
    let mut ikh: libc::c_int = imax2(
        k * 5 as libc::c_int + (kk << 1 as libc::c_int),
        nco * 7 as libc::c_int + 4 as libc::c_int * n2_stack,
    );
    iiwk = iwork(iwkmax, &mut iwkpt, ikh, 2 as libc::c_int);
    ikh = imax2(nco + 1 as libc::c_int + 2 as libc::c_int * n2_stack, k);
    irwk = iwork(iwkmax, &mut iwkpt, ikh, 4 as libc::c_int);
    /* NOTE:
    What follows below splits the remaining amount iwkmax - iwkpt of
    (int) workspace into hash tables as follows.
    type  size       index
    INT   2 * ldkey  i4 i5 i10
    REAL  2 * ldkey  i8 i9 i9a
    REAL  2 * ldstp  i6
    INT   6 * ldstp  i7
    Hence, we need ldkey times
    3 * 2 + 3 * 2 * s + 2 * mult * s + 6 * mult
    chunks of integer memory, where s = sizeof(REAL) / sizeof(INT).
    If doubles are used and are twice as long as ints, this gives
    18 + 10 * mult
    so that the value of ldkey can be obtained by dividing available
    (int) workspace by this number.

    In fact, because iwork() can actually s * n + s - 1 int chunks
    when allocating a REAL, we use ldkey = available / numb - 1.

    FIXME: Can we always assume that sizeof(double) / sizeof(int) is 2?
    */
    if 4 as libc::c_int == 4 as libc::c_int {
        numb = 18 as libc::c_int + 10 as libc::c_int * mult
    } else {
        numb = (mult << 3 as libc::c_int) + 12 as libc::c_int
    } /* Single precision reals */
    ldkey = (iwkmax - iwkpt) / numb - 1 as libc::c_int;
    if mult as libc::c_double * ldkey as libc::c_double
        > 2147483647 as libc::c_int as libc::c_double
    {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"integer overflow would happen in \'mult * ldkey\' = %g\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ),
            mult as libc::c_double * ldkey as libc::c_double,
        );
    }
    ldstp = mult * ldkey;
    i4 = iwork(
        iwkmax,
        &mut iwkpt,
        ldkey << 1 as libc::c_int,
        2 as libc::c_int,
    );
    i5 = iwork(
        iwkmax,
        &mut iwkpt,
        ldkey << 1 as libc::c_int,
        2 as libc::c_int,
    );
    i6 = iwork(
        iwkmax,
        &mut iwkpt,
        ldstp << 1 as libc::c_int,
        4 as libc::c_int,
    );
    i7 = iwork(
        iwkmax,
        &mut iwkpt,
        ldstp * 6 as libc::c_int,
        2 as libc::c_int,
    );
    i8 = iwork(
        iwkmax,
        &mut iwkpt,
        ldkey << 1 as libc::c_int,
        4 as libc::c_int,
    );
    i9 = iwork(
        iwkmax,
        &mut iwkpt,
        ldkey << 1 as libc::c_int,
        4 as libc::c_int,
    );
    i9a = iwork(
        iwkmax,
        &mut iwkpt,
        ldkey << 1 as libc::c_int,
        4 as libc::c_int,
    );
    i10 = iwork(
        iwkmax,
        &mut iwkpt,
        ldkey << 1 as libc::c_int,
        2 as libc::c_int,
    );
    /* Double precision reals */
    /* To convert to double precision, change RWRK to DWRK in the next CALL.
     */
    f2xact(
        nrow,
        ncol,
        table,
        ldtabl,
        expect,
        percnt,
        emin,
        prt,
        pre,
        equiv.offset(i1 as isize),
        (equiv as *mut libc::c_int).offset(i2 as isize),
        (equiv as *mut libc::c_int).offset(i3 as isize),
        (equiv as *mut libc::c_int).offset(i3a as isize),
        (equiv as *mut libc::c_int).offset(i3b as isize),
        (equiv as *mut libc::c_int).offset(i3c as isize),
        (equiv as *mut libc::c_int).offset(i4 as isize),
        ldkey,
        (equiv as *mut libc::c_int).offset(i5 as isize),
        equiv.offset(i6 as isize),
        ldstp,
        (equiv as *mut libc::c_int).offset(i7 as isize),
        equiv.offset(i8 as isize),
        equiv.offset(i9 as isize),
        equiv.offset(i9a as isize),
        (equiv as *mut libc::c_int).offset(i10 as isize),
        (equiv as *mut libc::c_int).offset(iiwk as isize),
        equiv.offset(irwk as isize),
        n2_stack,
    ); // (new; = 1/2 stacksize, used to be hardcoded = 200)
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1999-2017   The R Core Team.
 *
 *  Based on ACM TOMS643 (1993)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/*
  Fisher's exact test for contingency tables -- usage see below

  fexact.f -- translated by f2c (version 19971204).
  Run through a slightly modified version of MM's f2c-clean.
  Heavily hand-edited by KH and MM.
*/
/* <UTF8> chars are handled as whole strings */
unsafe extern "C" fn f2xact(
    mut nrow: libc::c_int,
    mut ncol: libc::c_int,
    mut table: *const libc::c_int,
    mut ldtabl: libc::c_int,
    mut expect: libc::c_double,
    mut percnt: libc::c_double,
    mut emin: libc::c_double,
    mut prt: *mut libc::c_double,
    mut pre: *mut libc::c_double,
    mut fact: *mut libc::c_double,
    mut ico: *mut libc::c_int,
    mut iro: *mut libc::c_int,
    mut kyy: *mut libc::c_int,
    mut idif: *mut libc::c_int,
    mut irn: *mut libc::c_int,
    mut key: *mut libc::c_int,
    mut ldkey: libc::c_int,
    mut ipoin: *mut libc::c_int,
    mut stp: *mut libc::c_double,
    mut ldstp: libc::c_int,
    mut ifrq: *mut libc::c_int,
    mut LP: *mut libc::c_double,
    mut SP: *mut libc::c_double,
    mut tm: *mut libc::c_double,
    mut key2: *mut libc::c_int,
    mut iwk: *mut libc::c_int,
    mut rwk: *mut libc::c_double,
    mut n2_stack: libc::c_int,
) {
    let mut current_block: u64;
    /*
    -----------------------------------------------------------------------
    Name:  F2XACT
    Purpose: Computes Fisher's exact test for a contingency table,
          routine with workspace variables specified.
    -----------------------------------------------------------------------
    */
    let imax: libc::c_int = 2147483647 as libc::c_int; /* the largest representable int on the machine.*/
    /* AMISS is a missing value indicator which is returned when the
    probability is not defined. */
    let _amiss: libc::c_double = -12345.0f64;
    /* TOL is chosen as the square root of the smallest relative spacing. */
    static mut tol: libc::c_double = 3.45254e-7f64;
    let mut ch_err_5: *const libc::c_char =
        b"The hash table key cannot be computed because the largest key\nis larger than the largest representable int.\nThe algorithm cannot proceed.\nReduce the workspace, consider using \'simulate.p.value=TRUE\' or another algorithm.\x00"
            as *const u8 as *const libc::c_char;
    /* Local variables -- changed from "static"
     *  (*does* change results very slightly on i386 linux) */
    let mut i: libc::c_int = 0;
    let mut ii: libc::c_int = 0 as libc::c_int;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut ifreq: libc::c_int = 0;
    let mut ikkey: libc::c_int = 0;
    let mut ikstp: libc::c_int = 0;
    let mut ikstp2: libc::c_int = 0;
    let mut ipn: libc::c_int = 0;
    let mut ipo: libc::c_int = 0;
    let mut itop: libc::c_int = 0;
    let mut itp: libc::c_int = 0 as libc::c_int;
    let mut jkey: libc::c_int = 0;
    let mut jstp: libc::c_int = 0;
    let mut jstp2: libc::c_int = 0;
    let mut jstp3: libc::c_int = 0;
    let mut jstp4: libc::c_int = 0;
    let mut k1: libc::c_int = 0;
    let mut kb: libc::c_int = 0;
    let mut kd: libc::c_int = 0;
    let mut ks: libc::c_int = 0;
    let mut kval: libc::c_int = 0 as libc::c_int;
    let mut kmax: libc::c_int = 0;
    let mut last: libc::c_int = 0;
    let mut ntot: libc::c_int = 0;
    let mut nco: libc::c_int = 0;
    let mut nro: libc::c_int = 0;
    let mut nro2: libc::c_int = 0;
    let mut nrb: libc::c_int = 0;
    let mut i31: libc::c_int = 0;
    let mut i32: libc::c_int = 0;
    let mut i33: libc::c_int = 0;
    let mut i34: libc::c_int = 0;
    let mut i35: libc::c_int = 0;
    let mut i36: libc::c_int = 0;
    let mut i37: libc::c_int = 0;
    let mut i38: libc::c_int = 0;
    let mut i39: libc::c_int = 0;
    let mut i41: libc::c_int = 0;
    let mut i42: libc::c_int = 0;
    let mut i43: libc::c_int = 0;
    let mut i44: libc::c_int = 0;
    let mut i45: libc::c_int = 0;
    let mut i46: libc::c_int = 0;
    let mut i47: libc::c_int = 0;
    let mut i48: libc::c_int = 0;
    let mut i310: libc::c_int = 0;
    let mut i311: libc::c_int = 0;
    let mut dspt: libc::c_double = 0.;
    let mut df: libc::c_double = 0.;
    let mut ddf: libc::c_double = 0.;
    let mut drn: libc::c_double = 0.;
    let mut dro: libc::c_double = 0.;
    let mut obs: libc::c_double = 0.;
    let mut obs2: libc::c_double = 0.;
    let mut obs3: libc::c_double = 0.;
    let mut pastp: libc::c_double = 0.;
    let mut pv: libc::c_double = 0.;
    let mut tmp: libc::c_double = 0.0f64;
    let mut ok_f7: Rboolean = FALSE;
    let mut nr_gt_nc: Rboolean = FALSE;
    let mut maybe_chisq: Rboolean = (expect > 0.0f64) as libc::c_int as Rboolean;
    let mut chisq: Rboolean = FALSE;
    let mut psh: Rboolean = FALSE;
    /* Parameter adjustments */
    table = table.offset(-((ldtabl + 1 as libc::c_int) as isize));
    ico = ico.offset(-1);
    iro = iro.offset(-1);
    kyy = kyy.offset(-1);
    idif = idif.offset(-1);
    irn = irn.offset(-1);
    key = key.offset(-1);
    ipoin = ipoin.offset(-1);
    stp = stp.offset(-1);
    ifrq = ifrq.offset(-1);
    LP = LP.offset(-1);
    SP = SP.offset(-1);
    tm = tm.offset(-1);
    key2 = key2.offset(-1);
    iwk = iwk.offset(-1);
    rwk = rwk.offset(-1);
    /* Check table dimensions */
    if nrow > ldtabl {
        prterr(
            1 as libc::c_int,
            b"NROW must be less than or equal to LDTABL.\x00" as *const u8 as *const libc::c_char,
        );
    }
    if ncol <= 1 as libc::c_int {
        prterr(
            4 as libc::c_int,
            b"NCOL must be at least 2\x00" as *const u8 as *const libc::c_char,
        );
    }
    /* Initialize KEY array */
    i = 1 as libc::c_int;
    while i <= ldkey << 1 as libc::c_int {
        *key.offset(i as isize) = -(9999 as libc::c_int);
        *key2.offset(i as isize) = -(9999 as libc::c_int);
        i += 1
    }
    /* Determine row and column marginals.
    Define   max(nrow,ncol) =: nco >= nro := min(nrow,ncol)
    */
    nr_gt_nc = (nrow > ncol) as libc::c_int as Rboolean;
    /* nco := max(nrow, ncol) : */
    if nr_gt_nc as u64 != 0 {
        nco = nrow
    } else {
        nco = ncol
    }
    /* Compute row marginals and total */
    ntot = 0 as libc::c_int;
    i = 1 as libc::c_int;
    while i <= nrow {
        *iro.offset(i as isize) = 0 as libc::c_int;
        j = 1 as libc::c_int;
        while j <= ncol {
            if (*table.offset((i + j * ldtabl) as isize) as libc::c_double) < 0.0f64 {
                prterr(
                    2 as libc::c_int,
                    b"All elements of TABLE must be nonnegative.\x00" as *const u8
                        as *const libc::c_char,
                );
            }
            *iro.offset(i as isize) += *table.offset((i + j * ldtabl) as isize);
            j += 1
        }
        ntot += *iro.offset(i as isize);
        i += 1
    }
    if ntot == 0 as libc::c_int {
        prterr(
            3 as libc::c_int,
            b"All elements of TABLE are zero.\nPRT and PRE are set to missing values.\x00"
                as *const u8 as *const libc::c_char,
        );
    }
    /* Column marginals */
    i = 1 as libc::c_int;
    while i <= ncol {
        *ico.offset(i as isize) = 0 as libc::c_int;
        j = 1 as libc::c_int;
        while j <= nrow {
            *ico.offset(i as isize) += *table.offset((j + i * ldtabl) as isize);
            j += 1
        }
        i += 1
    }
    /* sort marginals */
    R_isort(&mut *iro.offset(1 as libc::c_int as isize), nrow);
    R_isort(&mut *ico.offset(1 as libc::c_int as isize), ncol);
    // Swap marginals if necessary to ico[1:nco] & iro[1:nro]
    if nr_gt_nc as u64 != 0 {
        nro = ncol;
        /* Swap marginals */
        i = 1 as libc::c_int;
        while i <= nco {
            ii = *iro.offset(i as isize);
            if i <= nro {
                *iro.offset(i as isize) = *ico.offset(i as isize)
            }
            *ico.offset(i as isize) = ii;
            i += 1
        }
    } else {
        nro = nrow
    }
    /* Get multiplers for stack */
    *kyy.offset(1 as libc::c_int as isize) = 1 as libc::c_int;
    i = 1 as libc::c_int;
    while i < nro {
        /* Hash table multipliers */
        if *iro.offset(i as isize) + 1 as libc::c_int <= imax / *kyy.offset(i as isize) {
            *kyy.offset((i + 1 as libc::c_int) as isize) =
                *kyy.offset(i as isize) * (*iro.offset(i as isize) + 1 as libc::c_int)
        // no sense????  j /= kyy[i];
        } else {
            prterr(5 as libc::c_int, ch_err_5);
        }
        i += 1
    }
    /* Check for Maximum product : */
    /* original code: if (iro[nro - 1] + 1 > imax / kyy[nro - 1]) */
    if *iro.offset(nro as isize) + 1 as libc::c_int > imax / *kyy.offset(nro as isize) {
        /* L_ERR_5: */
        prterr(501 as libc::c_int, ch_err_5);
    }
    /* Compute log factorials */
    *fact.offset(0 as libc::c_int as isize) = 0.0f64;
    *fact.offset(1 as libc::c_int as isize) = 0.0f64;
    if ntot >= 2 as libc::c_int {
        *fact.offset(2 as libc::c_int as isize) = 2.0f64.ln()
    }
    /* MM: old code assuming .ln() to be SLOW */
    i = 3 as libc::c_int;
    while i <= ntot {
        *fact.offset(i as isize) =
            *fact.offset((i - 1 as libc::c_int) as isize) + (i as libc::c_double).ln();
        j = i + 1 as libc::c_int;
        if j <= ntot {
            *fact.offset(j as isize) = *fact.offset(i as isize)
                + *fact.offset(2 as libc::c_int as isize)
                + *fact.offset((j / 2 as libc::c_int) as isize)
                - *fact.offset((j / 2 as libc::c_int - 1 as libc::c_int) as isize)
        }
        i += 2 as libc::c_int
    }
    /* Compute obs := observed path length */
    obs = tol;
    ntot = 0 as libc::c_int;
    j = 1 as libc::c_int;
    while j <= nco {
        let mut dd: libc::c_double = 0.0f64;
        if nr_gt_nc as u64 != 0 {
            i = 1 as libc::c_int;
            while i <= nro {
                dd += *fact.offset(*table.offset((j + i * ldtabl) as isize) as isize);
                ntot += *table.offset((j + i * ldtabl) as isize);
                i += 1
            }
        } else {
            i = 1 as libc::c_int;
            ii = j * ldtabl + 1 as libc::c_int;
            while i <= nro {
                dd += *fact.offset(*table.offset(ii as isize) as isize);
                ntot += *table.offset(ii as isize);
                i += 1;
                ii += 1
            }
        }
        obs += *fact.offset(*ico.offset(j as isize) as isize) - dd;
        j += 1
    }
    /* Denominator of observed table: DRO */
    dro = f9xact(
        nro,
        ntot,
        &mut *iro.offset(1 as libc::c_int as isize) as *mut libc::c_int as *const libc::c_int,
        fact as *const libc::c_double,
    );
    /* improve: the following "easily" underflows to zero -- return "log()";
    but then we trash it anyway in Fexact() below */
    *prt = (obs - dro).exp();
    *pre = 0.0f64;
    itop = 0 as libc::c_int;
    /* Initialize pointers for workspace */
    /* f3xact */
    i31 = 1 as libc::c_int;
    i32 = i31 + nco;
    i33 = i32 + nco;
    i34 = i33 + nco;
    i35 = i34 + nco;
    i36 = i35 + nco;
    i37 = i36 + nco;
    i38 = i37 + nco;
    i39 = i38 + 2 as libc::c_int * n2_stack;
    i310 = 1 as libc::c_int;
    i311 = 1 as libc::c_int + 2 as libc::c_int * n2_stack;
    /* f4xact */
    i = nrow + ncol + 1 as libc::c_int;
    i41 = 1 as libc::c_int;
    i42 = i41 + i;
    i43 = i42 + i;
    i44 = i43 + i;
    i45 = i44 + i;
    i46 = i45 + i;
    i47 = i46 + i * nco;
    i48 = 1 as libc::c_int;
    /* Initialize pointers */
    k = nco;
    last = ldkey + 1 as libc::c_int;
    jkey = ldkey + 1 as libc::c_int;
    jstp = ldstp + 1 as libc::c_int;
    jstp2 = ldstp * 3 as libc::c_int + 1 as libc::c_int;
    jstp3 = (ldstp << 2 as libc::c_int) + 1 as libc::c_int;
    jstp4 = ldstp * 5 as libc::c_int + 1 as libc::c_int;
    ikkey = 0 as libc::c_int;
    ikstp = 0 as libc::c_int;
    ikstp2 = ldstp << 1 as libc::c_int;
    ipo = 1 as libc::c_int;
    *ipoin.offset(1 as libc::c_int as isize) = 1 as libc::c_int;
    *stp.offset(1 as libc::c_int as isize) = 0.0f64;
    *ifrq.offset(1 as libc::c_int as isize) = 1 as libc::c_int;
    *ifrq.offset((ikstp2 + 1 as libc::c_int) as isize) = -(1 as libc::c_int);
    'c_3999: loop
    /* Update pointers */
    {
        kb = nco - k + 1 as libc::c_int;
        ks = 0 as libc::c_int;
        n = *ico.offset(kb as isize);
        kd = nro + 1 as libc::c_int;
        kmax = nro;
        /* IDIF is the difference in going to the daughter */
        i = 1 as libc::c_int;
        while i <= nro {
            *idif.offset(i as isize) = 0 as libc::c_int;
            i += 1
        }
        loop
        /* Generate the first daughter */
        {
            kd -= 1;
            ntot = imin2(n, *iro.offset(kd as isize));
            *idif.offset(kd as isize) = ntot;
            if *idif.offset(kmax as isize) == 0 as libc::c_int {
                kmax -= 1
            }
            n -= ntot;
            if !(n > 0 as libc::c_int && kd != 1 as libc::c_int) {
                break;
            }
        }
        if !(n != 0 as libc::c_int) {
            k1 = k - 1 as libc::c_int;
            n = *ico.offset(kb as isize);
            ntot = 0 as libc::c_int;
            i = kb + 1 as libc::c_int;
            while i <= nco {
                ntot += *ico.offset(i as isize);
                i += 1
            }
            loop {
                /* Arc to daughter length=ICO[KB] */
                i = 1 as libc::c_int;
                while i <= nro {
                    *irn.offset(i as isize) = *iro.offset(i as isize) - *idif.offset(i as isize);
                    i += 1
                }
                if k1 > 1 as libc::c_int {
                    /* Sort irn */
                    if nro == 2 as libc::c_int {
                        if *irn.offset(1 as libc::c_int as isize)
                            > *irn.offset(2 as libc::c_int as isize)
                        {
                            ii = *irn.offset(1 as libc::c_int as isize);
                            *irn.offset(1 as libc::c_int as isize) =
                                *irn.offset(2 as libc::c_int as isize);
                            *irn.offset(2 as libc::c_int as isize) = ii
                        }
                    } else {
                        R_isort(&mut *irn.offset(1 as libc::c_int as isize), nro);
                    }
                    /* Adjust start for zero */
                    i = 1 as libc::c_int;
                    while i <= nro {
                        if *irn.offset(i as isize) != 0 as libc::c_int {
                            break;
                        }
                        i += 1
                    }
                    nrb = i
                } else {
                    nrb = 1 as libc::c_int
                }
                nro2 = nro - nrb + 1 as libc::c_int;
                /* Some table values */
                ddf = f9xact(
                    nro,
                    n,
                    &mut *idif.offset(1 as libc::c_int as isize) as *mut libc::c_int
                        as *const libc::c_int,
                    fact as *const libc::c_double,
                );
                drn = f9xact(
                    nro2,
                    ntot,
                    &mut *irn.offset(nrb as isize) as *mut libc::c_int as *const libc::c_int,
                    fact as *const libc::c_double,
                ) - dro
                    + ddf;
                /* Get hash value */
                if k1 > 1 as libc::c_int {
                    kval = *irn.offset(1 as libc::c_int as isize);
                    /* Note that with the corrected check at error "501",
                     * we won't have overflow in  kval  below : */
                    i = 2 as libc::c_int;
                    while i <= nro {
                        kval += *irn.offset(i as isize) * *kyy.offset(i as isize);
                        i += 1
                    }
                    /* Get hash table entry */
                    i = kval % (ldkey << 1 as libc::c_int) + 1 as libc::c_int;
                    /* Search for unused location */
                    itp = i;
                    loop {
                        if !(itp <= ldkey << 1 as libc::c_int) {
                            current_block = 1131197912709891142;
                            break;
                        }
                        ii = *key2.offset(itp as isize);
                        if ii == kval {
                            current_block = 3798921173508899050;
                            break;
                        }
                        if ii < 0 as libc::c_int {
                            *key2.offset(itp as isize) = kval;
                            *LP.offset(itp as isize) = 1.0f64;
                            *SP.offset(itp as isize) = 1.0f64;
                            current_block = 3798921173508899050;
                            break;
                        } else {
                            itp += 1
                        }
                    }
                    match current_block {
                        3798921173508899050 => {}
                        _ => {
                            itp = 1 as libc::c_int;
                            loop {
                                if !(itp <= i - 1 as libc::c_int) {
                                    current_block = 5267916556966421873;
                                    break;
                                }
                                ii = *key2.offset(itp as isize);
                                if ii == kval {
                                    current_block = 3798921173508899050;
                                    break;
                                }
                                if ii < 0 as libc::c_int {
                                    *key2.offset(itp as isize) = kval;
                                    *LP.offset(itp as isize) = 1.0f64;
                                    current_block = 3798921173508899050;
                                    break;
                                } else {
                                    itp += 1
                                }
                            }
                            match current_block {
                                3798921173508899050 => {}
                                _ => {
                                    /* KH
                                    prterr(6, "LDKEY is too small.\n" ... */
                                    error(dcgettext(b"stats\x00" as
                                                               *const u8 as
                                                               *const libc::c_char,
                                                           b"FEXACT error 6.  LDKEY=%d is too small for this problem,\n  (ii := key2[itp=%d] = %d, ldstp=%d)\nTry increasing the size of the workspace and possibly \'mult\'\x00"
                                                               as *const u8 as
                                                               *const libc::c_char,
                                                           5 as libc::c_int),
                                                 ldkey, itp, ii, ldstp);
                                }
                            }
                        }
                    }
                }
                psh = TRUE;
                /* Recover pastp */
                ipn = *ipoin.offset((ipo + ikkey) as isize);
                pastp = *stp.offset((ipn + ikstp) as isize);
                ifreq = *ifrq.offset((ipn + ikstp) as isize);
                /* Compute shortest and longest path */
                if k1 > 1 as libc::c_int {
                    obs2 = obs
                        - *fact.offset(*ico.offset((kb + 1 as libc::c_int) as isize) as isize)
                        - *fact.offset(*ico.offset((kb + 2 as libc::c_int) as isize) as isize)
                        - ddf;
                    i = 3 as libc::c_int;
                    while i <= k1 {
                        obs2 -= *fact.offset(*ico.offset((kb + i) as isize) as isize);
                        i += 1
                    }
                    if *LP.offset(itp as isize) > 0.0f64 {
                        dspt = obs - obs2 - ddf;
                        /* Compute longest path */
                        *LP.offset(itp as isize) = f3xact(
                            nro2,
                            &mut *irn.offset(nrb as isize) as *mut libc::c_int
                                as *const libc::c_int,
                            k1,
                            &mut *ico.offset((kb + 1 as libc::c_int) as isize) as *mut libc::c_int
                                as *const libc::c_int,
                            ntot,
                            fact as *const libc::c_double,
                            &mut *iwk.offset(i31 as isize),
                            &mut *iwk.offset(i32 as isize),
                            &mut *iwk.offset(i33 as isize),
                            &mut *iwk.offset(i34 as isize),
                            &mut *iwk.offset(i35 as isize),
                            &mut *iwk.offset(i36 as isize),
                            &mut *iwk.offset(i37 as isize),
                            &mut *iwk.offset(i38 as isize),
                            &mut *iwk.offset(i39 as isize),
                            &mut *rwk.offset(i310 as isize),
                            &mut *rwk.offset(i311 as isize),
                            tol,
                            n2_stack,
                        );
                        if *LP.offset(itp as isize) > 0.0f64 {
                            /* can this happen? */
                            REprintf(
                                b"___ LP[itp=%d] = %g > 0\n\x00" as *const u8
                                    as *const libc::c_char,
                                itp,
                                *LP.offset(itp as isize),
                            );
                            *LP.offset(itp as isize) = 0.0f64
                        }
                        /* Compute shortest path -- using  dspt  as offset */
                        *SP.offset(itp as isize) = f4xact(
                            nro2,
                            &mut *irn.offset(nrb as isize),
                            k1,
                            &mut *ico.offset((kb + 1 as libc::c_int) as isize),
                            dspt,
                            fact as *const libc::c_double,
                            &mut *iwk.offset(i47 as isize),
                            &mut *iwk.offset(i41 as isize),
                            &mut *iwk.offset(i42 as isize),
                            &mut *iwk.offset(i43 as isize),
                            &mut *iwk.offset(i44 as isize),
                            &mut *iwk.offset(i45 as isize),
                            &mut *iwk.offset(i46 as isize),
                            &mut *rwk.offset(i48 as isize),
                            tol,
                        );
                        /* SP[itp] = fmin2(0., SP[itp] - dspt);*/
                        if *SP.offset(itp as isize) > 0.0f64 {
                            /* can this happen? */
                            REprintf(
                                b"___ SP[itp=%d] = %g > 0\n\x00" as *const u8
                                    as *const libc::c_char,
                                itp,
                                *SP.offset(itp as isize),
                            );
                            *SP.offset(itp as isize) = 0.0f64
                        }
                        /* Use chi-squared approximation? */
                        if maybe_chisq as libc::c_uint != 0
                            && (*irn.offset(nrb as isize)
                                * *ico.offset((kb + 1 as libc::c_int) as isize))
                                as libc::c_double
                                > ntot as libc::c_double * emin
                        {
                            let mut ncell: libc::c_int = 0.0f64 as libc::c_int;
                            i = 0 as libc::c_int;
                            while i < nro2 {
                                j = 1 as libc::c_int;
                                while j <= k1 {
                                    if (*irn.offset((nrb + i) as isize)
                                        * *ico.offset((kb + j) as isize))
                                        as libc::c_double
                                        >= ntot as libc::c_double * expect
                                    {
                                        ncell += 1
                                    }
                                    j += 1
                                }
                                i += 1
                            }
                            if (ncell * 100 as libc::c_int) as libc::c_double
                                >= (k1 * nro2) as libc::c_double * percnt
                            {
                                tmp = 0.0f64;
                                i = 0 as libc::c_int;
                                while i < nro2 {
                                    tmp += *fact.offset(*irn.offset((nrb + i) as isize) as isize)
                                        - *fact.offset(
                                            (*irn.offset((nrb + i) as isize) - 1 as libc::c_int)
                                                as isize,
                                        );
                                    i += 1
                                }
                                tmp *= (k1 - 1 as libc::c_int) as libc::c_double;
                                j = 1 as libc::c_int;
                                while j <= k1 {
                                    tmp += (nro2 - 1 as libc::c_int) as libc::c_double
                                        * (*fact.offset(*ico.offset((kb + j) as isize) as isize)
                                            - *fact.offset(
                                                (*ico.offset((kb + j) as isize) - 1 as libc::c_int)
                                                    as isize,
                                            ));
                                    j += 1
                                }
                                df = ((nro2 - 1 as libc::c_int) * (k1 - 1 as libc::c_int))
                                    as libc::c_double;
                                tmp += df * 1.83787706640934548356065947281f64;
                                tmp -= (nro2 * k1 - 1 as libc::c_int) as libc::c_double
                                    * (*fact.offset(ntot as isize)
                                        - *fact.offset((ntot - 1 as libc::c_int) as isize));
                                *tm.offset(itp as isize) = (obs - dro) * -2.0f64 - tmp
                            } else {
                                /* tm[itp] set to a flag value */
                                *tm.offset(itp as isize) = -9876.0f64
                            }
                        } else {
                            *tm.offset(itp as isize) = -9876.0f64
                        }
                    }
                    obs3 = obs2 - *LP.offset(itp as isize);
                    obs2 -= *SP.offset(itp as isize);
                    if *tm.offset(itp as isize) == -9876.0f64 {
                        chisq = FALSE
                    } else {
                        chisq = TRUE;
                        tmp = *tm.offset(itp as isize)
                    }
                } else {
                    obs2 = obs - drn - dro;
                    obs3 = obs2
                }
                loop {
                    /* Process node with new PASTP */
                    if pastp <= obs3 {
                        /* Update pre */
                        *pre += ifreq as libc::c_double * (pastp + drn).exp()
                    } else if pastp < obs2 {
                        if chisq as u64 != 0 {
                            df = ((nro2 - 1 as libc::c_int) * (k1 - 1 as libc::c_int))
                                as libc::c_double;
                            pv = pgamma(
                                fmax2(0.0f64, tmp + (pastp + drn) * 2.0f64) / 2.0f64,
                                df / 2.0f64,
                                1.0f64,
                                FALSE as libc::c_int,
                                TRUE as libc::c_int,
                            );
                            *pre += ifreq as libc::c_double * (pastp + drn + pv).exp()
                        } else {
                            /* Put daughter on queue */
                            f5xact(
                                pastp + ddf,
                                tol,
                                &mut kval,
                                &mut *key.offset(jkey as isize),
                                ldkey,
                                &mut *ipoin.offset(jkey as isize),
                                &mut *stp.offset(jstp as isize),
                                ldstp,
                                &mut *ifrq.offset(jstp as isize),
                                &mut *ifrq.offset(jstp2 as isize),
                                &mut *ifrq.offset(jstp3 as isize),
                                &mut *ifrq.offset(jstp4 as isize),
                                ifreq,
                                &mut itop,
                                psh,
                            );
                            psh = FALSE
                        }
                    }
                    /* Get next PASTP on chain */
                    ipn = *ifrq.offset((ipn + ikstp2) as isize);
                    if !(ipn > 0 as libc::c_int) {
                        break;
                    }
                    pastp = *stp.offset((ipn + ikstp) as isize);
                    ifreq = *ifrq.offset((ipn + ikstp) as isize)
                }
                /* Generate a new daughter node */
                ok_f7 = f7xact(
                    kmax,
                    &mut *iro.offset(1 as libc::c_int as isize) as *mut libc::c_int
                        as *const libc::c_int,
                    &mut *idif.offset(1 as libc::c_int as isize),
                    &mut kd,
                    &mut ks,
                );
                if !(ok_f7 as u64 != 0) {
                    break;
                }
            }
        }
        loop
        /* Go get a new mother from stage K */
        {
            if f6xact(
                nro,
                &mut *iro.offset(1 as libc::c_int as isize),
                &mut *kyy.offset(1 as libc::c_int as isize) as *mut libc::c_int
                    as *const libc::c_int,
                &mut *key.offset((ikkey + 1 as libc::c_int) as isize),
                ldkey,
                &mut last,
                &mut ipo,
            ) as u64
                == 0
            {
                continue 'c_3999;
            }
            /* else : no additional nodes to process */
            k -= 1;
            itop = 0 as libc::c_int;
            ikkey = jkey - 1 as libc::c_int;
            ikstp = jstp - 1 as libc::c_int;
            ikstp2 = jstp2 - 1 as libc::c_int;
            jkey = ldkey - jkey + 2 as libc::c_int;
            jstp = ldstp - jstp + 2 as libc::c_int;
            jstp2 = (ldstp << 1 as libc::c_int) + jstp;
            i = 1 as libc::c_int;
            while i <= ldkey << 1 as libc::c_int {
                *key2.offset(i as isize) = -(9999 as libc::c_int);
                i += 1
            }
            if !(k >= 2 as libc::c_int) {
                break;
            }
        }
        return;
    }
}
/* f2xact() */
unsafe extern "C" fn f3xact(
    mut nrow: libc::c_int,
    mut irow: *const libc::c_int,
    mut ncol: libc::c_int,
    mut icol: *const libc::c_int,
    mut ntot: libc::c_int,
    mut fact: *const libc::c_double,
    mut ico: *mut libc::c_int,
    mut iro: *mut libc::c_int,
    mut it: *mut libc::c_int,
    mut lb: *mut libc::c_int,
    mut nr: *mut libc::c_int,
    mut nt: *mut libc::c_int,
    mut nu: *mut libc::c_int,
    mut itc: *mut libc::c_int,
    mut ist: *mut libc::c_int,
    mut stv: *mut libc::c_double,
    mut alen: *mut libc::c_double,
    mut tol: libc::c_double,
    mut ldst: libc::c_int,
) -> libc::c_double {
    /*
    -----------------------------------------------------------------------
     Name:       F3XACT
     Purpose:    Computes the longest path length for a given table.

     Arguments:
       NROW    - The number of rows in the table.   (Input)
       IROW    - Vector of length NROW containing the row sums
             for the table.     (Input)
       NCOL    - The number of columns in the table.  (Input)
       ICOL    - Vector of length K containing the column sums
             for the table.     (Input)
       NTOT    - The total count in the table.   (Input)
       FACT    - Vector containing the logarithms of factorials. (Input)
       ICO     - Work vector of length MAX(NROW,NCOL).
       IRO     - Work vector of length MAX(NROW,NCOL).
       IT     - Work vector of length MAX(NROW,NCOL).
       LB     - Work vector of length MAX(NROW,NCOL).
       NR     - Work vector of length MAX(NROW,NCOL).
       NT     - Work vector of length MAX(NROW,NCOL).
       NU     - Work vector of length MAX(NROW,NCOL).
       ITC     - Work vector of length 2*ldst (was 400)
       IST     - Work vector of length 2*ldst (was 400)
       STV     - Work vector of length 2*ldst (was 400)
       ALEN    - Work vector of length MAX(NROW,NCOL).
       TOL     - Tolerance.     (Input)
       ldst    - half stack size, aka 'n2_stack', was == 200       (Input)

     Return Value :
       LP     - The longest path for the table.   (Output)

     -----------------------------------------------------------------------
     */
    /* Now an argument :
    const int ldst = 200;  half stack size */
    /* Initialized data */
    static mut nst: libc::c_int = 0 as libc::c_int;
    static mut nitc: libc::c_int = 0 as libc::c_int;
    /* Local variables */
    let mut i: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut ii: libc::c_int = 0;
    let mut nn: libc::c_int = 0;
    let mut ks: libc::c_int = 0;
    let mut nr1: libc::c_int = 0;
    let mut nco: libc::c_int = 0;
    let mut ipn: libc::c_int = 0;
    let mut irl: libc::c_int = 0;
    let mut key: libc::c_int = 0;
    let mut lev: libc::c_int = 0;
    let mut itp: libc::c_int = 0;
    let mut nro: libc::c_int = 0;
    let mut kyy: libc::c_int = 0;
    let mut nc1s: libc::c_int = 0;
    let mut LP: libc::c_double = 0.;
    let mut v: libc::c_double = 0.;
    let mut val: libc::c_double = 0.;
    let mut vmn: libc::c_double = 0.;
    let mut xmin: Rboolean = FALSE;
    /* Parameter adjustments */
    stv = stv.offset(-1);
    ist = ist.offset(-1);
    itc = itc.offset(-1);
    nu = nu.offset(-1);
    nt = nt.offset(-1);
    nr = nr.offset(-1);
    lb = lb.offset(-1);
    it = it.offset(-1);
    iro = iro.offset(-1);
    ico = ico.offset(-1);
    icol = icol.offset(-1);
    irow = irow.offset(-1);
    if nrow <= 1 as libc::c_int {
        /* nrow is 1 */
        LP = 0.0f64;
        if nrow > 0 as libc::c_int {
            i = 1 as libc::c_int;
            while i <= ncol {
                LP -= *fact.offset(*icol.offset(i as isize) as isize);
                i += 1
            }
        }
        return LP;
    }
    if ncol <= 1 as libc::c_int {
        /* ncol is 1 */
        LP = 0.0f64;
        if ncol > 0 as libc::c_int {
            i = 1 as libc::c_int;
            while i <= nrow {
                LP -= *fact.offset(*irow.offset(i as isize) as isize);
                i += 1
            }
        }
        return LP;
    }
    /* 2 by 2 table */
    if nrow * ncol == 4 as libc::c_int {
        let mut n11: libc::c_int = (*irow.offset(1 as libc::c_int as isize) + 1 as libc::c_int)
            * (*icol.offset(1 as libc::c_int as isize) + 1 as libc::c_int)
            / (ntot + 2 as libc::c_int);
        let mut n12: libc::c_int = *irow.offset(1 as libc::c_int as isize) - n11;
        return -(*fact.offset(n11 as isize)
            + *fact.offset(n12 as isize)
            + *fact.offset((*icol.offset(1 as libc::c_int as isize) - n11) as isize)
            + *fact.offset((*icol.offset(2 as libc::c_int as isize) - n12) as isize));
    }
    /* ELSE:  larger than 2 x 2 : */
    /* Test for optimal table */
    val = 0.0f64;
    if *irow.offset(nrow as isize) <= *irow.offset(1 as libc::c_int as isize) + ncol {
        xmin = f10act(
            nrow,
            &*irow.offset(1 as libc::c_int as isize),
            ncol,
            &*icol.offset(1 as libc::c_int as isize),
            &mut val,
            fact,
            &mut *lb.offset(1 as libc::c_int as isize),
            &mut *nu.offset(1 as libc::c_int as isize),
            &mut *nr.offset(1 as libc::c_int as isize),
        )
    } else {
        xmin = FALSE
    }
    if xmin as u64 == 0
        && *icol.offset(ncol as isize) <= *icol.offset(1 as libc::c_int as isize) + nrow
    {
        xmin = f10act(
            ncol,
            &*icol.offset(1 as libc::c_int as isize),
            nrow,
            &*irow.offset(1 as libc::c_int as isize),
            &mut val,
            fact,
            &mut *lb.offset(1 as libc::c_int as isize),
            &mut *nu.offset(1 as libc::c_int as isize),
            &mut *nr.offset(1 as libc::c_int as isize),
        )
    }
    if xmin as u64 != 0 {
        return -val;
    }
    /* Setup for dynamic programming */
    i = 0 as libc::c_int;
    while i <= ncol {
        *alen.offset(i as isize) = 0.0f64;
        i += 1
    }
    i = 1 as libc::c_int;
    while i <= 2 as libc::c_int * ldst {
        *ist.offset(i as isize) = -(1 as libc::c_int);
        i += 1
    }
    nn = ntot;
    /* Minimize ncol */
    if nrow >= ncol {
        nro = nrow;
        nco = ncol;
        *ico.offset(1 as libc::c_int as isize) = *icol.offset(1 as libc::c_int as isize);
        *nt.offset(1 as libc::c_int as isize) = nn - *ico.offset(1 as libc::c_int as isize);
        i = 2 as libc::c_int;
        while i <= ncol {
            *ico.offset(i as isize) = *icol.offset(i as isize);
            *nt.offset(i as isize) =
                *nt.offset((i - 1 as libc::c_int) as isize) - *ico.offset(i as isize);
            i += 1
        }
        i = 1 as libc::c_int;
        while i <= nrow {
            *iro.offset(i as isize) = *irow.offset(i as isize);
            i += 1
        }
    } else {
        nro = ncol;
        nco = nrow;
        *ico.offset(1 as libc::c_int as isize) = *irow.offset(1 as libc::c_int as isize);
        *nt.offset(1 as libc::c_int as isize) = nn - *ico.offset(1 as libc::c_int as isize);
        i = 2 as libc::c_int;
        while i <= nrow {
            *ico.offset(i as isize) = *irow.offset(i as isize);
            *nt.offset(i as isize) =
                *nt.offset((i - 1 as libc::c_int) as isize) - *ico.offset(i as isize);
            i += 1
        }
        i = 1 as libc::c_int;
        while i <= ncol {
            *iro.offset(i as isize) = *icol.offset(i as isize);
            i += 1
        }
    }
    nc1s = nco - 1 as libc::c_int;
    kyy = *ico.offset(nco as isize) + 1 as libc::c_int;
    /* Initialize pointers */
    vmn = 1e100f64; /* to contain min(v..) */
    irl = 1 as libc::c_int;
    ks = 0 as libc::c_int;
    k = ldst;
    'c_7655: loop {
        /* Setup to generate new node */
        lev = 1 as libc::c_int;
        nr1 = nro - 1 as libc::c_int;
        let mut nrt: libc::c_int = *iro.offset(irl as isize);
        let mut nct: libc::c_int = *ico.offset(1 as libc::c_int as isize);
        *lb.offset(1 as libc::c_int as isize) = ((nrt as libc::c_double
            + 1 as libc::c_int as libc::c_double)
            * (nct + 1 as libc::c_int) as libc::c_double
            / (nn + nr1 * nc1s + 1 as libc::c_int) as libc::c_double
            - tol) as libc::c_int
            - 1 as libc::c_int;
        *nu.offset(1 as libc::c_int as isize) =
            ((nrt as libc::c_double + nc1s as libc::c_double) * (nct + nr1) as libc::c_double
                / (nn + nr1 + nc1s) as libc::c_double) as libc::c_int
                - *lb.offset(1 as libc::c_int as isize)
                + 1 as libc::c_int;
        *nr.offset(1 as libc::c_int as isize) = nrt - *lb.offset(1 as libc::c_int as isize);
        'c_7661: loop {
            /* Generate a node */
            let ref mut fresh0 = *nu.offset(lev as isize);
            *fresh0 -= 1;
            if *nu.offset(lev as isize) == 0 as libc::c_int {
                if lev == 1 as libc::c_int {
                    break;
                }
                lev -= 1
            } else {
                let ref mut fresh1 = *lb.offset(lev as isize);
                *fresh1 += 1;
                let ref mut fresh2 = *nr.offset(lev as isize);
                *fresh2 -= 1;
                loop {
                    *alen.offset(lev as isize) = *alen.offset((lev - 1 as libc::c_int) as isize)
                        + *fact.offset(*lb.offset(lev as isize) as isize);
                    if lev >= nc1s {
                        break;
                    }
                    let mut nn1: libc::c_int = *nt.offset(lev as isize);
                    let mut nrt_0: libc::c_int = *nr.offset(lev as isize);
                    lev += 1;
                    let mut nc1: libc::c_int = nco - lev;
                    let mut nct_0: libc::c_int = *ico.offset(lev as isize);
                    *lb.offset(lev as isize) = ((nrt_0 as libc::c_double
                        + 1 as libc::c_int as libc::c_double)
                        * (nct_0 + 1 as libc::c_int) as libc::c_double
                        / (nn1 + nr1 * nc1 + 1 as libc::c_int) as libc::c_double
                        - tol) as libc::c_int;
                    *nu.offset(lev as isize) = ((nrt_0 as libc::c_double + nc1 as libc::c_double)
                        * (nct_0 + nr1) as libc::c_double
                        / (nn1 + nr1 + nc1) as libc::c_double
                        - *lb.offset(lev as isize) as libc::c_double
                        + 1 as libc::c_int as libc::c_double)
                        as libc::c_int;
                    *nr.offset(lev as isize) = nrt_0 - *lb.offset(lev as isize)
                }
                *alen.offset(nco as isize) =
                    *alen.offset(lev as isize) + *fact.offset(*nr.offset(lev as isize) as isize);
                *lb.offset(nco as isize) = *nr.offset(lev as isize);
                v = val + *alen.offset(nco as isize);
                if nro == 2 as libc::c_int {
                    /* Only 1 row left */
                    v += *fact.offset(
                        (*ico.offset(1 as libc::c_int as isize)
                            - *lb.offset(1 as libc::c_int as isize))
                            as isize,
                    ) + *fact.offset(
                        (*ico.offset(2 as libc::c_int as isize)
                            - *lb.offset(2 as libc::c_int as isize))
                            as isize,
                    ); /* Column marginals are new node */
                    i = 3 as libc::c_int;
                    while i <= nco {
                        v += *fact
                            .offset((*ico.offset(i as isize) - *lb.offset(i as isize)) as isize);
                        i += 1
                    }
                    if v < vmn {
                        vmn = v
                    }
                } else if nro == 3 as libc::c_int && nco == 2 as libc::c_int {
                    let mut nn1_0: libc::c_int = nn - *iro.offset(irl as isize) + 2 as libc::c_int;
                    let mut ic1: libc::c_int = *ico.offset(1 as libc::c_int as isize)
                        - *lb.offset(1 as libc::c_int as isize);
                    let mut ic2: libc::c_int = *ico.offset(2 as libc::c_int as isize)
                        - *lb.offset(2 as libc::c_int as isize);
                    let mut n11_0: libc::c_int = (*iro.offset((irl + 1 as libc::c_int) as isize)
                        + 1 as libc::c_int)
                        * (ic1 + 1 as libc::c_int)
                        / nn1_0;
                    let mut n12_0: libc::c_int =
                        *iro.offset((irl + 1 as libc::c_int) as isize) - n11_0;
                    v += *fact.offset(n11_0 as isize)
                        + *fact.offset(n12_0 as isize)
                        + *fact.offset((ic1 - n11_0) as isize)
                        + *fact.offset((ic2 - n12_0) as isize);
                    if v < vmn {
                        vmn = v
                    }
                /* 3 rows and 2 columns */
                } else {
                    i = 1 as libc::c_int;
                    while i <= nco {
                        *it.offset(i as isize) = imax2(
                            *ico.offset(i as isize) - *lb.offset(i as isize),
                            0 as libc::c_int,
                        );
                        i += 1
                    }
                    /* Sort column marginals it[] : */
                    if nco == 2 as libc::c_int {
                        if *it.offset(1 as libc::c_int as isize)
                            > *it.offset(2 as libc::c_int as isize)
                        {
                            /* swap */
                            ii = *it.offset(1 as libc::c_int as isize);
                            *it.offset(1 as libc::c_int as isize) =
                                *it.offset(2 as libc::c_int as isize);
                            *it.offset(2 as libc::c_int as isize) = ii
                        }
                    } else {
                        R_isort(&mut *it.offset(1 as libc::c_int as isize), nco);
                    }
                    /* Compute hash value */
                    key = *it.offset(1 as libc::c_int as isize) * kyy
                        + *it.offset(2 as libc::c_int as isize);
                    i = 3 as libc::c_int;
                    while i <= nco {
                        key = *it.offset(i as isize) + key * kyy;
                        i += 1
                    }
                    if key < -(1 as libc::c_int) {
                        error(
                            dcgettext(
                                b"stats\x00" as *const u8 as *const libc::c_char,
                                b"Bug in fexact3, it[i=%d]=%d: negative key %d (kyy=%d)\n\x00"
                                    as *const u8
                                    as *const libc::c_char,
                                5 as libc::c_int,
                            ),
                            i,
                            *it.offset(i as isize),
                            key,
                            kyy,
                        );
                    }
                    /* Table index */
                    ipn = key % ldst + 1 as libc::c_int;
                    /* Find empty position */
                    itp = ipn;
                    ii = ks + ipn;
                    while itp <= ldst {
                        /* L180: Push onto stack */
                        /* L190:  Marginals already on stack */
                        if *ist.offset(ii as isize) < 0 as libc::c_int {
                            *ist.offset(ii as isize) = key;
                            *stv.offset(ii as isize) = v;
                            nst += 1;
                            ii = nst + ks;
                            *itc.offset(ii as isize) = itp;
                            continue 'c_7661;
                        } else if *ist.offset(ii as isize) == key {
                            *stv.offset(ii as isize) = fmin2(v, *stv.offset(ii as isize));
                            continue 'c_7661;
                        } else {
                            itp += 1;
                            ii += 1
                        }
                    }
                    itp = 1 as libc::c_int;
                    ii = ks + 1 as libc::c_int;
                    while itp <= ipn - 1 as libc::c_int {
                        if *ist.offset(ii as isize) < 0 as libc::c_int {
                            *ist.offset(ii as isize) = key;
                            *stv.offset(ii as isize) = v;
                            nst += 1;
                            ii = nst + ks;
                            *itc.offset(ii as isize) = itp;
                            continue 'c_7661;
                        } else if *ist.offset(ii as isize) == key {
                            *stv.offset(ii as isize) = fmin2(v, *stv.offset(ii as isize));
                            continue 'c_7661;
                        } else {
                            itp += 1;
                            ii += 1
                        }
                    }
                    /* this happens less, now that we check for negative key above: */
                    error(dcgettext(b"stats\x00" as *const u8 as
                                                   *const libc::c_char,
                                               b"FEXACT error 30.  Stack length exceeded in f3xact,\n  (ldst=%d, key=%d, ipn=%d, itp=%d, ist[ii=%d]=%d).\nIncrease workspace or consider using \'simulate.p.value=TRUE\'\x00"
                                                   as *const u8 as
                                                   *const libc::c_char,
                                               5 as libc::c_int), ldst, key,
                                     ipn, itp, ii, *ist.offset(ii as isize));
                }
            }
        }
        loop
        /* Pop item from stack */
        {
            if nitc > 0 as libc::c_int {
                /* Stack index */
                itp = *itc.offset((nitc + k) as isize) + k;
                nitc -= 1;
                val = *stv.offset(itp as isize);
                key = *ist.offset(itp as isize);
                *ist.offset(itp as isize) = -(1 as libc::c_int);
                /* Compute marginals */
                i = nco;
                while i >= 2 as libc::c_int {
                    *ico.offset(i as isize) = key % kyy;
                    key /= kyy;
                    i -= 1
                }
                *ico.offset(1 as libc::c_int as isize) = key;
                /* Set up nt array */
                *nt.offset(1 as libc::c_int as isize) = nn - *ico.offset(1 as libc::c_int as isize);
                i = 2 as libc::c_int;
                while i <= nco {
                    *nt.offset(i as isize) =
                        *nt.offset((i - 1 as libc::c_int) as isize) - *ico.offset(i as isize);
                    i += 1
                }
                /* Test for optimality (L90) */
                if *iro.offset(nro as isize) <= *iro.offset(irl as isize) + nco {
                    xmin = f10act(
                        nro,
                        &mut *iro.offset(irl as isize) as *mut libc::c_int as *const libc::c_int,
                        nco,
                        &mut *ico.offset(1 as libc::c_int as isize) as *mut libc::c_int
                            as *const libc::c_int,
                        &mut val,
                        fact,
                        &mut *lb.offset(1 as libc::c_int as isize),
                        &mut *nu.offset(1 as libc::c_int as isize),
                        &mut *nr.offset(1 as libc::c_int as isize),
                    )
                } else {
                    xmin = FALSE
                }
                if xmin as u64 == 0
                    && *ico.offset(nco as isize) <= *ico.offset(1 as libc::c_int as isize) + nro
                {
                    xmin = f10act(
                        nco,
                        &mut *ico.offset(1 as libc::c_int as isize) as *mut libc::c_int
                            as *const libc::c_int,
                        nro,
                        &mut *iro.offset(irl as isize) as *mut libc::c_int as *const libc::c_int,
                        &mut val,
                        fact,
                        &mut *lb.offset(1 as libc::c_int as isize),
                        &mut *nu.offset(1 as libc::c_int as isize),
                        &mut *nr.offset(1 as libc::c_int as isize),
                    )
                }
                if !(xmin as u64 != 0) {
                    continue 'c_7655;
                }
                if vmn > val {
                    vmn = val
                }
            } else {
                if !(nro > 2 as libc::c_int && nst > 0 as libc::c_int) {
                    break;
                }
                /* Go to next level */
                nitc = nst;
                nst = 0 as libc::c_int;
                k = ks;
                ks = ldst - ks;
                nn -= *iro.offset(irl as isize);
                irl += 1;
                nro -= 1
            }
        }
        // else
        return -vmn;
    }
}
// f3xact()
unsafe extern "C" fn f4xact(
    mut nrow: libc::c_int,
    mut irow: *mut libc::c_int,
    mut ncol: libc::c_int,
    mut icol: *mut libc::c_int,
    mut dspt: libc::c_double,
    mut fact: *const libc::c_double,
    mut icstk: *mut libc::c_int,
    mut ncstk: *mut libc::c_int,
    mut lstk: *mut libc::c_int,
    mut mstk: *mut libc::c_int,
    mut nstk: *mut libc::c_int,
    mut nrstk: *mut libc::c_int,
    mut irstk: *mut libc::c_int,
    mut ystk: *mut libc::c_double,
    mut tol: libc::c_double,
) -> libc::c_double {
    /*
    -----------------------------------------------------------------------
    Name:       F4XACT
    Purpose:    Computes the shortest path length for a given table.

    Arguments:
       NROW   - The number of rows in the table. (Input)
       IROW   - Vector of length NROW containing the row sums for the
            table.  (Input)
       NCOL   - The number of columns in the table.  (Input)
       ICOL   - Vector of length K containing the column sums for the
            table.  (Input)
       DSPT   - "offset"  for SP computation
       FACT   - Vector containing the logarithms of factorials.  (Input)
       ICSTK  - NCOL by NROW+NCOL+1 work array.
       NCSTK  - Work vector of length NROW+NCOL+1.
       LSTK   - Work vector of length NROW+NCOL+1.
       MSTK   - Work vector of length NROW+NCOL+1.
       NSTK   - Work vector of length NROW+NCOL+1.
       NRSTK  - Work vector of length NROW+NCOL+1.
       YSTK   - Work vector of length NROW+NCOL+1.
       IRSTK  - NROW by MAX(NROW,NCOL) work array.
       TOL    - Tolerance.     (Input)

    Return Value :

      SP     - The shortest path for the table.   (Output)
    -----------------------------------------------------------------------
    */
    /* Local variables */
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut l: libc::c_int = 0;
    let mut m: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut ic1: libc::c_int = 0;
    let mut ir1: libc::c_int = 0;
    let mut ict: libc::c_int = 0;
    let mut irt: libc::c_int = 0;
    let mut istk: libc::c_int = 0;
    let mut nco: libc::c_int = 0;
    let mut nro: libc::c_int = 0;
    let mut y: libc::c_double = 0.;
    let mut amx: libc::c_double = 0.;
    let mut SP: libc::c_double = 0.;
    /* Take care of the easy cases first */
    if nrow == 1 as libc::c_int {
        SP = 0.0f64;
        i = 0 as libc::c_int;
        while i < ncol {
            SP -= *fact.offset(*icol.offset(i as isize) as isize);
            i += 1
        }
        return SP;
    }
    if ncol == 1 as libc::c_int {
        SP = 0.0f64;
        i = 0 as libc::c_int;
        while i < nrow {
            SP -= *fact.offset(*irow.offset(i as isize) as isize);
            i += 1
        }
        return SP;
    }
    if nrow * ncol == 4 as libc::c_int {
        if *irow.offset(1 as libc::c_int as isize) <= *icol.offset(1 as libc::c_int as isize) {
            return -(*fact.offset(*irow.offset(1 as libc::c_int as isize) as isize)
                + *fact.offset(*icol.offset(1 as libc::c_int as isize) as isize)
                + *fact.offset(
                    (*icol.offset(1 as libc::c_int as isize)
                        - *irow.offset(1 as libc::c_int as isize)) as isize,
                ));
        } else {
            return -(*fact.offset(*icol.offset(1 as libc::c_int as isize) as isize)
                + *fact.offset(*irow.offset(1 as libc::c_int as isize) as isize)
                + *fact.offset(
                    (*irow.offset(1 as libc::c_int as isize)
                        - *icol.offset(1 as libc::c_int as isize)) as isize,
                ));
        }
    }
    /* Parameter adjustments */
    irstk = irstk.offset(-((nrow + 1 as libc::c_int) as isize));
    icstk = icstk.offset(-((ncol + 1 as libc::c_int) as isize));
    nrstk = nrstk.offset(-1);
    ncstk = ncstk.offset(-1);
    lstk = lstk.offset(-1);
    mstk = mstk.offset(-1);
    nstk = nstk.offset(-1);
    ystk = ystk.offset(-1);
    /* initialization before loop */
    i = 1 as libc::c_int;
    while i <= nrow {
        *irstk.offset((i + nrow) as isize) = *irow.offset((nrow - i) as isize);
        i += 1
    }
    j = 1 as libc::c_int;
    while j <= ncol {
        *icstk.offset((j + ncol) as isize) = *icol.offset((ncol - j) as isize);
        j += 1
    }
    nro = nrow;
    nco = ncol;
    *nrstk.offset(1 as libc::c_int as isize) = nro;
    *ncstk.offset(1 as libc::c_int as isize) = nco;
    *ystk.offset(1 as libc::c_int as isize) = 0.0f64;
    y = 0.0f64;
    istk = 1 as libc::c_int;
    l = 1 as libc::c_int;
    amx = 0.0f64;
    SP = dspt;
    loop
    /* First LOOP */
    {
        ir1 = *irstk.offset((istk * nrow + 1 as libc::c_int) as isize); /* end do */
        ic1 = *icstk.offset((istk * ncol + 1 as libc::c_int) as isize);
        if ir1 > ic1 {
            if nro >= nco {
                m = nco - 1 as libc::c_int;
                n = 2 as libc::c_int
            } else {
                m = nro;
                n = 1 as libc::c_int
            }
        } else if ir1 < ic1 {
            if nro <= nco {
                m = nro - 1 as libc::c_int;
                n = 1 as libc::c_int
            } else {
                m = nco;
                n = 2 as libc::c_int
            }
        } else if nro <= nco {
            m = nro - 1 as libc::c_int;
            n = 1 as libc::c_int
        } else {
            m = nco - 1 as libc::c_int;
            n = 2 as libc::c_int
        }
        loop {
            if n == 1 as libc::c_int {
                i = l;
                j = 1 as libc::c_int
            } else {
                i = 1 as libc::c_int;
                j = l
            }
            irt = *irstk.offset((i + istk * nrow) as isize);
            ict = *icstk.offset((j + istk * ncol) as isize);
            y += *fact.offset(imin2(irt, ict) as isize);
            if irt == ict {
                nro -= 1;
                nco -= 1;
                f11act(
                    &mut *irstk.offset((istk * nrow + 1 as libc::c_int) as isize)
                        as *mut libc::c_int as *const libc::c_int,
                    i,
                    nro,
                    &mut *irstk
                        .offset(((istk + 1 as libc::c_int) * nrow + 1 as libc::c_int) as isize),
                );
                f11act(
                    &mut *icstk.offset((istk * ncol + 1 as libc::c_int) as isize)
                        as *mut libc::c_int as *const libc::c_int,
                    j,
                    nco,
                    &mut *icstk
                        .offset(((istk + 1 as libc::c_int) * ncol + 1 as libc::c_int) as isize),
                );
            } else if irt > ict {
                nco -= 1;
                f11act(
                    &mut *icstk.offset((istk * ncol + 1 as libc::c_int) as isize)
                        as *mut libc::c_int as *const libc::c_int,
                    j,
                    nco,
                    &mut *icstk
                        .offset(((istk + 1 as libc::c_int) * ncol + 1 as libc::c_int) as isize),
                );
                f8xact(
                    &mut *irstk.offset((istk * nrow + 1 as libc::c_int) as isize)
                        as *mut libc::c_int as *const libc::c_int,
                    irt - ict,
                    i,
                    nro,
                    &mut *irstk
                        .offset(((istk + 1 as libc::c_int) * nrow + 1 as libc::c_int) as isize),
                );
            } else {
                nro -= 1;
                f11act(
                    &mut *irstk.offset((istk * nrow + 1 as libc::c_int) as isize)
                        as *mut libc::c_int as *const libc::c_int,
                    i,
                    nro,
                    &mut *irstk
                        .offset(((istk + 1 as libc::c_int) * nrow + 1 as libc::c_int) as isize),
                );
                f8xact(
                    &mut *icstk.offset((istk * ncol + 1 as libc::c_int) as isize)
                        as *mut libc::c_int as *const libc::c_int,
                    ict - irt,
                    j,
                    nco,
                    &mut *icstk
                        .offset(((istk + 1 as libc::c_int) * ncol + 1 as libc::c_int) as isize),
                );
            }
            if nro == 1 as libc::c_int {
                k = 1 as libc::c_int;
                while k <= nco {
                    y += *fact.offset(
                        *icstk.offset((k + (istk + 1 as libc::c_int) * ncol) as isize) as isize,
                    );
                    k += 1
                }
            } else {
                if !(nco == 1 as libc::c_int) {
                    break;
                }
                k = 1 as libc::c_int;
                while k <= nro {
                    y += *fact.offset(
                        *irstk.offset((k + (istk + 1 as libc::c_int) * nrow) as isize) as isize,
                    );
                    k += 1
                }
            }
            /* L90:*/
            if y > amx {
                amx = y;
                if SP - amx <= tol {
                    return -dspt;
                }
            }
            's_541: loop
            /* L100: */
            {
                istk -= 1;
                if istk == 0 as libc::c_int {
                    SP -= amx;
                    if SP - amx <= tol {
                        return -dspt;
                    } else {
                        return SP - dspt;
                    }
                }
                l = *lstk.offset(istk as isize) + 1 as libc::c_int;
                /* L110: */
                while !(l > *mstk.offset(istk as isize)) {
                    n = *nstk.offset(istk as isize);
                    nro = *nrstk.offset(istk as isize);
                    nco = *ncstk.offset(istk as isize);
                    y = *ystk.offset(istk as isize);
                    if n == 1 as libc::c_int {
                        if *irstk.offset((l + istk * nrow) as isize)
                            < *irstk.offset((l - 1 as libc::c_int + istk * nrow) as isize)
                        {
                            break 's_541;
                        }
                    } else if n == 2 as libc::c_int {
                        if *icstk.offset((l + istk * ncol) as isize)
                            < *icstk.offset((l - 1 as libc::c_int + istk * ncol) as isize)
                        {
                            break 's_541;
                        }
                    }
                    l += 1
                }
            }
        }
        *lstk.offset(istk as isize) = l;
        *mstk.offset(istk as isize) = m;
        *nstk.offset(istk as isize) = n;
        istk += 1;
        *nrstk.offset(istk as isize) = nro;
        *ncstk.offset(istk as isize) = nco;
        *ystk.offset(istk as isize) = y;
        l = 1 as libc::c_int
    }
}
unsafe extern "C" fn f5xact(
    mut pastp: libc::c_double,
    mut tol: libc::c_double,
    mut kval: *mut libc::c_int,
    mut key: *mut libc::c_int,
    mut ldkey: libc::c_int,
    mut ipoin: *mut libc::c_int,
    mut stp: *mut libc::c_double,
    mut ldstp: libc::c_int,
    mut ifrq: *mut libc::c_int,
    mut npoin: *mut libc::c_int,
    mut nr: *mut libc::c_int,
    mut nl: *mut libc::c_int,
    mut ifreq: libc::c_int,
    mut itop: *mut libc::c_int,
    mut psh: Rboolean,
) {
    let mut current_block: u64;
    /*
    -----------------------------------------------------------------------
    Name:       F5XACT aka "PUT"
    Purpose:    Put node on stack in network algorithm.

    Arguments:
       PASTP  - The past path length.    (Input)
       TOL    - Tolerance for equivalence of past path lengths. (Input)
       KVAL   - Key value.     (Input)
       KEY    - Vector of length LDKEY containing the key values. (in/output)
       LDKEY  - Length of vector KEY.    (Input)
       IPOIN  - Vector of length LDKEY pointing to the
            linked list of past path lengths.   (in/output)
       STP    - Vector of length LSDTP containing the
            linked lists of past path lengths.  (in/output)
       LDSTP  - Length of vector STP.    (Input)
       IFRQ   - Vector of length LDSTP containing the past path
            frequencies.     (in/output)
       NPOIN  - Vector of length LDSTP containing the pointers to
            the next past path length.   (in/output)
       NR     - Vector of length LDSTP containing the right object
            pointers in the tree of past path lengths.        (in/output)
       NL     - Vector of length LDSTP containing the left object
            pointers in the tree of past path lengths.        (in/output)
       IFREQ  - Frequency of the current path length.             (Input)
       ITOP   - Pointer to the top of STP.   (in/output)
       PSH    - Logical.      (Input)
            If PSH is true, the past path length is found in the
            table KEY.  Otherwise the location of the past path
            length is assumed known and to have been found in
            a previous call. ==>>>>> USING "static" variables
    -----------------------------------------------------------------------
    */
    /* Local variables */
    static mut itmp: libc::c_int = 0; /* << *need* static, see PSH above */
    static mut ird: libc::c_int = 0;
    static mut ipn: libc::c_int = 0;
    static mut itp: libc::c_int = 0;
    let mut test1: libc::c_double = 0.;
    let mut test2: libc::c_double = 0.;
    /* Parameter adjustments */
    nl = nl.offset(-1);
    nr = nr.offset(-1);
    npoin = npoin.offset(-1);
    ifrq = ifrq.offset(-1);
    stp = stp.offset(-1);
    /* Function Body */
    if psh as u64 != 0 {
        /* Convert KVAL to int in range 1, ..., LDKEY. */
        ird = *kval % ldkey;
        /* Search for an unused location */
        itp = ird;
        loop {
            if !(itp < ldkey) {
                current_block = 5399440093318478209;
                break;
            }
            if *key.offset(itp as isize) == *kval {
                current_block = 9893987329238638170;
                break;
            }
            if *key.offset(itp as isize) < 0 as libc::c_int {
                current_block = 6785045624214916619;
                break;
            }
            itp += 1
        }
        match current_block {
            9893987329238638170 => {}
            _ => {
                match current_block {
                    5399440093318478209 => {
                        itp = 0 as libc::c_int;
                        loop {
                            if !(itp < ird) {
                                current_block = 15976848397966268834;
                                break;
                            }
                            if *key.offset(itp as isize) == *kval {
                                current_block = 9893987329238638170;
                                break;
                            }
                            if *key.offset(itp as isize) < 0 as libc::c_int {
                                current_block = 6785045624214916619;
                                break;
                            }
                            itp += 1
                        }
                        match current_block {
                            9893987329238638170 => {}
                            6785045624214916619 => {}
                            _ => {
                                /* Return if KEY array is full */
                                /* KH
                                prterr(6, "LDKEY is too small for this problem.\n" ... */
                                error(dcgettext(b"stats\x00" as *const u8
                                                       as *const libc::c_char,
                                                   b"FEXACT error 6 (f5xact).  LDKEY=%d is too small for this problem: kval=%d.\nTry increasing the size of the workspace.\x00"
                                                       as *const u8 as
                                                       *const libc::c_char,
                                                   5 as libc::c_int), ldkey,
                                         *kval);
                            }
                        }
                    }
                    _ => {}
                }
                match current_block {
                    9893987329238638170 => {}
                    _ => {
                        /* Update KEY */
                        *key.offset(itp as isize) = *kval;
                        *itop += 1;
                        *ipoin.offset(itp as isize) = *itop;
                        /* Return if STP array full */
                        if *itop > ldstp {
                            /* KH
                            prterr(7, "LDSTP is too small for this problem.\n" .... */
                            error(dcgettext(b"stats\x00" as *const u8 as
                                                   *const libc::c_char,
                                               b"FEXACT error 7(%s). LDSTP=%d is too small for this problem,\n  (kval=%d, itop-ldstp=%d).\nIncrease workspace or consider using \'simulate.p.value=TRUE\'.\x00"
                                                   as *const u8 as
                                                   *const libc::c_char,
                                               5 as libc::c_int),
                                     b"update key\x00" as *const u8 as
                                         *const libc::c_char, ldstp, *kval,
                                     *itop - ldstp);
                        }
                        /* Update STP, etc. */
                        *npoin.offset(*itop as isize) = -(1 as libc::c_int);
                        *nr.offset(*itop as isize) = -(1 as libc::c_int);
                        *nl.offset(*itop as isize) = -(1 as libc::c_int);
                        *stp.offset(*itop as isize) = pastp;
                        *ifrq.offset(*itop as isize) = ifreq;
                        return;
                    }
                }
            }
        }
    }
    /* Find location, if any, of pastp */
    ipn = *ipoin.offset(itp as isize);
    test1 = pastp - tol;
    test2 = pastp + tol;
    loop {
        if *stp.offset(ipn as isize) < test1 {
            ipn = *nl.offset(ipn as isize)
        } else if *stp.offset(ipn as isize) > test2 {
            ipn = *nr.offset(ipn as isize)
        } else {
            *ifrq.offset(ipn as isize) += ifreq;
            return;
        }
        if !(ipn > 0 as libc::c_int) {
            break;
        }
    }
    /* Return if STP array full */
    *itop += 1;
    if *itop > ldstp {
        // Seeing this as the "final" error, even for large workspace
        /*
        prterr(7, "LDSTP is too small for this problem.\n" ... */
        let mut ipn0: libc::c_int = *ipoin.offset(itp as isize);
        error(dcgettext(b"stats\x00" as *const u8 as *const libc::c_char,
                           b"FEXACT error 7(%s). LDSTP=%d is too small for this problem,\n  (pastp=%g, ipn_0:=ipoin[itp=%d]=%d, stp[ipn_0]=%g).\nIncrease workspace or consider using \'simulate.p.value=TRUE\'\x00"
                               as *const u8 as *const libc::c_char,
                           5 as libc::c_int),
                 b"location\x00" as *const u8 as *const libc::c_char, ldstp,
                 pastp, itp, ipn0, *stp.offset(ipn0 as isize));
        // NB: *itop -ldstp == 1 here
    }
    /* Find location to add value */
    ipn = *ipoin.offset(itp as isize);
    itmp = ipn;
    loop {
        if *stp.offset(ipn as isize) < test1 {
            itmp = ipn;
            ipn = *nl.offset(ipn as isize);
            if ipn > 0 as libc::c_int {
                continue;
            }
            /* else */
            *nl.offset(itmp as isize) = *itop;
            break;
        } else {
            if !(*stp.offset(ipn as isize) > test2) {
                break;
            }
            itmp = ipn;
            ipn = *nr.offset(ipn as isize);
            if ipn > 0 as libc::c_int {
                continue;
            }
            /* else */
            *nr.offset(itmp as isize) = *itop;
            break;
        }
    }
    /* Update STP, etc. */
    *npoin.offset(*itop as isize) = *npoin.offset(itmp as isize);
    *npoin.offset(itmp as isize) = *itop;
    *stp.offset(*itop as isize) = pastp;
    *ifrq.offset(*itop as isize) = ifreq;
    *nl.offset(*itop as isize) = -(1 as libc::c_int);
    *nr.offset(*itop as isize) = -(1 as libc::c_int);
}
unsafe extern "C" fn f6xact(
    mut nrow: libc::c_int,
    mut irow: *mut libc::c_int,
    mut kyy: *const libc::c_int,
    mut key: *mut libc::c_int,
    mut ldkey: libc::c_int,
    mut last: *mut libc::c_int,
    mut ipn: *mut libc::c_int,
) -> Rboolean {
    /*
    -----------------------------------------------------------------------
    Name:       F6XACT  aka "GET"
    Purpose:    Pop a node off the stack.

    Arguments:
      NROW    - The number of rows in the table.   (Input)
      IROW    - Vector of length nrow containing the row sums on
            output.      (Output)
      KYY     - Constant mutlipliers used in forming the hash
            table key.     (Input)
      KEY     - Vector of length LDKEY containing the hash table
            keys.      (in/output)
      LDKEY   - Length of vector KEY.    (Input)
      LAST    - Index of the last key popped off the stack. (in/output)
      IPN     - Pointer to the linked list of past path lengths. (Output)

    Return value :
      TRUE if there are no additional nodes to process.           (Output)
    -----------------------------------------------------------------------
    */
    let mut kval: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    key = key.offset(-1);
    loop {
        *last += 1;
        if *last <= ldkey {
            if *key.offset(*last as isize) < 0 as libc::c_int {
                continue;
            }
            /* Get KVAL from the stack */
            kval = *key.offset(*last as isize);
            *key.offset(*last as isize) = -(9999 as libc::c_int);
            j = nrow - 1 as libc::c_int;
            while j > 0 as libc::c_int {
                *irow.offset(j as isize) = kval / *kyy.offset(j as isize);
                kval -= *irow.offset(j as isize) * *kyy.offset(j as isize);
                j -= 1
            }
            *irow.offset(0 as libc::c_int as isize) = kval;
            *ipn = *last;
            return FALSE;
        } else {
            *last = 0 as libc::c_int;
            return TRUE;
        }
    }
}
unsafe extern "C" fn f7xact(
    mut nrow: libc::c_int,
    mut iro: *const libc::c_int,
    mut idif: *mut libc::c_int,
    mut k: *mut libc::c_int,
    mut ks: *mut libc::c_int,
) -> Rboolean {
    /*
    -----------------------------------------------------------------------
    Name:       F7XACT
    Purpose:    Generate the new nodes for given marginal totals.

    Arguments:
      NROW    - The number of rows in the table.   (Input)
      IRO     - The row marginal totals.    (Input)
      IDIF    - The column counts for the new column.  (in/output)
      K     - Indicator for the row to decrement.  (in/output)
      KS     - Indicator for the row to increment.  (in/output)

    Return Value:
            If TRUE, a new table was generated.  Otherwise,
            no additional tables could be generated.
    -----------------------------------------------------------------------
    */
    let mut m: libc::c_int = 0;
    let mut kk: libc::c_int = 0;
    let mut mm: libc::c_int = 0;
    /* Parameter adjustments */
    idif = idif.offset(-1);
    iro = iro.offset(-1);
    /* Find node which can be incremented, ks */
    if *ks == 0 as libc::c_int {
        loop {
            *ks += 1;
            if !(*idif.offset(*ks as isize) == *iro.offset(*ks as isize)) {
                break;
            }
        }
    }
    /* Find node to decrement (>ks) */
    if *idif.offset(*k as isize) > 0 as libc::c_int && *k > *ks {
        let ref mut fresh3 = *idif.offset(*k as isize);
        *fresh3 -= 1;
        loop {
            *k -= 1;
            if !(*iro.offset(*k as isize) == 0 as libc::c_int) {
                break;
            }
        }
        m = *k;
        /* Find node to increment (>=ks) */
        while *idif.offset(m as isize) >= *iro.offset(m as isize) {
            m -= 1
        }
        let ref mut fresh4 = *idif.offset(m as isize);
        *fresh4 += 1;
        /* Change ks */
        if m == *ks && *idif.offset(m as isize) == *iro.offset(m as isize) {
            *ks = *k
        }
    } else {
        's_239: {
            let mut current_block_41: u64;
            loop
            /* Check for finish */
            {
                kk = *k + 1 as libc::c_int;
                loop {
                    if !(kk <= nrow) {
                        current_block_41 = 14401909646449704462;
                        break;
                    }
                    if *idif.offset(kk as isize) > 0 as libc::c_int {
                        current_block_41 = 11649451133945740713;
                        break;
                    }
                    kk += 1
                }
                match current_block_41 {
                    14401909646449704462 => return FALSE,
                    _ => {
                        /* Reallocate counts */
                        mm = 1 as libc::c_int;
                        let mut i: libc::c_int = 1 as libc::c_int;
                        while i <= *k {
                            mm += *idif.offset(i as isize);
                            *idif.offset(i as isize) = 0 as libc::c_int;
                            i += 1
                        }
                        *k = kk;
                        loop {
                            *k -= 1;
                            m = imin2(mm, *iro.offset(*k as isize));
                            *idif.offset(*k as isize) = m;
                            mm -= m;
                            if !(mm > 0 as libc::c_int && *k != 1 as libc::c_int) {
                                break;
                            }
                        }
                        /* Check that all counts reallocated */
                        if mm > 0 as libc::c_int {
                            if kk != nrow {
                                *k = kk
                            } else {
                                return FALSE;
                            }
                        } else {
                            /* Get ks */
                            let ref mut fresh5 = *idif.offset(kk as isize);
                            *fresh5 -= 1;
                            *ks = 0 as libc::c_int;
                            loop {
                                *ks += 1;
                                if *ks > *k {
                                    return TRUE;
                                }
                                if !(*idif.offset(*ks as isize) >= *iro.offset(*ks as isize)) {
                                    break;
                                }
                            }
                            break 's_239;
                        }
                    }
                }
            }
        }
    }
    return TRUE;
}
unsafe extern "C" fn f8xact(
    mut irow: *const libc::c_int,
    mut is: libc::c_int,
    mut i1: libc::c_int,
    mut izero: libc::c_int,
    mut new: *mut libc::c_int,
) {
    /*
    -----------------------------------------------------------------------
    Name:       F8XACT
    Purpose:    Routine for reducing a vector when there is a zero
            element.
    Arguments:
       IROW   - Vector containing the row counts.   (Input)
       IS     - Indicator.     (Input)
       I1     - Indicator.     (Input)
       IZERO  - Position of the zero.    (Input)
       NEW    - Vector of new row counts.    (Output)
    -----------------------------------------------------------------------
    */
    let mut i: libc::c_int = 0;
    /* Parameter adjustments */
    new = new.offset(-1);
    irow = irow.offset(-1);
    /* Function Body */
    i = 1 as libc::c_int;
    while i < i1 {
        *new.offset(i as isize) = *irow.offset(i as isize);
        i += 1
    }
    i = i1;
    while i <= izero - 1 as libc::c_int {
        if is >= *irow.offset((i + 1 as libc::c_int) as isize) {
            break;
        }
        *new.offset(i as isize) = *irow.offset((i + 1 as libc::c_int) as isize);
        i += 1
    }
    *new.offset(i as isize) = is;
    loop {
        i += 1;
        if i > izero {
            return;
        }
        *new.offset(i as isize) = *irow.offset(i as isize)
    }
}
unsafe extern "C" fn f9xact(
    mut n: libc::c_int,
    mut ntot: libc::c_int,
    mut ir: *const libc::c_int,
    mut fact: *const libc::c_double,
) -> libc::c_double {
    /*
    -----------------------------------------------------------------------
    Name:       F9XACT
    Purpose:    Computes the log of a multinomial coefficient.

    Arguments:
       N     - Length of IR.     (Input)
       NTOT   - Number for factorial in numerator.  (Input)
       IR     - Vector of length N containing the numbers for
            the denominator of the factorial.   (Input)
       FACT   - Table of log factorials.    (Input)
    Returns:
          - The log of the multinomal coefficient.  (Output)
    -----------------------------------------------------------------------
    */
    let mut d: libc::c_double = *fact.offset(ntot as isize);
    let mut k: libc::c_int = 0 as libc::c_int;
    while k < n {
        d -= *fact.offset(*ir.offset(k as isize) as isize);
        k += 1
    }
    return d;
}
unsafe extern "C" fn f10act(
    mut nrow: libc::c_int,
    mut irow: *const libc::c_int,
    mut ncol: libc::c_int,
    mut icol: *const libc::c_int,
    mut val: *mut libc::c_double,
    mut fact: *const libc::c_double,
    mut nd: *mut libc::c_int,
    mut ne: *mut libc::c_int,
    mut m: *mut libc::c_int,
) -> Rboolean {
    /*
    -----------------------------------------------------------------------
    Name:     F10ACT
    Purpose:  Computes the shortest path length for special tables.

    Arguments:
       NROW   - The number of rows in the table.   (Input)
       IROW   - Vector of length NROW containing the row totals. (Input)
       NCOL   - The number of columns in the table.  (Input)
       ICO    - Vector of length NCOL containing the column totals.(Input)
       VAL    - The shortest path.    (in/Output)
       FACT   - Vector containing the logarithms of factorials.   (Input)
       ND     - Workspace vector of length NROW.   ((Output))
       NE     - Workspace vector of length NCOL.   ((Output))
       M     - Workspace vector of length NCOL.   ((Output))

    Returns (VAL and):
       XMIN   - TRUE  iff shortest path obtained.   (Output)
    -----------------------------------------------------------------------
    */
    let mut i: libc::c_int = 0;
    let mut is: libc::c_int = 0;
    let mut ix: libc::c_int = 0;
    i = 0 as libc::c_int;
    while i < nrow - 1 as libc::c_int {
        *nd.offset(i as isize) = 0 as libc::c_int;
        i += 1
    }
    is = *icol.offset(0 as libc::c_int as isize) / nrow;
    ix = *icol.offset(0 as libc::c_int as isize) - nrow * is;
    *ne.offset(0 as libc::c_int as isize) = is;
    *m.offset(0 as libc::c_int as isize) = ix;
    if ix != 0 as libc::c_int {
        let ref mut fresh6 = *nd.offset((ix - 1 as libc::c_int) as isize);
        *fresh6 += 1
    }
    i = 1 as libc::c_int;
    while i < ncol {
        ix = *icol.offset(i as isize) / nrow;
        *ne.offset(i as isize) = ix;
        is += ix;
        ix = *icol.offset(i as isize) - nrow * ix;
        *m.offset(i as isize) = ix;
        if ix != 0 as libc::c_int {
            let ref mut fresh7 = *nd.offset((ix - 1 as libc::c_int) as isize);
            *fresh7 += 1
        }
        i += 1
    }
    i = nrow - 3 as libc::c_int;
    while i >= 0 as libc::c_int {
        *nd.offset(i as isize) += *nd.offset((i + 1 as libc::c_int) as isize);
        i -= 1
    }
    ix = 0 as libc::c_int;
    i = nrow;
    while i >= 2 as libc::c_int {
        ix += is + *nd.offset((nrow - i) as isize) - *irow.offset((i - 1 as libc::c_int) as isize);
        if ix < 0 as libc::c_int {
            return FALSE;
        }
        i -= 1
    }
    i = 0 as libc::c_int;
    while i < ncol {
        ix = *ne.offset(i as isize);
        is = *m.offset(i as isize);
        *val += is as libc::c_double * *fact.offset((ix + 1 as libc::c_int) as isize)
            + (nrow - is) as libc::c_double * *fact.offset(ix as isize);
        i += 1
    }
    return TRUE;
}
unsafe extern "C" fn f11act(
    mut irow: *const libc::c_int,
    mut i1: libc::c_int,
    mut i2: libc::c_int,
    mut new: *mut libc::c_int,
) {
    /*
    -----------------------------------------------------------------------
    Name:       F11ACT
    Purpose:    Routine for revising row totals.

    Arguments:
       IROW   - Vector containing the row totals. (Input)
       I1     - Indicator.   (Input)
       I2     - Indicator.   (Input)
       NEW    - Vector containing the row totals. (Output)
    -----------------------------------------------------------------------
    */
    let mut i: libc::c_int = 0;
    i = 0 as libc::c_int;
    while i < i1 - 1 as libc::c_int {
        *new.offset(i as isize) = *irow.offset(i as isize);
        i += 1
    }
    i = i1;
    while i <= i2 {
        *new.offset((i - 1 as libc::c_int) as isize) = *irow.offset(i as isize);
        i += 1
    }
}
unsafe extern "C" fn prterr(mut icode: libc::c_int, mut mes: *const libc::c_char) -> ! {
    /*
    -----------------------------------------------------------------------
    Name:       prterr
    Purpose:    Print an error message and stop.

    Arguments:
       icode  - Integer code for the error message.  (Input)
       mes    - Character string containing the error message. (Input)
    -----------------------------------------------------------------------
    */
    let mut R_problem_buf: [libc::c_char; 4096] = [0; 4096];
    sprintf(
        R_problem_buf.as_mut_ptr(),
        b"FEXACT error %d.\n%s\x00" as *const u8 as *const libc::c_char,
        icode,
        mes,
    );
    error(R_problem_buf.as_mut_ptr());
}
unsafe extern "C" fn iwork(
    mut iwkmax: libc::c_int,
    mut iwkpt: *mut libc::c_int,
    mut number: libc::c_int,
    mut itype: libc::c_int,
) -> libc::c_int {
    /*
    -----------------------------------------------------------------------
    Name:       iwork
    Purpose:    Routine for allocating workspace.

    Arguments:
       iwkmax - Maximum (int) amount of workspace.  (Input)
       iwkpt  - Amount of (int) workspace currently allocated. (in/output)
       number - Number of elements of workspace desired.  (Input)
       itype  - Workspace type.     (Input)
            ITYPE  TYPE
          2    integer
          3    float
          4    double
       iwork(): Index in rwrk, dwrk, or iwrk of the beginning of
            the first free element in the workspace array. (Output)
    -----------------------------------------------------------------------
    */
    let mut i: libc::c_int = 0;
    i = *iwkpt;
    if itype == 2 as libc::c_int || itype == 3 as libc::c_int {
        *iwkpt += number
    } else {
        /* double */
        if i % 2 as libc::c_int != 0 as libc::c_int {
            i += 1
        }
        *iwkpt += number << 1 as libc::c_int;
        i /= 2 as libc::c_int
    }
    if *iwkpt > iwkmax {
        prterr(
            40 as libc::c_int,
            b"Out of workspace.\x00" as *const u8 as *const libc::c_char,
        );
    }
    return i;
}
/* not USING_R */
#[no_mangle]
pub unsafe extern "C" fn Fexact(
    mut x: SEXP,
    mut pars: SEXP,
    mut work: SEXP,
    mut smult: SEXP,
) -> SEXP {
    let mut nr: libc::c_int = nrows(x);
    let mut nc: libc::c_int = ncols(x);
    let mut ws: libc::c_int = asInteger(work);
    let mut mult: libc::c_int = asInteger(smult);
    pars = protect(coerceVector(pars, 14 as libc::c_int as SEXPTYPE));
    let mut p: libc::c_double = 0.;
    let mut prt: libc::c_double = 0.;
    let mut rp: *mut libc::c_double = REAL(pars);
    fexact(
        nr,
        nc,
        INTEGER(x) as *const libc::c_int,
        nr,
        *rp.offset(0 as libc::c_int as isize),
        *rp.offset(1 as libc::c_int as isize),
        *rp.offset(2 as libc::c_int as isize),
        &mut prt,
        &mut p,
        ws,
        mult,
    );
    /*
      fexact(int nrow, int ncol, int *table, int ldtabl,
             double expect, double percnt, double emin,
         double *prt, double *pre,
         int workspace, int mult)
    */
    unprotect(1 as libc::c_int);
    return ScalarReal(p);
}
