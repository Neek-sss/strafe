use crate::sexprec::{SEXP, SEXPREC, SEXPREC_ALIGN, SEXPTYPE, VECSEXP};
use ::libc;
extern "C" {
    pub type R_allocator;
    #[no_mangle]
    fn SET_ATTRIB(x: SEXP, v: SEXP);
    #[no_mangle]
    fn sprintf(_: *mut libc::c_char, _: *const libc::c_char, _: ...) -> libc::c_int;
    #[no_mangle]
    fn snprintf(
        _: *mut libc::c_char,
        _: libc::c_ulong,
        _: *const libc::c_char,
        _: ...
    ) -> libc::c_int;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    #[no_mangle]
    fn installTrChar(_: SEXP) -> SEXP;
    /* IEEE -Inf */
    #[no_mangle]
    static mut R_NaReal: libc::c_double;
    /* NA_REAL: IEEE */
    #[no_mangle]
    static mut R_NaInt: libc::c_int;
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2016  The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     *
     *
     * Memory Allocation (garbage collected) --- INCLUDING S compatibility ---
     */
    /* Included by R.h: API */
    /* for size_t */
    #[no_mangle]
    fn vmaxget() -> *mut libc::c_void;
    #[no_mangle]
    fn vmaxset(_: *const libc::c_void);
    #[no_mangle]
    fn SET_STRING_ELT(x: SEXP, i: R_xlen_t, v: SEXP);
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    #[no_mangle]
    fn ALTREP_LENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn ALTVEC_DATAPTR(x: SEXP) -> *mut libc::c_void;
    #[no_mangle]
    fn ALTSTRING_ELT(_: SEXP, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn R_BadLongVector(_: SEXP, _: *const libc::c_char, _: libc::c_int) -> !;
    #[no_mangle]
    fn SET_TAG(x: SEXP, y: SEXP);
    #[no_mangle]
    fn SETCAR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    fn SETCDR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    fn SETCADR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    fn SETCADDR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    static mut R_NilValue: SEXP;
    #[no_mangle]
    static mut R_MissingArg: SEXP;
    #[no_mangle]
    static mut R_ClassSymbol: SEXP;
    #[no_mangle]
    static mut R_DimNamesSymbol: SEXP;
    #[no_mangle]
    static mut R_DimSymbol: SEXP;
    #[no_mangle]
    static mut R_LevelsSymbol: SEXP;
    #[no_mangle]
    static mut R_NamesSymbol: SEXP;
    #[no_mangle]
    static mut R_RowNamesSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn PairToVectorList(x: SEXP) -> SEXP;
    #[no_mangle]
    fn asLogical(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn allocMatrix(_: SEXPTYPE, _: libc::c_int, _: libc::c_int) -> SEXP;
    #[no_mangle]
    fn allocList(_: libc::c_int) -> SEXP;
    #[no_mangle]
    fn allocVector3(_: SEXPTYPE, _: R_xlen_t, _: *mut R_allocator_t) -> SEXP;
    #[no_mangle]
    fn cons(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn duplicate(_: SEXP) -> SEXP;
    #[no_mangle]
    fn duplicated(_: SEXP, _: Rboolean) -> SEXP;
    #[no_mangle]
    fn eval(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn getAttrib(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn install(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn mkChar(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn ncols(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn nrows(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn setAttrib(_: SEXP, _: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn translateChar(_: SEXP) -> *const libc::c_char;
    #[no_mangle]
    fn translateCharUTF8(_: SEXP) -> *const libc::c_char;
    #[no_mangle]
    fn type2char(_: SEXPTYPE) -> *const libc::c_char;
    #[no_mangle]
    fn R_signal_protect_error() -> !;
    #[no_mangle]
    fn R_signal_reprotect_error(i: PROTECT_INDEX) -> !;
    #[no_mangle]
    fn strcat(_: *mut libc::c_char, _: *const libc::c_char) -> *mut libc::c_char;
    #[no_mangle]
    fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    #[no_mangle]
    fn strncmp(_: *const libc::c_char, _: *const libc::c_char, _: libc::c_ulong) -> libc::c_int;
    #[no_mangle]
    fn strlen(_: *const libc::c_char) -> libc::c_ulong;
    #[no_mangle]
    static mut R_PPStackSize: libc::c_int;
    #[no_mangle]
    static mut R_PPStackTop: libc::c_int;
    #[no_mangle]
    static mut R_PPStack: *mut SEXP;
    /* INLINE_PROTECT */
    /* from dstruct.c */
    /*  length - length of objects  */
    #[no_mangle]
    fn envlength(rho: SEXP) -> libc::c_int;
    #[no_mangle]
    fn copyMostAttribNoTs(_: SEXP, _: SEXP);
    #[no_mangle]
    fn deparse1line(_: SEXP, _: Rboolean) -> SEXP;
    #[no_mangle]
    fn mkFalse() -> SEXP;
}
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
/*, MAYBE */
/* type for length of (standard, not long) vectors etc */
pub type R_len_t = libc::c_int;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
pub type PROTECT_INDEX = libc::c_int;
pub type R_allocator_t = R_allocator;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1999-2017  The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Internal header, not installed */
/* this header is always to be included from others.
  It is only called if COMPILING_R is defined (in util.c) or
  from GNU C systems.

  There are different conventions for inlining across compilation units.
  See http://www.greenend.org.uk/rjk/2003/03/inline.html
*/
/* Probably not able to use C99 semantics in gcc < 4.3.0 */
/* Apple's gcc build >5400 (since Xcode 3.0) doesn't support GNU inline in C99 mode */
/* This section is normally only used for versions of gcc which do not
   support C99 semantics.  __GNUC_STDC_INLINE__ is defined if
   GCC is following C99 inline semantics by default: we
   switch R's usage to the older GNU semantics via attributes.
   Do this even for __GNUC_GNUC_INLINE__ to shut up warnings in 4.2.x.
   __GNUC_STDC_INLINE__ and __GNUC_GNU_INLINE__ were added in gcc 4.2.0.
*/
/* ifdef COMPILING_R */
/* C99_INLINE_SEMANTICS */
/* for strlen, strcmp */
/* define inline-able functions */
#[inline]
unsafe extern "C" fn DATAPTR(mut x: SEXP) -> *mut libc::c_void {
    if (*x).sxpinfo.alt() != 0 {
        return ALTVEC_DATAPTR(x);
    } else {
        return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void;
    };
}
#[inline]
unsafe extern "C" fn XLENGTH_EX(mut x: SEXP) -> R_xlen_t {
    return if (*x).sxpinfo.alt() as libc::c_int != 0 {
        ALTREP_LENGTH(x)
    } else {
        (*(x as VECSEXP)).vecsxp.length
    };
}
#[inline]
unsafe extern "C" fn LENGTH_EX(
    mut x: SEXP,
    mut file: *const libc::c_char,
    mut line: libc::c_int,
) -> libc::c_int {
    if x == R_NilValue {
        return 0 as libc::c_int;
    }
    let mut len: R_xlen_t = XLENGTH_EX(x);
    if len > 2147483647 as libc::c_int as libc::c_long {
        R_BadLongVector(x, file, line);
    }
    return len as libc::c_int;
}
#[inline]
unsafe extern "C" fn INTEGER0(mut x: SEXP) -> *mut libc::c_int {
    return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void
        as *mut libc::c_int;
}
#[inline]
unsafe extern "C" fn SET_SCALAR_IVAL(mut x: SEXP, mut v: libc::c_int) {
    *INTEGER0(x).offset(0 as libc::c_int as isize) = v;
}
/* if not inlining use version in memory.c with more error checking */
#[inline]
unsafe extern "C" fn STRING_ELT(mut x: SEXP, mut i: R_xlen_t) -> SEXP {
    if (*x).sxpinfo.alt() != 0 {
        return ALTSTRING_ELT(x, i);
    } else {
        let mut ps: *mut SEXP = (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize)
            as *mut libc::c_void as *mut SEXP;
        return *ps.offset(i as isize);
    };
}
#[inline]
unsafe extern "C" fn protect(mut s: SEXP) -> SEXP {
    if R_PPStackTop < R_PPStackSize {
        let fresh0 = R_PPStackTop;
        R_PPStackTop = R_PPStackTop + 1;
        let ref mut fresh1 = *R_PPStack.offset(fresh0 as isize);
        *fresh1 = s
    } else {
        R_signal_protect_error();
    }
    return s;
}
#[inline]
unsafe extern "C" fn unprotect(mut l: libc::c_int) {
    R_PPStackTop -= l;
}
#[inline]
unsafe extern "C" fn R_ProtectWithIndex(mut s: SEXP, mut pi: *mut PROTECT_INDEX) {
    protect(s);
    *pi = R_PPStackTop - 1 as libc::c_int;
}
#[inline]
unsafe extern "C" fn R_Reprotect(mut s: SEXP, mut i: PROTECT_INDEX) {
    if i >= R_PPStackTop || i < 0 as libc::c_int {
        R_signal_reprotect_error(i);
    }
    let ref mut fresh2 = *R_PPStack.offset(i as isize);
    *fresh2 = s;
}
/* TODO: a  Length(.) {say} which is  length() + dispatch (S3 + S4) if needed
         for one approach, see do_seq_along() in ../main/seq.c
*/
#[inline]
unsafe extern "C" fn length(mut s: SEXP) -> R_len_t {
    match (*s).sxpinfo.type_0() as libc::c_int {
        0 => return 0 as libc::c_int,
        10 | 13 | 14 | 15 | 16 | 9 | 19 | 20 | 24 => {
            return LENGTH_EX(
                s,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                522 as libc::c_int,
            )
        }
        2 | 6 | 17 => {
            let mut i: libc::c_int = 0 as libc::c_int;
            while !s.is_null() && s != R_NilValue {
                i += 1;
                s = (*s).u.listsxp.cdrval
            }
            return i;
        }
        4 => return envlength(s),
        _ => return 1 as libc::c_int,
    };
}
/* regular allocVector() as a special case of allocVector3() with no custom allocator */
#[inline]
unsafe extern "C" fn allocVector(mut type_0: SEXPTYPE, mut length: R_xlen_t) -> SEXP {
    return allocVector3(type_0, length, 0 as *mut R_allocator_t);
}
/* Shorthands for creating small lists */
#[inline]
unsafe extern "C" fn list1(mut s: SEXP) -> SEXP {
    return cons(s, R_NilValue);
}
#[inline]
unsafe extern "C" fn list2(mut s: SEXP, mut t: SEXP) -> SEXP {
    protect(s);
    s = cons(s, list1(t));
    unprotect(1 as libc::c_int);
    return s;
}
#[inline]
unsafe extern "C" fn list3(mut s: SEXP, mut t: SEXP, mut u: SEXP) -> SEXP {
    protect(s);
    s = cons(s, list2(t, u));
    unprotect(1 as libc::c_int);
    return s;
}
#[inline]
unsafe extern "C" fn list4(mut s: SEXP, mut t: SEXP, mut u: SEXP, mut v: SEXP) -> SEXP {
    protect(s);
    s = cons(s, list3(t, u, v));
    unprotect(1 as libc::c_int);
    return s;
}
/* Destructive list append : See also ``append'' */
#[inline]
unsafe extern "C" fn listAppend(mut s: SEXP, mut t: SEXP) -> SEXP {
    let mut r: SEXP = 0 as *mut SEXPREC;
    if s == R_NilValue {
        return t;
    }
    r = s;
    while (*r).u.listsxp.cdrval != R_NilValue {
        r = (*r).u.listsxp.cdrval
    }
    SETCDR(r, t);
    return s;
}
/* Language based list constructs.  These are identical to the list */
/* constructs, but the results can be evaluated. */
/* Return a (language) dotted pair with the given car and cdr */
#[inline]
unsafe extern "C" fn lcons(mut car: SEXP, mut cdr: SEXP) -> SEXP {
    let mut e: SEXP = cons(car, cdr);
    (*e).sxpinfo.set_type_0(6 as libc::c_int as SEXPTYPE);
    return e;
}
#[inline]
unsafe extern "C" fn lang2(mut s: SEXP, mut t: SEXP) -> SEXP {
    protect(s);
    s = lcons(s, list1(t));
    unprotect(1 as libc::c_int);
    return s;
}
#[inline]
unsafe extern "C" fn lang3(mut s: SEXP, mut t: SEXP, mut u: SEXP) -> SEXP {
    protect(s);
    s = lcons(s, list2(t, u));
    unprotect(1 as libc::c_int);
    return s;
}
/* NOTE: R's inherits() is based on inherits3() in ../main/objects.c
 * Here, use char / CHAR() instead of the slower more general translateChar()
 */
#[inline]
unsafe extern "C" fn inherits(mut s: SEXP, mut name: *const libc::c_char) -> Rboolean {
    let mut klass: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut nclass: libc::c_int = 0;
    if (*s).sxpinfo.obj() != 0 {
        klass = getAttrib(s, R_ClassSymbol);
        nclass = length(klass);
        i = 0 as libc::c_int;
        while i < nclass {
            if strcmp(
                (STRING_ELT(klass, i as R_xlen_t) as *mut SEXPREC_ALIGN)
                    .offset(1 as libc::c_int as isize) as *mut libc::c_void
                    as *const libc::c_char,
                name,
            ) == 0
            {
                return TRUE;
            }
            i += 1
        }
    }
    return FALSE;
}
#[inline]
unsafe extern "C" fn isList(mut s: SEXP) -> Rboolean {
    return (s == R_NilValue || (*s).sxpinfo.type_0() as libc::c_int == 2 as libc::c_int)
        as libc::c_int as Rboolean;
}
#[inline]
unsafe extern "C" fn isNewList(mut s: SEXP) -> Rboolean {
    return (s == R_NilValue || (*s).sxpinfo.type_0() as libc::c_int == 19 as libc::c_int)
        as libc::c_int as Rboolean;
}
#[inline]
unsafe extern "C" fn isVector(mut s: SEXP) -> Rboolean
/* === isVectorList() or isVectorAtomic() */ {
    match (*s).sxpinfo.type_0() as libc::c_int {
        10 | 13 | 14 | 15 | 16 | 24 | 19 | 20 => return TRUE,
        _ => return FALSE,
    };
}
#[inline]
unsafe extern "C" fn isFrame(mut s: SEXP) -> Rboolean {
    let mut klass: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    if (*s).sxpinfo.obj() != 0 {
        klass = getAttrib(s, R_ClassSymbol);
        i = 0 as libc::c_int;
        while i < length(klass) {
            if strcmp(
                (STRING_ELT(klass, i as R_xlen_t) as *mut SEXPREC_ALIGN)
                    .offset(1 as libc::c_int as isize) as *mut libc::c_void
                    as *const libc::c_char,
                b"data.frame\x00" as *const u8 as *const libc::c_char,
            ) == 0
            {
                return TRUE;
            }
            i += 1
        }
    }
    return FALSE;
}
/* DIFFERENT than R's  is.language(.) in ../main/coerce.c [do_is(), case 301:]
 *                                    which is   <=>  SYMSXP || LANGSXP || EXPRSXP */
#[inline]
unsafe extern "C" fn isLanguage(mut s: SEXP) -> Rboolean {
    return (s == R_NilValue || (*s).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int)
        as libc::c_int as Rboolean;
}
#[inline]
unsafe extern "C" fn isMatrix(mut s: SEXP) -> Rboolean {
    let mut t: SEXP = 0 as *mut SEXPREC;
    if isVector(s) as u64 != 0 {
        t = getAttrib(s, R_DimSymbol);
        /* You are not supposed to be able to assign a non-integer dim,
        although this might be possible by misuse of ATTRIB. */
        if (*t).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
            && LENGTH_EX(
                t,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                902 as libc::c_int,
            ) == 2 as libc::c_int
        {
            return TRUE;
        }
    }
    return FALSE;
}
#[inline]
unsafe extern "C" fn isInteger(mut s: SEXP) -> Rboolean {
    return ((*s).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
        && inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as u64 == 0)
        as libc::c_int as Rboolean;
}
#[inline]
unsafe extern "C" fn isFactor(mut s: SEXP) -> Rboolean {
    return ((*s).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
        && inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as libc::c_uint != 0)
        as libc::c_int as Rboolean;
}
#[inline]
unsafe extern "C" fn nlevels(mut f: SEXP) -> libc::c_int {
    if isFactor(f) as u64 == 0 {
        return 0 as libc::c_int;
    }
    return LENGTH_EX(
        getAttrib(f, R_LevelsSymbol),
        b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
        941 as libc::c_int,
    );
}
/* Is an object of numeric type. */
/* FIXME:  the LGLSXP case should be excluded here
 * (really? in many places we affirm they are treated like INTs)*/
#[inline]
unsafe extern "C" fn isNumeric(mut s: SEXP) -> Rboolean {
    match (*s).sxpinfo.type_0() as libc::c_int {
        13 => {
            if inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as u64 != 0 {
                return FALSE;
            }
        }
        10 | 14 => {}
        _ => return FALSE,
    }
    return TRUE;
}
#[inline]
unsafe extern "C" fn ScalarInteger(mut x: libc::c_int) -> SEXP {
    let mut ans: SEXP = allocVector(13 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t);
    SET_SCALAR_IVAL(ans, x);
    return ans;
}
/* from gram.y */
/* short cut for  ScalarString(mkChar(s)) : */
#[inline]
unsafe extern "C" fn mkString(mut s: *const libc::c_char) -> SEXP {
    let mut t: SEXP = 0 as *mut SEXPREC;
    t = allocVector(16 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t);
    protect(t);
    SET_STRING_ELT(t, 0 as libc::c_int as R_xlen_t, mkChar(s));
    unprotect(1 as libc::c_int);
    return t;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1997--2017  The R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* inline-able versions, used just once! */
#[inline]
unsafe extern "C" fn isUnordered_int(mut s: SEXP) -> Rboolean {
    return ((*s).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
        && inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as libc::c_uint != 0
        && inherits(s, b"ordered\x00" as *const u8 as *const libc::c_char) as u64 == 0)
        as libc::c_int as Rboolean;
}
#[inline]
unsafe extern "C" fn isOrdered_int(mut s: SEXP) -> Rboolean {
    return ((*s).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
        && inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as libc::c_uint != 0
        && inherits(s, b"ordered\x00" as *const u8 as *const libc::c_char) as libc::c_uint != 0)
        as libc::c_int as Rboolean;
}
/*
 *  model.frame
 *
 *  The argument "terms" contains the terms object generated from the
 *  model formula.  We first evaluate the "variables" attribute of
 *  "terms" in the "data" environment.  This gives us a list of basic
 *  variables to be in the model frame.  We do some basic sanity
 *  checks on these to ensure that resulting object make sense.
 *
 *  The argument "dots" gives additional things like "weights", "offsets"
 *  and "subset" which will also go into the model frame so that they can
 *  be treated in parallel.
 *
 *  Next we subset the data frame according to "subset" and finally apply
 *  "na.action" to get the final data frame.
 *
 *  Note that the "terms" argument is glued to the model frame as an
 *  attribute.  Code downstream appears to need this.
 *
 */
/* model.frame(terms, rownames, variables, varnames, */
/*             dots, dotnames, subset, na.action) */
#[no_mangle]
pub unsafe extern "C" fn modelframe(
    mut _call: SEXP,
    mut _op: SEXP,
    mut args: SEXP,
    mut rho: SEXP,
) -> SEXP {
    let mut terms: SEXP = 0 as *mut SEXPREC;
    let mut data: SEXP = 0 as *mut SEXPREC;
    let mut names: SEXP = 0 as *mut SEXPREC;
    let mut variables: SEXP = 0 as *mut SEXPREC;
    let mut varnames: SEXP = 0 as *mut SEXPREC;
    let mut dots: SEXP = 0 as *mut SEXPREC;
    let mut dotnames: SEXP = 0 as *mut SEXPREC;
    let mut na_action: SEXP = 0 as *mut SEXPREC;
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut row_names: SEXP = 0 as *mut SEXPREC;
    let mut subset: SEXP = 0 as *mut SEXPREC;
    let mut tmp: SEXP = 0 as *mut SEXPREC;
    let mut buf: [libc::c_char; 256] = [0; 256];
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut nr: libc::c_int = 0;
    let mut nc: libc::c_int = 0;
    let mut nvars: libc::c_int = 0;
    let mut ndots: libc::c_int = 0;
    let mut nactualdots: libc::c_int = 0;
    let mut vmax: *const libc::c_void = vmaxget();
    args = (*args).u.listsxp.cdrval;
    terms = (*args).u.listsxp.carval;
    args = (*args).u.listsxp.cdrval;
    row_names = (*args).u.listsxp.carval;
    args = (*args).u.listsxp.cdrval;
    variables = (*args).u.listsxp.carval;
    args = (*args).u.listsxp.cdrval;
    varnames = (*args).u.listsxp.carval;
    args = (*args).u.listsxp.cdrval;
    dots = (*args).u.listsxp.carval;
    args = (*args).u.listsxp.cdrval;
    dotnames = (*args).u.listsxp.carval;
    args = (*args).u.listsxp.cdrval;
    subset = (*args).u.listsxp.carval;
    args = (*args).u.listsxp.cdrval;
    na_action = (*args).u.listsxp.carval;
    /* Argument Sanity Checks */
    if isNewList(variables) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid variables\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if !((*varnames).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int) {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid variable names\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    nvars = length(variables);
    if nvars != length(varnames) {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"number of variables != number of variable names\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if isNewList(dots) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid extra variables\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    ndots = length(dots);
    if ndots != length(dotnames) {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"number of variables != number of variable names\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if ndots != 0 && !((*dotnames).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int) {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid extra variable names\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    /*  check for NULL extra arguments -- moved from interpreted code */
    nactualdots = 0 as libc::c_int;
    i = 0 as libc::c_int;
    while i < ndots {
        if *(DATAPTR(dots) as *mut SEXP).offset(i as isize) != R_NilValue {
            nactualdots += 1
        }
        i += 1
    }
    /* Assemble the base data frame. */
    data = allocVector(
        19 as libc::c_int as SEXPTYPE,
        (nvars + nactualdots) as R_xlen_t,
    );
    protect(data);
    names = allocVector(
        16 as libc::c_int as SEXPTYPE,
        (nvars + nactualdots) as R_xlen_t,
    );
    protect(names);
    i = 0 as libc::c_int;
    while i < nvars {
        SET_VECTOR_ELT(
            data,
            i as R_xlen_t,
            *(DATAPTR(variables) as *mut SEXP).offset(i as isize),
        );
        SET_STRING_ELT(names, i as R_xlen_t, STRING_ELT(varnames, i as R_xlen_t));
        i += 1
    }
    i = 0 as libc::c_int;
    j = 0 as libc::c_int;
    while i < ndots {
        let mut ss: *const libc::c_char = 0 as *const libc::c_char;
        if !(*(DATAPTR(dots) as *mut SEXP).offset(i as isize) == R_NilValue) {
            ss = translateChar(STRING_ELT(dotnames, i as R_xlen_t));
            if strlen(ss).wrapping_add(3 as libc::c_int as libc::c_ulong)
                > 256 as libc::c_int as libc::c_ulong
            {
                error(
                    dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"overlong names in \'%s\'\x00" as *const u8 as *const libc::c_char,
                        5 as libc::c_int,
                    ),
                    ss,
                );
            }
            snprintf(
                buf.as_mut_ptr(),
                256 as libc::c_int as libc::c_ulong,
                b"(%s)\x00" as *const u8 as *const libc::c_char,
                ss,
            );
            SET_VECTOR_ELT(
                data,
                (nvars + j) as R_xlen_t,
                *(DATAPTR(dots) as *mut SEXP).offset(i as isize),
            );
            SET_STRING_ELT(names, (nvars + j) as R_xlen_t, mkChar(buf.as_mut_ptr()));
            j += 1
        }
        i += 1
    }
    setAttrib(data, R_NamesSymbol, names);
    unprotect(2 as libc::c_int);
    /* Sanity checks to ensure that the the answer can become */
    /* a data frame.  Be deeply suspicious here! */
    nc = length(data); /* -Wall */
    nr = 0 as libc::c_int;
    if nc > 0 as libc::c_int {
        nr = nrows(*(DATAPTR(data) as *mut SEXP).offset(0 as libc::c_int as isize));
        i = 0 as libc::c_int;
        while i < nc {
            ans = *(DATAPTR(data) as *mut SEXP).offset(i as isize);
            match (*ans).sxpinfo.type_0() as libc::c_int {
                10 | 13 | 14 | 15 | 16 | 24 => {}
                _ => {
                    error(
                        dcgettext(
                            b"stats\x00" as *const u8 as *const libc::c_char,
                            b"invalid type (%s) for variable \'%s\'\x00" as *const u8
                                as *const libc::c_char,
                            5 as libc::c_int,
                        ),
                        type2char((*ans).sxpinfo.type_0()),
                        translateChar(STRING_ELT(names, i as R_xlen_t)),
                    );
                }
            }
            if nrows(ans) != nr {
                error(
                    dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"variable lengths differ (found for \'%s\')\x00" as *const u8
                            as *const libc::c_char,
                        5 as libc::c_int,
                    ),
                    translateChar(STRING_ELT(names, i as R_xlen_t)),
                );
            }
            i += 1
        }
    } else {
        nr = length(row_names)
    }
    protect(data);
    protect(subset);
    /* Turn the data "list" into a "data.frame" */
    /* so that subsetting methods will work. */
    /* To do this we must attach "class"  and */
    /* "row.names" attributes */
    tmp = mkString(b"data.frame\x00" as *const u8 as *const libc::c_char);
    protect(tmp);
    setAttrib(data, R_ClassSymbol, tmp);
    unprotect(1 as libc::c_int);
    if length(row_names) == nr {
        setAttrib(data, R_RowNamesSymbol, row_names);
    } else {
        /*
        PROTECT(row_names = allocVector(INTSXP, nr));
        for (i = 0; i < nr; i++) INTEGER(row_names)[i] = i+1; */
        row_names = allocVector(13 as libc::c_int as SEXPTYPE, 2 as libc::c_int as R_xlen_t);
        protect(row_names);
        *(DATAPTR(row_names) as *mut libc::c_int).offset(0 as libc::c_int as isize) = R_NaInt;
        *(DATAPTR(row_names) as *mut libc::c_int).offset(1 as libc::c_int as isize) = nr;
        setAttrib(data, R_RowNamesSymbol, row_names);
        unprotect(1 as libc::c_int);
    }
    /* Do the subsetting, if required. */
    /* Need to save and restore 'most' attributes */
    if subset != R_NilValue {
        tmp = install(b"[.data.frame\x00" as *const u8 as *const libc::c_char);
        protect(tmp);
        tmp = lcons(tmp, list4(data, subset, R_MissingArg, mkFalse()));
        protect(tmp);
        data = eval(tmp, rho);
        unprotect(2 as libc::c_int);
    }
    unprotect(2 as libc::c_int);
    protect(data);
    /* finally, we run na.action on the data frame */
    /* usually, this will be na.omit */
    if na_action != R_NilValue {
        /* some na.actions need this to distinguish responses from
        explanatory variables */
        setAttrib(
            data,
            install(b"terms\x00" as *const u8 as *const libc::c_char),
            terms,
        );
        if (*na_action).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int
            && length(na_action) > 0 as libc::c_int
        {
            na_action = installTrChar(STRING_ELT(na_action, 0 as libc::c_int as R_xlen_t))
        }
        protect(na_action);
        tmp = lang2(na_action, data);
        protect(tmp);
        ans = eval(tmp, rho);
        protect(ans);
        if isNewList(ans) as u64 == 0 || length(ans) != length(data) {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"invalid result from na.action\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        /* need to transfer _all but tsp and dim_ attributes, possibly lost
        by subsetting in na.action. */
        /* But if data is unchanged, don't mess with it (PR#16436) */
        i = length(ans);
        loop {
            let fresh3 = i;
            i = i - 1;
            if !(fresh3 != 0) {
                break;
            }
            if *(DATAPTR(data) as *mut SEXP).offset(i as isize)
                != *(DATAPTR(ans) as *mut SEXP).offset(i as isize)
            {
                copyMostAttribNoTs(
                    *(DATAPTR(data) as *mut SEXP).offset(i as isize),
                    *(DATAPTR(ans) as *mut SEXP).offset(i as isize),
                );
            }
        }
        unprotect(3 as libc::c_int);
    } else {
        ans = data
    }
    unprotect(1 as libc::c_int);
    protect(ans);
    /* Finally, tack on a terms attribute
    Now done at R level.
    setAttrib(ans, install("terms"), terms); */
    unprotect(1 as libc::c_int);
    vmaxset(vmax);
    return ans;
}
/* The code below is related to model expansion */
/* and is ultimately called by modelmatrix. */
unsafe extern "C" fn firstfactor(
    mut x: *mut libc::c_double,
    mut nrx: libc::c_int,
    mut _ncx: libc::c_int,
    mut c: *mut libc::c_double,
    mut nrc: libc::c_int,
    mut ncc: libc::c_int,
    mut v: *mut libc::c_int,
) {
    let mut cj: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut xj: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut j: libc::c_int = 0 as libc::c_int;
    while j < ncc {
        xj = &mut *x.offset((j as libc::c_long * nrx as R_xlen_t) as isize) as *mut libc::c_double;
        cj = &mut *c.offset((j as libc::c_long * nrc as R_xlen_t) as isize) as *mut libc::c_double;
        let mut i: libc::c_int = 0 as libc::c_int;
        while i < nrx {
            if *v.offset(i as isize) == R_NaInt {
                *xj.offset(i as isize) = R_NaReal
            } else {
                *xj.offset(i as isize) =
                    *cj.offset((*v.offset(i as isize) - 1 as libc::c_int) as isize)
            }
            i += 1
        }
        j += 1
    }
}
unsafe extern "C" fn addfactor(
    mut x: *mut libc::c_double,
    mut nrx: libc::c_int,
    mut ncx: libc::c_int,
    mut c: *mut libc::c_double,
    mut nrc: libc::c_int,
    mut ncc: libc::c_int,
    mut v: *mut libc::c_int,
) {
    let mut ck: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut xj: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut yj: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut k: libc::c_int = ncc - 1 as libc::c_int;
    while k >= 0 as libc::c_int {
        let mut j: libc::c_int = 0 as libc::c_int;
        while j < ncx {
            xj = &mut *x.offset((j as libc::c_long * nrx as R_xlen_t) as isize)
                as *mut libc::c_double;
            yj = &mut *x.offset(
                ((k as libc::c_long * ncx as R_xlen_t + j as libc::c_long) * nrx as libc::c_long)
                    as isize,
            ) as *mut libc::c_double;
            ck = &mut *c.offset((k as libc::c_long * nrc as R_xlen_t) as isize)
                as *mut libc::c_double;
            let mut i: libc::c_int = 0 as libc::c_int;
            while i < nrx {
                if *v.offset(i as isize) == R_NaInt {
                    *yj.offset(i as isize) = R_NaReal
                } else {
                    *yj.offset(i as isize) = *ck
                        .offset((*v.offset(i as isize) - 1 as libc::c_int) as isize)
                        * *xj.offset(i as isize)
                }
                i += 1
            }
            j += 1
        }
        k -= 1
    }
}
unsafe extern "C" fn firstvar(
    mut x: *mut libc::c_double,
    mut nrx: libc::c_int,
    mut _ncx: libc::c_int,
    mut c: *mut libc::c_double,
    mut nrc: libc::c_int,
    mut ncc: libc::c_int,
) {
    let mut cj: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut xj: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut j: libc::c_int = 0 as libc::c_int;
    while j < ncc {
        xj = &mut *x.offset((j as libc::c_long * nrx as R_xlen_t) as isize) as *mut libc::c_double;
        cj = &mut *c.offset((j as libc::c_long * nrc as R_xlen_t) as isize) as *mut libc::c_double;
        let mut i: libc::c_int = 0 as libc::c_int;
        while i < nrx {
            *xj.offset(i as isize) = *cj.offset(i as isize);
            i += 1
        }
        j += 1
    }
}
unsafe extern "C" fn addvar(
    mut x: *mut libc::c_double,
    mut nrx: libc::c_int,
    mut ncx: libc::c_int,
    mut c: *mut libc::c_double,
    mut nrc: libc::c_int,
    mut ncc: libc::c_int,
) {
    let mut ck: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut xj: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut yj: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut k: libc::c_int = ncc - 1 as libc::c_int;
    while k >= 0 as libc::c_int {
        let mut j: libc::c_int = 0 as libc::c_int;
        while j < ncx {
            xj = &mut *x.offset((j as libc::c_long * nrx as R_xlen_t) as isize)
                as *mut libc::c_double;
            yj = &mut *x.offset(
                ((k as libc::c_long * ncx as R_xlen_t + j as libc::c_long) * nrx as libc::c_long)
                    as isize,
            ) as *mut libc::c_double;
            ck = &mut *c.offset((k as libc::c_long * nrc as R_xlen_t) as isize)
                as *mut libc::c_double;
            let mut i: libc::c_int = 0 as libc::c_int;
            while i < nrx {
                *yj.offset(i as isize) = *ck.offset(i as isize) * *xj.offset(i as isize);
                i += 1
            }
            j += 1
        }
        k -= 1
    }
}
unsafe extern "C" fn AppendString(
    mut buf: *mut libc::c_char,
    mut str: *const libc::c_char,
) -> *mut libc::c_char {
    while *str != 0 {
        let fresh4 = str;
        str = str.offset(1);
        let fresh5 = buf;
        buf = buf.offset(1);
        *fresh5 = *fresh4
    }
    *buf = '\u{0}' as i32 as libc::c_char;
    return buf;
}
unsafe extern "C" fn AppendInteger(
    mut buf: *mut libc::c_char,
    mut i: libc::c_int,
) -> *mut libc::c_char {
    sprintf(buf, b"%d\x00" as *const u8 as *const libc::c_char, i);
    while *buf != 0 {
        buf = buf.offset(1)
    }
    return buf;
}
unsafe extern "C" fn ColumnNames(mut x: SEXP) -> SEXP {
    let mut dn: SEXP = getAttrib(x, R_DimNamesSymbol);
    if dn == R_NilValue || length(dn) < 2 as libc::c_int {
        return R_NilValue;
    } else {
        return *(DATAPTR(dn) as *mut SEXP).offset(1 as libc::c_int as isize);
    };
}
// called from R as  .Externals2(C_modelmatrix, t, data)
#[no_mangle]
pub unsafe extern "C" fn modelmatrix(
    mut _call: SEXP,
    mut _op: SEXP,
    mut args: SEXP,
    mut rho: SEXP,
) -> SEXP {
    let mut expr: SEXP = 0 as *mut SEXPREC;
    let mut factors: SEXP = 0 as *mut SEXPREC;
    let mut terms: SEXP = 0 as *mut SEXPREC;
    let mut vars: SEXP = 0 as *mut SEXPREC;
    let mut vnames: SEXP = 0 as *mut SEXPREC;
    let mut assign: SEXP = 0 as *mut SEXPREC;
    let mut xnames: SEXP = 0 as *mut SEXPREC;
    let mut tnames: SEXP = 0 as *mut SEXPREC;
    let mut rnames: SEXP = 0 as *mut SEXPREC;
    let mut count: SEXP = 0 as *mut SEXPREC;
    let mut contrast: SEXP = 0 as *mut SEXPREC;
    let mut contr1: SEXP = 0 as *mut SEXPREC;
    let mut contr2: SEXP = 0 as *mut SEXPREC;
    let mut nlevs: SEXP = 0 as *mut SEXPREC;
    let mut ordered: SEXP = 0 as *mut SEXPREC;
    let mut columns: SEXP = 0 as *mut SEXPREC;
    let mut x: SEXP = 0 as *mut SEXPREC;
    let mut variable: SEXP = 0 as *mut SEXPREC;
    let mut var_i: SEXP = 0 as *mut SEXPREC;
    let mut fik: libc::c_int = 0;
    let mut first: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut kk: libc::c_int = 0;
    let mut ll: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut nc: libc::c_int = 0;
    let mut nterms: libc::c_int = 0;
    let mut nVar: libc::c_int = 0;
    let mut intrcept: libc::c_int = 0;
    let mut jstart: libc::c_int = 0;
    let mut jnext: libc::c_int = 0;
    let mut risponse: libc::c_int = 0;
    let mut indx: libc::c_int = 0;
    let mut rhs_response: libc::c_int = 0;
    let mut dk: libc::c_double = 0.;
    let mut dnc: libc::c_double = 0.;
    let mut buf: [libc::c_char; 4096] =
        *::std::mem::transmute::<&[u8; 4096],
                                 &mut [libc::c_char; 4096]>(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00");
    let mut bufp: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut addp: *const libc::c_char = 0 as *const libc::c_char;
    let mut nn: R_xlen_t = 0;
    args = (*args).u.listsxp.cdrval;
    /* Get the "terms" structure and extract */
    /* the intercept and response attributes. */
    terms = (*args).u.listsxp.carval; // = 't' in R's calling code
    intrcept = asLogical(getAttrib(
        terms,
        install(b"intercept\x00" as *const u8 as *const libc::c_char),
    ));
    if intrcept == R_NaInt {
        intrcept = 0 as libc::c_int
    }
    risponse = asLogical(getAttrib(
        terms,
        install(b"response\x00" as *const u8 as *const libc::c_char),
    ));
    if risponse == R_NaInt {
        risponse = 0 as libc::c_int
    }
    /* Get the factor pattern matrix.  We duplicate this because */
    /* we may want to alter it if we are in the no-intercept case. */
    /* Note: the values of "nVar" and "nterms" are the REAL number of */
    /* variables in the model data frame and the number of model terms. */
    nterms = 0 as libc::c_int; /* -Wall */
    nVar = nterms;
    factors = duplicate(getAttrib(
        terms,
        install(b"factors\x00" as *const u8 as *const libc::c_char),
    ));
    protect(factors);
    if length(factors) == 0 as libc::c_int {
        /* if (intrcept == 0)
        error("invalid model (zero parameters).");*/
        nVar = 1 as libc::c_int;
        nterms = 0 as libc::c_int
    } else if isInteger(factors) as libc::c_uint != 0 && isMatrix(factors) as libc::c_uint != 0 {
        nVar = nrows(factors);
        nterms = ncols(factors)
    } else {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"invalid \'%s\' argument\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            b"terms\x00" as *const u8 as *const libc::c_char,
        );
    }
    /* Get the variable names from the factor matrix */
    vnames = getAttrib(factors, R_DimNamesSymbol);
    if length(factors) > 0 as libc::c_int {
        if length(vnames) < 1 as libc::c_int
            || nVar - intrcept > 0 as libc::c_int
                && !((**(DATAPTR(vnames) as *mut SEXP).offset(0 as libc::c_int as isize))
                    .sxpinfo
                    .type_0() as libc::c_int
                    == 16 as libc::c_int)
        {
            error(
                dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"invalid \'%s\' argument\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ),
                b"terms\x00" as *const u8 as *const libc::c_char,
            );
        }
        vnames = *(DATAPTR(vnames) as *mut SEXP).offset(0 as libc::c_int as isize)
    }
    /* Get the variables from the model frame.  First perform */
    /* elementary sanity checks.  Notes:  1) We need at least */
    /* one variable (lhs or rhs) to compute the number of cases. */
    /* 2) We don't type-check the response. */
    vars = (*(*args).u.listsxp.cdrval).u.listsxp.carval;
    if isNewList(vars) as u64 == 0 || length(vars) < nVar {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid model frame\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if length(vars) == 0 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"do not know how many cases\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    n = nrows(*(DATAPTR(vars) as *mut SEXP).offset(0 as libc::c_int as isize));
    nn = n as R_xlen_t;
    /* This could be generated, so need to protect it */
    rnames = getAttrib(vars, R_RowNamesSymbol);
    protect(rnames);
    /* This section of the code checks the types of the variables
      in the model frame.  Note that it should really only check
      the variables if they appear in a term in the model.
      Because it does not, we need to allow other types here, as they
      might well occur on the LHS.
      The R code converts all character variables in the model frame to
      factors, so the only types that ought to be here are logical,
      integer (including factor), numeric and complex.
    */
    variable = allocVector(19 as libc::c_int as SEXPTYPE, nVar as R_xlen_t);
    protect(variable);
    nlevs = allocVector(13 as libc::c_int as SEXPTYPE, nVar as R_xlen_t);
    protect(nlevs);
    ordered = allocVector(10 as libc::c_int as SEXPTYPE, nVar as R_xlen_t);
    protect(ordered);
    columns = allocVector(13 as libc::c_int as SEXPTYPE, nVar as R_xlen_t);
    protect(columns);
    i = 0 as libc::c_int;
    while i < nVar {
        var_i = SET_VECTOR_ELT(
            variable,
            i as R_xlen_t,
            *(DATAPTR(vars) as *mut SEXP).offset(i as isize),
        );
        if nrows(var_i) != n {
            error(
                dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"variable lengths differ (found for variable %d)\x00" as *const u8
                        as *const libc::c_char,
                    5 as libc::c_int,
                ),
                i,
            );
        }
        if isOrdered_int(var_i) as u64 != 0 {
            *(DATAPTR(ordered) as *mut libc::c_int).offset(i as isize) = 1 as libc::c_int;
            let ref mut fresh6 = *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize);
            *fresh6 = nlevels(var_i);
            if *fresh6 < 1 as libc::c_int {
                error(
                    dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"variable %d has no levels\x00" as *const u8 as *const libc::c_char,
                        5 as libc::c_int,
                    ),
                    i + 1 as libc::c_int,
                );
            }
            /* else
            error(_("invalid variable type for '%s'"),
            CHAR(STRING_ELT(vnames, i))); */
            /* will get updated later when contrasts are set */
            *(DATAPTR(columns) as *mut libc::c_int).offset(i as isize) = ncols(var_i)
        } else if isUnordered_int(var_i) as u64 != 0 {
            *(DATAPTR(ordered) as *mut libc::c_int).offset(i as isize) = 0 as libc::c_int;
            let ref mut fresh7 = *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize);
            *fresh7 = nlevels(var_i);
            if *fresh7 < 1 as libc::c_int {
                error(
                    dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"variable %d has no levels\x00" as *const u8 as *const libc::c_char,
                        5 as libc::c_int,
                    ),
                    i + 1 as libc::c_int,
                );
            }
            /* will get updated later when contrasts are set */
            *(DATAPTR(columns) as *mut libc::c_int).offset(i as isize) = ncols(var_i)
        } else if (*var_i).sxpinfo.type_0() as libc::c_int == 10 as libc::c_int {
            *(DATAPTR(ordered) as *mut libc::c_int).offset(i as isize) = 0 as libc::c_int;
            *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize) = 2 as libc::c_int;
            *(DATAPTR(columns) as *mut libc::c_int).offset(i as isize) = ncols(var_i)
        } else if isNumeric(var_i) as u64 != 0 {
            SET_VECTOR_ELT(
                variable,
                i as R_xlen_t,
                coerceVector(var_i, 14 as libc::c_int as SEXPTYPE),
            );
            var_i = *(DATAPTR(variable) as *mut SEXP).offset(i as isize);
            *(DATAPTR(ordered) as *mut libc::c_int).offset(i as isize) = 0 as libc::c_int;
            *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize) = 0 as libc::c_int;
            *(DATAPTR(columns) as *mut libc::c_int).offset(i as isize) = ncols(var_i)
        } else {
            *(DATAPTR(ordered) as *mut libc::c_int).offset(i as isize) = 0 as libc::c_int;
            *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize) = 0 as libc::c_int;
            *(DATAPTR(columns) as *mut libc::c_int).offset(i as isize) = ncols(var_i)
        }
        i += 1
    }
    /* If there is no intercept we look through the factor pattern */
    /* matrix and adjust the code for the first factor found so that */
    /* it will be coded by dummy variables rather than contrasts. */
    if intrcept == 0 {
        j = 0 as libc::c_int;
        's_340: while j < nterms {
            i = risponse;
            while i < nVar {
                if *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize) > 1 as libc::c_int
                    && *(DATAPTR(factors) as *mut libc::c_int).offset((i + j * nVar) as isize)
                        > 0 as libc::c_int
                {
                    *(DATAPTR(factors) as *mut libc::c_int).offset((i + j * nVar) as isize) =
                        2 as libc::c_int;
                    break 's_340;
                } else {
                    i += 1
                }
            }
            j += 1
        }
    }
    /* Compute the required contrast or dummy variable matrices. */
    /* We set up a symbolic expression to evaluate these, substituting */
    /* the required arguments at call time.  The calls have the following */
    /* form: (contrast.type nlevs contrasts) */
    contr1 = allocVector(19 as libc::c_int as SEXPTYPE, nVar as R_xlen_t);
    protect(contr1);
    contr2 = allocVector(19 as libc::c_int as SEXPTYPE, nVar as R_xlen_t);
    protect(contr2);
    expr = allocList(3 as libc::c_int);
    protect(expr);
    (*expr).sxpinfo.set_type_0(6 as libc::c_int as SEXPTYPE);
    SETCAR(
        expr,
        install(b"contrasts\x00" as *const u8 as *const libc::c_char),
    );
    SETCADDR(
        expr,
        allocVector(10 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t),
    );
    /* FIXME: We need to allow a third argument to this function */
    /* which allows us to specify contrasts directly.  That argument */
    /* would be used here in exactly the same way as the below. */
    /* I.e. we would search the list of constrast specs before */
    /* we try the evaluation below. */
    i = 0 as libc::c_int;
    while i < nVar {
        if *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize) != 0 {
            k = 0 as libc::c_int;
            j = 0 as libc::c_int;
            while j < nterms {
                if *(DATAPTR(factors) as *mut libc::c_int).offset((i + j * nVar) as isize)
                    == 1 as libc::c_int
                {
                    k |= 1 as libc::c_int
                } else if *(DATAPTR(factors) as *mut libc::c_int).offset((i + j * nVar) as isize)
                    == 2 as libc::c_int
                {
                    k |= 2 as libc::c_int
                }
                j += 1
            }
            SETCADR(expr, *(DATAPTR(variable) as *mut SEXP).offset(i as isize));
            if k & 1 as libc::c_int != 0 {
                *(DATAPTR(
                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                ) as *mut libc::c_int)
                    .offset(0 as libc::c_int as isize) = 1 as libc::c_int;
                SET_VECTOR_ELT(contr1, i as R_xlen_t, eval(expr, rho));
            }
            if k & 2 as libc::c_int != 0 {
                *(DATAPTR(
                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                ) as *mut libc::c_int)
                    .offset(0 as libc::c_int as isize) = 0 as libc::c_int;
                SET_VECTOR_ELT(contr2, i as R_xlen_t, eval(expr, rho));
            }
        }
        i += 1
    }
    /* By convention, an rhs term identical to the response generates nothing
    in the model matrix (but interactions involving the response do). */
    rhs_response = -(1 as libc::c_int);
    if risponse > 0 as libc::c_int {
        /* there is a response specified */
        j = 0 as libc::c_int;
        while j < nterms {
            if *(DATAPTR(factors) as *mut libc::c_int)
                .offset((risponse - 1 as libc::c_int + j * nVar) as isize)
                != 0
            {
                i = 0 as libc::c_int;
                k = 0 as libc::c_int;
                while i < nVar {
                    k += (*(DATAPTR(factors) as *mut libc::c_int).offset((i + j * nVar) as isize)
                        > 0 as libc::c_int) as libc::c_int;
                    i += 1
                }
                if k == 1 as libc::c_int {
                    rhs_response = j;
                    break;
                }
            }
            j += 1
        }
    }
    /* We now have everything needed to build the design matrix. */
    /* The first step is to compute the matrix size and to allocate it. */
    /* Note that "count" holds a count of how many columns there are */
    /* for each term in the model and "nc" gives the total column count. */
    count = allocVector(13 as libc::c_int as SEXPTYPE, nterms as R_xlen_t); /* need this initialised */
    protect(count); /* accumulate in a double to detect overflow */
    if intrcept != 0 {
        dnc = 1 as libc::c_int as libc::c_double
    } else {
        dnc = 0 as libc::c_int as libc::c_double
    }
    j = 0 as libc::c_int;
    while j < nterms {
        if j == rhs_response {
            warning(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"the response appeared on the right-hand side and was dropped\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ));
            *(DATAPTR(count) as *mut libc::c_int).offset(j as isize) = 0 as libc::c_int
        } else {
            dk = 1 as libc::c_int as libc::c_double;
            i = 0 as libc::c_int;
            while i < nVar {
                if *(DATAPTR(factors) as *mut libc::c_int).offset((i + j * nVar) as isize) != 0 {
                    if *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize) != 0 {
                        match *(DATAPTR(factors) as *mut libc::c_int)
                            .offset((i + j * nVar) as isize)
                        {
                            1 => {
                                dk *= ncols(*(DATAPTR(contr1) as *mut SEXP).offset(i as isize))
                                    as libc::c_double
                            }
                            2 => {
                                dk *= ncols(*(DATAPTR(contr2) as *mut SEXP).offset(i as isize))
                                    as libc::c_double
                            }
                            _ => {}
                        }
                    } else {
                        dk *= *(DATAPTR(columns) as *mut libc::c_int).offset(i as isize)
                            as libc::c_double
                    }
                }
                i += 1
            }
            if dk > 2147483647 as libc::c_int as libc::c_double {
                error(
                    dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"term %d would require %.0g columns\x00" as *const u8
                            as *const libc::c_char,
                        5 as libc::c_int,
                    ),
                    j + 1 as libc::c_int,
                    dk,
                );
            }
            *(DATAPTR(count) as *mut libc::c_int).offset(j as isize) = dk as libc::c_int;
            dnc = dnc + dk
        }
        j += 1
    }
    if dnc > 2147483647 as libc::c_int as libc::c_double {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"matrix would require %.0g columns\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            dnc,
        );
    }
    nc = dnc as libc::c_int;
    /* Record which columns of the design matrix are associated */
    /* with which model terms. */
    assign = allocVector(13 as libc::c_int as SEXPTYPE, nc as R_xlen_t);
    protect(assign);
    k = 0 as libc::c_int;
    if intrcept != 0 {
        let fresh8 = k;
        k = k + 1;
        *(DATAPTR(assign) as *mut libc::c_int).offset(fresh8 as isize) = 0 as libc::c_int
    }
    j = 0 as libc::c_int;
    while j < nterms {
        if *(DATAPTR(count) as *mut libc::c_int).offset(j as isize) <= 0 as libc::c_int {
            warning(
                dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"problem with term %d in model.matrix: no columns are assigned\x00"
                        as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ),
                j + 1 as libc::c_int,
            );
        }
        i = 0 as libc::c_int;
        while i < *(DATAPTR(count) as *mut libc::c_int).offset(j as isize) {
            let fresh9 = k;
            k = k + 1;
            *(DATAPTR(assign) as *mut libc::c_int).offset(fresh9 as isize) = j + 1 as libc::c_int;
            i += 1
        }
        j += 1
    }
    /* Create column labels for the matrix columns. */
    xnames = allocVector(16 as libc::c_int as SEXPTYPE, nc as R_xlen_t);
    protect(xnames);
    /* Here we loop over the terms in the model and, within each */
    /* term, loop over the corresponding columns of the design */
    /* matrix, assembling the names. */
    /* FIXME : The body within these two loops should be embedded */
    /* in its own function. */
    k = 0 as libc::c_int;
    if intrcept != 0 {
        let fresh10 = k;
        k = k + 1;
        SET_STRING_ELT(
            xnames,
            fresh10 as R_xlen_t,
            mkChar(b"(Intercept)\x00" as *const u8 as *const libc::c_char),
        );
    }
    j = 0 as libc::c_int;
    while j < nterms {
        if !(j == rhs_response) {
            kk = 0 as libc::c_int;
            while kk < *(DATAPTR(count) as *mut libc::c_int).offset(j as isize) {
                first = 1 as libc::c_int;
                indx = kk;
                bufp =
                    &mut *buf.as_mut_ptr().offset(0 as libc::c_int as isize) as *mut libc::c_char;
                i = 0 as libc::c_int;
                while i < nVar {
                    ll = *(DATAPTR(factors) as *mut libc::c_int).offset((i + j * nVar) as isize);
                    if ll != 0 {
                        var_i = *(DATAPTR(variable) as *mut SEXP).offset(i as isize);
                        if first == 0 {
                            bufp = AppendString(bufp, b":\x00" as *const u8 as *const libc::c_char)
                        }
                        first = 0 as libc::c_int;
                        if isFactor(var_i) as libc::c_uint != 0
                            || (*var_i).sxpinfo.type_0() as libc::c_int == 10 as libc::c_int
                        {
                            if ll == 1 as libc::c_int {
                                x = ColumnNames(*(DATAPTR(contr1) as *mut SEXP).offset(i as isize));
                                ll = ncols(*(DATAPTR(contr1) as *mut SEXP).offset(i as isize))
                            } else {
                                x = ColumnNames(*(DATAPTR(contr2) as *mut SEXP).offset(i as isize));
                                ll = ncols(*(DATAPTR(contr2) as *mut SEXP).offset(i as isize))
                            }
                            addp = translateChar(STRING_ELT(vnames, i as R_xlen_t));
                            if strlen(buf.as_mut_ptr()).wrapping_add(strlen(addp))
                                < 4096 as libc::c_int as libc::c_ulong
                            {
                                bufp = AppendString(bufp, addp)
                            } else {
                                warning(dcgettext(
                                    b"stats\x00" as *const u8 as *const libc::c_char,
                                    b"term names will be truncated\x00" as *const u8
                                        as *const libc::c_char,
                                    5 as libc::c_int,
                                ));
                            }
                            if x == R_NilValue {
                                if strlen(buf.as_mut_ptr())
                                    .wrapping_add(10 as libc::c_int as libc::c_ulong)
                                    < 4096 as libc::c_int as libc::c_ulong
                                {
                                    bufp = AppendInteger(bufp, indx % ll + 1 as libc::c_int)
                                } else {
                                    warning(dcgettext(
                                        b"stats\x00" as *const u8 as *const libc::c_char,
                                        b"term names will be truncated\x00" as *const u8
                                            as *const libc::c_char,
                                        5 as libc::c_int,
                                    ));
                                }
                            } else {
                                addp = translateChar(STRING_ELT(x, (indx % ll) as R_xlen_t));
                                if strlen(buf.as_mut_ptr()).wrapping_add(strlen(addp))
                                    < 4096 as libc::c_int as libc::c_ulong
                                {
                                    bufp = AppendString(bufp, addp)
                                } else {
                                    warning(dcgettext(
                                        b"stats\x00" as *const u8 as *const libc::c_char,
                                        b"term names will be truncated\x00" as *const u8
                                            as *const libc::c_char,
                                        5 as libc::c_int,
                                    ));
                                }
                            }
                        } else if (*var_i).sxpinfo.type_0() as libc::c_int == 15 as libc::c_int {
                            error(dcgettext(
                                b"stats\x00" as *const u8 as *const libc::c_char,
                                b"complex variables are not currently allowed in model matrices\x00"
                                    as *const u8
                                    as *const libc::c_char,
                                5 as libc::c_int,
                            ));
                        } else {
                            if isNumeric(var_i) as u64 != 0 {
                                /* numeric */
                                x = ColumnNames(var_i);
                                ll = ncols(var_i);
                                addp = translateChar(STRING_ELT(vnames, i as R_xlen_t));
                                if strlen(buf.as_mut_ptr()).wrapping_add(strlen(addp))
                                    < 4096 as libc::c_int as libc::c_ulong
                                {
                                    bufp = AppendString(bufp, addp)
                                } else {
                                    warning(dcgettext(
                                        b"stats\x00" as *const u8 as *const libc::c_char,
                                        b"term names will be truncated\x00" as *const u8
                                            as *const libc::c_char,
                                        5 as libc::c_int,
                                    ));
                                }
                                if ll > 1 as libc::c_int {
                                    if x == R_NilValue {
                                        if strlen(buf.as_mut_ptr())
                                            .wrapping_add(10 as libc::c_int as libc::c_ulong)
                                            < 4096 as libc::c_int as libc::c_ulong
                                        {
                                            bufp = AppendInteger(bufp, indx % ll + 1 as libc::c_int)
                                        } else {
                                            warning(dcgettext(
                                                b"stats\x00" as *const u8 as *const libc::c_char,
                                                b"term names will be truncated\x00" as *const u8
                                                    as *const libc::c_char,
                                                5 as libc::c_int,
                                            ));
                                        }
                                    } else {
                                        addp =
                                            translateChar(STRING_ELT(x, (indx % ll) as R_xlen_t));
                                        if strlen(buf.as_mut_ptr()).wrapping_add(strlen(addp))
                                            < 4096 as libc::c_int as libc::c_ulong
                                        {
                                            bufp = AppendString(bufp, addp)
                                        } else {
                                            warning(dcgettext(
                                                b"stats\x00" as *const u8 as *const libc::c_char,
                                                b"term names will be truncated\x00" as *const u8
                                                    as *const libc::c_char,
                                                5 as libc::c_int,
                                            ));
                                        }
                                    }
                                }
                            } else {
                                error(dcgettext(b"stats\x00" as *const u8
                                                       as *const libc::c_char,
                                                   b"variables of type \'%s\' are not allowed in model matrices\x00"
                                                       as *const u8 as
                                                       *const libc::c_char,
                                                   5 as libc::c_int),
                                         type2char((*var_i).sxpinfo.type_0()));
                            }
                        }
                        indx /= ll
                    }
                    i += 1
                }
                let fresh11 = k;
                k = k + 1;
                SET_STRING_ELT(xnames, fresh11 as R_xlen_t, mkChar(buf.as_mut_ptr()));
                kk += 1
            }
        }
        j += 1
    }
    /* Allocate and compute the design matrix. */
    x = allocMatrix(14 as libc::c_int as SEXPTYPE, n, nc);
    protect(x);
    let mut rx: *mut libc::c_double = DATAPTR(x) as *mut libc::c_double;
    /* a) Begin with a column of 1s for the intercept. */
    jstart = intrcept;
    jnext = jstart;
    if jnext != 0 as libc::c_int {
        i = 0 as libc::c_int;
        while i < n {
            *rx.offset(i as isize) = 1.0f64;
            i += 1
        }
    }
    /* b) Now loop over the model terms */
    contrast = R_NilValue; /* -Wall */
    k = 0 as libc::c_int;
    while k < nterms {
        if !(k == rhs_response) {
            i = 0 as libc::c_int;
            while i < nVar {
                if !(*(DATAPTR(columns) as *mut libc::c_int).offset(i as isize) == 0 as libc::c_int)
                {
                    var_i = *(DATAPTR(variable) as *mut SEXP).offset(i as isize);
                    fik = *(DATAPTR(factors) as *mut libc::c_int).offset((i + k * nVar) as isize);
                    if fik != 0 {
                        match fik {
                            1 => {
                                contrast = coerceVector(
                                    *(DATAPTR(contr1) as *mut SEXP).offset(i as isize),
                                    14 as libc::c_int as SEXPTYPE,
                                )
                            }
                            2 => {
                                contrast = coerceVector(
                                    *(DATAPTR(contr2) as *mut SEXP).offset(i as isize),
                                    14 as libc::c_int as SEXPTYPE,
                                )
                            }
                            _ => {}
                        }
                        protect(contrast);
                        if jnext == jstart {
                            if *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize)
                                > 0 as libc::c_int
                            {
                                let mut adj: libc::c_int = if (*var_i).sxpinfo.type_0()
                                    as libc::c_int
                                    == 10 as libc::c_int
                                {
                                    1 as libc::c_int
                                } else {
                                    0 as libc::c_int
                                };
                                // avoid overflow of jstart * nn PR#15578
                                firstfactor(
                                    &mut *rx.offset((jstart as libc::c_long * nn) as isize),
                                    n,
                                    jnext - jstart,
                                    DATAPTR(contrast) as *mut libc::c_double,
                                    nrows(contrast),
                                    ncols(contrast),
                                    (DATAPTR(var_i) as *mut libc::c_int).offset(adj as isize),
                                );
                                jnext = jnext + ncols(contrast)
                            } else {
                                firstvar(
                                    &mut *rx.offset((jstart as libc::c_long * nn) as isize),
                                    n,
                                    jnext - jstart,
                                    DATAPTR(var_i) as *mut libc::c_double,
                                    n,
                                    ncols(var_i),
                                );
                                jnext = jnext + ncols(var_i)
                            }
                        } else if *(DATAPTR(nlevs) as *mut libc::c_int).offset(i as isize)
                            > 0 as libc::c_int
                        {
                            let mut adj_0: libc::c_int =
                                if (*var_i).sxpinfo.type_0() as libc::c_int == 10 as libc::c_int {
                                    1 as libc::c_int
                                } else {
                                    0 as libc::c_int
                                };
                            addfactor(
                                &mut *rx.offset((jstart as libc::c_long * nn) as isize),
                                n,
                                jnext - jstart,
                                DATAPTR(contrast) as *mut libc::c_double,
                                nrows(contrast),
                                ncols(contrast),
                                (DATAPTR(var_i) as *mut libc::c_int).offset(adj_0 as isize),
                            );
                            jnext = jnext + (jnext - jstart) * (ncols(contrast) - 1 as libc::c_int)
                        } else {
                            addvar(
                                &mut *rx.offset((jstart as libc::c_long * nn) as isize),
                                n,
                                jnext - jstart,
                                DATAPTR(var_i) as *mut libc::c_double,
                                n,
                                ncols(var_i),
                            );
                            jnext = jnext + (jnext - jstart) * (ncols(var_i) - 1 as libc::c_int)
                        }
                        unprotect(1 as libc::c_int);
                    }
                }
                i += 1
            }
            jstart = jnext
        }
        k += 1
    }
    tnames = allocVector(19 as libc::c_int as SEXPTYPE, 2 as libc::c_int as R_xlen_t);
    protect(tnames);
    SET_VECTOR_ELT(tnames, 0 as libc::c_int as R_xlen_t, rnames);
    SET_VECTOR_ELT(tnames, 1 as libc::c_int as R_xlen_t, xnames);
    setAttrib(x, R_DimNamesSymbol, tnames);
    setAttrib(
        x,
        install(b"assign\x00" as *const u8 as *const libc::c_char),
        assign,
    );
    unprotect(14 as libc::c_int);
    return x;
}
/* Update a model formula by the replacement of "." templates. */
static mut tildeSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut plusSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut minusSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut timesSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut slashSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut colonSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut powerSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut dotSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut parenSymbol: SEXP = 0 as *const SEXPREC as SEXP;
static mut inSymbol: SEXP = 0 as *const SEXPREC as SEXP;
unsafe extern "C" fn ExpandDots(mut object: SEXP, mut value: SEXP) -> SEXP {
    let mut current_block: u64;
    let mut op: SEXP = 0 as *mut SEXPREC;
    if (*object).sxpinfo.type_0() as libc::c_int == 1 as libc::c_int {
        if object == dotSymbol {
            object = duplicate(value)
        }
        return object;
    }
    if (*object).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int {
        if (*value).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int {
            op = (*value).u.listsxp.carval
        } else {
            op = 0 as SEXP
        }
        protect(object);
        if (*object).u.listsxp.carval == plusSymbol {
            if length(object) == 2 as libc::c_int {
                SETCADR(
                    object,
                    ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                );
                current_block = 9512719473022792396;
            } else if length(object) == 3 as libc::c_int {
                SETCADR(
                    object,
                    ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                );
                SETCADDR(
                    object,
                    ExpandDots(
                        (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                            .u
                            .listsxp
                            .carval,
                        value,
                    ),
                );
                current_block = 9512719473022792396;
            } else {
                current_block = 17463173268962985150;
            }
        } else if (*object).u.listsxp.carval == minusSymbol {
            if length(object) == 2 as libc::c_int {
                if (*(*object).u.listsxp.cdrval).u.listsxp.carval == dotSymbol
                    && (op == plusSymbol || op == minusSymbol)
                {
                    SETCADR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                        ),
                    );
                } else {
                    SETCADR(
                        object,
                        ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                    );
                }
                current_block = 9512719473022792396;
            } else if length(object) == 3 as libc::c_int {
                if (*(*object).u.listsxp.cdrval).u.listsxp.carval == dotSymbol
                    && (op == plusSymbol || op == minusSymbol)
                {
                    SETCADR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                        ),
                    );
                } else {
                    SETCADR(
                        object,
                        ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                    );
                }
                if (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval
                    == dotSymbol
                    && (op == plusSymbol || op == minusSymbol)
                {
                    SETCADDR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots(
                                (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                                    .u
                                    .listsxp
                                    .carval,
                                value,
                            ),
                        ),
                    );
                } else {
                    SETCADDR(
                        object,
                        ExpandDots(
                            (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            value,
                        ),
                    );
                }
                current_block = 9512719473022792396;
            } else {
                current_block = 17463173268962985150;
            }
        } else if (*object).u.listsxp.carval == timesSymbol
            || (*object).u.listsxp.carval == slashSymbol
        {
            if length(object) != 3 as libc::c_int {
                current_block = 17463173268962985150;
            } else {
                if (*(*object).u.listsxp.cdrval).u.listsxp.carval == dotSymbol
                    && (op == plusSymbol || op == minusSymbol)
                {
                    SETCADR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                        ),
                    );
                } else {
                    SETCADR(
                        object,
                        ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                    );
                }
                if (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval
                    == dotSymbol
                    && (op == plusSymbol || op == minusSymbol)
                {
                    SETCADDR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots(
                                (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                                    .u
                                    .listsxp
                                    .carval,
                                value,
                            ),
                        ),
                    );
                } else {
                    SETCADDR(
                        object,
                        ExpandDots(
                            (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            value,
                        ),
                    );
                }
                current_block = 9512719473022792396;
            }
        } else if (*object).u.listsxp.carval == colonSymbol {
            if length(object) != 3 as libc::c_int {
                current_block = 17463173268962985150;
            } else {
                if (*(*object).u.listsxp.cdrval).u.listsxp.carval == dotSymbol
                    && (op == plusSymbol
                        || op == minusSymbol
                        || op == timesSymbol
                        || op == slashSymbol)
                {
                    SETCADR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                        ),
                    );
                } else {
                    SETCADR(
                        object,
                        ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                    );
                }
                if (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval
                    == dotSymbol
                    && (op == plusSymbol || op == minusSymbol)
                {
                    SETCADDR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots(
                                (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                                    .u
                                    .listsxp
                                    .carval,
                                value,
                            ),
                        ),
                    );
                } else {
                    SETCADDR(
                        object,
                        ExpandDots(
                            (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            value,
                        ),
                    );
                }
                current_block = 9512719473022792396;
            }
        } else if (*object).u.listsxp.carval == powerSymbol {
            if length(object) != 3 as libc::c_int {
                current_block = 17463173268962985150;
            } else {
                if (*(*object).u.listsxp.cdrval).u.listsxp.carval == dotSymbol
                    && (op == plusSymbol
                        || op == minusSymbol
                        || op == timesSymbol
                        || op == slashSymbol
                        || op == colonSymbol)
                {
                    SETCADR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                        ),
                    );
                } else {
                    SETCADR(
                        object,
                        ExpandDots((*(*object).u.listsxp.cdrval).u.listsxp.carval, value),
                    );
                }
                if (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval
                    == dotSymbol
                    && (op == plusSymbol || op == minusSymbol)
                {
                    SETCADDR(
                        object,
                        lang2(
                            parenSymbol,
                            ExpandDots(
                                (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                                    .u
                                    .listsxp
                                    .carval,
                                value,
                            ),
                        ),
                    );
                } else {
                    SETCADDR(
                        object,
                        ExpandDots(
                            (*(*(*object).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            value,
                        ),
                    );
                }
                current_block = 9512719473022792396;
            }
        } else {
            op = object;
            while op != R_NilValue {
                SETCAR(op, ExpandDots((*op).u.listsxp.carval, value));
                op = (*op).u.listsxp.cdrval
            }
            current_block = 9512719473022792396;
        }
        match current_block {
            17463173268962985150 => {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"invalid formula in \'update\'\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
            _ => {
                unprotect(1 as libc::c_int);
                return object;
            }
        }
    } else {
        return object;
    };
    /*NOTREACHED*/
}
#[no_mangle]
pub unsafe extern "C" fn updateform(mut old: SEXP, mut new: SEXP) -> SEXP {
    let mut _new: SEXP = 0 as *mut SEXPREC;
    /* Always fetch these values rather than trying */
    /* to remember them between calls.  The overhead */
    /* is minimal and we don't have to worry about */
    /* intervening dump/restore problems. */
    tildeSymbol = install(b"~\x00" as *const u8 as *const libc::c_char);
    plusSymbol = install(b"+\x00" as *const u8 as *const libc::c_char);
    minusSymbol = install(b"-\x00" as *const u8 as *const libc::c_char);
    timesSymbol = install(b"*\x00" as *const u8 as *const libc::c_char);
    slashSymbol = install(b"/\x00" as *const u8 as *const libc::c_char);
    colonSymbol = install(b":\x00" as *const u8 as *const libc::c_char);
    powerSymbol = install(b"^\x00" as *const u8 as *const libc::c_char);
    dotSymbol = install(b".\x00" as *const u8 as *const libc::c_char);
    parenSymbol = install(b"(\x00" as *const u8 as *const libc::c_char);
    inSymbol = install(b"%in%\x00" as *const u8 as *const libc::c_char);
    /* We must duplicate here because the */
    /* formulae may be part of the parse tree */
    /* and we don't want to modify it. */
    _new = duplicate(new);
    protect(_new);
    /* Check of new and old formulae. */
    if (*old).sxpinfo.type_0() as libc::c_int != 6 as libc::c_int
        || (*_new).sxpinfo.type_0() as libc::c_int != 6 as libc::c_int
            && (*old).u.listsxp.carval != tildeSymbol
        || (*_new).u.listsxp.carval != tildeSymbol
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"formula expected\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if length(old) == 3 as libc::c_int {
        let mut lhs: SEXP = (*(*old).u.listsxp.cdrval).u.listsxp.carval;
        let mut rhs: SEXP = (*(*(*old).u.listsxp.cdrval).u.listsxp.cdrval)
            .u
            .listsxp
            .carval;
        /* We now check that new formula has a valid lhs.
        If it doesn't, we add one and set it to the rhs of the old
        formula. */
        if length(_new) == 2 as libc::c_int {
            SETCDR(_new, cons(lhs, (*_new).u.listsxp.cdrval));
        }
        /* Now we check the left and right sides of the new formula
        and substitute the correct value for any "." templates.
        We must parenthesize the rhs or we might upset arity and
        precedence. */
        protect(rhs);
        SETCADR(
            _new,
            ExpandDots((*(*_new).u.listsxp.cdrval).u.listsxp.carval, lhs),
        );
        SETCADDR(
            _new,
            ExpandDots(
                (*(*(*_new).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
                rhs,
            ),
        );
        unprotect(1 as libc::c_int);
    } else {
        /* The old formula had no lhs, so we only expand the rhs of the
        new formula. */
        let mut rhs_0: SEXP = (*(*old).u.listsxp.cdrval).u.listsxp.carval;
        if length(_new) == 3 as libc::c_int {
            SETCADDR(
                _new,
                ExpandDots(
                    (*(*(*_new).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                    rhs_0,
                ),
            );
        } else {
            SETCADR(
                _new,
                ExpandDots((*(*_new).u.listsxp.cdrval).u.listsxp.carval, rhs_0),
            );
        }
    }
    /* It might be overkill to zero the */
    /* the attribute list of the returned */
    /* value, but it can't hurt. */
    SET_ATTRIB(_new, R_NilValue);
    (*_new).sxpinfo.set_obj(0 as libc::c_int as libc::c_uint);
    let mut DotEnvSymbol: SEXP = install(b".Environment\x00" as *const u8 as *const libc::c_char);
    setAttrib(_new, DotEnvSymbol, getAttrib(old, DotEnvSymbol));
    unprotect(1 as libc::c_int);
    return _new;
}
static mut intercept: libc::c_int = 0;
/* intercept term in the model */
static mut parity: libc::c_int = 0;
/* +/- parity */
static mut response: libc::c_int = 0;
/* response term in the model */
static mut nvar: libc::c_int = 0;
/* Number of variables in the formula */
static mut nwords: libc::c_int = 0;
/* # of words (ints) to code a term */
static mut nterm: libc::c_int = 0;
/* # of model terms */
static mut varlist: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
/* variables in the model */
static mut vpi: PROTECT_INDEX = 0;
static mut framenames: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
/* variables names for specified frame */
static mut haveDot: Rboolean = FALSE;
/* does RHS of formula contain `.'? */
unsafe extern "C" fn isZeroOne(mut x: SEXP) -> libc::c_int {
    if isNumeric(x) as u64 == 0 {
        return 0 as libc::c_int;
    }
    return (asReal(x) == 0.0f64 || asReal(x) == 1.0f64) as libc::c_int;
}
unsafe extern "C" fn isZero(mut x: SEXP) -> libc::c_int {
    if isNumeric(x) as u64 == 0 {
        return 0 as libc::c_int;
    }
    return (asReal(x) == 0.0f64) as libc::c_int;
}
unsafe extern "C" fn isOne(mut x: SEXP) -> libc::c_int {
    if isNumeric(x) as u64 == 0 {
        return 0 as libc::c_int;
    }
    return (asReal(x) == 1.0f64) as libc::c_int;
}
/* MatchVar determines whether two ``variables'' are */
/* identical.  Expressions are identical if they have */
/* the same list structure and their atoms are identical. */
/* This is just EQUAL from lisp. */
/* See src/main/memory.c: probably could be simplified to pointer comparison */
unsafe extern "C" fn Seql2(mut a: SEXP, mut b: SEXP) -> libc::c_int {
    if a == b {
        return 1 as libc::c_int;
    } /* discard any memory used by translateCharUTF8 */
    if (*a).sxpinfo.gp() as libc::c_int & (1 as libc::c_int) << 5 as libc::c_int != 0
        && (*b).sxpinfo.gp() as libc::c_int & (1 as libc::c_int) << 5 as libc::c_int != 0
        && (*a).sxpinfo.gp() as libc::c_int
            & ((1 as libc::c_int) << 2 as libc::c_int | (1 as libc::c_int) << 3 as libc::c_int)
            == (*b).sxpinfo.gp() as libc::c_int
                & ((1 as libc::c_int) << 2 as libc::c_int | (1 as libc::c_int) << 3 as libc::c_int)
    {
        return 0 as libc::c_int;
    } else {
        let mut vmax: *const libc::c_void = vmaxget();
        let mut result: libc::c_int =
            (strcmp(translateCharUTF8(a), translateCharUTF8(b)) == 0) as libc::c_int;
        vmaxset(vmax);
        return result;
    };
}
unsafe extern "C" fn MatchVar(mut var1: SEXP, mut var2: SEXP) -> libc::c_int {
    /* For expedience, and sanity... */
    if var1 == var2 {
        return 1 as libc::c_int;
    }
    /* Handle Nulls */
    if (*var1).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int
        && (*var2).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int
    {
        return 1 as libc::c_int;
    }
    if (*var1).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int
        || (*var2).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int
    {
        return 0 as libc::c_int;
    }
    /* Non-atomic objects - compare CARs & CDRs (and TAGs:  PR#17235) */
    if (isList(var1) as libc::c_uint != 0 || isLanguage(var1) as libc::c_uint != 0)
        && (isList(var2) as libc::c_uint != 0 || isLanguage(var2) as libc::c_uint != 0)
    {
        return (MatchVar((*var1).u.listsxp.carval, (*var2).u.listsxp.carval) != 0
            && MatchVar((*var1).u.listsxp.cdrval, (*var2).u.listsxp.cdrval) != 0
            && MatchVar((*var1).u.listsxp.tagval, (*var2).u.listsxp.tagval) != 0)
            as libc::c_int;
    }
    /* Symbols */
    if (*var1).sxpinfo.type_0() as libc::c_int == 1 as libc::c_int
        && (*var2).sxpinfo.type_0() as libc::c_int == 1 as libc::c_int
    {
        return (var1 == var2) as libc::c_int;
    }
    /* Literal Numerics */
    if isNumeric(var1) as libc::c_uint != 0 && isNumeric(var2) as libc::c_uint != 0 {
        return (asReal(var1) == asReal(var2)) as libc::c_int;
    }
    /* Literal Strings */
    if (*var1).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int
        && (*var2).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int
    {
        return Seql2(
            STRING_ELT(var1, 0 as libc::c_int as R_xlen_t),
            STRING_ELT(var2, 0 as libc::c_int as R_xlen_t),
        );
    }
    /* Nothing else matches */
    return 0 as libc::c_int;
}
/* InstallVar locates a ``variable'' in the model variable list;
adding it to the global varlist if not found. */
unsafe extern "C" fn InstallVar(mut var: SEXP) -> libc::c_int {
    let mut v: SEXP = 0 as *mut SEXPREC;
    let mut indx: libc::c_int = 0;
    /* Check that variable is legitimate */
    if !((*var).sxpinfo.type_0() as libc::c_int == 1 as libc::c_int)
        && isLanguage(var) as u64 == 0
        && isZeroOne(var) == 0
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid term in model formula\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    /* Lookup/Install it */
    indx = 0 as libc::c_int;
    v = varlist;
    while (*v).u.listsxp.cdrval != R_NilValue {
        indx += 1;
        if MatchVar(var, (*(*v).u.listsxp.cdrval).u.listsxp.carval) != 0 {
            return indx;
        }
        v = (*v).u.listsxp.cdrval
    }
    SETCDR(v, cons(var, R_NilValue));
    return indx + 1 as libc::c_int;
}
/* If there is a dotsxp being expanded then we need to see
whether any of the variables in the data frame match with
the variable on the lhs. If so they shouldn't be included
in the factors */
unsafe extern "C" fn CheckRHS(mut v: SEXP) {
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut s: SEXP = 0 as *mut SEXPREC;
    let mut t: SEXP = 0 as *mut SEXPREC;
    while (isList(v) as libc::c_uint != 0 || isLanguage(v) as libc::c_uint != 0) && v != R_NilValue
    {
        CheckRHS((*v).u.listsxp.carval);
        v = (*v).u.listsxp.cdrval
    }
    if (*v).sxpinfo.type_0() as libc::c_int == 1 as libc::c_int {
        i = 0 as libc::c_int;
        while i < length(framenames) {
            s = installTrChar(STRING_ELT(framenames, i as R_xlen_t));
            if v == s {
                t = allocVector(
                    16 as libc::c_int as SEXPTYPE,
                    (length(framenames) - 1 as libc::c_int) as R_xlen_t,
                );
                j = 0 as libc::c_int;
                while j < length(t) {
                    if j < i {
                        SET_STRING_ELT(t, j as R_xlen_t, STRING_ELT(framenames, j as R_xlen_t));
                    } else {
                        SET_STRING_ELT(
                            t,
                            j as R_xlen_t,
                            STRING_ELT(framenames, (j + 1 as libc::c_int) as R_xlen_t),
                        );
                    }
                    j += 1
                }
                framenames = t;
                R_Reprotect(framenames, vpi);
            }
            i += 1
        }
    };
}
/* ExtractVars recursively extracts the variables
in a model formula.  It calls InstallVar to do
the installation.  The code takes care of unary/
+ and minus.  No checks are made of the other
``binary'' operators.  Maybe there should be some. */
unsafe extern "C" fn ExtractVars(mut formula: SEXP, mut checkonly: libc::c_int) {
    let mut len: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut v: SEXP = 0 as *mut SEXPREC;
    if (*formula).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int || isZeroOne(formula) != 0 {
        return;
    }
    if (*formula).sxpinfo.type_0() as libc::c_int == 1 as libc::c_int {
        if formula == dotSymbol {
            haveDot = TRUE
        }
        if checkonly == 0 {
            if formula == dotSymbol && framenames != R_NilValue {
                haveDot = TRUE;
                i = 0 as libc::c_int;
                while i < length(framenames) {
                    v = installTrChar(STRING_ELT(framenames, i as R_xlen_t));
                    if MatchVar(v, (*(*varlist).u.listsxp.cdrval).u.listsxp.carval) == 0 {
                        InstallVar(v);
                    }
                    i += 1
                }
            } else {
                InstallVar(formula);
            }
        }
        return;
    }
    if isLanguage(formula) as u64 != 0 {
        len = length(formula);
        if (*formula).u.listsxp.carval == tildeSymbol {
            if response != 0 {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"invalid model formula\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
            if (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                .sxpinfo
                .type_0() as libc::c_int
                == 0 as libc::c_int
            {
                response = 0 as libc::c_int;
                ExtractVars(
                    (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                    0 as libc::c_int,
                );
            } else {
                response = 1 as libc::c_int;
                InstallVar((*(*formula).u.listsxp.cdrval).u.listsxp.carval);
                ExtractVars(
                    (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                    0 as libc::c_int,
                );
            }
            return;
        }
        if (*formula).u.listsxp.carval == plusSymbol {
            if length(formula) > 1 as libc::c_int {
                ExtractVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval, checkonly);
            }
            if length(formula) > 2 as libc::c_int {
                ExtractVars(
                    (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                    checkonly,
                );
            }
            return;
        }
        if (*formula).u.listsxp.carval == colonSymbol {
            ExtractVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval, checkonly);
            ExtractVars(
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
                checkonly,
            );
            return;
        }
        if (*formula).u.listsxp.carval == powerSymbol {
            if isNumeric(
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) as u64
                == 0
            {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"invalid power in formula\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
            ExtractVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval, checkonly);
            return;
        }
        if (*formula).u.listsxp.carval == timesSymbol {
            ExtractVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval, checkonly);
            ExtractVars(
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
                checkonly,
            );
            return;
        }
        if (*formula).u.listsxp.carval == inSymbol {
            ExtractVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval, checkonly);
            ExtractVars(
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
                checkonly,
            );
            return;
        }
        if (*formula).u.listsxp.carval == slashSymbol {
            ExtractVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval, checkonly);
            ExtractVars(
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
                checkonly,
            );
            return;
        }
        if (*formula).u.listsxp.carval == minusSymbol {
            if len == 2 as libc::c_int {
                ExtractVars(
                    (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                    1 as libc::c_int,
                );
            } else {
                ExtractVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval, checkonly);
                ExtractVars(
                    (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                    1 as libc::c_int,
                );
            }
            return;
        }
        if (*formula).u.listsxp.carval == parenSymbol {
            ExtractVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval, checkonly);
            return;
        }
        InstallVar(formula);
        return;
    }
    error(dcgettext(
        b"stats\x00" as *const u8 as *const libc::c_char,
        b"invalid model formula in ExtractVars\x00" as *const u8 as *const libc::c_char,
        5 as libc::c_int,
    ));
}
/* AllocTerm allocates an integer array for
bit string representation of a model term */
unsafe extern "C" fn AllocTerm() -> SEXP {
    let mut i: libc::c_int = 0;
    let mut term: SEXP = allocVector(13 as libc::c_int as SEXPTYPE, nwords as R_xlen_t);
    i = 0 as libc::c_int;
    while i < nwords {
        *(DATAPTR(term) as *mut libc::c_int).offset(i as isize) = 0 as libc::c_int;
        i += 1
    }
    return term;
}
/* SetBit sets bit ``whichBit'' to value ``value''
in the bit string representation of a term. */
unsafe extern "C" fn SetBit(mut term: SEXP, mut whichBit: libc::c_int, mut value: libc::c_int) {
    let mut word: libc::c_int = 0;
    let mut offset: libc::c_int = 0;
    word = ((whichBit - 1 as libc::c_int) as libc::c_ulong).wrapping_div(
        (8 as libc::c_int as libc::c_ulong)
            .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong),
    ) as libc::c_int;
    offset = (8 as libc::c_int as libc::c_ulong)
        .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong)
        .wrapping_sub(whichBit as libc::c_ulong)
        .wrapping_rem(
            (8 as libc::c_int as libc::c_ulong)
                .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong),
        ) as libc::c_int;
    if value != 0 {
        *(DATAPTR(term) as *mut libc::c_int as *mut libc::c_uint).offset(word as isize) |=
            (1 as libc::c_int as libc::c_uint) << offset
    } else {
        *(DATAPTR(term) as *mut libc::c_int as *mut libc::c_uint).offset(word as isize) &=
            !((1 as libc::c_int as libc::c_uint) << offset)
    };
}
/* GetBit gets bit ``whichBit'' from the */
/* bit string representation of a term. */
unsafe extern "C" fn GetBit(mut term: SEXP, mut whichBit: libc::c_int) -> libc::c_int {
    let mut word: libc::c_uint = 0;
    let mut offset: libc::c_uint = 0;
    word = ((whichBit - 1 as libc::c_int) as libc::c_ulong).wrapping_div(
        (8 as libc::c_int as libc::c_ulong)
            .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong),
    ) as libc::c_int as libc::c_uint;
    offset = (8 as libc::c_int as libc::c_ulong)
        .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong)
        .wrapping_sub(whichBit as libc::c_ulong)
        .wrapping_rem(
            (8 as libc::c_int as libc::c_ulong)
                .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong),
        ) as libc::c_uint;
    return (*(DATAPTR(term) as *mut libc::c_int as *mut libc::c_uint).offset(word as isize)
        >> offset
        & 1 as libc::c_int as libc::c_uint) as libc::c_int;
}
/* OrBits computes a new (bit string) term */
/* which contains the logical OR of the bits */
/* in ``term1'' and ``term2''. */
unsafe extern "C" fn OrBits(mut term1: SEXP, mut term2: SEXP) -> SEXP {
    let mut term: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    term = AllocTerm();
    i = 0 as libc::c_int;
    while i < nwords {
        *(DATAPTR(term) as *mut libc::c_int).offset(i as isize) =
            *(DATAPTR(term1) as *mut libc::c_int).offset(i as isize)
                | *(DATAPTR(term2) as *mut libc::c_int).offset(i as isize);
        i += 1
    }
    return term;
}
// BitCount counts the number of ``on'' bits in a term
unsafe extern "C" fn BitCount(mut term: SEXP) -> libc::c_int {
    let mut sum: libc::c_int = 0 as libc::c_int;
    let mut i: libc::c_int = 1 as libc::c_int;
    while i <= nvar {
        sum += GetBit(term, i);
        i += 1
    }
    return sum;
}
/* TermZero tests whether a (bit string) term is zero */
unsafe extern "C" fn TermZero(mut term: SEXP) -> libc::c_int {
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < nwords {
        if *(DATAPTR(term) as *mut libc::c_int).offset(i as isize) != 0 as libc::c_int {
            return 0 as libc::c_int;
        }
        i += 1
    }
    return 1 as libc::c_int;
}
/* TermEqual tests two (bit string) terms for equality. */
unsafe extern "C" fn TermEqual(mut term1: SEXP, mut term2: SEXP) -> libc::c_int {
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < nwords {
        if *(DATAPTR(term1) as *mut libc::c_int).offset(i as isize)
            != *(DATAPTR(term2) as *mut libc::c_int).offset(i as isize)
        {
            return 0 as libc::c_int;
        }
        i += 1
    }
    return 1 as libc::c_int;
}
/* StripTerm strips the specified term from */
/* the given list.  This mutates the list. */
unsafe extern "C" fn StripTerm(mut term: SEXP, mut list: SEXP) -> SEXP {
    let mut root: SEXP = R_NilValue;
    let mut prev: SEXP = R_NilValue;
    if TermZero(term) != 0 {
        intercept = 0 as libc::c_int
    }
    while list != R_NilValue {
        if TermEqual(term, (*list).u.listsxp.carval) != 0 {
            if prev != R_NilValue {
                SETCDR(prev, (*list).u.listsxp.cdrval);
            }
        } else {
            if root == R_NilValue {
                root = list
            }
            prev = list
        }
        list = (*list).u.listsxp.cdrval
    }
    return root;
}
/* TrimRepeats removes duplicates of (bit string) terms in a model formula.
Also drops zero terms. */
unsafe extern "C" fn TrimRepeats(mut list: SEXP) -> SEXP {
    // Drop zero terms at the start of the list.
    while list != R_NilValue && TermZero((*list).u.listsxp.carval) != 0 {
        list = (*list).u.listsxp.cdrval
    }
    if list == R_NilValue || (*list).u.listsxp.cdrval == R_NilValue {
        return list;
    }
    protect(list);
    // Find out which terms are duplicates.
    let mut all_terms: SEXP = protect(PairToVectorList(list));
    let mut duplicate_sexp: SEXP = protect(duplicated(all_terms, FALSE));
    let mut i_p1: libc::c_int = 1 as libc::c_int;
    let mut is_duplicate: *mut libc::c_int = DATAPTR(duplicate_sexp) as *mut libc::c_int;
    // Remove the zero terms and duplicates from the list.
    let mut current: SEXP = list;
    while (*current).u.listsxp.cdrval != R_NilValue {
        let mut next: SEXP = (*current).u.listsxp.cdrval;
        if *is_duplicate.offset(i_p1 as isize) != 0 || TermZero((*next).u.listsxp.carval) != 0 {
            // Remove the node from the list.
            SETCDR(current, (*next).u.listsxp.cdrval);
        } else {
            current = next
        }
        i_p1 += 1
    }
    unprotect(3 as libc::c_int);
    return list;
}
/* defined below */
/* PlusTerms expands ``left'' and ``right'' and */
/* concatenates their terms (removing duplicates). */
unsafe extern "C" fn PlusTerms(mut left: SEXP, mut right: SEXP) -> SEXP {
    left = EncodeVars(left);
    protect(left);
    right = EncodeVars(right);
    unprotect(1 as libc::c_int);
    return TrimRepeats(listAppend(left, right));
}
/* InteractTerms expands ``left'' and ``right'' */
/* and forms a new list of terms containing the bitwise */
/* OR of each term in ``left'' with each term in ``right''. */
unsafe extern "C" fn InteractTerms(mut left: SEXP, mut right: SEXP) -> SEXP {
    let mut term: SEXP = 0 as *mut SEXPREC;
    let mut l: SEXP = 0 as *mut SEXPREC;
    let mut r: SEXP = 0 as *mut SEXPREC;
    let mut t: SEXP = 0 as *mut SEXPREC;
    left = EncodeVars(left);
    protect(left);
    right = EncodeVars(right);
    protect(right);
    term = allocList(length(left) * length(right));
    protect(term);
    t = term;
    l = left;
    while l != R_NilValue {
        r = right;
        while r != R_NilValue {
            SETCAR(t, OrBits((*l).u.listsxp.carval, (*r).u.listsxp.carval));
            t = (*t).u.listsxp.cdrval;
            r = (*r).u.listsxp.cdrval
        }
        l = (*l).u.listsxp.cdrval
    }
    unprotect(3 as libc::c_int);
    return TrimRepeats(term);
}
/* CrossTerms expands ``left'' and ``right'' */
/* and forms the ``cross'' of the list of terms.  */
/* Duplicates are removed. */
unsafe extern "C" fn CrossTerms(mut left: SEXP, mut right: SEXP) -> SEXP {
    let mut term: SEXP = 0 as *mut SEXPREC;
    let mut l: SEXP = 0 as *mut SEXPREC;
    let mut r: SEXP = 0 as *mut SEXPREC;
    let mut t: SEXP = 0 as *mut SEXPREC;
    left = EncodeVars(left);
    protect(left);
    right = EncodeVars(right);
    protect(right);
    term = allocList(length(left) * length(right));
    protect(term);
    t = term;
    l = left;
    while l != R_NilValue {
        r = right;
        while r != R_NilValue {
            SETCAR(t, OrBits((*l).u.listsxp.carval, (*r).u.listsxp.carval));
            t = (*t).u.listsxp.cdrval;
            r = (*r).u.listsxp.cdrval
        }
        l = (*l).u.listsxp.cdrval
    }
    unprotect(3 as libc::c_int);
    listAppend(right, term);
    listAppend(left, right);
    return TrimRepeats(left);
}
/* PowerTerms expands the ``left'' form and then */
/* raises it to the power specified by the right term. */
/* Allocation here is wasteful, but so what ... */
unsafe extern "C" fn PowerTerms(mut left: SEXP, mut right: SEXP) -> SEXP {
    let mut term: SEXP = 0 as *mut SEXPREC; /* -Wall */
    let mut l: SEXP = 0 as *mut SEXPREC;
    let mut r: SEXP = 0 as *mut SEXPREC;
    let mut t: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut ip: libc::c_int = 0;
    ip = asInteger(right);
    if ip == R_NaInt || ip <= 1 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid power in formula\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    term = R_NilValue;
    left = EncodeVars(left);
    protect(left);
    right = left;
    i = 1 as libc::c_int;
    while i < ip {
        protect(right);
        term = allocList(length(left) * length(right));
        protect(term);
        t = term;
        l = left;
        while l != R_NilValue {
            r = right;
            while r != R_NilValue {
                SETCAR(t, OrBits((*l).u.listsxp.carval, (*r).u.listsxp.carval));
                t = (*t).u.listsxp.cdrval;
                r = (*r).u.listsxp.cdrval
            }
            l = (*l).u.listsxp.cdrval
        }
        unprotect(2 as libc::c_int);
        right = TrimRepeats(term);
        i += 1
    }
    unprotect(1 as libc::c_int);
    return term;
}
/* InTerms expands ``left'' and ``right'' and */
/* forms the ``nest'' of the the left in the */
/* interaction of the right */
unsafe extern "C" fn InTerms(mut left: SEXP, mut right: SEXP) -> SEXP {
    let mut term: SEXP = 0 as *mut SEXPREC;
    let mut t: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    left = EncodeVars(left);
    protect(left);
    right = EncodeVars(right);
    protect(right);
    term = AllocTerm();
    protect(term);
    /* Bitwise or of all terms on right */
    t = right;
    while t != R_NilValue {
        i = 0 as libc::c_int;
        while i < nwords {
            *(DATAPTR(term) as *mut libc::c_int).offset(i as isize) =
                *(DATAPTR(term) as *mut libc::c_int).offset(i as isize)
                    | *(DATAPTR((*t).u.listsxp.carval) as *mut libc::c_int).offset(i as isize);
            i += 1
        }
        t = (*t).u.listsxp.cdrval
    }
    /* Now bitwise or with each term on the left */
    t = left;
    while t != R_NilValue {
        i = 0 as libc::c_int;
        while i < nwords {
            *(DATAPTR((*t).u.listsxp.carval) as *mut libc::c_int).offset(i as isize) =
                *(DATAPTR(term) as *mut libc::c_int).offset(i as isize)
                    | *(DATAPTR((*t).u.listsxp.carval) as *mut libc::c_int).offset(i as isize);
            i += 1
        }
        t = (*t).u.listsxp.cdrval
    }
    unprotect(3 as libc::c_int);
    return TrimRepeats(left);
}
/* NestTerms expands ``left'' and ``right'' */
/* and forms the ``nest'' of the list of terms.  */
/* Duplicates are removed. */
unsafe extern "C" fn NestTerms(mut left: SEXP, mut right: SEXP) -> SEXP {
    let mut term: SEXP = 0 as *mut SEXPREC;
    let mut t: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    left = EncodeVars(left);
    protect(left);
    right = EncodeVars(right);
    protect(right);
    term = AllocTerm();
    protect(term);
    /* Bitwise or of all terms on left */
    t = left;
    while t != R_NilValue {
        i = 0 as libc::c_int;
        while i < nwords {
            *(DATAPTR(term) as *mut libc::c_int).offset(i as isize) =
                *(DATAPTR(term) as *mut libc::c_int).offset(i as isize)
                    | *(DATAPTR((*t).u.listsxp.carval) as *mut libc::c_int).offset(i as isize);
            i += 1
        }
        t = (*t).u.listsxp.cdrval
    }
    /* Now bitwise or with each term on the right */
    t = right;
    while t != R_NilValue {
        i = 0 as libc::c_int;
        while i < nwords {
            *(DATAPTR((*t).u.listsxp.carval) as *mut libc::c_int).offset(i as isize) =
                *(DATAPTR(term) as *mut libc::c_int).offset(i as isize)
                    | *(DATAPTR((*t).u.listsxp.carval) as *mut libc::c_int).offset(i as isize);
            i += 1
        }
        t = (*t).u.listsxp.cdrval
    }
    unprotect(3 as libc::c_int);
    listAppend(left, right);
    return TrimRepeats(left);
}
/* DeleteTerms expands ``left'' and ``right'' */
/* and then removes any terms which appear in */
/* ``right'' from ``left''. */
unsafe extern "C" fn DeleteTerms(mut left: SEXP, mut right: SEXP) -> SEXP {
    let mut t: SEXP = 0 as *mut SEXPREC;
    left = EncodeVars(left);
    protect(left);
    parity = 1 as libc::c_int - parity;
    right = EncodeVars(right);
    protect(right);
    parity = 1 as libc::c_int - parity;
    t = right;
    while t != R_NilValue {
        left = StripTerm((*t).u.listsxp.carval, left);
        t = (*t).u.listsxp.cdrval
    }
    unprotect(2 as libc::c_int);
    return left;
}
/*==========================================================================*/
/* Model Formula Manipulation */
/* These functions take a numerically coded */
/* formula and fully expand it. */
/* EncodeVars performs  model expansion and bit string encoding. */
/* This is the real workhorse of model expansion. */
unsafe extern "C" fn EncodeVars(mut formula: SEXP) -> SEXP {
    let mut term: SEXP = 0 as *mut SEXPREC;
    let mut len: libc::c_int = 0;
    if (*formula).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int {
        return R_NilValue;
    }
    if isOne(formula) != 0 {
        if parity != 0 {
            intercept = 1 as libc::c_int
        } else {
            intercept = 0 as libc::c_int
        }
        return R_NilValue;
    } else {
        if isZero(formula) != 0 {
            if parity != 0 {
                intercept = 0 as libc::c_int
            } else {
                intercept = 1 as libc::c_int
            }
            return R_NilValue;
        }
    }
    if (*formula).sxpinfo.type_0() as libc::c_int == 1 as libc::c_int {
        if formula == dotSymbol && framenames != R_NilValue {
            /* prior to 1.7.0 this made term.labels in reverse order. */
            let mut r: SEXP = R_NilValue; /* -Wall */
            let mut v: SEXP = R_NilValue;
            let mut i: libc::c_int = 0;
            let mut j: libc::c_int = 0;
            let mut c: *const libc::c_char = 0 as *const libc::c_char;
            let mut vmax: *const libc::c_void = vmaxget();
            if LENGTH_EX(
                framenames,
                b"model.c\x00" as *const u8 as *const libc::c_char,
                1528 as libc::c_int,
            ) == 0
            {
                return r;
            }
            i = 0 as libc::c_int;
            while i < LENGTH_EX(
                framenames,
                b"model.c\x00" as *const u8 as *const libc::c_char,
                1529 as libc::c_int,
            ) {
                /* change in 1.6.0 do not use duplicated names */
                c = translateChar(STRING_ELT(framenames, i as R_xlen_t));
                j = 0 as libc::c_int;
                while j < i {
                    if strcmp(c, translateChar(STRING_ELT(framenames, j as R_xlen_t))) == 0 {
                        error(
                            dcgettext(
                                b"stats\x00" as *const u8 as *const libc::c_char,
                                b"duplicated name \'%s\' in data frame using \'.\'\x00" as *const u8
                                    as *const libc::c_char,
                                5 as libc::c_int,
                            ),
                            c,
                        );
                    }
                    j += 1
                }
                let mut cIndex: libc::c_int = InstallVar(install(c));
                term = AllocTerm();
                SetBit(term, cIndex, 1 as libc::c_int);
                if i == 0 as libc::c_int {
                    r = cons(term, R_NilValue);
                    v = r;
                    protect(v);
                } else {
                    SETCDR(v, cons(term, R_NilValue));
                    v = (*v).u.listsxp.cdrval
                }
                i += 1
            }
            unprotect(1 as libc::c_int);
            vmaxset(vmax);
            return r;
        } else {
            let mut formulaIndex: libc::c_int = InstallVar(formula);
            term = AllocTerm();
            SetBit(term, formulaIndex, 1 as libc::c_int);
            return cons(term, R_NilValue);
        }
    }
    if isLanguage(formula) as u64 != 0 {
        len = length(formula);
        if (*formula).u.listsxp.carval == tildeSymbol {
            if (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                .sxpinfo
                .type_0() as libc::c_int
                == 0 as libc::c_int
            {
                return EncodeVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval);
            } else {
                return EncodeVars(
                    (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                );
            }
        }
        if (*formula).u.listsxp.carval == plusSymbol {
            if len == 2 as libc::c_int {
                return EncodeVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval);
            } else {
                return PlusTerms(
                    (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                    (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                );
            }
        }
        if (*formula).u.listsxp.carval == colonSymbol {
            return InteractTerms(
                (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            );
        }
        if (*formula).u.listsxp.carval == timesSymbol {
            return CrossTerms(
                (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            );
        }
        if (*formula).u.listsxp.carval == inSymbol {
            return InTerms(
                (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            );
        }
        if (*formula).u.listsxp.carval == slashSymbol {
            return NestTerms(
                (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            );
        }
        if (*formula).u.listsxp.carval == powerSymbol {
            return PowerTerms(
                (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            );
        }
        if (*formula).u.listsxp.carval == minusSymbol {
            if len == 2 as libc::c_int {
                return DeleteTerms(R_NilValue, (*(*formula).u.listsxp.cdrval).u.listsxp.carval);
            }
            return DeleteTerms(
                (*(*formula).u.listsxp.cdrval).u.listsxp.carval,
                (*(*(*formula).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            );
        }
        if (*formula).u.listsxp.carval == parenSymbol {
            return EncodeVars((*(*formula).u.listsxp.cdrval).u.listsxp.carval);
        }
        let mut formulaIndex_0: libc::c_int = InstallVar(formula);
        term = AllocTerm();
        SetBit(term, formulaIndex_0, 1 as libc::c_int);
        return cons(term, R_NilValue);
    }
    error(dcgettext(
        b"stats\x00" as *const u8 as *const libc::c_char,
        b"invalid model formula in EncodeVars\x00" as *const u8 as *const libc::c_char,
        5 as libc::c_int,
    ));
    /*NOTREACHED*/
}
/* TermCode decides on the encoding of a model term. */
/* Returns 1 if variable ``whichBit'' in ``thisTerm'' */
/* is to be encoded by contrasts and 2 if it is to be */
/* encoded by dummy variables.  This is decided using */
/* the heuristic described in Statistical Models in S, page 38. */
unsafe extern "C" fn TermCode(
    mut termlist: SEXP,
    mut thisterm: SEXP,
    mut whichbit: libc::c_int,
    mut term: SEXP,
) -> libc::c_int {
    let mut t: SEXP = 0 as *mut SEXPREC;
    let mut allzero: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    i = 0 as libc::c_int;
    while i < nwords {
        *(DATAPTR(term) as *mut libc::c_int).offset(i as isize) =
            *(DATAPTR((*thisterm).u.listsxp.carval) as *mut libc::c_int).offset(i as isize);
        i += 1
    }
    /* Eliminate factor ``whichbit'' */
    SetBit(term, whichbit, 0 as libc::c_int);
    /* Search preceding terms for a match */
    /* Zero is a possibility - it is a special case */
    allzero = 1 as libc::c_int;
    i = 0 as libc::c_int;
    while i < nwords {
        if *(DATAPTR(term) as *mut libc::c_int).offset(i as isize) != 0 {
            allzero = 0 as libc::c_int;
            break;
        } else {
            i += 1
        }
    }
    if allzero != 0 {
        return 1 as libc::c_int;
    }
    t = termlist;
    while t != thisterm {
        allzero = 1 as libc::c_int;
        i = 0 as libc::c_int;
        while i < nwords {
            if !*(DATAPTR((*t).u.listsxp.carval) as *mut libc::c_int).offset(i as isize)
                & *(DATAPTR(term) as *mut libc::c_int).offset(i as isize)
                != 0
            {
                allzero = 0 as libc::c_int
            }
            i += 1
        }
        if allzero != 0 {
            return 1 as libc::c_int;
        }
        t = (*t).u.listsxp.cdrval
    }
    return 2 as libc::c_int;
}
/* Internal code for the ``terms'' function */
/* The value is a formula with an assortment */
/* of useful attributes. */
/* terms.formula(x, new.specials, abb, data, keep.order) */
#[no_mangle]
pub unsafe extern "C" fn termsform(mut args: SEXP) -> SEXP {
    let mut a: SEXP = 0 as *mut SEXPREC;
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut v: SEXP = 0 as *mut SEXPREC;
    let mut pattern: SEXP = 0 as *mut SEXPREC;
    let mut formula: SEXP = 0 as *mut SEXPREC;
    let mut varnames: SEXP = 0 as *mut SEXPREC;
    let mut term: SEXP = 0 as *mut SEXPREC;
    let mut termlabs: SEXP = 0 as *mut SEXPREC;
    let mut ord: SEXP = 0 as *mut SEXPREC;
    let mut specials: SEXP = 0 as *mut SEXPREC;
    let mut t: SEXP = 0 as *mut SEXPREC;
    let mut data: SEXP = 0 as *mut SEXPREC;
    let mut rhs: SEXP = 0 as *mut SEXPREC;
    let mut call: SEXP = 0 as *mut SEXPREC;
    let mut keepOrder: libc::c_int = 0;
    let mut allowDot: libc::c_int = 0;
    let mut hadFrameNames: Rboolean = FALSE;
    args = (*args).u.listsxp.cdrval;
    /* Always fetch these values rather than trying to remember them
    between calls.  The overhead is minimal. */
    tildeSymbol = install(b"~\x00" as *const u8 as *const libc::c_char);
    plusSymbol = install(b"+\x00" as *const u8 as *const libc::c_char);
    minusSymbol = install(b"-\x00" as *const u8 as *const libc::c_char);
    timesSymbol = install(b"*\x00" as *const u8 as *const libc::c_char);
    slashSymbol = install(b"/\x00" as *const u8 as *const libc::c_char);
    colonSymbol = install(b":\x00" as *const u8 as *const libc::c_char);
    powerSymbol = install(b"^\x00" as *const u8 as *const libc::c_char);
    dotSymbol = install(b".\x00" as *const u8 as *const libc::c_char);
    parenSymbol = install(b"(\x00" as *const u8 as *const libc::c_char);
    inSymbol = install(b"%in%\x00" as *const u8 as *const libc::c_char);
    /* Do we have a model formula? */
    /* Check for unary or binary ~ */
    if isLanguage((*args).u.listsxp.carval) as u64 == 0
        || (*(*args).u.listsxp.carval).u.listsxp.carval != tildeSymbol
        || length((*args).u.listsxp.carval) != 2 as libc::c_int
            && length((*args).u.listsxp.carval) != 3 as libc::c_int
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"argument is not a valid model\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    haveDot = FALSE;
    ans = duplicate((*args).u.listsxp.carval);
    protect(ans);
    /* The formula will be returned, modified if haveDot becomes TRUE */
    specials = (*(*args).u.listsxp.cdrval).u.listsxp.carval;
    if length(specials) != 0 && !((*specials).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int)
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"\'specials\' must be NULL or a character vector\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    a = (*(*args).u.listsxp.cdrval).u.listsxp.cdrval;
    /* We use data to get the value to substitute for "." in formulae */
    data = (*a).u.listsxp.carval;
    a = (*a).u.listsxp.cdrval;
    if (*data).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int
        || (*data).sxpinfo.type_0() as libc::c_int == 4 as libc::c_int
    {
        framenames = R_NilValue
    } else if isFrame(data) as u64 != 0 {
        framenames = getAttrib(data, R_NamesSymbol)
    } else {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"\'data\' argument is of the wrong type\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    R_ProtectWithIndex(framenames, &mut vpi);
    if framenames != R_NilValue {
        if length(framenames) != 0 {
            hadFrameNames = TRUE
        }
        if length((*args).u.listsxp.carval) == 3 as libc::c_int {
            CheckRHS(
                (*(*(*args).u.listsxp.carval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            );
        }
    }
    /* Preserve term order? */
    keepOrder = asLogical((*a).u.listsxp.carval);
    if keepOrder == R_NaInt {
        keepOrder = 0 as libc::c_int
    }
    a = (*a).u.listsxp.cdrval;
    allowDot = asLogical((*a).u.listsxp.carval);
    if allowDot == R_NaInt {
        allowDot = 0 as libc::c_int
    }
    if specials == R_NilValue {
        a = allocList(8 as libc::c_int);
        SET_ATTRIB(ans, a);
    } else {
        a = allocList(9 as libc::c_int);
        SET_ATTRIB(ans, a);
    }
    /* Step 1: Determine the ``variables'' in the model */
    /* Here we create an expression of the form */
    /* list(...).  You can evaluate it to get */
    /* the model variables or use substitute and then */
    /* pull the result apart to get the variable names. */
    intercept = 1 as libc::c_int;
    parity = 1 as libc::c_int;
    response = 0 as libc::c_int;
    varlist = lcons(
        install(b"list\x00" as *const u8 as *const libc::c_char),
        R_NilValue,
    );
    protect(varlist);
    ExtractVars((*args).u.listsxp.carval, 1 as libc::c_int);
    unprotect(1 as libc::c_int);
    SETCAR(a, varlist);
    SET_TAG(
        a,
        install(b"variables\x00" as *const u8 as *const libc::c_char),
    );
    a = (*a).u.listsxp.cdrval;
    nvar = length(varlist) - 1 as libc::c_int;
    /* in allocating words need to allow for intercept term */
    nwords = (nvar as libc::c_ulong)
        .wrapping_div(
            (8 as libc::c_int as libc::c_ulong)
                .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong),
        )
        .wrapping_add(1 as libc::c_int as libc::c_ulong) as libc::c_int;
    //    printf("nvar = %d, nwords = %d\n", nvar, nwords);
    /* Step 2: Recode the model terms in binary form */
    /* and at the same time, expand the model formula. */
    /* FIXME: this includes specials in the model */
    /* There perhaps needs to be a an extra pass */
    /* through the model to delete any terms which */
    /* contain specials.  Actually, specials should */
    /* only enter additively so this should also be */
    /* checked and abort forced if not. */
    /* BDR 2002-01-29: S does include specials, so code may rely on this */
    /* FIXME: this is also the point where nesting */
    /* needs to be taken care of. */
    formula = EncodeVars((*args).u.listsxp.carval); /* need to recompute, in case
                                                    EncodeVars stretched it */
    protect(formula);
    nvar = length(varlist) - 1 as libc::c_int;
    /* Step 2a: Compute variable names */
    varnames = allocVector(16 as libc::c_int as SEXPTYPE, nvar as R_xlen_t);
    protect(varnames);
    let mut i: R_xlen_t = 0;
    v = (*varlist).u.listsxp.cdrval;
    i = 0 as libc::c_int as R_xlen_t;
    while v != R_NilValue {
        let fresh12 = i;
        i = i + 1;
        SET_STRING_ELT(
            varnames,
            fresh12,
            STRING_ELT(
                deparse1line((*v).u.listsxp.carval, FALSE),
                0 as libc::c_int as R_xlen_t,
            ),
        );
        v = (*v).u.listsxp.cdrval
    }
    /* Step 2b: Find and remove any offset(s) */
    /* first see if any of the variables are offsets */
    let mut k: R_xlen_t = 0 as libc::c_int as R_xlen_t; /* has there been a non-offset term? */
    let mut l: R_xlen_t = response as R_xlen_t;
    while l < nvar as libc::c_long {
        if strncmp(
            (STRING_ELT(varnames, l) as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize)
                as *mut libc::c_void as *const libc::c_char,
            b"offset(\x00" as *const u8 as *const libc::c_char,
            7 as libc::c_int as libc::c_ulong,
        ) == 0
        {
            k += 1
        }
        l += 1
    }
    if k > 0 as libc::c_int as libc::c_long {
        let mut foundOne: Rboolean = FALSE;
        /* allocate the "offsets" attribute */
        v = allocVector(13 as libc::c_int as SEXPTYPE, k);
        SETCAR(a, v);
        let mut l_0: libc::c_int = response;
        let mut k_0: libc::c_int = 0 as libc::c_int;
        while l_0 < nvar {
            if strncmp(
                (STRING_ELT(varnames, l_0 as R_xlen_t) as *mut SEXPREC_ALIGN)
                    .offset(1 as libc::c_int as isize) as *mut libc::c_void
                    as *const libc::c_char,
                b"offset(\x00" as *const u8 as *const libc::c_char,
                7 as libc::c_int as libc::c_ulong,
            ) == 0
            {
                let fresh13 = k_0;
                k_0 = k_0 + 1;
                *(DATAPTR(v) as *mut libc::c_int).offset(fresh13 as isize) = l_0 + 1 as libc::c_int
            }
            l_0 += 1
        }
        SET_TAG(
            a,
            install(b"offset\x00" as *const u8 as *const libc::c_char),
        );
        a = (*a).u.listsxp.cdrval;
        /* now remove offset terms from the formula */
        call = formula; /* call is to be the previous term once one is found */
        loop {
            let mut thisterm: SEXP = if foundOne as libc::c_uint != 0 {
                (*call).u.listsxp.cdrval
            } else {
                call
            };
            let mut have_offset: Rboolean = FALSE;
            if length(thisterm) == 0 as libc::c_int {
                break;
            }
            let mut i_0: libc::c_int = 1 as libc::c_int;
            while i_0 <= nvar {
                if GetBit((*thisterm).u.listsxp.carval, i_0) != 0
                    && strncmp(
                        (STRING_ELT(varnames, (i_0 - 1 as libc::c_int) as R_xlen_t)
                            as *mut SEXPREC_ALIGN)
                            .offset(1 as libc::c_int as isize)
                            as *mut libc::c_void as *const libc::c_char,
                        b"offset(\x00" as *const u8 as *const libc::c_char,
                        7 as libc::c_int as libc::c_ulong,
                    ) == 0
                {
                    have_offset = TRUE;
                    break;
                } else {
                    i_0 += 1
                }
            }
            if have_offset as u64 != 0 {
                if foundOne as u64 == 0 {
                    formula = (*formula).u.listsxp.cdrval;
                    call = formula
                } else {
                    SETCDR(call, (*thisterm).u.listsxp.cdrval);
                }
            } else {
                if foundOne as u64 != 0 {
                    call = (*call).u.listsxp.cdrval
                }
                foundOne = TRUE
            }
        }
    }
    nterm = length(formula);
    /* Step 3: Reorder the model terms by BitCount, otherwise
    preserving their order. */
    ord = allocVector(13 as libc::c_int as SEXPTYPE, nterm as R_xlen_t);
    protect(ord);
    let mut n: R_xlen_t = 0;
    let mut sCounts: SEXP = 0 as *mut SEXPREC;
    let mut counts: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut bitmax: libc::c_int = 0 as libc::c_int;
    let mut iord: *mut libc::c_int = DATAPTR(ord) as *mut libc::c_int;
    let mut m: libc::c_int = 0 as libc::c_int;
    pattern = allocVector(19 as libc::c_int as SEXPTYPE, nterm as R_xlen_t);
    protect(pattern);
    sCounts = allocVector(13 as libc::c_int as SEXPTYPE, nterm as R_xlen_t);
    protect(sCounts);
    counts = DATAPTR(sCounts) as *mut libc::c_int;
    call = formula;
    n = 0 as libc::c_int as R_xlen_t;
    while call != R_NilValue {
        SET_VECTOR_ELT(pattern, n, (*call).u.listsxp.carval);
        *counts.offset(n as isize) = BitCount((*call).u.listsxp.carval);
        call = (*call).u.listsxp.cdrval;
        n += 1
    }
    n = 0 as libc::c_int as R_xlen_t;
    while n < nterm as libc::c_long {
        if *counts.offset(n as isize) > bitmax {
            bitmax = *counts.offset(n as isize)
        }
        n += 1
    }
    if keepOrder != 0 {
        n = 0 as libc::c_int as R_xlen_t;
        while n < nterm as libc::c_long {
            *iord.offset(n as isize) = *counts.offset(n as isize);
            n += 1
        }
    } else {
        call = formula;
        m = 0 as libc::c_int;
        let mut i_1: libc::c_int = 0 as libc::c_int;
        while i_1 <= bitmax {
            /* can order 0 occur? */
            n = 0 as libc::c_int as R_xlen_t;
            while n < nterm as libc::c_long {
                if *counts.offset(n as isize) == i_1 {
                    SETCAR(call, *(DATAPTR(pattern) as *mut SEXP).offset(n as isize));
                    call = (*call).u.listsxp.cdrval;
                    let fresh14 = m;
                    m = m + 1;
                    *iord.offset(fresh14 as isize) = i_1
                }
                n += 1
            }
            i_1 += 1
        }
    }
    unprotect(2 as libc::c_int);
    /* Step 4: Compute the factor pattern for the model. */
    /* 0 - the variable does not appear in this term. */
    /* 1 - code the variable by contrasts in this term. */
    /* 2 - code the variable by indicators in this term. */
    if nterm > 0 as libc::c_int {
        pattern = allocMatrix(13 as libc::c_int as SEXPTYPE, nvar, nterm);
        SETCAR(a, pattern);
        SET_TAG(
            a,
            install(b"factors\x00" as *const u8 as *const libc::c_char),
        );
        a = (*a).u.listsxp.cdrval;
        let mut i_2: R_xlen_t = 0 as libc::c_int as R_xlen_t;
        while i_2 < nterm as R_xlen_t * nvar as libc::c_long {
            *(DATAPTR(pattern) as *mut libc::c_int).offset(i_2 as isize) = 0 as libc::c_int;
            i_2 += 1
        }
        term = AllocTerm();
        protect(term);
        let mut n_0: R_xlen_t = 0 as libc::c_int as R_xlen_t;
        call = formula;
        while call != R_NilValue {
            let mut i_3: libc::c_int = 1 as libc::c_int;
            while i_3 <= nvar {
                if GetBit((*call).u.listsxp.carval, i_3) != 0 {
                    *(DATAPTR(pattern) as *mut libc::c_int).offset(
                        ((i_3 - 1 as libc::c_int) as libc::c_long + n_0 * nvar as libc::c_long)
                            as isize,
                    ) = TermCode(formula, call, i_3, term)
                }
                i_3 += 1
            }
            n_0 += 1;
            call = (*call).u.listsxp.cdrval
        }
        unprotect(1 as libc::c_int);
    } else {
        pattern = allocVector(13 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t);
        SETCAR(a, pattern);
        SET_TAG(
            a,
            install(b"factors\x00" as *const u8 as *const libc::c_char),
        );
        a = (*a).u.listsxp.cdrval
    }
    /* Step 5: Compute term labels */
    termlabs = allocVector(16 as libc::c_int as SEXPTYPE, nterm as R_xlen_t);
    protect(termlabs);
    let mut n_1: R_xlen_t = 0 as libc::c_int as R_xlen_t;
    call = formula;
    while call != R_NilValue {
        let mut l_1: R_xlen_t = 0 as libc::c_int as R_xlen_t;
        let mut i_4: libc::c_int = 1 as libc::c_int;
        while i_4 <= nvar {
            if GetBit((*call).u.listsxp.carval, i_4) != 0 {
                if l_1 > 0 as libc::c_int as libc::c_long {
                    l_1 += 1 as libc::c_int as libc::c_long
                }
                l_1 += strlen(
                    (STRING_ELT(varnames, (i_4 - 1 as libc::c_int) as R_xlen_t)
                        as *mut SEXPREC_ALIGN)
                        .offset(1 as libc::c_int as isize) as *mut libc::c_void
                        as *const libc::c_char,
                ) as libc::c_int as libc::c_long
            }
            i_4 += 1
        }
        let vla = (l_1 + 1 as libc::c_int as libc::c_long) as usize;
        let mut cbuf: Vec<libc::c_char> = ::std::vec::from_elem(0, vla);
        *cbuf.as_mut_ptr().offset(0 as libc::c_int as isize) = '\u{0}' as i32 as libc::c_char;
        l_1 = 0 as libc::c_int as R_xlen_t;
        let mut i_5: libc::c_int = 1 as libc::c_int;
        while i_5 <= nvar {
            if GetBit((*call).u.listsxp.carval, i_5) != 0 {
                if l_1 > 0 as libc::c_int as libc::c_long {
                    strcat(
                        cbuf.as_mut_ptr(),
                        b":\x00" as *const u8 as *const libc::c_char,
                    );
                }
                strcat(
                    cbuf.as_mut_ptr(),
                    (STRING_ELT(varnames, (i_5 - 1 as libc::c_int) as R_xlen_t)
                        as *mut SEXPREC_ALIGN)
                        .offset(1 as libc::c_int as isize) as *mut libc::c_void
                        as *const libc::c_char,
                );
                l_1 += 1
            }
            i_5 += 1
        }
        SET_STRING_ELT(termlabs, n_1, mkChar(cbuf.as_mut_ptr()));
        n_1 += 1;
        call = (*call).u.listsxp.cdrval
    }
    v = allocVector(19 as libc::c_int as SEXPTYPE, 2 as libc::c_int as R_xlen_t);
    protect(v);
    SET_VECTOR_ELT(v, 0 as libc::c_int as R_xlen_t, varnames);
    SET_VECTOR_ELT(v, 1 as libc::c_int as R_xlen_t, termlabs);
    if nterm > 0 as libc::c_int {
        setAttrib(pattern, R_DimNamesSymbol, v);
    }
    SETCAR(a, termlabs);
    SET_TAG(
        a,
        install(b"term.labels\x00" as *const u8 as *const libc::c_char),
    );
    a = (*a).u.listsxp.cdrval;
    /* If there are specials stick them in here */
    if specials != R_NilValue {
        let mut j: R_xlen_t = 0; /* keep termlabs until here */
        let mut vmax: *const libc::c_void = vmaxget();
        let mut i_6: libc::c_int = length(specials);
        v = allocList(i_6);
        protect(v);
        j = 0 as libc::c_int as R_xlen_t;
        t = v;
        while j < i_6 as libc::c_long {
            let mut ss: *const libc::c_char = translateChar(STRING_ELT(specials, j));
            SET_TAG(t, install(ss));
            let mut n_2: R_xlen_t = strlen(ss) as libc::c_int as R_xlen_t;
            SETCAR(
                t,
                allocVector(13 as libc::c_int as SEXPTYPE, 0 as libc::c_int as R_xlen_t),
            );
            let mut k_1: R_xlen_t = 0 as libc::c_int as R_xlen_t;
            let mut l_2: R_xlen_t = 0 as libc::c_int as R_xlen_t;
            while l_2 < nvar as libc::c_long {
                if strncmp(
                    (STRING_ELT(varnames, l_2) as *mut SEXPREC_ALIGN)
                        .offset(1 as libc::c_int as isize) as *mut libc::c_void
                        as *const libc::c_char,
                    ss,
                    n_2 as libc::c_ulong,
                ) == 0
                {
                    if *((STRING_ELT(varnames, l_2) as *mut SEXPREC_ALIGN)
                        .offset(1 as libc::c_int as isize)
                        as *mut libc::c_void as *const libc::c_char)
                        .offset(n_2 as isize) as libc::c_int
                        == '(' as i32
                    {
                        k_1 += 1
                    }
                }
                l_2 += 1
            }
            if k_1 > 0 as libc::c_int as libc::c_long {
                SETCAR(t, allocVector(13 as libc::c_int as SEXPTYPE, k_1));
                k_1 = 0 as libc::c_int as R_xlen_t;
                let mut l_3: libc::c_int = 0 as libc::c_int;
                while l_3 < nvar {
                    if strncmp(
                        (STRING_ELT(varnames, l_3 as R_xlen_t) as *mut SEXPREC_ALIGN)
                            .offset(1 as libc::c_int as isize)
                            as *mut libc::c_void as *const libc::c_char,
                        ss,
                        n_2 as libc::c_ulong,
                    ) == 0
                    {
                        if *((STRING_ELT(varnames, l_3 as R_xlen_t) as *mut SEXPREC_ALIGN)
                            .offset(1 as libc::c_int as isize)
                            as *mut libc::c_void
                            as *const libc::c_char)
                            .offset(n_2 as isize) as libc::c_int
                            == '(' as i32
                        {
                            let fresh15 = k_1;
                            k_1 = k_1 + 1;
                            *(DATAPTR((*t).u.listsxp.carval) as *mut libc::c_int)
                                .offset(fresh15 as isize) = l_3 + 1 as libc::c_int
                        }
                    }
                    l_3 += 1
                }
            } else {
                SETCAR(t, R_NilValue);
            }
            j += 1;
            t = (*t).u.listsxp.cdrval
        }
        SETCAR(a, v);
        SET_TAG(
            a,
            install(b"specials\x00" as *const u8 as *const libc::c_char),
        );
        a = (*a).u.listsxp.cdrval;
        unprotect(1 as libc::c_int);
        vmaxset(vmax);
    }
    unprotect(2 as libc::c_int);
    /* Step 6: Fix up the formula by substituting for dot, which should be
    the framenames joined by + */
    if haveDot as u64 != 0 {
        if length(framenames) != 0 {
            let mut ind: PROTECT_INDEX = 0; /* truncate if necessary */
            rhs = installTrChar(STRING_ELT(framenames, 0 as libc::c_int as R_xlen_t));
            R_ProtectWithIndex(rhs, &mut ind);
            let mut i_7: R_xlen_t = 1 as libc::c_int as R_xlen_t;
            while i_7
                < LENGTH_EX(
                    framenames,
                    b"model.c\x00" as *const u8 as *const libc::c_char,
                    1970 as libc::c_int,
                ) as libc::c_long
            {
                rhs = lang3(plusSymbol, rhs, installTrChar(STRING_ELT(framenames, i_7)));
                R_Reprotect(rhs, ind);
                i_7 += 1
            }
            if !((*(*(*(*ans).u.listsxp.cdrval).u.listsxp.cdrval)
                .u
                .listsxp
                .carval)
                .sxpinfo
                .type_0() as libc::c_int
                == 0 as libc::c_int)
            {
                SETCADDR(
                    ans,
                    ExpandDots(
                        (*(*(*ans).u.listsxp.cdrval).u.listsxp.cdrval)
                            .u
                            .listsxp
                            .carval,
                        rhs,
                    ),
                );
            } else {
                SETCADR(
                    ans,
                    ExpandDots((*(*ans).u.listsxp.cdrval).u.listsxp.carval, rhs),
                );
            }
            unprotect(1 as libc::c_int);
        } else if allowDot == 0 && hadFrameNames as u64 == 0 {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"\'.\' in formula and no \'data\' argument\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
    }
    SETCAR(
        a,
        allocVector(13 as libc::c_int as SEXPTYPE, nterm as R_xlen_t),
    );
    let mut n_3: R_xlen_t = 0 as libc::c_int as R_xlen_t;
    let mut ia: *mut libc::c_int = DATAPTR((*a).u.listsxp.carval) as *mut libc::c_int;
    let mut iord_0: *mut libc::c_int = DATAPTR(ord) as *mut libc::c_int;
    call = formula;
    while call != R_NilValue {
        *ia.offset(n_3 as isize) = *iord_0.offset(n_3 as isize);
        call = (*call).u.listsxp.cdrval;
        n_3 += 1
    }
    SET_TAG(a, install(b"order\x00" as *const u8 as *const libc::c_char));
    a = (*a).u.listsxp.cdrval;
    SETCAR(
        a,
        ScalarInteger((intercept != 0 as libc::c_int) as libc::c_int),
    );
    SET_TAG(
        a,
        install(b"intercept\x00" as *const u8 as *const libc::c_char),
    );
    a = (*a).u.listsxp.cdrval;
    SETCAR(
        a,
        ScalarInteger((response != 0 as libc::c_int) as libc::c_int),
    );
    SET_TAG(
        a,
        install(b"response\x00" as *const u8 as *const libc::c_char),
    );
    a = (*a).u.listsxp.cdrval;
    SETCAR(
        a,
        mkString(b"terms\x00" as *const u8 as *const libc::c_char),
    );
    SET_TAG(a, R_ClassSymbol);
    (*ans).sxpinfo.set_obj(1 as libc::c_int as libc::c_uint);
    SETCDR(a, R_NilValue);
    unprotect(5 as libc::c_int);
    return ans;
}
