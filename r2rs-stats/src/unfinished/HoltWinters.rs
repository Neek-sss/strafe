use ::libc;
extern "C" {
    #[no_mangle]
    fn memcpy(_: *mut libc::c_void, _: *const libc::c_void, _: libc::c_ulong) -> *mut libc::c_void;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2001-2017 The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/*  R : A Computer Language for Statistical Data Analysis
 *
 *  Copyright (C) 2003-7  The R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/.
 */
/* Originally contributed by David Meyer */
// memcpy
#[no_mangle]
pub unsafe extern "C" fn HoltWinters(
    mut x: *mut libc::c_double,
    mut xl: *mut libc::c_int,
    mut alpha: *mut libc::c_double,
    mut beta: *mut libc::c_double,
    mut gamma: *mut libc::c_double,
    mut start_time: *mut libc::c_int,
    mut seasonal: *mut libc::c_int,
    mut period: *mut libc::c_int,
    mut dotrend: *mut libc::c_int,
    mut doseasonal: *mut libc::c_int,
    mut a: *mut libc::c_double,
    mut b: *mut libc::c_double,
    mut s: *mut libc::c_double,
    mut SSE: *mut libc::c_double,
    mut level: *mut libc::c_double,
    mut trend: *mut libc::c_double,
    mut season: *mut libc::c_double,
) {
    let mut res: libc::c_double = 0 as libc::c_int as libc::c_double;
    let mut xhat: libc::c_double = 0 as libc::c_int as libc::c_double;
    let mut stmp: libc::c_double = 0 as libc::c_int as libc::c_double;
    let mut i: libc::c_int = 0;
    let mut i0: libc::c_int = 0;
    let mut s0: libc::c_int = 0;
    /* copy start values to the beginning of the vectors */
    *level.offset(0 as libc::c_int as isize) = *a;
    if *dotrend == 1 as libc::c_int {
        *trend.offset(0 as libc::c_int as isize) = *b
    }
    if *doseasonal == 1 as libc::c_int {
        memcpy(
            season as *mut libc::c_void,
            s as *const libc::c_void,
            (*period as libc::c_ulong)
                .wrapping_mul(::std::mem::size_of::<libc::c_double>() as libc::c_ulong),
        );
    }
    i = *start_time - 1 as libc::c_int;
    while i < *xl {
        /* indices for period i */
        i0 = i - *start_time + 2 as libc::c_int;
        s0 = i0 + *period - 1 as libc::c_int;
        /* forecast *for* period i */
        xhat = *level.offset((i0 - 1 as libc::c_int) as isize)
            + (if *dotrend == 1 as libc::c_int {
                *trend.offset((i0 - 1 as libc::c_int) as isize)
            } else {
                0 as libc::c_int as libc::c_double
            });
        stmp = if *doseasonal == 1 as libc::c_int {
            *season.offset((s0 - *period) as isize)
        } else {
            (*seasonal != 1 as libc::c_int) as libc::c_int as libc::c_double
        };
        if *seasonal == 1 as libc::c_int {
            xhat += stmp
        } else {
            xhat *= stmp
        }
        /* Sum of Squared Errors */
        res = *x.offset(i as isize) - xhat;
        *SSE += res * res;
        /* estimate of level *in* period i */
        if *seasonal == 1 as libc::c_int {
            *level.offset(i0 as isize) = *alpha * (*x.offset(i as isize) - stmp)
                + (1 as libc::c_int as libc::c_double - *alpha)
                    * (*level.offset((i0 - 1 as libc::c_int) as isize)
                        + *trend.offset((i0 - 1 as libc::c_int) as isize))
        } else {
            *level.offset(i0 as isize) = *alpha * (*x.offset(i as isize) / stmp)
                + (1 as libc::c_int as libc::c_double - *alpha)
                    * (*level.offset((i0 - 1 as libc::c_int) as isize)
                        + *trend.offset((i0 - 1 as libc::c_int) as isize))
        }
        /* estimate of trend *in* period i */
        if *dotrend == 1 as libc::c_int {
            *trend.offset(i0 as isize) = *beta
                * (*level.offset(i0 as isize) - *level.offset((i0 - 1 as libc::c_int) as isize))
                + (1 as libc::c_int as libc::c_double - *beta)
                    * *trend.offset((i0 - 1 as libc::c_int) as isize)
        }
        /* estimate of seasonal component *in* period i */
        if *doseasonal == 1 as libc::c_int {
            if *seasonal == 1 as libc::c_int {
                *season.offset(s0 as isize) = *gamma
                    * (*x.offset(i as isize) - *level.offset(i0 as isize))
                    + (1 as libc::c_int as libc::c_double - *gamma) * stmp
            } else {
                *season.offset(s0 as isize) = *gamma
                    * (*x.offset(i as isize) / *level.offset(i0 as isize))
                    + (1 as libc::c_int as libc::c_double - *gamma) * stmp
            }
        }
        i += 1
    }
}
