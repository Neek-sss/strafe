use crate::sexprec::{SEXP, SEXPREC, SEXPTYPE};
use ::libc;
extern "C" {
    #[no_mangle]
    fn drn2g_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn drn2gb_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn drmnhb_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn drmngb_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn drmnfb_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn drmnh_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn drmng_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn drmnf_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    #[no_mangle]
    fn R_chk_free(_: *mut libc::c_void);
    #[no_mangle]
    fn R_chk_calloc(_: size_t, _: size_t) -> *mut libc::c_void;
    #[no_mangle]
    fn STRING_ELT(x: SEXP, i: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn lang2(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn lang1(_: SEXP) -> SEXP;
    #[no_mangle]
    fn isNewList(_: SEXP) -> Rboolean;
    #[no_mangle]
    fn isMatrix(_: SEXP) -> Rboolean;
    #[no_mangle]
    fn isFunction(_: SEXP) -> Rboolean;
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn memcpy(_: *mut libc::c_void, _: *const libc::c_void, _: libc::c_ulong) -> *mut libc::c_void;
    #[no_mangle]
    fn memmove(_: *mut libc::c_void, _: *const libc::c_void, _: libc::c_ulong)
        -> *mut libc::c_void;
    #[no_mangle]
    fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    /* IEEE NaN */
    #[no_mangle]
    static mut R_PosInf: libc::c_double;
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn Rprintf(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn R_CHAR(x: SEXP) -> *const libc::c_char;
    #[no_mangle]
    fn isNull(s: SEXP) -> Rboolean;
    #[no_mangle]
    fn isReal(s: SEXP) -> Rboolean;
    #[no_mangle]
    fn isEnvironment(s: SEXP) -> Rboolean;
    #[no_mangle]
    fn TYPEOF(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn LENGTH(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn LOGICAL(x: SEXP) -> *mut libc::c_int;
    #[no_mangle]
    fn INTEGER(x: SEXP) -> *mut libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn VECTOR_ELT(x: SEXP, i: R_xlen_t) -> SEXP;
    #[no_mangle]
    static mut R_GlobalEnv: SEXP;
    #[no_mangle]
    static mut R_BaseEnv: SEXP;
    #[no_mangle]
    static mut R_NilValue: SEXP;
    #[no_mangle]
    static mut R_DimSymbol: SEXP;
    #[no_mangle]
    static mut R_NamesSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn defineVar(_: SEXP, _: SEXP, _: SEXP);
    #[no_mangle]
    fn duplicate(_: SEXP) -> SEXP;
    #[no_mangle]
    fn eval(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn findVarInFrame(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn getAttrib(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn install(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn ddot_(
        n: *const libc::c_int,
        dx: *const libc::c_double,
        incx: *const libc::c_int,
        dy: *const libc::c_double,
        incy: *const libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn dnrm2_(
        n: *const libc::c_int,
        dx: *const libc::c_double,
        incx: *const libc::c_int,
    ) -> libc::c_double;
    #[no_mangle]
    fn dswap_(
        n: *const libc::c_int,
        dx: *mut libc::c_double,
        incx: *const libc::c_int,
        dy: *mut libc::c_double,
        incy: *const libc::c_int,
    );
    /* port sources */
    /* dv7dfl.... provides default values to v. */
    #[no_mangle]
    fn dv7dfl_(Alg: *const libc::c_int, Lv: *const libc::c_int, v: *mut libc::c_double);
}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
pub type R_xlen_t = ptrdiff_t;
/*, MAYBE */
/* C-language replacements for Fortran utilities in PORT sources */
/* dd7tpr... returns inner product of two vectors. */
#[no_mangle]
pub unsafe extern "C" fn dd7tpr_(
    mut p: *mut libc::c_int,
    mut x: *const libc::c_double,
    mut y: *const libc::c_double,
) -> libc::c_double {
    let mut ione: libc::c_int = 1 as libc::c_int;
    return ddot_(p, x, &mut ione, y, &mut ione);
}
/* ditsum... prints iteration summary, initial and final alf. */
#[no_mangle]
pub unsafe extern "C" fn ditsum_(
    mut _d: *const libc::c_double,
    mut _g: *const libc::c_double,
    mut iv: *mut libc::c_int,
    mut _liv: *const libc::c_int,
    mut _lv: *const libc::c_int,
    mut n: *const libc::c_int,
    mut v: *mut libc::c_double,
    mut x: *const libc::c_double,
) {
    let mut i: libc::c_int = 0; /* offsets for 1-based indices */
    let mut nn: libc::c_int = *n; /* no iteration output */
    let mut ivm: *mut libc::c_int = iv.offset(-(1 as libc::c_int as isize));
    let mut vm: *mut libc::c_double = v.offset(-(1 as libc::c_int as isize));
    if *ivm.offset(19 as libc::c_int as isize) == 0 {
        return;
    }
    if *ivm.offset(31 as libc::c_int as isize) % *ivm.offset(19 as libc::c_int as isize) == 0 {
        /* output every ivm[OUTLEV] iterations */
        Rprintf(
            b"%3d:%#14.8g:\x00" as *const u8 as *const libc::c_char,
            *ivm.offset(31 as libc::c_int as isize),
            *vm.offset(10 as libc::c_int as isize),
        );
        i = 0 as libc::c_int;
        while i < nn {
            Rprintf(
                b" %#8g\x00" as *const u8 as *const libc::c_char,
                *x.offset(i as isize),
            );
            i += 1
        }
        Rprintf(b"\n\x00" as *const u8 as *const libc::c_char);
    };
}
/* *
 * Supply default values for elements of the iv and v arrays
 *
 * @param alg algorithm specification (1 <= alg <= 2)  (was alg <= 4, but reduced to work around gcc bug; see PR#15914)
 * @param iv integer working vector
 * @param liv length of iv
 * @param lv length of v
 * @param v double precision working vector
 */
#[no_mangle]
pub unsafe extern "C" fn divset(
    mut alg: libc::c_int,
    mut iv: *mut libc::c_int,
    mut liv: libc::c_int,
    mut lv: libc::c_int,
    mut v: *mut libc::c_double,
) {
    /*  ***  ALG = 1 MEANS REGRESSION CONSTANTS. */
    /*  ***  ALG = 2 MEANS GENERAL UNCONSTRAINED OPTIMIZATION CONSTANTS. */
    /* Initialized data */
    // alg[orithm] :          1   2   3    4
    static mut miniv: [libc::c_int; 5] = [
        0 as libc::c_int,
        82 as libc::c_int,
        59 as libc::c_int,
        103 as libc::c_int,
        103 as libc::c_int,
    ];
    static mut minv: [libc::c_int; 5] = [
        0 as libc::c_int,
        98 as libc::c_int,
        71 as libc::c_int,
        101 as libc::c_int,
        85 as libc::c_int,
    ];
    let mut mv: libc::c_int = 0;
    let mut miv: libc::c_int = 0;
    let mut alg1: libc::c_int = 0;
    /* Parameter adjustments - code will use 1-based indices*/
    iv = iv.offset(-1);
    v = v.offset(-1);
    /* Function Body */
    if 21 as libc::c_int <= liv {
        *iv.offset(21 as libc::c_int as isize) = 0 as libc::c_int
    } /* suppress all Fortran output */
    if 51 as libc::c_int <= liv {
        *iv.offset(51 as libc::c_int as isize) = alg
    }
    if alg < 1 as libc::c_int || alg > 4 as libc::c_int {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"Rf_divset: alg = %d must be 1, 2, 3, or 4\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ),
            alg,
        );
    }
    miv = miniv[alg as usize];
    if liv < miv {
        *iv.offset(1 as libc::c_int as isize) = 15 as libc::c_int;
        return;
    }
    mv = minv[alg as usize];
    if lv < mv {
        *iv.offset(1 as libc::c_int as isize) = 16 as libc::c_int;
        return;
    }
    alg1 = (alg - 1 as libc::c_int) % 2 as libc::c_int + 1 as libc::c_int;
    dv7dfl_(
        &mut alg1,
        &mut lv,
        &mut *v.offset(1 as libc::c_int as isize),
    );
    //       ------
    *iv.offset(1 as libc::c_int as isize) = 12 as libc::c_int; /* default is no iteration output */
    if alg > 2 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"port algorithms 3 or higher are not supported\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        )); /* was 1 but we suppress Fortran output */
    } /* was 1 but we suppress Fortran output */
    *iv.offset(3 as libc::c_int as isize) = 0 as libc::c_int;
    *iv.offset(44 as libc::c_int as isize) = miv;
    *iv.offset(45 as libc::c_int as isize) = mv;
    *iv.offset(42 as libc::c_int as isize) = mv + 1 as libc::c_int;
    *iv.offset(17 as libc::c_int as isize) = 200 as libc::c_int;
    *iv.offset(18 as libc::c_int as isize) = 150 as libc::c_int;
    *iv.offset(19 as libc::c_int as isize) = 0 as libc::c_int;
    *iv.offset(20 as libc::c_int as isize) = 1 as libc::c_int;
    *iv.offset(58 as libc::c_int as isize) = miv + 1 as libc::c_int;
    *iv.offset(22 as libc::c_int as isize) = 0 as libc::c_int;
    *iv.offset(23 as libc::c_int as isize) = 0 as libc::c_int;
    *iv.offset(4 as libc::c_int as isize) = 0 as libc::c_int;
    *iv.offset(24 as libc::c_int as isize) = 1 as libc::c_int;
    if alg1 >= 2 as libc::c_int {
        /*  GENERAL OPTIMIZATION values: nlminb() */
        *iv.offset(16 as libc::c_int as isize) = 0 as libc::c_int;
        *iv.offset(25 as libc::c_int as isize) = 1 as libc::c_int;
        *iv.offset(52 as libc::c_int as isize) = 0 as libc::c_int;
        *iv.offset(53 as libc::c_int as isize) = 0 as libc::c_int;
        *iv.offset(50 as libc::c_int as isize) = 25 as libc::c_int;
        *iv.offset(49 as libc::c_int as isize) = if alg > 2 as libc::c_int {
            61 as libc::c_int
        } else {
            47 as libc::c_int
        };
        *v.offset(31 as libc::c_int as isize) = 0.0f64
    /* since R 2.12.0:  Skip |f(x)| test */
    } else {
        /* REGRESSION  values: nls() */
        *iv.offset(14 as libc::c_int as isize) = 3 as libc::c_int;
        *iv.offset(15 as libc::c_int as isize) = 1 as libc::c_int;
        *iv.offset(16 as libc::c_int as isize) = 1 as libc::c_int;
        *iv.offset(71 as libc::c_int as isize) = 0 as libc::c_int;
        *iv.offset(75 as libc::c_int as isize) = 0 as libc::c_int;
        *iv.offset(25 as libc::c_int as isize) = 0 as libc::c_int;
        *iv.offset(76 as libc::c_int as isize) = 0 as libc::c_int;
        *iv.offset(50 as libc::c_int as isize) = 32 as libc::c_int;
        *iv.offset(60 as libc::c_int as isize) = if alg > 2 as libc::c_int {
            61 as libc::c_int
        } else {
            58 as libc::c_int
        };
        *iv.offset(49 as libc::c_int as isize) =
            *iv.offset(60 as libc::c_int as isize) + 9 as libc::c_int;
        *iv.offset(80 as libc::c_int as isize) = 1 as libc::c_int;
        *iv.offset(57 as libc::c_int as isize) = 3 as libc::c_int;
        *iv.offset(78 as libc::c_int as isize) = 0 as libc::c_int
    };
}
/* divset.... supply default values for elements of the iv and v arrays */
#[no_mangle]
pub unsafe extern "C" fn divset_(
    mut Alg: *const libc::c_int,
    mut iv: *mut libc::c_int,
    mut Liv: *const libc::c_int,
    mut Lv: *const libc::c_int,
    mut v: *mut libc::c_double,
) {
    divset(*Alg, iv, *Liv, *Lv, v);
}
/* dn2cvp... prints covariance matrix. */
#[no_mangle]
pub unsafe extern "C" fn dn2cvp_(
    mut _iv: *const libc::c_int,
    mut _liv: *mut libc::c_int,
    mut _lv: *mut libc::c_int,
    mut _p: *mut libc::c_int,
    mut _v: *const libc::c_double,
) {
    /* Done elsewhere */
}
/* dn2rdp... prints regression diagnostics for mlpsl and nl2s1. */
#[no_mangle]
pub unsafe extern "C" fn dn2rdp_(
    mut _iv: *const libc::c_int,
    mut _liv: *mut libc::c_int,
    mut _lv: *mut libc::c_int,
    mut _n: *mut libc::c_int,
    mut _rd: *const libc::c_double,
    mut _v: *const libc::c_double,
) {
    /* Done elsewhere */
}
/* ds7cpr... prints linear parameters at solution. */
#[no_mangle]
pub unsafe extern "C" fn ds7cpr_(
    mut _c: *const libc::c_double,
    mut _iv: *const libc::c_int,
    mut _l: *mut libc::c_int,
    mut _liv: *mut libc::c_int,
) {
    /* Done elsewhere */
}
/* dv2axy... computes scalar times one vector plus another */
#[no_mangle]
pub unsafe extern "C" fn dv2axy_(
    mut n: *mut libc::c_int,
    mut w: *mut libc::c_double,
    mut a: *const libc::c_double,
    mut x: *const libc::c_double,
    mut y: *const libc::c_double,
) {
    let mut i: libc::c_int = 0;
    let mut nn: libc::c_int = *n;
    let mut aa: libc::c_double = *a;
    i = 0 as libc::c_int;
    while i < nn {
        *w.offset(i as isize) = aa * *x.offset(i as isize) + *y.offset(i as isize);
        i += 1
    }
}
/* dv2nrm... returns the 2-norm of a vector. */
#[no_mangle]
pub unsafe extern "C" fn dv2nrm_(
    mut n: *mut libc::c_int,
    mut x: *const libc::c_double,
) -> libc::c_double {
    let mut ione: libc::c_int = 1 as libc::c_int;
    return dnrm2_(n, x, &mut ione);
}
/* dv7cpy.... copy src to dest */
#[no_mangle]
pub unsafe extern "C" fn dv7cpy_(
    mut n: *mut libc::c_int,
    mut dest: *mut libc::c_double,
    mut src: *const libc::c_double,
) {
    /* Was memcpy, but overlaps seen */
    memmove(
        dest as *mut libc::c_void,
        src as *const libc::c_void,
        (*n as libc::c_ulong)
            .wrapping_mul(::std::mem::size_of::<libc::c_double>() as libc::c_ulong),
    );
}
/* dv7ipr... applies forward permutation to vector.  */
#[no_mangle]
pub unsafe extern "C" fn dv7ipr_(
    mut n: *mut libc::c_int,
    mut ip: *const libc::c_int,
    mut x: *mut libc::c_double,
) {
    /* permute x so that x[i] := x[ip[i]]. */
    let mut i: libc::c_int = 0; /* ip contains 1-based indices */
    let mut nn: libc::c_int = *n;
    let mut xcp: *mut libc::c_double = R_chk_calloc(
        nn as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    i = 0 as libc::c_int;
    while i < nn {
        *xcp.offset(i as isize) = *x.offset((*ip.offset(i as isize) - 1 as libc::c_int) as isize);
        i += 1
    }
    memcpy(
        x as *mut libc::c_void,
        xcp as *const libc::c_void,
        (nn as size_t).wrapping_mul(::std::mem::size_of::<libc::c_double>() as libc::c_ulong),
    );
    R_chk_free(xcp as *mut libc::c_void);
    xcp = 0 as *mut libc::c_double;
}
/* dv7prm... applies reverse permutation to vector.  */
#[no_mangle]
pub unsafe extern "C" fn dv7prm_(
    mut n: *mut libc::c_int,
    mut ip: *const libc::c_int,
    mut x: *mut libc::c_double,
) {
    /* permute x so that x[ip[i]] := x[i]. */
    let mut i: libc::c_int = 0; /* ip contains 1-based indices */
    let mut nn: libc::c_int = *n;
    let mut xcp: *mut libc::c_double = R_chk_calloc(
        nn as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    i = 0 as libc::c_int;
    while i < nn {
        *xcp.offset((*ip.offset(i as isize) - 1 as libc::c_int) as isize) = *x.offset(i as isize);
        i += 1
    }
    memcpy(
        x as *mut libc::c_void,
        xcp as *const libc::c_void,
        (nn as size_t).wrapping_mul(::std::mem::size_of::<libc::c_double>() as libc::c_ulong),
    );
    R_chk_free(xcp as *mut libc::c_void);
    xcp = 0 as *mut libc::c_double;
}
/* dv7scl... scale src by *scal to dest */
#[no_mangle]
pub unsafe extern "C" fn dv7scl_(
    mut n: *mut libc::c_int,
    mut dest: *mut libc::c_double,
    mut scal: *const libc::c_double,
    mut src: *const libc::c_double,
) {
    let mut nn: libc::c_int = *n;
    let mut sc: libc::c_double = *scal;
    loop {
        let fresh0 = nn;
        nn = nn - 1;
        if !(fresh0 > 0 as libc::c_int) {
            break;
        }
        let fresh1 = src;
        src = src.offset(1);
        let fresh2 = dest;
        dest = dest.offset(1);
        *fresh2 = sc * *fresh1
    }
}
/* dv7scp... set values of an array to a constant */
#[no_mangle]
pub unsafe extern "C" fn dv7scp_(
    mut n: *mut libc::c_int,
    mut dest: *mut libc::c_double,
    mut c: *mut libc::c_double,
) {
    let mut nn: libc::c_int = *n;
    let mut cc: libc::c_double = *c;
    loop {
        let fresh3 = nn;
        nn = nn - 1;
        if !(fresh3 > 0 as libc::c_int) {
            break;
        }
        let fresh4 = dest;
        dest = dest.offset(1);
        *fresh4 = cc
    }
}
/* dv7swp... interchange n-vectors x and y. */
#[no_mangle]
pub unsafe extern "C" fn dv7swp_(
    mut n: *mut libc::c_int,
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
) {
    let mut ione: libc::c_int = 1 as libc::c_int;
    dswap_(n, x, &mut ione, y, &mut ione);
}
/* i7copy... copies one integer vector to another. */
#[no_mangle]
pub unsafe extern "C" fn i7copy_(
    mut n: *mut libc::c_int,
    mut dest: *mut libc::c_int,
    mut src: *const libc::c_int,
) {
    let mut nn: libc::c_int = *n;
    loop {
        let fresh5 = nn;
        nn = nn - 1;
        if !(fresh5 > 0 as libc::c_int) {
            break;
        }
        let fresh6 = src;
        src = src.offset(1);
        let fresh7 = dest;
        dest = dest.offset(1);
        *fresh7 = *fresh6
    }
}
/* i7pnvr... inverts permutation array. (Indices in array are 1-based) */
#[no_mangle]
pub unsafe extern "C" fn i7pnvr_(
    mut n: *mut libc::c_int,
    mut x: *mut libc::c_int,
    mut y: *const libc::c_int,
) {
    let mut i: libc::c_int = 0;
    let mut nn: libc::c_int = *n;
    i = 0 as libc::c_int;
    while i < nn {
        *x.offset((*y.offset(i as isize) - 1 as libc::c_int) as isize) = i + 1 as libc::c_int;
        i += 1
    }
}
/* stopx.... returns .true. if the break key has been pressed. */
#[no_mangle]
pub unsafe extern "C" fn stopx_() -> libc::c_int {
    return 0 as libc::c_int;
    /* interrupts are caught elsewhere */
}
unsafe extern "C" fn check_gv(
    mut gr: SEXP,
    mut hs: SEXP,
    mut rho: SEXP,
    mut n: libc::c_int,
    mut gv: *mut libc::c_double,
    mut hv: *mut libc::c_double,
) -> *mut libc::c_double {
    let mut gval: SEXP = protect(coerceVector(
        protect(eval(gr, rho)),
        14 as libc::c_int as SEXPTYPE,
    ));
    if LENGTH(gval) != n {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"gradient function must return a numeric vector of length %d\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ),
            n,
        );
    }
    memcpy(
        gv as *mut libc::c_void,
        REAL(gval) as *const libc::c_void,
        (n as size_t).wrapping_mul(::std::mem::size_of::<libc::c_double>() as libc::c_ulong),
    );
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < n {
        if (*gv.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
            error(b"NA/NaN gradient evaluation\x00" as *const u8 as *const libc::c_char);
        }
        i += 1
    }
    if !hv.is_null() {
        let mut hval: SEXP = protect(eval(hs, rho));
        let mut dim: SEXP = getAttrib(hval, R_DimSymbol);
        let mut i_0: libc::c_int = 0;
        let mut j: libc::c_int = 0;
        let mut pos: libc::c_int = 0;
        let mut rhval: *mut libc::c_double = REAL(hval);
        if isReal(hval) as u64 == 0
            || LENGTH(dim) != 2 as libc::c_int
            || *INTEGER(dim).offset(0 as libc::c_int as isize) != n
            || *INTEGER(dim).offset(1 as libc::c_int as isize) != n
        {
            error(
                dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"Hessian function must return a square numeric matrix of order %d\x00"
                        as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ),
                n,
            );
        }
        i_0 = 0 as libc::c_int;
        pos = 0 as libc::c_int;
        while i_0 < n {
            /* copy lower triangle row-wise */
            j = 0 as libc::c_int;
            while j <= i_0 {
                *hv.offset(pos as isize) = *rhval.offset((i_0 + j * n) as isize);
                if (*hv.offset(pos as isize)).is_nan() as i32 != 0 as libc::c_int {
                    error(b"NA/NaN Hessian evaluation\x00" as *const u8 as *const libc::c_char);
                }
                pos += 1;
                j += 1
            }
            i_0 += 1
        }
        unprotect(1 as libc::c_int);
    }
    unprotect(2 as libc::c_int);
    return gv;
}
#[no_mangle]
pub unsafe extern "C" fn nlminb_iterate(
    mut b: *mut libc::c_double,
    mut d: *mut libc::c_double,
    mut fx: libc::c_double,
    mut g: *mut libc::c_double,
    mut h: *mut libc::c_double,
    mut iv: *mut libc::c_int,
    mut liv: libc::c_int,
    mut lv: libc::c_int,
    mut n: libc::c_int,
    mut v: *mut libc::c_double,
    mut x: *mut libc::c_double,
) {
    let mut lh: libc::c_int = n * (n + 1 as libc::c_int) / 2 as libc::c_int;
    if !b.is_null() {
        if !g.is_null() {
            if !h.is_null() {
                drmnhb_(
                    b, d, &mut fx, g, h, iv, &mut lh, &mut liv, &mut lv, &mut n, v, x,
                );
            } else {
                drmngb_(b, d, &mut fx, g, iv, &mut liv, &mut lv, &mut n, v, x);
            }
        } else {
            drmnfb_(b, d, &mut fx, iv, &mut liv, &mut lv, &mut n, v, x);
        }
    } else if !g.is_null() {
        if !h.is_null() {
            drmnh_(
                d, &mut fx, g, h, iv, &mut lh, &mut liv, &mut lv, &mut n, v, x,
            );
        } else {
            drmng_(d, &mut fx, g, iv, &mut liv, &mut lv, &mut n, v, x);
        }
    } else {
        drmnf_(d, &mut fx, iv, &mut liv, &mut lv, &mut n, v, x);
    };
}
#[no_mangle]
pub unsafe extern "C" fn port_ivset(mut kind: SEXP, mut iv: SEXP, mut v: SEXP) -> SEXP {
    divset(asInteger(kind), INTEGER(iv), LENGTH(iv), LENGTH(v), REAL(v));
    return R_NilValue;
}
#[no_mangle]
pub unsafe extern "C" fn port_nlminb(
    mut fn_0: SEXP,
    mut gr: SEXP,
    mut hs: SEXP,
    mut rho: SEXP,
    mut lowerb: SEXP,
    mut upperb: SEXP,
    mut d: SEXP,
    mut iv: SEXP,
    mut v: SEXP,
) -> SEXP {
    let mut i: libc::c_int = 0;
    let mut n: libc::c_int = LENGTH(d);
    let mut xpt: SEXP = 0 as *mut SEXPREC;
    let mut dot_par_symbol: SEXP = install(b".par\x00" as *const u8 as *const libc::c_char);
    let mut b: *mut libc::c_double = 0 as *mut libc::c_void as *mut libc::c_double;
    let mut g: *mut libc::c_double = 0 as *mut libc::c_void as *mut libc::c_double;
    let mut h: *mut libc::c_double = 0 as *mut libc::c_void as *mut libc::c_double;
    let mut fx: libc::c_double = R_PosInf;
    if isNull(rho) as u64 != 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"use of NULL environment is defunct\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    } else {
        if isEnvironment(rho) as u64 == 0 {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"\'rho\' must be an environment\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
    }
    if isReal(d) as u64 == 0 || n < 1 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"\'d\' must be a nonempty numeric vector\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if hs != R_NilValue && gr == R_NilValue {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"When Hessian defined must also have gradient defined\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    xpt = findVarInFrame(rho, dot_par_symbol);
    if R_NilValue == xpt || isReal(xpt) as u64 == 0 || LENGTH(xpt) != n {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"environment \'rho\' must contain a numeric vector \'.par\' of length %d\x00"
                    as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            n,
        );
    }
    /* We are going to alter .par, so must duplicate it */
    defineVar(dot_par_symbol, duplicate(xpt), rho);
    xpt = findVarInFrame(rho, dot_par_symbol);
    protect(xpt);
    if LENGTH(lowerb) == n && LENGTH(upperb) == n {
        if isReal(lowerb) as libc::c_uint != 0 && isReal(upperb) as libc::c_uint != 0 {
            let mut rl: *mut libc::c_double = REAL(lowerb);
            let mut ru: *mut libc::c_double = REAL(upperb);
            b = R_alloc(
                (2 as libc::c_int * n) as size_t,
                ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
            ) as *mut libc::c_double;
            i = 0 as libc::c_int;
            while i < n {
                *b.offset((2 as libc::c_int * i) as isize) = *rl.offset(i as isize);
                *b.offset((2 as libc::c_int * i + 1 as libc::c_int) as isize) =
                    *ru.offset(i as isize);
                i += 1
            }
        } else {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"\'lower\' and \'upper\' must be numeric vectors\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
    }
    if gr != R_NilValue {
        g = R_alloc(
            n as size_t,
            ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_double;
        if hs != R_NilValue {
            h = R_alloc(
                (n * (n + 1 as libc::c_int) / 2 as libc::c_int) as size_t,
                ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
            ) as *mut libc::c_double
        }
    }
    loop {
        nlminb_iterate(
            b,
            REAL(d),
            fx,
            g,
            h,
            INTEGER(iv),
            LENGTH(iv),
            LENGTH(v),
            n,
            REAL(v),
            REAL(xpt),
        );
        if *INTEGER(iv).offset(0 as libc::c_int as isize) == 2 as libc::c_int && !g.is_null() {
            check_gv(gr, hs, rho, n, g, h);
        } else {
            fx = asReal(eval(fn_0, rho));
            if fx.is_nan() as i32 != 0 as libc::c_int {
                warning(b"NA/NaN function evaluation\x00" as *const u8 as *const libc::c_char);
                fx = R_PosInf
            }
        }
        /* duplicate .par value again in case a callback has stored
        value (package varComp does this) */
        defineVar(dot_par_symbol, duplicate(xpt), rho); /* xpt */
        xpt = findVarInFrame(rho, dot_par_symbol);
        unprotect(1 as libc::c_int);
        protect(xpt);
        if !(*INTEGER(iv).offset(0 as libc::c_int as isize) < 3 as libc::c_int) {
            break;
        }
    }
    unprotect(1 as libc::c_int);
    return R_NilValue;
}
#[no_mangle]
pub unsafe extern "C" fn nlsb_iterate(
    mut b: *mut libc::c_double,
    mut d: *mut libc::c_double,
    mut dr: *mut libc::c_double,
    mut iv: *mut libc::c_int,
    mut liv: libc::c_int,
    mut lv: libc::c_int,
    mut n: libc::c_int,
    mut nd: libc::c_int,
    mut p: libc::c_int,
    mut r: *mut libc::c_double,
    mut rd: *mut libc::c_double,
    mut v: *mut libc::c_double,
    mut x: *mut libc::c_double,
) {
    let mut ione: libc::c_int = 1 as libc::c_int;
    if !b.is_null() {
        drn2gb_(
            b, d, dr, iv, &mut liv, &mut lv, &mut n, &mut nd, &mut ione, &mut nd, &mut p, r, rd, v,
            x,
        );
    } else {
        drn2g_(
            d, dr, iv, &mut liv, &mut lv, &mut n, &mut nd, &mut ione, &mut nd, &mut p, r, rd, v, x,
        );
    };
}
/* *
 * Return the element of a given name from a named list
 *
 * @param list
 * @param nm name of desired element
 *
 * @return element of list with name nm
 */
#[inline]
unsafe extern "C" fn getElement(mut list: SEXP, mut nm: *mut libc::c_char) -> SEXP {
    let mut i: libc::c_int = 0;
    let mut names: SEXP = getAttrib(list, R_NamesSymbol);
    if isNewList(list) as u64 == 0 || LENGTH(names) != LENGTH(list) {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"\'getElement\' applies only to named lists\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    i = 0 as libc::c_int;
    while i < LENGTH(list) {
        if strcmp(R_CHAR(STRING_ELT(names, i as R_xlen_t)), nm) == 0 {
            /* ASCII only */
            return VECTOR_ELT(list, i as R_xlen_t);
        }
        i += 1
    }
    return R_NilValue;
}
/* *
 * Return the element of a given name from a named list after ensuring
 * that it is a function
 *
 * @param list
 * @param enm name of desired element
 * @param lnm string version of the name of the list
 *
 * @return a SEXP that points to a function
 */
#[inline]
unsafe extern "C" fn getFunc(
    mut list: SEXP,
    mut enm: *mut libc::c_char,
    mut lnm: *mut libc::c_char,
) -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    ans = getElement(list, enm);
    if isFunction(ans) as u64 == 0 {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"%s$%s() not found\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            lnm,
            enm,
        );
    }
    return ans;
}
unsafe extern "C" fn neggrad(mut gf: SEXP, mut rho: SEXP, mut gg: SEXP) {
    let mut val: SEXP = protect(eval(gf, rho));
    let mut dims: *mut libc::c_int = INTEGER(getAttrib(val, R_DimSymbol));
    let mut gdims: *mut libc::c_int = INTEGER(getAttrib(gg, R_DimSymbol));
    let mut i: libc::c_int = 0;
    let mut ntot: libc::c_int =
        *gdims.offset(0 as libc::c_int as isize) * *gdims.offset(1 as libc::c_int as isize);
    if TYPEOF(val) != TYPEOF(gg)
        || isMatrix(val) as u64 == 0
        || *dims.offset(0 as libc::c_int as isize) != *gdims.offset(0 as libc::c_int as isize)
        || *dims.offset(1 as libc::c_int as isize) != *gdims.offset(1 as libc::c_int as isize)
    {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"\'gradient\' must be a numeric matrix of dimension (%d,%d)\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ),
            *gdims.offset(0 as libc::c_int as isize),
            *gdims.offset(1 as libc::c_int as isize),
        );
    }
    i = 0 as libc::c_int;
    while i < ntot {
        *REAL(gg).offset(i as isize) = -*REAL(val).offset(i as isize);
        i += 1
    }
    unprotect(1 as libc::c_int);
}
/* *
 * Evaluate an expression in an environment, check that the length and
 * mode are as expected and store the result.
 *
 * @param fcn expression to evaluate
 * @param rho environment in which to evaluate it
 * @param vv position to store the result
 *
 * @return vv with new contents
 */
unsafe extern "C" fn eval_check_store(mut fcn: SEXP, mut rho: SEXP, mut vv: SEXP) -> SEXP {
    let mut v: SEXP = protect(eval(fcn, rho));
    if TYPEOF(v) != TYPEOF(vv) || LENGTH(v) != LENGTH(vv) {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"fcn produced mode %d, length %d - wanted mode %d, length %d\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ),
            TYPEOF(v),
            LENGTH(v),
            TYPEOF(vv),
            LENGTH(vv),
        );
    }
    match TYPEOF(v) {
        10 => {
            memcpy(
                LOGICAL(vv) as *mut libc::c_void,
                LOGICAL(v) as *const libc::c_void,
                (LENGTH(vv) as size_t)
                    .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong),
            );
        }
        13 => {
            memcpy(
                INTEGER(vv) as *mut libc::c_void,
                INTEGER(v) as *const libc::c_void,
                (LENGTH(vv) as size_t)
                    .wrapping_mul(::std::mem::size_of::<libc::c_int>() as libc::c_ulong),
            );
        }
        14 => {
            memcpy(
                REAL(vv) as *mut libc::c_void,
                REAL(v) as *const libc::c_void,
                (LENGTH(vv) as size_t)
                    .wrapping_mul(::std::mem::size_of::<libc::c_double>() as libc::c_ulong),
            );
        }
        _ => {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"invalid type for eval_check_store\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
    }
    unprotect(1 as libc::c_int);
    return vv;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2005-2016   The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Header file for the C utilities to accompany the Fortran
 * optimization routines for the port library.
 *
 * Copyright (C) 2005-5  the R Core Team
 * Licensed under the GNU General Public License, version 2 or later.
 */
// for memmove, memcpy, strcmp
/* PORT interface functions - reverse communication */
/* DRMNF(D, FX, IV, LIV, LV, N, V, X) */
/* DRMNG(D, FX, G, IV, LIV, LV, N, V, X) */
/* DRMNH(D, FX, G, H, IV, LH, LIV, LV, N, V, X) */
/* DRMNFB(B, D, FX, IV, LIV, LV, N, V, X) */
/* DRMNGB(B, D, FX, G, IV, LIV, LV, N, V, X) */
/* DRMNH(B, D, FX, G, H, IV, LH, LIV, LV, N, V, X) */
/* DRN2GB(B, D, DR, IV, LIV, LV, N, ND, N1, N2, P, R, RD, V, X) */
/* DRN2G(D, DR, IV, LIV, LV, N, ND, N1, N2, P, R, RD, V, X) */
/* DRNSGB(A, ALF, B, C, DA, IN, IV, L, L1, LA, LIV, LV, N, NDA, P, V, Y) */
/* DRNSG(A, ALF, C, DA, IN, IV, L, L1, LA, LIV, LV, N, NDA, P, V, Y) */
#[no_mangle]
pub unsafe extern "C" fn port_nlsb(
    mut m: SEXP,
    mut d: SEXP,
    mut gg: SEXP,
    mut iv: SEXP,
    mut v: SEXP,
    mut lowerb: SEXP,
    mut upperb: SEXP,
) -> SEXP {
    let mut dims: *mut libc::c_int = INTEGER(getAttrib(gg, R_DimSymbol));
    let mut i: libc::c_int = 0;
    let mut n: libc::c_int = LENGTH(d);
    let mut p: libc::c_int = LENGTH(d);
    let mut nd: libc::c_int = *dims.offset(0 as libc::c_int as isize);
    let mut getPars: SEXP = 0 as *mut SEXPREC;
    let mut setPars: SEXP = 0 as *mut SEXPREC;
    let mut resid: SEXP = 0 as *mut SEXPREC;
    let mut gradient: SEXP = 0 as *mut SEXPREC;
    let mut rr: SEXP = protect(allocVector(14 as libc::c_int as SEXPTYPE, nd as R_xlen_t));
    let mut x: SEXP = protect(allocVector(14 as libc::c_int as SEXPTYPE, n as R_xlen_t));
    // This used to use Calloc, but that will leak if
    // there is a premature return (and did in package drfit)
    let mut b: *mut libc::c_double = 0 as *mut libc::c_void as *mut libc::c_double;
    let mut rd: *mut libc::c_double = R_alloc(
        nd as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    if isReal(d) as u64 == 0 || n < 1 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"\'d\' must be a nonempty numeric vector\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if isNewList(m) as u64 == 0 {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"m must be a list\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    /* Initialize parameter vector */
    getPars = protect(lang1(getFunc(
        m,
        b"getPars\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
        b"m\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
    )));
    eval_check_store(getPars, R_GlobalEnv, x);
    /* Create the setPars call */
    setPars = protect(lang2(
        getFunc(
            m,
            b"setPars\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
            b"m\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
        ),
        x,
    ));
    /* Evaluate residual and gradient */
    resid = protect(lang1(getFunc(
        m,
        b"resid\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
        b"m\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
    )));
    eval_check_store(resid, R_GlobalEnv, rr);
    gradient = protect(lang1(getFunc(
        m,
        b"gradient\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
        b"m\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
    )));
    neggrad(gradient, R_GlobalEnv, gg);
    if LENGTH(lowerb) == n && LENGTH(upperb) == n {
        if isReal(lowerb) as libc::c_uint != 0 && isReal(upperb) as libc::c_uint != 0 {
            let mut rl: *mut libc::c_double = REAL(lowerb);
            let mut ru: *mut libc::c_double = REAL(upperb);
            b = R_alloc(
                (2 as libc::c_int * n) as size_t,
                ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
            ) as *mut libc::c_double;
            i = 0 as libc::c_int;
            while i < n {
                *b.offset((2 as libc::c_int * i) as isize) = *rl.offset(i as isize);
                *b.offset((2 as libc::c_int * i + 1 as libc::c_int) as isize) =
                    *ru.offset(i as isize);
                i += 1
            }
        } else {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"\'lowerb\' and \'upperb\' must be numeric vectors\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
    }
    loop {
        nlsb_iterate(
            b,
            REAL(d),
            REAL(gg),
            INTEGER(iv),
            LENGTH(iv),
            LENGTH(v),
            n,
            nd,
            p,
            REAL(rr),
            rd,
            REAL(v),
            REAL(x),
        );
        match *INTEGER(iv).offset(0 as libc::c_int as isize) {
            -3 => {
                eval(setPars, R_GlobalEnv);
                eval_check_store(resid, R_GlobalEnv, rr);
                neggrad(gradient, R_GlobalEnv, gg);
            }
            -2 => {
                eval_check_store(resid, R_GlobalEnv, rr);
                neggrad(gradient, R_GlobalEnv, gg);
            }
            -1 => {
                eval(setPars, R_GlobalEnv);
                eval_check_store(resid, R_GlobalEnv, rr);
                neggrad(gradient, R_GlobalEnv, gg);
            }
            0 => {
                Rprintf(
                    b"nlsb_iterate returned %d\x00" as *const u8 as *const libc::c_char,
                    *INTEGER(iv).offset(0 as libc::c_int as isize),
                );
            }
            1 => {
                eval(setPars, R_GlobalEnv);
                eval_check_store(resid, R_GlobalEnv, rr);
            }
            2 => {
                eval(setPars, R_GlobalEnv);
                neggrad(gradient, R_GlobalEnv, gg);
            }
            _ => {}
        }
        if !(*INTEGER(iv).offset(0 as libc::c_int as isize) < 3 as libc::c_int) {
            break;
        }
    }
    unprotect(6 as libc::c_int);
    return R_NilValue;
}
