use ::c2rust_bitfields;
pub type ptrdiff_t = libc::c_long;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
pub type SEXPTYPE = libc::c_uint;
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SEXPREC {
    pub sxpinfo: sxpinfo_struct,
    pub attrib: *mut SEXPREC,
    pub gengc_next_node: *mut SEXPREC,
    pub gengc_prev_node: *mut SEXPREC,
    pub u: C2RustUnnamed,
}
#[derive(Copy, Clone)]
#[repr(C)]
pub union C2RustUnnamed {
    pub primsxp: primsxp_struct,
    pub symsxp: symsxp_struct,
    pub listsxp: listsxp_struct,
    pub envsxp: envsxp_struct,
    pub closxp: closxp_struct,
    pub promsxp: promsxp_struct,
}
#[derive(Copy, Clone)]
#[repr(C)]
pub struct promsxp_struct {
    pub value: *mut SEXPREC,
    pub expr: *mut SEXPREC,
    pub env: *mut SEXPREC,
}
#[derive(Copy, Clone)]
#[repr(C)]
pub struct closxp_struct {
    pub formals: *mut SEXPREC,
    pub body: *mut SEXPREC,
    pub env: *mut SEXPREC,
}
#[derive(Copy, Clone)]
#[repr(C)]
pub struct envsxp_struct {
    pub frame: *mut SEXPREC,
    pub enclos: *mut SEXPREC,
    pub hashtab: *mut SEXPREC,
}
#[derive(Copy, Clone)]
#[repr(C)]
pub struct listsxp_struct {
    pub carval: *mut SEXPREC,
    pub cdrval: *mut SEXPREC,
    pub tagval: *mut SEXPREC,
}
#[derive(Copy, Clone)]
#[repr(C)]
pub struct symsxp_struct {
    pub pname: *mut SEXPREC,
    pub value: *mut SEXPREC,
    pub internal: *mut SEXPREC,
}
#[derive(Copy, Clone)]
#[repr(C)]
pub struct primsxp_struct {
    pub offset: libc::c_int,
}
/* Define SWITH_TO_REFCNT to use reference counting instead of the
'NAMED' mechanism. */
//#define SWITCH_TO_REFCNT
// ======================= USE_RINTERNALS section
/* This is intended for use only within R itself.
 * It defines internal structures that are otherwise only accessible
 * via SEXP, and macros to replace many (but not all) of accessor functions
 * (which are always defined).
 */
/* Flags */
#[derive(Copy, Clone, BitfieldStruct)]
#[repr(C)]
pub struct sxpinfo_struct {
    #[bitfield(name = "type_0", ty = "SEXPTYPE", bits = "0..=4")]
    #[bitfield(name = "scalar", ty = "libc::c_uint", bits = "5..=5")]
    #[bitfield(name = "obj", ty = "libc::c_uint", bits = "6..=6")]
    #[bitfield(name = "alt", ty = "libc::c_uint", bits = "7..=7")]
    #[bitfield(name = "gp", ty = "libc::c_uint", bits = "8..=23")]
    #[bitfield(name = "mark", ty = "libc::c_uint", bits = "24..=24")]
    #[bitfield(name = "debug", ty = "libc::c_uint", bits = "25..=25")]
    #[bitfield(name = "trace", ty = "libc::c_uint", bits = "26..=26")]
    #[bitfield(name = "spare", ty = "libc::c_uint", bits = "27..=27")]
    #[bitfield(name = "gcgen", ty = "libc::c_uint", bits = "28..=28")]
    #[bitfield(name = "gccls", ty = "libc::c_uint", bits = "29..=31")]
    #[bitfield(name = "named", ty = "libc::c_uint", bits = "32..=47")]
    #[bitfield(name = "extra", ty = "libc::c_uint", bits = "48..=63")]
    pub type_0_scalar_obj_alt_gp_mark_debug_trace_spare_gcgen_gccls_named_extra: [u8; 8],
}
/* raw bytes */
/* S4, non-vector */
/* used for detecting PROTECT issues in memory.c */
/* fresh node created in new page */
/* node released by GC */
/* Closure or Builtin or Special */
/* NOT YET */
/* These are also used with the write barrier on, in attrib.c and util.c */
pub type SEXP = *mut SEXPREC;
/*      Tot: 64 */
#[derive(Copy, Clone)]
#[repr(C)]
pub struct vecsxp_struct {
    pub length: R_xlen_t,
    pub truelength: R_xlen_t,
}
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VECTOR_SEXPREC {
    pub sxpinfo: sxpinfo_struct,
    pub attrib: *mut SEXPREC,
    pub gengc_next_node: *mut SEXPREC,
    pub gengc_prev_node: *mut SEXPREC,
    pub vecsxp: vecsxp_struct,
}
/* The generational collector uses a reduced version of SEXPREC as a
header in vector nodes.  The layout MUST be kept consistent with
the SEXPREC definition. The standard SEXPREC takes up 7 words
and the reduced version takes 6 words on most 64-bit systems. On most
32-bit systems, SEXPREC takes 8 words and the reduced version 7 words. */
pub type VECSEXP = *mut VECTOR_SEXPREC;
#[derive(Copy, Clone)]
#[repr(C)]
pub union SEXPREC_ALIGN {
    pub s: VECTOR_SEXPREC,
    pub align: libc::c_double,
}
pub type R_xlen_t = ptrdiff_t;
