use crate::sexprec::{SEXP, SEXPREC, SEXPREC_ALIGN, SEXPTYPE, VECSEXP};
use ::libc;
extern "C" {
    pub type R_allocator;
    #[no_mangle]
    fn snprintf(
        _: *mut libc::c_char,
        _: libc::c_ulong,
        _: *const libc::c_char,
        _: ...
    ) -> libc::c_int;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    #[no_mangle]
    fn installTrChar(_: SEXP) -> SEXP;
    /* IEEE -Inf */
    #[no_mangle]
    static mut R_NaReal: libc::c_double;
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2016  The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     *
     *
     * Memory Allocation (garbage collected) --- INCLUDING S compatibility ---
     */
    /* Included by R.h: API */
    /* for size_t */
    #[no_mangle]
    fn vmaxget() -> *mut libc::c_void;
    #[no_mangle]
    fn vmaxset(_: *const libc::c_void);
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn SET_STRING_ELT(x: SEXP, i: R_xlen_t, v: SEXP);
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    #[no_mangle]
    fn ALTREP_LENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn ALTVEC_DATAPTR(x: SEXP) -> *mut libc::c_void;
    #[no_mangle]
    fn ALTSTRING_ELT(_: SEXP, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn R_BadLongVector(_: SEXP, _: *const libc::c_char, _: libc::c_int) -> !;
    #[no_mangle]
    fn SET_TAG(x: SEXP, y: SEXP);
    #[no_mangle]
    fn SETCAR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    fn SETCDR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    fn SETCADR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    fn SETCADDR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    fn SETCADDDR(x: SEXP, y: SEXP) -> SEXP;
    #[no_mangle]
    fn SET_FORMALS(x: SEXP, v: SEXP);
    #[no_mangle]
    fn SET_BODY(x: SEXP, v: SEXP);
    #[no_mangle]
    fn SET_CLOENV(x: SEXP, v: SEXP);
    #[no_mangle]
    static mut R_GlobalEnv: SEXP;
    #[no_mangle]
    static mut R_NilValue: SEXP;
    #[no_mangle]
    static mut R_MissingArg: SEXP;
    #[no_mangle]
    static mut R_BraceSymbol: SEXP;
    #[no_mangle]
    static mut R_BracketSymbol: SEXP;
    #[no_mangle]
    static mut R_ClassSymbol: SEXP;
    #[no_mangle]
    fn asLogical(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asReal(x: SEXP) -> libc::c_double;
    #[no_mangle]
    fn allocList(_: libc::c_int) -> SEXP;
    #[no_mangle]
    fn allocSExp(_: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn allocVector3(_: SEXPTYPE, _: R_xlen_t, _: *mut R_allocator_t) -> SEXP;
    #[no_mangle]
    fn cons(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn duplicate(_: SEXP) -> SEXP;
    #[no_mangle]
    fn getAttrib(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn install(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn mkChar(_: *const libc::c_char) -> SEXP;
    #[no_mangle]
    fn translateChar(_: SEXP) -> *const libc::c_char;
    #[no_mangle]
    fn type2char(_: SEXPTYPE) -> *const libc::c_char;
    #[no_mangle]
    fn R_signal_protect_error() -> !;
    #[no_mangle]
    fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    #[no_mangle]
    static mut R_PPStackSize: libc::c_int;
    #[no_mangle]
    static mut R_PPStackTop: libc::c_int;
    #[no_mangle]
    static mut R_PPStack: *mut SEXP;
    /* INLINE_PROTECT */
    /* from dstruct.c */
    /*  length - length of objects  */
    #[no_mangle]
    fn envlength(rho: SEXP) -> libc::c_int;
    #[no_mangle]
    fn deparse1(_: SEXP, _: Rboolean, _: libc::c_int) -> SEXP;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1998-2001   The R Core Team
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
#[derive(Copy, Clone)]
#[repr(C)]
pub struct Rcomplex {
    pub r: libc::c_double,
    pub i: libc::c_double,
}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
pub type R_len_t = libc::c_int;
pub type R_xlen_t = ptrdiff_t;
pub type R_allocator_t = R_allocator;
#[inline]
unsafe extern "C" fn DATAPTR(mut x: SEXP) -> *mut libc::c_void {
    if (*x).sxpinfo.alt() != 0 {
        return ALTVEC_DATAPTR(x);
    } else {
        return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void;
    };
}
#[inline]
unsafe extern "C" fn XLENGTH_EX(mut x: SEXP) -> R_xlen_t {
    return if (*x).sxpinfo.alt() as libc::c_int != 0 {
        ALTREP_LENGTH(x)
    } else {
        (*(x as VECSEXP)).vecsxp.length
    };
}
#[inline]
unsafe extern "C" fn LENGTH_EX(
    mut x: SEXP,
    mut file: *const libc::c_char,
    mut line: libc::c_int,
) -> libc::c_int {
    if x == R_NilValue {
        return 0 as libc::c_int;
    }
    let mut len: R_xlen_t = XLENGTH_EX(x);
    if len > 2147483647 as libc::c_int as libc::c_long {
        R_BadLongVector(x, file, line);
    }
    return len as libc::c_int;
}
#[inline]
unsafe extern "C" fn INTEGER0(mut x: SEXP) -> *mut libc::c_int {
    return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void
        as *mut libc::c_int;
}
#[inline]
unsafe extern "C" fn SET_SCALAR_IVAL(mut x: SEXP, mut v: libc::c_int) {
    *INTEGER0(x).offset(0 as libc::c_int as isize) = v;
}
#[inline]
unsafe extern "C" fn REAL0(mut x: SEXP) -> *mut libc::c_double {
    return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void
        as *mut libc::c_double;
}
#[inline]
unsafe extern "C" fn SET_SCALAR_DVAL(mut x: SEXP, mut v: libc::c_double) {
    *REAL0(x).offset(0 as libc::c_int as isize) = v;
}
/*, MAYBE */
/* if not inlining use version in memory.c with more error checking */
#[inline]
unsafe extern "C" fn STRING_ELT(mut x: SEXP, mut i: R_xlen_t) -> SEXP {
    if (*x).sxpinfo.alt() != 0 {
        return ALTSTRING_ELT(x, i);
    } else {
        let mut ps: *mut SEXP = (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize)
            as *mut libc::c_void as *mut SEXP;
        return *ps.offset(i as isize);
    };
}
#[inline]
unsafe extern "C" fn protect(mut s: SEXP) -> SEXP {
    if R_PPStackTop < R_PPStackSize {
        let fresh0 = R_PPStackTop;
        R_PPStackTop = R_PPStackTop + 1;
        let ref mut fresh1 = *R_PPStack.offset(fresh0 as isize);
        *fresh1 = s
    } else {
        R_signal_protect_error();
    }
    return s;
}
#[inline]
unsafe extern "C" fn unprotect(mut l: libc::c_int) {
    R_PPStackTop -= l;
}
/* TODO: a  Length(.) {say} which is  length() + dispatch (S3 + S4) if needed
         for one approach, see do_seq_along() in ../main/seq.c
*/
#[inline]
unsafe extern "C" fn length(mut s: SEXP) -> R_len_t {
    match (*s).sxpinfo.type_0() as libc::c_int {
        0 => return 0 as libc::c_int,
        10 | 13 | 14 | 15 | 16 | 9 | 19 | 20 | 24 => {
            return LENGTH_EX(
                s,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                522 as libc::c_int,
            )
        }
        2 | 6 | 17 => {
            let mut i: libc::c_int = 0 as libc::c_int;
            while !s.is_null() && s != R_NilValue {
                i += 1;
                s = (*s).u.listsxp.cdrval
            }
            return i;
        }
        4 => return envlength(s),
        _ => return 1 as libc::c_int,
    };
}
/* regular allocVector() as a special case of allocVector3() with no custom allocator */
#[inline]
unsafe extern "C" fn allocVector(mut type_0: SEXPTYPE, mut length: R_xlen_t) -> SEXP {
    return allocVector3(type_0, length, 0 as *mut R_allocator_t);
}
/* Shorthands for creating small lists */
#[inline]
unsafe extern "C" fn list1(mut s: SEXP) -> SEXP {
    return cons(s, R_NilValue);
}
#[inline]
unsafe extern "C" fn list2(mut s: SEXP, mut t: SEXP) -> SEXP {
    protect(s);
    s = cons(s, list1(t));
    unprotect(1 as libc::c_int);
    return s;
}
#[inline]
unsafe extern "C" fn list3(mut s: SEXP, mut t: SEXP, mut u: SEXP) -> SEXP {
    protect(s);
    s = cons(s, list2(t, u));
    unprotect(1 as libc::c_int);
    return s;
}
#[inline]
unsafe extern "C" fn list4(mut s: SEXP, mut t: SEXP, mut u: SEXP, mut v: SEXP) -> SEXP {
    protect(s);
    s = cons(s, list3(t, u, v));
    unprotect(1 as libc::c_int);
    return s;
}
/* Language based list constructs.  These are identical to the list */
/* constructs, but the results can be evaluated. */
/* Return a (language) dotted pair with the given car and cdr */
#[inline]
unsafe extern "C" fn lcons(mut car: SEXP, mut cdr: SEXP) -> SEXP {
    let mut e: SEXP = cons(car, cdr);
    (*e).sxpinfo.set_type_0(6 as libc::c_int as SEXPTYPE);
    return e;
}
#[inline]
unsafe extern "C" fn lang2(mut s: SEXP, mut t: SEXP) -> SEXP {
    protect(s);
    s = lcons(s, list1(t));
    unprotect(1 as libc::c_int);
    return s;
}
#[inline]
unsafe extern "C" fn lang3(mut s: SEXP, mut t: SEXP, mut u: SEXP) -> SEXP {
    protect(s);
    s = lcons(s, list2(t, u));
    unprotect(1 as libc::c_int);
    return s;
}
#[inline]
unsafe extern "C" fn lang4(mut s: SEXP, mut t: SEXP, mut u: SEXP, mut v: SEXP) -> SEXP {
    protect(s);
    s = lcons(s, list3(t, u, v));
    unprotect(1 as libc::c_int);
    return s;
}
#[inline]
unsafe extern "C" fn lang5(
    mut s: SEXP,
    mut t: SEXP,
    mut u: SEXP,
    mut v: SEXP,
    mut w: SEXP,
) -> SEXP {
    protect(s);
    s = lcons(s, list4(t, u, v, w));
    unprotect(1 as libc::c_int);
    return s;
}
/* NOTE: R's inherits() is based on inherits3() in ../main/objects.c
 * Here, use char / CHAR() instead of the slower more general translateChar()
 */
#[inline]
unsafe extern "C" fn inherits(mut s: SEXP, mut name: *const libc::c_char) -> Rboolean {
    let mut klass: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut nclass: libc::c_int = 0;
    if (*s).sxpinfo.obj() != 0 {
        klass = getAttrib(s, R_ClassSymbol);
        nclass = length(klass);
        i = 0 as libc::c_int;
        while i < nclass {
            if strcmp(
                (STRING_ELT(klass, i as R_xlen_t) as *mut SEXPREC_ALIGN)
                    .offset(1 as libc::c_int as isize) as *mut libc::c_void
                    as *const libc::c_char,
                name,
            ) == 0
            {
                return TRUE;
            }
            i += 1
        }
    }
    return FALSE;
}
/* DIFFERENT than R's  is.language(.) in ../main/coerce.c [do_is(), case 301:]
 *                                    which is   <=>  SYMSXP || LANGSXP || EXPRSXP */
#[inline]
unsafe extern "C" fn isLanguage(mut s: SEXP) -> Rboolean {
    return (s == R_NilValue || (*s).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int)
        as libc::c_int as Rboolean;
}
/* Is an object of numeric type. */
/* FIXME:  the LGLSXP case should be excluded here
 * (really? in many places we affirm they are treated like INTs)*/
#[inline]
unsafe extern "C" fn isNumeric(mut s: SEXP) -> Rboolean {
    match (*s).sxpinfo.type_0() as libc::c_int {
        13 => {
            if inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as u64 != 0 {
                return FALSE;
            }
        }
        10 | 14 => {}
        _ => return FALSE,
    }
    return TRUE;
}
#[inline]
unsafe extern "C" fn ScalarInteger(mut x: libc::c_int) -> SEXP {
    let mut ans: SEXP = allocVector(13 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t);
    SET_SCALAR_IVAL(ans, x);
    return ans;
}
#[inline]
unsafe extern "C" fn ScalarReal(mut x: libc::c_double) -> SEXP {
    let mut ans: SEXP = allocVector(14 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t);
    SET_SCALAR_DVAL(ans, x);
    return ans;
}
#[inline]
unsafe extern "C" fn ScalarString(mut x: SEXP) -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    protect(x);
    ans = allocVector(16 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t);
    SET_STRING_ELT(ans, 0 as libc::c_int as R_xlen_t, x);
    unprotect(1 as libc::c_int);
    return ans;
}
/* from gram.y */
/* short cut for  ScalarString(mkChar(s)) : */
#[inline]
unsafe extern "C" fn mkString(mut s: *const libc::c_char) -> SEXP {
    let mut t: SEXP = 0 as *mut SEXPREC;
    t = allocVector(16 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t);
    protect(t);
    SET_STRING_ELT(t, 0 as libc::c_int as R_xlen_t, mkChar(s));
    unprotect(1 as libc::c_int);
    return t;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1998-2017   The R Core Team.
 *  Copyright (C) 2004-2017   The R Foundation
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 *
 *
 *  Symbolic Differentiation
 */
static mut ParenSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut PlusSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut MinusSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut TimesSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut DivideSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut PowerSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut ExpSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut LogSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut SinSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut CosSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut TanSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut SinhSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut CoshSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut TanhSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut SqrtSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut PnormSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut DnormSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut AsinSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut AcosSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut AtanSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut GammaSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut LGammaSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut DiGammaSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut TriGammaSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut PsiSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
/* new symbols in R 3.4.0: */
static mut PiSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut ExpM1Symbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut Log1PSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut Log2Symbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut Log10Symbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut SinPiSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut CosPiSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut TanPiSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut FactorialSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
static mut LFactorialSymbol: SEXP = 0 as *const SEXPREC as *mut SEXPREC;
/* possible future symbols
static SEXP Log1PExpSymbol;
static SEXP Log1MExpSymbol;
static SEXP Log1PMxSymbol;
*/
static mut Initialized: Rboolean = FALSE;
unsafe extern "C" fn InitDerivSymbols() {
    /* Called from doD() and deriv() */
    if Initialized as u64 != 0 {
        return;
    }
    ParenSymbol = install(b"(\x00" as *const u8 as *const libc::c_char);
    PlusSymbol = install(b"+\x00" as *const u8 as *const libc::c_char);
    MinusSymbol = install(b"-\x00" as *const u8 as *const libc::c_char);
    TimesSymbol = install(b"*\x00" as *const u8 as *const libc::c_char);
    DivideSymbol = install(b"/\x00" as *const u8 as *const libc::c_char);
    PowerSymbol = install(b"^\x00" as *const u8 as *const libc::c_char);
    ExpSymbol = install(b"exp\x00" as *const u8 as *const libc::c_char);
    LogSymbol = install(b"log\x00" as *const u8 as *const libc::c_char);
    SinSymbol = install(b"sin\x00" as *const u8 as *const libc::c_char);
    CosSymbol = install(b"cos\x00" as *const u8 as *const libc::c_char);
    TanSymbol = install(b"tan\x00" as *const u8 as *const libc::c_char);
    SinhSymbol = install(b"sinh\x00" as *const u8 as *const libc::c_char);
    CoshSymbol = install(b"cosh\x00" as *const u8 as *const libc::c_char);
    TanhSymbol = install(b"tanh\x00" as *const u8 as *const libc::c_char);
    SqrtSymbol = install(b"sqrt\x00" as *const u8 as *const libc::c_char);
    PnormSymbol = install(b"pnorm\x00" as *const u8 as *const libc::c_char);
    DnormSymbol = install(b"dnorm\x00" as *const u8 as *const libc::c_char);
    AsinSymbol = install(b"asin\x00" as *const u8 as *const libc::c_char);
    AcosSymbol = install(b"acos\x00" as *const u8 as *const libc::c_char);
    AtanSymbol = install(b"atan\x00" as *const u8 as *const libc::c_char);
    GammaSymbol = install(b"gamma\x00" as *const u8 as *const libc::c_char);
    LGammaSymbol = install(b"lgamma\x00" as *const u8 as *const libc::c_char);
    DiGammaSymbol = install(b"digamma\x00" as *const u8 as *const libc::c_char);
    TriGammaSymbol = install(b"trigamma\x00" as *const u8 as *const libc::c_char);
    PsiSymbol = install(b"psigamma\x00" as *const u8 as *const libc::c_char);
    /* new symbols */
    PiSymbol = install(b"pi\x00" as *const u8 as *const libc::c_char);
    ExpM1Symbol = install(b"expm1\x00" as *const u8 as *const libc::c_char);
    Log1PSymbol = install(b"log1p\x00" as *const u8 as *const libc::c_char);
    Log2Symbol = install(b"log2\x00" as *const u8 as *const libc::c_char);
    Log10Symbol = install(b"log10\x00" as *const u8 as *const libc::c_char);
    SinPiSymbol = install(b"sinpi\x00" as *const u8 as *const libc::c_char);
    CosPiSymbol = install(b"cospi\x00" as *const u8 as *const libc::c_char);
    TanPiSymbol = install(b"tanpi\x00" as *const u8 as *const libc::c_char);
    FactorialSymbol = install(b"factorial\x00" as *const u8 as *const libc::c_char);
    LFactorialSymbol = install(b"lfactorial\x00" as *const u8 as *const libc::c_char);
    /* possible future symbols
        Log1PExpSymbol = install("log1pexp");    # log(1+exp(x))
        Log1MExpSymbol = install("log1mexp");    # log(1-exp(-x)), for x > 0
        Log1PMxSymbol = install("log1pmx");      # log1p(x)-x
    */
    Initialized = TRUE;
}
unsafe extern "C" fn Constant(mut x: libc::c_double) -> SEXP {
    return ScalarReal(x);
}
unsafe extern "C" fn isZero(mut s: SEXP) -> libc::c_int {
    return (asReal(s) == 0.0f64) as libc::c_int;
}
unsafe extern "C" fn isOne(mut s: SEXP) -> libc::c_int {
    return (asReal(s) == 1.0f64) as libc::c_int;
}
unsafe extern "C" fn isUminus(mut s: SEXP) -> libc::c_int {
    if (*s).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int
        && (*s).u.listsxp.carval == MinusSymbol
    {
        match length(s) {
            2 => {
                return 1 as libc::c_int;
                /* for -Wall */
            }
            3 => {
                if (*(*(*s).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval
                    == R_MissingArg
                {
                    return 1 as libc::c_int;
                } else {
                    return 0 as libc::c_int;
                }
            }
            _ => {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"invalid form in unary minus check\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
        }
    } else {
        return 0 as libc::c_int;
    };
}
/* Pointer protect and return the argument */
unsafe extern "C" fn PP(mut s: SEXP) -> SEXP {
    protect(s);
    return s;
}
unsafe extern "C" fn simplify(mut fun: SEXP, mut arg1: SEXP, mut arg2: SEXP) -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    if fun == PlusSymbol {
        if isZero(arg1) != 0 {
            ans = arg2
        } else if isZero(arg2) != 0 {
            ans = arg1
        } else if isUminus(arg1) != 0 {
            ans = simplify(
                MinusSymbol,
                arg2,
                (*(*arg1).u.listsxp.cdrval).u.listsxp.carval,
            )
        } else if isUminus(arg2) != 0 {
            ans = simplify(
                MinusSymbol,
                arg1,
                (*(*arg2).u.listsxp.cdrval).u.listsxp.carval,
            )
        } else {
            ans = lang3(PlusSymbol, arg1, arg2)
        }
    } else if fun == MinusSymbol {
        if arg2 == R_MissingArg {
            if isZero(arg1) != 0 {
                ans = Constant(0.0f64)
            } else if isUminus(arg1) != 0 {
                ans = (*(*arg1).u.listsxp.cdrval).u.listsxp.carval
            } else {
                ans = lang2(MinusSymbol, arg1)
            }
        } else if isZero(arg2) != 0 {
            ans = arg1
        } else if isZero(arg1) != 0 {
            ans = simplify(MinusSymbol, arg2, R_MissingArg)
        } else if isUminus(arg1) != 0 {
            ans = simplify(
                MinusSymbol,
                PP(simplify(
                    PlusSymbol,
                    (*(*arg1).u.listsxp.cdrval).u.listsxp.carval,
                    arg2,
                )),
                R_MissingArg,
            );
            unprotect(1 as libc::c_int);
        } else if isUminus(arg2) != 0 {
            ans = simplify(
                PlusSymbol,
                arg1,
                (*(*arg2).u.listsxp.cdrval).u.listsxp.carval,
            )
        } else {
            ans = lang3(MinusSymbol, arg1, arg2)
        }
    } else if fun == TimesSymbol {
        if isZero(arg1) != 0 || isZero(arg2) != 0 {
            ans = Constant(0.0f64)
        } else if isOne(arg1) != 0 {
            ans = arg2
        } else if isOne(arg2) != 0 {
            ans = arg1
        } else if isUminus(arg1) != 0 {
            ans = simplify(
                MinusSymbol,
                PP(simplify(
                    TimesSymbol,
                    (*(*arg1).u.listsxp.cdrval).u.listsxp.carval,
                    arg2,
                )),
                R_MissingArg,
            );
            unprotect(1 as libc::c_int);
        } else if isUminus(arg2) != 0 {
            ans = simplify(
                MinusSymbol,
                PP(simplify(
                    TimesSymbol,
                    arg1,
                    (*(*arg2).u.listsxp.cdrval).u.listsxp.carval,
                )),
                R_MissingArg,
            );
            unprotect(1 as libc::c_int);
        } else {
            ans = lang3(TimesSymbol, arg1, arg2)
        }
    } else if fun == DivideSymbol {
        if isZero(arg1) != 0 {
            ans = Constant(0.0f64)
        } else if isZero(arg2) != 0 {
            ans = Constant(R_NaReal)
        } else if isOne(arg2) != 0 {
            ans = arg1
        } else if isUminus(arg1) != 0 {
            ans = simplify(
                MinusSymbol,
                PP(simplify(
                    DivideSymbol,
                    (*(*arg1).u.listsxp.cdrval).u.listsxp.carval,
                    arg2,
                )),
                R_MissingArg,
            );
            unprotect(1 as libc::c_int);
        } else if isUminus(arg2) != 0 {
            ans = simplify(
                MinusSymbol,
                PP(simplify(
                    DivideSymbol,
                    arg1,
                    (*(*arg2).u.listsxp.cdrval).u.listsxp.carval,
                )),
                R_MissingArg,
            );
            unprotect(1 as libc::c_int);
        } else {
            ans = lang3(DivideSymbol, arg1, arg2)
        }
    } else if fun == PowerSymbol {
        if isZero(arg2) != 0 {
            ans = Constant(1.0f64)
        } else if isZero(arg1) != 0 {
            ans = Constant(0.0f64)
        } else if isOne(arg1) != 0 {
            ans = Constant(1.0f64)
        } else if isOne(arg2) != 0 {
            ans = arg1
        } else {
            ans = lang3(PowerSymbol, arg1, arg2)
        }
    } else if fun == ExpSymbol {
        /* FIXME: simplify exp(lgamma( E )) = gamma( E ) */
        /* FIXME: simplify exp(lfactorial( E )) = factorial( E ) */
        ans = lang2(ExpSymbol, arg1)
    } else if fun == LogSymbol {
        /* FIXME: simplify log(gamma( E )) = lgamma( E ) */
        /* FIXME: simplify log(factorial( E )) = lfactorial( E ) */
        ans = lang2(LogSymbol, arg1)
    } else if fun == CosSymbol {
        ans = lang2(CosSymbol, arg1)
    } else if fun == SinSymbol {
        ans = lang2(SinSymbol, arg1)
    } else if fun == TanSymbol {
        ans = lang2(TanSymbol, arg1)
    } else if fun == CoshSymbol {
        ans = lang2(CoshSymbol, arg1)
    } else if fun == SinhSymbol {
        ans = lang2(SinhSymbol, arg1)
    } else if fun == TanhSymbol {
        ans = lang2(TanhSymbol, arg1)
    } else if fun == SqrtSymbol {
        ans = lang2(SqrtSymbol, arg1)
    } else if fun == PnormSymbol {
        ans = lang2(PnormSymbol, arg1)
    } else if fun == DnormSymbol {
        ans = lang2(DnormSymbol, arg1)
    } else if fun == AsinSymbol {
        ans = lang2(AsinSymbol, arg1)
    } else if fun == AcosSymbol {
        ans = lang2(AcosSymbol, arg1)
    } else if fun == AtanSymbol {
        ans = lang2(AtanSymbol, arg1)
    } else if fun == GammaSymbol {
        ans = lang2(GammaSymbol, arg1)
    } else if fun == LGammaSymbol {
        ans = lang2(LGammaSymbol, arg1)
    } else if fun == DiGammaSymbol {
        ans = lang2(DiGammaSymbol, arg1)
    } else if fun == TriGammaSymbol {
        ans = lang2(TriGammaSymbol, arg1)
    } else if fun == PsiSymbol {
        if arg2 == R_MissingArg {
            ans = lang2(PsiSymbol, arg1)
        } else {
            ans = lang3(PsiSymbol, arg1, arg2)
        }
    } else if fun == ExpM1Symbol {
        /* new symbols */
        /* FIXME: simplify expm1(log1p( E )) = E */
        ans = lang2(ExpM1Symbol, arg1)
    } else if fun == LogSymbol {
        /* FIXME: simplify log1p(expm1( E )) = E */
        ans = lang2(Log1PSymbol, arg1)
    } else if fun == Log2Symbol {
        ans = lang2(Log2Symbol, arg1)
    } else if fun == Log10Symbol {
        ans = lang2(Log10Symbol, arg1)
    } else if fun == CosPiSymbol {
        ans = lang2(CosPiSymbol, arg1)
    } else if fun == SinPiSymbol {
        ans = lang2(SinPiSymbol, arg1)
    } else if fun == TanPiSymbol {
        ans = lang2(TanPiSymbol, arg1)
    } else if fun == FactorialSymbol {
        ans = lang2(FactorialSymbol, arg1)
    } else if fun == LFactorialSymbol {
        ans = lang2(LFactorialSymbol, arg1)
    } else {
        /* possible future symbols
            else if (fun == Log1PExpSymbol) ans = lang2(Log1PExpSymbol, arg1);
            else if (fun == Log1MExpSymbol) ans = lang2(Log1MExpSymbol, arg1);
            else if (fun == Log1PMxSymbol) ans = lang2(Log1PMxSymbol, arg1);
        */
        ans = Constant(R_NaReal)
    }
    /* FIXME */
    return ans;
}
/* simplify() */
/* D() implements the "derivative table" : */
unsafe extern "C" fn D(mut expr: SEXP, mut var: SEXP) -> SEXP {
    let mut ans: SEXP = R_NilValue;
    let mut expr1: SEXP = 0 as *mut SEXPREC;
    let mut expr2: SEXP = 0 as *mut SEXPREC;
    match (*expr).sxpinfo.type_0() as libc::c_int {
        10 | 13 | 14 | 15 => ans = Constant(0 as libc::c_int as libc::c_double),
        1 => {
            if expr == var {
                ans = Constant(1.0f64)
            } else {
                ans = Constant(0.0f64)
            }
        }
        2 => {
            if inherits(expr, b"expression\x00" as *const u8 as *const libc::c_char) as u64 != 0 {
                ans = D((*expr).u.listsxp.carval, var)
            } else {
                ans = Constant(R_NaReal)
            }
        }
        6 => {
            if (*expr).u.listsxp.carval == ParenSymbol {
                ans = D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)
            } else if (*expr).u.listsxp.carval == PlusSymbol {
                if length(expr) == 2 as libc::c_int {
                    ans = D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)
                } else {
                    ans = simplify(
                        PlusSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        PP(D(
                            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            var,
                        )),
                    );
                    unprotect(2 as libc::c_int);
                }
            } else if (*expr).u.listsxp.carval == MinusSymbol {
                if length(expr) == 2 as libc::c_int {
                    ans = simplify(
                        MinusSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        R_MissingArg,
                    );
                    unprotect(1 as libc::c_int);
                } else {
                    ans = simplify(
                        MinusSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        PP(D(
                            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            var,
                        )),
                    );
                    unprotect(2 as libc::c_int);
                }
            } else if (*expr).u.listsxp.carval == TimesSymbol {
                ans = simplify(
                    PlusSymbol,
                    PP(simplify(
                        TimesSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                            .u
                            .listsxp
                            .carval,
                    )),
                    PP(simplify(
                        TimesSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        PP(D(
                            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            var,
                        )),
                    )),
                );
                unprotect(4 as libc::c_int);
            } else if (*expr).u.listsxp.carval == DivideSymbol {
                expr1 = D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var);
                protect(expr1);
                expr2 = D(
                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                    var,
                );
                protect(expr2);
                ans = simplify(
                    MinusSymbol,
                    PP(simplify(
                        DivideSymbol,
                        expr1,
                        (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                            .u
                            .listsxp
                            .carval,
                    )),
                    PP(simplify(
                        DivideSymbol,
                        PP(simplify(
                            TimesSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            expr2,
                        )),
                        PP(simplify(
                            PowerSymbol,
                            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            PP(Constant(2.0f64)),
                        )),
                    )),
                );
                unprotect(7 as libc::c_int);
            } else if (*expr).u.listsxp.carval == PowerSymbol {
                if (*(*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval)
                    .sxpinfo
                    .type_0() as libc::c_int
                    == 10 as libc::c_int
                    || isNumeric(
                        (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                            .u
                            .listsxp
                            .carval,
                    ) as libc::c_uint
                        != 0
                {
                    ans = simplify(
                        TimesSymbol,
                        (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                            .u
                            .listsxp
                            .carval,
                        PP(simplify(
                            TimesSymbol,
                            PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                            PP(simplify(
                                PowerSymbol,
                                (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                                PP(Constant(
                                    asReal(
                                        (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                            .u
                                            .listsxp
                                            .carval,
                                    ) - 1.0f64,
                                )),
                            )),
                        )),
                    );
                    unprotect(4 as libc::c_int);
                } else {
                    expr1 = simplify(
                        TimesSymbol,
                        PP(simplify(
                            PowerSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            PP(simplify(
                                MinusSymbol,
                                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                    .u
                                    .listsxp
                                    .carval,
                                PP(Constant(1.0f64)),
                            )),
                        )),
                        PP(simplify(
                            TimesSymbol,
                            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                            PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        )),
                    );
                    unprotect(5 as libc::c_int);
                    protect(expr1);
                    expr2 = simplify(
                        TimesSymbol,
                        PP(simplify(
                            PowerSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                .u
                                .listsxp
                                .carval,
                        )),
                        PP(simplify(
                            TimesSymbol,
                            PP(simplify(
                                LogSymbol,
                                (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                                R_MissingArg,
                            )),
                            PP(D(
                                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                    .u
                                    .listsxp
                                    .carval,
                                var,
                            )),
                        )),
                    );
                    unprotect(4 as libc::c_int);
                    protect(expr2);
                    ans = simplify(PlusSymbol, expr1, expr2);
                    unprotect(2 as libc::c_int);
                }
            } else if (*expr).u.listsxp.carval == ExpSymbol {
                ans = simplify(
                    TimesSymbol,
                    expr,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                );
                unprotect(1 as libc::c_int);
            } else if (*expr).u.listsxp.carval == LogSymbol {
                if length(expr) != 2 as libc::c_int {
                    error(b"only single-argument calls to log() are supported;\n  maybe use log(x,a) = log(x)/log(a)\x00"
                                 as *const u8 as *const libc::c_char);
                }
                ans = simplify(
                    DivideSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                );
                unprotect(1 as libc::c_int);
            } else if (*expr).u.listsxp.carval == CosSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        SinSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(simplify(
                        MinusSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        R_MissingArg,
                    )),
                );
                unprotect(3 as libc::c_int);
            } else if (*expr).u.listsxp.carval == SinSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        CosSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                );
                unprotect(2 as libc::c_int);
            } else if (*expr).u.listsxp.carval == TanSymbol {
                ans = simplify(
                    DivideSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        PowerSymbol,
                        PP(simplify(
                            CosSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            R_MissingArg,
                        )),
                        PP(Constant(2.0f64)),
                    )),
                );
                unprotect(4 as libc::c_int);
            } else if (*expr).u.listsxp.carval == CoshSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        SinhSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                );
                unprotect(2 as libc::c_int);
            } else if (*expr).u.listsxp.carval == SinhSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        CoshSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                );
                unprotect(2 as libc::c_int);
            } else if (*expr).u.listsxp.carval == TanhSymbol {
                ans = simplify(
                    DivideSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        PowerSymbol,
                        PP(simplify(
                            CoshSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            R_MissingArg,
                        )),
                        PP(Constant(2.0f64)),
                    )),
                );
                unprotect(4 as libc::c_int);
            } else if (*expr).u.listsxp.carval == SqrtSymbol {
                expr1 = allocList(3 as libc::c_int);
                protect(expr1);
                (*expr1).sxpinfo.set_type_0(6 as libc::c_int as SEXPTYPE);
                SETCAR(expr1, PowerSymbol);
                SETCADR(expr1, (*(*expr).u.listsxp.cdrval).u.listsxp.carval);
                SETCADDR(expr1, Constant(0.5f64));
                ans = D(expr1, var);
                unprotect(1 as libc::c_int);
            } else if (*expr).u.listsxp.carval == PnormSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        DnormSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                );
                unprotect(2 as libc::c_int);
            } else if (*expr).u.listsxp.carval == DnormSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        MinusSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(simplify(
                        TimesSymbol,
                        PP(simplify(
                            DnormSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            R_MissingArg,
                        )),
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    )),
                );
                unprotect(4 as libc::c_int);
            } else if (*expr).u.listsxp.carval == AsinSymbol {
                ans = simplify(
                    DivideSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        SqrtSymbol,
                        PP(simplify(
                            MinusSymbol,
                            PP(Constant(1.0f64)),
                            PP(simplify(
                                PowerSymbol,
                                (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                                PP(Constant(2.0f64)),
                            )),
                        )),
                        R_MissingArg,
                    )),
                );
                unprotect(6 as libc::c_int);
            } else if (*expr).u.listsxp.carval == AcosSymbol {
                ans = simplify(
                    MinusSymbol,
                    PP(simplify(
                        DivideSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        PP(simplify(
                            SqrtSymbol,
                            PP(simplify(
                                MinusSymbol,
                                PP(Constant(1.0f64)),
                                PP(simplify(
                                    PowerSymbol,
                                    (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                                    PP(Constant(2.0f64)),
                                )),
                            )),
                            R_MissingArg,
                        )),
                    )),
                    R_MissingArg,
                );
                unprotect(7 as libc::c_int);
            } else if (*expr).u.listsxp.carval == AtanSymbol {
                ans = simplify(
                    DivideSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        PlusSymbol,
                        PP(Constant(1.0f64)),
                        PP(simplify(
                            PowerSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            PP(Constant(2.0f64)),
                        )),
                    )),
                );
                unprotect(5 as libc::c_int);
            } else if (*expr).u.listsxp.carval == LGammaSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        DiGammaSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                );
                unprotect(2 as libc::c_int);
            } else if (*expr).u.listsxp.carval == GammaSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        TimesSymbol,
                        expr,
                        PP(simplify(
                            DiGammaSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            R_MissingArg,
                        )),
                    )),
                );
                unprotect(3 as libc::c_int);
            } else if (*expr).u.listsxp.carval == DiGammaSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        TriGammaSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                );
                unprotect(2 as libc::c_int);
            } else if (*expr).u.listsxp.carval == TriGammaSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        PsiSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        PP(ScalarInteger(2 as libc::c_int)),
                    )),
                );
                unprotect(3 as libc::c_int);
            } else if (*expr).u.listsxp.carval == PsiSymbol {
                if length(expr) == 2 as libc::c_int {
                    ans = simplify(
                        TimesSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        PP(simplify(
                            PsiSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            PP(ScalarInteger(1 as libc::c_int)),
                        )),
                    );
                    unprotect(3 as libc::c_int);
                } else if (*(*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval)
                    .sxpinfo
                    .type_0() as libc::c_int
                    == 13 as libc::c_int
                    || (*(*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval)
                        .sxpinfo
                        .type_0() as libc::c_int
                        == 14 as libc::c_int
                {
                    ans = simplify(
                        TimesSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        PP(simplify(
                            PsiSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            PP(ScalarInteger(
                                asInteger(
                                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                        .u
                                        .listsxp
                                        .carval,
                                ) + 1 as libc::c_int,
                            )),
                        )),
                    );
                    unprotect(3 as libc::c_int);
                } else {
                    ans = simplify(
                        TimesSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                        PP(simplify(
                            PsiSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            simplify(
                                PlusSymbol,
                                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                                    .u
                                    .listsxp
                                    .carval,
                                PP(ScalarInteger(1 as libc::c_int)),
                            ),
                        )),
                    );
                    unprotect(3 as libc::c_int);
                }
            } else if (*expr).u.listsxp.carval == ExpM1Symbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        ExpSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                );
                unprotect(2 as libc::c_int);
            } else if (*expr).u.listsxp.carval == Log1PSymbol {
                ans = simplify(
                    DivideSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        PlusSymbol,
                        PP(Constant(1.0f64)),
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                    )),
                );
                unprotect(3 as libc::c_int);
            } else if (*expr).u.listsxp.carval == Log2Symbol {
                ans = simplify(
                    DivideSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        TimesSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        PP(simplify(LogSymbol, PP(Constant(2.0f64)), R_MissingArg)),
                    )),
                );
                unprotect(4 as libc::c_int);
            } else if (*expr).u.listsxp.carval == Log10Symbol {
                ans = simplify(
                    DivideSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        TimesSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        PP(simplify(LogSymbol, PP(Constant(10.0f64)), R_MissingArg)),
                    )),
                );
                unprotect(4 as libc::c_int);
            } else if (*expr).u.listsxp.carval == CosPiSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        SinPiSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(simplify(
                        TimesSymbol,
                        PP(simplify(MinusSymbol, PiSymbol, R_MissingArg)),
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    )),
                );
                unprotect(4 as libc::c_int);
            } else if (*expr).u.listsxp.carval == SinPiSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(simplify(
                        CosPiSymbol,
                        (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                        R_MissingArg,
                    )),
                    PP(simplify(
                        TimesSymbol,
                        PiSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    )),
                );
                unprotect(3 as libc::c_int);
            } else if (*expr).u.listsxp.carval == TanPiSymbol {
                ans = simplify(
                    DivideSymbol,
                    PP(simplify(
                        TimesSymbol,
                        PiSymbol,
                        PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    )),
                    PP(simplify(
                        PowerSymbol,
                        PP(simplify(
                            CosPiSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            R_MissingArg,
                        )),
                        PP(Constant(2.0f64)),
                    )),
                );
                unprotect(5 as libc::c_int);
            } else if (*expr).u.listsxp.carval == LFactorialSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        DiGammaSymbol,
                        PP(simplify(
                            PlusSymbol,
                            (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                            PP(ScalarInteger(1 as libc::c_int)),
                        )),
                        R_MissingArg,
                    )),
                );
                unprotect(4 as libc::c_int);
            } else if (*expr).u.listsxp.carval == FactorialSymbol {
                ans = simplify(
                    TimesSymbol,
                    PP(D((*(*expr).u.listsxp.cdrval).u.listsxp.carval, var)),
                    PP(simplify(
                        TimesSymbol,
                        expr,
                        PP(simplify(
                            DiGammaSymbol,
                            PP(simplify(
                                PlusSymbol,
                                (*(*expr).u.listsxp.cdrval).u.listsxp.carval,
                                PP(ScalarInteger(1 as libc::c_int)),
                            )),
                            R_MissingArg,
                        )),
                    )),
                );
                unprotect(5 as libc::c_int);
            } else {
                /* new in R 3.4.0 */
                /* possible future symbols
                        else if (CAR(expr) == Log1PExpSymbol) {
                            ans = simplify(DivideSymbol,
                                           PP_S(TimesSymbol, PP(D(CADR(expr), var)),
                                                PP_S2(ExpSymbol, CADR(expr))),
                                           PP_S(PlusSymbol,PP(Constant(1.)),
                                                PP_S2(ExpSymbol, CADR(expr)) ));
                            UNPROTECT(6);
                        }
                        else if (CAR(expr) == Log1MExpSymbol) {
                            ans = simplify(DivideSymbol,
                                           PP_S(TimesSymbol, PP_S2(MinusSymbol, PP(D(CADR(expr), var))),
                                                PP_S2(ExpSymbol, PP_S2(MinusSymbol, CADR(expr))) ),
                                           PP_S2(ExpM1Symbol, PP_S2(MinusSymbol, CADR(expr))) );
                            UNPROTECT(7);
                        }
                        else if (CAR(expr) == Log1PMxSymbol) {
                            ans = simplify(DivideSymbol,
                                           PP_S2(MinusSymbol, PP(D(CADR(expr), var))),
                                           PP_S(PlusSymbol,PP(Constant(1.)), CADR(expr)) );
                            UNPROTECT(4);
                        }
                */
                let mut u: SEXP = deparse1((*expr).u.listsxp.carval, FALSE, 0 as libc::c_int);
                error(
                    dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"Function \'%s\' is not in the derivatives table\x00" as *const u8
                            as *const libc::c_char,
                        5 as libc::c_int,
                    ),
                    translateChar(STRING_ELT(u, 0 as libc::c_int as R_xlen_t)),
                );
            }
        }
        _ => ans = Constant(R_NaReal),
    }
    return ans;
}
/* D() */
unsafe extern "C" fn isPlusForm(mut expr: SEXP) -> libc::c_int {
    return ((*expr).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int
        && length(expr) == 3 as libc::c_int
        && (*expr).u.listsxp.carval == PlusSymbol) as libc::c_int;
}
unsafe extern "C" fn isMinusForm(mut expr: SEXP) -> libc::c_int {
    return ((*expr).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int
        && length(expr) == 3 as libc::c_int
        && (*expr).u.listsxp.carval == MinusSymbol) as libc::c_int;
}
unsafe extern "C" fn isTimesForm(mut expr: SEXP) -> libc::c_int {
    return ((*expr).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int
        && length(expr) == 3 as libc::c_int
        && (*expr).u.listsxp.carval == TimesSymbol) as libc::c_int;
}
unsafe extern "C" fn isDivideForm(mut expr: SEXP) -> libc::c_int {
    return ((*expr).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int
        && length(expr) == 3 as libc::c_int
        && (*expr).u.listsxp.carval == DivideSymbol) as libc::c_int;
}
unsafe extern "C" fn isPowerForm(mut expr: SEXP) -> libc::c_int {
    return ((*expr).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int
        && length(expr) == 3 as libc::c_int
        && (*expr).u.listsxp.carval == PowerSymbol) as libc::c_int;
}
unsafe extern "C" fn AddParens(mut expr: SEXP) -> SEXP {
    let mut e: SEXP = 0 as *mut SEXPREC;
    if (*expr).sxpinfo.type_0() as libc::c_int == 6 as libc::c_int {
        e = (*expr).u.listsxp.cdrval;
        while e != R_NilValue {
            SETCAR(e, AddParens((*e).u.listsxp.carval));
            e = (*e).u.listsxp.cdrval
        }
    }
    if isPlusForm(expr) != 0 {
        if isPlusForm(
            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                .u
                .listsxp
                .carval,
        ) != 0
        {
            SETCADDR(
                expr,
                lang2(
                    ParenSymbol,
                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                ),
            );
        }
    } else if isMinusForm(expr) != 0 {
        if isPlusForm(
            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                .u
                .listsxp
                .carval,
        ) != 0
            || isMinusForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
        {
            SETCADDR(
                expr,
                lang2(
                    ParenSymbol,
                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                ),
            );
        }
    } else if isTimesForm(expr) != 0 {
        if isPlusForm(
            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                .u
                .listsxp
                .carval,
        ) != 0
            || isMinusForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
            || isTimesForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
            || isDivideForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
        {
            SETCADDR(
                expr,
                lang2(
                    ParenSymbol,
                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                ),
            );
        }
        if isPlusForm((*(*expr).u.listsxp.cdrval).u.listsxp.carval) != 0
            || isMinusForm((*(*expr).u.listsxp.cdrval).u.listsxp.carval) != 0
        {
            SETCADR(
                expr,
                lang2(ParenSymbol, (*(*expr).u.listsxp.cdrval).u.listsxp.carval),
            );
        }
    } else if isDivideForm(expr) != 0 {
        if isPlusForm(
            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                .u
                .listsxp
                .carval,
        ) != 0
            || isMinusForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
            || isTimesForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
            || isDivideForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
        {
            SETCADDR(
                expr,
                lang2(
                    ParenSymbol,
                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                ),
            );
        }
        if isPlusForm((*(*expr).u.listsxp.cdrval).u.listsxp.carval) != 0
            || isMinusForm((*(*expr).u.listsxp.cdrval).u.listsxp.carval) != 0
        {
            SETCADR(
                expr,
                lang2(ParenSymbol, (*(*expr).u.listsxp.cdrval).u.listsxp.carval),
            );
        }
    } else if isPowerForm(expr) != 0 {
        if isPowerForm((*(*expr).u.listsxp.cdrval).u.listsxp.carval) != 0 {
            SETCADR(
                expr,
                lang2(ParenSymbol, (*(*expr).u.listsxp.cdrval).u.listsxp.carval),
            );
        }
        if isPlusForm(
            (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                .u
                .listsxp
                .carval,
        ) != 0
            || isMinusForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
            || isTimesForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
            || isDivideForm(
                (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                    .u
                    .listsxp
                    .carval,
            ) != 0
        {
            SETCADDR(
                expr,
                lang2(
                    ParenSymbol,
                    (*(*(*expr).u.listsxp.cdrval).u.listsxp.cdrval)
                        .u
                        .listsxp
                        .carval,
                ),
            );
        }
    }
    return expr;
}
#[no_mangle]
pub unsafe extern "C" fn doD(mut args: SEXP) -> SEXP {
    let mut expr: SEXP = 0 as *mut SEXPREC;
    let mut var: SEXP = 0 as *mut SEXPREC;
    args = (*args).u.listsxp.cdrval;
    if (*(*args).u.listsxp.carval).sxpinfo.type_0() as libc::c_int == 20 as libc::c_int {
        expr = *(DATAPTR((*args).u.listsxp.carval) as *mut SEXP).offset(0 as libc::c_int as isize)
    } else {
        expr = (*args).u.listsxp.carval
    }
    if !(isLanguage(expr) as libc::c_uint != 0
        || (*expr).sxpinfo.type_0() as libc::c_int == 1 as libc::c_int
        || isNumeric(expr) as libc::c_uint != 0
        || (*expr).sxpinfo.type_0() as libc::c_int == 15 as libc::c_int)
    {
        error(
            dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"expression must not be type \'%s\'\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ),
            type2char((*expr).sxpinfo.type_0()),
        );
    }
    var = (*(*args).u.listsxp.cdrval).u.listsxp.carval;
    if !((*var).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int)
        || length(var) < 1 as libc::c_int
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"variable must be a character string\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if length(var) > 1 as libc::c_int {
        warning(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"only the first element is used as variable name\x00" as *const u8
                as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    var = installTrChar(STRING_ELT(var, 0 as libc::c_int as R_xlen_t));
    InitDerivSymbols();
    expr = D(expr, var);
    protect(expr);
    expr = AddParens(expr);
    unprotect(1 as libc::c_int);
    return expr;
}
/* ------ FindSubexprs ------ and ------ Accumulate ------ */
unsafe extern "C" fn InvalidExpression(mut where_0: *mut libc::c_char) -> ! {
    error(
        dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid expression in \'%s\'\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ),
        where_0,
    );
}
unsafe extern "C" fn equal(mut expr1: SEXP, mut expr2: SEXP) -> libc::c_int {
    if (*expr1).sxpinfo.type_0() as libc::c_int == (*expr2).sxpinfo.type_0() as libc::c_int {
        match (*expr1).sxpinfo.type_0() as libc::c_int {
            0 => return 1 as libc::c_int,
            1 => return (expr1 == expr2) as libc::c_int,
            10 | 13 => {
                return (*(DATAPTR(expr1) as *mut libc::c_int).offset(0 as libc::c_int as isize)
                    == *(DATAPTR(expr2) as *mut libc::c_int).offset(0 as libc::c_int as isize))
                    as libc::c_int
            }
            14 => {
                return (*(DATAPTR(expr1) as *mut libc::c_double).offset(0 as libc::c_int as isize)
                    == *(DATAPTR(expr2) as *mut libc::c_double).offset(0 as libc::c_int as isize))
                    as libc::c_int
            }
            15 => {
                return ((*(DATAPTR(expr1) as *mut Rcomplex).offset(0 as libc::c_int as isize)).r
                    == (*(DATAPTR(expr2) as *mut Rcomplex).offset(0 as libc::c_int as isize)).r
                    && (*(DATAPTR(expr1) as *mut Rcomplex).offset(0 as libc::c_int as isize)).i
                        == (*(DATAPTR(expr2) as *mut Rcomplex).offset(0 as libc::c_int as isize)).i)
                    as libc::c_int
            }
            6 | 2 => {
                return (equal((*expr1).u.listsxp.carval, (*expr2).u.listsxp.carval) != 0
                    && equal((*expr1).u.listsxp.cdrval, (*expr2).u.listsxp.cdrval) != 0)
                    as libc::c_int
            }
            _ => {
                InvalidExpression(
                    b"equal\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
                );
            }
        }
    }
    return 0 as libc::c_int;
}
unsafe extern "C" fn Accumulate(mut expr: SEXP, mut exprlist: SEXP) -> libc::c_int {
    let mut e: SEXP = 0 as *mut SEXPREC;
    let mut k: libc::c_int = 0;
    e = exprlist;
    k = 0 as libc::c_int;
    while (*e).u.listsxp.cdrval != R_NilValue {
        e = (*e).u.listsxp.cdrval;
        k = k + 1 as libc::c_int;
        if equal(expr, (*e).u.listsxp.carval) != 0 {
            return k;
        }
    }
    SETCDR(e, cons(expr, R_NilValue));
    return k + 1 as libc::c_int;
}
unsafe extern "C" fn Accumulate2(mut expr: SEXP, mut exprlist: SEXP) -> libc::c_int {
    let mut e: SEXP = 0 as *mut SEXPREC;
    let mut k: libc::c_int = 0;
    e = exprlist;
    k = 0 as libc::c_int;
    while (*e).u.listsxp.cdrval != R_NilValue {
        e = (*e).u.listsxp.cdrval;
        k = k + 1 as libc::c_int
    }
    SETCDR(e, cons(expr, R_NilValue));
    return k + 1 as libc::c_int;
}
unsafe extern "C" fn MakeVariable(mut k: libc::c_int, mut tag: SEXP) -> SEXP {
    let mut vmax: *const libc::c_void = vmaxget();
    let mut buf: [libc::c_char; 64] = [0; 64];
    snprintf(
        buf.as_mut_ptr(),
        64 as libc::c_int as libc::c_ulong,
        b"%s%d\x00" as *const u8 as *const libc::c_char,
        translateChar(STRING_ELT(tag, 0 as libc::c_int as R_xlen_t)),
        k,
    );
    vmaxset(vmax);
    return install(buf.as_mut_ptr());
}
unsafe extern "C" fn FindSubexprs(
    mut expr: SEXP,
    mut exprlist: SEXP,
    mut tag: SEXP,
) -> libc::c_int {
    let mut e: SEXP = 0 as *mut SEXPREC;
    let mut k: libc::c_int = 0;
    match (*expr).sxpinfo.type_0() as libc::c_int {
        1 | 10 | 13 | 14 | 15 => {
            return 0 as libc::c_int;
            /*-Wall*/
        }
        2 => {
            if inherits(expr, b"expression\x00" as *const u8 as *const libc::c_char) as u64 != 0 {
                return FindSubexprs((*expr).u.listsxp.carval, exprlist, tag);
            } else {
                InvalidExpression(
                    b"FindSubexprs\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
                );
                /*-Wall*/
            }
        }
        6 => {
            if (*expr).u.listsxp.carval == install(b"(\x00" as *const u8 as *const libc::c_char) {
                return FindSubexprs((*(*expr).u.listsxp.cdrval).u.listsxp.carval, exprlist, tag);
            } else {
                e = (*expr).u.listsxp.cdrval; /* was real? */
                while e != R_NilValue {
                    k = FindSubexprs((*e).u.listsxp.carval, exprlist, tag);
                    if k != 0 as libc::c_int {
                        SETCAR(e, MakeVariable(k, tag));
                    }
                    e = (*e).u.listsxp.cdrval
                }
                return Accumulate(expr, exprlist);
            }
        }
        _ => {
            InvalidExpression(
                b"FindSubexprs\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
            );
        }
    };
}
unsafe extern "C" fn CountOccurrences(mut sym: SEXP, mut lst: SEXP) -> libc::c_int {
    match (*lst).sxpinfo.type_0() as libc::c_int {
        1 => return (lst == sym) as libc::c_int,
        2 | 6 => {
            return CountOccurrences(sym, (*lst).u.listsxp.carval)
                + CountOccurrences(sym, (*lst).u.listsxp.cdrval)
        }
        _ => return 0 as libc::c_int,
    };
}
unsafe extern "C" fn Replace(mut sym: SEXP, mut expr: SEXP, mut lst: SEXP) -> SEXP {
    match (*lst).sxpinfo.type_0() as libc::c_int {
        1 => {
            if lst == sym {
                return expr;
            } else {
                return lst;
            }
        }
        2 | 6 => {
            SETCAR(lst, Replace(sym, expr, (*lst).u.listsxp.carval));
            SETCDR(lst, Replace(sym, expr, (*lst).u.listsxp.cdrval));
            return lst;
        }
        _ => return lst,
    };
}
unsafe extern "C" fn CreateGrad(mut names: SEXP) -> SEXP {
    let mut p: SEXP = 0 as *mut SEXPREC;
    let mut q: SEXP = 0 as *mut SEXPREC;
    let mut data: SEXP = 0 as *mut SEXPREC;
    let mut dim: SEXP = 0 as *mut SEXPREC;
    let mut dimnames: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    n = length(names);
    dimnames = lang3(R_NilValue, R_NilValue, R_NilValue);
    protect(dimnames);
    SETCAR(
        dimnames,
        install(b"list\x00" as *const u8 as *const libc::c_char),
    );
    p = install(b"c\x00" as *const u8 as *const libc::c_char);
    q = allocList(n);
    protect(q);
    SETCADDR(dimnames, lcons(p, q));
    unprotect(1 as libc::c_int);
    i = 0 as libc::c_int;
    while i < n {
        SETCAR(q, ScalarString(STRING_ELT(names, i as R_xlen_t)));
        q = (*q).u.listsxp.cdrval;
        i += 1
    }
    dim = lang3(R_NilValue, R_NilValue, R_NilValue);
    protect(dim);
    SETCAR(dim, install(b"c\x00" as *const u8 as *const libc::c_char));
    SETCADR(
        dim,
        lang2(
            install(b"length\x00" as *const u8 as *const libc::c_char),
            install(b".value\x00" as *const u8 as *const libc::c_char),
        ),
    );
    SETCADDR(dim, ScalarInteger(length(names)));
    data = ScalarReal(0.0f64);
    protect(data);
    p = lang4(
        install(b"array\x00" as *const u8 as *const libc::c_char),
        data,
        dim,
        dimnames,
    );
    protect(p);
    p = lang3(
        install(b"<-\x00" as *const u8 as *const libc::c_char),
        install(b".grad\x00" as *const u8 as *const libc::c_char),
        p,
    );
    unprotect(4 as libc::c_int);
    return p;
}
unsafe extern "C" fn CreateHess(mut names: SEXP) -> SEXP {
    let mut p: SEXP = 0 as *mut SEXPREC;
    let mut q: SEXP = 0 as *mut SEXPREC;
    let mut data: SEXP = 0 as *mut SEXPREC;
    let mut dim: SEXP = 0 as *mut SEXPREC;
    let mut dimnames: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    n = length(names);
    dimnames = lang4(R_NilValue, R_NilValue, R_NilValue, R_NilValue);
    protect(dimnames);
    SETCAR(
        dimnames,
        install(b"list\x00" as *const u8 as *const libc::c_char),
    );
    p = install(b"c\x00" as *const u8 as *const libc::c_char);
    q = allocList(n);
    protect(q);
    SETCADDR(dimnames, lcons(p, q));
    unprotect(1 as libc::c_int);
    i = 0 as libc::c_int;
    while i < n {
        SETCAR(q, ScalarString(STRING_ELT(names, i as R_xlen_t)));
        q = (*q).u.listsxp.cdrval;
        i += 1
    }
    SETCADDDR(
        dimnames,
        duplicate(
            (*(*(*dimnames).u.listsxp.cdrval).u.listsxp.cdrval)
                .u
                .listsxp
                .carval,
        ),
    );
    dim = lang4(R_NilValue, R_NilValue, R_NilValue, R_NilValue);
    protect(dim);
    SETCAR(dim, install(b"c\x00" as *const u8 as *const libc::c_char));
    SETCADR(
        dim,
        lang2(
            install(b"length\x00" as *const u8 as *const libc::c_char),
            install(b".value\x00" as *const u8 as *const libc::c_char),
        ),
    );
    SETCADDR(dim, ScalarInteger(length(names)));
    SETCADDDR(dim, ScalarInteger(length(names)));
    data = ScalarReal(0.0f64);
    protect(data);
    p = lang4(
        install(b"array\x00" as *const u8 as *const libc::c_char),
        data,
        dim,
        dimnames,
    );
    protect(p);
    p = lang3(
        install(b"<-\x00" as *const u8 as *const libc::c_char),
        install(b".hessian\x00" as *const u8 as *const libc::c_char),
        p,
    );
    unprotect(4 as libc::c_int);
    return p;
}
unsafe extern "C" fn DerivAssign(mut name: SEXP, mut expr: SEXP) -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut newname: SEXP = 0 as *mut SEXPREC;
    ans = lang3(
        install(b"<-\x00" as *const u8 as *const libc::c_char),
        R_NilValue,
        expr,
    );
    protect(ans);
    newname = ScalarString(name);
    protect(newname);
    SETCADR(
        ans,
        lang4(
            R_BracketSymbol,
            install(b".grad\x00" as *const u8 as *const libc::c_char),
            R_MissingArg,
            newname,
        ),
    );
    unprotect(2 as libc::c_int);
    return ans;
}
unsafe extern "C" fn HessAssign1(mut name: SEXP, mut expr: SEXP) -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut newname: SEXP = 0 as *mut SEXPREC;
    ans = lang3(
        install(b"<-\x00" as *const u8 as *const libc::c_char),
        R_NilValue,
        expr,
    );
    protect(ans);
    newname = ScalarString(name);
    protect(newname);
    SETCADR(
        ans,
        lang5(
            R_BracketSymbol,
            install(b".hessian\x00" as *const u8 as *const libc::c_char),
            R_MissingArg,
            newname,
            newname,
        ),
    );
    unprotect(2 as libc::c_int);
    return ans;
}
unsafe extern "C" fn HessAssign2(mut name1: SEXP, mut name2: SEXP, mut expr: SEXP) -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut newname1: SEXP = 0 as *mut SEXPREC;
    let mut newname2: SEXP = 0 as *mut SEXPREC;
    let mut tmp1: SEXP = 0 as *mut SEXPREC;
    let mut tmp2: SEXP = 0 as *mut SEXPREC;
    let mut tmp3: SEXP = 0 as *mut SEXPREC;
    newname1 = ScalarString(name1);
    protect(newname1);
    newname2 = ScalarString(name2);
    protect(newname2);
    /* this is overkill, but PR#14772 found an issue */
    tmp1 = lang5(
        R_BracketSymbol,
        install(b".hessian\x00" as *const u8 as *const libc::c_char),
        R_MissingArg,
        newname1,
        newname2,
    );
    protect(tmp1);
    tmp2 = lang5(
        R_BracketSymbol,
        install(b".hessian\x00" as *const u8 as *const libc::c_char),
        R_MissingArg,
        newname2,
        newname1,
    );
    protect(tmp2);
    tmp3 = lang3(
        install(b"<-\x00" as *const u8 as *const libc::c_char),
        tmp2,
        expr,
    );
    protect(tmp3);
    ans = lang3(
        install(b"<-\x00" as *const u8 as *const libc::c_char),
        tmp1,
        tmp3,
    );
    unprotect(5 as libc::c_int);
    return ans;
}
/* attr(.value, "gradient") <- .grad */
unsafe extern "C" fn AddGrad() -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    ans = mkString(b"gradient\x00" as *const u8 as *const libc::c_char);
    protect(ans);
    ans = lang3(
        install(b"attr\x00" as *const u8 as *const libc::c_char),
        install(b".value\x00" as *const u8 as *const libc::c_char),
        ans,
    );
    protect(ans);
    ans = lang3(
        install(b"<-\x00" as *const u8 as *const libc::c_char),
        ans,
        install(b".grad\x00" as *const u8 as *const libc::c_char),
    );
    unprotect(2 as libc::c_int);
    return ans;
}
unsafe extern "C" fn AddHess() -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    ans = mkString(b"hessian\x00" as *const u8 as *const libc::c_char);
    protect(ans);
    ans = lang3(
        install(b"attr\x00" as *const u8 as *const libc::c_char),
        install(b".value\x00" as *const u8 as *const libc::c_char),
        ans,
    );
    protect(ans);
    ans = lang3(
        install(b"<-\x00" as *const u8 as *const libc::c_char),
        ans,
        install(b".hessian\x00" as *const u8 as *const libc::c_char),
    );
    unprotect(2 as libc::c_int);
    return ans;
}
unsafe extern "C" fn Prune(mut lst: SEXP) -> SEXP {
    if lst == R_NilValue {
        return lst;
    }
    SETCDR(lst, Prune((*lst).u.listsxp.cdrval));
    if (*lst).u.listsxp.carval == R_MissingArg {
        return (*lst).u.listsxp.cdrval;
    } else {
        return lst;
    };
}
#[no_mangle]
pub unsafe extern "C" fn deriv(mut args: SEXP) -> SEXP {
    /* deriv(expr, namevec, function.arg, tag, hessian) */
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut ans2: SEXP = 0 as *mut SEXPREC;
    let mut expr: SEXP = 0 as *mut SEXPREC;
    let mut funarg: SEXP = 0 as *mut SEXPREC;
    let mut names: SEXP = 0 as *mut SEXPREC;
    let mut s: SEXP = 0 as *mut SEXPREC;
    let mut f_index: libc::c_int = 0;
    let mut d_index: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut d2_index: *mut libc::c_int = 0 as *mut libc::c_int;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut nexpr: libc::c_int = 0;
    let mut nderiv: libc::c_int = 0 as libc::c_int;
    let mut hessian: libc::c_int = 0;
    let mut exprlist: SEXP = 0 as *mut SEXPREC;
    let mut tag: SEXP = 0 as *mut SEXPREC;
    args = (*args).u.listsxp.cdrval;
    InitDerivSymbols();
    exprlist = lcons(R_BraceSymbol, R_NilValue);
    protect(exprlist);
    /* expr: */
    if (*(*args).u.listsxp.carval).sxpinfo.type_0() as libc::c_int == 20 as libc::c_int {
        expr = *(DATAPTR((*args).u.listsxp.carval) as *mut SEXP).offset(0 as libc::c_int as isize);
        protect(expr);
    } else {
        expr = (*args).u.listsxp.carval;
        protect(expr);
    }
    args = (*args).u.listsxp.cdrval;
    /* namevec: */
    names = (*args).u.listsxp.carval;
    if !((*names).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int) || {
        nderiv = length(names);
        (nderiv) < 1 as libc::c_int
    } {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid variable names\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    args = (*args).u.listsxp.cdrval;
    /* function.arg: */
    funarg = (*args).u.listsxp.carval;
    args = (*args).u.listsxp.cdrval;
    /* tag: */
    tag = (*args).u.listsxp.carval;
    if !((*tag).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int)
        || length(tag) < 1 as libc::c_int
        || length(STRING_ELT(tag, 0 as libc::c_int as R_xlen_t)) < 1 as libc::c_int
        || length(STRING_ELT(tag, 0 as libc::c_int as R_xlen_t)) > 60 as libc::c_int
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"invalid tag\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    args = (*args).u.listsxp.cdrval;
    /* hessian: */
    hessian = asLogical((*args).u.listsxp.carval);
    /* NOTE: FindSubexprs is destructive, hence the duplication.
      It can allocate, so protect the duplicate.
    */
    ans = duplicate(expr); /*-Wall*/
    protect(ans); /* keep a temporary copy */
    f_index = FindSubexprs(ans, exprlist, tag); /* examine the derivative first */
    d_index = R_alloc(
        nderiv as size_t,
        ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_int; /* restore the copy */
    if hessian != 0 {
        d2_index = R_alloc(
            (nderiv * (1 as libc::c_int + nderiv) / 2 as libc::c_int) as size_t,
            ::std::mem::size_of::<libc::c_int>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_int
    } else {
        d2_index = d_index
    } /* install could allocate */
    unprotect(1 as libc::c_int); /* the first derivative is constant or simple variable */
    i = 0 as libc::c_int;
    k = 0 as libc::c_int;
    while i < nderiv {
        ans = duplicate(expr);
        protect(ans);
        ans = D(ans, installTrChar(STRING_ELT(names, i as R_xlen_t)));
        protect(ans);
        ans2 = duplicate(ans);
        protect(ans2);
        *d_index.offset(i as isize) = FindSubexprs(ans, exprlist, tag);
        ans = duplicate(ans2);
        protect(ans);
        if hessian != 0 {
            j = i;
            while j < nderiv {
                ans2 = duplicate(ans);
                protect(ans2);
                ans2 = D(ans2, installTrChar(STRING_ELT(names, j as R_xlen_t)));
                protect(ans2);
                *d2_index.offset(k as isize) = FindSubexprs(ans2, exprlist, tag);
                k += 1;
                unprotect(2 as libc::c_int);
                j += 1
            }
        }
        unprotect(4 as libc::c_int);
        i += 1
    }
    nexpr = length(exprlist) - 1 as libc::c_int;
    if f_index != 0 {
        Accumulate2(MakeVariable(f_index, tag), exprlist);
    } else {
        ans = duplicate(expr);
        protect(ans);
        Accumulate2(expr, exprlist);
        unprotect(1 as libc::c_int);
    }
    Accumulate2(R_NilValue, exprlist);
    if hessian != 0 {
        Accumulate2(R_NilValue, exprlist);
    }
    i = 0 as libc::c_int;
    k = 0 as libc::c_int;
    while i < nderiv {
        if *d_index.offset(i as isize) != 0 {
            Accumulate2(MakeVariable(*d_index.offset(i as isize), tag), exprlist);
            if hessian != 0 {
                ans = duplicate(expr);
                protect(ans);
                ans = D(ans, installTrChar(STRING_ELT(names, i as R_xlen_t)));
                protect(ans);
                j = i;
                while j < nderiv {
                    if *d2_index.offset(k as isize) != 0 {
                        Accumulate2(MakeVariable(*d2_index.offset(k as isize), tag), exprlist);
                    } else {
                        ans2 = duplicate(ans);
                        protect(ans2);
                        ans2 = D(ans2, installTrChar(STRING_ELT(names, j as R_xlen_t)));
                        protect(ans2);
                        Accumulate2(ans2, exprlist);
                        unprotect(2 as libc::c_int);
                    }
                    k += 1;
                    j += 1
                }
                unprotect(2 as libc::c_int);
            }
        } else {
            ans = duplicate(expr);
            protect(ans);
            ans = D(ans, installTrChar(STRING_ELT(names, i as R_xlen_t)));
            protect(ans);
            Accumulate2(ans, exprlist);
            unprotect(2 as libc::c_int);
            if hessian != 0 {
                j = i;
                while j < nderiv {
                    if *d2_index.offset(k as isize) != 0 {
                        Accumulate2(MakeVariable(*d2_index.offset(k as isize), tag), exprlist);
                    } else {
                        ans2 = duplicate(ans);
                        protect(ans2);
                        ans2 = D(ans2, installTrChar(STRING_ELT(names, j as R_xlen_t)));
                        protect(ans2);
                        if isZero(ans2) != 0 {
                            Accumulate2(R_MissingArg, exprlist);
                        } else {
                            Accumulate2(ans2, exprlist);
                        }
                        unprotect(2 as libc::c_int);
                    }
                    k += 1;
                    j += 1
                }
            }
        }
        i += 1
    }
    Accumulate2(R_NilValue, exprlist);
    Accumulate2(R_NilValue, exprlist);
    if hessian != 0 {
        Accumulate2(R_NilValue, exprlist);
    }
    i = 0 as libc::c_int;
    ans = (*exprlist).u.listsxp.cdrval;
    while i < nexpr {
        if CountOccurrences(
            MakeVariable(i + 1 as libc::c_int, tag),
            (*ans).u.listsxp.cdrval,
        ) < 2 as libc::c_int
        {
            SETCDR(
                ans,
                Replace(
                    MakeVariable(i + 1 as libc::c_int, tag),
                    (*ans).u.listsxp.carval,
                    (*ans).u.listsxp.cdrval,
                ),
            );
            SETCAR(ans, R_MissingArg);
        } else {
            let mut var: SEXP = 0 as *mut SEXPREC;
            var = MakeVariable(i + 1 as libc::c_int, tag);
            protect(var);
            SETCAR(
                ans,
                lang3(
                    install(b"<-\x00" as *const u8 as *const libc::c_char),
                    var,
                    AddParens((*ans).u.listsxp.carval),
                ),
            );
            unprotect(1 as libc::c_int);
        }
        i = i + 1 as libc::c_int;
        ans = (*ans).u.listsxp.cdrval
    }
    /* .value <- ... */
    SETCAR(
        ans,
        lang3(
            install(b"<-\x00" as *const u8 as *const libc::c_char),
            install(b".value\x00" as *const u8 as *const libc::c_char),
            AddParens((*ans).u.listsxp.carval),
        ),
    );
    ans = (*ans).u.listsxp.cdrval;
    /* .grad <- ... */
    SETCAR(ans, CreateGrad(names));
    ans = (*ans).u.listsxp.cdrval;
    /* .hessian <- ... */
    if hessian != 0 {
        SETCAR(ans, CreateHess(names));
        ans = (*ans).u.listsxp.cdrval
    }
    /* .grad[, "..."] <- ... */
    i = 0 as libc::c_int;
    while i < nderiv {
        SETCAR(
            ans,
            DerivAssign(
                STRING_ELT(names, i as R_xlen_t),
                AddParens((*ans).u.listsxp.carval),
            ),
        );
        ans = (*ans).u.listsxp.cdrval;
        if hessian != 0 {
            j = i;
            while j < nderiv {
                if (*ans).u.listsxp.carval != R_MissingArg {
                    if i == j {
                        SETCAR(
                            ans,
                            HessAssign1(
                                STRING_ELT(names, i as R_xlen_t),
                                AddParens((*ans).u.listsxp.carval),
                            ),
                        );
                    } else {
                        SETCAR(
                            ans,
                            HessAssign2(
                                STRING_ELT(names, i as R_xlen_t),
                                STRING_ELT(names, j as R_xlen_t),
                                AddParens((*ans).u.listsxp.carval),
                            ),
                        );
                    }
                }
                ans = (*ans).u.listsxp.cdrval;
                j += 1
            }
        }
        i += 1
    }
    /* attr(.value, "gradient") <- .grad */
    SETCAR(ans, AddGrad());
    ans = (*ans).u.listsxp.cdrval;
    if hessian != 0 {
        SETCAR(ans, AddHess());
        ans = (*ans).u.listsxp.cdrval
    }
    /* .value */
    SETCAR(
        ans,
        install(b".value\x00" as *const u8 as *const libc::c_char),
    );
    /* Prune the expression list removing eliminated sub-expressions */
    SETCDR(exprlist, Prune((*exprlist).u.listsxp.cdrval));
    if (*funarg).sxpinfo.type_0() as libc::c_int == 10 as libc::c_int
        && *(DATAPTR(funarg) as *mut libc::c_int).offset(0 as libc::c_int as isize) != 0
    {
        /* fun = TRUE */
        funarg = names
    }
    if (*funarg).sxpinfo.type_0() as libc::c_int == 3 as libc::c_int {
        s = allocSExp(3 as libc::c_int as SEXPTYPE);
        SET_FORMALS(s, (*funarg).u.closxp.formals);
        SET_CLOENV(s, (*funarg).u.closxp.env);
        funarg = s;
        SET_BODY(funarg, exprlist);
    } else if (*funarg).sxpinfo.type_0() as libc::c_int == 16 as libc::c_int {
        names = duplicate(funarg);
        protect(names);
        funarg = allocSExp(3 as libc::c_int as SEXPTYPE);
        protect(funarg);
        ans = allocList(length(names));
        protect(ans);
        SET_FORMALS(funarg, ans);
        i = 0 as libc::c_int;
        while i < length(names) {
            SET_TAG(ans, installTrChar(STRING_ELT(names, i as R_xlen_t)));
            SETCAR(ans, R_MissingArg);
            ans = (*ans).u.listsxp.cdrval;
            i += 1
        }
        unprotect(3 as libc::c_int);
        SET_BODY(funarg, exprlist);
        SET_CLOENV(funarg, R_GlobalEnv);
    } else {
        funarg = allocVector(20 as libc::c_int as SEXPTYPE, 1 as libc::c_int as R_xlen_t);
        SET_VECTOR_ELT(funarg, 0 as libc::c_int as R_xlen_t, exprlist);
        /* funarg = lang2(install("expression"), exprlist); */
    }
    unprotect(2 as libc::c_int);
    return funarg;
}
