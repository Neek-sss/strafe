use crate::sexprec::{SEXP, SEXPTYPE};
use ::libc;
extern "C" {
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    /* Hypergeometric Distibution */
    #[no_mangle]
    fn dhyper(
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_double,
        _: libc::c_int,
    ) -> libc::c_double;
    /* General Support Functions */
    #[no_mangle]
    fn imax2(_: libc::c_int, _: libc::c_int) -> libc::c_int;
    #[no_mangle]
    fn imin2(_: libc::c_int, _: libc::c_int) -> libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    /* Defining NO_RINLINEDFUNS disables use to simulate platforms where
    this is not available */
    /* need remapped names here for use with R_NO_REMAP */
    /*
       These are the inlinable functions that are provided in Rinlinedfuns.h
       It is *essential* that these do not appear in any other header file,
       with or without the Rf_ prefix.
    */
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
}
pub type size_t = libc::c_ulong;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type C2RustUnnamed = libc::c_uint;
pub const TRUE: C2RustUnnamed = 1;
pub const FALSE: C2RustUnnamed = 0;
pub type ptrdiff_t = libc::c_long;
/*, MAYBE */
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
/* nil = NULL */
/* symbols */
/* lists of dotted pairs */
/* closures */
/* environments */
/* promises: [un]evaluated closure arguments */
/* language constructs (special lists) */
/* special forms */
/* builtin non-special forms */
/* "scalar" string type (internal only)*/
/* logical vectors */
/* 11 and 12 were factors and ordered factors in the 1990s */
/* integer vectors */
/* real variables */
/* complex variables */
/* string vectors */
/* dot-dot-dot object */
/* make "any" args work.
Used in specifying types for symbol
registration to mean anything is okay  */
/* generic vectors */
/* expressions vectors */
/* byte code */
/* external pointer */
/* weak reference */
/* raw bytes */
/* S4, non-vector */
/* used for detecting PROTECT issues in memory.c */
/* fresh node created in new page */
/* node released by GC */
/* Closure or Builtin or Special */
/* NOT YET */
/* These are also used with the write barrier on, in attrib.c and util.c */
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000-2016  The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* for mantelhaen.test */
unsafe extern "C" fn int_d2x2xk(
    mut K: libc::c_int,
    mut m: *mut libc::c_double,
    mut n: *mut libc::c_double,
    mut t: *mut libc::c_double,
    mut d: *mut libc::c_double,
) {
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    let mut l: libc::c_int = 0;
    let mut w: libc::c_int = 0;
    let mut y: libc::c_int = 0;
    let mut z: libc::c_int = 0;
    let mut u: libc::c_double = 0.;
    let mut c: *mut *mut libc::c_double = 0 as *mut *mut libc::c_double;
    c = R_alloc(
        (K + 1 as libc::c_int) as size_t,
        ::std::mem::size_of::<*mut libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut *mut libc::c_double;
    z = 0 as libc::c_int;
    y = z;
    l = y;
    let ref mut fresh0 = *c.offset(0 as libc::c_int as isize);
    *fresh0 = R_alloc(
        1 as libc::c_int as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    *(*c.offset(0 as libc::c_int as isize)).offset(0 as libc::c_int as isize) =
        1 as libc::c_int as libc::c_double;
    i = 0 as libc::c_int;
    while i < K {
        y = imax2(0 as libc::c_int, (*t - *n) as libc::c_int);
        z = imin2(*m as libc::c_int, *t as libc::c_int);
        let ref mut fresh1 = *c.offset((i + 1 as libc::c_int) as isize);
        *fresh1 = R_alloc(
            (l + z - y + 1 as libc::c_int) as size_t,
            ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_double;
        j = 0 as libc::c_int;
        while j <= l + z - y {
            *(*c.offset((i + 1 as libc::c_int) as isize)).offset(j as isize) =
                0 as libc::c_int as libc::c_double;
            j += 1
        }
        j = 0 as libc::c_int;
        while j <= z - y {
            u = dhyper((j + y) as libc::c_double, *m, *n, *t, FALSE as libc::c_int);
            w = 0 as libc::c_int;
            while w <= l {
                *(*c.offset((i + 1 as libc::c_int) as isize)).offset((w + j) as isize) +=
                    *(*c.offset(i as isize)).offset(w as isize) * u;
                w += 1
            }
            j += 1
        }
        l = l + z - y;
        m = m.offset(1);
        n = n.offset(1);
        t = t.offset(1);
        i += 1
    }
    u = 0 as libc::c_int as libc::c_double;
    j = 0 as libc::c_int;
    while j <= l {
        u += *(*c.offset(K as isize)).offset(j as isize);
        j += 1
    }
    j = 0 as libc::c_int;
    while j <= l {
        *d.offset(j as isize) = *(*c.offset(K as isize)).offset(j as isize) / u;
        j += 1
    }
}
#[no_mangle]
pub unsafe extern "C" fn d2x2xk(
    mut sK: SEXP,
    mut m: SEXP,
    mut n: SEXP,
    mut t: SEXP,
    mut srn: SEXP,
) -> SEXP {
    let mut K: libc::c_int = asInteger(sK);
    let mut rn: libc::c_int = asInteger(srn);
    m = protect(coerceVector(m, 14 as libc::c_int as SEXPTYPE));
    n = protect(coerceVector(n, 14 as libc::c_int as SEXPTYPE));
    t = protect(coerceVector(t, 14 as libc::c_int as SEXPTYPE));
    let mut ans: SEXP = protect(allocVector(14 as libc::c_int as SEXPTYPE, rn as R_xlen_t));
    int_d2x2xk(K, REAL(m), REAL(n), REAL(t), REAL(ans));
    unprotect(4 as libc::c_int);
    return ans;
}
