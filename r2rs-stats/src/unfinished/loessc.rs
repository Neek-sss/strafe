use ::libc;
extern "C" {
    #[no_mangle]
    fn strncpy(_: *mut libc::c_char, _: *const libc::c_char, _: libc::c_ulong)
        -> *mut libc::c_char;
    #[no_mangle]
    fn strcat(_: *mut libc::c_char, _: *const libc::c_char) -> *mut libc::c_char;
    #[no_mangle]
    fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    #[no_mangle]
    fn snprintf(
        _: *mut libc::c_char,
        _: libc::c_ulong,
        _: *const libc::c_char,
        _: ...
    ) -> libc::c_int;

    #[no_mangle]
    fn R_chk_free(_: *mut libc::c_void);
    #[no_mangle]
    fn R_chk_calloc(_: size_t, _: size_t) -> *mut libc::c_void;
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    /* These (and many more) are in ./loessf.f : */
    #[no_mangle]
    fn lowesa_(
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn lowesb_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn lowesc_(
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn lowesd_(
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
    );
    #[no_mangle]
    fn lowese_(
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn lowesf_(
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn lowesl_(
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
    #[no_mangle]
    fn ehg169_(
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_int,
    );
    #[no_mangle]
    fn ehg196_(
        _: *mut libc::c_int,
        _: *mut libc::c_int,
        _: *mut libc::c_double,
        _: *mut libc::c_double,
    );
}
pub type size_t = libc::c_ulong;
static mut iv: *mut libc::c_int = 0 as *const libc::c_int as *mut libc::c_int;
static mut liv: libc::c_int = 0;
static mut lv: libc::c_int = 0;
static mut tau: libc::c_int = 0;
static mut v: *mut libc::c_double = 0 as *const libc::c_double as *mut libc::c_double;
/* these are set in an earlier call to loess_workspace or loess_grow */
unsafe extern "C" fn loess_free() {
    R_chk_free(v as *mut libc::c_void); /* = v(2) in Fortran (!) */
    v = 0 as *mut libc::c_double;
    R_chk_free(iv as *mut libc::c_void);
    iv = 0 as *mut libc::c_int;
}
#[no_mangle]
pub unsafe extern "C" fn loess_raw(
    mut y: *mut libc::c_double,
    mut x: *mut libc::c_double,
    mut weights: *mut libc::c_double,
    mut robust: *mut libc::c_double,
    mut d: *mut libc::c_int,
    mut n: *mut libc::c_int,
    mut span: *mut libc::c_double,
    mut degree: *mut libc::c_int,
    mut nonparametric: *mut libc::c_int,
    mut drop_square: *mut libc::c_int,
    mut sum_drop_sqr: *mut libc::c_int,
    mut cell: *mut libc::c_double,
    mut surf_stat: *mut *mut libc::c_char,
    mut surface: *mut libc::c_double,
    mut parameter: *mut libc::c_int,
    mut a: *mut libc::c_int,
    mut xi: *mut libc::c_double,
    mut vert: *mut libc::c_double,
    mut vval: *mut libc::c_double,
    mut diagonal: *mut libc::c_double,
    mut trL: *mut libc::c_double,
    mut one_delta: *mut libc::c_double,
    mut two_delta: *mut libc::c_double,
    mut setLf: *mut libc::c_int,
) {
    let mut zero: libc::c_int = 0 as libc::c_int;
    let mut one: libc::c_int = 1 as libc::c_int;
    let mut two: libc::c_int = 2 as libc::c_int;
    let mut nsing: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    let mut hat_matrix: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut LL: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut dzero: libc::c_double = 0.0f64;
    *trL = 0 as libc::c_int as libc::c_double;
    loess_workspace(
        d,
        n,
        span,
        degree,
        nonparametric,
        drop_square,
        sum_drop_sqr,
        setLf,
    );
    *v.offset(1 as libc::c_int as isize) = *cell;
    /* NB:  surf_stat  =  (surface / statistics);
     *                               statistics = "none" for all robustness iterations
     */
    if strcmp(
        *surf_stat,
        b"interpolate/none\x00" as *const u8 as *const libc::c_char,
    ) == 0
    {
        // default for loess.smooth() and robustness iter.
        lowesb_(
            x, y, robust, &mut dzero, &mut zero, iv, &mut liv, &mut lv, v,
        );
        lowese_(iv, &mut liv, &mut lv, v, n, x, surface);
        loess_prune(parameter, a, xi, vert, vval);
    } else if strcmp(
        *surf_stat,
        b"direct/none\x00" as *const u8 as *const libc::c_char,
    ) == 0
    {
        lowesf_(
            x, y, robust, iv, &mut liv, &mut lv, v, n, x, &mut dzero, &mut zero, surface,
        );
    } else if strcmp(
        *surf_stat,
        b"interpolate/1.approx\x00" as *const u8 as *const libc::c_char,
    ) == 0
    {
        // default (trace.hat is "exact")
        lowesb_(x, y, weights, diagonal, &mut one, iv, &mut liv, &mut lv, v);
        lowese_(iv, &mut liv, &mut lv, v, n, x, surface);
        nsing = *iv.offset(29 as libc::c_int as isize);
        i = 0 as libc::c_int;
        while i < *n {
            *trL = *trL + *diagonal.offset(i as isize);
            i += 1
        }
        lowesa_(trL, n, d, &mut tau, &mut nsing, one_delta, two_delta);
        loess_prune(parameter, a, xi, vert, vval);
    } else if strcmp(
        *surf_stat,
        b"interpolate/2.approx\x00" as *const u8 as *const libc::c_char,
    ) == 0
    {
        // default for trace.hat = "approximate"
        //                     vvvvvvv (had 'robust' in R <= 3.2.x)
        lowesb_(
            x, y, weights, &mut dzero, &mut zero, iv, &mut liv, &mut lv, v,
        );
        lowese_(iv, &mut liv, &mut lv, v, n, x, surface);
        nsing = *iv.offset(29 as libc::c_int as isize);
        ehg196_(&mut tau, d, span, trL);
        lowesa_(trL, n, d, &mut tau, &mut nsing, one_delta, two_delta);
        loess_prune(parameter, a, xi, vert, vval);
    } else if strcmp(
        *surf_stat,
        b"direct/approximate\x00" as *const u8 as *const libc::c_char,
    ) == 0
    {
        lowesf_(
            x, y, weights, iv, &mut liv, &mut lv, v, n, x, diagonal, &mut one, surface,
        );
        nsing = *iv.offset(29 as libc::c_int as isize);
        i = 0 as libc::c_int;
        while i < *n {
            *trL = *trL + *diagonal.offset(i as isize);
            i += 1
        }
        lowesa_(trL, n, d, &mut tau, &mut nsing, one_delta, two_delta);
    } else if strcmp(
        *surf_stat,
        b"interpolate/exact\x00" as *const u8 as *const libc::c_char,
    ) == 0
    {
        hat_matrix = R_alloc(
            (*n * *n) as size_t,
            ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_double;
        LL = R_alloc(
            (*n * *n) as size_t,
            ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_double;
        lowesb_(x, y, weights, diagonal, &mut one, iv, &mut liv, &mut lv, v);
        lowesl_(iv, &mut liv, &mut lv, v, n, x, hat_matrix);
        lowesc_(n, hat_matrix, LL, trL, one_delta, two_delta);
        lowese_(iv, &mut liv, &mut lv, v, n, x, surface);
        loess_prune(parameter, a, xi, vert, vval);
    } else if strcmp(
        *surf_stat,
        b"direct/exact\x00" as *const u8 as *const libc::c_char,
    ) == 0
    {
        hat_matrix = R_alloc(
            (*n * *n) as size_t,
            ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_double;
        LL = R_alloc(
            (*n * *n) as size_t,
            ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
        ) as *mut libc::c_double;
        lowesf_(
            x, y, weights, iv, &mut liv, &mut lv, v, n, x, hat_matrix, &mut two, surface,
        );
        lowesc_(n, hat_matrix, LL, trL, one_delta, two_delta);
        k = *n + 1 as libc::c_int;
        i = 0 as libc::c_int;
        while i < *n {
            *diagonal.offset(i as isize) = *hat_matrix.offset((i * k) as isize);
            i += 1
        }
    }
    loess_free();
}
#[no_mangle]
pub unsafe extern "C" fn loess_dfit(
    mut y: *mut libc::c_double,
    mut x: *mut libc::c_double,
    mut x_evaluate: *mut libc::c_double,
    mut weights: *mut libc::c_double,
    mut span: *mut libc::c_double,
    mut degree: *mut libc::c_int,
    mut nonparametric: *mut libc::c_int,
    mut drop_square: *mut libc::c_int,
    mut sum_drop_sqr: *mut libc::c_int,
    mut d: *mut libc::c_int,
    mut n: *mut libc::c_int,
    mut m: *mut libc::c_int,
    mut fit: *mut libc::c_double,
) {
    let mut zero: libc::c_int = 0 as libc::c_int;
    let mut dzero: libc::c_double = 0.0f64;
    loess_workspace(
        d,
        n,
        span,
        degree,
        nonparametric,
        drop_square,
        sum_drop_sqr,
        &mut zero,
    );
    lowesf_(
        x, y, weights, iv, &mut liv, &mut lv, v, m, x_evaluate, &mut dzero, &mut zero, fit,
    );
    loess_free();
}
#[no_mangle]
pub unsafe extern "C" fn loess_dfitse(
    mut y: *mut libc::c_double,
    mut x: *mut libc::c_double,
    mut x_evaluate: *mut libc::c_double,
    mut weights: *mut libc::c_double,
    mut robust: *mut libc::c_double,
    mut family: *mut libc::c_int,
    mut span: *mut libc::c_double,
    mut degree: *mut libc::c_int,
    mut nonparametric: *mut libc::c_int,
    mut drop_square: *mut libc::c_int,
    mut sum_drop_sqr: *mut libc::c_int,
    mut d: *mut libc::c_int,
    mut n: *mut libc::c_int,
    mut m: *mut libc::c_int,
    mut fit: *mut libc::c_double,
    mut L: *mut libc::c_double,
) {
    let mut zero: libc::c_int = 0 as libc::c_int;
    let mut two: libc::c_int = 2 as libc::c_int;
    let mut dzero: libc::c_double = 0.0f64;
    loess_workspace(
        d,
        n,
        span,
        degree,
        nonparametric,
        drop_square,
        sum_drop_sqr,
        &mut zero,
    );
    if *family == 1 as libc::c_int {
        lowesf_(
            x, y, weights, iv, &mut liv, &mut lv, v, m, x_evaluate, L, &mut two, fit,
        );
    } else if *family == 0 as libc::c_int {
        lowesf_(
            x, y, weights, iv, &mut liv, &mut lv, v, m, x_evaluate, L, &mut two, fit,
        );
        lowesf_(
            x, y, robust, iv, &mut liv, &mut lv, v, m, x_evaluate, &mut dzero, &mut zero, fit,
        );
    }
    loess_free();
}
#[no_mangle]
pub unsafe extern "C" fn loess_ifit(
    mut parameter: *mut libc::c_int,
    mut a: *mut libc::c_int,
    mut xi: *mut libc::c_double,
    mut vert: *mut libc::c_double,
    mut vval: *mut libc::c_double,
    mut m: *mut libc::c_int,
    mut x_evaluate: *mut libc::c_double,
    mut fit: *mut libc::c_double,
) {
    loess_grow(parameter, a, xi, vert, vval);
    lowese_(iv, &mut liv, &mut lv, v, m, x_evaluate, fit);
    loess_free();
}
#[no_mangle]
pub unsafe extern "C" fn loess_ise(
    mut y: *mut libc::c_double,
    mut x: *mut libc::c_double,
    mut x_evaluate: *mut libc::c_double,
    mut weights: *mut libc::c_double,
    mut span: *mut libc::c_double,
    mut degree: *mut libc::c_int,
    mut nonparametric: *mut libc::c_int,
    mut drop_square: *mut libc::c_int,
    mut sum_drop_sqr: *mut libc::c_int,
    mut cell: *mut libc::c_double,
    mut d: *mut libc::c_int,
    mut n: *mut libc::c_int,
    mut m: *mut libc::c_int,
    mut _fit: *mut libc::c_double,
    mut L: *mut libc::c_double,
) {
    let mut zero: libc::c_int = 0 as libc::c_int;
    let mut one: libc::c_int = 1 as libc::c_int;
    let mut dzero: libc::c_double = 0.0f64;
    loess_workspace(
        d,
        n,
        span,
        degree,
        nonparametric,
        drop_square,
        sum_drop_sqr,
        &mut one,
    );
    *v.offset(1 as libc::c_int as isize) = *cell;
    lowesb_(
        x, y, weights, &mut dzero, &mut zero, iv, &mut liv, &mut lv, v,
    );
    lowesl_(iv, &mut liv, &mut lv, v, m, x_evaluate, L);
    loess_free();
}
/*
 * The authors of this software are Cleveland, Grosse, and Shyu.
 * Copyright (c) 1989, 1992 by AT&T.
 * Permission to use, copy, modify, and distribute this software for any
 * purpose without fee is hereby granted, provided that this entire notice
 * is included in all copies of any software which is or includes a copy
 * or modification of this software and in all copies of the supporting
 * documentation for such software.
 * THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
 * WARRANTY.  IN PARTICULAR, NEITHER THE AUTHORS NOR AT&T MAKE ANY
 * REPRESENTATION OR WARRANTY OF ANY KIND CONCERNING THE MERCHANTABILITY
 * OF THIS SOFTWARE OR ITS FITNESS FOR ANY PARTICULAR PURPOSE.
 */
/* <UTF8> chars are handled as whole strings.
  They are passed from Fortran so had better be ASCII.
*/
/*
 *  Altered by B.D. Ripley to use F77_*, declare routines before use.
 *
 *  'protoize'd to ANSI C headers; indented: M.Maechler
 */
/* Forward declarations */
unsafe extern "C" fn loess_workspace(
    mut d: *mut libc::c_int,
    mut n: *mut libc::c_int,
    mut span: *mut libc::c_double,
    mut degree: *mut libc::c_int,
    mut nonparametric: *mut libc::c_int,
    mut drop_square: *mut libc::c_int,
    mut sum_drop_sqr: *mut libc::c_int,
    mut setLf: *mut libc::c_int,
) {
    let mut D: libc::c_int = *d;
    let mut N: libc::c_int = *n;
    let mut tau0: libc::c_int = 0;
    let mut nvmax: libc::c_int = 0;
    let mut nf: libc::c_int = 0;
    let mut version: libc::c_int = 106 as libc::c_int;
    let mut i: libc::c_int = 0;
    nvmax = if 200 as libc::c_int > N {
        200 as libc::c_int
    } else {
        N
    };
    nf = if N < (N as libc::c_double * *span + 1e-5f64).floor() as libc::c_int {
        N
    } else {
        (N as libc::c_double * *span + 1e-5f64).floor() as libc::c_int
    };
    if nf <= 0 as libc::c_int {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"span is too small\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    tau0 = if *degree > 1 as libc::c_int {
        (((D + 2 as libc::c_int) * (D + 1 as libc::c_int)) as libc::c_double * 0.5f64)
            as libc::c_int
    } else {
        (D) + 1 as libc::c_int
    };
    tau = tau0 - *sum_drop_sqr;
    lv = 50 as libc::c_int
        + (3 as libc::c_int * D + 3 as libc::c_int) * nvmax
        + N
        + (tau0 + 2 as libc::c_int) * nf;
    let mut dliv: libc::c_double = 50 as libc::c_int as libc::c_double
        + (2.0f64.powf(D as libc::c_double) + 4.0f64) * nvmax as libc::c_double
        + 2.0f64 * N as libc::c_double;
    if dliv < 2147483647 as libc::c_int as libc::c_double {
        liv = dliv as libc::c_int
    } else {
        error(b"workspace required is too large\x00" as *const u8 as *const libc::c_char);
    }
    if *setLf != 0 {
        lv = lv + (D + 1 as libc::c_int) * nf * nvmax;
        liv = liv + nf * nvmax
    }
    iv = R_chk_calloc(
        liv as size_t,
        ::std::mem::size_of::<libc::c_int>() as libc::c_ulong,
    ) as *mut libc::c_int;
    v = R_chk_calloc(
        lv as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    lowesd_(
        &mut version,
        iv,
        &mut liv,
        &mut lv,
        v,
        d,
        n,
        span,
        degree,
        &mut nvmax,
        setLf,
    );
    *iv.offset(32 as libc::c_int as isize) = *nonparametric;
    i = 0 as libc::c_int;
    while i < D {
        *iv.offset((i + 40 as libc::c_int) as isize) = *drop_square.offset(i as isize);
        i += 1
    }
}
unsafe extern "C" fn loess_prune(
    mut parameter: *mut libc::c_int,
    mut a: *mut libc::c_int,
    mut xi: *mut libc::c_double,
    mut vert: *mut libc::c_double,
    mut vval: *mut libc::c_double,
) {
    let mut d: libc::c_int = 0;
    let mut vc: libc::c_int = 0;
    let mut a1: libc::c_int = 0;
    let mut v1: libc::c_int = 0;
    let mut xi1: libc::c_int = 0;
    let mut vv1: libc::c_int = 0;
    let mut nc: libc::c_int = 0;
    let mut nv: libc::c_int = 0;
    let mut nvmax: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    d = *iv.offset(1 as libc::c_int as isize);
    vc = *iv.offset(3 as libc::c_int as isize) - 1 as libc::c_int;
    nc = *iv.offset(4 as libc::c_int as isize);
    nv = *iv.offset(5 as libc::c_int as isize);
    a1 = *iv.offset(6 as libc::c_int as isize) - 1 as libc::c_int;
    v1 = *iv.offset(10 as libc::c_int as isize) - 1 as libc::c_int;
    xi1 = *iv.offset(11 as libc::c_int as isize) - 1 as libc::c_int;
    vv1 = *iv.offset(12 as libc::c_int as isize) - 1 as libc::c_int;
    nvmax = *iv.offset(13 as libc::c_int as isize);
    i = 0 as libc::c_int;
    while i < 5 as libc::c_int {
        *parameter.offset(i as isize) = *iv.offset((i + 1 as libc::c_int) as isize);
        i += 1
    }
    *parameter.offset(5 as libc::c_int as isize) =
        *iv.offset(21 as libc::c_int as isize) - 1 as libc::c_int;
    *parameter.offset(6 as libc::c_int as isize) =
        *iv.offset(14 as libc::c_int as isize) - 1 as libc::c_int;
    i = 0 as libc::c_int;
    while i < d {
        k = nvmax * i;
        *vert.offset(i as isize) = *v.offset((v1 + k) as isize);
        *vert.offset((i + d) as isize) = *v.offset((v1 + vc + k) as isize);
        i += 1
    }
    i = 0 as libc::c_int;
    while i < nc {
        *xi.offset(i as isize) = *v.offset((xi1 + i) as isize);
        *a.offset(i as isize) = *iv.offset((a1 + i) as isize);
        i += 1
    }
    k = (d + 1 as libc::c_int) * nv;
    i = 0 as libc::c_int;
    while i < k {
        *vval.offset(i as isize) = *v.offset((vv1 + i) as isize);
        i += 1
    }
}
unsafe extern "C" fn loess_grow(
    mut parameter: *mut libc::c_int,
    mut a: *mut libc::c_int,
    mut xi: *mut libc::c_double,
    mut vert: *mut libc::c_double,
    mut vval: *mut libc::c_double,
) {
    let mut d: libc::c_int = 0;
    let mut vc: libc::c_int = 0;
    let mut nc: libc::c_int = 0;
    let mut nv: libc::c_int = 0;
    let mut a1: libc::c_int = 0;
    let mut v1: libc::c_int = 0;
    let mut xi1: libc::c_int = 0;
    let mut vv1: libc::c_int = 0;
    let mut i: libc::c_int = 0;
    let mut k: libc::c_int = 0;
    d = *parameter.offset(0 as libc::c_int as isize);
    vc = *parameter.offset(2 as libc::c_int as isize);
    nc = *parameter.offset(3 as libc::c_int as isize);
    nv = *parameter.offset(4 as libc::c_int as isize);
    liv = *parameter.offset(5 as libc::c_int as isize);
    lv = *parameter.offset(6 as libc::c_int as isize);
    iv = R_chk_calloc(
        liv as size_t,
        ::std::mem::size_of::<libc::c_int>() as libc::c_ulong,
    ) as *mut libc::c_int;
    v = R_chk_calloc(
        lv as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong,
    ) as *mut libc::c_double;
    *iv.offset(1 as libc::c_int as isize) = d;
    *iv.offset(2 as libc::c_int as isize) = *parameter.offset(1 as libc::c_int as isize);
    *iv.offset(3 as libc::c_int as isize) = vc;
    let ref mut fresh0 = *iv.offset(13 as libc::c_int as isize);
    *fresh0 = nv;
    *iv.offset(5 as libc::c_int as isize) = *fresh0;
    let ref mut fresh1 = *iv.offset(16 as libc::c_int as isize);
    *fresh1 = nc;
    *iv.offset(4 as libc::c_int as isize) = *fresh1;
    *iv.offset(6 as libc::c_int as isize) = 50 as libc::c_int;
    *iv.offset(7 as libc::c_int as isize) = *iv.offset(6 as libc::c_int as isize) + nc;
    *iv.offset(8 as libc::c_int as isize) = *iv.offset(7 as libc::c_int as isize) + vc * nc;
    *iv.offset(9 as libc::c_int as isize) = *iv.offset(8 as libc::c_int as isize) + nc;
    *iv.offset(10 as libc::c_int as isize) = 50 as libc::c_int;
    *iv.offset(12 as libc::c_int as isize) = *iv.offset(10 as libc::c_int as isize) + nv * d;
    *iv.offset(11 as libc::c_int as isize) =
        *iv.offset(12 as libc::c_int as isize) + (d + 1 as libc::c_int) * nv;
    *iv.offset(27 as libc::c_int as isize) = 173 as libc::c_int;
    v1 = *iv.offset(10 as libc::c_int as isize) - 1 as libc::c_int;
    xi1 = *iv.offset(11 as libc::c_int as isize) - 1 as libc::c_int;
    a1 = *iv.offset(6 as libc::c_int as isize) - 1 as libc::c_int;
    vv1 = *iv.offset(12 as libc::c_int as isize) - 1 as libc::c_int;
    i = 0 as libc::c_int;
    while i < d {
        k = nv * i;
        *v.offset((v1 + k) as isize) = *vert.offset(i as isize);
        *v.offset((v1 + vc - 1 as libc::c_int + k) as isize) = *vert.offset((i + d) as isize);
        i += 1
    }
    i = 0 as libc::c_int;
    while i < nc {
        *v.offset((xi1 + i) as isize) = *xi.offset(i as isize);
        *iv.offset((a1 + i) as isize) = *a.offset(i as isize);
        i += 1
    }
    k = (d + 1 as libc::c_int) * nv;
    i = 0 as libc::c_int;
    while i < k {
        *v.offset((vv1 + i) as isize) = *vval.offset(i as isize);
        i += 1
    }
    ehg169_(
        &mut d,
        &mut vc,
        &mut nc,
        &mut nc,
        &mut nv,
        &mut nv,
        v.offset(v1 as isize),
        iv.offset(a1 as isize),
        v.offset(xi1 as isize),
        iv.offset(*iv.offset(7 as libc::c_int as isize) as isize)
            .offset(-(1 as libc::c_int as isize)),
        iv.offset(*iv.offset(8 as libc::c_int as isize) as isize)
            .offset(-(1 as libc::c_int as isize)),
        iv.offset(*iv.offset(9 as libc::c_int as isize) as isize)
            .offset(-(1 as libc::c_int as isize)),
    );
}
/* exported (for loessf.f) : */
/* begin ehg's FORTRAN-callable C-codes */
#[no_mangle]
pub unsafe extern "C" fn ehg182_(mut i: *mut libc::c_int) {
    let mut msg: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut msg2: [libc::c_char; 50] = [0; 50];
    match *i {
        100 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"wrong version number in lowesd.   Probably typo in caller.\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        101 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"d>dMAX in ehg131.  Need to recompile with increased dimensions.\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        102 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"liv too small.    (Discovered by lowesd)\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        103 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"lv too small.     (Discovered by lowesd)\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        104 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"span too small.   fewer data values than degrees of freedom.\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        105 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"k>d2MAX in ehg136.  Need to recompile with increased dimensions.\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        106 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"lwork too small\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        107 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"invalid value for kernel\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        108 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"invalid value for ideg\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        109 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"lowstt only applies when kernel=1.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        110 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"not enough extra workspace for robustness calculation\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        120 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"zero-width neighborhood. make span bigger\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        121 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"all data on boundary of neighborhood. make span bigger\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        122 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"extrapolation not allowed with blending\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        123 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"ihat=1 (diag L) in l2fit only makes sense if z=x (eval=data).\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        171 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"lowesd must be called first.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        172 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"lowesf must not come between lowesb and lowese, lowesr, or lowesl.\x00"
                    as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        173 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"lowesb must come before lowese, lowesr, or lowesl.\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        174 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"lowesb need not be called twice.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        175 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"need setLf=.true. for lowesl.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        180 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"nv>nvmax in cpvert.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        181 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"nt>20 in eval.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        182 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"svddc failed in l2fit.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        183 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"didnt find edge in vleaf.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        184 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"zero-width cell found in vleaf.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        185 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"trouble descending to leaf in vleaf.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        186 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"insufficient workspace for lowesf.\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        187 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"insufficient stack space\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        188 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"lv too small for computing explicit L\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        191 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"computed trace L was negative; something is wrong!\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        192 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"computed delta was negative; something is wrong!\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        193 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"workspace in loread appears to be corrupted\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        194 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"trouble in l2fit/l2tr\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        195 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"only constant, linear, or quadratic local models allowed\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        196 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"degree must be at least 1 for vertex influence matrix\x00" as *const u8
                    as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        999 => {
            msg = dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"not yet implemented\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            )
        }
        _ => {
            snprintf(
                msg2.as_mut_ptr(),
                50 as libc::c_int as libc::c_ulong,
                b"Assert failed; error code %d\n\x00" as *const u8 as *const libc::c_char,
                *i,
            );
            msg = msg2.as_mut_ptr()
        }
    }
    warning(msg);
}
#[no_mangle]
pub unsafe extern "C" fn ehg183a_(
    mut s: *mut libc::c_char,
    mut nc: *mut libc::c_int,
    mut i: *mut libc::c_int,
    mut n: *mut libc::c_int,
    mut inc: *mut libc::c_int,
) {
    let mut nnc: libc::c_int = *nc;
    let mut mess: [libc::c_char; 4000] = [0; 4000];
    let mut num: [libc::c_char; 20] = [0; 20];
    strncpy(mess.as_mut_ptr(), s, nnc as libc::c_ulong);
    mess[nnc as usize] = '\u{0}' as i32 as libc::c_char;
    let mut j: libc::c_int = 0 as libc::c_int;
    while j < *n {
        snprintf(
            num.as_mut_ptr(),
            20 as libc::c_int as libc::c_ulong,
            b" %d\x00" as *const u8 as *const libc::c_char,
            *i.offset((j * *inc) as isize),
        );
        strcat(mess.as_mut_ptr(), num.as_mut_ptr());
        j += 1
    }
    strcat(
        mess.as_mut_ptr(),
        b"\n\x00" as *const u8 as *const libc::c_char,
    );
    warning(mess.as_mut_ptr());
}
#[no_mangle]
pub unsafe extern "C" fn ehg184a_(
    mut s: *mut libc::c_char,
    mut nc: *mut libc::c_int,
    mut x: *mut libc::c_double,
    mut n: *mut libc::c_int,
    mut inc: *mut libc::c_int,
) {
    let mut nnc: libc::c_int = *nc;
    let mut mess: [libc::c_char; 4000] = [0; 4000];
    let mut num: [libc::c_char; 30] = [0; 30];
    strncpy(mess.as_mut_ptr(), s, nnc as libc::c_ulong);
    mess[nnc as usize] = '\u{0}' as i32 as libc::c_char;
    let mut j: libc::c_int = 0 as libc::c_int;
    while j < *n {
        snprintf(
            num.as_mut_ptr(),
            30 as libc::c_int as libc::c_ulong,
            b" %.5g\x00" as *const u8 as *const libc::c_char,
            *x.offset((j * *inc) as isize),
        );
        strcat(mess.as_mut_ptr(), num.as_mut_ptr());
        j += 1
    }
    strcat(
        mess.as_mut_ptr(),
        b"\n\x00" as *const u8 as *const libc::c_char,
    );
    warning(mess.as_mut_ptr());
}
