use crate::sexprec::{SEXP, SEXPTYPE};
use ::libc;
extern "C" {
    #[no_mangle]
    fn memset(_: *mut libc::c_void, _: libc::c_int, _: libc::c_ulong) -> *mut libc::c_void;
    #[no_mangle]
    fn memcpy(_: *mut libc::c_void, _: *const libc::c_void, _: libc::c_ulong) -> *mut libc::c_void;
    #[no_mangle]
    fn LENGTH(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    /* Defining NO_RINLINEDFUNS disables use to simulate platforms where
    this is not available */
    /* need remapped names here for use with R_NO_REMAP */
    /*
       These are the inlinable functions that are provided in Rinlinedfuns.h
       It is *essential* that these do not appear in any other header file,
       with or without the Rf_ prefix.
    */
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn ScalarReal(_: libc::c_double) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
pub type R_xlen_t = ptrdiff_t;
/* Copyright (C) 1997-1999  Adrian Trapletti
   Copyright (C) 2012 The R Core Team

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, a copy is available at
   https://www.R-project.org/Licenses/
*/
unsafe extern "C" fn R_pp_sum(
    mut u: *mut libc::c_double,
    mut n: libc::c_int,
    mut l: libc::c_int,
) -> libc::c_double {
    let mut tmp1: libc::c_double = 0.;
    let mut tmp2: libc::c_double = 0.;
    tmp1 = 0.0f64;
    let mut i: libc::c_int = 1 as libc::c_int;
    while i <= l {
        tmp2 = 0.0f64;
        let mut j: libc::c_int = i;
        while j < n {
            tmp2 += *u.offset(j as isize) * *u.offset((j - i) as isize);
            j += 1
        }
        tmp2 *= 1.0f64 - i as libc::c_double / (l as libc::c_double + 1.0f64);
        tmp1 += tmp2;
        i += 1
    }
    return 2.0f64 * tmp1 / n as libc::c_double;
}
#[no_mangle]
pub unsafe extern "C" fn pp_sum(mut u: SEXP, mut sl: SEXP) -> SEXP {
    u = protect(coerceVector(u, 14 as libc::c_int as SEXPTYPE));
    let mut n: libc::c_int = LENGTH(u);
    let mut l: libc::c_int = asInteger(sl);
    let mut trm: libc::c_double = R_pp_sum(REAL(u), n, l);
    unprotect(1 as libc::c_int);
    return ScalarReal(trm);
}
#[no_mangle]
pub unsafe extern "C" fn intgrt_vec(mut x: SEXP, mut xi: SEXP, mut slag: SEXP) -> SEXP {
    x = protect(coerceVector(x, 14 as libc::c_int as SEXPTYPE));
    xi = protect(coerceVector(xi, 14 as libc::c_int as SEXPTYPE));
    let mut n: libc::c_int = LENGTH(x);
    let mut lag: libc::c_int = asInteger(slag);
    let mut ans: SEXP = protect(allocVector(
        14 as libc::c_int as SEXPTYPE,
        (n + lag) as R_xlen_t,
    ));
    let mut rx: *mut libc::c_double = REAL(x);
    let mut y: *mut libc::c_double = REAL(ans);
    memset(
        y as *mut libc::c_void,
        0 as libc::c_int,
        ((n + lag) as size_t)
            .wrapping_mul(::std::mem::size_of::<libc::c_double>() as libc::c_ulong),
    );
    memcpy(
        y as *mut libc::c_void,
        REAL(xi) as *const libc::c_void,
        (lag as size_t).wrapping_mul(::std::mem::size_of::<libc::c_double>() as libc::c_ulong),
    );
    let mut i: libc::c_int = lag;
    while i < lag + n {
        *y.offset(i as isize) = *rx.offset((i - lag) as isize) + *y.offset((i - lag) as isize);
        i += 1
    }
    unprotect(3 as libc::c_int);
    return ans;
}
