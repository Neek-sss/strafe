use crate::sexprec::{SEXP, SEXPTYPE};
use ::libc;
extern "C" {
    #[no_mangle]
    fn R_alloc(_: size_t, _: libc::c_int) -> *mut libc::c_char;
    /* Vector Access Functions */
    #[no_mangle]
    fn LENGTH(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn REAL(x: SEXP) -> *mut libc::c_double;
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    /* Defining NO_RINLINEDFUNS disables use to simulate platforms where
    this is not available */
    /* need remapped names here for use with R_NO_REMAP */
    /*
       These are the inlinable functions that are provided in Rinlinedfuns.h
       It is *essential* that these do not appear in any other header file,
       with or without the Rf_ prefix.
    */
    #[no_mangle]
    fn allocVector(_: SEXPTYPE, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn protect(_: SEXP) -> SEXP;
    #[no_mangle]
    fn unprotect(_: libc::c_int);
}
pub type size_t = libc::c_ulong;
pub type ptrdiff_t = libc::c_long;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
/* nil = NULL */
/* symbols */
/* lists of dotted pairs */
/* closures */
/* environments */
/* promises: [un]evaluated closure arguments */
/* language constructs (special lists) */
/* special forms */
/* builtin non-special forms */
/* "scalar" string type (internal only)*/
/* logical vectors */
/* 11 and 12 were factors and ordered factors in the 1990s */
/* integer vectors */
/* real variables */
/* complex variables */
/* string vectors */
/* dot-dot-dot object */
/* make "any" args work.
Used in specifying types for symbol
registration to mean anything is okay  */
/* generic vectors */
/* expressions vectors */
/* byte code */
/* external pointer */
/* weak reference */
/* raw bytes */
/* S4, non-vector */
/* used for detecting PROTECT issues in memory.c */
/* fresh node created in new page */
/* node released by GC */
/* Closure or Builtin or Special */
/* NOT YET */
/* These are also used with the write barrier on, in attrib.c and util.c */
/*
*  R : A Computer Language for Statistical Data Analysis

*  Copyright (C) 1999-2016  The R Core Team
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, a copy is available at
*  https://www.R-project.org/Licenses/.
*/
unsafe extern "C" fn burg(
    mut n: libc::c_int,
    mut x: *mut libc::c_double,
    mut pmax: libc::c_int,
    mut coefs: *mut libc::c_double,
    mut var1: *mut libc::c_double,
    mut var2: *mut libc::c_double,
) {
    let mut d: libc::c_double = 0.;
    let mut phii: libc::c_double = 0.;
    let mut u: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut v: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut u0: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut sum: libc::c_double = 0.;
    u = R_alloc(
        n as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    v = R_alloc(
        n as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    u0 = R_alloc(
        n as size_t,
        ::std::mem::size_of::<libc::c_double>() as libc::c_ulong as libc::c_int,
    ) as *mut libc::c_double;
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < pmax * pmax {
        *coefs.offset(i as isize) = 0.0f64;
        i += 1
    }
    sum = 0.0f64;
    let mut t: libc::c_int = 0 as libc::c_int;
    while t < n {
        let ref mut fresh0 = *v.offset(t as isize);
        *fresh0 = *x.offset((n - 1 as libc::c_int - t) as isize);
        *u.offset(t as isize) = *fresh0;
        sum += *x.offset(t as isize) * *x.offset(t as isize);
        t += 1
    }
    let ref mut fresh1 = *var2.offset(0 as libc::c_int as isize);
    *fresh1 = sum / n as libc::c_double;
    *var1.offset(0 as libc::c_int as isize) = *fresh1;
    let mut p: libc::c_int = 1 as libc::c_int;
    while p <= pmax {
        /* do AR(p) */
        sum = 0.0f64;
        d = 0 as libc::c_int as libc::c_double;
        let mut t_0: libc::c_int = p;
        while t_0 < n {
            sum += *v.offset(t_0 as isize) * *u.offset((t_0 - 1 as libc::c_int) as isize);
            d += *v.offset(t_0 as isize) * *v.offset(t_0 as isize)
                + *u.offset((t_0 - 1 as libc::c_int) as isize)
                    * *u.offset((t_0 - 1 as libc::c_int) as isize);
            t_0 += 1
        }
        phii = 2 as libc::c_int as libc::c_double * sum / d;
        *coefs.offset((pmax * (p - 1 as libc::c_int) + (p - 1 as libc::c_int)) as isize) = phii;
        if p > 1 as libc::c_int {
            let mut j: libc::c_int = 1 as libc::c_int;
            while j < p {
                *coefs.offset((p - 1 as libc::c_int + pmax * (j - 1 as libc::c_int)) as isize) =
                    *coefs.offset((p - 2 as libc::c_int + pmax * (j - 1 as libc::c_int)) as isize)
                        - phii
                            * *coefs.offset(
                                (p - 2 as libc::c_int + pmax * (p - j - 1 as libc::c_int)) as isize,
                            );
                j += 1
            }
        }
        /* update u and v */
        let mut t_1: libc::c_int = 0 as libc::c_int;
        while t_1 < n {
            *u0.offset(t_1 as isize) = *u.offset(t_1 as isize);
            t_1 += 1
        }
        let mut t_2: libc::c_int = p;
        while t_2 < n {
            *u.offset(t_2 as isize) =
                *u0.offset((t_2 - 1 as libc::c_int) as isize) - phii * *v.offset(t_2 as isize);
            *v.offset(t_2 as isize) =
                *v.offset(t_2 as isize) - phii * *u0.offset((t_2 - 1 as libc::c_int) as isize);
            t_2 += 1
        }
        *var1.offset(p as isize) = *var1.offset((p - 1 as libc::c_int) as isize)
            * (1 as libc::c_int as libc::c_double - phii * phii);
        d = 0.0f64;
        let mut t_3: libc::c_int = p;
        while t_3 < n {
            d += *v.offset(t_3 as isize) * *v.offset(t_3 as isize)
                + *u.offset(t_3 as isize) * *u.offset(t_3 as isize);
            t_3 += 1
        }
        *var2.offset(p as isize) = d / (2.0f64 * (n - p) as libc::c_double);
        p += 1
    }
}
#[no_mangle]
pub unsafe extern "C" fn Burg(mut x: SEXP, mut order: SEXP) -> SEXP {
    x = protect(coerceVector(x, 14 as libc::c_int as SEXPTYPE));
    let mut n: libc::c_int = LENGTH(x);
    let mut pmax: libc::c_int = asInteger(order);
    let mut coefs: SEXP = protect(allocVector(
        14 as libc::c_int as SEXPTYPE,
        (pmax * pmax) as R_xlen_t,
    ));
    let mut var1: SEXP = protect(allocVector(
        14 as libc::c_int as SEXPTYPE,
        (pmax + 1 as libc::c_int) as R_xlen_t,
    ));
    let mut var2: SEXP = protect(allocVector(
        14 as libc::c_int as SEXPTYPE,
        (pmax + 1 as libc::c_int) as R_xlen_t,
    ));
    burg(n, REAL(x), pmax, REAL(coefs), REAL(var1), REAL(var2));
    let mut ans: SEXP = protect(allocVector(
        19 as libc::c_int as SEXPTYPE,
        3 as libc::c_int as R_xlen_t,
    ));
    SET_VECTOR_ELT(ans, 0 as libc::c_int as R_xlen_t, coefs);
    SET_VECTOR_ELT(ans, 1 as libc::c_int as R_xlen_t, var1);
    SET_VECTOR_ELT(ans, 2 as libc::c_int as R_xlen_t, var2);
    unprotect(5 as libc::c_int);
    return ans;
}
