use crate::sexprec::{SEXP, SEXPREC, SEXPREC_ALIGN, SEXPTYPE, VECSEXP};
use ::libc;
extern "C" {
    pub type R_allocator;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    /* NA_REAL: IEEE */
    #[no_mangle]
    static mut R_NaInt: libc::c_int;
    #[no_mangle]
    fn ALTREP_LENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn ALTVEC_DATAPTR(x: SEXP) -> *mut libc::c_void;
    #[no_mangle]
    fn ALTSTRING_ELT(_: SEXP, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn R_BadLongVector(_: SEXP, _: *const libc::c_char, _: libc::c_int) -> !;
    #[no_mangle]
    static mut R_NilValue: SEXP;
    #[no_mangle]
    static mut R_DimSymbol: SEXP;
    #[no_mangle]
    static mut R_RowNamesSymbol: SEXP;
    #[no_mangle]
    static mut R_NaString: SEXP;
    #[no_mangle]
    fn allocVector3(_: SEXPTYPE, _: R_xlen_t, _: *mut R_allocator_t) -> SEXP;
    #[no_mangle]
    fn getAttrib(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn type2char(_: SEXPTYPE) -> *const libc::c_char;
    #[no_mangle]
    fn R_signal_protect_error() -> !;
    #[no_mangle]
    static mut R_PPStackSize: libc::c_int;
    #[no_mangle]
    static mut R_PPStackTop: libc::c_int;
    #[no_mangle]
    static mut R_PPStack: *mut SEXP;
    /* INLINE_PROTECT */
    /* from dstruct.c */
    /*  length - length of objects  */
    #[no_mangle]
    fn envlength(rho: SEXP) -> libc::c_int;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
}
#[derive(Copy, Clone)]
#[repr(C)]
pub struct Rcomplex {
    pub r: libc::c_double,
    pub i: libc::c_double,
}
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
pub type R_len_t = libc::c_int;
pub type R_xlen_t = ptrdiff_t;
pub type R_allocator_t = R_allocator;
/*, MAYBE */
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1999-2017  The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Internal header, not installed */
/* this header is always to be included from others.
  It is only called if COMPILING_R is defined (in util.c) or
  from GNU C systems.

  There are different conventions for inlining across compilation units.
  See http://www.greenend.org.uk/rjk/2003/03/inline.html
*/
/* Probably not able to use C99 semantics in gcc < 4.3.0 */
/* Apple's gcc build >5400 (since Xcode 3.0) doesn't support GNU inline in C99 mode */
/* This section is normally only used for versions of gcc which do not
   support C99 semantics.  __GNUC_STDC_INLINE__ is defined if
   GCC is following C99 inline semantics by default: we
   switch R's usage to the older GNU semantics via attributes.
   Do this even for __GNUC_GNUC_INLINE__ to shut up warnings in 4.2.x.
   __GNUC_STDC_INLINE__ and __GNUC_GNU_INLINE__ were added in gcc 4.2.0.
*/
/* ifdef COMPILING_R */
/* C99_INLINE_SEMANTICS */
/* for strlen, strcmp */
/* define inline-able functions */
#[inline]
unsafe extern "C" fn DATAPTR(mut x: SEXP) -> *mut libc::c_void {
    if (*x).sxpinfo.alt() != 0 {
        return ALTVEC_DATAPTR(x);
    } else {
        return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void;
    };
}
#[inline]
unsafe extern "C" fn XLENGTH_EX(mut x: SEXP) -> R_xlen_t {
    return if (*x).sxpinfo.alt() as libc::c_int != 0 {
        ALTREP_LENGTH(x)
    } else {
        (*(x as VECSEXP)).vecsxp.length
    };
}
#[inline]
unsafe extern "C" fn LENGTH_EX(
    mut x: SEXP,
    mut file: *const libc::c_char,
    mut line: libc::c_int,
) -> libc::c_int {
    if x == R_NilValue {
        return 0 as libc::c_int;
    }
    let mut len: R_xlen_t = XLENGTH_EX(x);
    if len > 2147483647 as libc::c_int as libc::c_long {
        R_BadLongVector(x, file, line);
    }
    return len as libc::c_int;
}
/* if not inlining use version in memory.c with more error checking */
#[inline]
unsafe extern "C" fn STRING_ELT(mut x: SEXP, mut i: R_xlen_t) -> SEXP {
    if (*x).sxpinfo.alt() != 0 {
        return ALTSTRING_ELT(x, i);
    } else {
        let mut ps: *mut SEXP = (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize)
            as *mut libc::c_void as *mut SEXP;
        return *ps.offset(i as isize);
    };
}
#[inline]
unsafe extern "C" fn protect(mut s: SEXP) -> SEXP {
    if R_PPStackTop < R_PPStackSize {
        let fresh0 = R_PPStackTop;
        R_PPStackTop = R_PPStackTop + 1;
        let ref mut fresh1 = *R_PPStack.offset(fresh0 as isize);
        *fresh1 = s
    } else {
        R_signal_protect_error();
    }
    return s;
}
#[inline]
unsafe extern "C" fn unprotect(mut l: libc::c_int) {
    R_PPStackTop -= l;
}
/* TODO: a  Length(.) {say} which is  length() + dispatch (S3 + S4) if needed
         for one approach, see do_seq_along() in ../main/seq.c
*/
#[inline]
unsafe extern "C" fn length(mut s: SEXP) -> R_len_t {
    match (*s).sxpinfo.type_0() as libc::c_int {
        0 => return 0 as libc::c_int,
        10 | 13 | 14 | 15 | 16 | 9 | 19 | 20 | 24 => {
            return LENGTH_EX(
                s,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                522 as libc::c_int,
            )
        }
        2 | 6 | 17 => {
            let mut i: libc::c_int = 0 as libc::c_int;
            while !s.is_null() && s != R_NilValue {
                i += 1;
                s = (*s).u.listsxp.cdrval
            }
            return i;
        }
        4 => return envlength(s),
        _ => return 1 as libc::c_int,
    };
}
/* regular allocVector() as a special case of allocVector3() with no custom allocator */
#[inline]
unsafe extern "C" fn allocVector(mut type_0: SEXPTYPE, mut length: R_xlen_t) -> SEXP {
    return allocVector3(type_0, length, 0 as *mut R_allocator_t);
}
#[inline]
unsafe extern "C" fn isList(mut s: SEXP) -> Rboolean {
    return (s == R_NilValue || (*s).sxpinfo.type_0() as libc::c_int == 2 as libc::c_int)
        as libc::c_int as Rboolean;
}
#[inline]
unsafe extern "C" fn isNewList(mut s: SEXP) -> Rboolean {
    return (s == R_NilValue || (*s).sxpinfo.type_0() as libc::c_int == 19 as libc::c_int)
        as libc::c_int as Rboolean;
}
#[inline]
unsafe extern "C" fn isVector(mut s: SEXP) -> Rboolean
/* === isVectorList() or isVectorAtomic() */ {
    match (*s).sxpinfo.type_0() as libc::c_int {
        10 | 13 | 14 | 15 | 16 | 24 | 19 | 20 => return TRUE,
        _ => return FALSE,
    };
}
#[inline]
unsafe extern "C" fn isMatrix(mut s: SEXP) -> Rboolean {
    let mut t: SEXP = 0 as *mut SEXPREC;
    if isVector(s) as u64 != 0 {
        t = getAttrib(s, R_DimSymbol);
        /* You are not supposed to be able to assign a non-integer dim,
        although this might be possible by misuse of ATTRIB. */
        if (*t).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
            && LENGTH_EX(
                t,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                902 as libc::c_int,
            ) == 2 as libc::c_int
        {
            return TRUE;
        }
    }
    return FALSE;
}
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1997-2013   The R Core Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Formerly in src/main/summary.c */
/* complete.cases(.) */
#[no_mangle]
pub unsafe extern "C" fn compcases(mut args: SEXP) -> SEXP {
    let mut current_block: u64;
    let mut s: SEXP = 0 as *mut SEXPREC;
    let mut t: SEXP = 0 as *mut SEXPREC;
    let mut u: SEXP = 0 as *mut SEXPREC;
    let mut rval: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut len: libc::c_int = 0;
    args = (*args).u.listsxp.cdrval;
    len = -(1 as libc::c_int);
    s = args;
    's_17: loop {
        if !(s != R_NilValue) {
            current_block = 1724319918354933278;
            break;
        }
        if isList((*s).u.listsxp.carval) as u64 != 0 {
            t = (*s).u.listsxp.carval;
            while t != R_NilValue {
                if isMatrix((*t).u.listsxp.carval) as u64 != 0 {
                    u = getAttrib((*t).u.listsxp.carval, R_DimSymbol);
                    if len < 0 as libc::c_int {
                        len = *(DATAPTR(u) as *mut libc::c_int).offset(0 as libc::c_int as isize)
                    } else if len
                        != *(DATAPTR(u) as *mut libc::c_int).offset(0 as libc::c_int as isize)
                    {
                        current_block = 6919955799065017118;
                        break 's_17;
                    }
                } else if isVector((*t).u.listsxp.carval) as u64 != 0 {
                    if len < 0 as libc::c_int {
                        len = LENGTH_EX(
                            (*t).u.listsxp.carval,
                            b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                            60 as libc::c_int,
                        )
                    } else if len
                        != LENGTH_EX(
                            (*t).u.listsxp.carval,
                            b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                            61 as libc::c_int,
                        )
                    {
                        current_block = 6919955799065017118;
                        break 's_17;
                    }
                } else {
                    error(
                        dcgettext(
                            b"stats\x00" as *const u8 as *const libc::c_char,
                            b"invalid \'type\' (%s) of argument\x00" as *const u8
                                as *const libc::c_char,
                            5 as libc::c_int,
                        ),
                        type2char((*(*t).u.listsxp.carval).sxpinfo.type_0()),
                    );
                }
                t = (*t).u.listsxp.cdrval
            }
        } else if isNewList((*s).u.listsxp.carval) as u64 != 0 {
            let mut it: libc::c_int = 0;
            let mut nt: libc::c_int = 0;
            t = (*s).u.listsxp.carval;
            nt = length(t);
            /* FIXME : Need to be careful with the use of isVector() */
            /* since this includes lists and expressions. */
            /* 0-column data frames are a special case */
            if nt != 0 {
                it = 0 as libc::c_int;
                while it < nt {
                    if isMatrix(*(DATAPTR(t) as *mut SEXP).offset(it as isize)) as u64 != 0 {
                        u = getAttrib(*(DATAPTR(t) as *mut SEXP).offset(it as isize), R_DimSymbol);
                        if len < 0 as libc::c_int {
                            len =
                                *(DATAPTR(u) as *mut libc::c_int).offset(0 as libc::c_int as isize)
                        } else if len
                            != *(DATAPTR(u) as *mut libc::c_int).offset(0 as libc::c_int as isize)
                        {
                            current_block = 6919955799065017118;
                            break 's_17;
                        }
                    } else if isVector(*(DATAPTR(t) as *mut SEXP).offset(it as isize)) as u64 != 0 {
                        if len < 0 as libc::c_int {
                            len = LENGTH_EX(
                                *(DATAPTR(t) as *mut SEXP).offset(it as isize),
                                b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                                85 as libc::c_int,
                            )
                        } else if len
                            != LENGTH_EX(
                                *(DATAPTR(t) as *mut SEXP).offset(it as isize),
                                b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                                86 as libc::c_int,
                            )
                        {
                            current_block = 6919955799065017118;
                            break 's_17;
                        }
                    } else {
                        error(
                            dcgettext(
                                b"stats\x00" as *const u8 as *const libc::c_char,
                                b"invalid \'type\' (%s) of argument\x00" as *const u8
                                    as *const libc::c_char,
                                5 as libc::c_int,
                            ),
                            b"unknown\x00" as *const u8 as *const libc::c_char,
                        );
                    }
                    it += 1
                }
            } else {
                u = getAttrib(t, R_RowNamesSymbol);
                if !((*u).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int) {
                    if len < 0 as libc::c_int {
                        len = LENGTH_EX(
                            u,
                            b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                            96 as libc::c_int,
                        )
                    } else if len
                        != *(DATAPTR(u) as *mut libc::c_int).offset(0 as libc::c_int as isize)
                    {
                        current_block = 6919955799065017118;
                        break;
                    }
                }
            }
        } else if isMatrix((*s).u.listsxp.carval) as u64 != 0 {
            u = getAttrib((*s).u.listsxp.carval, R_DimSymbol);
            if len < 0 as libc::c_int {
                len = *(DATAPTR(u) as *mut libc::c_int).offset(0 as libc::c_int as isize)
            } else if len != *(DATAPTR(u) as *mut libc::c_int).offset(0 as libc::c_int as isize) {
                current_block = 6919955799065017118;
                break;
            }
        } else if isVector((*s).u.listsxp.carval) as u64 != 0 {
            if len < 0 as libc::c_int {
                len = LENGTH_EX(
                    (*s).u.listsxp.carval,
                    b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                    111 as libc::c_int,
                )
            } else if len
                != LENGTH_EX(
                    (*s).u.listsxp.carval,
                    b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                    112 as libc::c_int,
                )
            {
                current_block = 6919955799065017118;
                break;
            }
        } else {
            error(
                dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"invalid \'type\' (%s) of argument\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ),
                type2char((*(*s).u.listsxp.carval).sxpinfo.type_0()),
            );
        }
        s = (*s).u.listsxp.cdrval
    }
    match current_block {
        6919955799065017118 => {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"not all arguments have the same length\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
        _ => {
            if len < 0 as libc::c_int {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"no input has determined the number of cases\x00" as *const u8
                        as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
            rval = allocVector(10 as libc::c_int as SEXPTYPE, len as R_xlen_t);
            protect(rval);
            i = 0 as libc::c_int;
            while i < len {
                *(DATAPTR(rval) as *mut libc::c_int).offset(i as isize) = 1 as libc::c_int;
                i += 1
            }
            /* FIXME : there is a lot of shared code here for vectors. */
            /* It should be abstracted out and optimized. */
            s = args;
            while s != R_NilValue {
                if isList((*s).u.listsxp.carval) as u64 != 0 {
                    /* Now we only need to worry about vectors */
                    /* since we use mod to handle arrays. */
                    /* FIXME : using mod like this causes */
                    /* a potential performance hit. */
                    t = (*s).u.listsxp.carval;
                    while t != R_NilValue {
                        u = (*t).u.listsxp.carval;
                        i = 0 as libc::c_int;
                        while i < LENGTH_EX(
                            u,
                            b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                            133 as libc::c_int,
                        ) {
                            match (*u).sxpinfo.type_0() as libc::c_int {
                                13 | 10 => {
                                    if *(DATAPTR(u) as *mut libc::c_int).offset(i as isize)
                                        == R_NaInt
                                    {
                                        *(DATAPTR(rval) as *mut libc::c_int)
                                            .offset((i % len) as isize) = 0 as libc::c_int
                                    }
                                }
                                14 => {
                                    if (*(DATAPTR(u) as *mut libc::c_double).offset(i as isize))
                                        .is_nan() as i32
                                        != 0 as libc::c_int
                                    {
                                        *(DATAPTR(rval) as *mut libc::c_int)
                                            .offset((i % len) as isize) = 0 as libc::c_int
                                    }
                                }
                                15 => {
                                    if (*(DATAPTR(u) as *mut Rcomplex).offset(i as isize))
                                        .r
                                        .is_nan() as i32
                                        != 0 as libc::c_int
                                        || (*(DATAPTR(u) as *mut Rcomplex).offset(i as isize))
                                            .i
                                            .is_nan()
                                            as i32
                                            != 0 as libc::c_int
                                    {
                                        *(DATAPTR(rval) as *mut libc::c_int)
                                            .offset((i % len) as isize) = 0 as libc::c_int
                                    }
                                }
                                16 => {
                                    if STRING_ELT(u, i as R_xlen_t) == R_NaString {
                                        *(DATAPTR(rval) as *mut libc::c_int)
                                            .offset((i % len) as isize) = 0 as libc::c_int
                                    }
                                }
                                _ => {
                                    unprotect(1 as libc::c_int);
                                    error(
                                        dcgettext(
                                            b"stats\x00" as *const u8 as *const libc::c_char,
                                            b"invalid \'type\' (%s) of argument\x00" as *const u8
                                                as *const libc::c_char,
                                            5 as libc::c_int,
                                        ),
                                        type2char((*u).sxpinfo.type_0()),
                                    );
                                }
                            }
                            i += 1
                        }
                        t = (*t).u.listsxp.cdrval
                    }
                }
                if isNewList((*s).u.listsxp.carval) as u64 != 0 {
                    let mut it_0: libc::c_int = 0;
                    let mut nt_0: libc::c_int = 0;
                    t = (*s).u.listsxp.carval;
                    nt_0 = length(t);
                    it_0 = 0 as libc::c_int;
                    while it_0 < nt_0 {
                        u = *(DATAPTR(t) as *mut SEXP).offset(it_0 as isize);
                        i = 0 as libc::c_int;
                        while i < LENGTH_EX(
                            u,
                            b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                            165 as libc::c_int,
                        ) {
                            match (*u).sxpinfo.type_0() as libc::c_int {
                                13 | 10 => {
                                    if *(DATAPTR(u) as *mut libc::c_int).offset(i as isize)
                                        == R_NaInt
                                    {
                                        *(DATAPTR(rval) as *mut libc::c_int)
                                            .offset((i % len) as isize) = 0 as libc::c_int
                                    }
                                }
                                14 => {
                                    if (*(DATAPTR(u) as *mut libc::c_double).offset(i as isize))
                                        .is_nan() as i32
                                        != 0 as libc::c_int
                                    {
                                        *(DATAPTR(rval) as *mut libc::c_int)
                                            .offset((i % len) as isize) = 0 as libc::c_int
                                    }
                                }
                                15 => {
                                    if (*(DATAPTR(u) as *mut Rcomplex).offset(i as isize))
                                        .r
                                        .is_nan() as i32
                                        != 0 as libc::c_int
                                        || (*(DATAPTR(u) as *mut Rcomplex).offset(i as isize))
                                            .i
                                            .is_nan()
                                            as i32
                                            != 0 as libc::c_int
                                    {
                                        *(DATAPTR(rval) as *mut libc::c_int)
                                            .offset((i % len) as isize) = 0 as libc::c_int
                                    }
                                }
                                16 => {
                                    if STRING_ELT(u, i as R_xlen_t) == R_NaString {
                                        *(DATAPTR(rval) as *mut libc::c_int)
                                            .offset((i % len) as isize) = 0 as libc::c_int
                                    }
                                }
                                _ => {
                                    unprotect(1 as libc::c_int);
                                    error(
                                        dcgettext(
                                            b"stats\x00" as *const u8 as *const libc::c_char,
                                            b"invalid \'type\' (%s) of argument\x00" as *const u8
                                                as *const libc::c_char,
                                            5 as libc::c_int,
                                        ),
                                        type2char((*u).sxpinfo.type_0()),
                                    );
                                }
                            }
                            i += 1
                        }
                        it_0 += 1
                    }
                } else {
                    i = 0 as libc::c_int;
                    while i < LENGTH_EX(
                        (*s).u.listsxp.carval,
                        b"complete_cases.c\x00" as *const u8 as *const libc::c_char,
                        192 as libc::c_int,
                    ) {
                        u = (*s).u.listsxp.carval;
                        match (*u).sxpinfo.type_0() as libc::c_int {
                            13 | 10 => {
                                if *(DATAPTR(u) as *mut libc::c_int).offset(i as isize) == R_NaInt {
                                    *(DATAPTR(rval) as *mut libc::c_int)
                                        .offset((i % len) as isize) = 0 as libc::c_int
                                }
                            }
                            14 => {
                                if (*(DATAPTR(u) as *mut libc::c_double).offset(i as isize))
                                    .is_nan() as i32
                                    != 0 as libc::c_int
                                {
                                    *(DATAPTR(rval) as *mut libc::c_int)
                                        .offset((i % len) as isize) = 0 as libc::c_int
                                }
                            }
                            15 => {
                                if (*(DATAPTR(u) as *mut Rcomplex).offset(i as isize))
                                    .r
                                    .is_nan() as i32
                                    != 0 as libc::c_int
                                    || (*(DATAPTR(u) as *mut Rcomplex).offset(i as isize))
                                        .i
                                        .is_nan() as i32
                                        != 0 as libc::c_int
                                {
                                    *(DATAPTR(rval) as *mut libc::c_int)
                                        .offset((i % len) as isize) = 0 as libc::c_int
                                }
                            }
                            16 => {
                                if STRING_ELT(u, i as R_xlen_t) == R_NaString {
                                    *(DATAPTR(rval) as *mut libc::c_int)
                                        .offset((i % len) as isize) = 0 as libc::c_int
                                }
                            }
                            _ => {
                                unprotect(1 as libc::c_int);
                                error(
                                    dcgettext(
                                        b"stats\x00" as *const u8 as *const libc::c_char,
                                        b"invalid \'type\' (%s) of argument\x00" as *const u8
                                            as *const libc::c_char,
                                        5 as libc::c_int,
                                    ),
                                    type2char((*u).sxpinfo.type_0()),
                                );
                            }
                        }
                        i += 1
                    }
                }
                s = (*s).u.listsxp.cdrval
            }
            unprotect(1 as libc::c_int);
            return rval;
        }
    };
    /* -Wall */
}
