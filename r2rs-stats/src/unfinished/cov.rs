use crate::sexprec::{SEXP, SEXPREC, SEXPREC_ALIGN, SEXPTYPE, VECSEXP};
use ::libc;
use ::num_traits::{ToPrimitive, Zero};
use nonstdfloat::f128;
extern "C" {
    pub type R_allocator;
    #[no_mangle]
    fn SET_VECTOR_ELT(x: SEXP, i: R_xlen_t, v: SEXP) -> SEXP;
    #[no_mangle]
    fn ALTREP_LENGTH(x: SEXP) -> R_xlen_t;
    #[no_mangle]
    fn ALTVEC_DATAPTR(x: SEXP) -> *mut libc::c_void;
    #[no_mangle]
    fn sqrt(_: libc::c_double) -> libc::c_double;
    #[no_mangle]
    fn sqrtl(_: f128) -> f128;
    /* IEEE -Inf */
    #[no_mangle]
    static mut R_NaReal: libc::c_double;
    /*
     *  R : A Computer Language for Statistical Data Analysis
     *  Copyright (C) 1998-2005   The R Core Team
     *
     *  This header file is free software; you can redistribute it and/or modify
     *  it under the terms of the GNU Lesser General Public License as published by
     *  the Free Software Foundation; either version 2.1 of the License, or
     *  (at your option) any later version.
     *
     *  This file is part of R. R is distributed under the terms of the
     *  GNU General Public License, either Version 2, June 1991 or Version 3,
     *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU Lesser General Public License for more details.
     *
     *  You should have received a copy of the GNU Lesser General Public License
     *  along with this program; if not, a copy is available at
     *  https://www.R-project.org/Licenses/
     */
    /* Included by R.h: API */
    #[no_mangle]
    fn error(_: *const libc::c_char, _: ...) -> !;
    #[no_mangle]
    fn warning(_: *const libc::c_char, _: ...);
    #[no_mangle]
    fn ALTSTRING_ELT(_: SEXP, _: R_xlen_t) -> SEXP;
    #[no_mangle]
    fn R_BadLongVector(_: SEXP, _: *const libc::c_char, _: libc::c_int) -> !;
    #[no_mangle]
    static mut R_NilValue: SEXP;
    #[no_mangle]
    static mut R_ClassSymbol: SEXP;
    #[no_mangle]
    static mut R_DimNamesSymbol: SEXP;
    #[no_mangle]
    static mut R_DimSymbol: SEXP;
    #[no_mangle]
    fn coerceVector(_: SEXP, _: SEXPTYPE) -> SEXP;
    #[no_mangle]
    fn asLogical(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn asInteger(x: SEXP) -> libc::c_int;
    #[no_mangle]
    fn allocMatrix(_: SEXPTYPE, _: libc::c_int, _: libc::c_int) -> SEXP;
    #[no_mangle]
    fn allocVector3(_: SEXPTYPE, _: R_xlen_t, _: *mut R_allocator_t) -> SEXP;
    #[no_mangle]
    fn duplicate(_: SEXP) -> SEXP;
    #[no_mangle]
    fn getAttrib(_: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn ncols(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn nrows(_: SEXP) -> libc::c_int;
    #[no_mangle]
    fn setAttrib(_: SEXP, _: SEXP, _: SEXP) -> SEXP;
    #[no_mangle]
    fn R_signal_protect_error() -> !;
    #[no_mangle]
    fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    #[no_mangle]
    static mut R_PPStackSize: libc::c_int;
    #[no_mangle]
    static mut R_PPStackTop: libc::c_int;
    #[no_mangle]
    static mut R_PPStack: *mut SEXP;
    /* INLINE_PROTECT */
    /* from dstruct.c */
    /*  length - length of objects  */
    #[no_mangle]
    fn envlength(rho: SEXP) -> libc::c_int;
    #[no_mangle]
    fn dcgettext(
        __domainname: *const libc::c_char,
        __msgid: *const libc::c_char,
        __category: libc::c_int,
    ) -> *mut libc::c_char;
    #[no_mangle]
    fn sign(_: libc::c_double) -> libc::c_double;
}
pub type ptrdiff_t = libc::c_long;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2000, 2001 The R Core Team.
 *
 *  This header file is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2.1 of the License, or
 *  (at your option) any later version.
 *
 *  This file is part of R. R is distributed under the terms of the
 *  GNU General Public License, either Version 2, June 1991 or Version 3,
 *  June 2007. See doc/COPYRIGHTS for details of the copyright status of R.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Included by R.h: API */
pub type Rboolean = libc::c_uint;
pub const TRUE: Rboolean = 1;
pub const FALSE: Rboolean = 0;
/*, MAYBE */
/* type for length of (standard, not long) vectors etc */
pub type R_len_t = libc::c_int;
/* both config.h and Rconfig.h set SIZEOF_SIZE_T, but Rconfig.h is
skipped if config.h has already been included. */
pub type R_xlen_t = ptrdiff_t;
/* Fundamental Data Types:  These are largely Lisp
 * influenced structures, with the exception of LGLSXP,
 * INTSXP, REALSXP, CPLXSXP and STRSXP which are the
 * element types for S-like data objects.
 *
 *   --> TypeTable[] in ../main/util.c for  typeof()
 */
/* UUID identifying the internals version -- packages using compiled
code should be re-installed when this changes */
/*  These exact numeric values are seldom used, but they are, e.g., in
 *  ../main/subassign.c, and they are serialized.
*/
/* NOT YET using enum:
 *  1) The SEXPREC struct below has 'SEXPTYPE type : 5'
 * (making FUNSXP and CLOSXP equivalent in there),
 * giving (-Wall only ?) warnings all over the place
 * 2) Many switch(type) { case ... } statements need a final `default:'
 * added in order to avoid warnings like [e.g. l.170 of ../main/util.c]
 *   "enumeration value `FUNSXP' not handled in switch"
 */
pub type R_allocator_t = R_allocator;
/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1999-2017  The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */
/* Internal header, not installed */
/* this header is always to be included from others.
  It is only called if COMPILING_R is defined (in util.c) or
  from GNU C systems.

  There are different conventions for inlining across compilation units.
  See http://www.greenend.org.uk/rjk/2003/03/inline.html
*/
/* Probably not able to use C99 semantics in gcc < 4.3.0 */
/* Apple's gcc build >5400 (since Xcode 3.0) doesn't support GNU inline in C99 mode */
/* This section is normally only used for versions of gcc which do not
   support C99 semantics.  __GNUC_STDC_INLINE__ is defined if
   GCC is following C99 inline semantics by default: we
   switch R's usage to the older GNU semantics via attributes.
   Do this even for __GNUC_GNUC_INLINE__ to shut up warnings in 4.2.x.
   __GNUC_STDC_INLINE__ and __GNUC_GNU_INLINE__ were added in gcc 4.2.0.
*/
/* ifdef COMPILING_R */
/* C99_INLINE_SEMANTICS */
/* for strlen, strcmp */
/* define inline-able functions */
#[inline]
unsafe extern "C" fn DATAPTR(mut x: SEXP) -> *mut libc::c_void {
    if (*x).sxpinfo.alt() != 0 {
        return ALTVEC_DATAPTR(x);
    } else {
        return (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize) as *mut libc::c_void;
    };
}
#[inline]
unsafe extern "C" fn XLENGTH_EX(mut x: SEXP) -> R_xlen_t {
    return if (*x).sxpinfo.alt() as libc::c_int != 0 {
        ALTREP_LENGTH(x)
    } else {
        (*(x as VECSEXP)).vecsxp.length
    };
}
#[inline]
unsafe extern "C" fn LENGTH_EX(
    mut x: SEXP,
    mut file: *const libc::c_char,
    mut line: libc::c_int,
) -> libc::c_int {
    if x == R_NilValue {
        return 0 as libc::c_int;
    }
    let mut len: R_xlen_t = XLENGTH_EX(x);
    if len > 2147483647 as libc::c_int as libc::c_long {
        R_BadLongVector(x, file, line);
    }
    return len as libc::c_int;
}
/* if not inlining use version in memory.c with more error checking */
#[inline]
unsafe extern "C" fn STRING_ELT(mut x: SEXP, mut i: R_xlen_t) -> SEXP {
    if (*x).sxpinfo.alt() != 0 {
        return ALTSTRING_ELT(x, i);
    } else {
        let mut ps: *mut SEXP = (x as *mut SEXPREC_ALIGN).offset(1 as libc::c_int as isize)
            as *mut libc::c_void as *mut SEXP;
        return *ps.offset(i as isize);
    };
}
#[inline]
unsafe extern "C" fn protect(mut s: SEXP) -> SEXP {
    if R_PPStackTop < R_PPStackSize {
        let fresh0 = R_PPStackTop;
        R_PPStackTop = R_PPStackTop + 1;
        let ref mut fresh1 = *R_PPStack.offset(fresh0 as isize);
        *fresh1 = s
    } else {
        R_signal_protect_error();
    }
    return s;
}
#[inline]
unsafe extern "C" fn unprotect(mut l: libc::c_int) {
    R_PPStackTop -= l;
}
/* TODO: a  Length(.) {say} which is  length() + dispatch (S3 + S4) if needed
         for one approach, see do_seq_along() in ../main/seq.c
*/
#[inline]
unsafe extern "C" fn length(mut s: SEXP) -> R_len_t {
    match (*s).sxpinfo.type_0() as libc::c_int {
        0 => return 0 as libc::c_int,
        10 | 13 | 14 | 15 | 16 | 9 | 19 | 20 | 24 => {
            return LENGTH_EX(
                s,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                522 as libc::c_int,
            )
        }
        2 | 6 | 17 => {
            let mut i: libc::c_int = 0 as libc::c_int;
            while !s.is_null() && s != R_NilValue {
                i += 1;
                s = (*s).u.listsxp.cdrval
            }
            return i;
        }
        4 => return envlength(s),
        _ => return 1 as libc::c_int,
    };
}
/* regular allocVector() as a special case of allocVector3() with no custom allocator */
#[inline]
unsafe extern "C" fn allocVector(mut type_0: SEXPTYPE, mut length: R_xlen_t) -> SEXP {
    return allocVector3(type_0, length, 0 as *mut R_allocator_t);
}
/* NOTE: R's inherits() is based on inherits3() in ../main/objects.c
 * Here, use char / CHAR() instead of the slower more general translateChar()
 */
#[inline]
unsafe extern "C" fn inherits(mut s: SEXP, mut name: *const libc::c_char) -> Rboolean {
    let mut klass: SEXP = 0 as *mut SEXPREC;
    let mut i: libc::c_int = 0;
    let mut nclass: libc::c_int = 0;
    if (*s).sxpinfo.obj() != 0 {
        klass = getAttrib(s, R_ClassSymbol);
        nclass = length(klass);
        i = 0 as libc::c_int;
        while i < nclass {
            if strcmp(
                (STRING_ELT(klass, i as R_xlen_t) as *mut SEXPREC_ALIGN)
                    .offset(1 as libc::c_int as isize) as *mut libc::c_void
                    as *const libc::c_char,
                name,
            ) == 0
            {
                return TRUE;
            }
            i += 1
        }
    }
    return FALSE;
}
#[inline]
unsafe extern "C" fn isVector(mut s: SEXP) -> Rboolean
/* === isVectorList() or isVectorAtomic() */ {
    match (*s).sxpinfo.type_0() as libc::c_int {
        10 | 13 | 14 | 15 | 16 | 24 | 19 | 20 => return TRUE,
        _ => return FALSE,
    };
}
#[inline]
unsafe extern "C" fn isMatrix(mut s: SEXP) -> Rboolean {
    let mut t: SEXP = 0 as *mut SEXPREC;
    if isVector(s) as u64 != 0 {
        t = getAttrib(s, R_DimSymbol);
        /* You are not supposed to be able to assign a non-integer dim,
        although this might be possible by misuse of ATTRIB. */
        if (*t).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
            && LENGTH_EX(
                t,
                b"../../../include/Rinlinedfuns.h\x00" as *const u8 as *const libc::c_char,
                902 as libc::c_int,
            ) == 2 as libc::c_int
        {
            return TRUE;
        }
    }
    return FALSE;
}
#[inline]
unsafe extern "C" fn isFactor(mut s: SEXP) -> Rboolean {
    return ((*s).sxpinfo.type_0() as libc::c_int == 13 as libc::c_int
        && inherits(s, b"factor\x00" as *const u8 as *const libc::c_char) as libc::c_uint != 0)
        as libc::c_int as Rboolean;
}
#[no_mangle]
pub unsafe extern "C" fn cor(
    mut x: SEXP,
    mut y: SEXP,
    mut na_method: SEXP,
    mut kendall: SEXP,
) -> SEXP {
    return corcov(x, y, na_method, kendall, TRUE);
}
#[no_mangle]
pub unsafe extern "C" fn cov(
    mut x: SEXP,
    mut y: SEXP,
    mut na_method: SEXP,
    mut kendall: SEXP,
) -> SEXP {
    return corcov(x, y, na_method, kendall, FALSE);
}
/* Note that "if (kendall)" and  "if (cor)" are used inside a double for() loop;
   which makes the code better readable -- and is hopefully dealt with
   by a smartly optimizing compiler
*/
/* * Compute   Cov(xx[], yy[])  or  Cor(.,.)  with n = length(xx)
 */
/* -Wall initializing */
/*kendall*/
/* Kendall's tau */
unsafe extern "C" fn cov_pairwise1(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut x: *mut libc::c_double,
    mut ans: *mut libc::c_double,
    mut sd_0: *mut Rboolean,
    mut cor_0: Rboolean,
    mut kendall: Rboolean,
) {
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < ncx {
        let mut xx: *mut libc::c_double = &mut *x.offset((i * n) as isize) as *mut libc::c_double;
        let mut j: libc::c_int = 0 as libc::c_int;
        while j <= i {
            let mut yy: *mut libc::c_double =
                &mut *x.offset((j * n) as isize) as *mut libc::c_double;
            let mut sum: f128 = f128::zero();
            let mut xmean: f128 = f128::new(0.0f64);
            let mut ymean: f128 = f128::new(0.0f64);
            let mut xsd: f128 = f128::zero();
            let mut ysd: f128 = f128::zero();
            let mut xm: f128 = f128::zero();
            let mut ym: f128 = f128::zero();
            let mut k: libc::c_int = 0;
            let mut nobs: libc::c_int = 0;
            let mut n1: libc::c_int = -(1 as libc::c_int);
            nobs = 0 as libc::c_int;
            if kendall as u64 == 0 {
                ymean = f128::new(0.0f64);
                xmean = ymean;
                k = 0 as libc::c_int;
                while k < n {
                    if !((*xx.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int
                        || (*yy.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int)
                    {
                        nobs += 1;
                        xmean += f128::new(*xx.offset(k as isize));
                        ymean += f128::new(*yy.offset(k as isize))
                    }
                    k += 1
                }
            } else {
                k = 0 as libc::c_int;
                while k < n {
                    if !((*xx.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int
                        || (*yy.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int)
                    {
                        nobs += 1
                    }
                    k += 1
                }
            }
            if nobs >= 2 as libc::c_int {
                sum = f128::new(0.0f64);
                ysd = sum;
                xsd = ysd;
                if kendall as u64 == 0 {
                    xmean /= f128::new(nobs);
                    ymean /= f128::new(nobs);
                    n1 = nobs - 1 as libc::c_int
                }
                k = 0 as libc::c_int;
                while k < n {
                    if !((*xx.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int
                        || (*yy.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int)
                    {
                        if kendall as u64 == 0 {
                            xm = f128::new(*xx.offset(k as isize)) - xmean;
                            ym = f128::new(*yy.offset(k as isize)) - ymean;
                            sum += xm * ym;
                            if cor_0 as u64 != 0 {
                                xsd += xm * xm;
                                ysd += ym * ym
                            }
                        } else {
                            n1 = 0 as libc::c_int;
                            while n1 < k {
                                if !((*xx.offset(n1 as isize)).is_nan() as i32 != 0 as libc::c_int
                                    || (*yy.offset(n1 as isize)).is_nan() as i32
                                        != 0 as libc::c_int)
                                {
                                    xm = f128::new(sign(
                                        *xx.offset(k as isize) - *xx.offset(n1 as isize),
                                    ));
                                    ym = f128::new(sign(
                                        *yy.offset(k as isize) - *yy.offset(n1 as isize),
                                    ));
                                    sum += xm * ym;
                                    if cor_0 as u64 != 0 {
                                        xsd += xm * xm;
                                        ysd += ym * ym
                                    }
                                }
                                n1 += 1
                            }
                        }
                    }
                    k += 1
                }
                if cor_0 as u64 != 0 {
                    if xsd == f128::new(0.0f64) || ysd == f128::new(0.0f64) {
                        *sd_0 = TRUE;
                        sum = f128::new(R_NaReal)
                    } else {
                        if kendall as u64 == 0 {
                            xsd /= f128::new(n1);
                            ysd /= f128::new(n1);
                            sum /= f128::new(n1)
                        }
                        sum /= sqrtl(xsd) * sqrtl(ysd);
                        sum = if sum >= f128::new(1.0f64) {
                            f128::new(1.0f64)
                        } else if sum <= f128::new(-1.0f64) {
                            f128::new(-1.0f64)
                        } else {
                            sum
                        }
                    }
                } else if kendall as u64 == 0 {
                    sum /= f128::new(n1)
                }
                *ans.offset((i + j * ncx) as isize) = tof64!(sum)
            } else {
                *ans.offset((i + j * ncx) as isize) = R_NaReal
            }
            *ans.offset((j + i * ncx) as isize) = *ans.offset((i + j * ncx) as isize);
            j += 1
        }
        i += 1
    }
}
unsafe extern "C" fn cov_pairwise2(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut ncy: libc::c_int,
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut ans: *mut libc::c_double,
    mut sd_0: *mut Rboolean,
    mut cor_0: Rboolean,
    mut kendall: Rboolean,
) {
    let mut i: libc::c_int = 0 as libc::c_int;
    while i < ncx {
        let mut xx: *mut libc::c_double = &mut *x.offset((i * n) as isize) as *mut libc::c_double;
        let mut j: libc::c_int = 0 as libc::c_int;
        while j < ncy {
            let mut yy: *mut libc::c_double =
                &mut *y.offset((j * n) as isize) as *mut libc::c_double;
            let mut sum: f128 = f128::zero();
            let mut xmean: f128 = f128::new(0.0f64);
            let mut ymean: f128 = f128::new(0.0f64);
            let mut xsd: f128 = f128::zero();
            let mut ysd: f128 = f128::zero();
            let mut xm: f128 = f128::zero();
            let mut ym: f128 = f128::zero();
            let mut k: libc::c_int = 0;
            let mut nobs: libc::c_int = 0;
            let mut n1: libc::c_int = -(1 as libc::c_int);
            nobs = 0 as libc::c_int;
            if kendall as u64 == 0 {
                ymean = f128::new(0.0f64);
                xmean = ymean;
                k = 0 as libc::c_int;
                while k < n {
                    if !((*xx.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int
                        || (*yy.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int)
                    {
                        nobs += 1;
                        xmean += f128::new(*xx.offset(k as isize));
                        ymean += f128::new(*yy.offset(k as isize))
                    }
                    k += 1
                }
            } else {
                k = 0 as libc::c_int;
                while k < n {
                    if !((*xx.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int
                        || (*yy.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int)
                    {
                        nobs += 1
                    }
                    k += 1
                }
            }
            if nobs >= 2 as libc::c_int {
                sum = f128::new(0.0f64);
                ysd = sum;
                xsd = ysd;
                if kendall as u64 == 0 {
                    xmean /= f128::new(nobs);
                    ymean /= f128::new(nobs);
                    n1 = nobs - 1 as libc::c_int
                }
                k = 0 as libc::c_int;
                while k < n {
                    if !((*xx.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int
                        || (*yy.offset(k as isize)).is_nan() as i32 != 0 as libc::c_int)
                    {
                        if kendall as u64 == 0 {
                            xm = f128::new(*xx.offset(k as isize)) - xmean;
                            ym = f128::new(*yy.offset(k as isize)) - ymean;
                            sum += xm * ym;
                            if cor_0 as u64 != 0 {
                                xsd += xm * xm;
                                ysd += ym * ym
                            }
                        } else {
                            n1 = 0 as libc::c_int;
                            while n1 < k {
                                if !((*xx.offset(n1 as isize)).is_nan() as i32 != 0 as libc::c_int
                                    || (*yy.offset(n1 as isize)).is_nan() as i32
                                        != 0 as libc::c_int)
                                {
                                    xm = f128::new(sign(
                                        *xx.offset(k as isize) - *xx.offset(n1 as isize),
                                    ));
                                    ym = f128::new(sign(
                                        *yy.offset(k as isize) - *yy.offset(n1 as isize),
                                    ));
                                    sum += xm * ym;
                                    if cor_0 as u64 != 0 {
                                        xsd += xm * xm;
                                        ysd += ym * ym
                                    }
                                }
                                n1 += 1
                            }
                        }
                    }
                    k += 1
                }
                if cor_0 as u64 != 0 {
                    if xsd == f128::new(0.0f64) || ysd == f128::new(0.0f64) {
                        *sd_0 = TRUE;
                        sum = f128::new(R_NaReal)
                    } else {
                        if kendall as u64 == 0 {
                            xsd /= f128::new(n1);
                            ysd /= f128::new(n1);
                            sum /= f128::new(n1)
                        }
                        sum /= sqrtl(xsd) * sqrtl(ysd);
                        sum = if sum >= f128::new(1.0f64) {
                            f128::new(1.0f64)
                        } else if sum <= f128::new(-1.0f64) {
                            f128::new(-1.0f64)
                        } else {
                            sum
                        }
                    }
                } else if kendall as u64 == 0 {
                    sum /= f128::new(n1)
                }
                *ans.offset((i + j * ncx) as isize) = tof64!(sum)
            } else {
                *ans.offset((i + j * ncx) as isize) = R_NaReal
            }
            j += 1
        }
        i += 1
    }
}
/* method = "complete" or "all.obs" (only difference: na_fail):
 *           --------      -------
*/
/* -Wall */
/* too many missing */
/* total number of complete observations */
/* This uses two passes for better accuracy */
/* variable means */
/* This uses two passes for better accuracy */
/* variable means (has_na) */
unsafe extern "C" fn cov_complete1(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut x: *mut libc::c_double,
    mut xm: *mut libc::c_double,
    mut ind: *mut libc::c_int,
    mut ans: *mut libc::c_double,
    mut sd_0: *mut Rboolean,
    mut cor_0: Rboolean,
    mut kendall: Rboolean,
) {
    let mut sum: f128 = f128::zero();
    let mut tmp: f128 = f128::zero();
    let mut xxm: f128 = f128::zero();
    let mut yym: f128 = f128::zero();
    let mut xx: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut yy: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i: R_xlen_t = 0;
    let mut j: R_xlen_t = 0;
    let mut k: R_xlen_t = 0;
    let mut n1: R_xlen_t = -(1 as libc::c_int) as R_xlen_t;
    let mut nobs: libc::c_int = 0;
    nobs = 0 as libc::c_int;
    k = 0 as libc::c_int as R_xlen_t;
    while k < n as libc::c_long {
        if *ind.offset(k as isize) != 0 as libc::c_int {
            nobs += 1
        }
        k += 1
    }
    if nobs <= 1 as libc::c_int {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            j = 0 as libc::c_int as R_xlen_t;
            while j < ncx as libc::c_long {
                *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal;
                j += 1
            }
            i += 1
        }
        return;
    }
    if kendall as u64 == 0 {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
            sum = f128::new(0.0f64);
            k = 0 as libc::c_int as R_xlen_t;
            while k < n as libc::c_long {
                if *ind.offset(k as isize) != 0 as libc::c_int {
                    sum += f128::new(*xx.offset(k as isize))
                }
                k += 1
            }
            tmp = sum / f128::new(nobs);
            if tof64!(tmp).is_finite() as i32 != 0 {
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        sum += f128::new(*xx.offset(k as isize)) - tmp
                    }
                    k += 1
                }
                tmp = tmp + sum / f128::new(nobs)
            }
            *xm.offset(i as isize) = tof64!(tmp);
            i += 1
        }
        /* -> xm[] */
        n1 = (nobs - 1 as libc::c_int) as R_xlen_t
    }
    i = 0 as libc::c_int as R_xlen_t;
    while i < ncx as libc::c_long {
        xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
        if kendall as u64 == 0 {
            xxm = f128::new(*xm.offset(i as isize));
            j = 0 as libc::c_int as R_xlen_t;
            while j <= i {
                yy = &mut *x.offset((j * n as libc::c_long) as isize) as *mut libc::c_double;
                yym = f128::new(*xm.offset(j as isize));
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        sum += (f128::new(*xx.offset(k as isize)) - xxm)
                            * (f128::new(*yy.offset(k as isize)) - yym)
                    }
                    k += 1
                }
                let ref mut fresh2 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                *fresh2 = (sum / f128::new(n1)).to_f64().unwrap();
                *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh2;
                j += 1
            }
        } else {
            /* Kendall's tau */
            j = 0 as libc::c_int as R_xlen_t;
            while j <= i {
                yy = &mut *x.offset((j * n as libc::c_long) as isize) as *mut libc::c_double;
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        n1 = 0 as libc::c_int as R_xlen_t;
                        while n1 < n as libc::c_long {
                            if *ind.offset(n1 as isize) != 0 as libc::c_int {
                                sum += f128::new(
                                    sign(*xx.offset(k as isize) - *xx.offset(n1 as isize))
                                        * sign(*yy.offset(k as isize) - *yy.offset(n1 as isize)),
                                )
                            }
                            n1 += 1
                        }
                    }
                    k += 1
                }
                let ref mut fresh3 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                *fresh3 = tof64!(sum);
                *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh3;
                j += 1
            }
        }
        i += 1
    }
    if cor_0 as u64 != 0 {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            *xm.offset(i as isize) = sqrt(*ans.offset((i + i * ncx as libc::c_long) as isize));
            i += 1
        }
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            j = 0 as libc::c_int as R_xlen_t;
            while j < i {
                if *xm.offset(i as isize) == 0 as libc::c_int as libc::c_double
                    || *xm.offset(j as isize) == 0 as libc::c_int as libc::c_double
                {
                    *sd_0 = TRUE;
                    let ref mut fresh4 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                    *fresh4 = R_NaReal;
                    *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh4
                } else {
                    sum = f128::new(
                        *ans.offset((i + j * ncx as libc::c_long) as isize)
                            / (*xm.offset(i as isize) * *xm.offset(j as isize)),
                    );
                    let ref mut fresh5 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                    *fresh5 = if sum >= f128::new(1.0f64) {
                        f128::new(1.0f64)
                    } else if sum <= f128::new(-1.0f64) {
                        f128::new(-1.0f64)
                    } else {
                        sum
                    }
                    .to_f64()
                    .unwrap();
                    *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh5
                }
                j += 1
            }
            *ans.offset((i + i * ncx as libc::c_long) as isize) = 1.0f64;
            i += 1
        }
    };
}
/* cov_complete1 */
unsafe extern "C" fn cov_na_1(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut x: *mut libc::c_double,
    mut xm: *mut libc::c_double,
    mut has_na: *mut libc::c_int,
    mut ans: *mut libc::c_double,
    mut sd_0: *mut Rboolean,
    mut cor_0: Rboolean,
    mut kendall: Rboolean,
) {
    let mut sum: f128 = f128::zero();
    let mut tmp: f128 = f128::zero();
    let mut xxm: f128 = f128::zero();
    let mut yym: f128 = f128::zero();
    let mut xx: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut yy: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i: R_xlen_t = 0;
    let mut j: R_xlen_t = 0;
    let mut k: R_xlen_t = 0;
    let mut n1: R_xlen_t = -(1 as libc::c_int) as R_xlen_t;
    if n <= 1 as libc::c_int {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            j = 0 as libc::c_int as R_xlen_t;
            while j < ncx as libc::c_long {
                *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal;
                j += 1
            }
            i += 1
        }
        return;
    }
    if kendall as u64 == 0 {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            if *has_na.offset(i as isize) != 0 {
                tmp = f128::new(R_NaReal)
            } else {
                xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    sum += f128::new(*xx.offset(k as isize));
                    k += 1
                }
                tmp = sum / f128::new(n);
                if tof64!(tmp).is_finite() as i32 != 0 {
                    sum = f128::new(0.0f64);
                    k = 0 as libc::c_int as R_xlen_t;
                    while k < n as libc::c_long {
                        sum += f128::new(*xx.offset(k as isize)) - tmp;
                        k += 1
                    }
                    tmp = tmp + sum / f128::new(n)
                }
            }
            *xm.offset(i as isize) = tof64!(tmp);
            i += 1
        }
        /* -> xm[] */
        n1 = (n - 1 as libc::c_int) as R_xlen_t
    }
    i = 0 as libc::c_int as R_xlen_t;
    while i < ncx as libc::c_long {
        if *has_na.offset(i as isize) != 0 {
            j = 0 as libc::c_int as R_xlen_t;
            while j <= i {
                let ref mut fresh6 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                *fresh6 = R_NaReal;
                *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh6;
                j += 1
            }
        } else {
            xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
            if kendall as u64 == 0 {
                xxm = f128::new(*xm.offset(i as isize));
                j = 0 as libc::c_int as R_xlen_t;
                while j <= i {
                    if *has_na.offset(j as isize) != 0 {
                        let ref mut fresh7 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                        *fresh7 = R_NaReal;
                        *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh7
                    } else {
                        yy =
                            &mut *x.offset((j * n as libc::c_long) as isize) as *mut libc::c_double;
                        yym = f128::new(*xm.offset(j as isize));
                        sum = f128::new(0.0f64);
                        k = 0 as libc::c_int as R_xlen_t;
                        while k < n as libc::c_long {
                            sum += (f128::new(*xx.offset(k as isize)) - xxm)
                                * (f128::new(*yy.offset(k as isize)) - yym);
                            k += 1
                        }
                        let ref mut fresh8 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                        *fresh8 = (sum / f128::new(n1)).to_f64().unwrap();
                        *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh8
                    }
                    j += 1
                }
            } else {
                /* Kendall's tau */
                j = 0 as libc::c_int as R_xlen_t;
                while j <= i {
                    if *has_na.offset(j as isize) != 0 {
                        let ref mut fresh9 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                        *fresh9 = R_NaReal;
                        *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh9
                    } else {
                        yy =
                            &mut *x.offset((j * n as libc::c_long) as isize) as *mut libc::c_double;
                        sum = f128::new(0.0f64);
                        k = 0 as libc::c_int as R_xlen_t;
                        while k < n as libc::c_long {
                            n1 = 0 as libc::c_int as R_xlen_t;
                            while n1 < n as libc::c_long {
                                sum += f128::new(
                                    sign(*xx.offset(k as isize) - *xx.offset(n1 as isize))
                                        * sign(*yy.offset(k as isize) - *yy.offset(n1 as isize)),
                                );
                                n1 += 1
                            }
                            k += 1
                        }
                        let ref mut fresh10 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                        *fresh10 = tof64!(sum);
                        *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh10
                    }
                    j += 1
                }
            }
        }
        i += 1
    }
    if cor_0 as u64 != 0 {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            if *has_na.offset(i as isize) == 0 {
                *xm.offset(i as isize) = sqrt(*ans.offset((i + i * ncx as libc::c_long) as isize))
            }
            i += 1
        }
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            if *has_na.offset(i as isize) == 0 {
                j = 0 as libc::c_int as R_xlen_t;
                while j < i {
                    if *xm.offset(i as isize) == 0 as libc::c_int as libc::c_double
                        || *xm.offset(j as isize) == 0 as libc::c_int as libc::c_double
                    {
                        *sd_0 = TRUE;
                        let ref mut fresh11 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                        *fresh11 = R_NaReal;
                        *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh11
                    } else {
                        sum = f128::new(
                            *ans.offset((i + j * ncx as libc::c_long) as isize)
                                / (*xm.offset(i as isize) * *xm.offset(j as isize)),
                        );
                        let ref mut fresh12 = *ans.offset((i + j * ncx as libc::c_long) as isize);
                        *fresh12 = if sum >= f128::new(1.0f64) {
                            f128::new(1.0f64)
                        } else if sum <= f128::new(-1.0f64) {
                            f128::new(-1.0f64)
                        } else {
                            sum
                        }
                        .to_f64()
                        .unwrap();
                        *ans.offset((j + i * ncx as libc::c_long) as isize) = *fresh12
                    }
                    j += 1
                }
            }
            *ans.offset((i + i * ncx as libc::c_long) as isize) = 1.0f64;
            i += 1
        }
    };
}
/* cov_na_1() */
unsafe extern "C" fn cov_complete2(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut ncy: libc::c_int,
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut xm: *mut libc::c_double,
    mut ym: *mut libc::c_double,
    mut ind: *mut libc::c_int,
    mut ans: *mut libc::c_double,
    mut sd_0: *mut Rboolean,
    mut cor_0: Rboolean,
    mut kendall: Rboolean,
) {
    let mut sum: f128 = f128::zero();
    let mut tmp: f128 = f128::zero();
    let mut xxm: f128 = f128::zero();
    let mut yym: f128 = f128::zero();
    let mut xx: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut yy: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i: R_xlen_t = 0;
    let mut j: R_xlen_t = 0;
    let mut k: R_xlen_t = 0;
    let mut n1: R_xlen_t = -(1 as libc::c_int) as R_xlen_t;
    let mut nobs: libc::c_int = 0;
    nobs = 0 as libc::c_int;
    k = 0 as libc::c_int as R_xlen_t;
    while k < n as libc::c_long {
        if *ind.offset(k as isize) != 0 as libc::c_int {
            nobs += 1
        }
        k += 1
    }
    if nobs <= 1 as libc::c_int {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            j = 0 as libc::c_int as R_xlen_t;
            while j < ncy as libc::c_long {
                *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal;
                j += 1
            }
            i += 1
        }
        return;
    }
    if kendall as u64 == 0 {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
            sum = f128::new(0.0f64);
            k = 0 as libc::c_int as R_xlen_t;
            while k < n as libc::c_long {
                if *ind.offset(k as isize) != 0 as libc::c_int {
                    sum += f128::new(*xx.offset(k as isize))
                }
                k += 1
            }
            tmp = sum / f128::new(nobs);
            if tof64!(tmp).is_finite() as i32 != 0 {
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        sum += f128::new(*xx.offset(k as isize)) - tmp
                    }
                    k += 1
                }
                tmp = tmp + sum / f128::new(nobs)
            }
            *xm.offset(i as isize) = tof64!(tmp);
            i += 1
        }
        /* -> xm[] */
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncy as libc::c_long {
            xx = &mut *y.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
            sum = f128::new(0.0f64);
            k = 0 as libc::c_int as R_xlen_t;
            while k < n as libc::c_long {
                if *ind.offset(k as isize) != 0 as libc::c_int {
                    sum += f128::new(*xx.offset(k as isize))
                }
                k += 1
            }
            tmp = sum / f128::new(nobs);
            if tof64!(tmp).is_finite() as i32 != 0 {
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        sum += f128::new(*xx.offset(k as isize)) - tmp
                    }
                    k += 1
                }
                tmp = tmp + sum / f128::new(nobs)
            }
            *ym.offset(i as isize) = tof64!(tmp);
            i += 1
        }
        /* -> ym[] */
        n1 = (nobs - 1 as libc::c_int) as R_xlen_t
    }
    i = 0 as libc::c_int as R_xlen_t;
    while i < ncx as libc::c_long {
        xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
        if kendall as u64 == 0 {
            xxm = f128::new(*xm.offset(i as isize));
            j = 0 as libc::c_int as R_xlen_t;
            while j < ncy as libc::c_long {
                yy = &mut *y.offset((j * n as libc::c_long) as isize) as *mut libc::c_double;
                yym = f128::new(*ym.offset(j as isize));
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        sum += (f128::new(*xx.offset(k as isize)) - xxm)
                            * (f128::new(*yy.offset(k as isize)) - yym)
                    }
                    k += 1
                }
                *ans.offset((i + j * ncx as libc::c_long) as isize) =
                    (sum / f128::new(n1)).to_f64().unwrap();
                j += 1
            }
        } else {
            /* Kendall's tau */
            j = 0 as libc::c_int as R_xlen_t;
            while j < ncy as libc::c_long {
                yy = &mut *y.offset((j * n as libc::c_long) as isize) as *mut libc::c_double;
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        n1 = 0 as libc::c_int as R_xlen_t;
                        while n1 < n as libc::c_long {
                            if *ind.offset(n1 as isize) != 0 as libc::c_int {
                                sum += f128::new(
                                    sign(*xx.offset(k as isize) - *xx.offset(n1 as isize))
                                        * sign(*yy.offset(k as isize) - *yy.offset(n1 as isize)),
                                )
                            }
                            n1 += 1
                        }
                    }
                    k += 1
                }
                *ans.offset((i + j * ncx as libc::c_long) as isize) = tof64!(sum);
                j += 1
            }
        }
        i += 1
    }
    if cor_0 as u64 != 0 {
        /* Var(X[i]) */
        /* Kendall's tau */
        /* = sign(. - .)^2 */
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
            sum = f128::new(0.0f64);
            if kendall as u64 == 0 {
                xxm = f128::new(*xm.offset(i as isize));
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        sum += (f128::new(*xx.offset(k as isize)) - xxm)
                            * (f128::new(*xx.offset(k as isize)) - xxm)
                    }
                    k += 1
                }
                sum /= f128::new(n1)
            } else {
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        n1 = 0 as libc::c_int as R_xlen_t;
                        while n1 < n as libc::c_long {
                            if *ind.offset(n1 as isize) != 0 as libc::c_int
                                && *xx.offset(k as isize) != *xx.offset(n1 as isize)
                            {
                                sum += f128::new(1.)
                            }
                            n1 += 1
                        }
                    }
                    k += 1
                }
            }
            *xm.offset(i as isize) = sqrtl(sum).to_f64().unwrap();
            i += 1
        }
        /* -> xm[.] */
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncy as libc::c_long {
            xx = &mut *y.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
            sum = f128::new(0.0f64);
            if kendall as u64 == 0 {
                xxm = f128::new(*ym.offset(i as isize));
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        sum += (f128::new(*xx.offset(k as isize)) - xxm)
                            * (f128::new(*xx.offset(k as isize)) - xxm)
                    }
                    k += 1
                }
                sum /= f128::new(n1)
            } else {
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    if *ind.offset(k as isize) != 0 as libc::c_int {
                        n1 = 0 as libc::c_int as R_xlen_t;
                        while n1 < n as libc::c_long {
                            if *ind.offset(n1 as isize) != 0 as libc::c_int
                                && *xx.offset(k as isize) != *xx.offset(n1 as isize)
                            {
                                sum += f128::new(1.)
                            }
                            n1 += 1
                        }
                    }
                    k += 1
                }
            }
            *ym.offset(i as isize) = sqrtl(sum).to_f64().unwrap();
            i += 1
        }
        /* -> ym[.] */
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            j = 0 as libc::c_int as R_xlen_t;
            while j < ncy as libc::c_long {
                if *xm.offset(i as isize) == 0.0f64 || *ym.offset(j as isize) == 0.0f64 {
                    *sd_0 = TRUE;
                    *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal
                } else {
                    *ans.offset((i + j * ncx as libc::c_long) as isize) /=
                        *xm.offset(i as isize) * *ym.offset(j as isize);
                    *ans.offset((i + j * ncx as libc::c_long) as isize) =
                        if *ans.offset((i + j * ncx as libc::c_long) as isize) >= 1.0f64 {
                            1.0f64
                        } else if *ans.offset((i + j * ncx as libc::c_long) as isize) <= -1.0f64 {
                            -1.0f64
                        } else {
                            *ans.offset((i + j * ncx as libc::c_long) as isize)
                        }
                }
                j += 1
            }
            i += 1
        }
    };
    /* cor */
}
/* cov_complete2 */
unsafe extern "C" fn cov_na_2(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut ncy: libc::c_int,
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut xm: *mut libc::c_double,
    mut ym: *mut libc::c_double,
    mut has_na_x: *mut libc::c_int,
    mut has_na_y: *mut libc::c_int,
    mut ans: *mut libc::c_double,
    mut sd_0: *mut Rboolean,
    mut cor_0: Rboolean,
    mut kendall: Rboolean,
) {
    let mut sum: f128 = f128::zero();
    let mut tmp: f128 = f128::zero();
    let mut xxm: f128 = f128::zero();
    let mut yym: f128 = f128::zero();
    let mut xx: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut yy: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i: R_xlen_t = 0;
    let mut j: R_xlen_t = 0;
    let mut k: R_xlen_t = 0;
    let mut n1: R_xlen_t = -(1 as libc::c_int) as R_xlen_t;
    if n <= 1 as libc::c_int {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            j = 0 as libc::c_int as R_xlen_t;
            while j < ncy as libc::c_long {
                *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal;
                j += 1
            }
            i += 1
        }
        return;
    }
    if kendall as u64 == 0 {
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            if *has_na_x.offset(i as isize) != 0 {
                tmp = f128::new(R_NaReal)
            } else {
                xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    sum += f128::new(*xx.offset(k as isize));
                    k += 1
                }
                tmp = sum / f128::new(n);
                if tof64!(tmp).is_finite() as i32 != 0 {
                    sum = f128::new(0.0f64);
                    k = 0 as libc::c_int as R_xlen_t;
                    while k < n as libc::c_long {
                        sum += f128::new(*xx.offset(k as isize)) - tmp;
                        k += 1
                    }
                    tmp = tmp + sum / f128::new(n)
                }
            }
            *xm.offset(i as isize) = tof64!(tmp);
            i += 1
        }
        /* -> xm[] */
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncy as libc::c_long {
            if *has_na_y.offset(i as isize) != 0 {
                tmp = f128::new(R_NaReal)
            } else {
                xx = &mut *y.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
                sum = f128::new(0.0f64);
                k = 0 as libc::c_int as R_xlen_t;
                while k < n as libc::c_long {
                    sum += f128::new(*xx.offset(k as isize));
                    k += 1
                }
                tmp = sum / f128::new(n);
                if tof64!(tmp).is_finite() as i32 != 0 {
                    sum = f128::new(0.0f64);
                    k = 0 as libc::c_int as R_xlen_t;
                    while k < n as libc::c_long {
                        sum += f128::new(*xx.offset(k as isize)) - tmp;
                        k += 1
                    }
                    tmp = tmp + sum / f128::new(n)
                }
            }
            *ym.offset(i as isize) = tof64!(tmp);
            i += 1
        }
        /* -> ym[] */
        n1 = (n - 1 as libc::c_int) as R_xlen_t
    }
    i = 0 as libc::c_int as R_xlen_t;
    while i < ncx as libc::c_long {
        if *has_na_x.offset(i as isize) != 0 {
            j = 0 as libc::c_int as R_xlen_t;
            while j < ncy as libc::c_long {
                *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal;
                j += 1
            }
        } else {
            xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
            if kendall as u64 == 0 {
                xxm = f128::new(*xm.offset(i as isize));
                j = 0 as libc::c_int as R_xlen_t;
                while j < ncy as libc::c_long {
                    if *has_na_y.offset(j as isize) != 0 {
                        *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal
                    } else {
                        yy =
                            &mut *y.offset((j * n as libc::c_long) as isize) as *mut libc::c_double;
                        yym = f128::new(*ym.offset(j as isize));
                        sum = f128::new(0.0f64);
                        k = 0 as libc::c_int as R_xlen_t;
                        while k < n as libc::c_long {
                            sum += (f128::new(*xx.offset(k as isize)) - xxm)
                                * (f128::new(*yy.offset(k as isize)) - yym);
                            k += 1
                        }
                        *ans.offset((i + j * ncx as libc::c_long) as isize) =
                            (sum / f128::new(n1)).to_f64().unwrap()
                    }
                    j += 1
                }
            } else {
                /* Kendall's tau */
                j = 0 as libc::c_int as R_xlen_t;
                while j < ncy as libc::c_long {
                    if *has_na_y.offset(j as isize) != 0 {
                        *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal
                    } else {
                        yy =
                            &mut *y.offset((j * n as libc::c_long) as isize) as *mut libc::c_double;
                        sum = f128::new(0.0f64);
                        k = 0 as libc::c_int as R_xlen_t;
                        while k < n as libc::c_long {
                            n1 = 0 as libc::c_int as R_xlen_t;
                            while n1 < n as libc::c_long {
                                sum += f128::new(
                                    sign(*xx.offset(k as isize) - *xx.offset(n1 as isize))
                                        * sign(*yy.offset(k as isize) - *yy.offset(n1 as isize)),
                                );
                                n1 += 1
                            }
                            k += 1
                        }
                        *ans.offset((i + j * ncx as libc::c_long) as isize) = tof64!(sum)
                    }
                    j += 1
                }
            }
        }
        i += 1
    }
    if cor_0 as u64 != 0 {
        /* Var(X[j]) */
        /* Kendall's tau */
        /* = sign(. - .)^2 */
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            if *has_na_x.offset(i as isize) == 0 {
                xx = &mut *x.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
                sum = f128::new(0.0f64);
                if kendall as u64 == 0 {
                    xxm = f128::new(*xm.offset(i as isize));
                    k = 0 as libc::c_int as R_xlen_t;
                    while k < n as libc::c_long {
                        sum += (f128::new(*xx.offset(k as isize)) - xxm)
                            * (f128::new(*xx.offset(k as isize)) - xxm);
                        k += 1
                    }
                    sum /= f128::new(n1)
                } else {
                    k = 0 as libc::c_int as R_xlen_t;
                    while k < n as libc::c_long {
                        n1 = 0 as libc::c_int as R_xlen_t;
                        while n1 < n as libc::c_long {
                            if *xx.offset(k as isize) != *xx.offset(n1 as isize) {
                                sum += f128::new(1.)
                            }
                            n1 += 1
                        }
                        k += 1
                    }
                }
                *xm.offset(i as isize) = sqrtl(sum).to_f64().unwrap()
            }
            i += 1
        }
        /* -> xm[.] */
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncy as libc::c_long {
            if *has_na_y.offset(i as isize) == 0 {
                xx = &mut *y.offset((i * n as libc::c_long) as isize) as *mut libc::c_double;
                sum = f128::new(0.0f64);
                if kendall as u64 == 0 {
                    xxm = f128::new(*ym.offset(i as isize));
                    k = 0 as libc::c_int as R_xlen_t;
                    while k < n as libc::c_long {
                        sum += (f128::new(*xx.offset(k as isize)) - xxm)
                            * (f128::new(*xx.offset(k as isize)) - xxm);
                        k += 1
                    }
                    sum /= f128::new(n1)
                } else {
                    k = 0 as libc::c_int as R_xlen_t;
                    while k < n as libc::c_long {
                        n1 = 0 as libc::c_int as R_xlen_t;
                        while n1 < n as libc::c_long {
                            if *xx.offset(k as isize) != *xx.offset(n1 as isize) {
                                sum += f128::new(1.)
                            }
                            n1 += 1
                        }
                        k += 1
                    }
                }
                *ym.offset(i as isize) = sqrtl(sum).to_f64().unwrap()
            }
            i += 1
        }
        /* -> ym[.] */
        i = 0 as libc::c_int as R_xlen_t;
        while i < ncx as libc::c_long {
            if *has_na_x.offset(i as isize) == 0 {
                j = 0 as libc::c_int as R_xlen_t;
                while j < ncy as libc::c_long {
                    if *has_na_y.offset(j as isize) == 0 {
                        if *xm.offset(i as isize) == 0.0f64 || *ym.offset(j as isize) == 0.0f64 {
                            *sd_0 = TRUE;
                            *ans.offset((i + j * ncx as libc::c_long) as isize) = R_NaReal
                        } else {
                            *ans.offset((i + j * ncx as libc::c_long) as isize) /=
                                *xm.offset(i as isize) * *ym.offset(j as isize);
                            *ans.offset((i + j * ncx as libc::c_long) as isize) =
                                if *ans.offset((i + j * ncx as libc::c_long) as isize) >= 1.0f64 {
                                    1.0f64
                                } else if *ans.offset((i + j * ncx as libc::c_long) as isize)
                                    <= -1.0f64
                                {
                                    -1.0f64
                                } else {
                                    *ans.offset((i + j * ncx as libc::c_long) as isize)
                                }
                        }
                    }
                    j += 1
                }
            }
            i += 1
        }
    };
    /* cor */
}
/* cov_na_2 */
/* complete[12]() returns indicator vector ind[] of complete.cases(), or
 * -------------- if(na_fail) signals error if any NA/NaN is encountered
 */
/* This might look slightly inefficient, but it is designed to
 * optimise paging in virtual memory systems ...
 * (or at least that's my story, and I'm sticking to it.)
*/
unsafe extern "C" fn complete1(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut x: *mut libc::c_double,
    mut ind: *mut libc::c_int,
    mut na_fail: Rboolean,
) {
    let mut z: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    i = 0 as libc::c_int;
    while i < n {
        *ind.offset(i as isize) = 1 as libc::c_int;
        i += 1
    }
    j = 0 as libc::c_int;
    while j < ncx {
        z = &mut *x.offset((j * n) as isize) as *mut libc::c_double;
        i = 0 as libc::c_int;
        while i < n {
            if (*z.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                if na_fail as u64 != 0 {
                    error(dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"missing observations in cov/cor\x00" as *const u8 as *const libc::c_char,
                        5 as libc::c_int,
                    ));
                } else {
                    *ind.offset(i as isize) = 0 as libc::c_int
                }
            }
            i += 1
        }
        j += 1
    }
}
unsafe extern "C" fn complete2(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut ncy: libc::c_int,
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut ind: *mut libc::c_int,
    mut na_fail: Rboolean,
) {
    let mut z: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    i = 0 as libc::c_int;
    while i < n {
        *ind.offset(i as isize) = 1 as libc::c_int;
        i += 1
    }
    j = 0 as libc::c_int;
    while j < ncx {
        z = &mut *x.offset((j * n) as isize) as *mut libc::c_double;
        i = 0 as libc::c_int;
        while i < n {
            if (*z.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                if na_fail as u64 != 0 {
                    error(dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"missing observations in cov/cor\x00" as *const u8 as *const libc::c_char,
                        5 as libc::c_int,
                    ));
                } else {
                    *ind.offset(i as isize) = 0 as libc::c_int
                }
            }
            i += 1
        }
        j += 1
    }
    j = 0 as libc::c_int;
    while j < ncy {
        z = &mut *y.offset((j * n) as isize) as *mut libc::c_double;
        i = 0 as libc::c_int;
        while i < n {
            if (*z.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                if na_fail as u64 != 0 {
                    error(dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"missing observations in cov/cor\x00" as *const u8 as *const libc::c_char,
                        5 as libc::c_int,
                    ));
                } else {
                    *ind.offset(i as isize) = 0 as libc::c_int
                }
            }
            i += 1
        }
        j += 1
    }
}
unsafe extern "C" fn find_na_1(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut x: *mut libc::c_double,
    mut has_na: *mut libc::c_int,
) {
    let mut z: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    j = 0 as libc::c_int;
    while j < ncx {
        z = &mut *x.offset((j * n) as isize) as *mut libc::c_double;
        *has_na.offset(j as isize) = 0 as libc::c_int;
        i = 0 as libc::c_int;
        while i < n {
            if (*z.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                *has_na.offset(j as isize) = 1 as libc::c_int;
                break;
            } else {
                i += 1
            }
        }
        j += 1
    }
}
unsafe extern "C" fn find_na_2(
    mut n: libc::c_int,
    mut ncx: libc::c_int,
    mut ncy: libc::c_int,
    mut x: *mut libc::c_double,
    mut y: *mut libc::c_double,
    mut has_na_x: *mut libc::c_int,
    mut has_na_y: *mut libc::c_int,
) {
    let mut z: *mut libc::c_double = 0 as *mut libc::c_double;
    let mut i: libc::c_int = 0;
    let mut j: libc::c_int = 0;
    j = 0 as libc::c_int;
    while j < ncx {
        z = &mut *x.offset((j * n) as isize) as *mut libc::c_double;
        *has_na_x.offset(j as isize) = 0 as libc::c_int;
        i = 0 as libc::c_int;
        while i < n {
            if (*z.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                *has_na_x.offset(j as isize) = 1 as libc::c_int;
                break;
            } else {
                i += 1
            }
        }
        j += 1
    }
    j = 0 as libc::c_int;
    while j < ncy {
        z = &mut *y.offset((j * n) as isize) as *mut libc::c_double;
        *has_na_y.offset(j as isize) = 0 as libc::c_int;
        i = 0 as libc::c_int;
        while i < n {
            if (*z.offset(i as isize)).is_nan() as i32 != 0 as libc::c_int {
                *has_na_y.offset(j as isize) = 1 as libc::c_int;
                break;
            } else {
                i += 1
            }
        }
        j += 1
    }
}
/* co[vr](x, y, use =
    { 1,  2,  3,     4,  5  }
  "all.obs", "complete.obs", "pairwise.complete", "everything", "na.or.complete"
      kendall = TRUE/FALSE)
*/
unsafe extern "C" fn corcov(
    mut x: SEXP,
    mut y: SEXP,
    mut na_method: SEXP,
    mut skendall: SEXP,
    mut cor_0: Rboolean,
) -> SEXP {
    let mut ans: SEXP = 0 as *mut SEXPREC;
    let mut xm: SEXP = 0 as *mut SEXPREC;
    let mut ym: SEXP = 0 as *mut SEXPREC;
    let mut ind: SEXP = 0 as *mut SEXPREC;
    let mut ansmat: Rboolean = FALSE;
    let mut kendall: Rboolean = FALSE;
    let mut pair: Rboolean = FALSE;
    let mut na_fail: Rboolean = FALSE;
    let mut everything: Rboolean = FALSE;
    let mut sd_0: Rboolean = FALSE;
    let mut empty_err: Rboolean = FALSE;
    let mut i: libc::c_int = 0;
    let mut method: libc::c_int = 0;
    let mut n: libc::c_int = 0;
    let mut ncx: libc::c_int = 0;
    let mut ncy: libc::c_int = 0;
    let mut nprotect: libc::c_int = 2 as libc::c_int;
    /* Arg.1: x */
    if (*x).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int {
        /* never allowed */
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"\'x\' is NULL\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if isFactor(x) as u64 != 0 {
        error(dcgettext(b"stats\x00" as *const u8 as *const libc::c_char,
                           b"Calling var(x) on a factor x is defunct.\n  Use something like \'all(duplicated(x)[-1L])\' to test for a constant vector.\x00"
                               as *const u8 as *const libc::c_char,
                           5 as libc::c_int));
    }
    /* length check of x -- only if(empty_err) --> below */
    x = protect(coerceVector(x, 14 as libc::c_int as SEXPTYPE));
    ansmat = isMatrix(x);
    if ansmat as u64 != 0 {
        n = nrows(x);
        ncx = ncols(x)
    } else {
        n = length(x);
        ncx = 1 as libc::c_int
    }
    /* Arg.2: y */
    if (*y).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int {
        /* y = x  : var() */
        ncy = ncx
    } else {
        if isFactor(y) as u64 != 0 {
            error(dcgettext(b"stats\x00" as *const u8 as
                                   *const libc::c_char,
                               b"Calling var(x) on a factor x is defunct.\n  Use something like \'all(duplicated(x)[-1L])\' to test for a constant vector.\x00"
                                   as *const u8 as *const libc::c_char,
                               5 as libc::c_int));
        }
        y = protect(coerceVector(y, 14 as libc::c_int as SEXPTYPE));
        nprotect += 1;
        if isMatrix(y) as u64 != 0 {
            if nrows(y) != n {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"incompatible dimensions\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
            ncy = ncols(y);
            ansmat = TRUE
        } else {
            if length(y) != n {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"incompatible dimensions\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
            ncy = 1 as libc::c_int
        }
    }
    /* Arg.3:  method */
    method = asInteger(na_method);
    /* Arg.4:  kendall */
    kendall = asLogical(skendall) as Rboolean;
    /* "default: complete" (easier for -Wall) */
    na_fail = FALSE;
    everything = FALSE;
    empty_err = TRUE;
    pair = FALSE;
    match method {
        1 => {
            /* use all :  no NAs */
            na_fail = TRUE
        }
        2 => {
            /* complete */
            /* did na.omit in R */
            if LENGTH_EX(
                x,
                b"cov.c\x00" as *const u8 as *const libc::c_char,
                707 as libc::c_int,
            ) == 0
            {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"no complete element pairs\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
        }
        3 => {
            /* pairwise.complete */
            pair = TRUE
        }
        4 => {
            /* "everything": NAs are propagated */
            everything = TRUE;
            empty_err = FALSE
        }
        5 => {
            /* "na.or.complete": NAs are propagated */
            empty_err = FALSE
        }
        _ => {
            error(dcgettext(
                b"stats\x00" as *const u8 as *const libc::c_char,
                b"invalid \'use\' (computational method)\x00" as *const u8 as *const libc::c_char,
                5 as libc::c_int,
            ));
        }
    }
    if empty_err as libc::c_uint != 0
        && LENGTH_EX(
            x,
            b"cov.c\x00" as *const u8 as *const libc::c_char,
            722 as libc::c_int,
        ) == 0
    {
        error(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"\'x\' is empty\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    if ansmat as u64 != 0 {
        ans = allocMatrix(14 as libc::c_int as SEXPTYPE, ncx, ncy);
        protect(ans);
    } else {
        ans = allocVector(14 as libc::c_int as SEXPTYPE, (ncx * ncy) as R_xlen_t);
        protect(ans);
    }
    sd_0 = FALSE;
    if (*y).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int {
        if everything as u64 != 0 {
            /* NA's are propagated */
            xm = allocVector(14 as libc::c_int as SEXPTYPE, ncx as R_xlen_t);
            protect(xm);
            ind = allocVector(10 as libc::c_int as SEXPTYPE, ncx as R_xlen_t);
            protect(ind);
            find_na_1(
                n,
                ncx,
                DATAPTR(x) as *mut libc::c_double,
                DATAPTR(ind) as *mut libc::c_int,
            );
            cov_na_1(
                n,
                ncx,
                DATAPTR(x) as *mut libc::c_double,
                DATAPTR(xm) as *mut libc::c_double,
                DATAPTR(ind) as *mut libc::c_int,
                DATAPTR(ans) as *mut libc::c_double,
                &mut sd_0,
                cor_0,
                kendall,
            );
            unprotect(2 as libc::c_int);
        } else if pair as u64 == 0 {
            /* all | complete "var" */
            xm = allocVector(14 as libc::c_int as SEXPTYPE, ncx as R_xlen_t);
            protect(xm);
            ind = allocVector(13 as libc::c_int as SEXPTYPE, n as R_xlen_t);
            protect(ind);
            complete1(
                n,
                ncx,
                DATAPTR(x) as *mut libc::c_double,
                DATAPTR(ind) as *mut libc::c_int,
                na_fail,
            );
            cov_complete1(
                n,
                ncx,
                DATAPTR(x) as *mut libc::c_double,
                DATAPTR(xm) as *mut libc::c_double,
                DATAPTR(ind) as *mut libc::c_int,
                DATAPTR(ans) as *mut libc::c_double,
                &mut sd_0,
                cor_0,
                kendall,
            );
            if empty_err as u64 != 0 {
                let mut indany: Rboolean = FALSE;
                i = 0 as libc::c_int;
                while i < n {
                    if *(DATAPTR(ind) as *mut libc::c_int).offset(i as isize) == 1 as libc::c_int {
                        indany = TRUE;
                        break;
                    } else {
                        i += 1
                    }
                }
                if indany as u64 == 0 {
                    error(dcgettext(
                        b"stats\x00" as *const u8 as *const libc::c_char,
                        b"no complete element pairs\x00" as *const u8 as *const libc::c_char,
                        5 as libc::c_int,
                    ));
                }
            }
            unprotect(2 as libc::c_int);
        } else {
            /* pairwise "var" */
            cov_pairwise1(
                n,
                ncx,
                DATAPTR(x) as *mut libc::c_double,
                DATAPTR(ans) as *mut libc::c_double,
                &mut sd_0,
                cor_0,
                kendall,
            );
        }
    } else if everything as u64 != 0 {
        let mut has_na_y: SEXP = 0 as *mut SEXPREC;
        xm = allocVector(14 as libc::c_int as SEXPTYPE, ncx as R_xlen_t);
        protect(xm);
        ym = allocVector(14 as libc::c_int as SEXPTYPE, ncy as R_xlen_t);
        protect(ym);
        ind = allocVector(10 as libc::c_int as SEXPTYPE, ncx as R_xlen_t);
        protect(ind);
        has_na_y = allocVector(10 as libc::c_int as SEXPTYPE, ncy as R_xlen_t);
        protect(has_na_y);
        find_na_2(
            n,
            ncx,
            ncy,
            DATAPTR(x) as *mut libc::c_double,
            DATAPTR(y) as *mut libc::c_double,
            DATAPTR(ind) as *mut libc::c_int,
            DATAPTR(has_na_y) as *mut libc::c_int,
        );
        cov_na_2(
            n,
            ncx,
            ncy,
            DATAPTR(x) as *mut libc::c_double,
            DATAPTR(y) as *mut libc::c_double,
            DATAPTR(xm) as *mut libc::c_double,
            DATAPTR(ym) as *mut libc::c_double,
            DATAPTR(ind) as *mut libc::c_int,
            DATAPTR(has_na_y) as *mut libc::c_int,
            DATAPTR(ans) as *mut libc::c_double,
            &mut sd_0,
            cor_0,
            kendall,
        );
        unprotect(4 as libc::c_int);
    } else if pair as u64 == 0 {
        /* Co[vr] (x, y) */
        /* all | complete */
        xm = allocVector(14 as libc::c_int as SEXPTYPE, ncx as R_xlen_t);
        protect(xm);
        ym = allocVector(14 as libc::c_int as SEXPTYPE, ncy as R_xlen_t);
        protect(ym);
        ind = allocVector(13 as libc::c_int as SEXPTYPE, n as R_xlen_t);
        protect(ind);
        complete2(
            n,
            ncx,
            ncy,
            DATAPTR(x) as *mut libc::c_double,
            DATAPTR(y) as *mut libc::c_double,
            DATAPTR(ind) as *mut libc::c_int,
            na_fail,
        );
        cov_complete2(
            n,
            ncx,
            ncy,
            DATAPTR(x) as *mut libc::c_double,
            DATAPTR(y) as *mut libc::c_double,
            DATAPTR(xm) as *mut libc::c_double,
            DATAPTR(ym) as *mut libc::c_double,
            DATAPTR(ind) as *mut libc::c_int,
            DATAPTR(ans) as *mut libc::c_double,
            &mut sd_0,
            cor_0,
            kendall,
        );
        if empty_err as u64 != 0 {
            let mut indany_0: Rboolean = FALSE;
            i = 0 as libc::c_int;
            while i < n {
                if *(DATAPTR(ind) as *mut libc::c_int).offset(i as isize) == 1 as libc::c_int {
                    indany_0 = TRUE;
                    break;
                } else {
                    i += 1
                }
            }
            if indany_0 as u64 == 0 {
                error(dcgettext(
                    b"stats\x00" as *const u8 as *const libc::c_char,
                    b"no complete element pairs\x00" as *const u8 as *const libc::c_char,
                    5 as libc::c_int,
                ));
            }
        }
        unprotect(3 as libc::c_int);
    } else {
        /* pairwise */
        cov_pairwise2(
            n,
            ncx,
            ncy,
            DATAPTR(x) as *mut libc::c_double,
            DATAPTR(y) as *mut libc::c_double,
            DATAPTR(ans) as *mut libc::c_double,
            &mut sd_0,
            cor_0,
            kendall,
        );
    }
    if ansmat as u64 != 0 {
        /* set dimnames() when applicable */
        if (*y).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int {
            x = getAttrib(x, R_DimNamesSymbol);
            if !((*x).sxpinfo.type_0() as libc::c_int == 0 as libc::c_int)
                && !((**(DATAPTR(x) as *mut SEXP).offset(1 as libc::c_int as isize))
                    .sxpinfo
                    .type_0() as libc::c_int
                    == 0 as libc::c_int)
            {
                ind = allocVector(19 as libc::c_int as SEXPTYPE, 2 as libc::c_int as R_xlen_t);
                protect(ind);
                SET_VECTOR_ELT(
                    ind,
                    0 as libc::c_int as R_xlen_t,
                    duplicate(*(DATAPTR(x) as *mut SEXP).offset(1 as libc::c_int as isize)),
                );
                SET_VECTOR_ELT(
                    ind,
                    1 as libc::c_int as R_xlen_t,
                    duplicate(*(DATAPTR(x) as *mut SEXP).offset(1 as libc::c_int as isize)),
                );
                setAttrib(ans, R_DimNamesSymbol, ind);
                unprotect(1 as libc::c_int);
            }
        } else {
            x = getAttrib(x, R_DimNamesSymbol);
            y = getAttrib(y, R_DimNamesSymbol);
            if length(x) >= 2 as libc::c_int
                && !((**(DATAPTR(x) as *mut SEXP).offset(1 as libc::c_int as isize))
                    .sxpinfo
                    .type_0() as libc::c_int
                    == 0 as libc::c_int)
                || length(y) >= 2 as libc::c_int
                    && !((**(DATAPTR(y) as *mut SEXP).offset(1 as libc::c_int as isize))
                        .sxpinfo
                        .type_0() as libc::c_int
                        == 0 as libc::c_int)
            {
                ind = allocVector(19 as libc::c_int as SEXPTYPE, 2 as libc::c_int as R_xlen_t);
                protect(ind);
                if length(x) >= 2 as libc::c_int
                    && !((**(DATAPTR(x) as *mut SEXP).offset(1 as libc::c_int as isize))
                        .sxpinfo
                        .type_0() as libc::c_int
                        == 0 as libc::c_int)
                {
                    SET_VECTOR_ELT(
                        ind,
                        0 as libc::c_int as R_xlen_t,
                        duplicate(*(DATAPTR(x) as *mut SEXP).offset(1 as libc::c_int as isize)),
                    );
                }
                if length(y) >= 2 as libc::c_int
                    && !((**(DATAPTR(y) as *mut SEXP).offset(1 as libc::c_int as isize))
                        .sxpinfo
                        .type_0() as libc::c_int
                        == 0 as libc::c_int)
                {
                    SET_VECTOR_ELT(
                        ind,
                        1 as libc::c_int as R_xlen_t,
                        duplicate(*(DATAPTR(y) as *mut SEXP).offset(1 as libc::c_int as isize)),
                    );
                }
                setAttrib(ans, R_DimNamesSymbol, ind);
                unprotect(1 as libc::c_int);
            }
        }
    }
    if sd_0 as u64 != 0 {
        /* only in cor() */
        warning(dcgettext(
            b"stats\x00" as *const u8 as *const libc::c_char,
            b"the standard deviation is zero\x00" as *const u8 as *const libc::c_char,
            5 as libc::c_int,
        ));
    }
    unprotect(nprotect);
    return ans;
}
