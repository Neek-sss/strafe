// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal,
};
use strafe_type::FloatConstraint;

use crate::funcs::{
    ppoints::ppoints,
    var::{sd, variance},
};

pub fn sd_test_inner(x: &[f64]) {
    let rust_sd = sd(x);
    let r_sd = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_slice(x),
        )))
        .set_display(&RString::from_str("sd(x)").unwrap())
        .run()
        .expect("R running error")
        .as_f64()
        .expect("R running error");
    r_assert_relative_equal!(rust_sd, r_sd);
}

pub fn variance_test_inner(x: &[f64]) {
    let rust_variance = variance(x);
    let r_variance = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_slice(x),
        )))
        .set_display(&RString::from_str("var(x)").unwrap())
        .run()
        .expect("R running error")
        .as_f64()
        .expect("R running error");
    r_assert_relative_equal!(rust_variance, r_variance);
}

pub fn ppoints_test_inner(n: usize) {
    let rust_ppoints = ppoints(n, None);
    let r_ppoints = RTester::new()
        .set_script(&RString::from_string(format!(
            "n={};",
            RString::from_f64(n as f64),
        )))
        .set_display(&RString::from_str("ppoints(n)").unwrap())
        .run()
        .expect("R running error")
        .as_f64_vec()
        .expect("R running error");
    for (rust_ret, r_ret) in rust_ppoints.into_iter().zip(r_ppoints.into_iter()) {
        r_assert_relative_equal!(rust_ret.unwrap(), r_ret);
    }
}
