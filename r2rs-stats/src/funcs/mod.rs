// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

mod cov;
mod hat;
mod linear_regression;
mod lowess;
pub mod opt;
mod optimize;
mod p_adjust;
mod ppoints;
mod predict;
mod residuals;
mod sum_of_squares;
mod var;

pub use self::{
    cov::*, hat::hat, linear_regression::*, lowess::lowess, optimize::optimize, p_adjust::*,
    ppoints::ppoints, predict::*, residuals::*, sum_of_squares::*, var::variance,
};

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
