// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn sd_test() {
    sd_test_inner(&[1.0, 55.45, 3048.0, 035453.0, 0.1022, 0.0, 0.05, 0.05])
}

#[test]
fn variance_test() {
    variance_test_inner(&[1.0, 55.45, 3048.0, 035453.0, 0.1022, 0.0, 0.05, 0.05])
}

#[test]
fn ppoints_test() {
    ppoints_test_inner(5)
}
