use strafe_type::Probability64;

pub fn ppoints(n: usize, a: Option<f64>) -> Vec<Probability64> {
    let a = if let Some(a) = a {
        a
    } else {
        if n <= 10 {
            3.0 / 8.0
        } else {
            0.5
        }
    };

    (1..=n)
        .map(|f| ((f as f64 - a) / (n as f64 + 1.0 - 2.0 * a)).into())
        .collect()
}
