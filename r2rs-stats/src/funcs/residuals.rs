// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::funcs::predict;

pub fn residuals(y_real: &DMatrix<f64>, x: &DMatrix<f64>, b: &DMatrix<f64>) -> DMatrix<f64> {
    y_real - predict(x, b)
}

pub fn weighted_residuals(
    y_real: &DMatrix<f64>,
    x: &DMatrix<f64>,
    b: &DMatrix<f64>,
    w: &DMatrix<f64>,
) -> DMatrix<f64> {
    let mut sqrt_w = w.clone();
    sqrt_w.as_mut_slice().iter_mut().for_each(|w| *w = w.sqrt());

    let mut weighted_x = x.clone();
    weighted_x
        .column_iter_mut()
        .for_each(|mut row| row.component_mul_assign(&sqrt_w));
    let weighted_y = y_real.component_mul(&sqrt_w);

    residuals(&weighted_y, &weighted_x, b).component_div(&sqrt_w)
}
