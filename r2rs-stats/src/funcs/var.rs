// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use r2rs_base::traits::StatisticalSlice;

pub fn variance(x: &[f64]) -> f64 {
    let mean = x.mean();
    x.iter().fold(0.0, |s, x_i| s + (x_i - mean).powi(2)) / (x.len() - 1) as f64
}

pub fn sd(x: &[f64]) -> f64 {
    variance(x).sqrt()
}
