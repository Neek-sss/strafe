// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub fn fminfn(
    n: usize,
    p: &[f64],
    parscale: &[f64],
    fnscale: f64,
    min_func: &dyn Fn(Vec<f64>) -> f64,
) -> f64 {
    let mut beta = vec![0.0; n];
    for i in 0..n {
        beta[i] = p[i] * parscale[i];
    }
    let s = min_func(beta);
    s / fnscale
}
