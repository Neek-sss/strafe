// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

mod brent;
mod cgmin;
mod fminfn;
mod fmingr;
mod hessian;
mod lbfgsb;
mod nmmin;
mod samin;
mod vmmin;

use std::error::Error;

pub use cgmin::CG;
pub use lbfgsb::L_BFGS_B;
pub use nmmin::NelderMead;
pub use samin::SANN;
pub use vmmin::BFGS;

pub struct OptimOptions {
    pub parscale: Option<Vec<f64>>,
    pub fnscale: f64,
    pub abstol: f64,
    pub reltol: f64,
    pub maxit: usize,
}

#[derive(Clone, Debug)]
pub struct OptimResponse<T> {
    pub parameters: T,
    pub value: f64,
    pub function_count: usize,
    pub gradient_count: usize,
    pub convergence: bool,
}

pub trait Optim {
    type ParameterType;
    fn optim(
        &mut self,
        par: Self::ParameterType,
        min_func: &dyn Fn(Self::ParameterType) -> f64,
    ) -> Result<OptimResponse<Self::ParameterType>, Box<dyn Error>>;
}

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
