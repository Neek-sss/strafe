// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

fn opt_funcs() -> (
    (Box<dyn Fn(Vec<f64>) -> f64>, String),
    (Box<dyn Fn(Vec<f64>) -> Vec<f64>>, String),
) {
    let f_func = |x: Vec<f64>| 100.0 * (x[1] - x[0] * x[0]).powi(2) + (1.0 - x[0]).powi(2);
    let f_string =
        "f_func <- function(x) {100 * (x[2] - x[1] * x[1])^2 + (1 - x[1])^2};".to_string();
    let g_func = |x: Vec<f64>| {
        vec![
            -400.0 * x[0] * (x[1] - x[0] * x[0]) - 2.0 * (1.0 - x[0]),
            200.0 * (x[1] - x[0] * x[0]),
        ]
    };
    let g_string = "g_func <- function(x) {c(-400 * x[1] * (x[2] - x[1] * x[1]) - 2 * (1 - x[1]), 200 * (x[2] - x[1] * x[1]))};".to_string();
    ((Box::new(f_func), f_string), (Box::new(g_func), g_string))
}

#[test]
fn brent_test() {
    let f_func = |x: f64| 100.0 * x.powi(2) + (1.0 - x).powi(2);
    let f_string = "f_func <- function(x) {100 * x^2 + (1 - x)^2};".to_string();
    brent_test_inner(-1.2, -100.0, 100.0, &f_func, f_string);
}

#[test]
fn cgmin_test() {
    let ((f_func, f_string), _) = opt_funcs();
    cgmin_test_inner(&[-1.2, 1.0], &f_func, f_string, None, None);
}

#[test]
fn lbfgsb_test_1() {
    let f = |x: Vec<f64>| x[0].powi(2) + x[1].powi(2);
    let f_string = "f_func <- function(x) {x[1]^2 + x[2]^2};".to_string();
    let g = |x: Vec<f64>| vec![x[0] * 2.0, x[1] * 2.0];
    let g_string = "g_func <- function(x) {c(x[1] * 2.0, x[2] * 2.0)};".to_string();
    lbfgsb_test_inner(
        &[2315.756, 453210.5687],
        &f,
        f_string,
        Some(Box::new(g)),
        Some(g_string),
    );
}

#[test]
fn lbfgsb_test_2() {
    let p = vec![3.0; 25];
    let f_func = |x: Vec<f64>| {
        let p = x.len();
        let mut in_1 = (0..(p - 1)).map(|i| x[i]).collect::<Vec<_>>();
        in_1.insert(0, 1.0);
        let mut in_2 = x.clone();
        in_2.remove(0);
        in_2.insert(0, 1.0);
        let mut ret = vec![std::f64::NAN; x.len()];
        for i in 0..x.len() {
            ret[i] = in_1[i] * (x[i] - in_2[i].powi(2)).powi(2);
        }
        let r_s = ret.into_iter().sum::<f64>();
        r_s
    };
    let f_string =
        "f_func = function(x) {sum(c(1, x[1:(length(x)-1)]) * (x -  c(1, x[2:(length(x))])^2)^2)};"
            .to_string();
    lbfgsb_test_inner(&p, &f_func, f_string, None, None);
}

#[test]
fn lbfgsb_test_3() {
    let ((f_func, f_string), _) = opt_funcs();
    lbfgsb_test_inner(&[-1.2, 1.0], &f_func, f_string, None, None);
}

#[test]
fn nmmin_test() {
    let ((f_func, f_string), _) = opt_funcs();
    nmmin_test_inner(&[-1.2, 1.0], &f_func, f_string);
}

#[test]
fn samin_test() {
    let ((f_func, f_string), _) = opt_funcs();
    samin_test_inner(&[-1.2, 1.0], 1, &f_func, f_string, None, None);
}

#[test]
fn vmmin_test() {
    let ((f_func, f_string), _) = opt_funcs();
    vmmin_test_inner(&[-1.2, 1.0], &f_func, f_string, None, None);
}
