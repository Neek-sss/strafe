use nalgebra::DMatrix;

pub fn hat(x: &DMatrix<f64>, intercept: bool) -> Vec<f64> {
    let qrx = if intercept {
        x.clone().insert_column(0, 1.0).qr()
    } else {
        x.clone().qr()
    }
    .q();

    let mut d = DMatrix::<f64>::zeros(qrx.nrows(), qrx.ncols());
    d.fill_diagonal(1.0);

    (qrx * d.transpose())
        .row_iter()
        .map(|r| r.iter().map(|f| f.powi(2)).sum::<f64>())
        .collect::<Vec<_>>()
}
