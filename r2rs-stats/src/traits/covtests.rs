// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use super::tests::*;
use crate::{
    funcs::{Method, NAMethod},
    traits::{data_set::DeviationType, tests::sd_test_inner},
};

fn longley_mat() -> DMatrix<f64> {
    DMatrix::<f64>::from_vec(
        16,
        7,
        vec![
            83.000, 88.500, 88.200, 89.500, 96.200, 98.100, 99.000, 100.000, 101.200, 104.600,
            108.400, 110.800, 112.600, 114.200, 115.700, 116.900, 234.289, 259.426, 258.054,
            284.599, 328.975, 346.999, 365.385, 363.112, 397.469, 419.180, 442.769, 444.546,
            482.704, 502.601, 518.173, 554.894, 235.600, 232.500, 368.200, 335.100, 209.900,
            193.200, 187.000, 357.800, 290.400, 282.200, 293.600, 468.100, 381.300, 393.100,
            480.600, 400.700, 159.000, 145.600, 161.600, 165.000, 309.900, 359.400, 354.700,
            335.000, 304.800, 285.700, 279.800, 263.700, 255.200, 251.400, 257.200, 282.700,
            107.608, 108.632, 109.773, 110.929, 112.075, 113.270, 115.094, 116.219, 117.388,
            118.734, 120.445, 121.950, 123.366, 125.368, 127.852, 130.081, 1947.000, 1948.000,
            1949.000, 1950.000, 1951.000, 1952.000, 1953.000, 1954.000, 1955.000, 1956.000,
            1957.000, 1958.000, 1959.000, 1960.000, 1961.000, 1962.000, 60.323, 61.122, 60.171,
            61.187, 63.221, 63.639, 64.989, 63.761, 66.019, 67.857, 68.169, 66.513, 68.655, 69.564,
            69.331, 70.551,
        ],
    )
}

#[test]
fn min_test() {
    min_test_inner(1);
}

#[test]
fn max_test() {
    max_test_inner(1);
}

#[test]
fn ranks_test() {
    ranks_test_inner(1);
}

#[test]
fn mean_test() {
    mean_test_inner(1);
}

#[test]
fn median_test_1() {
    median_test_inner(1, 100);
}

#[test]
fn median_test_2() {
    median_test_inner(1, 99);
}

#[test]
fn median_test_3() {
    median_test_inner(1, 1);
}

#[test]
fn cumsum_test() {
    cumsum_test_inner(1);
}

#[test]
fn mad_test_1() {
    mad_test_inner(1, DeviationType::Mean(None));
}

#[test]
fn mad_test_2() {
    mad_test_inner(1, DeviationType::Median(None));
}

#[test]
fn mad_test_3() {
    mad_test_inner(1, DeviationType::Other(3.3));
}

#[test]
fn sd_test() {
    sd_test_inner(&longley_mat())
}

#[test]
fn var_test_1() {
    var_test_inner(&longley_mat(), NAMethod::Everything)
}

#[test]
fn var_test_2() {
    var_test_inner(&longley_mat(), NAMethod::AllObs)
}

#[test]
fn var_test_3() {
    var_test_inner(&longley_mat(), NAMethod::CompleteObs)
}

#[test]
fn var_test_4() {
    var_test_inner(&longley_mat(), NAMethod::NAOrComplete)
}

#[test]
fn var_test_5() {
    var_test_inner(&longley_mat(), NAMethod::PairwiseCompleteObs)
}

#[test]
fn cov_test_1() {
    cov_test_inner(&longley_mat(), NAMethod::Everything, Method::Pearson)
}

#[test]
fn cov_test_2() {
    cov_test_inner(&longley_mat(), NAMethod::Everything, Method::Kendall)
}

#[test]
fn cov_test_3() {
    cov_test_inner(&longley_mat(), NAMethod::Everything, Method::Spearman)
}

#[test]
fn cov_test_4() {
    cov_test_inner(&longley_mat(), NAMethod::CompleteObs, Method::Spearman)
}

#[test]
fn cov_test_5() {
    cov_test_inner(
        &longley_mat(),
        NAMethod::PairwiseCompleteObs,
        Method::Pearson,
    )
}

#[test]
fn cov_y_test() {
    let x = longley_mat();
    let mut y = longley_mat();
    y.iter_mut().for_each(|y_i| *y_i = y_i.powi(7));
    cov_y_test_inner(&x, &y, NAMethod::Everything, Method::Pearson)
}

#[test]
fn cor_test_1() {
    cor_test_inner(&longley_mat(), NAMethod::Everything, Method::Pearson)
}

#[test]
fn cor_test_2() {
    cor_test_inner(&longley_mat(), NAMethod::Everything, Method::Kendall)
}

#[test]
fn cor_test_3() {
    cor_test_inner(&longley_mat(), NAMethod::Everything, Method::Spearman)
}

#[test]
fn cor_test_4() {
    cor_test_inner(&longley_mat(), NAMethod::CompleteObs, Method::Spearman)
}

#[test]
fn cor_test_5() {
    cor_test_inner(
        &longley_mat(),
        NAMethod::PairwiseCompleteObs,
        Method::Kendall,
    )
}

#[test]
fn cor_test_6() {
    cor_test_inner(&longley_mat(), NAMethod::AllObs, Method::Kendall)
}

#[test]
fn cor_test_7() {
    cor_test_inner(
        &longley_mat(),
        NAMethod::PairwiseCompleteObs,
        Method::Spearman,
    )
}

#[test]
fn cor_test_8() {
    cor_test_inner(
        &longley_mat(),
        NAMethod::PairwiseCompleteObs,
        Method::Kendall,
    )
}

#[test]
fn cor_test_9() {
    cor_test_inner(
        &longley_mat(),
        NAMethod::PairwiseCompleteObs,
        Method::Pearson,
    )
}

#[test]
fn cor_y_test() {
    let x = longley_mat();
    let mut y = longley_mat();
    y.iter_mut().for_each(|y_i| *y_i = y_i.powi(7));
    cor_y_test_inner(&x, &y, NAMethod::Everything, Method::Pearson)
}

#[test]
fn cov2cor_test() {
    cov2cor_test_inner(&longley_mat(), NAMethod::Everything, Method::Pearson)
}
