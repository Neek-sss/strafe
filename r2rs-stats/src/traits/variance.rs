// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::DMatrix;

use crate::funcs::{cor, cov, cov2cor, sd, var, Method, NAMethod};

pub trait Variance {
    fn standard_deviation(&self) -> f64;
    fn var(&self, na_method: NAMethod) -> f64;
    fn covariance(&self, method: Method, na_method: NAMethod) -> Self;
    fn covariance_y(&self, y: &Self, method: Method, na_method: NAMethod) -> Self;
    fn correlation(&self, na_method: NAMethod, method: Method) -> Self;
    fn correlation_y(&self, y: &Self, na_method: NAMethod, method: Method) -> Self;
    fn covariance_to_correlation(&self) -> Self;
}

impl Variance for DMatrix<f64> {
    fn standard_deviation(&self) -> f64 {
        sd(self.as_slice())
    }

    fn var(&self, na_method: NAMethod) -> f64 {
        var(self, na_method)
    }

    fn covariance(&self, method: Method, na_method: NAMethod) -> Self {
        cov(self, self, method, na_method)
    }

    fn covariance_y(&self, y: &Self, method: Method, na_method: NAMethod) -> Self {
        cov(self, y, method, na_method)
    }

    fn correlation(&self, na_method: NAMethod, method: Method) -> Self {
        cor(self, self, na_method, method)
    }

    fn correlation_y(&self, y: &Self, na_method: NAMethod, method: Method) -> Self {
        cor(self, y, na_method, method)
    }

    fn covariance_to_correlation(&self) -> Self {
        cov2cor(self)
    }
}
