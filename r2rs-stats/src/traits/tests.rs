// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use nalgebra::DMatrix;
use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal,
};

use crate::{
    funcs::{Method, NAMethod},
    traits::{
        data_set::{DeviationType, StatArray},
        variance::Variance,
    },
};

pub fn min_test_inner(seed: usize) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "min({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = random_nums.min();

    r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
}

pub fn max_test_inner(seed: usize) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "max({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = random_nums.max();

    r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
}

pub fn ranks_test_inner(seed: usize) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "rank({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let rust_ret = random_nums.ranks();

    for (r_ret, rust_ret) in r_ret.into_iter().zip(rust_ret.into_iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn mean_test_inner(seed: usize) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "mean({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = random_nums.mean();

    r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
}

pub fn median_test_inner(seed: usize, num: usize) {
    let seed = seed as u32;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "median({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = random_nums.median();

    r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
}

pub fn cumsum_test_inner(seed: usize) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "cumsum({})",
            RString::from_f64_slice(&random_nums)
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let rust_ret = random_nums.cumsum();

    for (r_ret, rust_ret) in r_ret.into_iter().zip(rust_ret.into_iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn mad_test_inner(seed: usize, deviation_type: DeviationType) {
    let seed = seed as u32;
    let num = 100;

    let random_nums = RTester::new()
        .set_seed(seed as u16)
        .set_rand_type("Wichmann-Hill")
        .set_display(&RString::from_string(format!("runif({})", num,)))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let (cen, cons) = match deviation_type {
        DeviationType::Mean(c) => (
            if let Some(c) = c {
                RString::from_f64(c)
            } else {
                RString::from_str("mean(x)").unwrap()
            },
            RString::from_f64(1.253),
        ),
        DeviationType::Median(c) => (
            if let Some(c) = c {
                RString::from_f64(c)
            } else {
                RString::from_str("median(x)").unwrap()
            },
            RString::from_f64(1.4826),
        ),
        DeviationType::Other(c) => (RString::from_f64(c), RString::from_f64(1.0)),
    };

    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_slice(&random_nums)
        )))
        .set_display(&RString::from_string(format!(
            "mad(x, center={}, constant={})",
            cen, cons,
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = random_nums.mad(deviation_type);

    r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
}

pub fn sd_test_inner(x: &DMatrix<f64>) {
    let rust_ret = x.standard_deviation();
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_matrix(x)
        )))
        .set_display(&RString::from_string(format!("sd(x)",)))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
}

fn na_method_r(na_method: &NAMethod) -> String {
    match na_method {
        NAMethod::Everything => "'everything'",
        NAMethod::AllObs => "'all.obs'",
        NAMethod::CompleteObs => "'complete.obs'",
        NAMethod::NAOrComplete => "'na.or.complete'",
        NAMethod::PairwiseCompleteObs => "'pairwise.complete.obs'",
    }
    .to_string()
}

fn method_r(method: &Method) -> String {
    match method {
        Method::Pearson => "'pearson'",
        Method::Kendall => "'kendall'",
        Method::Spearman => "'spearman'",
    }
    .to_string()
}

pub fn var_test_inner(x: &DMatrix<f64>, na_method: NAMethod) {
    let rust_ret = x.var(na_method);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};use={};",
            RString::from_f64_matrix(x),
            na_method_r(&na_method),
        )))
        .set_display(&RString::from_string(
            format!("var(as.double(x), use=use)",),
        ))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
}

pub fn cov_test_inner(x: &DMatrix<f64>, na_method: NAMethod, method: Method) {
    let rust_ret = x.covariance(method, na_method);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};use={};method={};",
            RString::from_f64_matrix(x),
            na_method_r(&na_method),
            method_r(&method),
        )))
        .set_display(&RString::from_string(format!(
            "cov(x, use=use, method=method)",
        )))
        .run()
        .unwrap()
        .as_f64_matrix()
        .unwrap();

    for (&r_ret, &rust_ret) in r_ret.as_slice().iter().zip(rust_ret.as_slice().iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn cov_y_test_inner(x: &DMatrix<f64>, y: &DMatrix<f64>, na_method: NAMethod, method: Method) {
    let rust_ret = x.covariance_y(y, method, na_method);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};y={};use={};method={};",
            RString::from_f64_matrix(x),
            RString::from_f64_matrix(y),
            na_method_r(&na_method),
            method_r(&method),
        )))
        .set_display(&RString::from_string(format!(
            "cov(x, y, use=use, method=method)",
        )))
        .run()
        .unwrap()
        .as_f64_matrix()
        .unwrap();

    for (&r_ret, &rust_ret) in r_ret.as_slice().iter().zip(rust_ret.as_slice().iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn cor_test_inner(x: &DMatrix<f64>, na_method: NAMethod, method: Method) {
    let rust_ret = x.correlation(na_method, method);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};use={};method={};",
            RString::from_f64_matrix(x),
            na_method_r(&na_method),
            method_r(&method),
        )))
        .set_display(&RString::from_string(format!(
            "cor(x, use=use, method=method)",
        )))
        .run()
        .unwrap()
        .as_f64_matrix()
        .unwrap();

    for (&r_ret, &rust_ret) in r_ret.as_slice().iter().zip(rust_ret.as_slice().iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn cor_y_test_inner(x: &DMatrix<f64>, y: &DMatrix<f64>, na_method: NAMethod, method: Method) {
    let rust_ret = x.correlation_y(y, na_method, method);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};y={};use={};method={};",
            RString::from_f64_matrix(x),
            RString::from_f64_matrix(y),
            na_method_r(&na_method),
            method_r(&method),
        )))
        .set_display(&RString::from_string(format!(
            "cor(x, y, use=use, method=method)"
        )))
        .run()
        .unwrap()
        .as_f64_matrix()
        .unwrap();

    for (&r_ret, &rust_ret) in r_ret.as_slice().iter().zip(rust_ret.as_slice().iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn cov2cor_test_inner(x: &DMatrix<f64>, na_method: NAMethod, method: Method) {
    let rust_ret = x.covariance(method, na_method).covariance_to_correlation();
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "x={};use={};method={};",
            RString::from_f64_matrix(x),
            na_method_r(&na_method),
            method_r(&method),
        )))
        .set_display(&RString::from_string(format!(
            "cov2cor(cov(x, use=use, method=method))",
        )))
        .run()
        .unwrap()
        .as_f64_matrix()
        .unwrap();

    for (&r_ret, &rust_ret) in r_ret.as_slice().iter().zip(rust_ret.as_slice().iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}
