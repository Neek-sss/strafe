* [x] abbey
* [x] accdeaths
* [ ] addterm
* [x] Aids2
* [x] Animals
* [x] anorexia
* [x] area
* [ ] as.fractions
* [x] bacteria
* [x] bandwidth.nrd
* [x] bcv
* [x] beav1
* [x] beav2
* [x] biopsy
* [x] birthwt
* [x] Boston
* [ ] boxcox
* [x] cabbages
* [x] caith
* [x] Cars93
* [x] cats
* [x] cement
* [x] chem
* [ ] con2tr
* [ ] contr.sdif
* [x] coop
* [x] corresp
* [ ] cov.mcd
* [ ] cov.mve
* [ ] cov.rob
* [ ] cov.trob
* [x] cpus
* [x] crabs
* [x] Cushings
* [x] DDT
* [x] deaths
* [ ] denumerate
* [ ] dose.p
* [x] drivers
* [ ] dropterm
* [x] eagles
* [ ] enlist
* [x] epil
* [ ] eqscplot
* [x] farms
* [ ] fbeta
* [x] fgl
* [ ] fitdistr
* [x] forbes
* [ ] fractions
* [ ] frequency.polygon
* [x] GAGurine
* [x] galaxies
* [ ] gamma.dispersion
* [ ] gamma.shape
* [x] gehan
* [x] genotype
* [x] geyser
* [x] gilgais
* [ ] ginv
* [ ] glm.convert
* [ ] glm.nb
* [ ] glmmPQL
* [x] hills
* [ ] hist.FD
* [ ] hist.scott
* [x] housing
* [ ] huber
* [ ] hubers
* [x] immer
* [x] Insurance
* [ ] is.fractions
* [ ] isoMDS
* [ ] kde2d
* [ ] lda
* [ ] ldahist
* [x] leuk
* [ ] lm.gls
* [ ] lm.ridge
* [ ] lmsreg
* [ ] lmwork
* [ ] loglm
* [ ] loglm1
* [ ] logtrans
* [ ] lqs
* [ ] lqs.formula
* [ ] ltsreg
* [x] mammals
* [ ] mca
* [x] mcycle
* [x] Melanoma
* [x] MENARCHE
* [x] michelson
* [x] minn38
* [x] motors
* [x] muscle
* [ ] mvrnorm
* [ ] nclass.freq
* [ ] neg.bin
* [ ] negative.binomial
* [ ] negexp.SSival
* [ ] newcomb
* [x] nlschools
* [x] npk
* [x] npr1
* [ ] Null
* [x] oats
* [x] OME
* [x] painters
* [ ] parcoord
* [x] petrol
* [x] phones
* [x] Pima.te
* [x] Pima.tr
* [x] Pima.tr2
* [ ] polr
* [x] psi.bisquare
* [x] psi.hampel
* [x] psi.huber
* [ ] qda
* [x] quine
* [x] Rabbit
* [ ] rational
* [ ] renumerate
* [x] rlm
* [ ] rms.curv
* [ ] rnegbin
* [x] road
* [x] rotifer
* [x] Rubber
* [ ] sammon
* [ ] select
* [ ] Shepard
* [x] ships
* [x] shoes
* [x] shrimp
* [x] shuttle
* [x] Sitka
* [x] Sitka89
* [x] Skye
* [x] snails
* [x] SP500
* [ ] stdres
* [x] steam
* [ ] stepAIC
* [x] stormer
* [ ] studres
* [x] survey
* [x] synth.te
* [x] synth.tr
* [ ] theta.md
* [ ] theta.ml
* [ ] theta.mm
* [ ] topo
* [x] Traffic
* [ ] truehist
* [ ] ucv
* [x] UScereal
* [x] UScrime
* [x] VA
* [x] waders
* [x] whiteside
* [ ] width.SJ
* [ ] write.matrix
* [x] wtloss