// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use nalgebra::{DMatrix, RealField};
use polars::datatypes::DataType;

use crate::{
    data::{caith, geyser},
    tests::*,
};

#[test]
fn robust_linear_regression_test_1() {
    robust_linear_regression_test_inner(
        DMatrix::from_vec(3, 1, vec![4.0, 7.0, 8.0]),
        vec![1.0, 2.0, 3.0],
        false,
    );
}

#[test]
fn robust_linear_regression_test_2() {
    robust_linear_regression_test_inner(
        DMatrix::from_vec(
            12,
            2,
            vec![
                -9.87693551459278,
                1.06467900124022,
                -8.16295118028367,
                2.86117007324033,
                -9.80629654596706,
                -9.92927951270931,
                -3.73862658481547,
                3.72541509888261,
                3.4364055081821,
                3.55169134064384,
                8.00117733376128,
                0.25994069647508,
                -8.8265790741021,
                -4.7550610161282,
                3.8607996641334,
                -7.9098240234679,
                8.5347010145277,
                -5.1935152931124,
                1.3579251457373,
                -3.4774157250015,
                7.5219208974209,
                -7.7954237437331,
                -9.9158400203837,
                -6.1942007476916,
            ],
        ),
        vec![
            551.6892440366098,
            451.56883856917244,
            -288.1849725502132,
            670.6732887878694,
            -633.2014202846472,
            303.96847310171347,
            -66.56592406135866,
            359.9600461175186,
            -383.6412988177564,
            619.8334975021962,
            898.0573565178088,
            470.5806853522099,
        ],
        false,
    );
}

#[test]
fn robust_linear_regression_test_3() {
    random_robust_linear_regression_test_inner(1, 8.0, 10.0, 27.0, false);
}

#[test]
fn psi_huber_test_1() {
    psi_huber_inner(&[1.0, 2.0, 3.0], 2.5, false);
}

#[test]
fn psi_huber_test_2() {
    psi_huber_inner(&[1.0, 2.0, 3.0], 2.5, true);
}

#[test]
fn psi_hampel_test_1() {
    psi_hampel_inner(&[1.0, 2.0, 3.0], 2.5, 1.1, 3.7, false);
}

#[test]
fn psi_hampel_test_2() {
    psi_hampel_inner(&[1.0, 2.0, 3.0], 2.5, 1.1, 3.7, true);
}

#[test]
fn psi_hampel_test_3() {
    psi_hampel_inner(&[1.0, 2.0, 3.0], 1.1, 2.5, 3.7, true);
}

#[test]
fn psi_hampel_test_4() {
    psi_hampel_inner(&[1.0, 2.0, 3.0], 1.1, 2.5, 3.7, false);
}

#[test]
fn psi_bisquare_test_1() {
    psi_bisquare_inner(&[1.0, 2.0, 3.0], 2.5, false);
}

#[test]
fn psi_bisquare_test_2() {
    psi_bisquare_inner(&[1.0, 2.0, 3.0], 2.5, true);
}

#[test]
fn area_1() {
    area_inner(0.0, f64::pi())
}

#[test]
fn area_2() {
    area_inner(0.0, -f64::pi())
}

#[test]
fn bcv_1() {
    let geyser = geyser().unwrap();

    let x = DMatrix::<f64>::from_iterator(
        geyser.height(),
        1,
        geyser.column("duration").unwrap().iter().map(|f| {
            let ret: f64 = f.cast(&DataType::Float64).try_extract().unwrap();
            ret
        }),
    );

    bcv_inner(&x)
}

#[test]
fn bandwidth_nrd_1() {
    let geyser = geyser().unwrap();

    let x = DMatrix::<f64>::from_iterator(
        geyser.height(),
        1,
        geyser.column("duration").unwrap().iter().map(|f| {
            let ret: f64 = f.cast(&DataType::Float64).try_extract().unwrap();
            ret
        }),
    );

    bandwidth_nrd_inner(&x);
}

#[test]
fn corresp_1() {
    let caith = caith().unwrap();

    corresp_inner(&caith);
}
