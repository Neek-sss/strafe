/// # Adaptive Numerical Integration
///
/// ## Description:
///
/// Integrate a function of one variable over a finite range using a
/// recursive adaptive method.  This function is mainly for
/// demonstration purposes.
///
/// ## Usage:
///
/// area(f, a, b, ..., fa = f(a, ...), fb = f(b, ...),
///  limit = 10, eps = 1e-05)
///
/// ## Arguments:
///
/// * f: The integrand as an ‘S’ function object.  The variable of
///  integration must be the first argument.
/// * a: Lower limit of integration.
/// * b: Upper limit of integration.
/// * ...: Additional arguments needed by the integrand.
/// * fa: Function value at the lower limit.
/// * fb: Function value at the upper limit.
/// * limit: Limit on the depth to which recursion is allowed to go.
/// * eps: Error tolerance to control the process.
///
/// ## Details:
///
/// The method divides the interval in two and compares the values
/// given by Simpson's rule and the trapezium rule.  If these are
/// within eps of each other the Simpson's rule result is given,
/// otherwise the process is applied separately to each half of the
/// interval and the results added together.
///
/// ## Value:
///
/// The integral from ‘a’ to ‘b’ of ‘f(x)’.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (1994) _Modern Applied
/// Statistics with S-Plus._ Springer.  pp. 105-110.
///
/// ## Examples:
///
/// ```r
/// area(sin, 0, pi)  # integrate the sin function from 0 to pi.
/// ```
pub fn area(
    f: &dyn Fn(f64) -> f64,
    lower_limit: f64,
    upper_limit: f64,
    max_iterations: usize,
    epsilon: f64,
) -> f64 {
    let h = upper_limit - lower_limit;
    let d = (lower_limit + upper_limit) / 2.0;
    let fa = f(lower_limit);
    let fb = f(upper_limit);
    let fd = f(d);
    let a1 = ((fa + fb) * h) / 2.0;
    let a2 = ((fa + 4.0 * fd + fb) * h) / 6.0;
    if (a1 - a2).abs() < epsilon || max_iterations == 0 {
        a2
    } else {
        area(f, lower_limit, d, max_iterations - 1, epsilon)
            + area(f, d, upper_limit, max_iterations - 1, epsilon)
    }
}
