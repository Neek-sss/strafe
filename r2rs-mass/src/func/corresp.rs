use nalgebra::DMatrix;
use strafe_type::ModelMatrix;

#[derive(Clone, Debug)]
pub struct Correspondence {
    pub correlation: f64,
    pub row_scores: Vec<f64>,
    pub column_scores: Vec<f64>,
}

/// # Simple Correspondence Analysis
///
/// ## Description:
///
/// Find the principal canonical correlation and corresponding row-
/// and column-scores from a correspondence analysis of a two-way
/// contingency table.
///
/// ## Usage:
///
/// ```r
/// corresp(x, ...)
///
/// ## S3 method for class 'matrix'
/// corresp(x, nf = 1, ...)
///
/// ## S3 method for class 'factor'
/// corresp(x, y, ...)
///
/// ## S3 method for class 'data.frame'
/// corresp(x, ...)
///
/// ## S3 method for class 'xtabs'
/// corresp(x, ...)
///
/// ## S3 method for class 'formula'
/// corresp(formula, data, ...)
/// ```
///
/// ## Arguments:
///
/// * x, formula: The function is generic, accepting various forms of the
///   principal argument for specifying a two-way frequency table.
///   Currently accepted forms are matrices, data frames (coerced
///   to frequency tables), objects of class ‘"xtabs"’ and formulae
///   of the form ‘~ F1 + F2’, where ‘F1’ and ‘F2’ are factors.
/// * nf: The number of factors to be computed. Note that although 1 is
///   the most usual, one school of thought takes the first two
///   singular vectors for a sort of biplot.
/// * y: a second factor for a cross-classification.
/// * data: an optional data frame, list or environment against which to
///   preferentially resolve variables in the formula.
/// * ...: If the principal argument is a formula, a data frame may be
///   specified as well from which variables in the formula are
///   preferentially satisfied.
///
/// ## Details:
///
/// See Venables & Ripley (2002).  The ‘plot’ method produces a
/// graphical representation of the table if ‘nf=1’, with the _areas_
/// of circles representing the numbers of points.  If ‘nf’ is two or
/// more the ‘biplot’ method is called, which plots the second and
/// third columns of the matrices ‘A = Dr^(-1/2) U L’ and ‘B =
/// Dc^(-1/2) V L’ where the singular value decomposition is ‘U L V’.
/// Thus the x-axis is the canonical correlation times the row and
/// column scores.  Although this is called a biplot, it does _not_
/// have any useful inner product relationship between the row and
/// column scores.  Think of this as an equally-scaled plot with two
/// unrelated sets of labels.  The origin is marked on the plot with a
/// cross.  (For other versions of this plot see the book.)
///
/// ## Value:
///
/// An list object of class ‘"correspondence"’ for which ‘print’,
/// ‘plot’ and ‘biplot’ methods are supplied.  The main components are
/// the canonical correlation(s) and the row and column scores.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// Gower, J. C. and Hand, D. J. (1996) _Biplots._ Chapman & Hall.
///
/// ## See Also:
///
/// ‘svd’, ‘princomp’.
///
/// ## Examples:
///
/// ```r
/// ## IGNORE_RDIFF_BEGIN
/// ## The signs can vary by platform
/// (ct <- corresp(~ Age + Eth, data = quine))
/// plot(ct)
///
/// corresp(caith)
/// biplot(corresp(caith, nf = 2))
/// ## IGNORE_RDIFF_END
/// ```
pub fn corresp(mm: &ModelMatrix) -> Vec<Correspondence> {
    let x = mm.matrix();
    let n = x.iter().sum::<f64>();

    let mut dr = &x * DMatrix::<f64>::repeat(x.ncols(), 1, 1.0 / n);
    let mut dc = DMatrix::<f64>::repeat(1, x.nrows(), 1.0 / n) * &x;

    let mut x1 = (x / n) - (&dr * &dc);

    for dr in dr.iter_mut() {
        *dr = 1.0 / dr.sqrt();
    }
    for dc in dc.iter_mut() {
        *dc = 1.0 / dc.sqrt();
    }

    for (mut row, &mul) in x1.row_iter_mut().zip(dr.iter()) {
        for val in row.iter_mut() {
            *val *= mul;
        }
    }
    for (mut column, &mul) in x1.column_iter_mut().zip(dc.iter()) {
        for val in column.iter_mut() {
            *val *= mul;
        }
    }

    let x_svd = x1.svd(true, true);
    let u = x_svd.u.unwrap();
    let v = x_svd.v_t.unwrap().transpose();
    let d = x_svd.singular_values;

    d.into_iter()
        .zip(u.column_iter())
        .zip(v.column_iter())
        .map(|((&cor, row), column)| Correspondence {
            correlation: cor,
            row_scores: row.into_iter().zip(dr.iter()).map(|(v, d)| v * d).collect(),
            column_scores: column
                .into_iter()
                .zip(dc.iter())
                .map(|(v, d)| v * d)
                .collect(),
        })
        .collect()
}
