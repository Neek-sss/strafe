use nalgebra::DMatrix;
use r2rs_base::traits::{QuantileType, StatisticalSlice};
use r2rs_nmath::{func::choose, traits::RNG};
use r2rs_stats::{
    funcs::{cov, Method, NAMethod},
    traits::StatArray,
};
use strafe_type::{FloatConstraint, ModelMatrix};

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum CovRobMethod {
    MinimumVolumeEllipsoid,
    MinimumCovarianceDeterminant,
    ClassicalProductMoment,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum CovRobSample {
    Best,
    Exact,
    Sample,
    Amount(usize),
}

#[derive(Copy, Clone, Debug)]
pub struct CovRobArguments {
    method: CovRobMethod,
    quantile_used: Option<usize>,
    samp: CovRobSample,
}

impl Default for CovRobArguments {
    fn default() -> Self {
        Self {
            method: CovRobMethod::ClassicalProductMoment,
            quantile_used: None,
            samp: CovRobSample::Best,
        }
    }
}

#[derive(Clone, Debug)]
pub struct CovRob {
    center: Vec<f64>,
    cov: DMatrix<f64>,
    cor: DMatrix<f64>,
    n_obs: usize,
    crit: Option<f64>,
    best: Vec<usize>,
}

impl Default for CovRob {
    fn default() -> Self {
        Self {
            center: Default::default(),
            cov: DMatrix::<f64>::zeros(0, 0),
            cor: DMatrix::<f64>::zeros(0, 0),
            n_obs: Default::default(),
            crit: Default::default(),
            best: Default::default(),
        }
    }
}

/// # Resistant Estimation of Multivariate Location and Scatter
///
/// ## Description:
///
/// Compute a multivariate location and scale estimate with a high
/// breakdown point - this can be thought of as estimating the mean
/// and covariance of the ‘good’ part of the data. ‘cov.mve’ and
/// ‘cov.mcd’ are compatibility wrappers.
///
/// ## Usage:
///
/// ```r
/// cov.rob(x, cor = FALSE, quantile.used = floor((n + p + 1)/2),
///  method = c("mve", "mcd", "classical"),
///  nsamp = "best", seed)
///
/// cov.mve(...)
/// cov.mcd(...)
/// ```
///
/// ## Arguments:
///
/// * x: a matrix or data frame.
/// * cor: should the returned result include a correlation matrix?
/// * quantile.used: the minimum number of the data points regarded as ‘good’
/// points.
/// * method: the method to be used - minimum volume ellipsoid, minimum
/// covariance determinant or classical product-moment. Using
/// ‘cov.mve’ or ‘cov.mcd’ forces ‘mve’ or ‘mcd’ respectively.
/// * nsamp: the number of samples or ‘"best"’ or ‘"exact"’ or ‘"sample"’.
/// The limit If ‘"sample"’ the number chosen is ‘min(5*p,
/// 3000)’, taken from Rousseeuw and Hubert (1997). If ‘"best"’
/// exhaustive enumeration is done up to 5000 samples: if
/// ‘"exact"’ exhaustive enumeration will be attempted.
/// * seed: the seed to be used for random sampling: see ‘RNGkind’. The
/// current value of ‘.Random.seed’ will be preserved if it is
/// set.
/// * ...: arguments to ‘cov.rob’ other than ‘method’.
///
/// ## Details:
///
/// For method ‘"mve"’, an approximate search is made of a subset of
/// size ‘quantile.used’ with an enclosing ellipsoid of smallest
/// volume; in method ‘"mcd"’ it is the volume of the Gaussian
/// confidence ellipsoid, equivalently the determinant of the
/// classical covariance matrix, that is minimized. The mean of the
/// subset provides a first estimate of the location, and the rescaled
/// covariance matrix a first estimate of scatter. The Mahalanobis
/// distances of all the points from the location estimate for this
/// covariance matrix are calculated, and those points within the
/// 97.5% point under Gaussian assumptions are declared to be ‘good’.
/// The final estimates are the mean and rescaled covariance of the
/// ‘good’ points.
///
/// The rescaling is by the appropriate percentile under Gaussian
/// data; in addition the first covariance matrix has an _ad hoc_
/// finite-sample correction given by Marazzi.
///
/// For method ‘"mve"’ the search is made over ellipsoids determined
/// by the covariance matrix of ‘p’ of the data points. For method
/// ‘"mcd"’ an additional improvement step suggested by Rousseeuw and
/// van Driessen (1999) is used, in which once a subset of size
/// ‘quantile.used’ is selected, an ellipsoid based on its covariance
/// is tested (as this will have no larger a determinant, and may be
/// smaller).
///
/// There is a hard limit on the allowed number of samples, 2^31 - 1.
/// However, practical limits are likely to be much lower and one
/// might check the number of samples used for exhaustive enumeration,
/// ‘combn(NROW(x), NCOL(x) + 1)’, before attempting it.
///
/// ## Value:
///
/// A list with components
/// * center: the final estimate of location.
/// * cov: the final estimate of scatter.
/// * cor: (only is ‘cor = TRUE’) the estimate of the correlation
/// matrix.
/// * sing: message giving number of singular samples out of total
/// * crit: the value of the criterion on log scale. For MCD this is the
/// determinant, and for MVE it is proportional to the volume.
/// * best: the subset used. For MVE the best sample, for MCD the best
/// set of size ‘quantile.used’.
/// * n.obs: total number of observations.
///
/// ## References:
///
/// P. J. Rousseeuw and A. M. Leroy (1987) _Robust Regression and
/// Outlier Detection._ Wiley.
///
/// A. Marazzi (1993) _Algorithms, Routines and S Functions for Robust
/// Statistics._ Wadsworth and Brooks/Cole.
///
/// P. J. Rousseeuw and B. C. van Zomeren (1990) Unmasking
/// multivariate outliers and leverage points, _Journal of the
/// American Statistical Association_, *85*, 633-639.
///
/// P. J. Rousseeuw and K. van Driessen (1999) A fast algorithm for
/// the minimum covariance determinant estimator. _Technometrics_
/// *41*, 212-223.
///
/// P. Rousseeuw and M. Hubert (1997) Recent developments in PROGRESS.
/// In _L1-Statistical Procedures and Related Topics _ ed Y. Dodge,
/// IMS Lecture Notes volume *31*, pp. 201-214.
///
/// ## See Also:
///
/// ‘lqs’
///
/// ## Examples:
///
/// ```r
/// set.seed(123)
/// cov.rob(stackloss)
/// cov.rob(stack.x, method = "mcd", nsamp = "exact")
/// ```
pub fn cov_rob(x: &ModelMatrix, arguments: &CovRobArguments) -> CovRob {
    let mut arguments = *arguments;
    let mut x = x.matrix();
    let method = arguments.method;
    let n = x.nrows();
    let p = x.ncols();
    // let quantile_used = arguments.quantile_used.unwrap_or_else(|| (n + p + 1) / 2);

    let mut ret = CovRob::default();

    if method == CovRobMethod::ClassicalProductMoment {
        ret.center = x
            .column_iter()
            .map(|column| column.iter().cloned().collect::<Vec<_>>().mean())
            .collect();
        ret.cov = cov(&x, &x, Method::Pearson, NAMethod::Everything);
    } else {
        let divisor = x
            .column_iter()
            .map(|c| c.as_slice().iqr(QuantileType::S))
            .collect::<Vec<_>>();
        for (mut column, div) in x.column_iter_mut().zip(divisor.iter()) {
            for val in column.iter_mut() {
                *val /= div;
            }
        }
        // let qn = quantile_used;
        let ps = p + 1;
        let nexact = choose(n, ps).unwrap() as usize;
        if arguments.samp == CovRobSample::Best {
            if nexact < 5000 {
                arguments.samp = CovRobSample::Exact;
            } else {
                arguments.samp = CovRobSample::Sample;
            }
        }
        if let CovRobSample::Amount(a) = arguments.samp {
            if a > nexact {
                arguments.samp = CovRobSample::Exact;
            }
        }
        let samp = arguments.samp != CovRobSample::Exact;
        if samp {
            if arguments.samp == CovRobSample::Sample {
                arguments.samp = CovRobSample::Amount(3000.min(500 * ps));
            }
        } else {
            arguments.samp = CovRobSample::Exact;
        }

        //     z <- .C(MASS:::mve_fitlots, as.double(x), as.integer(n), as.integer(p),
        //             as.integer(qn), as.integer(method == "mcd"), as.integer(samp),
        //             as.integer(ps), as.integer(nsamp), crit = double(1),
        //             sing = integer(1L), bestone = integer(n))
        //     z$sing <- paste(z$sing, "singular samples of size", ps,
        //                     "out of", nsamp)
        //     crit <- z$crit + 2 * sum(log(divisor)) + if (method ==
        //                                                  "mcd")
        //       -p * log(qn - 1)
        //     else 0
        //     best <- seq(n)[z$bestone != 0]
        //     if (!length(best))
        //       stop("'x' is probably collinear")
        //     means <- colMeans(x[best, , drop = FALSE])
        //     rcov <- var(x[best, , drop = FALSE]) * (1 + 15/(n - p))^2
        //     dist <- mahalanobis(x, means, rcov)
        //     cut <- qchisq(0.975, p) * quantile(dist, qn/n)/qchisq(qn/n,
        //                                                           p)
        //     cov <- divisor * var(x[dist < cut, , drop = FALSE]) *
        //       rep(divisor, rep(p, p))
        //     attr(cov, "names") <- NULL
        //     ans <- list(center = colMeans(x[dist < cut, , drop = FALSE]) *
        //                   divisor, cov = cov, msg = z$sing, crit = crit, best = best)
    }

    let mut sd = ret.cov.diagonal();
    for sd in sd.iter_mut() {
        *sd = sd.sqrt();
    }

    let mut cor = ret.cov.clone();
    for (mut row, sd) in cor.row_iter_mut().zip(sd.iter()) {
        for val in row.iter_mut() {
            *val /= sd;
        }
    }
    for (mut column, sd) in cor.column_iter_mut().zip(sd.iter()) {
        for val in column.iter_mut() {
            *val /= sd;
        }
    }
    ret.cor = cor;

    ret.n_obs = n;

    ret
}

fn mve_fitlots() -> (f64, usize, Vec<bool>) {
    (
        0.0,    // crit
        0,      // sing
        vec![], // betsone (length == nrows of x)
    )
}

/// Sampling k from 0:n-1 without replacement.
fn sample_noreplace<R: RNG>(n: usize, k: usize, rng: &mut R) -> Vec<usize> {
    let mut nn = n;
    let mut ind = vec![0; n];

    let mut x = Vec::new();
    for i in 0..n {
        ind[i] = i;
    }
    for _ in 0..k {
        let j = (nn as f64 * rng.unif_rand()) as usize;
        x.push(ind[j]);
        nn -= 1;
        ind[j] = ind[nn];
    }
    x
}

/// Find all subsets of size k of n in order: this gets a new one each call
fn next_set(n: usize, k: usize) -> Vec<usize> {
    let mut x = vec![0; k];
    let mut j = k - 1;
    x[j] += 1;
    let mut tmp = x[j];
    while j > 0 && x[j] >= n - (k - 1 - j) {
        j -= 1;
        x[j] += 1;
        tmp = x[j];
    }
    for i in (j + 1)..k {
        tmp += 1;
        x[i] = tmp;
    }
    x
}

pub fn cov_mve(x: &ModelMatrix, arguments: &CovRobArguments) -> CovRob {
    if arguments.method != CovRobMethod::MinimumVolumeEllipsoid {
        let mut arguments = arguments.clone();
        arguments.method = CovRobMethod::MinimumVolumeEllipsoid;
        cov_rob(x, &arguments)
    } else {
        cov_rob(x, arguments)
    }
}

pub fn cov_mcd(x: &ModelMatrix, arguments: &CovRobArguments) -> CovRob {
    if arguments.method != CovRobMethod::MinimumCovarianceDeterminant {
        let mut arguments = arguments.clone();
        arguments.method = CovRobMethod::MinimumCovarianceDeterminant;
        cov_rob(x, &arguments)
    } else {
        cov_rob(x, arguments)
    }
}

#[test]
fn t() {
    let stackloss = ModelMatrix::from(strafe_datasets::stackloss().unwrap());

    let rust_covrob = cov_mve(&stackloss, &Default::default());

    println!("center: {:?}", rust_covrob.center);
    println!("cov: {}", rust_covrob.cov);
    println!("cor: {}", rust_covrob.cor);
    println!("n_obs: {}", rust_covrob.n_obs);
    println!("crit: {:?}", rust_covrob.crit);
    println!("best: {:?}", rust_covrob.best);
}

#[test]
fn t1() {
    use std::str::FromStr;

    use strafe_testing::{
        r::{RString, RTester},
        r_assert_relative_equal,
    };

    let stackloss = ModelMatrix::from(strafe_datasets::stackloss().unwrap());

    let rust_covrob = cov_rob(&stackloss, &Default::default());

    println!("center: {:?}", rust_covrob.center);
    println!("cov: {}", rust_covrob.cov);
    println!("cor: {}", rust_covrob.cor);
    println!("n_obs: {}", rust_covrob.n_obs);
    println!("crit: {:?}", rust_covrob.crit);
    println!("best: {:?}", rust_covrob.best);

    let library = RString::from_library_name("MASS");

    let r_covrob_center = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};ret=cov.rob(x, method='classical', cor=T)$center;",
            RString::from_f64_matrix(&stackloss.matrix()),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R error");

    for (r_ret, rust_ret) in rust_covrob
        .center
        .into_iter()
        .zip(r_covrob_center.into_iter())
    {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }

    let r_covrob_cov = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};ret=cov.rob(x, method='classical', cor=T)$cov;",
            RString::from_f64_matrix(&stackloss.matrix()),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R error")
        .as_f64_matrix()
        .expect("R error");

    for (&r_ret, &rust_ret) in rust_covrob.cov.into_iter().zip(r_covrob_cov.into_iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }

    let r_covrob_cor = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};ret=cov.rob(x, method='classical', cor=T)$cor;",
            RString::from_f64_matrix(&stackloss.matrix()),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R error")
        .as_f64_matrix()
        .expect("R error");

    for (&r_ret, &rust_ret) in rust_covrob.cor.into_iter().zip(r_covrob_cor.into_iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }

    let r_covrob_nobs = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};ret=cov.rob(x, method='classical', cor=T)$n.obs;",
            RString::from_f64_matrix(&stackloss.matrix()),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R error")
        .as_f64()
        .expect("R error");

    assert_eq!(r_covrob_nobs as usize, rust_covrob.n_obs);
}
