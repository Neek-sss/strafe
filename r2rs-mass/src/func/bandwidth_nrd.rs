use nalgebra::DMatrix;
use r2rs_base::traits::{QuantileType, StatisticalSlice};
use r2rs_stats::funcs::var;

/// # Bandwidth for density() via Normal Reference Distribution
///
/// ## Description:
///
/// A well-supported rule-of-thumb for choosing the bandwidth of a
/// Gaussian kernel density estimator.
///
/// ## Usage:
///
/// bandwidth.nrd(x)
///
/// ## Arguments:
///
/// * x: A data vector.
///
/// ## Value:
///
/// A bandwidth on a scale suitable for the ‘width’ argument of
/// ‘density’.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Springer, equation (5.5) on page 130.
///
/// ## Examples:
///
/// ```r
/// # The function is currently defined as
/// function(x) {
///     r <- quantile(x, c(0.25, 0.75))
///     h <- (r[2] - r[1])/1.34
///     4 * 1.06 * min(sqrt(var(x)), h) * length(x)^(-1/5)
/// }
/// ```
pub fn bandwidth_nrd(x: &DMatrix<f64>) -> f64 {
    let r = x.as_slice().quantile(&[0.25, 0.75], QuantileType::S);
    let h = (r[1] - r[0]) / 1.34;
    4.0 * 1.06 * var(x, Default::default()).sqrt().min(h) * (x.len() as f64).powf(-1.0 / 5.0)
}
