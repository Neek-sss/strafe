use nalgebra::DMatrix;
use r2rs_stats::funcs::{optimize, var};

use crate::func::bcv::{bcv_bin::bcv_bin, den_bin::den_bin};

mod bcv_bin;
mod den_bin;

/// # Biased Cross-Validation for Bandwidth Selection
///
/// ## Description:
///
/// Uses biased cross-validation to select the bandwidth of a Gaussian
/// kernel density estimator.
///
/// ## Usage:
///
/// bcv(x, nb = 1000, lower, upper)
///
/// ## Arguments:
///
/// * x: a numeric vector
/// * nb: number of bins to use.
/// * lower, upper: Range over which to minimize.  The default is almost
///   always satisfactory.
///
/// ## Value:
///
/// a bandwidth
///
/// ## References:
///
/// Scott, D. W. (1992) _Multivariate Density Estimation: Theory,
/// Practice, and Visualization._ Wiley.
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## See Also:
///
/// ‘ucv’, ‘width.SJ’, ‘density’
///
/// ## Examples:
///
/// ```r
/// bcv(geyser$duration)
/// ```
pub fn bcv(x: &DMatrix<f64>) -> f64 {
    // x: &[f64], nb = 1000, lower = 0.1 * hmax, upper = hmax
    let number_of_bins = 1000;

    let n = x.len();
    let hmax = 1.144 * var(x, Default::default()).sqrt() * (n as f64).powf(-1.0 / 5.0) * 4.0;
    let lower_bound = 0.1 * hmax;
    let upper_bound = hmax;

    let (d, cnt) = den_bin(n, number_of_bins, x);

    let fbcv = |h| bcv_bin(n, cnt.len(), d, &cnt, h);

    optimize(&fbcv, lower_bound, upper_bound, 0.1 * lower_bound, false).0
}
