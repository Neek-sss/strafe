use num_traits::FloatConst;

pub fn bcv_bin(n: usize, nb: usize, d: f64, x: &[usize], h: f64) -> f64 {
    // int   i,
    let nn = n;
    let nbin = nb;
    // double delta, sum, term;
    let hh = h / 4.0;

    let mut sum = 0.0;
    for i in 0..nbin {
        let mut delta = i as f64 * d / hh;
        delta *= delta;
        if delta >= crate::DELMAX as f64 {
            break;
        }
        let term = (-delta / 4.0).exp() * (delta * delta - 12.0 * delta + 12.0);
        sum += term * x[i] as f64;
    }
    1.0 / (2.0 * nn as f64 * hh * f64::PI().sqrt())
        + sum / (64.0 * nn as f64 * nn as f64 * hh * f64::PI().sqrt())
}
