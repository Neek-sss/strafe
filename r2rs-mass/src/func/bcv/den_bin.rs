use nalgebra::DMatrix;

pub fn den_bin(n: usize, nb: usize, x: &DMatrix<f64>) -> (f64, Vec<usize>) {
    let mut cnt = vec![0; nb];
    let nn = n;

    let mut xmin = x[0];
    let mut xmax = xmin;
    for i in 1..nn {
        xmin = xmin.min(x[i]);
        xmax = xmax.max(x[i]);
    }

    let rang = (xmax - xmin) * 1.01;
    let d = rang / (nb as f64);
    let dd = d;
    for i in 1..nn {
        let ii = (x[i] / dd) as isize;
        for j in 0..i {
            let jj = (x[j] / dd) as isize;
            let iij = (ii - jj).abs() as usize;
            cnt[iij] += 1;
        }
    }

    (d, cnt)
}
