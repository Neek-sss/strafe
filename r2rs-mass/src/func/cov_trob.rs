/// # Covariance Estimation for Multivariate t Distribution
///
/// ## Description:
///
/// Estimates a covariance or correlation matrix assuming the data
/// came from a multivariate t distribution: this provides some degree
/// of robustness to outlier without giving a high breakdown point.
///
/// ## Usage:
///
/// ```r
/// cov.trob(x, wt = rep(1, n), cor = FALSE, center = TRUE, nu = 5,
/// maxit = 25, tol = 0.01)
/// ```
///
/// ## Arguments:
///
/// * x: data matrix. Missing values (NAs) are not allowed.
/// * wt: A vector of weights for each case: these are treated as if
/// the case ‘i’ actually occurred ‘wt\[i\]’ times.
/// * cor: Flag to choose between returning the correlation (‘cor =
/// TRUE’) or covariance (‘cor = FALSE’) matrix.
/// * center: a logical value or a numeric vector providing the location
/// about which the covariance is to be taken. If ‘center =
/// FALSE’, no centering is done; if ‘center = TRUE’ the MLE of
/// the location vector is used.
/// * nu: ‘degrees of freedom’ for the multivariate t distribution.
/// Must exceed 2 (so that the covariance matrix is finite).
/// * maxit: Maximum number of iterations in fitting.
/// * tol: Convergence tolerance for fitting.
///
/// ## Value:
///
/// A list with the following components
///
/// * cov: the fitted covariance matrix.
/// * center: the estimated or specified location vector.
/// * wt: the specified weights: only returned if the ‘wt’ argument was
/// given.
/// * n.obs: the number of cases used in the fitting.
/// * cor: the fitted correlation matrix: only returned if ‘cor = TRUE’.
/// * call: The matched call.
/// * iter: The number of iterations used.
///
/// ## References:
///
/// J. T. Kent, D. E. Tyler and Y. Vardi (1994) A curious likelihood
/// identity for the multivariate t-distribution.  _Communications in
/// Statistics-Simulation and Computation_ *23*, 441-453.
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
///
/// ## See Also:
///
/// ‘cov’, ‘cov.wt’, ‘cov.mve’
///
/// ## Examples:
///
/// ```r
/// cov.trob(stackloss)
/// ```
pub fn cov_trob() {}
