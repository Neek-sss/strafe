pub mod robust_linear_model;

pub mod data;
pub mod func;

pub(crate) const DELMAX: usize = 1000;

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
