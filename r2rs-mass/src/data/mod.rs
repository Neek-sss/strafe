use std::io::Cursor;

use polars::prelude::*;

/// # Determinations of Nickel Content
///
/// ## Description:
///
/// A numeric vector of 31 determinations of nickel content (ppm) in a
/// Canadian syenite rock.
///
/// ## Usage:
///
/// abbey
///
/// ## Source:
///
/// S. Abbey (1988) _Geostandards Newsletter_ *12*, 241.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn abbey() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("abbey.csv"))).finish()
}

/// # Accidental Deaths in the US 1973-1978
///
/// ## Description:
///
/// A regular time series giving the monthly totals of accidental
/// deaths in the USA.
///
/// ## Usage:
///
/// accdeaths
///
/// ## Details:
///
/// The values for first six months of 1979 (p. 326) were ‘7798 7406
/// 8363 8460 9217 9316’.
///
/// ## Source:
///
/// P. J. Brockwell and R. A. Davis (1991) _Time Series: Theory and
/// Methods._ Springer, New York.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn accdeaths() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("accdeaths.csv"))).finish()
}

/// # Australian AIDS Survival Data
///
/// ## Description:
///
/// Data on patients diagnosed with AIDS in Australia before 1 July
/// 1991.
///
/// ## Usage:
///
/// Aids2
///  
/// ## Format:
///
/// This data frame contains 2843 rows and the following columns:
///
/// * ‘state’ Grouped state of origin: ‘"NSW "’includes ACT and
/// ‘"other"’ is WA, SA, NT and TAS.
/// * ‘sex’ Sex of patient.
/// * ‘diag’ (Julian) date of diagnosis.
/// * ‘death’ (Julian) date of death or end of observation.
/// * ‘status’ ‘"A"’ (alive) or ‘"D"’ (dead) at end of observation.
/// * ‘T.categ’ Reported transmission category.
/// * ‘age’ Age (years) at diagnosis.
///
/// ## Note:
///
/// This data set has been slightly jittered as a condition of its
/// release, to ensure patient confidentiality.
///
/// ## Source:
///
/// Dr P. J. Solomon and the Australian National Centre in HIV
/// Epidemiology and Clinical Research.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn aids2() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Aids2.csv"))).finish()
}

/// # Brain and Body Weights for 28 Species
///
/// ## Description:
///
/// Average brain and body weights for 28 species of land animals.
///
/// ## Usage:
///
/// Animals
///
/// ## Format:
///
/// * ‘body’ body weight in kg.
/// * ‘brain’ brain weight in g.
///
/// ## Note:
///
/// The name ‘Animals’ avoided conflicts with a system dataset
/// ‘animals’ in S-PLUS 4.5 and later.
///
/// ## Source:
///
/// P. J. Rousseeuw and A. M. Leroy (1987) _Robust Regression and
/// Outlier Detection._ Wiley, p. 57.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn animals() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Animals.csv"))).finish()
}

/// # Anorexia Data on Weight Change
///
/// ## Description:
///
/// The ‘anorexia’ data frame has 72 rows and 3 columns.  Weight
/// change data for young female anorexia patients.
///
/// ## Usage:
///
/// anorexia
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Treat’ Factor of three levels: ‘"Cont"’ (control), ‘"CBT"’
/// (Cognitive Behavioural treatment) and ‘"FT"’ (family
/// treatment).
/// * ‘Prewt’ Weight of patient before study period, in lbs.
/// * ‘Postwt’ Weight of patient after study period, in lbs.
///
/// ## Source:
///
/// Hand, D. J., Daly, F., McConway, K., Lunn, D. and Ostrowski, E.
/// eds (1993) _A Handbook of Small Data Sets._ Chapman & Hall, Data
/// set 285 (p. 229)
///
/// (Note that the original source mistakenly says that weights are in
/// kg.)
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn anorexia() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("anorexia.csv"))).finish()
}

/// # Presence of Bacteria after Drug Treatments
///
/// ## Description:
///
/// Tests of the presence of the bacteria _H. influenzae_ in children
/// with otitis media in the Northern Territory of Australia.
///
/// ## Usage:
///
/// bacteria
///
/// ## Format:
///
/// This data frame has 220 rows and the following columns:
///
/// * y presence or absence: a factor with levels ‘n’ and ‘y’.
/// * ap active/placebo: a factor with levels ‘a’ and ‘p’.
/// * hilo hi/low compliance: a factor with levels ‘hi’ amd ‘lo’.
/// * week numeric: week of test.
/// * ID subject ID: a factor.
/// * trt a factor with levels ‘placebo’, ‘drug’ and ‘drug+’, a
/// re-coding of ‘ap’ and ‘hilo’.
///
/// ## Details:
///
/// Dr A. Leach tested the effects of a drug on 50 children with a
/// history of otitis media in the Northern Territory of Australia.
/// The children were randomized to the drug or the a placebo, and
/// also to receive active encouragement to comply with taking the
/// drug.
///
/// The presence of _H. influenzae_ was checked at weeks 0, 2, 4, 6
/// and 11: 30 of the checks were missing and are not included in this
/// data frame.
///
/// ## Source:
///
/// Dr Amanda Leach _via_ Mr James McBroom.
///
/// ## References:
///
/// Menzies School of Health Research 1999-2000 Annual Report. p.20.
/// <https://www.menzies.edu.au/icms_docs/172302_2000_Annual_report.pdf>.
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// contrasts(bacteria$trt) <- structure(contr.sdif(3),
///  dimnames = list(NULL, c("drug", "encourage")))
/// ## fixed effects analyses
/// ## IGNORE_RDIFF_BEGIN
/// summary(glm(y ~ trt * week, binomial, data = bacteria))
/// summary(glm(y ~ trt + week, binomial, data = bacteria))
/// summary(glm(y ~ trt + I(week > 2), binomial, data = bacteria))
/// ## IGNORE_RDIFF_END
///
/// # conditional random-effects analysis
/// library(survival)
/// bacteria$Time <- rep(1, nrow(bacteria))
/// coxph(Surv(Time, unclass(y)) ~ week + strata(ID),
/// data = bacteria, method = "exact")
/// coxph(Surv(Time, unclass(y)) ~ factor(week) + strata(ID),
/// data = bacteria, method = "exact")
/// coxph(Surv(Time, unclass(y)) ~ I(week > 2) + strata(ID),
/// data = bacteria, method = "exact")
///
/// # PQL glmm analysis
/// library(nlme)
/// ## IGNORE_RDIFF_BEGIN
/// summary(glmmPQL(y ~ trt + I(week > 2), random = ~ 1 | ID,
///  family = binomial, data = bacteria))
/// ## IGNORE_RDIFF_END
/// ```
pub fn bacteria() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("bacteria.csv"))).finish()
}

/// # Body Temperature Series of Beaver 1
///
/// ## Description:
///
/// Reynolds (1994) describes a small part of a study of the long-term
/// temperature dynamics of beaver _Castor canadensis_ in
/// north-central Wisconsin.  Body temperature was measured by
/// telemetry every 10 minutes for four females, but data from a one
/// period of less than a day for each of two animals is used there.
///
/// ## Usage:
///
/// beav1
///
/// ## Format:
///
/// The ‘beav1’ data frame has 114 rows and 4 columns.  This data
/// frame contains the following columns:
///
/// * ‘day’ Day of observation (in days since the beginning of 1990),
/// December 12-13.
/// * ‘time’ Time of observation, in the form ‘0330’ for 3.30am.
/// * ‘temp’ Measured body temperature in degrees Celsius.
/// * ‘activ’ Indicator of activity outside the retreat.
///
/// ## Note:
///
/// The observation at 22:20 is missing.
///
/// ## Source:
///
/// P. S. Reynolds (1994) Time-series analyses of beaver body
/// temperatures.  Chapter 11 of Lange, N., Ryan, L., Billard, L.,
/// Brillinger, D., Conquest, L.  and Greenhouse, J. eds (1994) _Case
/// Studies in Biometry._ New York: John Wiley and Sons.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## See Also:
///
/// ‘beav2’
///
/// ## Examples:
///
/// ```r
/// beav1 <- within(beav1,
/// hours <- 24*(day-346) + trunc(time/100) + (time%%100)/60)
/// plot(beav1$hours, beav1$temp, type="l", xlab="time",
/// ylab="temperature", main="Beaver 1")
/// usr <- par("usr"); usr[3:4] <- c(-0.2, 8); par(usr=usr)
/// lines(beav1$hours, beav1$activ, type="s", lty=2)
/// temp <- ts(c(beav1$temp[1:82], NA, beav1$temp[83:114]),
///  start = 9.5, frequency = 6)
/// activ <- ts(c(beav1$activ[1:82], NA, beav1$activ[83:114]),
/// start = 9.5, frequency = 6)
///
/// acf(temp[1:53])
/// acf(temp[1:53], type = "partial")
/// ar(temp[1:53])
/// act <- c(rep(0, 10), activ)
/// X <- cbind(1, act = act[11:125], act1 = act[10:124],
///  act2 = act[9:123], act3 = act[8:122])
/// alpha <- 0.80
/// stemp <- as.vector(temp - alpha*lag(temp, -1))
/// sX <- X[-1, ] - alpha * X[-115,]
/// beav1.ls <- lm(stemp ~ -1 + sX, na.action = na.omit)
/// summary(beav1.ls, correlation = FALSE)
/// rm(temp, activ)
/// ```
pub fn beav1() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("beav1.csv"))).finish()
}

/// # Body Temperature Series of Beaver 2
///
/// ## Description:
///
/// Reynolds (1994) describes a small part of a study of the long-term
/// temperature dynamics of beaver _Castor canadensis_ in
/// north-central Wisconsin.  Body temperature was measured by
/// telemetry every 10 minutes for four females, but data from a one
/// period of less than a day for each of two animals is used there.
///
/// ## Usage:
///
/// beav2
///
/// ## Format:
///
/// * The ‘beav2’ data frame has 100 rows and 4 columns.  This data
/// frame contains the following columns:
/// * ‘day’ Day of observation (in days since the beginning of 1990),
///  November 3-4.
/// * ‘time’ Time of observation, in the form ‘0330’ for 3.30am.
/// * ‘temp’ Measured body temperature in degrees Celsius.
/// * ‘activ’ Indicator of activity outside the retreat.
///
/// ## Source:
///
/// P. S. Reynolds (1994) Time-series analyses of beaver body
/// temperatures.  Chapter 11 of Lange, N., Ryan, L., Billard, L.,
/// Brillinger, D., Conquest, L.  and Greenhouse, J. eds (1994) _Case
/// Studies in Biometry._ New York: John Wiley and Sons.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## See Also:
///
/// ‘beav1’
///
/// ## Examples:
///
/// ```r
/// attach(beav2)
/// beav2$hours <- 24*(day-307) + trunc(time/100) + (time%%100)/60
/// plot(beav2$hours, beav2$temp, type = "l", xlab = "time",
/// ylab = "temperature", main = "Beaver 2")
/// usr <- par("usr"); usr[3:4] <- c(-0.2, 8); par(usr = usr)
/// lines(beav2$hours, beav2$activ, type = "s", lty = 2)
///
/// temp <- ts(temp, start = 8+2/3, frequency = 6)
/// activ <- ts(activ, start = 8+2/3, frequency = 6)
/// acf(temp[activ == 0]); acf(temp[activ == 1]) # also look at PACFs
/// ar(temp[activ == 0]); ar(temp[activ == 1])
///
/// arima(temp, order = c(1,0,0), xreg = activ)
/// dreg <- cbind(sin = sin(2*pi*beav2$hours/24), cos = cos(2*pi*beav2$hours/24))
/// arima(temp, order = c(1,0,0), xreg = cbind(active=activ, dreg))
///
/// ## IGNORE_RDIFF_BEGIN
/// library(nlme) # for gls and corAR1
/// beav2.gls <- gls(temp ~ activ, data = beav2, correlation = corAR1(0.8),
///  method = "ML")
/// summary(beav2.gls)
/// summary(update(beav2.gls, subset = 6:100))
/// detach("beav2"); rm(temp, activ)
/// ## IGNORE_RDIFF_END
/// ```
pub fn beav2() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("beav2.csv"))).finish()
}

/// # Biopsy Data on Breast Cancer Patients
///
/// ## Description:
///
/// This breast cancer database was obtained from the University of
/// Wisconsin Hospitals, Madison from Dr. William H. Wolberg. He
/// assessed biopsies of breast tumours for 699 patients up to 15 July
/// 1992; each of nine attributes has been scored on a scale of 1 to
/// 10, and the outcome is also known. There are 699 rows and 11
/// columns.
///
/// ## Usage:
///
/// biopsy
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘ID’ sample code number (not unique).
/// * ‘V1’ clump thickness.
/// * ‘V2’ uniformity of cell size.
/// * ‘V3’ uniformity of cell shape.
/// * ‘V4’ marginal adhesion.
/// * ‘V5’ single epithelial cell size.
/// * ‘V6’ bare nuclei (16 values are missing).
/// * ‘V7’ bland chromatin.
/// * ‘V8’ normal nucleoli.
/// * ‘V9’ mitoses.
/// * ‘class’ ‘"benign"’ or ‘"malignant"’.
///
/// ## Source:
///
/// P. M. Murphy and D. W. Aha (1992). UCI Repository of machine
/// learning databases. \[Machine-readable data repository\]. Irvine,
/// CA: University of California, Department of Information and
/// Computer Science.
///
/// O. L. Mangasarian and W. H. Wolberg (1990) Cancer diagnosis via
/// linear programming.  _SIAM News_ *23*, pp 1 & 18.
///
/// William H. Wolberg and O.L. Mangasarian (1990) Multisurface method
/// of pattern separation for medical diagnosis applied to breast
/// cytology.  _Proceedings of the National Academy of Sciences,
/// U.S.A._ *87*, pp. 9193-9196.
///
/// O. L. Mangasarian, R. Setiono and W.H. Wolberg (1990) Pattern
/// recognition via linear programming: Theory and application to
/// medical diagnosis. In _Large-scale Numerical Optimization_ eds
/// Thomas F. Coleman and Yuying Li, SIAM Publications, Philadelphia,
/// pp 22-30.
///
/// K. P. Bennett and O. L. Mangasarian (1992) Robust linear
/// programming discrimination of two linearly inseparable sets.
/// _Optimization Methods and Software_ *1*, pp. 23-34 (Gordon &
/// Breach Science Publishers).
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn biopsy() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("biopsy.csv"))).finish()
}

/// # Risk Factors Associated with Low Infant Birth Weight
///
/// ## Description:
///
/// The ‘birthwt’ data frame has 189 rows and 10 columns.  The data
/// were collected at Baystate Medical Center, Springfield, Mass
/// during 1986.
///
/// ## Usage:
///
/// birthwt
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘low’ indicator of birth weight less than 2.5 kg.
/// * ‘age’ mother's age in years.
/// * ‘lwt’ mother's weight in pounds at last menstrual period.
/// * ‘race’ mother's race (‘1’ = white, ‘2’ = black, ‘3’ = other).
/// * ‘smoke’ smoking status during pregnancy.
/// * ‘ptl’ number of previous premature labours.
/// * ‘ht’ history of hypertension.
/// * ‘ui’ presence of uterine irritability.
/// * ‘ftv’ number of physician visits during the first trimester.
/// * ‘bwt’ birth weight in grams.
///
/// ## Source:
///
/// Hosmer, D.W. and Lemeshow, S. (1989) _Applied Logistic
/// Regression._ New York: Wiley
///
/// References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// bwt <- with(birthwt, {
/// race <- factor(race, labels = c("white", "black", "other"))
/// ptd <- factor(ptl > 0)
/// ftv <- factor(ftv)
/// levels(ftv)[-(1:2)] <- "2+"
/// data.frame(low = factor(low), age, lwt, race, smoke = (smoke > 0),
///  ptd, ht = (ht > 0), ui = (ui > 0), ftv)
/// })
/// options(contrasts = c("contr.treatment", "contr.poly"))
/// glm(low ~ ., binomial, bwt)
/// ```
pub fn birthwt() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("birthwt.csv"))).finish()
}

/// # Housing Values in Suburbs of Boston
///
/// ## Description:
///
/// The ‘Boston’ data frame has 506 rows and 14 columns.
///
/// ## Usage:
///
/// Boston
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘crim’ per capita crime rate by town.
/// * ‘zn’ proportion of residential land zoned for lots over 25,000
///  sq.ft.
/// * ‘indus’ proportion of non-retail business acres per town.
/// * ‘chas’ Charles River dummy variable (= 1 if tract bounds river; 0
///  otherwise).
/// * ‘nox’ nitrogen oxides concentration (parts per 10 million).
/// * ‘rm’ average number of rooms per dwelling.
/// * ‘age’ proportion of owner-occupied units built prior to 1940.
/// * ‘dis’ weighted mean of distances to five Boston employment
///  centres.
/// * ‘rad’ index of accessibility to radial highways.
/// * ‘tax’ full-value property-tax rate per $10,000.
/// * ‘ptratio’ pupil-teacher ratio by town.
/// * ‘black’ 1000(Bk - 0.63)^2 where Bk is the proportion of blacks by
///  town.
/// * ‘lstat’ lower status of the population (percent).
/// * ‘medv’ median value of owner-occupied homes in $1000s.
///
/// ## Source:
///
/// Harrison, D. and Rubinfeld, D.L. (1978) Hedonic prices and the
/// demand for clean air.  _J. Environ. Economics and Management_ *5*,
/// 81-102.
///
/// Belsley D.A., Kuh, E.  and Welsch, R.E. (1980) _Regression
/// Diagnostics. Identifying Influential Data and Sources of
/// Collinearity._ New York: Wiley.
pub fn boston() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Boston.csv"))).finish()
}

/// # Data from a cabbage field trial
///
/// ## Description:
///
/// The ‘cabbages’ data set has 60 observations and 4 variables
///
/// ## Usage:
///
/// cabbages
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Cult’ Factor giving the cultivar of the cabbage, two levels:
///  ‘c39’ and ‘c52’.
/// * ‘Date’ Factor specifying one of three planting dates: ‘d16’, ‘d20’
///  or ‘d21’.
/// * ‘HeadWt’ Weight of the cabbage head, presumably in kg.
/// * ‘VitC’ Ascorbic acid content, in undefined units.
///
/// ## Source:
///
/// Rawlings, J. O. (1988) _Applied Regression Analysis: A Research
/// Tool._ Wadsworth and Brooks/Cole.  Example 8.4, page 219.
/// (Rawlings cites the original source as the files of the late Dr
/// Gertrude M Cox.)
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn cabbages() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("cabbages.csv"))).finish()
}

/// # Colours of Eyes and Hair of People in Caithness
///
/// ## Description:
///
/// Data on the cross-classification of people in Caithness, Scotland,
/// by eye and hair colour. The region of the UK is particularly
/// interesting as there is a mixture of people of Nordic, Celtic and
/// Anglo-Saxon origin.
///
/// ## Usage:
///
/// caith
///
/// ## Format:
///
/// A 4 by 5 table with rows the eye colours (blue, light, medium,
/// dark) and columns the hair colours (fair, red, medium, dark,
/// black).
///
/// ## Source:
///
/// Fisher, R.A. (1940) The precision of discriminant functions.
/// _Annals of Eugenics (London)_ *10*, 422-429.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// ## IGNORE_RDIFF_BEGIN
/// ## The signs can vary by platform
/// corresp(caith)
/// ## IGNORE_RDIFF_END
/// dimnames(caith)[[2]] <- c("F", "R", "M", "D", "B")
/// par(mfcol=c(1,3))
/// plot(corresp(caith, nf=2)); title("symmetric")
/// plot(corresp(caith, nf=2), type="rows"); title("rows")
/// plot(corresp(caith, nf=2), type="col"); title("columns")
/// par(mfrow=c(1,1))
/// ```
pub fn caith() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("caith.csv"))).finish()
}

/// # Data from 93 Cars on Sale in the USA in 1993
///
/// ## Description:
///
/// The ‘Cars93’ data frame has 93 rows and 27 columns.
///
/// ## Usage:
///
/// Cars93
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Manufacturer’ Manufacturer.
/// * ‘Model’ Model.
/// * ‘Type’ Type: a factor with levels ‘"Small"’, ‘"Sporty"’,
///  ‘"Compact"’, ‘"Midsize"’, ‘"Large"’ and ‘"Van"’.
/// * ‘Min.Price’ Minimum Price (in $1,000): price for a basic version.
/// * ‘Price’ Midrange Price (in $1,000): average of ‘Min.Price’ and
///  ‘Max.Price’.
/// * ‘Max.Price’ Maximum Price (in $1,000): price for “a premium
///  version”.
/// * ‘MPG.city’ City MPG (miles per US gallon by EPA rating).
/// * ‘MPG.highway’ Highway MPG.
/// * ‘AirBags’ Air Bags standard. Factor: none, driver only, or driver
///  & passenger.
/// * ‘DriveTrain’ Drive train type: rear wheel, front wheel or 4WD;
///  (factor).
/// * ‘Cylinders’ Number of cylinders (missing for Mazda RX-7, which has
///  a rotary engine).
/// * ‘EngineSize’ Engine size (litres).
/// * ‘Horsepower’ Horsepower (maximum).
/// * ‘RPM’ RPM (revs per minute at maximum horsepower).
/// * ‘Rev.per.mile’ Engine revolutions per mile (in highest gear).
/// * ‘Man.trans.avail’ Is a manual transmission version available? (yes
///  or no, Factor).
/// * ‘Fuel.tank.capacity’ Fuel tank capacity (US gallons).
/// * ‘Passengers’ Passenger capacity (persons)
/// * ‘Length’ Length (inches).
/// * ‘Wheelbase’ Wheelbase (inches).
/// * ‘Width’ Width (inches).
/// * ‘Turn.circle’ U-turn space (feet).
/// * ‘Rear.seat.room’ Rear seat room (inches) (missing for 2-seater
///  vehicles).
/// * ‘Luggage.room’ Luggage capacity (cubic feet) (missing for vans).
/// * ‘Weight’ Weight (pounds).
/// * ‘Origin’ Of non-USA or USA company origins? (factor).
/// * ‘Make’ Combination of Manufacturer and Model (character).
///
/// ## Details:
///
/// Cars were selected at random from among 1993 passenger car models
/// that were listed in both the _Consumer Reports_ issue and the
/// _PACE Buying Guide_.  Pickup trucks and Sport/Utility vehicles
/// were eliminated due to incomplete information in the _Consumer
/// Reports_ source.  Duplicate models (e.g., Dodge Shadow and
/// Plymouth Sundance) were listed at most once.
///
/// Further description can be found in Lock (1993).
///
/// ## Source:
///
/// Lock, R. H. (1993) 1993 New Car Data.  _Journal of Statistics
/// Education_ *1*(1).  doi:10.1080/10691898.1993.11910459
/// <https://doi.org/10.1080/10691898.1993.11910459>
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn cars93() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Cars93.csv"))).finish()
}

/// # Anatomical Data from Domestic Cats
///
/// ## Description:
///
/// The heart and body weights of samples of male and female cats used
/// for _digitalis_ experiments.  The cats were all adult, over 2 kg
/// body weight.
///
/// ## Usage:
///
/// cats
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Sex’ sex: Factor with levels ‘"F"’ and ‘"M"’.
/// * ‘Bwt’ body weight in kg.
/// * ‘Hwt’ heart weight in g.
///
/// ## Source:
///
/// R. A. Fisher (1947) The analysis of covariance method for the
/// relation between a part and the whole, _Biometrics_ *3*, 65-68.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn cats() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("cats.csv"))).finish()
}

/// # Heat Evolved by Setting Cements
///
/// ## Description:
///
/// Experiment on the heat evolved in the setting of each of 13
/// cements.
///
/// ## Usage:
///
/// cement
///
/// ## Format:
///
/// * ‘x1, x2, x3, x4’ Proportions (%) of active ingredients.
/// * ‘y’ heat evolved in cals/gm.
///
/// ## Details:
///
/// Thirteen samples of Portland cement were set. For each sample, the
/// percentages of the four main chemical ingredients was accurately
/// measured.  While the cement was setting the amount of heat evolved
/// was also measured.
///
/// ## Source:
///
/// Woods, H., Steinour, H.H. and Starke, H.R. (1932) Effect of
/// composition of Portland cement on heat evolved during hardening.
/// _Industrial Engineering and Chemistry_, *24*, 1207-1214.
///
/// ## References:
///
/// Hald, A. (1957) _Statistical Theory with Engineering
/// Applications._ Wiley, New York.
///
/// ## Examples:
///
/// ```r
/// lm(y ~ x1 + x2 + x3 + x4, cement)
/// ```
pub fn cement() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("cement.csv"))).finish()
}

/// # Copper in Wholemeal Flour
///
/// ## Description:
///
/// A numeric vector of 24 determinations of copper in wholemeal
/// flour, in parts per million.
///
/// ## Usage:
///
/// chem
///
/// ## Source:
///
/// Analytical Methods Committee (1989) Robust statistics - how not to
/// reject outliers. _The Analyst_ *114*, 1693-1702.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn chem() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("chem.csv"))).finish()
}

/// # Co-operative Trial in Analytical Chemistry
///
/// ## Description:
///
/// Seven specimens were sent to 6 laboratories in 3 separate batches
/// and each analysed for Analyte.  Each analysis was duplicated.
///
/// ## Usage:
///
/// coop
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Lab’ Laboratory, ‘L1’, ‘L2’, ..., ‘L6’.
/// * ‘Spc’ Specimen, ‘S1’, ‘S2’, ..., ‘S7’.
/// * ‘Bat’ Batch, ‘B1’, ‘B2’, ‘B3’ (nested within ‘Spc/Lab’),
/// * ‘Conc’ Concentration of Analyte in g/kg.
///
/// ## Source:
///
/// Analytical Methods Committee (1987) Recommendations for the
/// conduct and interpretation of co-operative trials, _The Analyst_
/// *112*, 679-686.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## See Also:
///
/// ‘chem’, ‘abbey’.
pub fn coop() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("coop.csv"))).finish()
}

/// # Performance of Computer CPUs
///
/// ## Description:
///
/// A relative performance measure and characteristics of 209 CPUs.
///
/// ## Usage:
///
/// cpus
///
/// ## Format:
///
/// The components are:
///
/// * ‘name’ manufacturer and model.
/// * ‘syct’ cycle time in nanoseconds.
/// * ‘mmin’ minimum main memory in kilobytes.
/// * ‘mmax’ maximum main memory in kilobytes.
/// * ‘cach’ cache size in kilobytes.
/// * ‘chmin’ minimum number of channels.
/// * ‘chmax’ maximum number of channels.
/// * ‘perf’ published performance on a benchmark mix relative to an IBM
///  370/158-3.
/// * ‘estperf’ estimated performance (by Ein-Dor & Feldmesser).
///
/// ## Source:
///
/// P. Ein-Dor and J. Feldmesser (1987) Attributes of the performance
/// of central processing units: a relative performance prediction
/// model.  _Comm. ACM._ *30*, 308-317.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn cpus() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("cpus.csv"))).finish()
}

const CRABS: &'static str = include_str!("crabs.csv");

/// # Morphological Measurements on Leptograpsus Crabs
///
/// ## Description:
///
/// The ‘crabs’ data frame has 200 rows and 8 columns, describing 5
/// morphological measurements on 50 crabs each of two colour forms
/// and both sexes, of the species _Leptograpsus variegatus_ collected
/// at Fremantle, W. Australia.
///
/// ## Usage:
///
/// crabs
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘sp’ ‘species’ - ‘"B"’ or ‘"O"’ for blue or orange.
/// * ‘sex’ as it says.
/// * ‘index’ index ‘1:50’ within each of the four groups.
/// * ‘FL’ frontal lobe size (mm).
/// * ‘RW’ rear width (mm).
/// * ‘CL’ carapace length (mm).
/// * ‘CW’ carapace width (mm).
/// * ‘BD’ body depth (mm).
///
/// ## Source:
///
/// Campbell, N.A. and Mahon, R.J. (1974) A multivariate study of
/// variation in two species of rock crab of genus _Leptograpsus._
/// _Australian Journal of Zoology_ *22*, 417-425.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn crabs() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(CRABS)).finish()
}

/// # Diagnostic Tests on Patients with Cushing's Syndrome
///
/// ## Description:
///
/// Cushing's syndrome is a hypertensive disorder associated with
/// over-secretion of cortisol by the adrenal gland. The observations
/// are urinary excretion rates of two steroid metabolites.
///
/// ## Usage:
///
/// Cushings
///
/// ## Format:
///
/// The ‘Cushings’ data frame has 27 rows and 3 columns:
///
/// * ‘Tetrahydrocortisone’ urinary excretion rate (mg/24hr) of
///  Tetrahydrocortisone.
/// * ‘Pregnanetriol’ urinary excretion rate (mg/24hr) of Pregnanetriol.
/// * ‘Type’ underlying type of syndrome, coded ‘a’ (adenoma) , ‘b’
///  (bilateral hyperplasia), ‘c’ (carcinoma) or ‘u’ for unknown.
///
/// ## Source:
///
/// J. Aitchison and I. R. Dunsmore (1975) _Statistical Prediction
/// Analysis._ Cambridge University Press, Tables 11.1-3.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn cushings() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Cushings.csv"))).finish()
}

/// # DDT in Kale
///
/// ## Description:
///
/// A numeric vector of 15 measurements by different laboratories of
/// the pesticide DDT in kale, in ppm (parts per million) using the
/// multiple pesticide residue measurement.
///
/// ## Usage:
///
/// DDT
///
/// ## Source:
///
/// C. E. Finsterwalder (1976) Collaborative study of an extension of
/// the Mills _et al_ method for the determination of pesticide
/// residues in food.  _J. Off. Anal. Chem._ *59*, 169-171
///
/// R. G. Staudte and S. J. Sheather (1990) _Robust Estimation and
/// Testing._ Wiley
pub fn ddt() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("DDT.csv"))).finish()
}

/// # Monthly Deaths from Lung Diseases in the UK
///
/// ## Description:
///
/// A time series giving the monthly deaths from bronchitis, emphysema
/// and asthma in the UK, 1974-1979, both sexes (‘deaths’),
///
/// ## Usage:
///
/// deaths
///
/// ## Source:
///
/// P. J. Diggle (1990) _Time Series: A Biostatistical Introduction._
/// Oxford, table A.3
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## See Also:
///
/// This the same as dataset ‘ldeaths’ in R's ‘datasets’ package.
pub fn deaths() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("deaths.csv"))).finish()
}

/// # Deaths of Car Drivers in Great Britain 1969-84
///
/// ## Description:
///
/// A regular time series giving the monthly totals of car drivers in
/// Great Britain killed or seriously injured Jan 1969 to Dec 1984.
/// Compulsory wearing of seat belts was introduced on 31 Jan 1983
///
/// ## Usage:
///
/// drivers
///
/// ## Source:
///
/// Harvey, A.C. (1989) _Forecasting, Structural Time Series Models
/// and the Kalman Filter._ Cambridge University Press, pp. 519-523.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn drivers() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("drivers.csv"))).finish()
}

/// # Foraging Ecology of Bald Eagles
///
/// ## Description:
///
/// Knight and Skagen collected during a field study on the foraging
/// behaviour of wintering Bald Eagles in Washington State, USA data
/// concerning 160 attempts by one (pirating) Bald Eagle to steal a
/// chum salmon from another (feeding) Bald Eagle.
///
/// ## Usage:
///
/// eagles
///
/// ## Format:
///
/// The ‘eagles’ data frame has 8 rows and 5 columns.
///
/// * ‘y’ Number of successful attempts.
/// * ‘n’ Total number of attempts.
/// * ‘P’ Size of pirating eagle (‘L’ = large, ‘S’ = small).
/// * ‘A’ Age of pirating eagle (‘I’ = immature, ‘A’ = adult).
/// * ‘V’ Size of victim eagle (‘L’ = large, ‘S’ = small).
///
/// ## Source:
///
/// Knight, R. L. and Skagen, S. K. (1988) Agonistic asymmetries and
/// the foraging ecology of Bald Eagles.  _Ecology_ *69*, 1188-1194.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
///
/// ## Examples:
///
/// ```r
/// eagles.glm <- glm(cbind(y, n - y) ~ P*A + V, data = eagles,
/// family = binomial)
/// dropterm(eagles.glm)
/// prof <- profile(eagles.glm)
/// plot(prof)
/// pairs(prof)
/// ```
pub fn eagles() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("eagles.csv"))).finish()
}

/// # Seizure Counts for Epileptics
///
/// ## Description:
///
/// Thall and Vail (1990) give a data set on two-week seizure counts
/// for 59 epileptics.  The number of seizures was recorded for a
/// baseline period of 8 weeks, and then patients were randomly
/// assigned to a treatment group or a control group.  Counts were
/// then recorded for four successive two-week periods. The subject's
/// age is the only covariate.
///
/// ## Usage:
///
/// epil
///
/// ## Format:
///
/// This data frame has 236 rows and the following 9 columns:
///
/// * ‘y’ the count for the 2-week period.
/// * ‘trt’ treatment, ‘"placebo"’ or ‘"progabide"’.
/// * ‘base’ the counts in the baseline 8-week period.
/// * ‘age’ subject's age, in years.
/// * ‘V4’ ‘0/1’ indicator variable of period 4.
/// * ‘subject’ subject number, 1 to 59.
/// * ‘period’ period, 1 to 4.
/// * ‘lbase’ log-counts for the baseline period, centred to have zero
///  mean.
/// * ‘lage’ log-ages, centred to have zero mean.
///
/// ## Source:
///
/// Thall, P. F. and Vail, S. C. (1990) Some covariance models for
/// longitudinal count data with over-dispersion.  _Biometrics_ *46*,
/// 657-671.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth Edition. Springer.
///
/// ## Examples:
///
/// ```r
/// ## IGNORE_RDIFF_BEGIN
/// summary(glm(y ~ lbase*trt + lage + V4, family = poisson,
/// data = epil), correlation = FALSE)
/// ## IGNORE_RDIFF_END
/// epil2 <- epil[epil$period == 1, ]
/// epil2["period"] <- rep(0, 59); epil2["y"] <- epil2["base"]
/// epil["time"] <- 1; epil2["time"] <- 4
/// epil2 <- rbind(epil, epil2)
/// epil2$pred <- unclass(epil2$trt) * (epil2$period > 0)
/// epil2$subject <- factor(epil2$subject)
/// epil3 <- aggregate(epil2, list(epil2$subject, epil2$period > 0),
/// function(x) if(is.numeric(x)) sum(x) else x[1])
/// epil3$pred <- factor(epil3$pred,
/// labels = c("base", "placebo", "drug"))
///
/// contrasts(epil3$pred) <- structure(contr.sdif(3),
/// dimnames = list(NULL, c("placebo-base", "drug-placebo")))
/// ## IGNORE_RDIFF_BEGIN
/// summary(glm(y ~ pred + factor(subject) + offset(log(time)),
/// family = poisson, data = epil3), correlation = FALSE)
/// ## IGNORE_RDIFF_END
///
/// summary(glmmPQL(y ~ lbase*trt + lage + V4,
/// random = ~ 1 | subject,
/// family = poisson, data = epil))
/// summary(glmmPQL(y ~ pred, random = ~1 | subject,
/// family = poisson, data = epil3))
/// ```
pub fn epil() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("epil.csv"))).finish()
}

/// # Ecological Factors in Farm Management
///
/// ## Description:
///
/// The ‘farms’ data frame has 20 rows and 4 columns. The rows are
/// farms on the Dutch island of Terschelling and the columns are
/// factors describing the management of grassland.
///
/// ## Usage:
///
/// farms
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Mois’ Five levels of soil moisture - level 3 does not occur at
///  these 20 farms.
/// * ‘Manag’ Grassland management type (‘SF’ = standard, ‘BF’ =
///  biological, ‘HF’ = hobby farming, ‘NM’ = nature
///  conservation).
/// * ‘Use’ Grassland use (‘U1’ = hay production, ‘U2’ = intermediate,
///  ‘U3’ = grazing).
/// * ‘Manure’ Manure usage - classes ‘C0’ to ‘C4’.
///
/// ## Source:
///
/// J.C. Gower and D.J. Hand (1996) _Biplots_. Chapman & Hall, Table
/// 4.6.
///
/// ## Quoted as from:
/// R.H.G. Jongman, C.J.F. ter Braak and O.F.R. van Tongeren (1987)
/// _Data Analysis in Community and Landscape Ecology._ PUDOC,
/// Wageningen.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// farms.mca <- mca(farms, abbrev = TRUE)  # Use levels as names
/// eqscplot(farms.mca$cs, type = "n")
/// text(farms.mca$rs, cex = 0.7)
/// text(farms.mca$cs, labels = dimnames(farms.mca$cs)[[1]], cex = 0.7)
/// ```
pub fn farms() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("farms.csv"))).finish()
}

/// # Measurements of Forensic Glass Fragments
///
/// ## Description:
///
/// The ‘fgl’ data frame has 214 rows and 10 columns. It was collected
/// by B. German on fragments of glass collected in forensic work.
///
/// ## Usage:
///
/// fgl
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘RI’ refractive index; more precisely the refractive index is
///  1.518xxxx.
///
/// The next 8 measurements are percentages by weight of oxides.
///
/// * ‘Na’ sodium.
/// * ‘Mg’ manganese.
/// * ‘Al’ aluminium.
/// * ‘Si’ silicon.
/// * ‘K’ potassium.
/// * ‘Ca’ calcium.
/// * ‘Ba’ barium.
/// * ‘Fe’ iron.
/// * ‘type’ The fragments were originally classed into seven types, one
///  of which was absent in this dataset.  The categories which
///  occur are window float glass (‘WinF’: 70), window non-float
///  glass (‘WinNF’: 76), vehicle window glass (‘Veh’: 17),
///  containers (‘Con’: 13), tableware (‘Tabl’: 9) and vehicle
///  headlamps (‘Head’: 29).
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn fgl() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("fgl.csv"))).finish()
}

/// # Forbes' Data on Boiling Points in the Alps
///
/// ## Description:
///
/// A data frame with 17 observations on boiling point of water and
/// barometric pressure in inches of mercury.
///
/// ## Usage:
///
/// forbes
///
/// ## Format:
///
/// * ‘bp’ boiling point (degrees Farenheit).
/// * ‘pres’ barometric pressure in inches of mercury.
///
/// ## Source:
///
/// A. C. Atkinson (1985) _Plots, Transformations and Regression._
/// Oxford.
///
/// S. Weisberg (1980) _Applied Linear Regression._ Wiley.
pub fn forbes() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("forbes.csv"))).finish()
}

/// # Level of GAG in Urine of Children
///
/// ## Description:
///
/// Data were collected on the concentration of a chemical GAG in the
/// urine of 314 children aged from zero to seventeen years.  The aim
/// of the study was to produce a chart to help a paediatrican to
/// assess if a child's GAG concentration is ‘normal’.
///
/// ## Usage:
///
/// GAGurine
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Age’ age of child in years.
/// * ‘GAG’ concentration of GAG (the units have been lost).
///
/// ## Source:
///
/// Mrs Susan Prosser, Paediatrics Department, University of Oxford,
/// via Department of Statistics Consulting Service.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn gagurine() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("GAGurine.csv"))).finish()
}

/// # Velocities for 82 Galaxies
///
/// ## Description:
///
/// A numeric vector of velocities in km/sec of 82 galaxies from 6
/// well-separated conic sections of an ‘unfilled’ survey of the
/// Corona Borealis region.  Multimodality in such surveys is evidence
/// for voids and superclusters in the far universe.
///
/// ## Usage:
///
/// galaxies
///
/// ## Note:
///
/// There is an 83rd measurement of 5607 km/sec in the Postman _et
/// al._ paper which is omitted in Roeder (1990) and from the dataset
/// here.
///
/// There is also a typo: this dataset has 78th observation 26690
/// which should be 26960.
///
/// ## Source:
///
/// Roeder, K. (1990) Density estimation with confidence sets
/// exemplified by superclusters and voids in galaxies.  _Journal of
/// the American Statistical Association_ *85*, 617-624.
///
/// Postman, M., Huchra, J. P. and Geller, M. J. (1986) Probes of
/// large-scale structures in the Corona Borealis region.
/// _Astronomical Journal_ *92*, 1238-1247.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// gal <- galaxies/1000
/// c(width.SJ(gal, method = "dpi"), width.SJ(gal))
/// plot(x = c(0, 40), y = c(0, 0.3), type = "n", bty = "l",
///  xlab = "velocity of galaxy (1000km/s)", ylab = "density")
/// rug(gal)
/// lines(density(gal, width = 3.25, n = 200), lty = 1)
/// lines(density(gal, width = 2.56, n = 200), lty = 3)
/// ```
pub fn galaxies() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("galaxies.csv"))).finish()
}

/// # Remission Times of Leukaemia Patients
///
/// ## Description:
///
/// A data frame from a trial of 42 leukaemia patients. Some were
/// treated with the drug _6-mercaptopurine_ and the rest are
/// controls.  The trial was designed as matched pairs, both withdrawn
/// from the trial when either came out of remission.
///
/// ## Usage:
///
/// gehan
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘pair’ label for pair.
/// * ‘time’ remission time in weeks.
/// * ‘cens’ censoring, 0/1.
/// * ‘treat’ treatment, control or 6-MP.
///
/// ## Source:
///
/// Cox, D. R. and Oakes, D. (1984) _Analysis of Survival Data._
/// Chapman & Hall, p. 7. Taken from
///
/// Gehan, E.A. (1965) A generalized Wilcoxon test for comparing
/// arbitrarily single-censored samples.  _Biometrika_ *52*, 203-233.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// library(survival)
/// gehan.surv <- survfit(Surv(time, cens) ~ treat, data = gehan,
///  conf.type = "log-log")
/// summary(gehan.surv)
/// survreg(Surv(time, cens) ~ factor(pair) + treat, gehan, dist = "exponential")
/// summary(survreg(Surv(time, cens) ~ treat, gehan, dist = "exponential"))
/// summary(survreg(Surv(time, cens) ~ treat, gehan))
/// gehan.cox <- coxph(Surv(time, cens) ~ treat, gehan)
/// summary(gehan.cox)
/// ```
pub fn gehan() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("gehan.csv"))).finish()
}

/// # Rat Genotype Data
///
/// ## Description:
///
/// Data from a foster feeding experiment with rat mothers and litters
/// of four different genotypes: ‘A’, ‘B’, ‘I’ and ‘J’.  Rat litters
/// were separated from their natural mothers at birth and given to
/// foster mothers to rear.
///
/// ## Usage:
///
/// genotype
///
/// ## Format:
///
/// The data frame has the following components:
///
/// * ‘Litter’ genotype of the litter.
/// * ‘Mother’ genotype of the foster mother.
/// * ‘Wt’ Litter average weight gain of the litter, in grams at age 28
///  days.  (The source states that the within-litter variability
///  is negligible.)
///
/// ## Source:
///
/// Scheffe, H. (1959) _The Analysis of Variance_ Wiley p. 140.
///
/// Bailey, D. W. (1953) _The Inheritance of Maternal Influences on
/// the Growth of the Rat._ Unpublished Ph.D. thesis, University of
/// California. Table B of the Appendix.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn genotype() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("genotype.csv"))).finish()
}

/// # Old Faithful Geyser Data
///
/// # Description:
///
/// A version of the eruptions data from the ‘Old Faithful’ geyser in
/// Yellowstone National Park, Wyoming. This version comes from
/// Azzalini and Bowman (1990) and is of continuous measurement from
/// August 1 to August 15, 1985.
///
/// Some nocturnal duration measurements were coded as 2, 3 or 4
/// minutes, having originally been described as ‘short’, ‘medium’ or
/// ‘long’.
///
/// # Usage:
///
/// geyser
///
/// # Format:
///
/// A data frame with 299 observations on 2 variables.
///
/// * ‘duration’  numeric  Eruption time in mins
/// * ‘waiting’numeric  Waiting time for this eruption
///
/// ## Note:
///
/// The ‘waiting’ time was incorrectly described as the time to the
/// next eruption in the original files, and corrected for ‘MASS’
/// version 7.3-30.
///
/// ## References:
///
/// Azzalini, A. and Bowman, A. W. (1990) A look at some data on the
/// Old Faithful geyser.  _Applied Statistics_ *39*, 357-365.
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## See Also:
///
/// ‘faithful’.
///
/// CRAN package ‘sm’.
pub fn geyser() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("geyser.csv"))).finish()
}

/// # Line Transect of Soil in Gilgai Territory
///
/// ## Description:
///
/// This dataset was collected on a line transect survey in gilgai
/// territory in New South Wales, Australia.  Gilgais are natural
/// gentle depressions in otherwise flat land, and sometimes seem to
/// be regularly distributed. The data collection was stimulated by
/// the question: are these patterns reflected in soil properties?  At
/// each of 365 sampling locations on a linear grid of 4 meters
/// spacing, samples were taken at depths 0-10 cm, 30-40 cm and 80-90
/// cm below the surface. pH, electrical conductivity and chloride
/// content were measured on a 1:5 soil:water extract from each
/// sample.
///
/// ## Usage:
///
/// gilgais
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘pH00’ pH at depth 0-10 cm.
/// * ‘pH30’ pH at depth 30-40 cm.
/// * ‘pH80’ pH at depth 80-90 cm.
/// * ‘e00’ electrical conductivity in mS/cm (0-10 cm).
/// * ‘e30’ electrical conductivity in mS/cm (30-40 cm).
/// * ‘e80’ electrical conductivity in mS/cm (80-90 cm).
/// * ‘c00’ chloride content in ppm (0-10 cm).
/// * ‘c30’ chloride content in ppm (30-40 cm).
/// * ‘c80’ chloride content in ppm (80-90 cm).
///
/// ## Source:
///
/// Webster, R. (1977) Spectral analysis of gilgai soil.  _Australian
/// Journal of Soil Research_ *15*, 191-204.
///
/// Laslett, G. M. (1989) Kriging and splines: An empirical comparison
/// of their predictive performance in some applications (with
/// discussion).  _Journal of the American Statistical Association_
/// *89*, 319-409
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn gilgais() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("gilgais.csv"))).finish()
}

/// # Record Times in Scottish Hill Races
///
/// ## Description:
///
/// The record times in 1984 for 35 Scottish hill races.
///
/// ## Usage:
///
/// hills
///
/// ## Format:
///
/// The components are:
///
/// * ‘dist’ distance in miles (on the map).
/// * ‘climb’ total height gained during the route, in feet.
/// * ‘time’ record time in minutes.
///
/// ## Source:
///
/// A.C. Atkinson (1986) Comment: Aspects of diagnostic regression
/// analysis.  _Statistical Science_ *1*, 397-402.
///
/// [A.C. Atkinson (1988) Transformations unmasked. _Technometrics_
/// *30*, 311-318 “corrects” the time for Knock Hill from 78.65 to
/// 18.65. It is unclear if this based on the original records.]
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn hills() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("hills.csv"))).finish()
}

/// # Frequency Table from a Copenhagen Housing Conditions Survey
///
/// ## Description:
///
/// The ‘housing’ data frame has 72 rows and 5 variables.
///
/// ## Usage:
///
/// housing
///
/// ## Format:
///
/// * ‘Sat’ Satisfaction of householders with their present housing
///  circumstances, (High, Medium or Low, ordered factor).
/// * ‘Infl’ Perceived degree of influence householders have on the
///  management of the property (High, Medium, Low).
/// * ‘Type’ Type of rental accommodation, (Tower, Atrium, Apartment,
///  Terrace).
/// * ‘Cont’ Contact residents are afforded with other residents, (Low,
///  High).
/// * ‘Freq’ Frequencies: the numbers of residents in each class.
///
/// ## Source:
///
/// Madsen, M. (1976) Statistical analysis of multiple contingency
/// tables. Two examples.  _Scand. J. Statist._ *3*, 97-106.
///
/// Cox, D. R. and Snell, E. J. (1984) _Applied Statistics, Principles
/// and Examples_.  Chapman & Hall.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// options(contrasts = c("contr.treatment", "contr.poly"))
///
/// # Surrogate Poisson models
/// house.glm0 <- glm(Freq ~ Infl*Type*Cont + Sat, family = poisson,
/// data = housing)
/// ## IGNORE_RDIFF_BEGIN
/// summary(house.glm0, correlation = FALSE)
/// ## IGNORE_RDIFF_END
///
/// addterm(house.glm0, ~. + Sat:(Infl+Type+Cont), test = "Chisq")
///
/// house.glm1 <- update(house.glm0, . ~ . + Sat*(Infl+Type+Cont))
/// ## IGNORE_RDIFF_BEGIN
/// summary(house.glm1, correlation = FALSE)
/// ## IGNORE_RDIFF_END
///
/// 1 - pchisq(deviance(house.glm1), house.glm1$df.residual)
///
/// dropterm(house.glm1, test = "Chisq")
///
/// addterm(house.glm1, ~. + Sat:(Infl+Type+Cont)^2, test  =  "Chisq")
///
/// hnames <- lapply(housing[, -5], levels) # omit Freq
/// newData <- expand.grid(hnames)
/// newData$Sat <- ordered(newData$Sat)
/// house.pm <- predict(house.glm1, newData,
///  type = "response")  # poisson means
/// house.pm <- matrix(house.pm, ncol = 3, byrow = TRUE,
/// dimnames = list(NULL, hnames[[1]]))
/// house.pr <- house.pm/drop(house.pm %*% rep(1, 3))
/// cbind(expand.grid(hnames[-1]), round(house.pr, 2))
///
/// # Iterative proportional scaling
/// loglm(Freq ~ Infl*Type*Cont + Sat*(Infl+Type+Cont), data = housing)
///
///
/// # multinomial model
/// library(nnet)
/// (house.mult<- multinom(Sat ~ Infl + Type + Cont, weights = Freq,
///  data = housing))
/// house.mult2 <- multinom(Sat ~ Infl*Type*Cont, weights = Freq,
/// data = housing)
/// anova(house.mult, house.mult2)
///
/// house.pm <- predict(house.mult, expand.grid(hnames[-1]), type = "probs")
/// cbind(expand.grid(hnames[-1]), round(house.pm, 2))
///
/// # proportional odds model
/// house.cpr <- apply(house.pr, 1, cumsum)
/// logit <- function(x) log(x/(1-x))
/// house.ld <- logit(house.cpr[2, ]) - logit(house.cpr[1, ])
/// (ratio <- sort(drop(house.ld)))
/// mean(ratio)
///
/// (house.plr <- polr(Sat ~ Infl + Type + Cont,
/// data = housing, weights = Freq))
///
/// house.pr1 <- predict(house.plr, expand.grid(hnames[-1]), type = "probs")
/// cbind(expand.grid(hnames[-1]), round(house.pr1, 2))
///
/// Fr <- matrix(housing$Freq, ncol  =  3, byrow = TRUE)
/// 2*sum(Fr*log(house.pr/house.pr1))
///
/// house.plr2 <- stepAIC(house.plr, ~.^2)
/// house.plr2$anova
/// ```
pub fn housing() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("housing.csv"))).finish()
}

/// # Yields from a Barley Field Trial
///
/// ## Description:
///
/// The ‘immer’ data frame has 30 rows and 4 columns.  Five varieties
/// of barley were grown in six locations in each of 1931 and 1932.
///
/// ## Usage:
///
/// immer
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Loc’ The location.
/// * ‘Var’ The variety of barley (‘"manchuria"’, ‘"svansota"’,
///  ‘"velvet"’, ‘"trebi"’ and ‘"peatland"’).
/// * ‘Y1’ Yield in 1931.
/// * ‘Y2’ Yield in 1932.
///
/// ## Source:
///
/// Immer, F.R., Hayes, H.D. and LeRoy Powers (1934) Statistical
/// determination of barley varietal adaptation.  _Journal of the
/// American Society for Agronomy_ *26*, 403-419.
///
/// Fisher, R.A. (1947) _The Design of Experiments._ 4th edition.
/// Edinburgh: Oliver and Boyd.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
///
/// ## Examples:
///
/// ```r
/// immer.aov <- aov(cbind(Y1,Y2) ~ Loc + Var, data = immer)
/// summary(immer.aov)
///
/// immer.aov <- aov((Y1+Y2)/2 ~ Var + Loc, data = immer)
/// summary(immer.aov)
/// model.tables(immer.aov, type = "means", se = TRUE, cterms = "Var")
/// ```
pub fn immer() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("immer.csv"))).finish()
}

/// # Numbers of Car Insurance claims
///
/// ## Description:
///
/// The data given in data frame ‘Insurance’ consist of the numbers of
/// policyholders of an insurance company who were exposed to risk,
/// and the numbers of car insurance claims made by those
/// policyholders in the third quarter of 1973.
///
/// ## Usage:
///
/// Insurance
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘District’ factor: district of residence of policyholder (1 to 4):
///  4 is major cities.
/// * ‘Group’ an ordered factor: group of car with levels <1 litre,
///  1-1.5 litre, 1.5-2 litre, >2 litre.
/// * ‘Age’ an ordered factor: the age of the insured in 4 groups
///  labelled <25, 25-29, 30-35, >35.
/// * ‘Holders’ numbers of policyholders.
/// * ‘Claims’ numbers of claims
///
/// ## Source:
///
/// L. A. Baxter, S. M. Coutts and G. A. F. Ross (1980) Applications
/// of linear models in motor insurance.  _Proceedings of the 21st
/// International Congress of Actuaries, Zurich_ pp. 11-29.
///
/// M. Aitkin, D. Anderson, B. Francis and J. Hinde (1989)
/// _Statistical Modelling in GLIM._ Oxford University Press.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
///
/// ## Examples:
///
/// ```r
/// ## main-effects fit as Poisson GLM with offset
/// glm(Claims ~ District + Group + Age + offset(log(Holders)),
/// data = Insurance, family = poisson)
///
/// # same via loglm
/// loglm(Claims ~ District + Group + Age + offset(log(Holders)),
/// data = Insurance)
/// ```
pub fn insurance() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Insurance.csv"))).finish()
}

/// # Survival Times and White Blood Counts for Leukaemia Patients
///
/// ## Description:
///
/// A data frame of data from 33 leukaemia patients.
///
/// ## Usage:
///
/// leuk
///
/// ## Format:
///
/// A data frame with columns:
///
/// * ‘wbc’ white blood count.
/// * ‘ag’ a test result, ‘"present"’ or ‘"absent"’.
/// * ‘time’ survival time in weeks.
///
/// ## Details:
///
/// Survival times are given for 33 patients who died from acute
/// myelogenous leukaemia.  Also measured was the patient's white
/// blood cell count at the time of diagnosis.  The patients were also
/// factored into 2 groups according to the presence or absence of a
/// morphologic characteristic of white blood cells. Patients termed
/// AG positive were identified by the presence of Auer rods and/or
/// significant granulation of the leukaemic cells in the bone marrow
/// at the time of diagnosis.
///
/// ## Source:
///
/// Cox, D. R. and Oakes, D. (1984) _Analysis of Survival Data_.
/// Chapman & Hall, p. 9.
///
/// ## Taken from:
///
/// Feigl, P. & Zelen, M. (1965) Estimation of exponential survival
/// probabilities with concomitant information. _Biometrics_ *21*,
/// 826-838.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// library(survival)
/// plot(survfit(Surv(time) ~ ag, data = leuk), lty = 2:3, col = 2:3)
///
/// # now Cox models
/// leuk.cox <- coxph(Surv(time) ~ ag + log(wbc), leuk)
/// summary(leuk.cox)
/// ```
pub fn leuk() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("leuk.csv"))).finish()
}

/// # Brain and Body Weights for 62 Species of Land Mammals
///
/// ## Description:
///
/// A data frame with average brain and body weights for 62 species of
/// land mammals.
///
/// ## Usage:
///
/// mammals
///
/// ## Format:
///
/// * ‘body’ body weight in kg.
/// * ‘brain’ brain weight in g.
/// * ‘name’ Common name of species.  (Rock hyrax-a = _Heterohyrax
///  brucci_, Rock hyrax-b = _Procavia habessinic._.)
///
/// ## Source:
///
/// Weisberg, S. (1985) _Applied Linear Regression._ 2nd edition.
/// Wiley, pp. 144-5.
///
/// Selected from: Allison, T. and Cicchetti, D. V. (1976) Sleep in
/// mammals: ecological and constitutional correlates.  _Science_
/// *194*, 732-734.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn mammals() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("mammals.csv"))).finish()
}

/// # Data from a Simulated Motorcycle Accident
///
/// ## Description:
///
/// A data frame giving a series of measurements of head acceleration
/// in a simulated motorcycle accident, used to test crash helmets.
///
/// ## Usage:
///
/// mcycle
///
/// ## Format:
///
/// * ‘times’ in milliseconds after impact.
/// * ‘accel’ in g.
///
/// ## Source:
///
/// Silverman, B. W. (1985) Some aspects of the spline smoothing
/// approach to non-parametric curve fitting.  _Journal of the Royal
/// Statistical Society series B_ *47*, 1-52.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn mcycle() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("mcycle.csv"))).finish()
}

/// # Survival from Malignant Melanoma
///
/// ## Description:
///
/// The ‘Melanoma’ data frame has data on 205 patients in Denmark with
/// malignant melanoma.
///
/// ## Usage:
///
/// Melanoma
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘time’ survival time in days, possibly censored.
/// * ‘status’ ‘1’ died from melanoma, ‘2’ alive, ‘3’ dead from other
///  causes.
/// * ‘sex’ ‘1’ = male, ‘0’ = female.
/// * ‘age’ age in years.
/// * ‘year’ of operation.
/// * ‘thickness’ tumour thickness in mm.
/// * ‘ulcer’ ‘1’ = presence, ‘0’ = absence.
///
/// ## Source:
///
/// P. K. Andersen, O. Borgan, R. D. Gill and N. Keiding (1993)
/// _Statistical Models based on Counting Processes._ Springer.
pub fn melanoma() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Melanoma.csv"))).finish()
}

/// # Age of Menarche in Warsaw
///
/// ## Description:
///
/// Proportions of female children at various ages during adolescence
/// who have reached menarche.
///
/// ## Usage:
///
/// menarche
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Age’ Average age of the group.  (The groups are reasonably age
///  homogeneous.)
/// * ‘Total’ Total number of children in the group.
/// * ‘Menarche’ Number who have reached menarche.
///
/// ## Source:
///
/// Milicer, H. and Szczotka, F. (1966) Age at Menarche in Warsaw
/// girls in 1965.  _Human Biology_ *38*, 199-203.
///
/// The data are also given in
/// Aranda-Ordaz, F.J. (1981) On two families of transformations to
/// additivity for binary response data.  _Biometrika_ *68*, 357-363.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// mprob <- glm(cbind(Menarche, Total - Menarche) ~ Age,
/// binomial(link = probit), data = menarche)
/// ```
pub fn menarche() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("menarche.csv"))).finish()
}

/// # Michelson's Speed of Light Data
///
/// ## Description:
///
/// Measurements of the speed of light in air, made between 5th June
/// and 2nd July, 1879.  The data consists of five experiments, each
/// consisting of 20 consecutive runs.  The response is the speed of
/// light in km/s, less 299000.  The currently accepted value, on this
/// scale of measurement, is 734.5.
///
/// ## Usage:
///
/// michelson
///
/// ## Format:
///
/// The data frame contains the following components:
///
/// * ‘Expt’ The experiment number, from 1 to 5.
/// * ‘Run’ The run number within each experiment.
/// * ‘Speed’ Speed-of-light measurement.
///
/// ## Source:
///
/// A.J. Weekes (1986) _A Genstat Primer._ Edward Arnold.
///
/// S. M. Stigler (1977) Do robust estimators work with real data?
/// _Annals of Statistics_ *5*, 1055-1098.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn michelson() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("michelson.csv"))).finish()
}

/// # Minnesota High School Graduates of 1938
///
/// ## Description:
///
/// The Minnesota high school graduates of 1938 were classified
/// according to four factors, described below.  The ‘minn38’ data
/// frame has 168 rows and 5 columns.
///
/// ## Usage:
///
/// minn38
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘hs’ high school rank: ‘"L"’, ‘"M"’ and ‘"U"’ for lower, middle
///  and upper third.
/// * ‘phs’ post high school status: Enrolled in college, (‘"C"’),
///  enrolled in non-collegiate school, (‘"N"’), employed
///  full-time, (‘"E"’) and other, (‘"O"’).
/// * ‘fol’ father's occupational level, (seven levels, ‘"F1"’, ‘"F2"’,
///  ..., ‘"F7"’).
/// * ‘sex’ sex: factor with levels‘"F"’ or ‘"M"’.
/// * ‘f’ frequency.
///
/// ## Source:
///
/// From R. L. Plackett, (1974) _The Analysis of Categorical Data._
/// London: Griffin
///
/// who quotes the data from
///
/// Hoyt, C. J., Krishnaiah, P. R. and Torrance, E. P. (1959) Analysis
/// of complex contingency tables, _J. Exp. Ed._ *27*, 187-194.
pub fn minn38() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("minn38.csv"))).finish()
}

/// # Accelerated Life Testing of Motorettes
///
/// ## Description:
///
/// The ‘motors’ data frame has 40 rows and 3 columns.  It describes
/// an accelerated life test at each of four temperatures of 10
/// motorettes, and has rather discrete times.
///
/// ## Usage:
///
/// motors
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘temp’ the temperature (degrees C) of the test.
/// * ‘time’ the time in hours to failure or censoring at 8064 hours (=
///  336 days).
/// * ‘cens’ an indicator variable for death.
///
/// ## Source:
///
/// Kalbfleisch, J. D. and Prentice, R. L. (1980) _The Statistical
/// Analysis of Failure Time Data._ New York: Wiley.
///
/// ## Taken from:
///
/// Nelson, W. D. and Hahn, G. J. (1972) Linear regression of a
/// regression relationship from censored data.  Part 1 - simple
/// methods and their application.  _Technometrics_, *14*, 247-276.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// library(survival)
/// plot(survfit(Surv(time, cens) ~ factor(temp), motors), conf.int = FALSE)
/// # fit Weibull model
/// motor.wei <- survreg(Surv(time, cens) ~ temp, motors)
/// summary(motor.wei)
/// # and predict at 130C
/// unlist(predict(motor.wei, data.frame(temp=130), se.fit = TRUE))
///
/// motor.cox <- coxph(Surv(time, cens) ~ temp, motors)
/// summary(motor.cox)
/// # predict at temperature 200
/// plot(survfit(motor.cox, newdata = data.frame(temp=200),
///  conf.type = "log-log"))
/// summary( survfit(motor.cox, newdata = data.frame(temp=130)) )
/// ```
pub fn motors() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("motors.csv"))).finish()
}

/// # Effect of Calcium Chloride on Muscle Contraction in Rat Hearts
///
/// ## Description:
///
/// The purpose of this experiment was to assess the influence of
/// calcium in solution on the contraction of heart muscle in rats.
/// The left auricle of 21 rat hearts was isolated and on several
/// occasions a constant-length strip of tissue was electrically
/// stimulated and dipped into various concentrations of calcium
/// chloride solution, after which the shortening of the strip was
/// accurately measured as the response.
///
/// ## Usage:
///
/// muscle
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Strip’ which heart muscle strip was used?
/// * ‘Conc’ concentration of calcium chloride solution, in multiples of
///  2.2 mM.
/// * ‘Length’ the change in length (shortening) of the strip,
///  (allegedly) in mm.
///
/// ## Source:
///
/// Linder, A., Chakravarti, I. M. and Vuagnat, P. (1964) Fitting
/// asymptotic regression curves with different asymptotes.  In
/// _Contributions to Statistics. Presented to Professor P. C.
/// Mahalanobis on the occasion of his 70th birthday_, ed. C. R. Rao,
/// pp. 221-228. Oxford: Pergamon Press.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth Edition. Springer.
///
/// ## Examples:
///
/// ```r
/// ## IGNORE_RDIFF_BEGIN
/// A <- model.matrix(~ Strip - 1, data=muscle)
/// rats.nls1 <- nls(log(Length) ~ cbind(A, rho^Conc),
/// data = muscle, start = c(rho=0.1), algorithm="plinear")
/// (B <- coef(rats.nls1))
///
/// st <- list(alpha = B[2:22], beta = B[23], rho = B[1])
/// (rats.nls2 <- nls(log(Length) ~ alpha[Strip] + beta*rho^Conc,
/// data = muscle, start = st))
/// ## IGNORE_RDIFF_END
///
/// Muscle <- with(muscle, {
/// Muscle <- expand.grid(Conc = sort(unique(Conc)), Strip = levels(Strip))
/// Muscle$Yhat <- predict(rats.nls2, Muscle)
/// Muscle <- cbind(Muscle, logLength = rep(as.numeric(NA), 126))
/// ind <- match(paste(Strip, Conc),
/// paste(Muscle$Strip, Muscle$Conc))
/// Muscle$logLength[ind] <- log(Length)
/// Muscle})
///
/// lattice::xyplot(Yhat ~ Conc | Strip, Muscle, as.table = TRUE,
/// ylim = range(c(Muscle$Yhat, Muscle$logLength), na.rm = TRUE),
/// subscripts = TRUE, xlab = "Calcium Chloride concentration (mM)",
/// ylab = "log(Length in mm)", panel =
/// function(x, y, subscripts, ...) {
/// panel.xyplot(x, Muscle$logLength[subscripts], ...)
/// llines(spline(x, y))
/// })
/// ```
pub fn muscle() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("muscle.csv"))).finish()
}

/// # Newcomb's Measurements of the Passage Time of Light
///
/// ## Description:
///
/// A numeric vector giving the ‘Third Series’ of measurements of the
/// passage time of light recorded by Newcomb in 1882.  The given
/// values divided by 1000 plus 24.8 give the time in millionths of a
/// second for light to traverse a known distance. The ‘true’ value is
/// now considered to be 33.02.
///
/// The dataset is given in the order in Staudte and Sheather.
/// Stigler (1977, Table 5) gives the dataset as
///
/// 28 26 33 24 34 -44 27 16 40 -2 29 22 24 21 25 30 23 29 31 19
/// 24 20 36 32 36 28 25 21 28 29 37 25 28 26 30 32 36 26 30 22
/// 36 23 27 27 28 27 31 27 26 33 26 32 32 24 39 28 24 25 32 25
/// 29 27 28 29 16 23
///
/// However, order is not relevant to its use as an example of robust
/// estimation.  (Thanks to Anthony Unwin for bringing this difference
/// to our attention.)
///
/// ## Usage:
///
/// newcomb
///
/// ## Source:
///
/// S. M. Stigler (1973) Simon Newcomb, Percy Daniell, and the history
/// of robust estimation 1885-1920.  _Journal of the American
/// Statistical Association_ *68*, 872-879.
///
/// S. M. Stigler (1977) Do robust estimators work with _real_ data?
/// _Annals of Statistics_, *5*, 1055-1098.
///
/// R. G. Staudte and S. J. Sheather (1990) _Robust Estimation and
/// Testing._ Wiley.
pub fn newcomb() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("newcomb.csv"))).finish()
}

/// # Eighth-Grade Pupils in the Netherlands
///
/// ## Description:
///
/// Snijders and Bosker (1999) use as a running example a study of
/// 2287 eighth-grade pupils (aged about 11) in 132 classes in 131
/// schools in the Netherlands.  Only the variables used in our
/// examples are supplied.
///
/// ## Usage:
///
/// nlschools
///
/// ## Format:
///
/// This data frame contains 2287 rows and the following columns:
///
/// * ‘lang’ language test score.
/// * ‘IQ’ verbal IQ.
/// * ‘class’ class ID.
/// * ‘GS’ class size: number of eighth-grade pupils recorded in the
///  class (there may be others: see ‘COMB’, and some may have
///  been omitted with missing values).
/// * ‘SES’ social-economic status of pupil's family.
/// * ‘COMB’ were the pupils taught in a multi-grade class (‘0/1’)?
///  Classes which contained pupils from grades 7 and 8 are coded
///  ‘1’, but only eighth-graders were tested.
///
/// ## Source:
///
/// Snijders, T. A. B. and Bosker, R. J. (1999) _Multilevel Analysis.
/// An Introduction to Basic and Advanced Multilevel Modelling._
/// London: Sage.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// nl1 <- within(nlschools, {
/// IQave <- tapply(IQ, class, mean)[as.character(class)]
/// IQ <- IQ - IQave
/// })
/// cen <- c("IQ", "IQave", "SES")
/// nl1[cen] <- scale(nl1[cen], center = TRUE, scale = FALSE)
///
/// nl.lme <- nlme::lme(lang ~ IQ*COMB + IQave + SES,
///  random = ~ IQ | class, data = nl1)
/// ## IGNORE_RDIFF_BEGIN
/// summary(nl.lme)
/// ## IGNORE_RDIFF_END
/// ```
pub fn nlschools() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("nlschools.csv"))).finish()
}

// /// # Classical N, P, K Factorial Experiment
// ///
// /// ## Description:
// ///
// /// A classical N, P, K (nitrogen, phosphate, potassium) factorial
// /// experiment on the growth of peas conducted on 6 blocks. Each half
// /// of a fractional factorial design confounding the NPK interaction
// /// was used on 3 of the plots.
// ///
// /// ## Usage:
// ///
// /// npk
// ///
// /// ## Format:
// ///
// /// The ‘npk’ data frame has 24 rows and 5 columns:
// ///
// /// * ‘block’ which block (label 1 to 6).
// /// * ‘N’ indicator (0/1) for the application of nitrogen.
// /// * ‘P’ indicator (0/1) for the application of phosphate.
// /// * ‘K’ indicator (0/1) for the application of potassium.
// /// * ‘yield’ Yield of peas, in pounds/plot (the plots were (1/70)
// ///  acre).
// ///
// /// ## Note:
// ///
// /// This dataset is also contained in R 3.0.2 and later.
// ///
// /// ## Source:
// ///
// /// Imperial College, London, M.Sc. exercise sheet.
// ///
// /// ## References:
// ///
// /// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
// /// Statistics with S._ Fourth edition.  Springer.
// ///
// /// ## Examples:
// ///
// /// ```r
// /// options(contrasts = c("contr.sum", "contr.poly"))
// /// npk.aov <- aov(yield ~ block + N*P*K, npk)
// /// ## IGNORE_RDIFF_BEGIN
// /// npk.aov
// /// summary(npk.aov)
// /// alias(npk.aov)
// /// coef(npk.aov)
// /// options(contrasts = c("contr.treatment", "contr.poly"))
// /// npk.aov1 <- aov(yield ~ block + N + K, data = npk)
// /// summary.lm(npk.aov1)
// /// se.contrast(npk.aov1, list(N=="0", N=="1"), data = npk)
// /// model.tables(npk.aov1, type = "means", se = TRUE)
// /// ## IGNORE_RDIFF_END
// /// ```
// pub fn npk() -> PolarsResult<DataFrame> {
//     CsvReader::new(Cursor::new(include_str!("npk.csv"))).finish()
// }

/// # US Naval Petroleum Reserve No. 1 data
///
/// ## Description:
///
/// Data on the locations, porosity and permeability (a measure of oil
/// flow) on 104 oil wells in the US Naval Petroleum Reserve No. 1 in
/// California.
///
/// ## Usage:
///
/// npr1
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘x’ x coordinates, in miles (origin unspecified)..
/// * ‘y’ y coordinates, in miles.
/// * ‘perm’ permeability in milli-Darcies.
/// * ‘por’ porosity (%).
///
/// ## Source:
///
/// Maher, J.C., Carter, R.D. and Lantz, R.J. (1975) Petroleum geology
/// of Naval Petroleum Reserve No. 1, Elk Hills, Kern County,
/// California.  _USGS Professional Paper_ *912*.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn npr1() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("npr1.csv"))).finish()
}

/// # Data from an Oats Field Trial
///
/// ## Description:
///
/// The yield of oats from a split-plot field trial using three
/// varieties and four levels of manurial treatment.  The experiment
/// was laid out in 6 blocks of 3 main plots, each split into 4
/// sub-plots.  The varieties were applied to the main plots and the
/// manurial treatments to the sub-plots.
///
/// ## Usage:
///
/// oats
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘B’ Blocks, levels I, II, III, IV, V and VI.
/// * ‘V’ Varieties, 3 levels.
/// * ‘N’ Nitrogen (manurial) treatment, levels 0.0cwt, 0.2cwt, 0.4cwt
///  and 0.6cwt, showing the application in cwt/acre.
/// * ‘Y’ Yields in 1/4lbs per sub-plot, each of area 1/80 acre.
///
/// ## Source:
///
/// Yates, F. (1935) Complex experiments, _Journal of the Royal
/// Statistical Society Suppl._ *2*, 181-247.
///
/// Also given in Yates, F. (1970) _Experimental design: Selected
/// papers of Frank Yates, C.B.E, F.R.S._ London: Griffin.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// oats$Nf <- ordered(oats$N, levels = sort(levels(oats$N)))
/// oats.aov <- aov(Y ~ Nf*V + Error(B/V), data = oats, qr = TRUE)
/// ## IGNORE_RDIFF_BEGIN
/// summary(oats.aov)
/// summary(oats.aov, split = list(Nf=list(L=1, Dev=2:3)))
/// ## IGNORE_RDIFF_END
/// par(mfrow = c(1,2), pty = "s")
/// plot(fitted(oats.aov[[4]]), studres(oats.aov[[4]]))
/// abline(h = 0, lty = 2)
/// oats.pr <- proj(oats.aov)
/// qqnorm(oats.pr[[4]][,"Residuals"], ylab = "Stratum 4 residuals")
/// qqline(oats.pr[[4]][,"Residuals"])
///
/// par(mfrow = c(1,1), pty = "m")
/// oats.aov2 <- aov(Y ~ N + V + Error(B/V), data = oats, qr = TRUE)
/// model.tables(oats.aov2, type = "means", se = TRUE)
/// ```
pub fn oats() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("oats.csv"))).finish()
}

/// # Tests of Auditory Perception in Children with OME
///
/// ## Description:
///
/// Experiments were performed on children on their ability to
/// differentiate a signal in broad-band noise. The noise was played
/// from a pair of speakers and a signal was added to just one
/// channel; the subject had to turn his/her head to the channel with
/// the added signal.  The signal was either coherent (the amplitude
/// of the noise was increased for a period) or incoherent
/// (independent noise was added for the same period to form the same
/// increase in power).
///
/// The threshold used in the original analysis was the stimulus
/// loudness needs to get 75% correct responses. Some of the children
/// had suffered from otitis media with effusion (OME).
///
/// ## Usage:
///
/// OME
///
/// ## Format:
///
/// The ‘OME’ data frame has 1129 rows and 7 columns:
///
/// * ‘ID’ Subject ID (1 to 99, with some IDs missing). A few subjects
///  were measured at different ages.
/// * ‘OME’ ‘"low"’ or ‘"high"’ or ‘"N/A"’ (at ages other than 30 and 60
///  months).
/// * ‘Age’ Age of the subject (months).
/// * ‘Loud’ Loudness of stimulus, in decibels.
/// * ‘Noise’ Whether the signal in the stimulus was ‘"coherent"’ or
///  ‘"incoherent"’.
/// * ‘Correct’ Number of correct responses from ‘Trials’ trials.
/// * ‘Trials’ Number of trials performed.
///
/// ## Background:
///
/// The experiment was to study otitis media with effusion (OME), a
/// very common childhood condition where the middle ear space, which
/// is normally air-filled, becomes congested by a fluid.  There is a
/// concomitant fluctuating, conductive hearing loss which can result
/// in various language, cognitive and social deficits.  The term
/// ‘binaural hearing’ is used to describe the listening conditions in
/// which the brain is processing information from both ears at the
/// same time.  The brain computes differences in the intensity and/or
/// timing of signals arriving at each ear which contributes to sound
/// localisation and also to our ability to hear in background noise.
///
/// Some years ago, it was found that children of 7-8 years with a
/// history of significant OME had significantly worse binaural
/// hearing than children without such a history, despite having
/// equivalent sensitivity.  The question remained as to whether it
/// was the timing, the duration, or the degree of severity of the
/// otitis media episodes during critical periods, which affected
/// later binaural hearing.  In an attempt to begin to answer this
/// question, 95 children were monitored for the presence of effusion
/// every month since birth.  On the basis of OME experience in their
/// first two years, the test population was split into one group of
/// high OME prevalence and one of low prevalence.
///
/// ## Source:
///
/// Sarah Hogan, Dept of Physiology, University of Oxford, via Dept of
/// Statistics Consulting Service
///
/// ## Examples:
///
/// ```r
/// # Fit logistic curve from p = 0.5 to p = 1.0
/// fp1 <- deriv(~ 0.5 + 0.5/(1 + exp(-(x-L75)/scal)),
/// c("L75", "scal"),
/// function(x,L75,scal)NULL)
/// nls(Correct/Trials ~ fp1(Loud, L75, scal), data = OME,
/// start = c(L75=45, scal=3))
/// nls(Correct/Trials ~ fp1(Loud, L75, scal),
/// data = OME[OME$Noise == "coherent",],
/// start=c(L75=45, scal=3))
/// nls(Correct/Trials ~ fp1(Loud, L75, scal),
/// data = OME[OME$Noise == "incoherent",],
/// start = c(L75=45, scal=3))
///
/// # individual fits for each experiment
///
/// aa <- factor(OME$Age)
/// ab <- 10*OME$ID + unclass(aa)
/// ac <- unclass(factor(ab))
/// OME$UID <- as.vector(ac)
/// OME$UIDn <- OME$UID + 0.1*(OME$Noise == "incoherent")
/// rm(aa, ab, ac)
/// OMEi <- OME
///
/// library(nlme)
/// fp2 <- deriv(~ 0.5 + 0.5/(1 + exp(-(x-L75)/2)),
/// "L75", function(x,L75) NULL)
/// dec <- getOption("OutDec")
/// options(show.error.messages = FALSE, OutDec=".")
/// OMEi.nls <- nlsList(Correct/Trials ~ fp2(Loud, L75) | UIDn,
/// data = OMEi, start = list(L75=45), control = list(maxiter=100))
/// options(show.error.messages = TRUE, OutDec=dec)
/// tmp <- sapply(OMEi.nls, function(X)
///  {if(is.null(X)) NA else as.vector(coef(X))})
/// OMEif <- data.frame(UID = round(as.numeric((names(tmp)))),
/// Noise = rep(c("coherent", "incoherent"), 110),
/// L75 = as.vector(tmp), stringsAsFactors = TRUE)
/// OMEif$Age <- OME$Age[match(OMEif$UID, OME$UID)]
/// OMEif$OME <- OME$OME[match(OMEif$UID, OME$UID)]
/// OMEif <- OMEif[OMEif$L75 > 30,]
/// summary(lm(L75 ~ Noise/Age, data = OMEif, na.action = na.omit))
/// summary(lm(L75 ~ Noise/(Age + OME), data = OMEif,
///  subset = (Age >= 30 & Age <= 60),
///  na.action = na.omit), correlation = FALSE)
///
/// # Or fit by weighted least squares
/// fpl75 <- deriv(~ sqrt(n)*(r/n - 0.5 - 0.5/(1 + exp(-(x-L75)/scal))),
/// c("L75", "scal"),
/// function(r,n,x,L75,scal) NULL)
/// nls(0 ~ fpl75(Correct, Trials, Loud, L75, scal),
/// data = OME[OME$Noise == "coherent",],
/// start = c(L75=45, scal=3))
/// nls(0 ~ fpl75(Correct, Trials, Loud, L75, scal),
/// data = OME[OME$Noise == "incoherent",],
/// start = c(L75=45, scal=3))
///
/// # Test to see if the curves shift with age
/// fpl75age <- deriv(~sqrt(n)*(r/n -  0.5 - 0.5/(1 +
/// exp(-(x-L75-slope*age)/scal))),
/// c("L75", "slope", "scal"),
/// function(r,n,x,age,L75,slope,scal) NULL)
/// OME.nls1 <-
/// nls(0 ~ fpl75age(Correct, Trials, Loud, Age, L75, slope, scal),
/// data = OME[OME$Noise == "coherent",],
/// start = c(L75=45, slope=0, scal=2))
/// sqrt(diag(vcov(OME.nls1)))
///
/// OME.nls2 <-
/// nls(0 ~ fpl75age(Correct, Trials, Loud, Age, L75, slope, scal),
/// data = OME[OME$Noise == "incoherent",],
/// start = c(L75=45, slope=0, scal=2))
/// sqrt(diag(vcov(OME.nls2)))
///
/// # Now allow random effects by using NLME
/// OMEf <- OME[rep(1:nrow(OME), OME$Trials),]
/// OMEf$Resp <- with(OME, rep(rep(c(1,0), length(Trials)),
///  t(cbind(Correct, Trials-Correct))))
/// OMEf <- OMEf[, -match(c("Correct", "Trials"), names(OMEf))]
///
/// ## Not run:
/// ## these fail in R on most platforms
/// fp2 <- deriv(~ 0.5 + 0.5/(1 + exp(-(x-L75)/exp(lsc))),
/// c("L75", "lsc"),
/// function(x, L75, lsc) NULL)
/// try(summary(nlme(Resp ~ fp2(Loud, L75, lsc),
///  fixed = list(L75 ~ Age, lsc ~ 1),
///  random = L75 + lsc ~ 1 | UID,
///  data = OMEf[OMEf$Noise == "coherent",], method = "ML",
///  start = list(fixed=c(L75=c(48.7, -0.03), lsc=0.24)), verbose = TRUE)))
///
/// try(summary(nlme(Resp ~ fp2(Loud, L75, lsc),
///  fixed = list(L75 ~ Age, lsc ~ 1),
///  random = L75 + lsc ~ 1 | UID,
///  data = OMEf[OMEf$Noise == "incoherent",], method = "ML",
///  start = list(fixed=c(L75=c(41.5, -0.1), lsc=0)), verbose = TRUE)))
/// ## End(Not run)
/// ```
pub fn ome() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("OME.csv"))).finish()
}

/// # The Painter's Data of de Piles
///
/// ## Description:
///
/// The subjective assessment, on a 0 to 20 integer scale, of 54
/// classical painters.  The painters were assessed on four
/// characteristics: composition, drawing, colour and expression.  The
/// data is due to the Eighteenth century art critic, de Piles.
///
/// ## Usage:
///
/// painters
///
/// ## Format:
///
/// The row names of the data frame are the painters. The components
/// are:
///
/// * ‘Composition’ Composition score.
/// * ‘Drawing’ Drawing score.
/// * ‘Colour’ Colour score.
/// * ‘Expression’ Expression score.
/// * ‘School’ The school to which a painter belongs, as indicated by a
///  factor level code as follows: ‘"A"’: Renaissance; ‘"B"’:
///  Mannerist; ‘"C"’: Seicento; ‘"D"’: Venetian; ‘"E"’: Lombard;
///  ‘"F"’: Sixteenth Century; ‘"G"’: Seventeenth Century; ‘"H"’:
///  French.
///
/// ## Source:
///
/// A. J. Weekes (1986) _A Genstat Primer._ Edward Arnold.
///
/// M. Davenport and G. Studdert-Kennedy (1972) The statistical
/// analysis of aesthetic judgement: an exploration.  _Applied
/// Statistics_ *21*, 324-333.
///
/// I. T. Jolliffe (1986) _Principal Component Analysis._ Springer.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn painters() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("painters.csv"))).finish()
}

/// # N. L. Prater's Petrol Refinery Data
///
/// ## Description:
///
/// The yield of a petroleum refining process with four covariates.
/// The crude oil appears to come from only 10 distinct samples.
///
/// These data were originally used by Prater (1956) to build an
/// estimation equation for the yield of the refining process of crude
/// oil to gasoline.
///
/// ## Usage:
///
/// petrol
///
/// ## Format:
///
/// The variables are as follows
///
/// * ‘No’ crude oil sample identification label. (Factor.)
/// * ‘SG’ specific gravity, degrees API.  (Constant within sample.)
/// * ‘VP’ vapour pressure in pounds per square inch. (Constant within
///  sample.)
/// * ‘V10’ volatility of crude; ASTM 10% point. (Constant within
///  sample.)
/// * ‘EP’ desired volatility of gasoline. (The end point.  Varies
///  within sample.)
/// * ‘Y’ yield as a percentage of crude.
///
/// ## Source:
///
/// N. H. Prater (1956) Estimate gasoline yields from crudes.
/// _Petroleum Refiner_ *35*, 236-238.
///
/// This dataset is also given in D. J. Hand, F. Daly, K. McConway, D.
/// Lunn and E. Ostrowski (eds) (1994) _A Handbook of Small Data
/// Sets._ Chapman & Hall.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// library(nlme)
/// Petrol <- petrol
/// Petrol[, 2:5] <- scale(as.matrix(Petrol[, 2:5]), scale = FALSE)
/// pet3.lme <- lme(Y ~ SG + VP + V10 + EP,
/// random = ~ 1 | No, data = Petrol)
/// pet3.lme <- update(pet3.lme, method = "ML")
/// pet4.lme <- update(pet3.lme, fixed. = Y ~ V10 + EP)
/// anova(pet4.lme, pet3.lme)
/// ```
pub fn petrol() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("petrol.csv"))).finish()
}

/// # Belgium Phone Calls 1950-1973
///
/// ## Description:
///
/// A list object with the annual numbers of telephone calls, in
/// Belgium.  The components are:
///
/// * ‘year’ last two digits of the year.
/// * ‘calls’ number of telephone calls made (in millions of calls).
///
/// ## Usage:
///
/// phones
///
/// ## Source:
///
/// P. J. Rousseeuw and A. M. Leroy (1987) _Robust Regression &
/// Outlier Detection._ Wiley.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn phones() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("phones.csv"))).finish()
}

/// # Diabetes in Pima Indian Women
///
/// ## Description:
///
/// A population of women who were at least 21 years old, of Pima
/// Indian heritage and living near Phoenix, Arizona, was tested for
/// diabetes according to World Health Organization criteria.  The
/// data were collected by the US National Institute of Diabetes and
/// Digestive and Kidney Diseases.  We used the 532 complete records
/// after dropping the (mainly missing) data on serum insulin.
///
/// ## Usage:
///
/// Pima.tr
/// Pima.tr2
/// Pima.te
///
/// ## Format:
///
/// These data frames contains the following columns:
///
/// * ‘npreg’ number of pregnancies.
/// * ‘glu’ plasma glucose concentration in an oral glucose tolerance
///  test.
/// * ‘bp’ diastolic blood pressure (mm Hg).
/// * ‘skin’ triceps skin fold thickness (mm).
/// * ‘bmi’ body mass index (weight in kg/(height in m)\^2).
/// * ‘ped’ diabetes pedigree function.
/// * ‘age’ age in years.
/// * ‘type’ ‘Yes’ or ‘No’, for diabetic according to WHO criteria.
///
/// ## Details:
///
/// The training set ‘Pima.tr’ contains a randomly selected set of 200
/// subjects, and ‘Pima.te’ contains the remaining 332 subjects.
/// ‘Pima.tr2’ contains ‘Pima.tr’ plus 100 subjects with missing
/// values in the explanatory variables.
///
/// ## Source:
///
/// Smith, J. W., Everhart, J. E., Dickson, W. C., Knowler, W. C.  and
/// Johannes, R. S. (1988) Using the ADAP learning algorithm to
/// forecast the onset of _diabetes mellitus_.  In _Proceedings of the
/// Symposium on Computer Applications in Medical Care (Washington,
/// 1988),_ ed. R. A. Greenes, pp. 261-265. Los Alamitos, CA: IEEE
/// Computer Society Press.
///
/// Ripley, B.D. (1996) _Pattern Recognition and Neural Networks._
/// Cambridge: Cambridge University Press.
pub fn pima_te() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Pima.te.csv"))).finish()
}

/// # Diabetes in Pima Indian Women
///
/// ## Description:
///
/// A population of women who were at least 21 years old, of Pima
/// Indian heritage and living near Phoenix, Arizona, was tested for
/// diabetes according to World Health Organization criteria.  The
/// data were collected by the US National Institute of Diabetes and
/// Digestive and Kidney Diseases.  We used the 532 complete records
/// after dropping the (mainly missing) data on serum insulin.
///
/// ## Usage:
///
/// Pima.tr
/// Pima.tr2
/// Pima.te
///
/// ## Format:
///
/// These data frames contains the following columns:
///
/// * ‘npreg’ number of pregnancies.
/// * ‘glu’ plasma glucose concentration in an oral glucose tolerance
///  test.
/// * ‘bp’ diastolic blood pressure (mm Hg).
/// * ‘skin’ triceps skin fold thickness (mm).
/// * ‘bmi’ body mass index (weight in kg/(height in m)\^2).
/// * ‘ped’ diabetes pedigree function.
/// * ‘age’ age in years.
/// * ‘type’ ‘Yes’ or ‘No’, for diabetic according to WHO criteria.
///
/// ## Details:
///
/// The training set ‘Pima.tr’ contains a randomly selected set of 200
/// subjects, and ‘Pima.te’ contains the remaining 332 subjects.
/// ‘Pima.tr2’ contains ‘Pima.tr’ plus 100 subjects with missing
/// values in the explanatory variables.
///
/// ## Source:
///
/// Smith, J. W., Everhart, J. E., Dickson, W. C., Knowler, W. C.  and
/// Johannes, R. S. (1988) Using the ADAP learning algorithm to
/// forecast the onset of _diabetes mellitus_.  In _Proceedings of the
/// Symposium on Computer Applications in Medical Care (Washington,
/// 1988),_ ed. R. A. Greenes, pp. 261-265. Los Alamitos, CA: IEEE
/// Computer Society Press.
///
/// Ripley, B.D. (1996) _Pattern Recognition and Neural Networks._
/// Cambridge: Cambridge University Press.
pub fn pima_tr() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Pima.tr.csv"))).finish()
}

/// # Diabetes in Pima Indian Women
///
/// ## Description:
///
/// A population of women who were at least 21 years old, of Pima
/// Indian heritage and living near Phoenix, Arizona, was tested for
/// diabetes according to World Health Organization criteria.  The
/// data were collected by the US National Institute of Diabetes and
/// Digestive and Kidney Diseases.  We used the 532 complete records
/// after dropping the (mainly missing) data on serum insulin.
///
/// ## Usage:
///
/// Pima.tr
/// Pima.tr2
/// Pima.te
///
/// ## Format:
///
/// These data frames contains the following columns:
///
/// * ‘npreg’ number of pregnancies.
/// * ‘glu’ plasma glucose concentration in an oral glucose tolerance
///  test.
/// * ‘bp’ diastolic blood pressure (mm Hg).
/// * ‘skin’ triceps skin fold thickness (mm).
/// * ‘bmi’ body mass index (weight in kg/(height in m)\^2).
/// * ‘ped’ diabetes pedigree function.
/// * ‘age’ age in years.
/// * ‘type’ ‘Yes’ or ‘No’, for diabetic according to WHO criteria.
///
/// ## Details:
///
/// The training set ‘Pima.tr’ contains a randomly selected set of 200
/// subjects, and ‘Pima.te’ contains the remaining 332 subjects.
/// ‘Pima.tr2’ contains ‘Pima.tr’ plus 100 subjects with missing
/// values in the explanatory variables.
///
/// ## Source:
///
/// Smith, J. W., Everhart, J. E., Dickson, W. C., Knowler, W. C.  and
/// Johannes, R. S. (1988) Using the ADAP learning algorithm to
/// forecast the onset of _diabetes mellitus_.  In _Proceedings of the
/// Symposium on Computer Applications in Medical Care (Washington,
/// 1988),_ ed. R. A. Greenes, pp. 261-265. Los Alamitos, CA: IEEE
/// Computer Society Press.
///
/// Ripley, B.D. (1996) _Pattern Recognition and Neural Networks._
/// Cambridge: Cambridge University Press.
pub fn pima_tr2() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Pima.tr2.csv"))).finish()
}

/// # Absenteeism from School in Rural New South Wales
///
/// ## Description:
///
/// The ‘quine’ data frame has 146 rows and 5 columns.  Children from
/// Walgett, New South Wales, Australia, were classified by Culture,
/// Age, Sex and Learner status and the number of days absent from
/// school in a particular school year was recorded.
///
/// ## Usage:
///
/// quine
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘Eth’ ethnic background: Aboriginal or Not, (‘"A"’ or ‘"N"’).
/// * ‘Sex’ sex: factor with levels (‘"F"’ or ‘"M"’).
/// * ‘Age’ age group: Primary (‘"F0"’), or forms ‘"F1,"’ ‘"F2"’ or
///  ‘"F3"’.
/// * ‘Lrn’ learner status: factor with levels Average or Slow learner,
///  (‘"AL"’ or ‘"SL"’).
/// * ‘Days’ days absent from school in the year.
///
/// ## Source:
///
/// S. Quine, quoted in Aitkin, M. (1978) The analysis of unbalanced
/// cross classifications (with discussion).  _Journal of the Royal
/// Statistical Society series A_ *141*, 195-223.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn quine() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("quine.csv"))).finish()
}

/// # Blood Pressure in Rabbits
///
/// ## Description:
///
/// Five rabbits were studied on two occasions, after treatment with
/// saline (control) and after treatment with the 5-HT_3 antagonist
/// MDL 72222.  After each treatment ascending doses of
/// phenylbiguanide were injected intravenously at 10 minute intervals
/// and the responses of mean blood pressure measured.  The goal was
/// to test whether the cardiogenic chemoreflex elicited by
/// phenylbiguanide depends on the activation of 5-HT_3 receptors.
///
/// ## Usage:
///
/// Rabbit
///
/// ## Format:
///
/// This data frame contains 60 rows and the following variables:
///
/// * ‘BPchange’ change in blood pressure relative to the start of the
///  experiment.
/// * ‘Dose’ dose of Phenylbiguanide in micrograms.
/// * ‘Run’ label of run (‘"C1"’ to ‘"C5"’, then ‘"M1"’ to ‘"M5"’).
/// * ‘Treatment’ placebo or the 5-HT_3 antagonist MDL 72222.
/// * ‘Animal’ label of animal used (‘"R1"’ to ‘"R5"’).
///
/// ## Source:
///
/// J. Ludbrook (1994) Repeated measurements and multiple comparisons
/// in cardiovascular research.  _Cardiovascular Research_ *28*,
/// 303-311.
/// [The numerical data are not in the paper but were supplied by
/// Professor Ludbrook]
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn rabbit() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Rabbit.csv"))).finish()
}

/// # Road Accident Deaths in US States
///
/// ## Description:
///
/// A data frame with the annual deaths in road accidents for half the
/// US states.
///
/// ## Usage:
///
/// road
///
/// ## Format:
///
/// Columns are:
///
/// * ‘state’ name.
/// * ‘deaths’ number of deaths.
/// * ‘drivers’ number of drivers (in 10,000s).
/// * ‘popden’ population density in people per square mile.
/// * ‘rural’ length of rural roads, in 1000s of miles.
/// * ‘temp’ average daily maximum temperature in January.
/// * ‘fuel’ fuel consumption in 10,000,000 US gallons per year.
///
/// ## Source:
///
/// Imperial College, London M.Sc. exercise
pub fn road() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("road.csv"))).finish()
}

/// # Numbers of Rotifers by Fluid Density
///
/// ## Description:
///
/// The data give the numbers of rotifers falling out of suspension
/// for different fluid densities. There are two species, ‘pm’
/// _Polyartha major_ and ‘kc’, _Keratella cochlearis_ and for each
/// species the number falling out and the total number are given.
///
/// ## Usage:
///
/// rotifer
///
/// ## Format:
///
/// * ‘density’ specific density of fluid.
/// * ‘pm.y’ number falling out for _P. major_.
/// * ‘pm.total’ total number of _P. major_.
/// * ‘kc.y’ number falling out for _K. cochlearis_.
/// * ‘kc.tot’ total number of _K. cochlearis_.
///
/// ## Source:
///
/// D. Collett (1991) _Modelling Binary Data._ Chapman & Hall. p. 217
pub fn rotifer() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("rotifer.csv"))).finish()
}

/// # Accelerated Testing of Tyre Rubber
///
/// ## Description:
///
/// Data frame from accelerated testing of tyre rubber.
///
/// ## Usage:
///
/// Rubber
///
/// ## Format:
///
/// * ‘loss’ the abrasion loss in gm/hr.
/// * ‘hard’ the hardness in Shore units.
/// * ‘tens’ tensile strength in kg/sq m.
///
/// ## Source:
///
/// O.L. Davies (1947) _Statistical Methods in Research and
/// Production._ Oliver and Boyd, Table 6.1 p. 119.
///
/// O.L. Davies and P.L. Goldsmith (1972) _Statistical Methods in
/// Research and Production._ 4th edition, Longmans, Table 8.1 p. 239.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn rubber() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Rubber.csv"))).finish()
}

/// # Ships Damage Data
///
/// ## Description:
///
/// Data frame giving the number of damage incidents and aggregate
/// months of service by ship type, year of construction, and period
/// of operation.
///
/// ## Usage:
///
/// ships
///
/// ## Format:
///
/// * ‘type’ type: ‘"A"’ to ‘"E"’.
/// * ‘year’ year of construction: 1960-64, 65-69, 70-74, 75-79 (coded
///  as ‘"60"’, ‘"65"’, ‘"70"’, ‘"75"’).
/// * ‘period’ period of operation : 1960-74, 75-79.
/// * ‘service’ aggregate months of service.
/// * ‘incidents’ number of damage incidents.
///
/// ## Source:
///
/// P. McCullagh and J. A. Nelder, (1983), _Generalized Linear
/// Models._ Chapman & Hall, section 6.3.2, page 137
pub fn ships() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("ships.csv"))).finish()
}

/// # Shoe wear data of Box, Hunter and Hunter
///
/// ## Description:
///
/// A list of two vectors, giving the wear of shoes of materials A and
/// B for one foot each of ten boys.
///
/// ## Usage:
///
/// shoes
///
/// ## Source:
///
/// G. E. P. Box, W. G. Hunter and J. S. Hunter (1978) _Statistics for
/// Experimenters._ Wiley, p. 100
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn shoes() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("shoes.csv"))).finish()
}

/// # Percentage of Shrimp in Shrimp Cocktail
///
/// ## Description:
///
/// A numeric vector with 18 determinations by different laboratories
/// of the amount (percentage of the declared total weight) of shrimp
/// in shrimp cocktail.
///
/// ## Usage:
///
/// shrimp
///
/// ## Source:
///
/// F. J. King and J. J. Ryan (1976) Collaborative study of the
/// determination of the amount of shrimp in shrimp cocktail. _J. Off.
/// Anal. Chem._ *59*, 644-649.
///
/// R. G. Staudte and S. J. Sheather (1990) _Robust Estimation and
/// Testing._ Wiley.
pub fn shrimp() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("shrimp.csv"))).finish()
}

/// # Space Shuttle Autolander Problem
///
/// ## Description:
///
/// The ‘shuttle’ data frame has 256 rows and 7 columns.  The first
/// six columns are categorical variables giving example conditions;
/// the seventh is the decision.  The first 253 rows are the training
/// set, the last 3 the test conditions.
///
/// ## Usage:
///
/// shuttle
///
/// ## Format:
///
/// This data frame contains the following factor columns:
///
/// * ‘stability’ stable positioning or not (‘stab’ / ‘xstab’).
/// * ‘error’ size of error (‘MM’ / ‘SS’ / ‘LX’ / ‘XL’).
/// * ‘sign’ sign of error, positive or negative (‘pp’ / ‘nn’).
/// * ‘wind’ wind sign (‘head’ / ‘tail’).
/// * ‘magn’ wind strength (‘Light’ / ‘Medium’ / ‘Strong’ / ‘Out of
///  Range’).
/// * ‘vis’ visibility (‘yes’ / ‘no’).
/// * ‘use’ use the autolander or not. (‘auto’ / ‘noauto’.)
///
/// ## Source:
///
/// D. Michie (1989) Problems of computer-aided concept formation. In
/// _Applications of Expert Systems 2_, ed. J. R. Quinlan, Turing
/// Institute Press / Addison-Wesley, pp. 310-333.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn shuttle() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("shuttle.csv"))).finish()
}

/// # Growth Curves for Sitka Spruce Trees in 1988
///
/// ## Description:
///
/// The ‘Sitka’ data frame has 395 rows and 4 columns.  It gives
/// repeated measurements on the log-size of 79 Sitka spruce trees, 54
/// of which were grown in ozone-enriched chambers and 25 were
/// controls.  The size was measured five times in 1988, at roughly
/// monthly intervals.
///
/// ## Usage:
///
/// Sitka
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘size’ measured size (height times diameter squared) of tree, on
///  log scale.
/// * ‘Time’ time of measurement in days since 1 January 1988.
/// * ‘tree’ number of tree.
/// * ‘treat’ either ‘"ozone"’ for an ozone-enriched chamber or
///  ‘"control"’.
///
/// ## Source:
///
/// P. J. Diggle, K.-Y. Liang and S. L. Zeger (1994) _Analysis of
/// Longitudinal Data._ Clarendon Press, Oxford
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## See Also:
///
/// ‘Sitka89’.
pub fn sitka() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Sitka.csv"))).finish()
}

/// # Growth Curves for Sitka Spruce Trees in 1989
///
/// ## Description:
///
/// The ‘Sitka89’ data frame has 632 rows and 4 columns.  It gives
/// repeated measurements on the log-size of 79 Sitka spruce trees, 54
/// of which were grown in ozone-enriched chambers and 25 were
/// controls.  The size was measured eight times in 1989, at roughly
/// monthly intervals.
///
/// ## Usage:
///
/// Sitka89
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘size’ measured size (height times diameter squared) of tree, on
///  log scale.
/// * ‘Time’ time of measurement in days since 1 January 1988.
/// * ‘tree’ number of tree.
/// * ‘treat’ either ‘"ozone"’ for an ozone-enriched chamber or
///  ‘"control"’.
///
/// ## Source:
///
/// P. J. Diggle, K.-Y. Liang and S. L. Zeger (1994) _Analysis of
/// Longitudinal Data._ Clarendon Press, Oxford
///
/// ## See Also:
///
/// ‘Sitka’
pub fn sitka89() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Sitka89.csv"))).finish()
}

/// # AFM Compositions of Aphyric Skye Lavas
///
/// ## Description:
///
/// The ‘Skye’ data frame has 23 rows and 3 columns.
///
/// ## Usage:
///
/// Skye
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘A’ Percentage of sodium and potassium oxides.
/// * ‘F’ Percentage of iron oxide.
/// * ‘M’ Percentage of magnesium oxide.
///
/// ## Source:
///
/// R. N. Thompson, J. Esson and A. C. Duncan (1972) Major element
/// chemical variation in the Eocene lavas of the Isle of Skye. _J.
/// Petrology_, *13*, 219-253.
///
/// ## References:
///
/// J. Aitchison (1986) _The Statistical Analysis of Compositional
/// Data._ Chapman and Hall, p.360.
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// # ternary() is from the on-line answers.
/// ternary <- function(X, pch = par("pch"), lcex = 1,
///  add = FALSE, ord = 1:3, ...)
/// {
///  X <- as.matrix(X)
///  if(any(X < 0)) stop("X must be non-negative")
///  s <- drop(X %*% rep(1, ncol(X)))
///  if(any(s<=0)) stop("each row of X must have a positive sum")
///  if(max(abs(s-1)) > 1e-6) {
/// warning("row(s) of X will be rescaled")
/// X <- X / s
///  }
///  X <- X[, ord]
///  s3 <- sqrt(1/3)
///  if(!add)
///  {
/// oldpty <- par("pty")
/// on.exit(par(pty=oldpty))
/// par(pty="s")
/// plot(c(-s3, s3), c(0.5-s3, 0.5+s3), type="n", axes=FALSE,
/// xlab="", ylab="")
/// polygon(c(0, -s3, s3), c(1, 0, 0), density=0)
/// lab <- NULL
/// if(!is.null(dn <- dimnames(X))) lab <- dn[[2]]
/// if(length(lab) < 3) lab <- as.character(1:3)
/// eps <- 0.05 * lcex
/// text(c(0, s3+eps*0.7, -s3-eps*0.7),
/// c(1+eps, -0.1*eps, -0.1*eps), lab, cex=lcex)
///  }
///  points((X[,2] - X[,3])*s3, X[,1], ...)
/// }
///
/// ternary(Skye/100, ord=c(1,3,2))
/// ```
pub fn skye() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Skye.csv"))).finish()
}

/// # Snail Mortality Data
///
/// ## Description:
///
/// Groups of 20 snails were held for periods of 1, 2, 3 or 4 weeks in
/// carefully controlled conditions of temperature and relative
/// humidity.  There were two species of snail, A and B, and the
/// experiment was designed as a 4 by 3 by 4 by 2 completely
/// randomized design.  At the end of the exposure time the snails
/// were tested to see if they had survived; the process itself is
/// fatal for the animals.  The object of the exercise was to model
/// the probability of survival in terms of the stimulus variables,
/// and in particular to test for differences between species.
///
/// The data are unusual in that in most cases fatalities during the
/// experiment were fairly small.
///
/// ## Usage:
///
/// snails
///
/// ## Format:
///
/// The data frame contains the following components:
///
/// * ‘Species’ snail species A (‘1’) or B (‘2’).
/// * ‘Exposure’ exposure in weeks.
/// * ‘Rel.Hum’ relative humidity (4 levels).
/// * ‘Temp’ temperature, in degrees Celsius (3 levels).
/// * ‘Deaths’ number of deaths.
/// * ‘N’ number of snails exposed.
///
/// ## Source:
///
/// Zoology Department, The University of Adelaide.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn snails() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("snails.csv"))).finish()
}

/// # Returns of the Standard and Poors 500
///
/// ## Description:
///
/// Returns of the Standard and Poors 500 Index in the 1990's
///
/// ## Usage:
///
/// SP500
///
/// ## Format:
///
/// A vector of returns of the Standard and Poors 500 index for all
/// the trading days in 1990, 1991, ..., 1999.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn sp500() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("SP500.csv"))).finish()
}

/// # The Saturated Steam Pressure Data
///
/// ## Description:
///
/// Temperature and pressure in a saturated steam driven experimental
/// device.
///
/// ## Usage:
///
/// steam
///
/// ## Format:
///
/// The data frame contains the following components:
///
/// * ‘Temp’ temperature, in degrees Celsius.
/// * ‘Press’ pressure, in Pascals.
///
/// ## Source:
///
/// N.R. Draper and H. Smith (1981) _Applied Regression Analysis._
/// Wiley, pp. 518-9.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn steam() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("steam.csv"))).finish()
}

const STORMER: &'static str = include_str!("stormer.csv");

/// # The Stormer Viscometer Data
///
/// ## Description:
///
/// The stormer viscometer measures the viscosity of a fluid by
/// measuring the time taken for an inner cylinder in the mechanism to
/// perform a fixed number of revolutions in response to an actuating
/// weight.  The viscometer is calibrated by measuring the time taken
/// with varying weights while the mechanism is suspended in fluids of
/// accurately known viscosity.  The data comes from such a
/// calibration, and theoretical considerations suggest a nonlinear
/// relationship between time, weight and viscosity, of the form ‘Time
/// = (B1*Viscosity)/(Weight - B2) + E’ where ‘B1’ and ‘B2’ are
/// unknown parameters to be estimated, and ‘E’ is error.
///
/// ## Usage:
///
/// stormer
///
/// ## Format:
///
/// The data frame contains the following components:
///
/// * ‘Viscosity’ viscosity of fluid.
/// * ‘Wt’ actuating weight.
/// * ‘Time’ time taken.
///
/// ## Source:
///
/// E. J. Williams (1959) _Regression Analysis._ Wiley.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn stormer() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(STORMER)).finish()
}

/// # Student Survey Data
///
/// ## Description:
///
/// This data frame contains the responses of 237 Statistics I
/// students at the University of Adelaide to a number of questions.
///
/// ## Usage:
///
/// survey
///
/// ## Format:
///
/// The components of the data frame are:
///
/// * ‘Sex’ The sex of the student. (Factor with levels ‘"Male"’ and
///  ‘"Female"’.)
/// * ‘Wr.Hnd’ span (distance from tip of thumb to tip of little finger
///  of spread hand) of writing hand, in centimetres.
/// * ‘NW.Hnd’ span of non-writing hand.
/// * ‘W.Hnd’ writing hand of student. (Factor, with levels ‘"Left"’ and
///  ‘"Right"’.)
/// * ‘Fold’ “Fold your arms! Which is on top” (Factor, with levels ‘"R
///  on L"’, ‘"L on R"’, ‘"Neither"’.)
/// * ‘Pulse’ pulse rate of student (beats per minute).
/// * ‘Clap’ ‘Clap your hands!  Which hand is on top?’ (Factor, with
///  levels ‘"Right"’, ‘"Left"’, ‘"Neither"’.)
/// * ‘Exer’ how often the student exercises. (Factor, with levels
///  ‘"Freq"’ (frequently), ‘"Some"’, ‘"None"’.)
/// * ‘Smoke’ how much the student smokes. (Factor, levels ‘"Heavy"’,
///  ‘"Regul"’ (regularly), ‘"Occas"’ (occasionally), ‘"Never"’.)
/// * ‘Height’ height of the student in centimetres.
/// * ‘M.I’ whether the student expressed height in imperial
///  (feet/inches) or metric (centimetres/metres) units. (Factor,
///  levels ‘"Metric"’, ‘"Imperial"’.)
/// * ‘Age’ age of the student in years.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn survey() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("survey.csv"))).finish()
}

/// # Synthetic Classification Problem
///
/// ## Description:
///
/// The ‘synth.tr’ data frame has 250 rows and 3 columns. The
/// ‘synth.te’ data frame has 100 rows and 3 columns. It is intended
/// that ‘synth.tr’ be used from training and ‘synth.te’ for testing.
///
/// ## Usage:
///
/// synth.tr
/// synth.te
///
/// ## Format:
///
/// These data frames contains the following columns:
///
/// * ‘xs’ x-coordinate
/// * ‘ys’ y-coordinate
/// * ‘yc’ class, coded as 0 or 1.
///
/// ## Source:
///
/// Ripley, B.D. (1994) Neural networks and related methods for
/// classification (with discussion). _Journal of the Royal
/// Statistical Society series B_ *56*, 409-456.
///
/// Ripley, B.D. (1996) _Pattern Recognition and Neural Networks._
/// Cambridge: Cambridge University Press.
pub fn synth_te() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("synth.te.csv"))).finish()
}

/// # Synthetic Classification Problem
///
/// ## Description:
///
/// The ‘synth.tr’ data frame has 250 rows and 3 columns. The
/// ‘synth.te’ data frame has 100 rows and 3 columns. It is intended
/// that ‘synth.tr’ be used from training and ‘synth.te’ for testing.
///
/// ## Usage:
///
/// synth.tr
/// synth.te
///
/// ## Format:
///
/// These data frames contains the following columns:
///
/// * ‘xs’ x-coordinate
/// * ‘ys’ y-coordinate
/// * ‘yc’ class, coded as 0 or 1.
///
/// ## Source:
///
/// Ripley, B.D. (1994) Neural networks and related methods for
/// classification (with discussion). _Journal of the Royal
/// Statistical Society series B_ *56*, 409-456.
///
/// Ripley, B.D. (1996) _Pattern Recognition and Neural Networks._
/// Cambridge: Cambridge University Press.
pub fn synth_tr() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("synth.tr.csv"))).finish()
}

/// # Spatial Topographic Data
///
/// ## Description:
///
/// The ‘topo’ data frame has 52 rows and 3 columns, of topographic
/// heights within a 310 feet square.
///
/// ## Usage:
///
/// topo
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘x’ x coordinates (units of 50 feet)
/// * ‘y’ y coordinates (units of 50 feet)
/// * ‘z’ heights (feet)
///
/// ## Source:
///
/// Davis, J.C. (1973) _Statistics and Data Analysis in Geology._
/// Wiley.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn topo() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("topo.csv"))).finish()
}

/// # Effect of Swedish Speed Limits on Accidents
///
/// ## Description:
///
/// An experiment was performed in Sweden in 1961-2 to assess the
/// effect of a speed limit on the motorway accident rate.  The
/// experiment was conducted on 92 days in each year, matched so that
/// day ‘j’ in 1962 was comparable to day ‘j’ in 1961.  On some days
/// the speed limit was in effect and enforced, while on other days
/// there was no speed limit and cars tended to be driven faster.  The
/// speed limit days tended to be in contiguous blocks.
///
/// ## Usage:
///
/// Traffic
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘year’ 1961 or 1962.
/// * ‘day’ of year.
/// * ‘limit’ was there a speed limit?
/// * ‘y’ traffic accident count for that day.
///
/// ## Source:
///
/// Svensson, A. (1981) On the goodness-of-fit test for the
/// multiplicative Poisson model.  _Annals of Statistics,_ *9*,
/// 697-704.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn traffic() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("Traffic.csv"))).finish()
}

/// # Nutritional and Marketing Information on US Cereals
///
/// ## Description:
///
/// The ‘UScereal’ data frame has 65 rows and 11 columns.  The data
/// come from the 1993 ASA Statistical Graphics Exposition, and are
/// taken from the mandatory F&DA food label. The data have been
/// normalized here to a portion of one American cup.
///
/// ## Usage:
///
/// UScereal
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘mfr’ Manufacturer, represented by its first initial: G=General
///  Mills, K=Kelloggs, N=Nabisco, P=Post, Q=Quaker Oats,
///  R=Ralston Purina.
/// * ‘calories’ number of calories in one portion.
/// * ‘protein’ grams of protein in one portion.
/// * ‘fat’ grams of fat in one portion.
/// * ‘sodium’ milligrams of sodium in one portion.
/// * ‘fibre’ grams of dietary fibre in one portion.
/// * ‘carbo’ grams of complex carbohydrates in one portion.
/// * ‘sugars’ grams of sugars in one portion.
/// * ‘shelf’ display shelf (1, 2, or 3, counting from the floor).
/// * ‘potassium’ grams of potassium.
/// * ‘vitamins’ vitamins and minerals (none, enriched, or 100%).
///
/// ## Source:
///
/// The original data are available at
///<http://lib.stat.cmu.edu/datasets/1993.expo/>.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn uscereal() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("UScereal.csv"))).finish()
}

/// # The Effect of Punishment Regimes on Crime Rates
///
/// ## Description:
///
/// Criminologists are interested in the effect of punishment regimes
/// on crime rates.  This has been studied using aggregate data on 47
/// states of the USA for 1960 given in this data frame.  The
/// variables seem to have been re-scaled to convenient numbers.
///
/// ## Usage:
///
/// UScrime
///
/// ## Format:
///
/// This data frame contains the following columns:
///
/// * ‘M’ percentage of males aged 14-24.
/// * ‘So’ indicator variable for a Southern state.
/// * ‘Ed’ mean years of schooling.
/// * ‘Po1’ police expenditure in 1960.
/// * ‘Po2’ police expenditure in 1959.
/// * ‘LF’ labour force participation rate.
/// * ‘M.F’ number of males per 1000 females.
/// * ‘Pop’ state population.
/// * ‘NW’ number of non-whites per 1000 people.
/// * ‘U1’ unemployment rate of urban males 14-24.
/// * ‘U2’ unemployment rate of urban males 35-39.
/// * ‘GDP’ gross domestic product per head.
/// * ‘Ineq’ income inequality.
/// * ‘Prob’ probability of imprisonment.
/// * ‘Time’ average time served in state prisons.
/// * ‘y’ rate of crimes in a particular category per head of
///  population.
///
/// ## Source:
///
/// Ehrlich, I. (1973) Participation in illegitimate activities: a
/// theoretical and empirical investigation.  _Journal of Political
/// Economy_, *81*, 521-565.
///
/// Vandaele, W. (1978) Participation in illegitimate activities:
/// Ehrlich revisited.  In _Deterrence and Incapacitation_, eds A.
/// Blumstein, J. Cohen and D. Nagin, pp. 270-335.  US National
/// Academy of Sciences.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S-PLUS._ Fourth Edition. Springer.
pub fn uscrime() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("UScrime.csv"))).finish()
}

/// # Veteran's Administration Lung Cancer Trial
///
/// ## Description:
///
/// Veteran's Administration lung cancer trial from Kalbfleisch &
/// Prentice.
///
/// ## Usage:
///
/// VA
///
/// ## Format:
///
/// A data frame with columns:
///
/// * ‘stime’ survival or follow-up time in days.
/// * ‘status’ dead or censored.
/// * ‘treat’ treatment: standard or test.
/// * ‘age’ patient's age in years.
/// * ‘Karn’ Karnofsky score of patient's performance on a scale of 0 to
///  100.
/// * ‘diag.time’ times since diagnosis in months at entry to trial.
/// * ‘cell’ one of four cell types.
/// * ‘prior’ prior therapy?
///
/// ## Source:
///
/// Kalbfleisch, J.D. and Prentice R.L. (1980) _The Statistical
/// Analysis of Failure Time Data._ Wiley.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
pub fn va() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("VA.csv"))).finish()
}

/// # Counts of Waders at 15 Sites in South Africa
///
/// ## Description:
///
/// The ‘waders’ data frame has 15 rows and 19 columns.  The entries
/// are counts of waders in summer.
///
/// ## Usage:
///
/// waders
///
/// ## Format:
///
/// This data frame contains the following columns (species)
///
/// * ‘S1’ Oystercatcher
/// * ‘S2’ White-fronted Plover
/// * ‘S3’ Kitt Lutz's Plover
/// * ‘S4’ Three-banded Plover
/// * ‘S5’ Grey Plover
/// * ‘S6’ Ringed Plover
/// * ‘S7’ Bar-tailed Godwit
/// * ‘S8’ Whimbrel
/// * ‘S9’ Marsh Sandpiper
/// * ‘S10’ Greenshank
/// * ‘S11’ Common Sandpiper
/// * ‘S12’ Turnstone
/// * ‘S13’ Knot
/// * ‘S14’ Sanderling
/// * ‘S15’ Little Stint
/// * ‘S16’ Curlew Sandpiper
/// * ‘S17’ Ruff
/// * ‘S18’ Avocet
/// * ‘S19’ Black-winged Stilt
///
/// The rows are the sites:
///
/// * A = Namibia North coast
/// * B = Namibia North wetland
/// * C = Namibia South coast
/// * D = Namibia South wetland
/// * E = Cape North coast
/// * F = Cape North wetland
/// * G = Cape West coast
/// * H = Cape West wetland
/// * I = Cape South coast
/// * J = Cape South wetland
/// * K = Cape East coast
/// * L = Cape East wetland
/// * M = Transkei coast
/// * N = Natal coast
/// * O = Natal wetland
///
/// ## Source:
///
/// J.C. Gower and D.J. Hand (1996) _Biplots_ Chapman & Hall Table
/// 9.1. Quoted as from:
///
/// R.W. Summers, L.G. Underhill, D.J. Pearson and D.A. Scott (1987)
/// Wader migration systems in south and eastern Africa and western
/// Asia.  _Wader Study Group Bulletin_ *49* Supplement, 15-34.
///
/// ## Examples:
///
/// plot(corresp(waders, nf=2))
pub fn waders() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("waders.csv"))).finish()
}

/// # House Insulation: Whiteside's Data
///
/// ## Description:
///
/// Mr Derek Whiteside of the UK Building Research Station recorded
/// the weekly gas consumption and average external temperature at his
/// own house in south-east England for two heating seasons, one of 26
/// weeks before, and one of 30 weeks after cavity-wall insulation was
/// installed. The object of the exercise was to assess the effect of
/// the insulation on gas consumption.
///
/// ## Usage:
///
/// whiteside
///
/// ## Format:
///
/// The ‘whiteside’ data frame has 56 rows and 3 columns.:
///
/// * ‘Insul’ A factor, before or after insulation.
/// * ‘Temp’ Purportedly the average outside temperature in degrees
///  Celsius. (These values is far too low for any 56-week period
///  in the 1960s in South-East England. It might be the weekly
///  average of daily minima.)
/// * ‘Gas’ The weekly gas consumption in 1000s of cubic feet.
///
/// ## Source:
///
/// A data set collected in the 1960s by Mr Derek Whiteside of the UK
/// Building Research Station. Reported by
///
/// Hand, D. J., Daly, F., McConway, K., Lunn, D. and Ostrowski, E.
/// eds (1993) _A Handbook of Small Data Sets._ Chapman & Hall, p. 69.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// require(lattice)
/// xyplot(Gas ~ Temp | Insul, whiteside, panel =
///  function(x, y, ...) {
/// panel.xyplot(x, y, ...)
/// panel.lmline(x, y, ...)
///  }, xlab = "Average external temperature (deg. C)",
///  ylab = "Gas consumption  (1000 cubic feet)", aspect = "xy",
///  strip = function(...) strip.default(..., style = 1))
///
/// gasB <- lm(Gas ~ Temp, whiteside, subset = Insul=="Before")
/// gasA <- update(gasB, subset = Insul=="After")
/// summary(gasB)
/// summary(gasA)
/// gasBA <- lm(Gas ~ Insul/Temp - 1, whiteside)
/// summary(gasBA)
///
/// gasQ <- lm(Gas ~ Insul/(Temp + I(Temp^2)) - 1, whiteside)
/// coef(summary(gasQ))
///
/// gasPR <- lm(Gas ~ Insul + Temp, whiteside)
/// anova(gasPR, gasBA)
/// options(contrasts = c("contr.treatment", "contr.poly"))
/// gasBA1 <- lm(Gas ~ Insul*Temp, whiteside)
/// coef(summary(gasBA1))
/// ```
pub fn whiteside() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("whiteside.csv"))).finish()
}

/// # Weight Loss Data from an Obese Patient
///
/// # Description:
///
/// The data frame gives the weight, in kilograms, of an obese patient
/// at 52 time points over an 8 month period of a weight
/// rehabilitation programme.
///
/// # Usage:
///
/// wtloss
///
/// # Format:
///
/// This data frame contains the following columns:
///
/// * ‘Days’ time in days since the start of the programme.
/// * ‘Weight’ weight in kilograms of the patient.
///
/// ## Source:
///
/// Dr T. Davies, Adelaide.
///
/// ## References:
///
/// Venables, W. N. and Ripley, B. D. (2002) _Modern Applied
/// Statistics with S._ Fourth edition.  Springer.
///
/// ## Examples:
///
/// ```r
/// ## IGNORE_RDIFF_BEGIN
/// wtloss.fm <- nls(Weight ~ b0 + b1*2^(-Days/th),
/// data = wtloss, start = list(b0=90, b1=95, th=120))
/// wtloss.fm
/// ## IGNORE_RDIFF_END
/// plot(wtloss)
/// with(wtloss, lines(Days, fitted(wtloss.fm)))
/// ```
pub fn wtloss() -> PolarsResult<DataFrame> {
    CsvReader::new(Cursor::new(include_str!("wtloss.csv"))).finish()
}
