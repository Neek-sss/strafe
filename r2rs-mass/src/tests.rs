// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use nalgebra::DMatrix;
use num_traits::ToPrimitive;
use polars::frame::DataFrame;
use strafe_testing::{
    r::{RGenerator, RString, RTester},
    r_assert_relative_equal,
};
use strafe_trait::{Model, ModelBuilder};
use strafe_type::ModelMatrix;

use crate::{
    func::{area, bandwidth_nrd, bcv, corresp},
    robust_linear_model::{bisquare, hampel, huber, RobustLinearRegressionBuilder},
};

pub fn psi_huber_inner(u: &[f64], k: f64, deriv: bool) {
    let library = RString::from_library_name("MASS");

    let r_val = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "u={};k={};deriv={};",
            RString::from_f64_slice(&u),
            RString::from_f64(k),
            RString::from_bool(deriv)
        )))
        .set_display(&RString::from_str("as.double(psi.huber(u, k, deriv))").unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R error");

    let rust_val = huber(u, k, deriv);

    for (r_ret, rust_ret) in r_val.into_iter().zip(rust_val.into_iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn psi_hampel_inner(u: &[f64], a: f64, b: f64, c: f64, deriv: bool) {
    let library = RString::from_library_name("MASS");

    let r_val = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "u={};a={};b={};c={};deriv={};",
            RString::from_f64_slice(&u),
            RString::from_f64(a),
            RString::from_f64(b),
            RString::from_f64(c),
            RString::from_bool(deriv)
        )))
        .set_display(&RString::from_str("as.double(psi.hampel(u, a, b, c, deriv))").unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R error");

    let rust_val = hampel(u, a, b, c, deriv);

    for (r_ret, rust_ret) in r_val.into_iter().zip(rust_val.into_iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn psi_bisquare_inner(u: &[f64], c: f64, deriv: bool) {
    let library = RString::from_library_name("MASS");

    let r_val = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "u={};c={};deriv={};",
            RString::from_f64_slice(&u),
            RString::from_f64(c),
            RString::from_bool(deriv)
        )))
        .set_display(&RString::from_str("as.double(psi.bisquare(u, c, deriv))").unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R error");

    let rust_val = bisquare(u, c, deriv);

    for (r_ret, rust_ret) in r_val.into_iter().zip(rust_val.into_iter()) {
        r_assert_relative_equal!(r_ret, rust_ret, 1e-4);
    }
}

pub fn random_robust_linear_regression_test_inner(
    seed: u16,
    num_features: f64,
    num_rows: f64,
    fuzz_amount: f64,
    stu_res: bool,
) {
    let num_features = num_features.to_usize().unwrap();
    let num_rows = num_rows.to_usize().unwrap();
    let fuzz_amount = fuzz_amount.to_usize().unwrap();

    let xs =
        RGenerator::new()
            .set_seed(seed)
            .generate_uniform_matrix(num_rows, num_features, (-10, 10));

    let features = RGenerator::new()
        .set_seed(seed + 1)
        .generate_uniform(num_features + 1, (-100, 100));

    let errors =
        RGenerator::new()
            .set_seed(seed + 2)
            .generate_errors(num_rows, (0, 1), fuzz_amount);

    let ys = (0..num_rows)
        .map(|i| {
            (0..num_features).fold(features[0] + errors[i], |ret, j| {
                ret + (features[j + 1] * xs[(i, j)])
            })
        })
        .collect::<Vec<_>>();

    robust_linear_regression_test_inner(xs, ys, stu_res)
}

pub fn robust_linear_regression_test_inner(xs: DMatrix<f64>, ys: Vec<f64>, stu_res: bool) {
    let library = RString::from_library_name("MASS");

    let r_regression = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};",
            RString::from_f64_slice(&ys),
            RString::from_f64_matrix(&xs)
        )))
        .set_display(&RString::from_str("summary(rlm(y~x))").unwrap())
        .run()
        .expect("R error")
        .as_rlm_regression();
    let r_conf = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};",
            RString::from_f64_slice(&ys),
            RString::from_f64_matrix(&xs)
        )))
        .set_display(&RString::from_str("predict(rlm(y~x), interval='confidence')").unwrap())
        .run()
        .expect("R error")
        .as_f64_matrix()
        .expect("Cannot get R matrix");
    let r_pred = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};",
            RString::from_f64_slice(&ys),
            RString::from_f64_matrix(&xs)
        )))
        .set_display(&RString::from_str("predict(rlm(y~x), interval='prediction')").unwrap())
        .run()
        .expect("R error")
        .as_f64_matrix()
        .expect("Cannot get R matrix");
    let r_vcov = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "y={};x={};",
            RString::from_f64_slice(&ys),
            RString::from_f64_matrix(&xs)
        )))
        .set_display(&RString::from_str("vcov(rlm(y~x))").unwrap())
        .run()
        .expect("R error")
        .as_f64_matrix()
        .expect("Cannot get R matrix");

    let y = DMatrix::from_iterator(ys.len(), 1, ys.iter().copied());

    let model = RobustLinearRegressionBuilder::new()
        .with_x(&ModelMatrix::from(&xs))
        .with_y(&ModelMatrix::from(&y))
        .build();

    let rust_vcov = model.variance().unwrap();

    for i in 0..xs.ncols() {
        r_assert_relative_equal!(
            r_regression.coefs[i].estimate,
            model.parameters().unwrap()[i].estimate(),
            1e-4
        );
        assert!(
            r_regression.coefs[i].estimate
                >= model.parameters().unwrap()[i].confidence_interval().0
        );
        assert!(
            r_regression.coefs[i].estimate
                <= model.parameters().unwrap()[i].confidence_interval().1
        );
    }

    for (&r_lower_conf, &rust_lower_conf) in r_conf
        .column(0)
        .iter()
        .zip(model.predictions().unwrap().estimate().matrix().iter())
    {
        r_assert_relative_equal!(r_lower_conf, rust_lower_conf, 1e-4);
    }
    for (&r_lower_conf, &rust_lower_conf) in r_conf.column(1).iter().zip(
        model
            .predictions()
            .unwrap()
            .confidence_interval()
            .matrix()
            .column(0)
            .iter(),
    ) {
        r_assert_relative_equal!(r_lower_conf, rust_lower_conf, 1e-4);
    }
    for (&r_upper_conf, &rust_upper_conf) in r_conf.column(2).iter().zip(
        model
            .predictions()
            .unwrap()
            .confidence_interval()
            .matrix()
            .column(1)
            .iter(),
    ) {
        r_assert_relative_equal!(r_upper_conf, rust_upper_conf, 1e-4);
    }

    for (&r_lower_conf, &rust_lower_conf) in r_pred.column(1).iter().zip(
        model
            .manipulator()
            .unwrap()
            .confidence_interval()
            .matrix()
            .column(0)
            .iter(),
    ) {
        r_assert_relative_equal!(r_lower_conf, rust_lower_conf, 1e-4);
    }
    for (&r_upper_conf, &rust_upper_conf) in r_pred.column(2).iter().zip(
        model
            .manipulator()
            .unwrap()
            .confidence_interval()
            .matrix()
            .column(1)
            .iter(),
    ) {
        r_assert_relative_equal!(r_upper_conf, rust_upper_conf, 1e-4);
    }

    for (&r_vcov_i, &rust_vcov_i) in r_vcov
        .data
        .as_slice()
        .iter()
        .zip(rust_vcov.data.as_slice().iter())
    {
        r_assert_relative_equal!(r_vcov_i, rust_vcov_i, 1e-4);
    }

    if stu_res {
        let r_res = RTester::new()
            .add_library(&library)
            .set_script(&RString::from_string(format!(
                "y={};x={};",
                RString::from_f64_slice(&ys),
                RString::from_f64_matrix(&xs)
            )))
            .set_display(&RString::from_str("unname(rstudent(rlm(y~x)))").unwrap())
            .run()
            .expect("R error")
            .as_f64_vec()
            .expect("R error");

        for (&rust_ret, &r_ret) in model
            .studentized_residuals()
            .unwrap()
            .matrix()
            .iter()
            .zip(r_res.iter())
        {
            r_assert_relative_equal!(rust_ret, r_ret, 1e-1);
        }
    }

    // TODO: maybe implement this R^2 stuff in R for this test
    // r_squared = function(model) {
    //   sse = sum((model$resid)^2)
    //   observed = model$resid + model$fitted
    //   sst = sum((observed - mean(observed))^2)
    //   value = 1 - (sse/sst)
    //   return(value)
    // }

    // robust_r_squared = function(model) {
    //   sse = sum((model$w * model$resid)^2)
    //   observed = model$resid + model$fitted
    //   sst = sum((model$w*observed - mean(model$w * observed))^2)
    //   value = 1 - sse/sst
    //   return(value)
    // }
}

pub fn area_inner(lower_limit: f64, upper_limit: f64) {
    let library = RString::from_library_name("MASS");

    let rust_area = area(&|a: f64| a.sin(), lower_limit, upper_limit, 10, 1e-5);
    let r_area = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "a={};b={};",
            RString::from_f64(lower_limit),
            RString::from_f64(upper_limit)
        )))
        .set_display(&RString::from_str("area(sin, a, b)").unwrap())
        .run()
        .expect("R error")
        .as_f64()
        .expect("Cannot get R");
    r_assert_relative_equal!(r_area, rust_area, 1e-4);
}

pub fn bcv_inner(x: &DMatrix<f64>) {
    let rust_bcv = bcv(x);

    let library = RString::from_library_name("MASS");
    let r_bcv = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_matrix(x),
        )))
        .set_display(&RString::from_str("bcv(x)").unwrap())
        .run()
        .expect("R error")
        .as_f64()
        .expect("R error");

    r_assert_relative_equal!(rust_bcv, r_bcv, 1e-4);
}

pub fn bandwidth_nrd_inner(x: &DMatrix<f64>) {
    let rust_bandwidth_nrd = bandwidth_nrd(x);

    let library = RString::from_library_name("MASS");
    let r_bandwidth_nrd = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};",
            RString::from_f64_matrix(x),
        )))
        .set_display(&RString::from_str("bandwidth.nrd(x)").unwrap())
        .run()
        .expect("R error")
        .as_f64()
        .expect("R error");

    r_assert_relative_equal!(rust_bandwidth_nrd, r_bandwidth_nrd, 1e-4);
}

pub fn corresp_inner(x: &DataFrame) {
    let x = ModelMatrix::from(x);
    let rust_correspondence = corresp(&x);

    let library = RString::from_library_name("MASS");
    let r_correlation = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};nf={};",
            RString::from_f64_matrix(&x.matrix()),
            RString::from_f64(x.matrix().nrows() / 2),
        )))
        .set_display(&RString::from_str("corresp(x, nf)$cor").unwrap())
        .run()
        .expect("R error")
        .as_f64_vec()
        .expect("R error");
    let r_row_score = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};nf={};r = corresp(x, nf)$rscore; row.names(r) = NULL;",
            RString::from_f64_matrix(&x.matrix()),
            RString::from_f64(x.matrix().nrows() / 2),
        )))
        .set_display(&RString::from_str("r").unwrap())
        .run()
        .expect("R error")
        .as_f64_matrix()
        .expect("R error");
    let r_col_score = RTester::new()
        .add_library(&library)
        .set_script(&RString::from_string(format!(
            "x={};nf={};r = corresp(x, nf)$cscore; row.names(r) = NULL;",
            RString::from_f64_matrix(&x.matrix()),
            RString::from_f64(x.matrix().nrows() / 2),
        )))
        .set_display(&RString::from_str("r").unwrap())
        .run()
        .expect("R error")
        .as_f64_matrix()
        .expect("R error");

    println!("rust: {rust_correspondence:#?}");

    println!("r:");
    println!("scores: {:?}", r_correlation);
    println!("row_scores: {}", r_row_score);
    println!("column_scores: {}", r_col_score);

    for (rust_ret, r_ret) in rust_correspondence
        .iter()
        .map(|c| c.correlation)
        .zip(r_correlation.into_iter())
    {
        r_assert_relative_equal!(rust_ret, r_ret, 1e-4);
    }

    for (rust_ret_vec, r_ret_vec) in rust_correspondence
        .iter()
        .map(|c| c.row_scores.clone())
        .zip(r_row_score.column_iter())
    {
        for (&rust_ret, &r_ret) in rust_ret_vec.iter().zip(r_ret_vec.iter()) {
            r_assert_relative_equal!(rust_ret.abs(), r_ret.abs(), 1e-4);
        }
    }

    for (rust_ret_vec, r_ret_vec) in rust_correspondence
        .iter()
        .map(|c| c.column_scores.clone())
        .zip(r_col_score.column_iter())
    {
        for (&rust_ret, &r_ret) in rust_ret_vec.iter().zip(r_ret_vec.iter()) {
            r_assert_relative_equal!(rust_ret.abs(), r_ret.abs(), 1e-4);
        }
    }
}
