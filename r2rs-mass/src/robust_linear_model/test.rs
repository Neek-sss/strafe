// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;
use r2rs_stats::tests::{
    NormalResidualStatistic, NormalResidualTest, SignificanceOfRegressionStatistic,
    SignificanceOfRegressionTest, TCoefficient, TCoefficientBuilder,
};
use strafe_trait::{
    Assumption, Concept, Conclusion, InsignificantCoefficient, NoSignificantFeature,
    NormalResiduals, SignificantCoefficient, SomeSignficantFeature, StatisticalTest,
};

use crate::robust_linear_model::base::RobustLinearRegression;

pub struct RobustLinearRegressionTest {
    pub residual_test: NormalResidualStatistic,
    pub significance_test: SignificanceOfRegressionStatistic,
    pub significant_coef_tests: Vec<TCoefficient>,
}

impl StatisticalTest for RobustLinearRegression {
    type Input = ();
    type Output = Result<RobustLinearRegressionTest, Box<dyn std::error::Error>>;

    fn assumptions() -> Vec<Box<dyn Assumption>> {
        vec![Box::new(NormalResiduals::new())]
    }

    fn null_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![
            Box::new(NoSignificantFeature::new()),
            Box::new(InsignificantCoefficient::new()),
        ]
    }

    fn alternate_hypotheses() -> Vec<Box<dyn Conclusion>> {
        vec![
            Box::new(SomeSignficantFeature::new()),
            Box::new(SignificantCoefficient::new()),
        ]
    }

    fn test(&mut self, _: &Self::Input) -> Self::Output {
        let residual_test = NormalResidualTest::new()
            .with_alpha(self.alpha)
            .test(&self.model_data)?;

        let significance_test = SignificanceOfRegressionTest::new()
            .with_alpha(self.alpha)
            .with_scale(self.get_scale())
            .with_df(f64::infinity())
            .test(&self.model_data)?;

        let significant_coef_tests = TCoefficientBuilder::new()
            .with_alpha(self.alpha)
            .test(&self.model_data)?;

        Ok(RobustLinearRegressionTest {
            residual_test,
            significance_test,
            significant_coef_tests,
        })
    }
}
