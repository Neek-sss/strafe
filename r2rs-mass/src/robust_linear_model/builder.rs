// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::sync::Arc;

use strafe_trait::ModelBuilder;
use strafe_type::{Alpha64, ModelMatrix};

use crate::robust_linear_model::{huber, robust_linear_regression, RobustLinearRegression};

pub struct RobustLinearRegressionBuilder {
    pub(crate) x: Option<ModelMatrix>,
    pub(crate) y: Option<ModelMatrix>,
    pub(crate) w: Option<ModelMatrix>,
    pub(crate) alpha: Alpha64,
    pub(crate) psi: Arc<dyn Fn(&[f64], bool) -> Vec<f64>>,
    pub(crate) max_iter: usize,
}

impl RobustLinearRegressionBuilder {
    pub fn new() -> Self {
        Self {
            psi: Arc::new(|u, deriv| huber(u, 1.345, deriv)),
            alpha: 0.05.into(),
            max_iter: 20,
            x: None,
            y: None,
            w: None,
        }
    }

    pub fn with_psi<F>(self, psi: &'static F) -> Self
    where
        F: 'static + Fn(&[f64], bool) -> Vec<f64>,
    {
        Self {
            psi: Arc::new(psi),
            ..self
        }
    }

    pub fn with_max_iter(self, max_iter: usize) -> Self {
        Self { max_iter, ..self }
    }
}

impl ModelBuilder for RobustLinearRegressionBuilder {
    type Model = RobustLinearRegression;

    fn with_x(self, x: &ModelMatrix) -> Self {
        Self {
            x: Some(x.clone()),
            ..self
        }
    }

    fn with_y(self, y: &ModelMatrix) -> Self {
        Self {
            y: Some(y.clone()),
            ..self
        }
    }

    fn with_weights(self, weights: &ModelMatrix) -> Self {
        Self {
            w: Some(weights.clone()),
            ..self
        }
    }

    fn with_alpha<A: Into<Alpha64>>(self, alpha: A) -> Self {
        Self {
            alpha: alpha.into(),
            ..self
        }
    }

    fn build(self) -> RobustLinearRegression {
        let (b, w, scale) = robust_linear_regression(
            &self.x.as_ref().unwrap().matrix(),
            &self.y.as_ref().unwrap().matrix(),
            self.psi.as_ref(),
            self.max_iter,
        );

        let w = ModelMatrix::from(w);

        let x1 = ModelMatrix::from(self.x.as_ref().unwrap().matrix().insert_column(0, 1.0));

        let model_data = (
            x1.clone(),
            self.y.as_ref().unwrap().matrix(),
            b.clone(),
            w.clone().matrix(),
        );

        RobustLinearRegression {
            x: self.x.as_ref().unwrap().clone(),
            y: self.y.as_ref().unwrap().clone(),
            x1,
            w,
            b,
            scale,
            psi: self.psi,
            alpha: self.alpha,
            model_data,
        }
    }
}
