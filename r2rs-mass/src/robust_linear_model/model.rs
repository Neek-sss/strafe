// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::error::Error;

use nalgebra::DMatrix;
use num_traits::Float;
use r2rs_stats::{
    funcs::{variance, weighted_residuals},
    tests::{LenientAdjustedRSquaredTest, LinearEstimator, TCoefficientBuilder},
    traits::StatArray,
};
use strafe_trait::{
    Manipulator, Model, Statistic, StatisticalEstimate, StatisticalEstimator, StatisticalTest,
};
use strafe_type::ModelMatrix;

use crate::robust_linear_model::RobustLinearRegression;

impl Model for RobustLinearRegression {
    fn get_x(&self) -> ModelMatrix {
        self.x.clone()
    }

    fn get_x1(&self) -> ModelMatrix {
        self.x1.clone()
    }

    fn get_y(&self) -> ModelMatrix {
        self.y.clone()
    }

    fn get_weights(&self) -> ModelMatrix {
        self.w.clone()
    }

    fn get_intercept(&self) -> bool {
        true
    }

    fn manipulator(
        &self,
    ) -> Result<
        Box<dyn StatisticalEstimate<Manipulator<ModelMatrix, ModelMatrix>, ModelMatrix>>,
        Box<dyn Error>,
    > {
        Ok(Box::new(
            LinearEstimator::new()
                .with_alpha(self.alpha)
                .with_scale(self.get_scale())
                .with_df(f64::infinity())
                .estimate(&self.model_data)?,
        ))
    }

    fn determination(&self) -> Result<Box<dyn Statistic>, Box<dyn Error>> {
        Ok(Box::new(
            LenientAdjustedRSquaredTest::new()
                .with_alpha(self.alpha)
                .with_scale(self.get_scale())
                .with_df(f64::infinity())
                .test(&self.model_data)?,
        ))
    }

    fn parameters(
        &self,
    ) -> Result<Vec<Box<dyn StatisticalEstimate<f64, (f64, f64)>>>, Box<dyn Error>> {
        Ok(TCoefficientBuilder::new()
            .with_alpha(self.alpha)
            .estimate(&self.model_data)?
            .iter()
            .map(|c| {
                let x: Box<dyn StatisticalEstimate<f64, (f64, f64)>> = Box::new(c.clone());
                x
            })
            .collect())
    }

    fn predictions(
        &self,
    ) -> Result<Box<dyn StatisticalEstimate<ModelMatrix, ModelMatrix>>, Box<dyn Error>> {
        Ok(Box::new(
            LinearEstimator::new()
                .with_alpha(self.alpha)
                .with_scale(self.get_scale())
                .with_df(f64::infinity())
                .estimate(&self.model_data)?,
        ))
    }

    fn residuals(&self) -> Result<ModelMatrix, Box<dyn Error>> {
        // let mut weighted_x = self.x.clone();
        // weighted_x
        //     .column_iter_mut()
        //     .for_each(|mut row| row.component_mul_assign(&self.w));
        // let weighted_y = self.y.component_mul(&self.w);

        let ret = Ok(ModelMatrix::from(weighted_residuals(
            &self.y.matrix(),
            &self.x1.matrix(),
            &self.b,
            &self.w.matrix(),
        )));

        ret

        // residuals(&weighted_y, &weighted_x, &self.b)
    }

    fn studentized_residuals(&self) -> Result<ModelMatrix, Box<dyn Error>> {
        let x = self.x1.matrix();
        let hat = self.leverage();
        let sum = self
            .residuals()?
            .matrix()
            .into_iter()
            .map(|f| f.powi(2))
            .sum::<f64>();
        let denom = x.nrows() as f64 - x.ncols() as f64 - 1.0;
        let sigma = self
            .residuals()?
            .matrix()
            .into_iter()
            .enumerate()
            .map(|(i, &res)| {
                if hat[i] < 1.0 {
                    ((sum - res.powi(2)) / denom).sqrt()
                } else {
                    (sum / denom).sqrt()
                }
            })
            .collect::<Vec<_>>();

        let mut ret = self.residuals()?.matrix();
        for ((r_i, sigma_i), h_ii) in ret.iter_mut().zip(sigma.into_iter()).zip(hat.into_iter()) {
            *r_i = *r_i / (sigma_i * (1.0 - h_ii).sqrt());
        }

        Ok(ModelMatrix::from(ret))
    }
    fn standardized_residuals(&self) -> Result<ModelMatrix, Box<dyn Error>> {
        let x = self.x1.matrix();
        let hat = self.leverage();
        let sum = self
            .residuals()?
            .matrix()
            .into_iter()
            .map(|f| f.powi(2))
            .sum::<f64>();

        let mut ret = self.residuals()?.matrix();
        let sd = (sum / (x.nrows() as f64 - x.ncols() as f64 - 1.0)).sqrt();

        for (r_i, h_ii) in ret.iter_mut().zip(hat.into_iter()) {
            *r_i = *r_i / (sd * (1.0 - h_ii).sqrt());
        }

        Ok(ModelMatrix::from(ret))
    }

    fn variance(&self) -> Result<DMatrix<f64>, Box<dyn Error>> {
        let mut weights = self.w.matrix();
        for w in weights.iter_mut() {
            *w = w.sqrt();
        }
        let r = self.x1.matrix().qr().r();
        let rinv = DMatrix::from_diagonal_element(r.nrows(), r.ncols(), 1.0);
        let rinv = r.solve_upper_triangular(&rinv).unwrap();
        let cov_unscaled = rinv.clone() * rinv.transpose();

        let n = self.w.matrix().len();
        let p = self.parameters()?.len();
        let rdf = n - p;
        let s_res = self
            .residuals()?
            .matrix()
            .into_iter()
            .map(|r_i| r_i / self.scale)
            .collect::<Vec<_>>();
        let w = (self.psi)(&s_res, false);
        let s = self
            .residuals()?
            .matrix()
            .into_iter()
            .zip(w.into_iter())
            .map(|(wresid_i, w_i)| (wresid_i * w_i).powi(2))
            .sum::<f64>()
            / rdf as f64;

        let res_div_s = self
            .residuals()?
            .matrix()
            .into_iter()
            .map(|r| r / self.scale)
            .collect::<Vec<_>>();
        let psiprime = (self.psi)(&res_div_s, true);
        let mn = psiprime.mean();
        let kappa = 1.0 + p as f64 * variance(&psiprime) / (n as f64 * mn.powi(2));
        let std_dev = s.sqrt() * (kappa / mn);

        Ok(std_dev.powi(2) * cov_unscaled)
    }
}
