// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

#[cfg(test)]
use log::info;
use nalgebra::DMatrix;
use r2rs_nmath::{distribution::NormalBuilder, traits::Distribution};
use r2rs_stats::{
    funcs::{linear_regression_qr, linear_regression_sqrt_weighted, residuals, weighted_residuals},
    traits::{DeviationType, StatArray},
};
use strafe_type::FloatConstraint;

fn irls_delta(old: &[f64], new: &[f64]) -> f64 {
    let numerator = old
        .iter()
        .zip(new.iter())
        .map(|(&o, &n)| (o - n).powi(2))
        .sum::<f64>();
    let denominator = 1e-20_f64.max(old.iter().map(|o| o.powi(2)).sum());
    (numerator / denominator).sqrt()
}

// TODO: Only used if robust fit convergence test is NULL (other values are residuals, weights, or coefficients)
//fn irls_rrxwr(x: &[f64], w: &[f64], r: &[f64]) -> f64 {
//    let n = x.len();
//    let w = w.iter().map(|w_i| w_i.sqrt()).collect::<Vec<_>>();
//    let a = DMatrix::from_iterator(1, n, w.iter().zip(r.iter()).map(|(w_i, r_i)| w_i * r_i));
//    let b = DMatrix::from_iterator(n, 1, x.iter().copied());
//    let mut one = a * b;
//    one.iter_mut().for_each(|i| *i = i.abs());
//    let c = DMatrix::from_iterator(1, n, w.iter().copied());
//    let d = DMatrix::from_iterator(n, 1, x.iter().map(|x_i| x_i.powi(2)));
//    let mut two = c * d;
//    two.iter_mut().for_each(|i| *i = i.sqrt());
//    let three = w
//   .iter()
//   .zip(r.iter())
//   .map(|(w_i, r_i)| w_i * r_i.powi(2))
//   .sum::<f64>()
//   .sqrt();
//    one.component_div(&two)
//   .iter()
//   .copied()
//   .fold(None, |m: Option<f64>, i| {
//  if let Some(m_i) = m {
// Some(m_i.max(i))
//  } else {
// Some(i)
//  }
//   })
//   .unwrap()
//   / three
//}

pub(crate) fn robust_linear_regression(
    x: &DMatrix<f64>,
    y: &DMatrix<f64>,
    psi: &dyn Fn(&[f64], bool) -> Vec<f64>,
    max_iter: usize,
) -> (DMatrix<f64>, DMatrix<f64>, f64) {
    let k2 = 1.345_f64;

    let mut current_coefficients = linear_regression_qr(&x.clone().insert_column(0, 1.0), y);
    let mut current_residuals =
        residuals(y, &x.clone().insert_column(0, 1.0), &current_coefficients);

    let mut done = false;

    let mut norm_builder = NormalBuilder::new();
    norm_builder.with_mean(0.0_f64);
    norm_builder.with_standard_deviation(1.0_f64);
    let norm = norm_builder.build();

    let theta = 2.0 * norm.probability(k2, true).unwrap() - 1.0;
    let _gamma = theta + k2.powi(2) * (1.0 - theta) - 2.0 * k2 * norm.density(k2).unwrap();

    let mut scale = current_residuals
        .as_slice()
        .mad(DeviationType::Median(Some(0.0)));

    let mut weights = DMatrix::from_iterator(x.shape().0, 1, (0..x.shape().0).map(|_| 1.0));

    #[allow(unused_variables)] // i is used in tests, compiler just doesn't notice cfg
    for i in 0..max_iter {
        let test_vec = current_residuals.clone();
        scale = current_residuals
            .iter()
            .copied()
            .map(|r_i| r_i.abs())
            .collect::<Vec<_>>()
            .median()
            / 0.6745;
        weights = DMatrix::from_iterator(
            weights.shape().0,
            weights.shape().1,
            psi(
                &current_residuals
                    .iter()
                    .copied()
                    .map(|r_i| r_i / scale)
                    .collect::<Vec<_>>(),
                false,
            )
            .into_iter(),
        );
        current_coefficients = linear_regression_sqrt_weighted(x, y, &weights);
        current_residuals = weighted_residuals(
            y,
            &x.clone().insert_column(0, 1.0),
            &current_coefficients,
            &weights,
        );
        done = irls_delta(test_vec.as_slice(), current_residuals.as_slice()) <= 1e-4;
        if done {
            #[cfg(test)]
            info!("rlm converged in {} steps", i + 1);
            break;
        }
    }

    if !done {
        #[cfg(test)]
        eprintln!("'rlm' failed to converge in {} steps", max_iter);
        //   warn!("'rlm' failed to converge in {} steps", max_iter);
    }

    (current_coefficients, weights, scale)
}
