// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

// k = 1.345
pub fn huber(u: &[f64], k: f64, deriv: bool) -> Vec<f64> {
    if deriv {
        u.iter()
            .map(|u_i| if u_i.abs() <= k { 1.0 } else { 0.0 })
            .collect()
    } else {
        u.iter()
            .copied()
            .map(|u_i| (k / u_i.abs()).min(1.0))
            .collect()
    }
}

// a = 2, b = 4, c = 8
pub fn hampel(u: &[f64], a: f64, b: f64, c: f64, deriv: bool) -> Vec<f64> {
    let u = u
        .iter()
        .map(|u_i| (u_i.abs() + 1e-50).min(c))
        .collect::<Vec<_>>();
    if deriv {
        u.iter()
            .map(|&u_i| {
                if u_i <= a {
                    1.0
                } else if u_i <= b {
                    0.0
                } else {
                    -a / (c - b)
                }
            })
            .collect()
    } else {
        u.iter()
            .map(|&u_i| {
                if u_i.abs() <= c {
                    if u_i <= a {
                        u_i.clone()
                    } else if u_i <= b {
                        a
                    } else {
                        a * (c - u_i) / (c - b)
                    }
                } else {
                    0.0
                }
            })
            .zip(u.iter())
            .map(|(r_i, &u_i)| r_i / u_i)
            .collect()
    }
}

// c = 4.685
pub fn bisquare(u: &[f64], c: f64, deriv: bool) -> Vec<f64> {
    if deriv {
        u.iter()
            .map(|u_i| {
                let t = (u_i / c).powi(2);
                if t < 1.0 {
                    (1.0 - t) * (1.0 - 5.0 * t)
                } else {
                    0.0
                }
            })
            .collect()
    } else {
        u.iter()
            .map(|&u_i| (1.0 - (u_i / c).abs().min(1.0).powi(2)).powi(2))
            .collect()
    }
}
