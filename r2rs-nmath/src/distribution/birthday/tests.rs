// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal_result,
};

use crate::{distribution::birthday::BirthdayBuilder, traits::Distribution};

pub fn probability_inner(q: f64, classes: f64, coincident: f64) {
    let mut builder = BirthdayBuilder::new();
    builder.with_classes(classes);
    builder.with_coincident(coincident);
    let birthday = builder.build();

    let rust_ans = birthday.probability(q, false);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pbirthday({}, {}, {})",
            RString::from_f64(q),
            RString::from_f64(classes),
            RString::from_f64(coincident),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn quantile_inner(p: f64, classes: f64, coincident: f64) {
    let mut builder = BirthdayBuilder::new();
    builder.with_classes(classes);
    builder.with_coincident(coincident);
    let birthday = builder.build();

    let rust_ans = birthday.quantile(p, false);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qbirthday({}, {}, {})",
            RString::from_f64(p),
            RString::from_f64(classes),
            RString::from_f64(coincident),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}
