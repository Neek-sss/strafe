// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn probability_test_1() {
    probability_inner(1.0, 1.0, 1.0);
}

#[test]
fn probability_test_2() {
    probability_inner(1.0, 1.0, 2.0);
}

#[test]
fn probability_test_3() {
    probability_inner(1.0, 1.0, 3.0);
}

#[test]
fn probability_test_4() {
    probability_inner(10.0, 1.0, 3.0);
}

#[test]
fn probability_test_5() {
    probability_inner(5.0, 3.0, 3.0);
}

#[test]
fn probability_test_6() {
    probability_inner(2.0, 4.0, 2.0);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1.0, 1.0, 1.0);
}

#[test]
fn quantile_test_2() {
    quantile_inner(0.0, 1.0, 1.0);
}

#[test]
fn quantile_test_3() {
    quantile_inner(0.25, 3.0, 4.0);
}

// TODO: Causes infinite loop
// #[test]
// fn quantile_test_4() {
//     quantile_inner(0.75, 1.0, 1.0);
// }
