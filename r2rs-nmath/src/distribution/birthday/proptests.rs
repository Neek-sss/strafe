// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_n64, stat_p64, stat_r64, strafe_default_proptest_options},
};

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn probability(
        q in stat_r64(None, None, None),
        classes in stat_n64(None, None, None),
        coincident in stat_n64(None, None, None),
    ) {
        probability_inner(q, classes, coincident);
    }

    #[test]
    fn quantile(
        p in stat_p64(None, None, None),
        classes in stat_n64(None, Some(1e2), None),
        coincident in stat_n64(Some(2.0), Some(1e2), None),
    ) {
        quantile_inner(p, classes, coincident);
    }
}
