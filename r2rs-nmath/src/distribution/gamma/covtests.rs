// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;

use super::tests::*;

#[test]
fn density_test_1() {
    density_inner(1.0, 1.0, 1.0);
}

#[test]
fn density_test_2() {
    density_inner(-1.0, 1.0, 1.0);
}

#[test]
fn density_test_3() {
    density_inner(1.0, 0.0, 1.0);
}

#[test]
fn density_test_4() {
    density_inner(0.0, 0.0, 1.0);
}

#[test]
fn density_test_5() {
    density_inner(0.0, 0.1, 1.0);
}

#[test]
fn density_test_6() {
    density_inner(0.0, 2.0, 1.0);
}

#[test]
fn density_test_7() {
    density_inner(0.0, 1.0, 1.0);
}

#[test]
fn density_test_8() {
    density_inner(7.0, 0.1, 1.0);
}

#[test]
fn log_density_test_1() {
    log_density_inner(1.0, 1.0, 1.0);
}

#[test]
fn log_density_test_2() {
    log_density_inner(7.0, 0.1, 1.0);
}

#[test]
fn probability_test_1() {
    probability_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn probability_test_2() {
    probability_inner(1.0, 0.0, 1.0, false);
}

#[test]
fn probability_test_3() {
    probability_inner(-1.0, 0.0, 1.0, false);
}

#[test]
fn probability_test_4() {
    probability_inner(0.0, 1.0, 1.0, false);
}

#[test]
fn probability_test_5() {
    probability_inner(30.0, 100.0, 20.0, false);
}

#[test]
fn probability_test_6() {
    probability_inner(0.5, 2.0, 3.0, false);
}

#[test]
fn probability_test_7() {
    probability_inner(0.5, 0.5, 3.0, true);
}

#[test]
fn probability_test_8() {
    probability_inner(302.0 * 300.0, 300.0, 300.0, false);
}

#[test]
fn log_probability_test_1() {
    log_probability_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn log_probability_test_2() {
    log_probability_inner(30.0, 100.0, 20.0, false);
}

#[test]
fn log_probability_test_3() {
    log_probability_inner(30.0, 100.0, 20.0, true);
}

#[test]
fn log_probability_test_4() {
    log_probability_inner(3e-100, 1e-200, 2e-300, false);
}

#[test]
fn log_probability_test_5() {
    log_probability_inner(0.5, 2.0, 3.0, false);
}

#[test]
fn log_probability_test_6() {
    log_probability_inner(0.5, 2.0, 3.0, true);
}

#[test]
fn log_probability_test_7() {
    log_probability_inner(0.5, 0.5, 3.0, true);
}

#[test]
fn log_probability_test_8() {
    log_probability_inner(0.5, 2.0, 3.0, false);
}

#[test]
fn log_probability_test_9() {
    log_probability_inner(3e-300, 1e-200, 2e-300, false);
}

#[test]
fn log_probability_test_10() {
    log_probability_inner(302.0 * 300.0, 300.0, 300.0, false);
}

#[test]
fn log_probability_test_11() {
    log_probability_inner(302.0 * 300.0, 300.0, 300.0, true);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn quantile_test_2() {
    quantile_inner(0.75, 0.0, 1.0, false);
}

#[test]
fn quantile_test_3() {
    quantile_inner(0.75, 1e-20, 1.0, false);
}

#[test]
fn quantile_test_4() {
    quantile_inner(0.99, 58.0, 22e200, true);
}

#[test]
fn quantile_test_5() {
    quantile_inner(5e-20, 0.1, 2e-10, false);
}

#[test]
fn log_quantile_test_1() {
    log_quantile_inner(-1.0, 1.0, 1.0, false);
}

#[test]
fn random_test_1() {
    random_inner(1, 1.0, 1.0);
}

#[test]
fn random_test_2() {
    random_inner(1, 0.0, 1.0);
}

#[test]
fn random_test_3() {
    random_inner(1, 0.1, 1.0);
}

#[test]
fn random_test_4() {
    random_inner(1, f64::infinity(), 1.0);
}
