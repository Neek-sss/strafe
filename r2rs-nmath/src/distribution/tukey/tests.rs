// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal_result,
};

use crate::{distribution::tukey::TukeyBuilder, traits::Distribution};

pub fn probability_inner(q: f64, means: f64, df: f64, ranges: f64, lower_tail: bool) {
    let mut builder = TukeyBuilder::new();
    builder.with_means(means);
    builder.with_df(df);
    builder.with_ranges(ranges);
    let unif = builder.build();

    let rust_ans = unif.probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "ptukey({}, {}, {}, {}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(means),
            RString::from_f64(df),
            RString::from_f64(ranges),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_probability_inner(q: f64, ranges: f64, means: f64, df: f64, lower_tail: bool) {
    let mut builder = TukeyBuilder::new();
    builder.with_means(means);
    builder.with_df(df);
    builder.with_ranges(ranges);
    let unif = builder.build();

    let rust_ans = unif.log_probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "ptukey({}, {}, {}, {}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(means),
            RString::from_f64(df),
            RString::from_f64(ranges),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn quantile_inner(p: f64, ranges: f64, means: f64, df: f64, lower_tail: bool) {
    let mut builder = TukeyBuilder::new();
    builder.with_means(means);
    builder.with_df(df);
    builder.with_ranges(ranges);
    let unif = builder.build();

    let rust_ans = unif.quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qtukey({}, {}, {}, {}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(means),
            RString::from_f64(df),
            RString::from_f64(ranges),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_quantile_inner(p: f64, ranges: f64, means: f64, df: f64, lower_tail: bool) {
    let mut builder = TukeyBuilder::new();
    builder.with_means(means);
    builder.with_df(df);
    builder.with_ranges(ranges);
    let unif = builder.build();

    let rust_ans = unif.log_quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qtukey({}, {}, {}, {}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(means),
            RString::from_f64(df),
            RString::from_f64(ranges),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}
