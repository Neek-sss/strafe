// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn probability_test_1() {
    probability_inner(1.0, 2.0, 2.0, 1.0, false);
}

#[test]
fn log_probability_test_1() {
    log_probability_inner(1.0, 1.0, 2.0, 2.0, false);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1.0, 1.0, 2.0, 2.0, false);
}

#[test]
fn log_quantile_test_1() {
    log_quantile_inner(-1.0, 1.0, 2.0, 2.0, false);
}
