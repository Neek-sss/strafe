// Translation of nmath's qweibull
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, LogProbability64, Probability64, Rational64, Real64};

use crate::traits::DPQ;

/// The quantile function of the Weibull distribution.
pub fn qweibull<P: Into<Probability64>, RA1: Into<Rational64>, RA2: Into<Rational64>>(
    p: P,
    shape: RA1,
    scale: RA2,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qweibull_inner(p, shape, scale, lower_tail, false)
}

/// The quantile function of the Weibull distribution.
pub fn log_qweibull<LP: Into<LogProbability64>, RA1: Into<Rational64>, RA2: Into<Rational64>>(
    p: LP,
    shape: RA1,
    scale: RA2,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qweibull_inner(p, shape, scale, lower_tail, true)
}

pub fn qweibull_inner<R1: Into<Rational64>, R2: Into<Rational64>>(
    p: f64,
    shape: R1,
    scale: R2,
    lower_tail: bool,
    log: bool,
) -> Real64 {
    let shape = shape.into().unwrap();
    let scale = scale.into().unwrap();

    if let Some(ret) = p.q_p01_boundaries(0.0, f64::infinity(), lower_tail, log) {
        return ret.into();
    }

    (scale * (-p.dt_clog(lower_tail, log)).powf(1.0 / shape)).into()
}
