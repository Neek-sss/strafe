// Translation of nmath's pweibull
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, LogProbability64, Probability64, Rational64, Real64};

use crate::traits::DPQ;

/// The distribution function of the Weibull distribution.
pub fn pweibull<RE: Into<Real64>, RA1: Into<Rational64>, RA2: Into<Rational64>>(
    x: RE,
    shape: RA1,
    scale: RA2,
    lower_tail: bool,
) -> Probability64 {
    let mut x = x.into().unwrap();
    let shape = shape.into().unwrap();
    let scale = scale.into().unwrap();

    if x <= 0.0 {
        return f64::dt_0(lower_tail, false).into();
    }
    x = -(x / scale).powf(shape);
    return if lower_tail {
        -x.exp_m1()
    } else {
        x.d_exp(false)
    }
    .into();
}

/// The distribution function of the Weibull distribution.
pub fn log_pweibull<RE: Into<Real64>, RA1: Into<Rational64>, RA2: Into<Rational64>>(
    x: RE,
    shape: RA1,
    scale: RA2,
    lower_tail: bool,
) -> LogProbability64 {
    let mut x = x.into().unwrap();
    let shape = shape.into().unwrap();
    let scale = scale.into().unwrap();

    if x <= 0.0 {
        return f64::dt_0(lower_tail, true).into();
    }
    x = -(x / scale).powf(shape);
    if lower_tail {
        x.log1_exp()
    } else {
        x.d_exp(true)
    }
    .into()
}
