// Translation of nmath's rweibull
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Rational64, Real64};

use crate::traits::RNG;

/// Random variates from the Weibull distribution.
pub fn rweibull<RA1: Into<Rational64>, RA2: Into<Rational64>, R: RNG>(
    shape: RA1,
    scale: RA2,
    rng: &mut R,
) -> Real64 {
    let shape = shape.into().unwrap();
    let scale = scale.into().unwrap();

    (scale * (-rng.unif_rand().ln()).powf(1.0 / shape)).into()
}
