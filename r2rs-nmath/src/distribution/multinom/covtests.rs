// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_type::{pos64, r64};

use super::tests::*;

#[test]
fn density_test_1() {
    density_inner(vec![r64!(1.0)], Some(vec![pos64!(1)]), None);
}

#[test]
fn density_test_2() {
    density_inner(
        vec![r64!(5), r64!(2), r64!(8)],
        Some(vec![pos64!(1), pos64!(0), pos64!(2)]),
        None,
    );
}

#[test]
fn density_test_3() {
    density_inner(
        vec![r64!(5), r64!(0), r64!(8)],
        Some(vec![pos64!(1), pos64!(0), pos64!(2)]),
        None,
    );
}

#[test]
fn log_density_test_1() {
    log_density_inner(vec![r64!(1.0)], Some(vec![pos64!(1)]), None);
}

#[test]
fn random_test_1() {
    random_inner(1, Some(vec![pos64!(1)]), Some(4));
}

// TODO
// #[test]
// fn random_test_2() {
//     random_inner(
//         1,
//         Some(vec![pos64!(1), pos64!(4), pos64!(2)]),
//         Some(3),
//     );
// }
