// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal, r_assert_relative_equal_result,
};
use strafe_type::FloatConstraint;

use crate::{
    distribution::nbinom::NegativeBinomialBuilder,
    reset_statics,
    rng::MarsagliaMulticarry,
    traits::{Distribution, RNG},
};

pub fn density_inner(x: f64, success_probability: f64, size: f64) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_success_probability(success_probability);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.density(x);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "dnbinom({}, {}, {}, log={})",
            RString::from_f64(x),
            RString::from_f64(size),
            RString::from_f64(success_probability),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn density_mu_inner(x: f64, mu: f64, size: f64) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_mu(mu);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.density(x);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "dnbinom({}, {}, mu={}, log={})",
            RString::from_f64(x),
            RString::from_f64(size),
            RString::from_f64(mu),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_density_inner(x: f64, success_probability: f64, size: f64) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_success_probability(success_probability);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.log_density(x);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "dnbinom({}, {}, {}, log={})",
            RString::from_f64(x),
            RString::from_f64(size),
            RString::from_f64(success_probability),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_density_mu_inner(x: f64, mu: f64, size: f64) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_mu(mu);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.log_density(x);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "dnbinom({}, {}, mu={}, log={})",
            RString::from_f64(x),
            RString::from_f64(size),
            RString::from_f64(mu),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn probability_inner(q: f64, success_probability: f64, size: f64, lower_tail: bool) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_success_probability(success_probability);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pnbinom({}, {}, {}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(size),
            RString::from_f64(success_probability),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn probability_mu_inner(q: f64, mu: f64, size: f64, lower_tail: bool) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_mu(mu);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pnbinom({}, {}, mu={}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(size),
            RString::from_f64(mu),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_probability_inner(q: f64, success_probability: f64, size: f64, lower_tail: bool) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_success_probability(success_probability);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.log_probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pnbinom({}, {}, {}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(size),
            RString::from_f64(success_probability),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_probability_mu_inner(q: f64, mu: f64, size: f64, lower_tail: bool) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_mu(mu);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.log_probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pnbinom({}, {}, mu={}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(size),
            RString::from_f64(mu),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn quantile_inner(p: f64, success_probability: f64, size: f64, lower_tail: bool) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_success_probability(success_probability);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qnbinom({}, {}, {}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(size),
            RString::from_f64(success_probability),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn quantile_mu_inner(p: f64, mu: f64, size: f64, lower_tail: bool) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_mu(mu);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qnbinom({}, {}, mu={}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(size),
            RString::from_f64(mu),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_quantile_inner(p: f64, success_probability: f64, size: f64, lower_tail: bool) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_success_probability(success_probability);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.log_quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qnbinom({}, {}, {}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(size),
            RString::from_f64(success_probability),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_quantile_mu_inner(p: f64, mu: f64, size: f64, lower_tail: bool) {
    let mut builder = NegativeBinomialBuilder::new();
    builder.with_mu(mu);
    builder.with_size(size);
    let nbinom = builder.build();

    let rust_ans = nbinom.log_quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qnbinom({}, {}, mu={}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(size),
            RString::from_f64(mu),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn random_inner(seed: u16, success_probability: f64, size: f64) {
    let num = 50;

    let r_state = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_str(".Random.seed").unwrap())
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap()
        .into_iter()
        .skip(1)
        .map(|i| i as i32)
        .collect::<Vec<_>>();

    let mut rust_rng = MarsagliaMulticarry::new();
    rust_rng.set_seed(seed as u32);

    let rust_state = rust_rng.get_state();

    for (r, rust) in r_state.iter().zip(rust_state.iter()) {
        assert_eq!(r, rust, "States not equal!");
    }

    let r_ret = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_string(format!(
            "rnbinom({}, prob={}, size={})",
            RString::from_f64(num),
            RString::from_f64(success_probability),
            RString::from_f64(size),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut builder = NegativeBinomialBuilder::new();
    builder.with_success_probability(success_probability);
    builder.with_size(size);
    let nbinom = builder.build();
    reset_statics();
    let rust_ret = (0..num)
        .map(|_| nbinom.random_sample(&mut rust_rng))
        .collect::<Vec<_>>();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        r_assert_relative_equal!(*r, rust.unwrap());
    }
}

pub fn random_mu_inner(seed: u16, mu: f64, size: f64) {
    let num = 50;

    let r_state = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_str(".Random.seed").unwrap())
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap()
        .into_iter()
        .skip(1)
        .map(|i| i as i32)
        .collect::<Vec<_>>();

    let mut rust_rng = MarsagliaMulticarry::new();
    rust_rng.set_seed(seed as u32);

    let rust_state = rust_rng.get_state();

    for (r, rust) in r_state.iter().zip(rust_state.iter()) {
        assert_eq!(r, rust, "States not equal!");
    }

    let r_ret = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_string(format!(
            "rnbinom({}, mu={}, size={})",
            RString::from_f64(num),
            RString::from_f64(mu),
            RString::from_f64(size),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut builder = NegativeBinomialBuilder::new();
    builder.with_mu(mu);
    builder.with_size(size);
    let nbinom = builder.build();
    reset_statics();
    let rust_ret = (0..num)
        .map(|_| nbinom.random_sample(&mut rust_rng))
        .collect::<Vec<_>>();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        r_assert_relative_equal!(*r, rust.unwrap());
    }
}
