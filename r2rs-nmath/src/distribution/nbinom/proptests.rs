// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_lp64, stat_p64, stat_pos64, stat_r64, strafe_default_proptest_options},
};

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn density(
        x in stat_r64(None, None, None),
        success_probability in stat_p64(Some(1e-6), None, None),
        size in stat_pos64(None, None, None),
    ) {
        density_inner(x, success_probability, size);
    }

    #[test]
    fn density_mu(
        x in stat_r64(None, None, None),
        mu in stat_pos64(None, None, None),
        size in stat_pos64(None, None, None),
    ) {
        density_mu_inner(x, mu, size);
    }

    #[test]
    fn log_density(
        x in stat_r64(None, None, None),
        success_probability in stat_p64(Some(1e-6), None, None),
        size in stat_pos64(None, None, None),
    ) {
        log_density_inner(x, success_probability, size);
    }

    #[test]
    fn log_density_mu(
        x in stat_r64(None, None, None),
        mu in stat_pos64(None, None, None),
        size in stat_pos64(None, None, None),
    ) {
        log_density_mu_inner(x, mu, size);
    }

    #[test]
    fn probability(
        q in stat_r64(None, None, None),
        success_probability in stat_p64(Some(1e-6), None, None),
        size in stat_pos64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        probability_inner(q, success_probability, size, lower_tail);
    }

    #[test]
    fn probability_mu(
        q in stat_r64(None, None, None),
        mu in stat_pos64(None, None, None),
        size in stat_pos64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        probability_mu_inner(q, mu, size, lower_tail);
    }

    #[test]
    fn log_probability(
        q in stat_r64(None, None, None),
        success_probability in stat_p64(Some(1e-6), None, None),
        size in stat_pos64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        log_probability_inner(q, success_probability, size, lower_tail);
    }

    #[test]
    fn log_mu_probability(
        q in stat_r64(None, None, None),
        mu in stat_pos64(None, None, None),
        size in stat_pos64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        log_probability_mu_inner(q, mu, size, lower_tail);
    }

    #[test]
    fn quantile(
        p in stat_p64(None, None, None),
        success_probability in stat_p64(Some(1e-6), None, None),
        size in stat_pos64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        quantile_inner(p, success_probability, size, lower_tail);
    }

    #[test]
    fn quantile_mu(
        p in stat_p64(None, None, None),
        mu in stat_pos64(None, None, None),
        size in stat_pos64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        quantile_mu_inner(p, mu, size, lower_tail);
    }

    #[test]
    fn log_quantile(
        p in stat_lp64(None, None, None),
        success_probability in stat_p64(Some(1e-6), None, None),
        size in stat_pos64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        log_quantile_inner(p, success_probability, size, lower_tail);
    }

    #[test]
    fn log_quantile_mu(
        p in stat_lp64(None, None, None),
        mu in stat_pos64(None, None, None),
        size in stat_pos64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        log_quantile_mu_inner(p, mu, size, lower_tail);
    }

    #[test]
    fn random(
        seed in 0..std::u16::MAX,
        success_probability in stat_p64(Some(1e-4), Some(1e4), None),
        size in stat_pos64(Some(1e-4), Some(1e4), None),
    ) {
        random_inner(seed, success_probability, size);
    }

    #[test]
    fn random_mu(
        seed in 0..std::u16::MAX,
        mu in stat_pos64(None, None, None),
        size in stat_pos64(Some(1e-4), Some(1e4), None),
    ) {
        random_mu_inner(seed, mu, size);
    }
}
