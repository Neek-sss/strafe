// Translation of nmath's qnbinom
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, LogProbability64, Positive64, Probability64, Real64};

use super::{log_pnbinom, pnbinom};
use crate::{distribution::func::discrete_body, traits::DPQ};

/// The quantile function of the negative binomial distribution,
/// for the (size, prob) parametrizations
///
/// x = the number of failures before the n-th succes
///
/// Uses the Cornish-Fisher Expansion to include a skewness
/// correction to a normal approximation.  This gives an
/// initial value which never seems to be off by more than
/// 1 or 2.  A search is then conducted of values close to
/// this initial start point.
pub fn qnbinom<PR1: Into<Probability64>, PO: Into<Positive64>, PR2: Into<Probability64>>(
    p: PR1,
    size: PO,
    prob: PR2,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qnbinom_inner(p, size, prob, lower_tail, false)
}

/// The quantile function of the negative binomial distribution,
/// for the (size, prob) parametrizations
///
/// x = the number of failures before the n-th succes
///
/// Uses the Cornish-Fisher Expansion to include a skewness
/// correction to a normal approximation.  This gives an
/// initial value which never seems to be off by more than
/// 1 or 2.  A search is then conducted of values close to
/// this initial start point.
pub fn log_qnbinom<LP: Into<LogProbability64>, PO: Into<Positive64>, PR: Into<Probability64>>(
    p: LP,
    size: PO,
    prob: PR,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qnbinom_inner(p, size, prob, lower_tail, true)
}

fn qnbinom_inner<PO: Into<Positive64>, PR: Into<Probability64>>(
    p: f64,
    size: PO,
    prob: PR,
    lower_tail: bool,
    log: bool,
) -> Real64 {
    let size = size.into().unwrap();
    let prob = prob.into().unwrap();

    /* this happens if specified via mu, size, since
       prob == size/(size+mu)
    */
    if prob == 0.0 && size == 0.0 {
        return 0.0.into();
    }

    if prob <= 0.0 {
        return f64::nan().into();
    }

    if prob == 1.0 || size == 0.0 {
        return 0.0.into();
    }

    if let Some(ret) = p.q_p01_boundaries(0.0, f64::infinity(), lower_tail, log) {
        return ret.into();
    }

    let Q = 1.0 / prob;
    let P = (1.0 - prob) * Q; // = (1 - prob) / prob  =  Q - 1
    let mu = size * P;
    let sigma = (size * P * Q).sqrt();
    let gamma = (Q + P) / sigma;

    // R_DBG_printf("qnbinom(p=%.12g, size=%.15g, prob=%g, l.t.=%d, log=%d):"
    //              " mu=%g, sigma=%g, gamma=%g;\n",
    //              p, size, prob, lower_tail, log_p, mu, sigma, gamma);

    let y = discrete_body(
        mu,
        sigma,
        gamma,
        p,
        None,
        lower_tail,
        log,
        &|p| pnbinom(p, size, prob, lower_tail).unwrap(),
        &|p| log_pnbinom(p, size, prob, lower_tail).unwrap(),
    );

    y.into()
}
