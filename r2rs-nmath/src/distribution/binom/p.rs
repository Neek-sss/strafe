// Translation of nmath's pbinom
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, LogProbability64, PositiveInteger64, Probability64, Real64};

use crate::{
    distribution::beta::{log_pbeta, pbeta},
    traits::DPQ,
};

/// The distribution function of the binomial distribution.
pub fn pbinom<R: Into<Real64>, PO: Into<PositiveInteger64>, PR: Into<Probability64>>(
    x: R,
    n: PO,
    p: PR,
    lower_tail: bool,
) -> Probability64 {
    pbinom_inner(x, n, p, lower_tail, false).into()
}

/// The distribution function of the binomial distribution.
pub fn log_pbinom<R: Into<Real64>, PO: Into<PositiveInteger64>, PR: Into<Probability64>>(
    x: R,
    n: PO,
    p: PR,
    lower_tail: bool,
) -> LogProbability64 {
    pbinom_inner(x, n, p, lower_tail, true).into()
}

pub fn pbinom_inner<R: Into<Real64>, PO: Into<PositiveInteger64>, PR: Into<Probability64>>(
    x: R,
    n: PO,
    p: PR,
    lower_tail: bool,
    log: bool,
) -> f64 {
    let mut x = x.into().unwrap();
    let n = n.into().unwrap();
    let p = p.into().unwrap();

    if !n.is_finite() || !p.is_finite() {
        return f64::nan();
    }

    if x < 0.0 {
        return f64::dt_0(lower_tail, log);
    }
    x = (x + 1e-7).floor();
    if n <= x {
        return f64::dt_1(lower_tail, log);
    }
    if log {
        log_pbeta(p, x + 1.0, n - x, !lower_tail).unwrap()
    } else {
        pbeta(p, x + 1.0, n - x, !lower_tail).unwrap()
    }
}
