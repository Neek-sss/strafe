// Translation of nmath's qbinom
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, LogProbability64, PositiveInteger64, Probability64, Real64};

use super::{log_pbinom, pbinom};
use crate::{distribution::func::discrete_body, traits::DPQ};

/// The quantile function of the binomial distribution.
///
/// Uses the Cornish-Fisher Expansion to include a skewness
/// correction to a normal approximation.  This gives an
/// initial value which never seems to be off by more than
/// 1 or 2.  A search is then conducted of values close to
/// this initial start point.
pub fn qbinom<PR1: Into<Probability64>, PO: Into<PositiveInteger64>, PR2: Into<Probability64>>(
    p: PR1,
    n: PO,
    pr: PR2,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qbinom_inner(p, n, pr, lower_tail, false)
}

/// The quantile function of the binomial distribution.
///
/// Uses the Cornish-Fisher Expansion to include a skewness
/// correction to a normal approximation.  This gives an
/// initial value which never seems to be off by more than
/// 1 or 2.  A search is then conducted of values close to
/// this initial start point.
pub fn log_qbinom<
    LP: Into<LogProbability64>,
    PO: Into<PositiveInteger64>,
    PR: Into<Probability64>,
>(
    p: LP,
    n: PO,
    pr: PR,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qbinom_inner(p, n, pr, lower_tail, true)
}

fn qbinom_inner<PO: Into<PositiveInteger64>, PR: Into<Probability64>>(
    p: f64,
    n: PO,
    pr: PR,
    lower_tail: bool,
    log: bool,
) -> Real64 {
    let n = n.into().unwrap();
    let pr = pr.into().unwrap();

    if !n.is_finite() || !pr.is_finite() {
        return f64::nan().into();
    }

    if !p.is_finite() && !log {
        /* if log is true, p = -Inf is a legitimate value */
        return f64::nan().into();
    }

    if let Some(ret) = p.q_p01_boundaries(0.0, n, lower_tail, log) {
        return ret.into();
    }

    if pr == 0.0 || n == 0.0 {
        return 0.0.into();
    }

    if pr == 1.0 {
        return n.into();
    }

    let q = 1.0 - pr;
    let mu = n * pr;
    let sigma = (n * pr * q).sqrt();
    let gamma = (q - pr) / sigma;

    // R_DBG_printf("qbinom(p=%.12g, n=%.15g, pr=%.7g, l.t.=%d, log=%d): sigma=%g, gamma=%g;\n",
    //              p, n,pr, lower_tail, log_p, sigma, gamma);

    let ret = discrete_body(
        mu,
        sigma,
        gamma,
        p,
        Some(n),
        lower_tail,
        log,
        &|p| pbinom(p, n, pr, lower_tail).unwrap(),
        &|p| log_pbinom(p, n, pr, lower_tail).unwrap(),
    );
    ret.into()
}
