// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    num_to_w64,
    proptest::prelude::*,
    proptest_ext::{stat_lp64, stat_p64, stat_r64, stat_w64, strafe_default_proptest_options},
};
use strafe_type::tof64;

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn density(
        x in stat_r64(None, None, None),
        (group_1, group_2, number_drawn) in (
            stat_w64(None, None, None),
            stat_w64(None, None, None),
            stat_w64(None, None, None),
        ).prop_map(|(g1, g2, nd)| {
            (
                g1,
                g2,
                num_to_w64!(tof64!(nd) % (tof64!(g1) + tof64!(g2)))
            )
        })
    ) {
        density_inner(x, group_1, group_2, number_drawn);
    }

    #[test]
    fn log_density(
        x in stat_r64(None, None, None),
        (group_1, group_2, number_drawn) in (
            stat_w64(None, None, None),
            stat_w64(None, None, None),
            stat_w64(None, None, None),
        ).prop_map(|(g1, g2, nd)| {
            (
                g1,
                g2,
                num_to_w64!(tof64!(nd) % (tof64!(g1) + tof64!(g2)))
            )
        })
    ) {
        log_density_inner(x, group_1, group_2, number_drawn);
    }

    #[test]
    fn probability(
        q in stat_r64(None, None, None),
        (group_1, group_2, number_drawn) in (
            stat_w64(None, None, None),
            stat_w64(None, None, None),
            stat_w64(None, None, None),
        ).prop_map(|(g1, g2, nd)| {
            (
                g1,
                g2,
                num_to_w64!(tof64!(nd) % (tof64!(g1) + tof64!(g2)))
            )
        }),
        lower_tail in prop::bool::ANY,
    ) {
        probability_inner(q, group_1, group_2, number_drawn, lower_tail);
    }

    #[test]
    fn log_probability(
        q in stat_r64(None, None, None),
        (group_1, group_2, number_drawn) in (
            stat_w64(None, None, None),
            stat_w64(None, None, None),
            stat_w64(None, None, None),
        ).prop_map(|(g1, g2, nd)| {
            (
                g1,
                g2,
                num_to_w64!(tof64!(nd) % (tof64!(g1) + tof64!(g2)))
            )
        }),
        lower_tail in prop::bool::ANY,
    ) {
        log_probability_inner(q, group_1, group_2, number_drawn, lower_tail);
    }

    #[test]
    fn quantile(
        p in stat_p64(None, None, None),
        (group_1, group_2, number_drawn) in (
            stat_w64(None, None, None),
            stat_w64(None, None, None),
            stat_w64(None, None, None),
        ).prop_map(|(g1, g2, nd)| {
            (
                g1,
                g2,
                num_to_w64!(tof64!(nd) % (tof64!(g1) + tof64!(g2)))
            )
        }),
        lower_tail in prop::bool::ANY,
    ) {
        quantile_inner(p, group_1, group_2, number_drawn, lower_tail);
    }

    #[test]
    fn log_quantile(
        p in stat_lp64(None, None, None),
        (group_1, group_2, number_drawn) in (
            stat_w64(None, None, None),
            stat_w64(None, None, None),
            stat_w64(None, None, None),
        ).prop_map(|(g1, g2, nd)| {
            (
                g1,
                g2,
                num_to_w64!(tof64!(nd) % (tof64!(g1) + tof64!(g2)))
            )
        }),
        lower_tail in prop::bool::ANY,
    ) {
        log_quantile_inner(p, group_1, group_2, number_drawn, lower_tail);
    }

    #[test]
    fn random(
        seed in 0..std::u16::MAX,
        (group_1, group_2, number_drawn) in (
            stat_w64(None, Some(1e5), None),
            stat_w64(None, Some(1e5), None),
            stat_w64(None, Some(1e5), None),
        ).prop_map(|(g1, g2, nd)| {
            (
                g1,
                g2,
                num_to_w64!(tof64!(nd) % (tof64!(g1) + tof64!(g2)))
            )
        })
    ) {
        random_inner(seed, group_1, group_2, number_drawn);
    }
}
