// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn density_test_1() {
    density_inner(1.0, 1.0, 1.0, 1.0);
}

#[test]
fn density_test_2() {
    density_inner(-1.0, 1.0, 1.0, 1.0);
}

#[test]
fn density_test_3() {
    density_inner(1.1, 1.0, 1.0, 1.0);
}

#[test]
fn density_test_4() {
    density_inner(2.0, 1.0, 1.0, 1.0);
}

#[test]
fn density_test_5() {
    density_inner(0.0, 1.0, 1.0, 0.0);
}

#[test]
fn log_density_test_1() {
    log_density_inner(1.0, 1.0, 1.0, 1.0);
}

#[test]
fn probability_test_1() {
    probability_inner(1.0, 1.0, 1.0, 1.0, false);
}

#[test]
fn probability_test_2() {
    probability_inner(5.0, 1.0, 1.0, 1.0, false);
}

#[test]
fn probability_test_3() {
    probability_inner(5.0, 21.0, 10.0, 20.0, false);
}

#[test]
fn log_probability_test_1() {
    log_probability_inner(1.0, 1.0, 1.0, 1.0, false);
}

#[test]
fn log_probability_test_2() {
    log_probability_inner(5.0, 21.0, 10.0, 20.0, false);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1.0, 1.0, 1.0, 1.0, false);
}

#[test]
fn log_quantile_test_1() {
    log_quantile_inner(-1.0, 1.0, 1.0, 1.0, false);
}

// TODO
// #[test]
// fn random_test_1() {
//     random_inner(1, 1, 1, 1);
// }

// TODO: Need to figure out how to tap the rest of the code for random

#[test]
fn random_test_2() {
    random_inner(2, 2.0, 2.0, 1.0);
}

#[test]
fn random_test_3() {
    random_inner(2, i32::MAX as f64, 2.0, 1.0);
}
