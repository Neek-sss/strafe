// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal, r_assert_relative_equal_result,
};
use strafe_type::FloatConstraint;

use crate::{
    distribution::geom::GeometricBuilder,
    reset_statics,
    rng::MarsagliaMulticarry,
    traits::{Distribution, RNG},
};

pub fn density_inner(x: f64, success_probability: f64) {
    let mut builder = GeometricBuilder::new();
    builder.with_success_probability(success_probability);
    let geom = builder.build();

    let rust_ans = geom.density(x);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "dgeom({}, {}, log={})",
            RString::from_f64(x),
            RString::from_f64(success_probability),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_density_inner(x: f64, success_probability: f64) {
    let mut builder = GeometricBuilder::new();
    builder.with_success_probability(success_probability);
    let geom = builder.build();

    let rust_ans = geom.log_density(x);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "dgeom({}, {}, log={})",
            RString::from_f64(x),
            RString::from_f64(success_probability),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn probability_inner(q: f64, success_probability: f64, lower_tail: bool) {
    let mut builder = GeometricBuilder::new();
    builder.with_success_probability(success_probability);
    let geom = builder.build();

    let rust_ans = geom.probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pgeom({}, {}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(success_probability),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_probability_inner(q: f64, success_probability: f64, lower_tail: bool) {
    let mut builder = GeometricBuilder::new();
    builder.with_success_probability(success_probability);
    let geom = builder.build();

    let rust_ans = geom.log_probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pgeom({}, {}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(success_probability),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn quantile_inner(p: f64, success_probability: f64, lower_tail: bool) {
    let mut builder = GeometricBuilder::new();
    builder.with_success_probability(success_probability);
    let geom = builder.build();

    let rust_ans = geom.quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qgeom({}, {}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(success_probability),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_quantile_inner(p: f64, success_probability: f64, lower_tail: bool) {
    let mut builder = GeometricBuilder::new();
    builder.with_success_probability(success_probability);
    let geom = builder.build();

    let rust_ans = geom.log_quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qgeom({}, {}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(success_probability),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn random_inner(seed: u16, success_probability: f64) {
    let num = 50;

    let r_state = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_str(".Random.seed").unwrap())
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap()
        .into_iter()
        .skip(1)
        .map(|i| i as i32)
        .collect::<Vec<_>>();

    let mut rust_rng = MarsagliaMulticarry::new();
    rust_rng.set_seed(seed as u32);

    let rust_state = rust_rng.get_state();

    for (r, rust) in r_state.iter().zip(rust_state.iter()) {
        assert_eq!(r, rust, "States not equal!");
    }

    let r_ret = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_string(format!(
            "rgeom({}, {})",
            RString::from_f64(num as f64),
            RString::from_f64(success_probability),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut builder = GeometricBuilder::new();
    builder.with_success_probability(success_probability);
    let geom = builder.build();
    reset_statics();
    let rust_ret = (0..num)
        .map(|_| geom.random_sample(&mut rust_rng))
        .collect::<Vec<_>>();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        r_assert_relative_equal!(*r, rust.unwrap());
    }
}
