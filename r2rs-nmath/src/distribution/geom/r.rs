// Translation of nmath's rgeom
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Probability64, Real64};

use crate::{distribution::pois::rpois, rng::NMathRNG, traits::RNG};

/// Random variates from the geometric distribution.
///
/// We generate lambda as exponential with scale parameter
/// p / (1 - p).  Return a Poisson deviate with mean lambda.
/// See Example 1.5 in Devroye (1986), Chapter 10, pages 488f.
pub fn rgeom<P: Into<Probability64>, R: RNG>(p: P, rng: &mut R) -> Real64 {
    let p = p.into().unwrap();

    if !p.is_finite() || p <= 0.0 {
        return f64::nan().into();
    }

    rpois(rng.exp_rand() * ((1.0 - p) / p), rng)
}
