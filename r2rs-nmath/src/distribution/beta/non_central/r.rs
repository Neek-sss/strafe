// Translation of nmath's rnbeta
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Positive64, Rational64, Real64};

use crate::{
    distribution::chisq::{rchisq, rnchisq},
    traits::RNG,
};

pub fn rnbeta<R1: Into<Rational64>, R2: Into<Rational64>, P: Into<Positive64>, R: RNG>(
    shape1: R1,
    shape2: R2,
    ncp: P,
    rng: &mut R,
) -> Real64 {
    let shape1 = shape1.into().unwrap();
    let shape2 = shape2.into().unwrap();

    let X = rnchisq(2.0 * shape1, ncp, rng).unwrap();
    (X / (X + rchisq(2.0 * shape2, rng).unwrap())).into()
}
