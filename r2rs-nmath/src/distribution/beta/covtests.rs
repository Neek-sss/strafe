// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;

use super::tests::*;

#[test]
fn density_test_1() {
    density_inner(-1.0, 1.0, 1.0);
}

#[test]
fn density_test_2() {
    density_inner(1.0, 0.0, 0.0);
}

#[test]
fn density_test_3() {
    density_inner(0.5, 0.0, 0.0);
}

#[test]
fn density_test_4() {
    density_inner(0.0, 0.0, 1.0);
}

#[test]
fn density_test_5() {
    density_inner(0.1, 0.0, 1.0);
}

#[test]
fn density_test_6() {
    density_inner(1.0, 1.0, 0.0);
}

#[test]
fn density_test_7() {
    density_inner(0.1, 1.0, 0.0);
}

#[test]
fn density_test_8() {
    density_inner(0.0, 2.0, 1.0);
}

#[test]
fn density_test_9() {
    density_inner(0.0, 0.5, 1.0);
}

#[test]
fn density_test_10() {
    density_inner(0.0, 1.0, 1.0);
}

#[test]
fn density_test_11() {
    density_inner(1.0, 1.0, 2.0);
}

#[test]
fn density_test_12() {
    density_inner(1.0, 1.0, 0.5);
}

#[test]
fn density_test_13() {
    density_inner(1.0, 1.0, 1.0);
}

#[test]
fn density_test_14() {
    density_inner(0.5, f64::infinity(), f64::infinity());
}

#[test]
fn density_test_15() {
    density_inner(1.0, f64::infinity(), f64::infinity());
}

#[test]
fn density_test_16() {
    density_inner(0.5, 1.5, 1.5);
}

#[test]
fn density_test_17() {
    density_inner(0.5, 2.5, 1.5);
}

#[test]
fn density_test_18() {
    density_inner(0.5, 2.5, 2.5);
}

#[test]
fn log_density_test_1() {
    log_density_inner(1.0, 1.0, 1.0);
}

#[test]
fn probability_test_1() {
    probability_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn probability_test_2() {
    probability_inner(0.0, 1.0, 1.0, false);
}

#[test]
fn probability_test_3() {
    probability_inner(0.5, 1.0, 1.0, false);
}

#[test]
fn probability_test_4() {
    probability_inner(0.5, 0.0, 0.0, false);
}

#[test]
fn probability_test_5() {
    probability_inner(0.5, 0.0, 1.0, false);
}

#[test]
fn probability_test_6() {
    probability_inner(0.5, 1.0, 0.0, false);
}

#[test]
fn probability_test_7() {
    probability_inner(0.25, f64::infinity(), f64::infinity(), false);
}

#[test]
fn probability_test_8() {
    probability_inner(0.75, f64::infinity(), f64::infinity(), false);
}

#[test]
fn probability_test_9() {
    probability_inner(0.5, 1e-20, 1e-21, false);
}

#[test]
fn probability_test_10() {
    probability_inner(0.5, 1e-1, 1.0, false);
}

#[test]
fn probability_test_11() {
    probability_inner(0.6, 1e-1, 1.0, false);
}

#[test]
fn probability_test_12() {
    probability_inner(0.6, 1e-1, 1.1, false);
}

#[test]
fn probability_test_13() {
    probability_inner(0.6, 1.1, 1e-1, false);
}

#[test]
fn probability_test_14() {
    probability_inner(0.005, 1e-2, 1.1, false);
}

#[test]
fn probability_test_15() {
    probability_inner(0.005, 1e-2, 16.0, false);
}

#[test]
fn probability_test_16() {
    probability_inner(0.005, 1e-2, 0.05, false);
}

#[test]
fn probability_test_17() {
    probability_inner(0.000005, 1e-1, 1.05, false);
}

#[test]
fn probability_test_18() {
    probability_inner(0.4, 1e-1, 1e-17, false);
}

#[test]
fn probability_test_19() {
    probability_inner(0.4, 1e-17, 1e-1, false);
}

#[test]
fn log_probability_test_1() {
    log_probability_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn log_probability_test_2() {
    log_probability_inner(0.5, 1e-20, 1e-21, false);
}

#[test]
fn log_probability_test_3() {
    log_probability_inner(0.5, 1e-21, 1e-20, false);
}

#[test]
fn log_probability_test_4() {
    log_probability_inner(0.4, 1e-1, 1e-17, false);
}

#[test]
fn log_probability_test_5() {
    log_probability_inner(0.5, 1e-1, 1.0, false);
}

#[test]
fn log_probability_test_6() {
    log_probability_inner(0.6, 1e-1, 1.1, false);
}

#[test]
fn log_probability_test_7() {
    log_probability_inner(0.005, 1e-2, 1.1, false);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn quantile_test_2() {
    quantile_inner(0.0, 1.0, 1.0, false);
}

#[test]
fn quantile_test_3() {
    quantile_inner(0.25, 1.0, 1.0, false);
}

#[test]
fn quantile_test_4() {
    quantile_inner(0.75, 1.0, 1.0, false);
}

#[test]
fn quantile_test_5() {
    quantile_inner(0.1, 0.0, 0.0, false);
}

#[test]
fn quantile_test_6() {
    quantile_inner(0.9, 0.0, 0.0, false);
}

#[test]
fn quantile_test_7() {
    quantile_inner(0.5, 0.0, 0.0, false);
}

#[test]
fn quantile_test_8() {
    quantile_inner(0.5, 0.0, 1.0, false);
}

#[test]
fn quantile_test_9() {
    quantile_inner(0.5, 1.0, 0.0, false);
}

#[test]
fn quantile_test_10() {
    quantile_inner(0.5, f64::infinity(), f64::infinity(), false);
}

#[test]
fn quantile_test_11() {
    quantile_inner(0.5, 21.0, 18.0, false);
}

#[test]
fn quantile_test_12() {
    quantile_inner(0.5, 21.0, 18.0, true);
}

#[test]
fn quantile_test_13() {
    quantile_inner(0.5, 18.0, 21e300, false);
}

// TODO: BRatio Error
// #[test]
// fn quantile_test_14() {
//     quantile_inner(0.5, 18e300, 21.0, false);
// }

// TODO: P Error
// #[test]
// fn quantile_test_14() {
//     quantile_inner(0.5, 18e50, 21.0, false);
// }

#[test]
fn log_quantile_test_1() {
    log_quantile_inner(-1.0, 1.0, 1.0, false);
}

#[test]
fn log_quantile_test_2() {
    log_quantile_inner(-1.0, 21.0, 18.0, false);
}

#[test]
fn log_quantile_test_3() {
    log_quantile_inner(-1e100, 21.0, 18.0, false);
}

#[test]
fn random_test_1() {
    random_inner(1, 1.0, 1.0);
}

#[test]
fn random_test_2() {
    random_inner(1, f64::infinity(), f64::infinity());
}

#[test]
fn random_test_3() {
    random_inner(1, 0.0, 0.0);
}

#[test]
fn random_test_4() {
    random_inner(1, 1.0, 1.0);
}

#[test]
fn random_test_5() {
    random_inner(1, f64::infinity(), 1.0);
}

#[test]
fn random_test_6() {
    random_inner(1, 1.0, f64::infinity());
}

#[test]
fn random_test_7() {
    random_inner(1, 2.0, 3.0);
}
