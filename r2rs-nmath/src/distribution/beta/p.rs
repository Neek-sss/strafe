// Translation of nmath's pbeta
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, LogProbability64, Positive64, Probability64, Real64};

use crate::{distribution::func::bratio, traits::DPQ};

/// Returns distribution function of the beta distribution.
/// ( = The incomplete beta ratio $I_x(p,q)$ ).
pub fn pbeta_raw(x: f64, a: f64, b: f64, lower_tail: bool, log: bool) -> f64 {
    // treat limit cases correctly here:
    if a == 0.0 || b == 0.0 || !a.is_finite() || !b.is_finite() {
        // NB:  0 < x < 1 :
        return if a == 0.0 && b == 0.0 {
            // point mass 1/2 at each of {0,1} :
            if log {
                -std::f64::consts::LN_2
            } else {
                0.5
            }
        } else if a == 0.0 || a / b == 0.0 {
            // point mass 1 at 0 ==> P(X <= x) = 1, all x > 0
            f64::dt_1(lower_tail, log)
        } else if b == 0.0 || b / a == 0.0 {
            // point mass 1 at 1 ==> P(X <= x) = 0, all x < 1
            f64::dt_0(lower_tail, log)
        } else {
            // else, remaining case:  a = b = Inf : point mass 1 at 1/2
            if x < 0.5 {
                f64::dt_0(lower_tail, log)
            } else {
                f64::dt_1(lower_tail, log)
            }
        };
    }
    if x >= 1.0 {
        // may happen when called from qbeta()
        return f64::dt_1(lower_tail, log);
    }

    // Now:  0 < a < Inf;  0 < b < Inf

    let x1 = 0.5 - x + 0.5;
    let mut w = f64::nan();
    let mut w1 = f64::nan();
    let mut ierr = 0;
    //====
    bratio(a, b, x, x1, &mut w, &mut w1, &mut ierr, log); /* -> ./toms708.c */
    if ierr != 0 {
        panic!("Error in bratio {}", ierr);
    }
    //====
    if lower_tail {
        w
    } else {
        w1
    }
}
/* pbeta_raw() */
pub fn pbeta<R: Into<Real64>, P1: Into<Positive64>, P2: Into<Positive64>>(
    x: R,
    a: P1,
    b: P2,
    lower_tail: bool,
) -> Probability64 {
    pbeta_inner(x, a, b, lower_tail, false).into()
}

/* pbeta_raw() */
pub fn log_pbeta<R: Into<Real64>, P1: Into<Positive64>, P2: Into<Positive64>>(
    x: R,
    a: P1,
    b: P2,
    lower_tail: bool,
) -> LogProbability64 {
    pbeta_inner(x, a, b, lower_tail, true).into()
}

pub fn pbeta_inner<R: Into<Real64>, P1: Into<Positive64>, P2: Into<Positive64>>(
    x: R,
    a: P1,
    b: P2,
    lower_tail: bool,
    log: bool,
) -> f64 {
    let x = x.into().unwrap();
    let a = a.into().unwrap();
    let b = b.into().unwrap();

    // allowing a==0 and b==0  <==> treat as one- or two-point mass

    if x <= 0.0 {
        return f64::dt_0(lower_tail, log);
    }
    if x >= 1.0 {
        return f64::dt_1(lower_tail, log);
    }

    pbeta_raw(x, a, b, lower_tail, log)
}
