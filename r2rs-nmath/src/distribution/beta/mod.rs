mod d;
mod non_central;
mod p;
mod q;
mod r;

use strafe_type::{FloatConstraint, LogProbability64, Positive64, Probability64, Real64};

pub(crate) use self::{d::*, non_central::*, p::*, q::*, r::*};
use crate::traits::{Distribution, RNG};

/// # The Beta Distribution
///
/// ## Description:
///
/// Density, distribution function, quantile function and random
/// generation for the Beta distribution with parameters ‘shape1’ and
/// ‘shape2’ (and optional non-centrality parameter ‘ncp’).
///
/// ## Arguments:
///
/// * x, q: vector of quantiles.
/// * p: vector of probabilities.
/// * n: number of observations.
/// * shape1, shape2: non-negative parameters of the Beta distribution.
/// * ncp: non-centrality parameter.
/// * log, log: logical; if true, probabilities $p$ are given as $\log(p)$.
/// * lower_tail: logical; if true (default), probabilities are $P\[X <= x\]$, otherwise, $P\[X > x\]$.
///
/// ## Details:
///
/// The Beta distribution with parameters $\text{shape1} = a$ and $\text{shape2} = b$
/// has density
///
/// $Gamma(a+b)/(Gamma(a)Gamma(b))x^(a-1)(1-x)^(b-1)$
///
/// for $a > 0$, $b > 0$ and $0 <= x <= 1$ where the boundary values at $x=0$
/// or $x=1$ are defined as by continuity (as limits).
///
/// The mean is $\frac{a}{a+b}$ and the variance is $\frac{ab}{(a+b)^2 (a+b+1)}$.
/// These moments and all distributional properties can be defined as
/// limits (leading to point masses at 0, 1/2, or 1) when a or b are
/// zero or infinite, and the corresponding ‘\[dpqr\]beta()’ functions
/// are defined correspondingly.
///
/// ‘pbeta’ is closely related to the incomplete beta function.  As
/// defined by Abramowitz and Stegun 6.6.1
///
/// $B_x(a,b) = integral_0^x t^(a-1) (1-t)^(b-1) dt$,
///
/// and 6.6.2 $I_x(a,b) = \frac{B_x(a,b)}{B(a,b)}$ where $B(a,b) = B_1(a,b)$ is
/// the Beta function (‘beta’).
///
/// $I_x(a,b)$ is ‘pbeta(x, a, b)’.
///
/// The noncentral Beta distribution (with $\text{ncp} = \lambda$) is defined
/// (Johnson _et al_, 1995, pp. 502) as the distribution of $\frac{X}{X+Y}$
/// where $X ~ \chi^2_2a(\lambda)$ and $Y ~ \chi^2_2b$.
///
/// ## Density Plot
///
/// ```rust
/// # use r2rs_base::traits::StatisticalSlice;
/// # use r2rs_nmath::{distribution::BetaBuilder, traits::Distribution};
/// # use strafe_plot::prelude::{IntoDrawingArea, Line, Plot, PlotOptions, SVGBackend, BLACK};
/// # use strafe_type::FloatConstraint;
/// let beta = BetaBuilder::new().build();
/// let x = <[f64]>::sequence(-1.0, 2.0, 1000);
/// let y = x
///     .iter()
///     .map(|x| beta.density(x).unwrap())
///     .collect::<Vec<_>>();
///
/// let root = SVGBackend::new("density.svg", (1024, 768)).into_drawing_area();
/// Plot::new()
///     .with_options(PlotOptions {
///         x_axis_label: "x".to_string(),
///         y_axis_label: "density".to_string(),
///         ..Default::default()
///     })
///     .with_plottable(Line {
///         x,
///         y,
///         color: BLACK,
///         ..Default::default()
///     })
///     .plot(&root)
///     .unwrap();
/// # use std::fs::rename;
/// #     drop(root);
/// #     rename(
/// #             format!("density.svg"),
/// #             format!("src/distribution/beta/doctest_out/density.svg"),
/// #     )
/// #     .unwrap();
/// ```
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("density", "src/distribution/beta/doctest_out/density.svg")))]
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = "![Density][density]"))]
///
/// ## Value:
///
/// ‘dbeta’ gives the density, ‘pbeta’ the distribution function,
/// ‘qbeta’ the quantile function, and ‘rbeta’ generates random
/// deviates.
///
/// Invalid arguments will result in return value ‘NaN’, with a
/// warning.
///
/// The length of the result is determined by ‘n’ for ‘rbeta’, and is
/// the maximum of the lengths of the numerical arguments for the
/// other functions.
///
/// The numerical arguments other than ‘n’ are recycled to the length
/// of the result.  Only the first elements of the logical arguments
/// are used.
///
/// ## Note:
///
/// Supplying ‘ncp = 0’ uses the algorithm for the non-central
/// distribution, which is not the same algorithm used if ‘ncp’ is
/// omitted.  This is to give consistent behaviour in extreme cases
/// with values of ‘ncp’ very near zero.
///
/// ## Source:
///
/// • The central ‘dbeta’ is based on a binomial probability, using
/// code contributed by Catherine Loader (see ‘dbinom’) if either
/// shape parameter is larger than one, otherwise directly from
/// the definition.  The non-central case is based on the
/// derivation as a Poisson mixture of betas (Johnson _et al_,
/// 1995, pp. 502-3).
///
/// • The central ‘pbeta’ for the default (‘log = FALSE’) uses a
/// C translation based on
///
/// Didonato, A. and Morris, A., Jr, (1992) Algorithm 708:
/// Significant digit computation of the incomplete beta function
/// ratios, _ACM Transactions on Mathematical Software_, *18*,
/// 360-373.  (See also
/// Brown, B. and Lawrence Levy, L. (1994) Certification of
/// algorithm 708: Significant digit computation of the
/// incomplete beta, _ACM Transactions on Mathematical Software_,
/// *20*, 393-397.)
///
/// We have slightly tweaked the original “TOMS 708” algorithm,
/// and enhanced for ‘log.p = TRUE’.  For that (log-scale) case,
/// underflow to ‘-Inf’ (i.e., P = 0) or ‘0’, (i.e., P = 1) still
/// happens because the original algorithm was designed without
/// log-scale considerations.  Underflow to ‘-Inf’ now typically
/// signals a ‘warning’.
///
/// • The non-central ‘pbeta’ uses a C translation of
///
/// Lenth, R. V. (1987) Algorithm AS226: Computing noncentral
/// beta probabilities. _Appl. Statist_, *36*, 241-244,
/// incorporating
/// Frick, H. (1990)'s AS R84, _Appl. Statist_, *39*, 311-2, and
/// Lam, M.L. (1995)'s AS R95, _Appl. Statist_, *44*, 551-2.
///
/// This computes the lower tail only, so the upper tail suffers
/// from cancellation and a warning will be given when this is
/// likely to be significant.
///
/// • The central case of ‘qbeta’ is based on a C translation of
///
/// Cran, G. W., K. J. Martin and G. E. Thomas (1977).  Remark AS
/// R19 and Algorithm AS 109, _Applied Statistics_, *26*,
/// 111-114, and subsequent remarks (AS83 and correction).
///
/// • The central case of ‘rbeta’ is based on a C translation of
///
/// R. C. H. Cheng (1978).  Generating beta variates with
/// nonintegral shape parameters.  _Communications of the ACM_,
/// *21*, 317-322.
///
/// ## References:
///
/// Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) _The New S
/// Language_.  Wadsworth & Brooks/Cole.
///
/// Abramowitz, M. and Stegun, I. A. (1972) _Handbook of Mathematical
/// Functions._ New York: Dover.  Chapter 6: Gamma and Related
/// Functions.
///
/// Johnson, N. L., Kotz, S. and Balakrishnan, N. (1995) _Continuous
/// Univariate Distributions_, volume 2, especially chapter 25. Wiley,
/// New York.
///
/// ## See Also:
///
/// Distributions for other standard distributions.
///
/// ‘beta’ for the Beta function.
///
/// ## Examples:
///
/// We'll generate some plots with this function to examine the Beta distribution
/// ```rust
/// # use num_traits::float::Float;
/// # use r2rs_base::traits::StatisticalSlice;
/// # use r2rs_nmath::{distribution::BetaBuilder, traits::Distribution};
/// # use strafe_plot::{
/// #     plot::Plot,
/// #     prelude::{
/// #         full_palette::GREY, HorizontalLine, IntoDrawingArea, Line, PlotOptions, SVGBackend,
/// #         BLACK, GREEN, RED,
/// #     },
/// # };
/// # use std::fs::rename;
/// use strafe_type::FloatConstraint;
/// let plot_beta = |a: f64, b: f64| {
///     let mut is_lim = false;
///     let x = if a == 0.0 || b == 0.0 || a.is_infinite() || b.is_infinite() {
///         is_lim = true;
///
///         let eps = 1e-10;
///
///         let mut ret = vec![0.0, eps];
///         ret.append(&mut (1..=7).map(|i| i as f64 / 16.0).collect());
///         ret.append(&mut vec![0.5 - eps, 0.5, 0.5 + eps]);
///         ret.append(&mut (9..=15).map(|i| i as f64 / 16.0).collect());
///         ret.append(&mut vec![1.0 - eps, 1.0]);
///
///         ret
///     } else {
///         <[f64]>::sequence(0.0, 1.0, 1025)
///     };
///
///     let beta = BetaBuilder::new().with_shape1(a).with_shape2(b).build();
///
///     let density = x
///         .iter()
///         .map(|x| beta.density(x).unwrap())
///         .map(|y| if !y.is_finite() { 1e100 } else { y })
///         .collect::<Vec<_>>();
///     let probability = x
///         .iter()
///         .map(|x| beta.probability(x, true).unwrap())
///         .map(|y| if !y.is_finite() { 1e100 } else { y })
///         .collect::<Vec<_>>();
///     let quantile = x
///         .iter()
///         .map(|x| beta.quantile(x, true).unwrap())
///         .map(|y| if !y.is_finite() { 1e100 } else { y })
///         .collect::<Vec<_>>();
///
///     let file_name = format!("plot_beta_{a}_{b}.svg");
///     let root = SVGBackend::new(&file_name, (1024, 768)).into_drawing_area();
///     Plot::new()
///         .with_options(PlotOptions {
///             y_min: if is_lim { Some(0.0) } else { None },
///             y_max: if is_lim { Some(1.1) } else { None },
///             title: format!("Beta a={a}, b={b}"),
///             x_axis_label: "x".to_string(),
///             legend_x: 0.5,
///             legend_y: 0.0,
///             ..Default::default()
///         })
///         .with_plottable(Line {
///             x: x.clone(),
///             y: density,
///             legend: true,
///             label: "Density".to_string(),
///             color: BLACK,
///             ..Default::default()
///         })
///         .with_plottable(Line {
///             x: x.clone(),
///             y: probability,
///             legend: true,
///             label: "Probability".to_string(),
///             color: RED,
///             dash: true,
///             ..Default::default()
///         })
///         .with_plottable(Line {
///             x: x.clone(),
///             y: quantile,
///             legend: true,
///             label: "Quantile".to_string(),
///             color: GREEN,
///             dash: true,
///             ..Default::default()
///         })
///         .with_plottable(Line {
///             x: vec![-100.0, 100.0],
///             y: vec![-100.0, 100.0],
///             color: GREY,
///             force_fit_all: false,
///             ..Default::default()
///         })
///         .with_plottable(HorizontalLine {
///             color: GREY,
///             y: 1.0,
///             ..Default::default()
///         })
///         .plot(&root)
///         .unwrap();
/// #     drop(root);
/// #     rename(
/// #             format!("plot_beta_{a}_{b}.svg"),
/// #             format!("src/distribution/beta/doctest_out/plot_beta_{a}_{b}.svg"),
/// #     )
/// #     .unwrap();
/// };
/// ```
/// ```rust
/// # use num_traits::float::Float;
/// # use r2rs_base::traits::StatisticalSlice;
/// # use r2rs_nmath::{distribution::BetaBuilder, traits::Distribution};
/// # use strafe_plot::{
/// #     plot::Plot,
/// #     prelude::{
/// #         full_palette::GREY, HorizontalLine, IntoDrawingArea, Line, PlotOptions, SVGBackend,
/// #         BLACK, GREEN, RED,
/// #     },
/// # };
/// # use strafe_type::FloatConstraint;
/// # use std::fs::rename;
/// # let plot_beta = |a: f64, b: f64| {
/// #     let mut is_lim = false;
/// #     let x = if a == 0.0 || b == 0.0 || a.is_infinite() || b.is_infinite() {
/// #         is_lim = true;
/// #
/// #         let eps = 1e-10;
/// #
/// #         let mut ret = vec![0.0, eps];
/// #         ret.append(&mut (1..=7).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![0.5 - eps, 0.5, 0.5 + eps]);
/// #         ret.append(&mut (9..=15).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![1.0 - eps, 1.0]);
/// #
/// #         ret
/// #     } else {
/// #         <[f64]>::sequence(0.0, 1.0, 1025)
/// #     };
/// #
/// #     let beta = BetaBuilder::new().with_shape1(a).with_shape2(b).build();
/// #
/// #     let density = x
/// #         .iter()
/// #         .map(|x| beta.density(x).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let probability = x
/// #         .iter()
/// #         .map(|x| beta.probability(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let quantile = x
/// #         .iter()
/// #         .map(|x| beta.quantile(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #
/// #     let file_name = format!("plot_beta_{a}_{b}.svg");
/// #     let root = SVGBackend::new(&file_name, (1024, 768)).into_drawing_area();
/// #     Plot::new()
/// #         .with_options(PlotOptions {
/// #             y_min: if is_lim { Some(0.0) } else { None },
/// #             y_max: if is_lim { Some(1.1) } else { None },
/// #             title: format!("Beta a={a}, b={b}"),
/// #             x_axis_label: "x".to_string(),
/// #             legend_x: 0.5,
/// #             legend_y: 0.0,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: density,
/// #             legend: true,
/// #             label: "Density".to_string(),
/// #             color: BLACK,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: probability,
/// #             legend: true,
/// #             label: "Probability".to_string(),
/// #             color: RED,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: quantile,
/// #             legend: true,
/// #             label: "Quantile".to_string(),
/// #             color: GREEN,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: vec![-100.0, 100.0],
/// #             y: vec![-100.0, 100.0],
/// #             color: GREY,
/// #             force_fit_all: false,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(HorizontalLine {
/// #             color: GREY,
/// #             y: 1.0,
/// #             ..Default::default()
/// #         })
/// #         .plot(&root)
/// #         .unwrap();
/// #     drop(root);
/// #     rename(
/// #             format!("plot_beta_{a}_{b}.svg"),
/// #             format!("src/distribution/beta/doctest_out/plot_beta_{a}_{b}.svg"),
/// #     )
/// #     .unwrap();
/// # };
/// plot_beta(3.0, 1.0);
/// ```
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_3_1", "src/distribution/beta/doctest_out/plot_beta_3_1.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta 3 1][plot_beta_3_1]")
)]
/// ```rust
/// # use num_traits::float::Float;
/// # use r2rs_base::traits::StatisticalSlice;
/// # use r2rs_nmath::{distribution::BetaBuilder, traits::Distribution};
/// # use strafe_plot::{
/// #     plot::Plot,
/// #     prelude::{
/// #         full_palette::GREY, HorizontalLine, IntoDrawingArea, Line, PlotOptions, SVGBackend,
/// #         BLACK, GREEN, RED,
/// #     },
/// # };
/// # use strafe_type::FloatConstraint;
/// # use std::fs::rename;
/// # let plot_beta = |a: f64, b: f64| {
/// #     let mut is_lim = false;
/// #     let x = if a == 0.0 || b == 0.0 || a.is_infinite() || b.is_infinite() {
/// #         is_lim = true;
/// #
/// #         let eps = 1e-10;
/// #
/// #         let mut ret = vec![0.0, eps];
/// #         ret.append(&mut (1..=7).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![0.5 - eps, 0.5, 0.5 + eps]);
/// #         ret.append(&mut (9..=15).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![1.0 - eps, 1.0]);
/// #
/// #         ret
/// #     } else {
/// #         <[f64]>::sequence(0.0, 1.0, 1025)
/// #     };
/// #
/// #     let beta = BetaBuilder::new().with_shape1(a).with_shape2(b).build();
/// #
/// #     let density = x
/// #         .iter()
/// #         .map(|x| beta.density(x).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let probability = x
/// #         .iter()
/// #         .map(|x| beta.probability(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let quantile = x
/// #         .iter()
/// #         .map(|x| beta.quantile(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #
/// #     let file_name = format!("plot_beta_{a}_{b}.svg");
/// #     let root = SVGBackend::new(&file_name, (1024, 768)).into_drawing_area();
/// #     Plot::new()
/// #         .with_options(PlotOptions {
/// #             y_min: if is_lim { Some(0.0) } else { None },
/// #             y_max: if is_lim { Some(1.1) } else { None },
/// #             title: format!("Beta a={a}, b={b}"),
/// #             x_axis_label: "x".to_string(),
/// #             legend_x: 0.5,
/// #             legend_y: 0.0,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: density,
/// #             legend: true,
/// #             label: "Density".to_string(),
/// #             color: BLACK,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: probability,
/// #             legend: true,
/// #             label: "Probability".to_string(),
/// #             color: RED,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: quantile,
/// #             legend: true,
/// #             label: "Quantile".to_string(),
/// #             color: GREEN,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: vec![-100.0, 100.0],
/// #             y: vec![-100.0, 100.0],
/// #             color: GREY,
/// #             force_fit_all: false,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(HorizontalLine {
/// #             color: GREY,
/// #             y: 1.0,
/// #             ..Default::default()
/// #         })
/// #         .plot(&root)
/// #         .unwrap();
/// #     drop(root);
/// #     rename(
/// #             format!("plot_beta_{a}_{b}.svg"),
/// #             format!("src/distribution/beta/doctest_out/plot_beta_{a}_{b}.svg"),
/// #     )
/// #     .unwrap();
/// # };
/// plot_beta(2.0, 4.0);
/// plot_beta(3.0, 7.0);
/// ```
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_2_4", "src/distribution/beta/doctest_out/plot_beta_2_4.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta 2 4][plot_beta_2_4]")
)]
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_3_7", "src/distribution/beta/doctest_out/plot_beta_3_7.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta 3 7][plot_beta_3_7]")
)]
/// ```rust
/// # use num_traits::float::Float;
/// # use r2rs_base::traits::StatisticalSlice;
/// # use r2rs_nmath::{distribution::BetaBuilder, traits::Distribution};
/// # use strafe_plot::{
/// #     plot::Plot,
/// #     prelude::{
/// #         full_palette::GREY, HorizontalLine, IntoDrawingArea, Line, PlotOptions, SVGBackend,
/// #         BLACK, GREEN, RED,
/// #     },
/// # };
/// # use strafe_type::FloatConstraint;
/// # use std::fs::rename;
/// # let plot_beta = |a: f64, b: f64| {
/// #     let mut is_lim = false;
/// #     let x = if a == 0.0 || b == 0.0 || a.is_infinite() || b.is_infinite() {
/// #         is_lim = true;
/// #
/// #         let eps = 1e-10;
/// #
/// #         let mut ret = vec![0.0, eps];
/// #         ret.append(&mut (1..=7).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![0.5 - eps, 0.5, 0.5 + eps]);
/// #         ret.append(&mut (9..=15).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![1.0 - eps, 1.0]);
/// #
/// #         ret
/// #     } else {
/// #         <[f64]>::sequence(0.0, 1.0, 1025)
/// #     };
/// #
/// #     let beta = BetaBuilder::new().with_shape1(a).with_shape2(b).build();
/// #
/// #     let density = x
/// #         .iter()
/// #         .map(|x| beta.density(x).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let probability = x
/// #         .iter()
/// #         .map(|x| beta.probability(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let quantile = x
/// #         .iter()
/// #         .map(|x| beta.quantile(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #
/// #     let file_name = format!("plot_beta_{a}_{b}.svg");
/// #     let root = SVGBackend::new(&file_name, (1024, 768)).into_drawing_area();
/// #     Plot::new()
/// #         .with_options(PlotOptions {
/// #             y_min: if is_lim { Some(0.0) } else { None },
/// #             y_max: if is_lim { Some(1.1) } else { None },
/// #             title: format!("Beta a={a}, b={b}"),
/// #             x_axis_label: "x".to_string(),
/// #             legend_x: 0.5,
/// #             legend_y: 0.0,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: density,
/// #             legend: true,
/// #             label: "Density".to_string(),
/// #             color: BLACK,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: probability,
/// #             legend: true,
/// #             label: "Probability".to_string(),
/// #             color: RED,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: quantile,
/// #             legend: true,
/// #             label: "Quantile".to_string(),
/// #             color: GREEN,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: vec![-100.0, 100.0],
/// #             y: vec![-100.0, 100.0],
/// #             color: GREY,
/// #             force_fit_all: false,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(HorizontalLine {
/// #             color: GREY,
/// #             y: 1.0,
/// #             ..Default::default()
/// #         })
/// #         .plot(&root)
/// #         .unwrap();
/// #     drop(root);
/// #     rename(
/// #             format!("plot_beta_{a}_{b}.svg"),
/// #             format!("src/distribution/beta/doctest_out/plot_beta_{a}_{b}.svg"),
/// #     )
/// #     .unwrap();
/// # };
/// plot_beta(0.0, 0.0); // point masses at  {0, 1}
/// ```
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_0_0", "src/distribution/beta/doctest_out/plot_beta_0_0.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta 0 0][plot_beta_0_0]")
)]
/// ```rust
/// # use num_traits::float::Float;
/// # use r2rs_base::traits::StatisticalSlice;
/// # use r2rs_nmath::{distribution::BetaBuilder, traits::Distribution};
/// # use strafe_plot::{
/// #     plot::Plot,
/// #     prelude::{
/// #         full_palette::GREY, HorizontalLine, IntoDrawingArea, Line, PlotOptions, SVGBackend,
/// #         BLACK, GREEN, RED,
/// #     },
/// # };
/// # use strafe_type::FloatConstraint;
/// # use std::fs::rename;
/// # let plot_beta = |a: f64, b: f64| {
/// #     let mut is_lim = false;
/// #     let x = if a == 0.0 || b == 0.0 || a.is_infinite() || b.is_infinite() {
/// #         is_lim = true;
/// #
/// #         let eps = 1e-10;
/// #
/// #         let mut ret = vec![0.0, eps];
/// #         ret.append(&mut (1..=7).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![0.5 - eps, 0.5, 0.5 + eps]);
/// #         ret.append(&mut (9..=15).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![1.0 - eps, 1.0]);
/// #
/// #         ret
/// #     } else {
/// #         <[f64]>::sequence(0.0, 1.0, 1025)
/// #     };
/// #
/// #     let beta = BetaBuilder::new().with_shape1(a).with_shape2(b).build();
/// #
/// #     let density = x
/// #         .iter()
/// #         .map(|x| beta.density(x).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let probability = x
/// #         .iter()
/// #         .map(|x| beta.probability(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let quantile = x
/// #         .iter()
/// #         .map(|x| beta.quantile(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #
/// #     let file_name = format!("plot_beta_{a}_{b}.svg");
/// #     let root = SVGBackend::new(&file_name, (1024, 768)).into_drawing_area();
/// #     Plot::new()
/// #         .with_options(PlotOptions {
/// #             y_min: if is_lim { Some(0.0) } else { None },
/// #             y_max: if is_lim { Some(1.1) } else { None },
/// #             title: format!("Beta a={a}, b={b}"),
/// #             x_axis_label: "x".to_string(),
/// #             legend_x: 0.5,
/// #             legend_y: 0.0,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: density,
/// #             legend: true,
/// #             label: "Density".to_string(),
/// #             color: BLACK,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: probability,
/// #             legend: true,
/// #             label: "Probability".to_string(),
/// #             color: RED,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: quantile,
/// #             legend: true,
/// #             label: "Quantile".to_string(),
/// #             color: GREEN,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: vec![-100.0, 100.0],
/// #             y: vec![-100.0, 100.0],
/// #             color: GREY,
/// #             force_fit_all: false,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(HorizontalLine {
/// #             color: GREY,
/// #             y: 1.0,
/// #             ..Default::default()
/// #         })
/// #         .plot(&root)
/// #         .unwrap();
/// #     drop(root);
/// #     rename(
/// #             format!("plot_beta_{a}_{b}.svg"),
/// #             format!("src/distribution/beta/doctest_out/plot_beta_{a}_{b}.svg"),
/// #     )
/// #     .unwrap();
/// # };
/// plot_beta(0.0, 2.0);
/// plot_beta(1.0, f64::infinity()); // point mass at 0 ; the same as
/// ```
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_0_2", "src/distribution/beta/doctest_out/plot_beta_0_2.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta 0 2][plot_beta_0_2]")
)]
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_1_inf", "src/distribution/beta/doctest_out/plot_beta_1_inf.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta 1 inf][plot_beta_1_inf]")
)]
/// ```rust
/// # use num_traits::float::Float;
/// # use r2rs_base::traits::StatisticalSlice;
/// # use r2rs_nmath::{distribution::BetaBuilder, traits::Distribution};
/// # use strafe_plot::{
/// #     plot::Plot,
/// #     prelude::{
/// #         full_palette::GREY, HorizontalLine, IntoDrawingArea, Line, PlotOptions, SVGBackend,
/// #         BLACK, GREEN, RED,
/// #     },
/// # };
/// # use strafe_type::FloatConstraint;
/// # use std::fs::rename;
/// # let plot_beta = |a: f64, b: f64| {
/// #     let mut is_lim = false;
/// #     let x = if a == 0.0 || b == 0.0 || a.is_infinite() || b.is_infinite() {
/// #         is_lim = true;
/// #
/// #         let eps = 1e-10;
/// #
/// #         let mut ret = vec![0.0, eps];
/// #         ret.append(&mut (1..=7).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![0.5 - eps, 0.5, 0.5 + eps]);
/// #         ret.append(&mut (9..=15).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![1.0 - eps, 1.0]);
/// #
/// #         ret
/// #     } else {
/// #         <[f64]>::sequence(0.0, 1.0, 1025)
/// #     };
/// #
/// #     let beta = BetaBuilder::new().with_shape1(a).with_shape2(b).build();
/// #
/// #     let density = x
/// #         .iter()
/// #         .map(|x| beta.density(x).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let probability = x
/// #         .iter()
/// #         .map(|x| beta.probability(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let quantile = x
/// #         .iter()
/// #         .map(|x| beta.quantile(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #
/// #     let file_name = format!("plot_beta_{a}_{b}.svg");
/// #     let root = SVGBackend::new(&file_name, (1024, 768)).into_drawing_area();
/// #     Plot::new()
/// #         .with_options(PlotOptions {
/// #             y_min: if is_lim { Some(0.0) } else { None },
/// #             y_max: if is_lim { Some(1.1) } else { None },
/// #             title: format!("Beta a={a}, b={b}"),
/// #             x_axis_label: "x".to_string(),
/// #             legend_x: 0.5,
/// #             legend_y: 0.0,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: density,
/// #             legend: true,
/// #             label: "Density".to_string(),
/// #             color: BLACK,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: probability,
/// #             legend: true,
/// #             label: "Probability".to_string(),
/// #             color: RED,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: quantile,
/// #             legend: true,
/// #             label: "Quantile".to_string(),
/// #             color: GREEN,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: vec![-100.0, 100.0],
/// #             y: vec![-100.0, 100.0],
/// #             color: GREY,
/// #             force_fit_all: false,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(HorizontalLine {
/// #             color: GREY,
/// #             y: 1.0,
/// #             ..Default::default()
/// #         })
/// #         .plot(&root)
/// #         .unwrap();
/// #     drop(root);
/// #     rename(
/// #             format!("plot_beta_{a}_{b}.svg"),
/// #             format!("src/distribution/beta/doctest_out/plot_beta_{a}_{b}.svg"),
/// #     )
/// #     .unwrap();
/// # };
/// plot_beta(f64::infinity(), 2.0); // point mass at 1 ; the same as
/// plot_beta(3.0, 0.0);
/// ```
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_inf_2", "src/distribution/beta/doctest_out/plot_beta_inf_2.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta Inf 2][plot_beta_inf_2]")
)]
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_3_0", "src/distribution/beta/doctest_out/plot_beta_3_0.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta 3 0][plot_beta_3_0]")
)]
/// ```rust
/// # use num_traits::float::Float;
/// # use r2rs_base::traits::StatisticalSlice;
/// # use r2rs_nmath::{distribution::BetaBuilder, traits::Distribution};
/// # use strafe_plot::{
/// #     plot::Plot,
/// #     prelude::{
/// #         full_palette::GREY, HorizontalLine, IntoDrawingArea, Line, PlotOptions, SVGBackend,
/// #         BLACK, GREEN, RED,
/// #     },
/// # };
/// # use strafe_type::FloatConstraint;
/// # use std::fs::rename;
/// # let plot_beta = |a: f64, b: f64| {
/// #     let mut is_lim = false;
/// #     let x = if a == 0.0 || b == 0.0 || a.is_infinite() || b.is_infinite() {
/// #         is_lim = true;
/// #
/// #         let eps = 1e-10;
/// #
/// #         let mut ret = vec![0.0, eps];
/// #         ret.append(&mut (1..=7).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![0.5 - eps, 0.5, 0.5 + eps]);
/// #         ret.append(&mut (9..=15).map(|i| i as f64 / 16.0).collect());
/// #         ret.append(&mut vec![1.0 - eps, 1.0]);
/// #
/// #         ret
/// #     } else {
/// #         <[f64]>::sequence(0.0, 1.0, 1025)
/// #     };
/// #
/// #     let beta = BetaBuilder::new().with_shape1(a).with_shape2(b).build();
/// #
/// #     let density = x
/// #         .iter()
/// #         .map(|x| beta.density(x).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let probability = x
/// #         .iter()
/// #         .map(|x| beta.probability(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #     let quantile = x
/// #         .iter()
/// #         .map(|x| beta.quantile(x, true).unwrap())
/// #         .map(|y| if !y.is_finite() { 1e100 } else { y })
/// #         .collect::<Vec<_>>();
/// #
/// #     let file_name = format!("plot_beta_{a}_{b}.svg");
/// #     let root = SVGBackend::new(&file_name, (1024, 768)).into_drawing_area();
/// #     Plot::new()
/// #         .with_options(PlotOptions {
/// #             y_min: if is_lim { Some(0.0) } else { None },
/// #             y_max: if is_lim { Some(1.1) } else { None },
/// #             title: format!("Beta a={a}, b={b}"),
/// #             x_axis_label: "x".to_string(),
/// #             legend_x: 0.5,
/// #             legend_y: 0.0,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: density,
/// #             legend: true,
/// #             label: "Density".to_string(),
/// #             color: BLACK,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: probability,
/// #             legend: true,
/// #             label: "Probability".to_string(),
/// #             color: RED,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: x.clone(),
/// #             y: quantile,
/// #             legend: true,
/// #             label: "Quantile".to_string(),
/// #             color: GREEN,
/// #             dash: true,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(Line {
/// #             x: vec![-100.0, 100.0],
/// #             y: vec![-100.0, 100.0],
/// #             color: GREY,
/// #             force_fit_all: false,
/// #             ..Default::default()
/// #         })
/// #         .with_plottable(HorizontalLine {
/// #             color: GREY,
/// #             y: 1.0,
/// #             ..Default::default()
/// #         })
/// #         .plot(&root)
/// #         .unwrap();
/// #     drop(root);
/// #     rename(
/// #             format!("plot_beta_{a}_{b}.svg"),
/// #             format!("src/distribution/beta/doctest_out/plot_beta_{a}_{b}.svg"),
/// #     )
/// #     .unwrap();
/// # };
/// plot_beta(f64::infinity(), f64::infinity()); // point mass at 1/2
/// ```
#[cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("plot_beta_inf_inf", "src/distribution/beta/doctest_out/plot_beta_inf_inf.svg")))]
#[cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Plot Beta Inf Inf][plot_beta_inf_inf]")
)]
pub struct Beta {
    shape1: Positive64,
    shape2: Positive64,
    ncp: Option<Positive64>,
}

impl Distribution for Beta {
    fn density<R: Into<Real64>>(&self, x: R) -> Real64 {
        if let Some(ncp) = self.ncp {
            dnbeta(x, self.shape1.unwrap(), self.shape2.unwrap(), ncp, false)
        } else {
            dbeta(x, self.shape1, self.shape2, false)
        }
    }

    fn log_density<R: Into<Real64>>(&self, x: R) -> Real64 {
        if let Some(ncp) = self.ncp {
            dnbeta(x, self.shape1.unwrap(), self.shape2.unwrap(), ncp, true)
        } else {
            dbeta(x, self.shape1, self.shape2, true)
        }
    }

    fn probability<R: Into<Real64>>(&self, q: R, lower_tail: bool) -> Probability64 {
        if let Some(ncp) = self.ncp {
            pnbeta(
                q,
                self.shape1.unwrap(),
                self.shape2.unwrap(),
                ncp,
                lower_tail,
            )
        } else {
            pbeta(q, self.shape1, self.shape2, lower_tail)
        }
    }

    fn log_probability<R: Into<Real64>>(&self, q: R, lower_tail: bool) -> LogProbability64 {
        if let Some(ncp) = self.ncp {
            log_pnbeta(
                q,
                self.shape1.unwrap(),
                self.shape2.unwrap(),
                ncp,
                lower_tail,
            )
        } else {
            log_pbeta(q, self.shape1, self.shape2, lower_tail)
        }
    }

    fn quantile<P: Into<Probability64>>(&self, p: P, lower_tail: bool) -> Real64 {
        if let Some(ncp) = self.ncp {
            qnbeta(
                p,
                self.shape1.unwrap(),
                self.shape2.unwrap(),
                ncp,
                lower_tail,
            )
        } else {
            qbeta(p, self.shape1, self.shape2, lower_tail)
        }
    }

    fn log_quantile<LP: Into<LogProbability64>>(&self, p: LP, lower_tail: bool) -> Real64 {
        if let Some(ncp) = self.ncp {
            log_qnbeta(
                p,
                self.shape1.unwrap(),
                self.shape2.unwrap(),
                ncp,
                lower_tail,
            )
        } else {
            log_qbeta(p, self.shape1, self.shape2, lower_tail)
        }
    }

    fn random_sample<R: RNG>(&self, rng: &mut R) -> Real64 {
        if let Some(ncp) = self.ncp {
            rnbeta(self.shape1.unwrap(), self.shape2.unwrap(), ncp, rng)
        } else {
            rbeta(self.shape1, self.shape2, rng)
        }
    }
}

pub struct BetaBuilder {
    shape1: Option<Positive64>,
    shape2: Option<Positive64>,
    ncp: Option<Positive64>,
}

impl BetaBuilder {
    pub fn new() -> Self {
        Self {
            shape1: None,
            shape2: None,
            ncp: None,
        }
    }

    pub fn with_shape1<P: Into<Positive64>>(&mut self, shape1: P) -> &mut Self {
        self.shape1 = Some(shape1.into());
        self
    }

    pub fn with_shape2<P: Into<Positive64>>(&mut self, shape2: P) -> &mut Self {
        self.shape2 = Some(shape2.into());
        self
    }

    pub fn with_ncp<P: Into<Positive64>>(&mut self, ncp: P) -> &mut Self {
        self.ncp = Some(ncp.into());
        self
    }

    pub fn build(&self) -> Beta {
        let shape1 = self.shape1.unwrap_or(1.0.into());
        let shape2 = self.shape2.unwrap_or(1.0.into());

        Beta {
            shape1,
            shape2,
            ncp: self.ncp,
        }
    }
}

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;

#[cfg(all(test, feature = "current"))]
mod covtests;
