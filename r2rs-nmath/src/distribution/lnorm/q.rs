// Translation of nmath's qnorm
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, LogProbability64, Positive64, Probability64, Real64};

use crate::{
    distribution::norm::{log_qnorm, qnorm},
    traits::DPQ,
};

/// This the lognormal quantile function.
pub fn qlnorm<PR: Into<Probability64>, R: Into<Real64>, PO: Into<Positive64>>(
    p: PR,
    meanlog: R,
    sdlog: PO,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qlnorm_inner(p, meanlog, sdlog, lower_tail, false)
}

/// This the lognormal quantile function.
pub fn log_qlnorm<LP: Into<LogProbability64>, R: Into<Real64>, P: Into<Positive64>>(
    p: LP,
    meanlog: R,
    sdlog: P,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qlnorm_inner(p, meanlog, sdlog, lower_tail, true)
}
fn qlnorm_inner<R: Into<Real64>, P: Into<Positive64>>(
    p: f64,
    meanlog: R,
    sdlog: P,
    lower_tail: bool,
    log: bool,
) -> Real64 {
    if let Some(ret) = p.q_p01_boundaries(0.0, f64::infinity(), lower_tail, log) {
        return ret.into();
    }

    let ret = if log {
        log_qnorm(p, meanlog, sdlog, lower_tail).unwrap().exp()
    } else {
        qnorm(p, meanlog, sdlog, lower_tail).unwrap().exp()
    };
    ret.into()
}
