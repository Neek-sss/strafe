// Translation of nmath's rnorm
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Positive64, Real64};

use crate::{distribution::norm::rnorm, traits::RNG};

/// Random variates from the lognormal distribution.
pub fn rlnorm<RE: Into<Real64>, P: Into<Positive64>, R: RNG>(
    meanlog: RE,
    sdlog: P,
    rng: &mut R,
) -> Real64 {
    let meanlog = meanlog.into().unwrap();
    let sdlog = sdlog.into().unwrap();

    if !sdlog.is_finite() {
        return f64::nan().into();
    }

    rnorm(meanlog, sdlog, rng).unwrap().exp().into()
}
