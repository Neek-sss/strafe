// Translation of nmath's pnorm
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, LogProbability64, Positive64, Probability64, Real64};

use crate::{
    distribution::norm::{log_pnorm, pnorm},
    traits::DPQ,
};

/// The lognormal distribution function.
pub fn plnorm<R1: Into<Real64>, R2: Into<Real64>, P: Into<Positive64>>(
    x: R1,
    meanlog: R2,
    sdlog: P,
    lower_tail: bool,
) -> Probability64 {
    plnorm_inner(x, meanlog, sdlog, lower_tail, false).into()
}

/// The lognormal distribution function.
pub fn log_plnorm<R1: Into<Real64>, R2: Into<Real64>, P: Into<Positive64>>(
    x: R1,
    meanlog: R2,
    sdlog: P,
    lower_tail: bool,
) -> LogProbability64 {
    plnorm_inner(x, meanlog, sdlog, lower_tail, true).into()
}

fn plnorm_inner<R1: Into<Real64>, R2: Into<Real64>, P: Into<Positive64>>(
    x: R1,
    meanlog: R2,
    sdlog: P,
    lower_tail: bool,
    log: bool,
) -> f64 {
    let x = x.into().unwrap();
    let meanlog = meanlog.into().unwrap();
    let sdlog = sdlog.into().unwrap();

    if x > 0.0 {
        if log {
            log_pnorm(x.ln(), meanlog, sdlog, lower_tail).unwrap()
        } else {
            pnorm(x.ln(), meanlog, sdlog, lower_tail).unwrap()
        }
    } else {
        f64::dt_0(lower_tail, log)
    }
}
