// Translation of nmath's dsignrank
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Natural64, Real64};

use crate::{distribution::signrank::csignrank, traits::DPQ};

/// The density of the Wilcoxon Signed Rank distribution.
pub fn dsignrank<R: Into<Real64>, N: Into<Natural64>>(x: R, n: N, log: bool) -> Real64 {
    let mut x = x.into().unwrap();
    let mut n = n.into().unwrap();

    let mut d = 0.0;

    n = n.round();

    if (x - x.round()).abs() > 1e-7 {
        return f64::d_0(log).into();
    }
    x = x.round();
    if x < 0.0 || x > n * (n + 1.0) / 2.0 {
        return f64::d_0(log).into();
    }

    let nn = n;
    d = ((csignrank(x as i32, nn as i32).ln()) - n * std::f64::consts::LN_2).d_exp(log);
    return d.into();
}
