// Translation of nmath's rf
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Rational64, Real64};

use crate::{distribution::chisq::rchisq, traits::RNG};

/// Pseudo-random variates from an F distribution.
///
/// This function calls rchisq to do the real work
pub fn rf<RA1: Into<Rational64>, RA2: Into<Rational64>, R: RNG>(
    n1: RA1,
    n2: RA2,
    rng: &mut R,
) -> Real64 {
    let n1 = n1.into().unwrap();
    let n2 = n2.into().unwrap();

    let mut v1 = 0.0;
    let mut v2 = 0.0;

    v1 = if n1.is_finite() {
        rchisq(n1, rng).unwrap() / n1
    } else {
        1.0
    };
    v2 = if n2.is_finite() {
        rchisq(n2, rng).unwrap() / n2
    } else {
        1.0
    };
    (v1 / v2).into()
}
