// Translation of nmath's rnf
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Positive64, Rational64, Real64};

use crate::{
    distribution::chisq::{rchisq, rnchisq},
    traits::RNG,
};

pub fn rnf<RA1: Into<Rational64>, RA2: Into<Rational64>, P: Into<Positive64>, R: RNG>(
    df1: RA1,
    df2: RA2,
    ncp: P,
    rng: &mut R,
) -> Real64 {
    let df1 = df1.into().unwrap();
    let df2 = df2.into().unwrap();

    ((rnchisq(df1, ncp, rng).unwrap() / df1) / (rchisq(df2, rng).unwrap() / df2)).into()
}
