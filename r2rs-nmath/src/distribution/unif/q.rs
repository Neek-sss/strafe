// Translation of nmath's qunif
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{Finite64, FloatConstraint, LogProbability64, Probability64, Real64};

use crate::traits::DPQ;

/// The quantile function of the uniform distribution.
pub fn qunif<P: Into<Probability64>, F1: Into<Finite64>, F2: Into<Finite64>>(
    p: P,
    a: F1,
    b: F2,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qunif_inner(p, a, b, lower_tail, false)
}

/// The quantile function of the uniform distribution.
pub fn log_qunif<LP: Into<LogProbability64>, F1: Into<Finite64>, F2: Into<Finite64>>(
    p: LP,
    a: F1,
    b: F2,
    lower_tail: bool,
) -> Real64 {
    let p = p.into().unwrap();
    qunif_inner(p, a, b, lower_tail, true)
}

fn qunif_inner<F1: Into<Finite64>, F2: Into<Finite64>>(
    p: f64,
    a: F1,
    b: F2,
    lower_tail: bool,
    log: bool,
) -> Real64 {
    let a = a.into().unwrap();
    let b = b.into().unwrap();

    if b < a {
        return f64::nan().into();
    }
    if b == a {
        return a.into();
    }

    (a + p.dt_qiv(lower_tail, log) * (b - a)).into()
}
