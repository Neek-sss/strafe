// Translation of nmath's dunif
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{Finite64, FloatConstraint, Real64};

use crate::traits::DPQ;

/// The density of the uniform distribution.
pub fn dunif<R: Into<Real64>, F1: Into<Finite64>, F2: Into<Finite64>>(
    x: R,
    a: F1,
    b: F2,
    log: bool,
) -> Real64 {
    let x = x.into().unwrap();
    let a = a.into().unwrap();
    let b = b.into().unwrap();

    if b <= a {
        return f64::nan().into();
    }

    if a <= x && x <= b {
        return if log { -(b - a).ln() } else { (1.0) / (b - a) }.into();
    }
    f64::d_0(log).into()
}
