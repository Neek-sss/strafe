// Translation of nmath's runif
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{Finite64, FloatConstraint, Real64};

use crate::traits::RNG;

/// Random variates from the uniform distribution.
pub fn runif<F1: Into<Finite64>, F2: Into<Finite64>, R: RNG>(a: F1, b: F2, rng: &mut R) -> Real64 {
    let a = a.into().unwrap();
    let b = b.into().unwrap();

    return if a == b {
        a
    } else {
        let mut u = 0.0;
        loop
        /* This is true of all builtin generators, but protect against
        user-supplied ones */
        {
            u = rng.unif_rand();
            if !(u <= 0.0 || u >= 1.0) {
                break;
            }
        }
        a + (b - a) * u
    }
    .into();
}
