// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    num_to_fin64,
    proptest::prelude::*,
    proptest_ext::{stat_fin64, stat_lp64, stat_p64, stat_r64, strafe_default_proptest_options},
};
use strafe_type::tof64;

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn density(
        x in stat_r64(None, None, None),
        (lower_bound, upper_bound) in (
            stat_fin64(None, None, None),
            stat_fin64(None, None, None),
        ).prop_map(|(l, u)| if tof64!(l) < tof64!(u) {
            (l, u)
        } else if tof64!(l) > tof64!(u) {
            (u, l)
        } else {
            (
                l,
                num_to_fin64!(tof64!(u) + 1.0),
            )
        })
    ) {
        density_inner(x, lower_bound, upper_bound);
    }

    #[test]
    fn log_density(
        x in stat_r64(None, None, None),
        (lower_bound, upper_bound) in (
            stat_fin64(None, None, None),
            stat_fin64(None, None, None),
        ).prop_map(|(l, u)| if tof64!(l) < tof64!(u) {
            (l, u)
        } else if tof64!(l) > tof64!(u) {
            (u, l)
        } else {
            (
                l,
                num_to_fin64!(tof64!(u) + 1.0),
            )
        })
    ) {
        log_density_inner(x, lower_bound, upper_bound);
    }

    #[test]
    fn probability(
        q in stat_r64(None, None, None),
        (lower_bound, upper_bound) in (
            stat_fin64(None, None, None),
            stat_fin64(None, None, None),
        ).prop_map(|(l, u)| if tof64!(l) < tof64!(u) {
            (l, u)
        } else if tof64!(l) > tof64!(u) {
            (u, l)
        } else {
            (
                l,
                num_to_fin64!(tof64!(u) + 1.0),
            )
        }),
        lower_tail in prop::bool::ANY,
    ) {
        probability_inner(q, lower_bound, upper_bound, lower_tail);
    }

    #[test]
    fn log_probability(
        q in stat_r64(None, None, None),
        (lower_bound, upper_bound) in (
            stat_fin64(None, None, None),
            stat_fin64(None, None, None),
        ).prop_map(|(l, u)| if tof64!(l) < tof64!(u) {
            (l, u)
        } else if tof64!(l) > tof64!(u) {
            (u, l)
        } else {
            (
                l,
                num_to_fin64!(tof64!(u) + 1.0),
            )
        }),
        lower_tail in prop::bool::ANY,
    ) {
        log_probability_inner(q, lower_bound, upper_bound, lower_tail);
    }

    #[test]
    fn quantile(
        p in stat_p64(None, None, None),
        (lower_bound, upper_bound) in (
            stat_fin64(None, None, None),
            stat_fin64(None, None, None),
        ).prop_map(|(l, u)| if tof64!(l) < tof64!(u) {
            (l, u)
        } else if tof64!(l) > tof64!(u) {
            (u, l)
        } else {
            (
                l,
                num_to_fin64!(tof64!(u) + 1.0),
            )
        }),
        lower_tail in prop::bool::ANY,
    ) {
        quantile_inner(p, lower_bound, upper_bound, lower_tail);
    }

    #[test]
    fn log_quantile(
        p in stat_lp64(None, None, None),
        (lower_bound, upper_bound) in (
            stat_fin64(None, None, None),
            stat_fin64(None, None, None),
        ).prop_map(|(l, u)| if tof64!(l) < tof64!(u) {
            (l, u)
        } else if tof64!(l) > tof64!(u) {
            (u, l)
        } else {
            (
                l,
                num_to_fin64!(tof64!(u) + 1.0),
            )
        }),
        lower_tail in prop::bool::ANY,
    ) {
        log_quantile_inner(p, lower_bound, upper_bound, lower_tail);
    }

    #[test]
    fn random(
        seed in 0..std::u16::MAX,
        (lower_bound, upper_bound) in (
            stat_fin64(None, None, None),
            stat_fin64(None, None, None),
        ).prop_map(|(l, u)| if tof64!(l) < tof64!(u) {
            (l, u)
        } else if tof64!(l) > tof64!(u) {
            (u, l)
        } else {
            (
                l,
                num_to_fin64!(tof64!(u) + 1.0),
            )
        })
    ) {
        random_inner(seed, lower_bound, upper_bound);
    }
}
