// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;

use super::tests::*;

#[test]
fn density_test_1() {
    density_inner(1.0, 1.0);
}

#[test]
fn density_test_2() {
    density_inner(1.5, 1.5);
}

#[test]
fn density_test_3() {
    density_inner(3.0, 25.0);
}

#[test]
fn density_test_4() {
    density_inner(3.0, 2.0);
}

#[test]
fn density_test_5() {
    density_inner(-1.0, 2.0);
}

#[test]
fn density_test_6() {
    density_inner(0.0, 0.0);
}

#[test]
fn density_test_7() {
    density_inner(1.0, 0.0);
}

#[test]
fn density_test_8() {
    density_inner(-3.0, 2.0);
}

#[test]
fn density_test_9() {
    density_inner(3.0, 4.0);
}

#[test]
fn density_test_10() {
    density_inner(4.0, 3.0);
}

#[test]
fn log_density_test_1() {
    log_density_inner(1.0, 1.0);
}

#[test]
fn log_density_test_2() {
    log_density_inner(1.0, f64::infinity());
}

#[test]
fn probability_test_1() {
    probability_inner(1.0, 1.0, false);
}

#[test]
fn probability_test_2() {
    probability_inner(-1.0, 1.0, false);
}

#[test]
fn probability_test_3() {
    probability_inner(1.0, 0.0, false);
}

#[test]
fn probability_test_4() {
    probability_inner(f64::infinity(), 1.0, false);
}

#[test]
fn log_probability_test_1() {
    log_probability_inner(1.0, 1.0, false);
}

#[test]
fn log_probability_test_2() {
    log_probability_inner(-1.0, 1.0, false);
}

#[test]
fn log_probability_test_3() {
    log_probability_inner(1.0, 0.0, false);
}

#[test]
fn log_probability_test_4() {
    log_probability_inner(f64::infinity(), 1.0, false);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1.0, 1.0, false);
}

#[test]
fn quantile_test_2() {
    quantile_inner(1.0, 0.0, false);
}

#[test]
fn quantile_test_5() {
    quantile_inner(0.0, 2.0, false);
}

#[test]
fn log_quantile_test_1() {
    log_quantile_inner(-1.0, 1.0, false);
}

#[test]
fn log_quantile_test_2() {
    log_quantile_inner(-1.0, 1e7, false);
}

#[test]
fn random_test_1() {
    random_inner(1, 1.0);
}

#[test]
fn random_test_2() {
    random_inner(1, 1.5);
}

#[test]
fn random_test_3() {
    random_inner(3, 25.0);
}

#[test]
fn random_test_4() {
    random_inner(3, 0.0);
}
