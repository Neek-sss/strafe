// Translation of nmath's ppois
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, LogProbability64, Positive64, Probability64, Real64};

use crate::{
    distribution::gamma::{log_pgamma, pgamma},
    traits::DPQ,
};

/// The distribution function of the Poisson distribution.
pub fn ppois<R: Into<Real64>, P: Into<Positive64>>(
    x: R,
    lambda: P,
    lower_tail: bool,
) -> Probability64 {
    ppois_inner(x, lambda, lower_tail, false).into()
}

/// The distribution function of the Poisson distribution.
pub fn log_ppois<R: Into<Real64>, P: Into<Positive64>>(
    x: R,
    lambda: P,
    lower_tail: bool,
) -> LogProbability64 {
    ppois_inner(x, lambda, lower_tail, true).into()
}

fn ppois_inner<R: Into<Real64>, P: Into<Positive64>>(
    x: R,
    lambda: P,
    lower_tail: bool,
    log: bool,
) -> f64 {
    let mut x = x.into().unwrap();
    let lambda = lambda.into().unwrap();

    if x < 0.0 {
        return f64::dt_0(lower_tail, log);
    }
    if lambda == 0.0 {
        return f64::dt_1(lower_tail, log);
    }
    if !x.is_finite() {
        return f64::dt_1(lower_tail, log);
    }
    x = (x + 1e-7).floor();

    if log {
        log_pgamma(lambda, x + 1.0, 1.0, !lower_tail).into()
    } else {
        pgamma(lambda, x + 1.0, 1.0, !lower_tail).into()
    }
}
