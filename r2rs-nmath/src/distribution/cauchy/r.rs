// Translation of nmath's rcauchy
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Natural64, Real64};

use crate::traits::RNG;

/// Random variates from the Cauchy distribution.
pub fn rcauchy<RE: Into<Real64>, N: Into<Natural64>, R: RNG>(
    location: RE,
    scale: N,
    rng: &mut R,
) -> Real64 {
    let location = location.into().unwrap();
    let scale = scale.into().unwrap();

    if !scale.is_finite() {
        return f64::nan().into();
    }
    if !location.is_finite() {
        location
    } else {
        location + scale * (std::f64::consts::PI * rng.unif_rand()).tan()
    }
    .into()
}
