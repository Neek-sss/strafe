// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_lp64, stat_n64, stat_p64, stat_r64, strafe_default_proptest_options},
};

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn density(
        x in stat_r64(None, None, None),
        location in stat_r64(None, None, None),
        scale in stat_n64(None, None, None),
    ) {
        density_inner(x, location, scale);
    }

    #[test]
    fn log_density(
        x in stat_r64(None, None, None),
        location in stat_r64(None, None, None),
        scale in stat_n64(None, None, None),
    ) {
        log_density_inner(x, location, scale);
    }

    #[test]
    fn probability(
        q in stat_r64(None, None, None),
        location in stat_r64(None, None, None),
        scale in stat_n64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        probability_inner(q, location, scale, lower_tail);
    }

    #[test]
    fn log_probability(
        q in stat_r64(None, None, None),
        location in stat_r64(None, None, None),
        scale in stat_n64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        log_probability_inner(q, location, scale, lower_tail);
    }

    #[test]
    fn quantile(
        p in stat_p64(None, None, None),
        location in stat_r64(None, None, None),
        scale in stat_n64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        quantile_inner(p, location, scale, lower_tail);
    }

    #[test]
    fn log_quantile(
        p in stat_lp64(None, None, None),
        location in stat_r64(None, None, None),
        scale in stat_n64(None, None, None),
        lower_tail in prop::bool::ANY,
    ) {
        log_quantile_inner(p, location, scale, lower_tail);
    }

    #[test]
    fn random(
        seed in 0..std::u16::MAX,
        location in stat_r64(None, None, None),
        scale in stat_n64(None, None, None),
    ) {
        random_inner(seed, location, scale);
    }
}
