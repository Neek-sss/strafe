// Translation of nmath's dcauchy
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Natural64, Real64};

/// The density of the Cauchy distribution.
pub fn dcauchy<R1: Into<Real64>, R2: Into<Real64>, N: Into<Natural64>>(
    x: R1,
    location: R2,
    scale: N,
    log: bool,
) -> Real64 {
    let x = x.into().unwrap();
    let location = location.into().unwrap();
    let scale = scale.into().unwrap();

    let mut y = 0.0;

    y = (x - location) / scale;
    if log {
        -(std::f64::consts::PI * scale * (1.0 + y * y)).ln()
    } else {
        1.0 / (std::f64::consts::PI * scale * (1.0 + y * y))
    }
    .into()
}
