// Translation of nmath's rlogis
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Rational64, Real64};

use crate::traits::RNG;

pub fn rlogis<RE: Into<Real64>, RA: Into<Rational64>, R: RNG>(
    location: RE,
    scale: RA,
    rng: &mut R,
) -> Real64 {
    let location = location.into().unwrap();
    let scale = scale.into().unwrap();

    if !scale.is_finite() {
        return f64::nan().into();
    }

    if !location.is_finite() {
        location
    } else {
        let u = rng.unif_rand();
        location + scale * (u / (1.0 - u)).ln()
    }
    .into()
}
