// Translation of nmath's dlogis
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Rational64, Real64};

/// Logistic Distribution
pub fn dlogis<RE1: Into<Real64>, RE2: Into<Real64>, RA: Into<Rational64>>(
    x: RE1,
    location: RE2,
    scale: RA,
    log: bool,
) -> Real64 {
    let mut x = x.into().unwrap();
    let location = location.into().unwrap();
    let scale = scale.into().unwrap();

    let mut e = 0.0;
    let mut f = 0.0;

    x = ((x - location) / scale).abs();
    e = (-x).exp();
    f = 1.0 + e;

    if log {
        -(x + (scale * f * f).ln())
    } else {
        (e) / (scale * f * f)
    }
    .into()
}
