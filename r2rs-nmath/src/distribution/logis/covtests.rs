// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;

use super::tests::*;

#[test]
fn density_test_1() {
    density_inner(1.0, 1.0, 1.0);
}

#[test]
fn log_density_test_1() {
    log_density_inner(1.0, 1.0, 1.0);
}

#[test]
fn probability_test_1() {
    probability_inner(f64::neg_infinity(), 1.0, 1.0, false);
}

#[test]
fn probability_test_2() {
    probability_inner(f64::infinity(), 1.0, 1.0, false);
}

#[test]
fn probability_test_3() {
    probability_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn log_probability_test_1() {
    log_probability_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn log_probability_test_2() {
    log_probability_inner(30.0, 1.0, 1.0, false);
}

#[test]
fn log_probability_test_3() {
    log_probability_inner(50.0, 1.0, 1.0, false);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1.0, 1.0, 1.0, false);
}

#[test]
fn quantile_test_2() {
    quantile_inner(1.0, 0.0, 1.0, false);
}

#[test]
fn quantile_test_3() {
    quantile_inner(0.75, 1.0, 1.0, false);
}

// TODO
// #[test]
// fn quantile_test_4() {
//     quantile_inner(0.75, 1.0, 1.0, true);
// }

#[test]
fn log_quantile_test_1() {
    log_quantile_inner(-1.0, 1.0, 1.0, false);
}

#[test]
fn log_quantile_test_2() {
    log_quantile_inner(-1.0, 1.0, 1.0, true);
}

#[test]
fn random_test_1() {
    random_inner(1, 1.0, 1.0);
}

#[test]
fn random_test_2() {
    random_inner(1, f64::infinity(), 1.0);
}
