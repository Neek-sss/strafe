// Translation of nmath's qchisq
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, LogProbability64, Probability64, Rational64, Real64};

use crate::distribution::gamma::{log_qgamma, qgamma};

/// The quantile function of the chi-squared distribution.
pub fn qchisq<P: Into<Probability64>, R: Into<Rational64>>(
    p: P,
    df: R,
    lower_tail: bool,
) -> Real64 {
    qgamma(p, 0.5 * df.into().unwrap(), 2.0, lower_tail)
}

/// The quantile function of the chi-squared distribution.
pub fn log_qchisq<LP: Into<LogProbability64>, R: Into<Rational64>>(
    p: LP,
    df: R,
    lower_tail: bool,
) -> Real64 {
    log_qgamma(p, 0.5 * df.into().unwrap(), 2.0, lower_tail)
}
