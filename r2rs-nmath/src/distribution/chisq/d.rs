// Translation of nmath's dchisq
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Rational64, Real64};

use crate::distribution::gamma::dgamma;

/// The density of the chi-squared distribution.
pub fn dchisq<RE: Into<Real64>, RA: Into<Rational64>>(x: RE, df: RA, log: bool) -> Real64 {
    dgamma(x, df.into().unwrap() / 2.0, 2.0, log)
}
