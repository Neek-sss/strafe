// Translation of nmath's rnchisq
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Positive64, Rational64, Real64};

use crate::{
    distribution::{chisq::rchisq, gamma::rgamma, pois::rpois},
    traits::RNG,
};

/// Random variates from the NON CENTRAL chi-squared distribution.
///
/// According to Hans R. Kuensch's suggestion (30 sep 2002):
///
/// It should be easy to do the general case (ncp > 0) by decomposing it
/// as the sum of a central chisquare with df degrees of freedom plus a
/// noncentral chisquare with zero degrees of freedom (which is a Poisson
/// mixture of central chisquares with integer degrees of freedom),
/// see Formula (29.5b-c) in Johnson, Kotz, Balakrishnan (1995).
///
/// The noncentral chisquare with arbitrary degrees of freedom is of interest
/// for simulating the Cox-Ingersoll-Ross model for interest rates in
/// finance.
pub fn rnchisq<RA: Into<Rational64>, P: Into<Positive64>, R: RNG>(
    df: RA,
    lambda: P,
    rng: &mut R,
) -> Real64 {
    let df = df.into().unwrap();
    let lambda = lambda.into().unwrap();

    if !lambda.is_finite() {
        return f64::nan().into();
    }

    if lambda == 0.0 {
        rgamma(df / 2.0, 2.0, rng)
    } else {
        let mut r = rpois(lambda / 2.0, rng).unwrap();
        if r > 0.0 {
            r = rchisq(2.0 * r, rng).unwrap()
        }
        if df > 0.0 {
            r += rgamma(df / 2.0, 2.0, rng).unwrap()
        }
        r.into()
    }
}
