// Translation of nmath's pchisq
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, LogProbability64, Probability64, Rational64, Real64};

use crate::distribution::gamma::{log_pgamma, pgamma};

/// The distribution function of the chi-squared distribution.
pub fn pchisq<RE: Into<Real64>, RA: Into<Rational64>>(
    x: RE,
    df: RA,
    lower_tail: bool,
) -> Probability64 {
    pgamma(x, df.into().unwrap() / 2.0, 2.0, lower_tail)
}

/// The distribution function of the chi-squared distribution.
pub fn log_pchisq<RE: Into<Real64>, RA: Into<Rational64>>(
    x: RE,
    df: RA,
    lower_tail: bool,
) -> LogProbability64 {
    log_pgamma(x, df.into().unwrap() / 2.0, 2.0, lower_tail)
}
