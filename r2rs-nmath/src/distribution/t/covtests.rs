// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;

use super::tests::*;

#[test]
fn density_test_1() {
    density_inner(1.0, 1.0);
}

#[test]
fn density_test_2() {
    density_inner(f64::infinity(), 1.0);
}

#[test]
fn density_test_3() {
    density_inner(1.0, f64::infinity());
}

#[test]
fn density_test_4() {
    density_inner(1e127, 1.0);
}

#[test]
fn density_test_5() {
    density_inner(5.0, 1000.0);
}

#[test]
fn log_density_test_1() {
    log_density_inner(1.0, 1.0);
}

#[test]
fn probability_test_1() {
    probability_inner(1.0, 1.0, false);
}

#[test]
fn probability_test_2() {
    probability_inner(f64::neg_infinity(), 1.0, false);
}

#[test]
fn probability_test_3() {
    probability_inner(f64::infinity(), 1.0, false);
}

#[test]
fn probability_test_4() {
    probability_inner(1.0, f64::infinity(), false);
}

#[test]
fn probability_test_5() {
    probability_inner(2127.51, 54.0, false);
}

#[test]
fn probability_test_6() {
    probability_inner(1e100, 5.0, false);
}

#[test]
fn probability_test_7() {
    probability_inner(-2127.51, 54.0, false);
}

#[test]
fn probability_test_8() {
    probability_inner(7.0, 65.0, false);
}

#[test]
fn log_probability_test_1() {
    log_probability_inner(1.0, 1.0, false);
}

#[test]
fn log_probability_test_2() {
    log_probability_inner(f64::infinity(), 1.0, false);
}

#[test]
fn log_probability_test_3() {
    log_probability_inner(f64::neg_infinity(), 1.0, false);
}

#[test]
fn log_probability_test_4() {
    log_probability_inner(1e100, 5.0, false);
}

#[test]
fn log_probability_test_5() {
    log_probability_inner(7.0, 65.0, false);
}

#[test]
fn log_probability_test_6() {
    log_probability_inner(-2127.51, 54.0, false);
}

#[test]
fn log_probability_test_7() {
    log_probability_inner(-1.0, 1.0, false);
}

#[test]
fn log_probability_test_8() {
    log_probability_inner(21.0, f64::infinity(), false);
}

#[test]
fn quantile_test_1() {
    quantile_inner(1.0, 1.0, false);
}

#[test]
fn quantile_test_2() {
    quantile_inner(0.5, 1e21, false);
}

#[test]
fn quantile_test_3() {
    quantile_inner(0.5, 12.0, false);
}

#[test]
fn quantile_test_4() {
    quantile_inner(0.75, 12.0, true);
}

#[test]
fn quantile_test_5() {
    quantile_inner(0.75, 12.0, false);
}

#[test]
fn quantile_test_6() {
    quantile_inner(0.75, 2.0 + 1e-25, true);
}

#[test]
fn quantile_test_7() {
    quantile_inner(0.75, 1.0 + 1e-25, true);
}

#[test]
fn quantile_test_8() {
    quantile_inner(0.5, 2.0 + 1e-25, true);
}

#[test]
fn quantile_test_9() {
    quantile_inner(0.5, 1.0 + 1e-25, true);
}

#[test]
fn quantile_test_10() {
    quantile_inner(1e-23, 2.0 + 1e-25, true);
}

#[test]
fn quantile_test_11() {
    quantile_inner(0.25, 1.1, true);
}

#[test]
fn quantile_test_12() {
    quantile_inner(1e-25, 1.1, true);
}

#[test]
fn quantile_test_13() {
    quantile_inner(1.0 - 1e-25, 1.1, true);
}

#[test]
fn quantile_test_14() {
    quantile_inner(0.5, 0.5, true);
}

#[test]
fn quantile_test_15() {
    quantile_inner(1.0 - 1e-14, 0.5, true);
}

#[test]
fn log_quantile_test_1() {
    log_quantile_inner(-1.0, 1.0, false);
}

#[test]
fn log_quantile_test_2() {
    log_quantile_inner(f64::neg_infinity(), 1.0, false);
}

#[test]
fn log_quantile_test_3() {
    log_quantile_inner(-1.0, 0.5, false);
}

#[test]
fn log_quantile_test_4() {
    log_quantile_inner(-1.0, 1e30, false);
}

#[test]
fn log_quantile_test_5() {
    log_quantile_inner(-1.0, 1.0 + 1e-25, false);
}

#[test]
fn log_quantile_test_6() {
    log_quantile_inner(-1.0, 2.0 + 1e-25, false);
}

#[test]
fn log_quantile_test_7() {
    log_quantile_inner(-1.0, 8.5, false);
}

#[test]
fn log_quantile_test_8() {
    log_quantile_inner(-1.0, 1.0 + 1e-25, true);
}

#[test]
fn log_quantile_test_9() {
    log_quantile_inner(-1.0, 2.0 + 1e-25, true);
}

#[test]
fn log_quantile_test_10() {
    log_quantile_inner(-1.0, 1.9, true);
}

#[test]
fn log_quantile_test_11() {
    log_quantile_inner(0.0, 2.8, true);
}

#[test]
fn log_quantile_test_12() {
    log_quantile_inner(-200.0, 10.2, false);
}

#[test]
fn random_test_1() {
    random_inner(1, 1.0);
}

#[test]
fn random_test_2() {
    random_inner(1, f64::infinity());
}
