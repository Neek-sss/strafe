// Translation of nmath's dnt
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Rational64, Real64};

use crate::{
    distribution::{
        norm::dnorm,
        t::{dt, pnt},
    },
    func::lgamma,
    traits::DPQ,
};

/// From Johnson, Kotz and Balakrishnan (1995) \[2nd ed.; formula (31.15), p.516\],
/// the non-central t density is
///
/// f(x, df, ncp) =
/// exp(-.5*ncp^2) * gamma((df+1)/2) / (sqrt(pi*df)* gamma(df/2)) * (df/(df+x^2))^((df+1)/2) *
/// sum_{j=0}^Inf  gamma((df+j+1)/2)/(factorial(j)* gamma((df+1)/2)) * (x*ncp*sqrt(2)/sqrt(df+x^2))^ j
///
/// is used to evaluate the density at x != 0 and
///
/// f(0, df, ncp) = (-.5*ncp^2).exp() / (pi.sqrt()*df.sqrt()*gamma(df/2))*gamma((df+1)/2)
///
/// The functional relationship
///
/// f(x, df, ncp) = df/x * (F(sqrt((df+2)/df)*x, df+2, ncp) - F(x, df, ncp))
///
/// is used to evaluate the density at x != 0 and
///
/// f(0, df, ncp) = exp(-.5*ncp^2) / (sqrt(pi)*sqrt(df)*gamma(df/2))*gamma((df+1)/2)
///
/// is used for x=0.
///
/// All calculations are done on log-scale to increase stability.
///
/// FIXME: pnt() is known to be inaccurate in the (very) left tail and for ncp > 38
/// ==> use a direct log-space summation formula in that case
pub fn dnt<RE1: Into<Real64>, RA: Into<Rational64>, RE2: Into<Real64>>(
    x: RE1,
    df: RA,
    ncp: RE2,
    log: bool,
) -> Real64 {
    let x = x.into().unwrap();
    let df = df.into().unwrap();
    let ncp = ncp.into().unwrap();

    let mut u = 0.0;

    if ncp == 0.0 {
        return dt(x, df, log);
    }

    /* If x is infinite then return 0 */
    if !x.is_finite() {
        return f64::d_0(log).into();
    }

    /* If infinite df then the density is identical to a
       normal distribution with mean = ncp.  However, the formula
       loses a lot of accuracy around df=1e9
    */
    if !df.is_finite() || df > 1e8 {
        return dnorm(x, ncp, 1.0, log);
    }

    /* Do calculations on log scale to stabilize */
    /* Consider two cases: x ~= 0 or not */
    if x.abs() > (df * 2.2204460492503131e-16).sqrt() {
        u = df.ln() - x.abs().ln()
            + (pnt(x * ((df + 2.0) / df).sqrt(), df + 2.0, ncp, true).unwrap()
                - pnt(x, df, ncp, true).unwrap())
            .abs()
            .ln()
    /* FIXME: the above still suffers from cancellation (but not horribly) */
    } else {
        /* x ~= 0 : -> same value as for  x = 0 */
        u = lgamma((df + 1.0) / 2.0).unwrap()
            - lgamma(df / 2.0).unwrap()
            - (strafe_consts::LN_SQRT_PI + 0.5 * (df.ln() + ncp * ncp))
    }

    if log { u } else { u.exp() }.into()
}
