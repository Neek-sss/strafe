// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal, r_assert_relative_equal_result,
};
use strafe_type::FloatConstraint;

use crate::{
    distribution::TBuilder,
    reset_statics,
    rng::MarsagliaMulticarry,
    traits::{Distribution, RNG},
};

pub fn density_inner(x: f64, df: f64, ncp: f64) {
    let mut builder = TBuilder::new();
    builder.with_df(df).unwrap();
    builder.with_ncp(ncp);
    let nt = builder.build();

    let rust_ans = nt.density(x);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "dt({}, {}, ncp={}, log={})",
            RString::from_f64(x),
            RString::from_f64(df),
            RString::from_f64(ncp),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_density_inner(x: f64, df: f64, ncp: f64) {
    let mut builder = TBuilder::new();
    builder.with_df(df).unwrap();
    builder.with_ncp(ncp);
    let nt = builder.build();

    let rust_ans = nt.log_density(x);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "dt({}, {}, ncp={}, log={})",
            RString::from_f64(x),
            RString::from_f64(df),
            RString::from_f64(ncp),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn probability_inner(q: f64, df: f64, ncp: f64, lower_tail: bool) {
    let mut builder = TBuilder::new();
    builder.with_df(df).unwrap();
    builder.with_ncp(ncp);
    let nt = builder.build();

    let rust_ans = nt.probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pt({}, {}, ncp={}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(df),
            RString::from_f64(ncp),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_probability_inner(q: f64, df: f64, ncp: f64, lower_tail: bool) {
    let mut builder = TBuilder::new();
    builder.with_df(df).unwrap();
    builder.with_ncp(ncp);
    let nt = builder.build();

    let rust_ans = nt.log_probability(q, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "pt({}, {}, ncp={}, lower.tail={}, log={})",
            RString::from_f64(q),
            RString::from_f64(df),
            RString::from_f64(ncp),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn quantile_inner(p: f64, df: f64, ncp: f64, lower_tail: bool) {
    let mut builder = TBuilder::new();
    builder.with_df(df).unwrap();
    builder.with_ncp(ncp);
    let nt = builder.build();

    let rust_ans = nt.quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qt({}, {}, ncp={}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(df),
            RString::from_f64(ncp),
            RString::from_bool(lower_tail),
            RString::from_bool(false),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn log_quantile_inner(p: f64, df: f64, ncp: f64, lower_tail: bool) {
    let mut builder = TBuilder::new();
    builder.with_df(df).unwrap();
    builder.with_ncp(ncp);
    let nt = builder.build();

    let rust_ans = nt.log_quantile(p, lower_tail);
    let r_ans = RTester::new()
        .set_display(&RString::from_string(format!(
            "qt({}, {}, ncp={}, lower.tail={}, log={})",
            RString::from_f64(p),
            RString::from_f64(df),
            RString::from_f64(ncp),
            RString::from_bool(lower_tail),
            RString::from_bool(true),
        )))
        .run()
        .expect("R running error")
        .as_f64();

    r_assert_relative_equal_result!(r_ans, &rust_ans);
}

pub fn random_inner(seed: u16, df: f64, ncp: f64) {
    let num = 1;

    let r_state = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_str(".Random.seed").unwrap())
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap()
        .into_iter()
        .skip(1)
        .map(|i| i as i32)
        .collect::<Vec<_>>();

    let mut rust_rng = MarsagliaMulticarry::new();
    rust_rng.set_seed(seed as u32);

    let rust_state = rust_rng.get_state();

    for (r, rust) in r_state.iter().zip(rust_state.iter()) {
        assert_eq!(r, rust, "States not equal!");
    }

    let r_ret = RTester::new()
        .set_seed(seed)
        .set_display(&RString::from_string(format!(
            "rt({}, {}, ncp={})",
            RString::from_f64(num),
            RString::from_f64(df),
            RString::from_f64(ncp),
        )))
        .run()
        .unwrap()
        .as_f64_vec()
        .unwrap();

    let mut builder = TBuilder::new();
    builder.with_df(df).unwrap();
    builder.with_ncp(ncp);
    let nt = builder.build();
    reset_statics();
    let rust_ret = (0..num)
        .map(|_| nt.random_sample(&mut rust_rng))
        .collect::<Vec<_>>();

    for (r, rust) in r_ret.iter().zip(rust_ret.iter()) {
        r_assert_relative_equal!(*r, rust.unwrap());
    }
}
