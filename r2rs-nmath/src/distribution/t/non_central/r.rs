// Translation of nmath's rnt
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, Rational64, Real64};

use crate::{
    distribution::{chisq::rchisq, norm::rnorm},
    traits::RNG,
};

pub fn rnt<RA: Into<Rational64>, RE: Into<Real64>, R: RNG>(df: RA, ncp: RE, rng: &mut R) -> Real64 {
    let df = df.into().unwrap();

    (rnorm(ncp, 1.0, rng).unwrap() / (rchisq(df, rng).unwrap() / df).sqrt()).into()
}
