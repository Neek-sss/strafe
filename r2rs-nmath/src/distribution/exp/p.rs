// Translation of nmath's pexp
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, LogProbability64, Positive64, Probability64, Real64};

use crate::traits::DPQ;

/// The distribution function of the exponential distribution.
pub fn pexp<R: Into<Real64>, P: Into<Positive64>>(
    x: R,
    scale: P,
    lower_tail: bool,
) -> Probability64 {
    pexp_inner(x, scale, lower_tail, false).into()
}

/// The distribution function of the exponential distribution.
pub fn log_pexp<R: Into<Real64>, P: Into<Positive64>>(
    x: R,
    scale: P,
    lower_tail: bool,
) -> LogProbability64 {
    pexp_inner(x, scale, lower_tail, true).into()
}

fn pexp_inner<R: Into<Real64>, P: Into<Positive64>>(
    x: R,
    scale: P,
    lower_tail: bool,
    log: bool,
) -> f64 {
    let mut x = x.into().unwrap();
    let scale = scale.into().unwrap();

    if x <= 0.0 {
        return f64::dt_0(lower_tail, log);
    }
    /* same as weibull( shape = 1): */
    x = -(x / scale);
    if log {
        if lower_tail {
            x.log1_exp()
        } else {
            x.d_exp(true)
        }
    } else if lower_tail {
        -x.exp_m1()
    } else {
        x.d_exp(false)
    }
}
