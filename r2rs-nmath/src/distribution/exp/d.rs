// Translation of nmath's dexp
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Positive64, Real64};

use crate::traits::DPQ;

/// The density of the exponential distribution.
pub fn dexp<R: Into<Real64>, P: Into<Positive64>>(x: R, scale: P, log: bool) -> Real64 {
    let x = x.into().unwrap();
    let scale = scale.into().unwrap();

    if scale <= 0.0 {
        return f64::nan().into();
    }

    if x < 0.0 {
        return f64::d_0(log).into();
    }
    if log {
        (-x / scale) - scale.ln()
    } else {
        ((-x / scale).exp()) / scale
    }
    .into()
}
