// Translation of nmath's qexp
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strafe_type::{FloatConstraint, LogProbability64, Positive64, Probability64, Real64};

use crate::traits::DPQ;

/// The quantile function of the exponential distribution.
pub fn qexp<PR: Into<Probability64>, PO: Into<Positive64>>(
    p: PR,
    scale: PO,
    lower_tail: bool,
) -> Real64 {
    qexp_inner(p.into().unwrap(), scale, lower_tail, false)
}

/// The quantile function of the exponential distribution.
pub fn log_qexp<LP: Into<LogProbability64>, PO: Into<Positive64>>(
    p: LP,
    scale: PO,
    lower_tail: bool,
) -> Real64 {
    qexp_inner(p.into().unwrap(), scale, lower_tail, true)
}

fn qexp_inner<P: Into<Positive64>>(p: f64, scale: P, lower_tail: bool, log: bool) -> Real64 {
    if p == f64::dt_0(lower_tail, log) {
        return 0.0.into();
    }

    (-scale.into().unwrap() * p.dt_clog(lower_tail, log)).into()
}
