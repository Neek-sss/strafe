use strafe_type::{LogProbability64, Probability64, Real64};

use crate::traits::RNG;

/// A numerical distribution from a statistical viewpoint
pub trait Distribution {
    /// The density of the values at a given point
    fn density<R: Into<Real64>>(&self, x: R) -> Real64;
    /// The logarithmic density of the values at a given point
    fn log_density<R: Into<Real64>>(&self, x: R) -> Real64;
    /// PDF; The probability that a value is found in a distribution (inverse of quantile)
    fn probability<R: Into<Real64>>(&self, q: R, lower_tail: bool) -> Probability64;
    /// log(PDF); The logarithmic probability that a value is found in a distribution (inverse of quantile)
    fn log_probability<R: Into<Real64>>(&self, q: R, lower_tail: bool) -> LogProbability64;
    /// The value in the distribution that is associated with a probability (inverse of probability)
    fn quantile<P: Into<Probability64>>(&self, p: P, lower_tail: bool) -> Real64;
    /// The logarithmic value in the distribution that is associated with a probability (inverse of probability)
    fn log_quantile<LP: Into<LogProbability64>>(&self, p: LP, lower_tail: bool) -> Real64;
    /// Generates a random sample from the distribution
    fn random_sample<R: RNG>(&self, rng: &mut R) -> Real64;
}
