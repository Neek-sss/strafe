mod cospi;

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;

use cospi::{cospi, sinpi, tanpi};

pub trait TrigPI {
    fn cos_pi(self: &Self) -> Self;
    fn sin_pi(self: &Self) -> Self;
    fn tan_pi(self: &Self) -> Self;
}

impl TrigPI for f64 {
    fn cos_pi(&self) -> Self {
        cospi(*self)
    }

    fn sin_pi(&self) -> Self {
        sinpi(*self)
    }

    fn tan_pi(&self) -> Self {
        tanpi(*self)
    }
}
