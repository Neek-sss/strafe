// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;

use super::tests::*;

#[test]
fn cos_pi_test_1() {
    cos_pi_test_inner(1.0);
}

#[test]
fn cos_pi_test_2() {
    cos_pi_test_inner(f64::nan());
}

#[test]
fn cos_pi_test_3() {
    cos_pi_test_inner(f64::infinity());
}

#[test]
fn cos_pi_test_4() {
    cos_pi_test_inner(1.5);
}

#[test]
fn cos_pi_test_5() {
    cos_pi_test_inner(0.0);
}

#[test]
fn cos_pi_test_6() {
    cos_pi_test_inner(1.7);
}

#[test]
fn sin_pi_test_1() {
    sin_pi_test_inner(1.0);
}

#[test]
fn sin_pi_test_2() {
    sin_pi_test_inner(f64::nan());
}

#[test]
fn sin_pi_test_3() {
    sin_pi_test_inner(f64::infinity());
}

#[test]
fn sin_pi_test_4() {
    sin_pi_test_inner(-1.0);
}

#[test]
fn sin_pi_test_5() {
    sin_pi_test_inner(2.0);
}

#[test]
fn sin_pi_test_6() {
    sin_pi_test_inner(0.5);
}

#[test]
fn sin_pi_test_7() {
    sin_pi_test_inner(-0.5);
}

#[test]
fn sin_pi_test_8() {
    sin_pi_test_inner(1.7);
}

#[test]
fn tan_pi_test_1() {
    tan_pi_test_inner(1.0);
}

#[test]
fn tan_pi_test_2() {
    tan_pi_test_inner(f64::nan());
}

#[test]
fn tan_pi_test_3() {
    tan_pi_test_inner(f64::infinity());
}

#[test]
fn tan_pi_test_4() {
    tan_pi_test_inner(-0.5);
}

#[test]
fn tan_pi_test_5() {
    tan_pi_test_inner(0.7);
}

#[test]
fn tan_pi_test_6() {
    tan_pi_test_inner(0.5);
}
