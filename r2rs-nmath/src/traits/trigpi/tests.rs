// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal,
};

use crate::traits::TrigPI;

pub fn cos_pi_test_inner(x: f64) {
    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "cospi({})",
            RString::from_f64(x),
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = x.cos_pi();

    r_assert_relative_equal!(r_ret, rust_ret);
}

pub fn sin_pi_test_inner(x: f64) {
    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "sinpi({})",
            RString::from_f64(x),
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = x.sin_pi();

    r_assert_relative_equal!(r_ret, rust_ret);
}

pub fn tan_pi_test_inner(x: f64) {
    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "tanpi({})",
            RString::from_f64(x),
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    let rust_ret = x.tan_pi();

    r_assert_relative_equal!(r_ret, rust_ret);
}
