// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

mod fprec;
mod fround;

use self::{fprec::fprec, fround::fround};

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;

pub trait DigitPrec {
    fn precision_round_to(&self, precision: usize) -> Self;
    fn round_to(&self, digits: isize) -> Self;
}

impl DigitPrec for f64 {
    fn precision_round_to(&self, precision: usize) -> Self {
        fprec(*self, precision)
    }

    fn round_to(&self, digits: isize) -> Self {
        fround(*self, digits)
    }
}
