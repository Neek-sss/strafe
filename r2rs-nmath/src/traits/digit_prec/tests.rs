// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal,
};

use crate::traits::DigitPrec;

pub fn precision_round_to_test_inner(x: f64, digits: usize) {
    let rust_ret = x.precision_round_to(digits);
    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "signif({}, {})",
            RString::from_f64(x),
            RString::from_f64(digits),
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    r_assert_relative_equal!(r_ret, rust_ret);
}

pub fn round_to_test_inner(x: f64, digits: isize) {
    let rust_ret = x.round_to(digits);
    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "round({}, {})",
            RString::from_f64(x),
            RString::from_f64(digits),
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    r_assert_relative_equal!(r_ret, rust_ret);
}

pub fn round_test_inner(x: f64) {
    let rust_ret = x.round();
    let r_ret = RTester::new()
        .set_display(&RString::from_string(format!(
            "round({})",
            RString::from_f64(x),
        )))
        .run()
        .unwrap()
        .as_f64()
        .unwrap();

    r_assert_relative_equal!(r_ret, rust_ret);
}
