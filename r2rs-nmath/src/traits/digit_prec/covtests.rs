// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;

use super::tests::*;

#[test]
fn precision_round_to_test_1() {
    precision_round_to_test_inner(1543283045408.545235840840, 3)
}

#[test]
fn precision_round_to_test_2() {
    precision_round_to_test_inner(1543283045408.545235840840, 0)
}

#[test]
fn precision_round_to_test_3() {
    precision_round_to_test_inner(0.0, 3)
}

#[test]
fn precision_round_to_test_4() {
    precision_round_to_test_inner(f64::max_value(), 3)
}

#[test]
fn precision_round_to_test_5() {
    precision_round_to_test_inner(f64::nan(), 3)
}

#[test]
fn precision_round_to_test_6() {
    precision_round_to_test_inner(f64::infinity(), 3)
}

#[test]
fn precision_round_to_test_7() {
    precision_round_to_test_inner(1543283045408.545235840840, 32)
}

#[test]
fn precision_round_to_test_8() {
    precision_round_to_test_inner(-1543283045408.545235840840, 3)
}

#[test]
fn round_to_test_1() {
    round_to_test_inner(12540248064.554036548528425, 3)
}

#[test]
fn round_to_test_2() {
    round_to_test_inner(12540248064.554036548528425, -3)
}

#[test]
fn round_to_test_3() {
    round_to_test_inner(12540248064.554036548528425, 0)
}

#[test]
fn round_to_test_4() {
    round_to_test_inner(12540248064.554036548528425, 400)
}

#[test]
fn round_to_test_5() {
    round_to_test_inner(f64::max_value(), 3)
}

#[test]
fn round_to_test_6() {
    round_to_test_inner(-12540248064.554036548528425, 3)
}

#[test]
fn round_to_test_7() {
    round_to_test_inner(f64::infinity(), 3)
}

#[test]
fn round_to_test_8() {
    round_to_test_inner(f64::nan(), 3)
}

#[test]
fn round_test() {
    round_test_inner(15402542408.525402540285402)
}
