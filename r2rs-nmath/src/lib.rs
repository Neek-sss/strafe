#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(unused_assignments)]
#![allow(unused_comparisons)]
//#![warn(missing_docs)]

#[macro_use]
extern crate log;

#[cfg(test)]
#[macro_use]
extern crate approx;

pub mod distribution;
pub mod func;
pub mod rng;
pub mod traits;

pub fn reset_statics() {
    distribution::beta::reset_rbeta_statics();
    distribution::binom::reset_rbinom_statics();
    distribution::hyper::reset_rhyper_statics();
    distribution::pois::reset_rpois_statics();
    distribution::signrank::reset_signrank_static();
    distribution::wilcox::reset_wilcox_statics();
}
