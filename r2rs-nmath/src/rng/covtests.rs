// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn mersenne_twister_rng() {
    mersenne_twister_rng_inner(1);
}

#[test]
fn lecuyer_cmrg_rng() {
    lecuyer_cmrg_rng_inner(1);
}

#[test]
fn marsagllia_multicarry_rng() {
    marsagllia_multicarry_rng_inner(1);
}

#[test]
fn super_duper_rng() {
    super_duper_rng_inner(1);
}

#[test]
fn wichmann_hill_rng() {
    wichmann_hill_rng_inner(1);
}
