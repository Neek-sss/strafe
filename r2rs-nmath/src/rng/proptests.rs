// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{proptest::prelude::*, proptest_ext::strafe_default_proptest_options};

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn mersenne_twister_rng(
        seed in prop::num::u16::ANY,
    ) {
        mersenne_twister_rng_inner(seed);
    }

    #[test]
    fn lecuyer_cmrg_rng(
        seed in prop::num::u16::ANY,
    ) {
        lecuyer_cmrg_rng_inner(seed);
    }

    #[test]
    fn marsagllia_multicarry_rng(
        seed in prop::num::u16::ANY,
    ) {
        marsagllia_multicarry_rng_inner(seed);
    }

    #[test]
    fn super_duper_rng(
        seed in prop::num::u16::ANY,
    ) {
        super_duper_rng_inner(seed);
    }

    #[test]
    fn wichmann_hill_rng(
        seed in prop::num::u16::ANY,
    ) {
        wichmann_hill_rng_inner(seed);
    }
}
