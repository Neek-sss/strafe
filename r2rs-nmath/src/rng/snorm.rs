// REFERENCE
//
// Ahrens, J.H. and Dieter, U.
// Extensions of Forsythe's method for random sampling from
// the normal distribution.
// Math. Comput. 27, 927-937.
//
// The definitions of the constants a[k], d[k], t[k] and
// h[k] are according to the abovementioned article

use strafe_type::FloatConstraint;

use crate::{distribution::norm::qnorm, traits::RNG};

pub fn norm_rand(rng: &mut impl RNG) -> f64 {
    /*----------- Constants and definitions for  Kinderman - Ramage --- */
    /*
     *  REFERENCE
     *
     *    Kinderman A. J. and Ramage J. G. (1976).
     *    Computer generation of normal random variables.
     *    JASA 71, 893-896.
     */
    let mut u1 = 0.0;

    /* 2^27 */
    /* unif_rand() alone is not of high enough precision */
    u1 = rng.unif_rand();
    u1 = (134217728.0 * u1) + rng.unif_rand();
    qnorm(u1 / 134217728.0, 0.0, 1.0, true).unwrap()
}
