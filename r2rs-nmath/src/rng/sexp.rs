// Mathlib : A C Library of Special Functions
// Copyright (C) 1998 Ross Ihaka
// Copyright (C) 2000-2002 the R Core Team
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, a copy is available at
// https://www.R-project.org/Licenses/
//
// REFERENCE
//
// Ahrens, J.H. and Dieter, U. (1972).
// Computer methods for sampling from the exponential and
// normal distributions.
// Comm. ACM, 15, 873-882.

use crate::traits::RNG;

/// Random variates from the standard exponential distribution.
pub fn exp_rand(rng: &mut impl RNG) -> f64 {
    /* q[k-1] = sum(log(2)^k / k!)  k=1,..,n, */
    /* The highest n (here 16) is determined by q[n-1] = 1.0 */
    /* within standard precision */
    static q: [f64; 16] = [
        std::f64::consts::LN_2,
        0.9333736875190459,
        0.9888777961838675,
        0.9984959252914960,
        0.9998292811061389,
        0.9999833164100727,
        0.9999985691438767,
        0.9999998906925558,
        0.9999999924734159,
        0.9999999995283275,
        0.9999999999728814,
        0.9999999999985598,
        0.9999999999999289,
        0.9999999999999968,
        0.9999999999999999,
        1.0000000000000000,
    ]; /* precaution if u = 0 is ever returned */
    let mut a = 0.0;
    let mut u = rng.unif_rand();
    while u <= 0.0 || u >= 1.0 {
        u = rng.unif_rand()
    }
    loop {
        u += u;
        if u > 1.0 {
            break;
        }
        a += q[0]
    }
    u -= 1.0;
    if u <= q[0] {
        return a + u;
    }
    let mut i = 0;
    let mut ustar = rng.unif_rand();
    let mut umin = ustar;
    loop {
        ustar = rng.unif_rand();
        if umin > ustar {
            umin = ustar
        }
        i += 1;
        if !(u > q[i]) {
            break;
        }
    }
    return a + umin * q[0];
}
