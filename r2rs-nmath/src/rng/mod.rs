mod lecuyer_cmrg;
mod marsaglia_multicarry;
mod mersenne_twister;
mod sexp;
mod snorm;
mod super_duper;
mod wichmann_hill;

pub use self::{
    lecuyer_cmrg::*, marsaglia_multicarry::*, mersenne_twister::*, super_duper::*, wichmann_hill::*,
};
use self::{sexp::exp_rand, snorm::norm_rand};
use crate::traits::RNG;

pub trait NMathRNG {
    fn norm_rand(&mut self) -> f64;
    fn exp_rand(&mut self) -> f64;
}

impl<R> NMathRNG for R
where
    R: RNG + Sized,
{
    fn norm_rand(&mut self) -> f64 {
        norm_rand(self)
    }

    fn exp_rand(&mut self) -> f64 {
        exp_rand(self)
    }
}

static i2_32m1: f64 = 2.328306437080797e-10;

pub fn fixup(x: f64) -> f64 {
    if x <= 0.0 {
        0.5 * i2_32m1
    } else if (1.0 - x) <= 0.0 {
        1.0 - 0.5 * i2_32m1
    } else {
        x
    }
}

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
