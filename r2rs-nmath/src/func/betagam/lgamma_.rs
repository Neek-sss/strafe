// Translation of nmath's lgamma
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Real64};

use crate::{
    func::betagam::{gamma, lgammacor},
    traits::TrigPI,
};

/// The function lgammafn_sign in addition assigns the sign of
/// the gamma function to the address in the second argument if
/// this is not None.
pub fn lgammafn_sign<R: Into<Real64>>(x: R, sgn: &mut Option<i32>) -> Real64 {
    let x = x.into().unwrap();

    let mut ans = 0.0;
    let mut y = 0.0;
    let mut sinpiy = 0.0;

    /* For IEEE double precision DBL_EPSILON = 2^-52 = 2.220446049250313e-16 :
      xmax  = DBL_MAX / log(DBL_MAX) = 2^f64::MAX_EXP / (f64::MAX_EXP * log(2)) = 2^1014 / log(2)
      dxrel = sqrt(DBL_EPSILON) = 2^-26 = 5^26 * 1e-26 (is *exact* below !)
    */
    const xmax: f64 = 2.5327372760800758e+305;
    const dxrel: f64 = 1.490116119384765625e-8;

    if !sgn.is_none() {
        *sgn = Some(1);
    }

    if !sgn.is_none() && x < 0.0 && (-x).floor() % 2.0 == 0.0 {
        *sgn = Some(-1)
    }

    if x <= 0.0 && x == x.trunc() {
        /* Negative integer argument */
        // No warning: this is the best answer; was  ML_ERROR(ME_RANGE, "lgamma");
        return f64::infinity().into();
        /* +Inf, since lgamma(x) = log|gamma(x)| */
    }

    y = x.abs();

    if y < 1e-306 {
        // denormalized range, R change
        return (-y.ln()).into();
    }
    if y <= 10.0 {
        return gamma(x).unwrap().unwrap().abs().ln().into();
    }
    /*
    ELSE  y = |x| > 10 ---------------------- */

    if y > xmax {
        // No warning: +Inf is the best answer
        return f64::infinity().into();
    }

    if x > 0.0 {
        /* i.e. y = x > 10 */
        return if x > 1e17 {
            x * (x.ln() - 1.0)
        } else if x > 4934720.0 {
            strafe_consts::LN_SQRT_2TPI + (x - 0.5) * x.ln() - x
        } else {
            strafe_consts::LN_SQRT_2TPI + (x - 0.5) * x.ln() - x + lgammacor(x).unwrap()
        }
        .into();
    }
    /* else: x < -10; y = -x */
    sinpiy = y.sin_pi().abs();

    if sinpiy == 0.0 {
        /* Negative integer argument ===
        Now UNNECESSARY: caught above */
        warn!(" ** should NEVER happen! *** [lgamma.c: Neg.int, y={}]", y);
        return f64::nan().into();
    }

    ans =
        strafe_consts::LN_SQRT_PID2 + (x - 0.5) * y.ln() - x - sinpiy.ln() - lgammacor(y).unwrap();

    if ((x - (x - 0.5).trunc()) * ans / x).abs() < dxrel {
        /* The answer is less than half precision because
         * the argument is too near a negative integer; e.g. for  lgamma(1e-7 - 11) */
        warn!("full precision may not have been achieved in lgamma")
    }

    return ans.into();
}

/// The function lgammafn computes log|gamma(x)|.
///
/// The accuracy of this routine compares (very) favourably
/// with those of the Sun Microsystems portable mathematical
/// library.
pub fn lgamma<R: Into<Real64>>(x: R) -> Real64 {
    return lgammafn_sign(x, &mut None);
}
