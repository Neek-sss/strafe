// Translation of nmath's polygamma
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::{Float, FloatConst};
use strafe_consts::LOG10_2;
use strafe_type::{FloatConstraint, Positive64, PositiveInteger64, Real64};

use crate::traits::DPQ;

const n_max: usize = 100;

/// Compute  d_n(x) = (d/dx)^n cot(x)  ; cot(x) := cos(x) / sin(x)
fn d_n_cot(x: f64, n: usize) -> f64 {
    if n == 0 {
        x.cos() / x.sin()
    } else if n == 1 {
        // -1/sin^2
        -1.0 / x.sin().pow_di(2)
    } else if n == 2 {
        // 2 cos / sin^3
        2.0 * x.cos() / x.sin().pow_di(3)
    } else if n == 3 {
        // (-4 cos^2 - 2) / sin^4 ; num.= -2(2 cos^2 +1) = -2(3 - 2 sin^2)
        // tt = -2*(2*R_pow_di(cos(x), 2) + 1.)/R_pow_di(sin(x), 4);
        let sin2 = x.sin().pow_di(2); // = sin^2
        -2.0 * (3.0 - 2.0 * sin2) / sin2.pow_di(2)
    } else if n == 4 {
        // 8 cos (cos^2  + 2) / sin^5
        let co = x.cos();
        8.0 * co * (co.pow_di(2) + 2.0) / x.sin().pow_di(5)
    } else if n == 5 {
        // (-16 cos^4 - 88 cos^2 - 16)/ sin^6
        let co2 = x.cos().pow_di(2); // cos^2
        -8.0 * (2.0 * co2.pow_di(2) + 11.0 * co2 + 2.0) / x.sin().pow_di(6)
    } else {
        f64::nan()
    }
}

#[derive(Clone, Debug, Default)]
struct DPsiState {
    i: usize,
    j: isize,
    k: usize,
    nn: usize,
    np: usize,
    nx: usize,
    _fn: usize,
    arg: f64,
    den: f64,
    eps: f64,
    fx: f64,
    rxsq: f64,
    s: f64,
    t: f64,
    ta: f64,
    tk: f64,
    tol: f64,
    tols: f64,
    tss: f64,
    tst: f64,
    tt: f64,
    t1: f64,
    t2: f64,
    xdmln: f64,
    xdmy: f64,
    xinc: f64,
    xm: f64,
    xmin: f64,
    xq: f64,
    trm: Vec<f64>,
    trmr: Vec<f64>,
    exit: bool,
}

/// Compute the derivatives of the psi function
/// and polygamma functions.
///
/// The following definitions are used in dpsifn:
///
/// ### Definition 1
///
/// $\psi(x) = \frac{d}{dx} (ln(\gamma(x))$, the first derivative of the log gamma function.
///
/// ### Definition 2
/// $\psi(k,x) = \frac{d}{dx} (\psi(x))$, the k-th derivative of $\psi(x)$.
///
///
/// "dpsifn" computes a sequence of scaled derivatives of
/// the psi function; i.e. for fixed x and m it computes
/// the m-member sequence
///
/// $\frac{(-1)^{k+1}}{\gamma(k+1)} * \psi(k,x)$ for k = n,...,n+m-1
///
/// where $\psi(k,x)$ is as defined above.   For kode=1, dpsifn
/// returns the scaled derivatives as described.  kode=2 is
/// operative only when k=0 and in that case dpsifn returns
/// $-\psi(x) + ln(x)$. That is, the logarithmic behavior for
/// large x is removed when kode=2 and k=0.0  When sums or
/// differences of psi functions are computed the logarithmic
/// terms can be combined analytically and computed separately
/// to help retain significant digits.
///
/// Note that dpsifn(x, 0, 1, 1, ans) results in ans = $-\psi(x)$.
///
/// ## Arguments
///
/// * x: x > 0.
/// * n: first member of the sequence, 0 <= n <= 100
///     * n == 0 gives
///         * ans(1) = -psi(x) for kode=1
///         * -psi(x)+ln(x) for kode=2
/// * kode: selection parameter
///     * kode == 1 returns scaled derivatives of the psi function.
///     * kode == 2 returns scaled derivatives of the psi function except when n=0.0 In this case,
///              ans(1) = -psi(x) + ln(x) is returned.
/// * m: number of members of the sequence, m >= 1
///
/// ## Returns
///
/// * ans: a vector of length at least m whose first m
/// components contain the sequence of derivatives
/// scaled according to kode.
/// * nz: underflow flag
///     * nz == 0, a normal return
///     * nz != 0, underflow, last nz components of ans are set to zero, ans(m-k+1)=0.0, k=1,...,nz
/// * ierr: error flag
///     * ierr=0, a normal return, computation completed
///     * ierr=1, input error,  no computation
///     * ierr=2, overflow,  x too small or n+m-1 too large or both
///     * ierr=3, error,   n too large. dimensioned array trmr(nmax) is not large enough for n
///
/// The nominal computational accuracy is the maximum of unit
/// roundoff (d1mach(4)) and 1e-18 since critical constants
/// are given to only 18 digits.
///
/// The basic method of evaluation is the asymptotic expansion
/// for large x >= xmin followed by backward recursion on a two
/// term recursion relation
///
/// $w(x+1) + x^{-n-1} = w(x)$
///
/// this is supplemented by a series
///
/// $\sum( (x+k)^{-n-1} , k=0,1,2,... )$
///
/// which converges rapidly for large n. both xmin and the
/// number of terms of the series are calculated from the unit
/// roundoff of the machine environment.
pub fn dpsi<R: Into<Real64>>(
    x: R,
    n: usize,
    kode: i32,
    m: usize,
    ans: &mut [Real64],
    nz: &mut i32,
    ierr: &mut i32,
) {
    let mut x = x.into().unwrap();

    let mut state = DPsiState::default();
    state.trm = vec![0.0; 23];
    state.trmr = vec![0.0; n_max + 1];

    let bvalues = [
        /* Bernoulli Numbers */
        1.00000000000000000e+00,
        -5.00000000000000000e-01,
        1.66666666666666667e-01,
        -3.33333333333333333e-02,
        2.38095238095238095e-02,
        -3.33333333333333333e-02,
        7.57575757575757576e-02,
        -2.53113553113553114e-01,
        1.16666666666666667e+00,
        -7.09215686274509804e+00,
        5.49711779448621554e+01,
        -5.29124242424242424e+02,
        6.19212318840579710e+03,
        -8.65802531135531136e+04,
        1.42551716666666667e+06,
        -2.72982310678160920e+07,
        6.01580873900642368e+08,
        -1.51163157670921569e+10,
        4.29614643061166667e+11,
        -1.37116552050883328e+13,
        4.88332318973593167e+14,
        -1.92965793419400681e+16,
    ];

    *ierr = 0;
    *nz = 0;

    if kode < 1 || kode > 2 || m < 1 {
        *ierr = 1;
        return;
    }
    if x <= 0.0 {
        /* use	Abramowitz & Stegun 6.4.7 "Reflection Formula", p.260
        *  psi(n, 1-x) + (-1)^(n+1) psi(n, x) = (-1)^n pi (d/dx)^n cot(pi*x)
        *  psi(n, x) = (-1)^n psi(n, 1-x)   -  pi^{n+1} d_n(pi*x),
                            where    d_n(x) := (d/dx)^n cot(x)
        */
        if x == x.round() {
            /* non-positive integer : +Inf or NaN depends on n */
            state.j = 0;
            while state.j < m as isize {
                /* k = j + n : */
                ans[state.j as usize] = if (state.j as usize + n) % 2 != 0 {
                    f64::infinity().into()
                } else {
                    f64::nan().into()
                };
                state.j += 1;
            }
            return;
        }
        /* This could cancel badly */
        dpsi(1.0 - x, n, /*kode = */ 1, m, ans, nz, ierr);
        /* ans[j] == (-1)^(k+1) / gamma(k+1) * psi(k, 1 - x)
         *	     for j = 0:(m-1) ,	k = n + j
         */

        /* For now: only work for  n in {0,1,..,5} : */
        if n > 5 {
            /* not yet implemented for x < 0 and n >= 6 */
            *ierr = 4;
            return;
        }
        // tt := d_n(pi * x)
        x *= f64::PI(); /* pi * x */

        // t := pi^(n+1) * d_n(x) / gamma(n+1)
        state.t1 = 1.0;
        state.t2 = 1.0;
        state.s = 1.0;
        state.k = 0;
        state.j = state.k as isize - n as isize;
        while state.j < m as isize {
            /* k == n+j , s = (-1)^k */
            state.t1 *= f64::PI(); /* t1 == pi^(k+1) */
            if state.k >= 2 {
                state.t2 *= state.k as f64; /* t2 == k! == gamma(k+1) */
            }
            if state.j >= 0 {
                /* now using d_k(x) */
                ans[state.j as usize] = (state.s
                    * (ans[state.j as usize].unwrap() + state.t1 / state.t2 * d_n_cot(x, state.k)))
                .into();
            }

            state.k += 1;
            state.j += 1;
            state.s = -state.s;
        }
        /* if (n == 0 && kode == 2)  -- nonsense for x < 0 !
         *     ans[0] += log(x); */
        return;
    } /* x <= 0 */

    /* else :  x > 0 */
    let xln = x.ln();
    if kode == 1 && m == 1 {
        /* the R case  ---  for very large x: */
        let lrg = 1.0 / (2.0 * f64::epsilon());
        if n == 0 && x * xln > lrg {
            ans[0] = (-xln).into();
            return;
        } else if n >= 1 && x > n as f64 * lrg {
            ans[0] = ((-(n as f64) * xln).exp() / n as f64).into(); /* == x^-n / n  ==  1/(n * x^n) */
            return;
        }
    }
    state.nx = (-f64::MIN_EXP).min(f64::MAX_EXP) as usize; /* = 1021 */

    let r1m5 = LOG10_2; // = M_LOG10_2 = log10(2) = 0.30103..
    let r1m4 = f64::EPSILON * 0.5; // = DBL_EPSILON * 0.5 = 2^-53 = 1.110223e-16
    let wdtol = r1m4.max(0.5e-18); /* = 2^-53 = 1.11e-16 */

    /* elim = approximate exponential over and underflow limit */
    let elim = 2.302 * (state.nx as f64 * r1m5 - 3.0); /* = 700.6174... */

    let rln = (r1m5 * f64::MANTISSA_DIGITS as f64).min(18.06); // = 0.30103 * 53 = 15.95.. ~= #{decimals}
    let mut fln = rln.max(3.0) - 3.0; // = 12.95..
    let yint = 3.50 + 0.40 * fln; // = 8.6818..
    let slope = 0.21 + fln * (0.0006038 * fln + 0.008677); // = 0.4237..
    let mut mm = m;
    loop {
        state.nn = n + mm - 1;
        state._fn = state.nn;
        state.t = (state._fn + 1) as f64 * xln;

        /* overflow and underflow test for small and large x */
        if state.t.abs() > elim {
            if state.t.abs() <= 0.0 {
                *ierr = 2;
                return;
            }
        } else {
            if x < wdtol {
                ans[0] = x.pow_di(-(n as isize) - 1).into();
                if mm != 1 {
                    state.k = 1;
                    while state.k < mm {
                        ans[state.k] = (ans[state.k - 1].unwrap() / x).into();
                        state.k += 1;
                    }
                }
                if n == 0 && kode == 2 {
                    ans[0] = (ans[0].unwrap() + xln).into();
                }
                return;
            }

            /* compute xmin and the number of terms of the series, fln+1 */
            state.xm = yint + slope * state._fn as f64;
            let mx = state.xm as usize + 1;
            state.xmin = mx as f64;
            if n != 0 {
                state.xm = -2.302 * rln - 0.0.min(xln);
                state.arg = 0.0.min(state.xm / n as f64);
                state.eps = state.arg.exp();
                state.xm = if state.arg.abs() < 1.0e-3 {
                    -state.arg
                } else {
                    1.0 - state.eps
                };
                fln = x * state.xm / state.eps;
                state.xm = state.xmin - x;
                if state.xm > 7.0 && fln < 15.0 {
                    break;
                }
            }
            state.xdmy = x;
            state.xdmln = xln;
            state.xinc = 0.0;
            if x < state.xmin {
                state.nx = x as usize;
                state.xinc = state.xmin - state.nx as f64;
                state.xdmy = x + state.xinc;
                state.xdmln = state.xdmy.ln();
            }

            /* generate w(n+mm-1, x) by the asymptotic expansion */

            state.t = state._fn as f64 * state.xdmln;
            state.t1 = state.xdmln + state.xdmln;
            state.t2 = state.t + state.xdmln;
            state.tk = state.t.abs().max(state.t1.abs().max(state.t2.abs()));
            if state.tk <= elim {
                /* for all but large x */
                l10(&mut state, x, kode, ans, ierr, &bvalues, wdtol, mm);
                if state.exit {
                    return;
                }
            }
        }
        *nz += 1; /* nz := #{underflows} */
        mm -= 1;
        ans[mm] = 0.0.into();
        if mm == 0 {
            return;
        }
    } /* end{for()} */
    state.nn = fln as usize + 1;
    state.np = n + 1;
    state.t1 = (n + 1) as f64 * xln;
    state.t = (-state.t1).exp();
    state.s = state.t;
    state.den = x;

    state.i = 1;
    while state.i <= state.nn {
        state.den += 1.0;
        state.trm[state.i] = state.den.powf(-(state.np as f64));
        state.s += state.trm[state.i];

        state.i += 1;
    }
    ans[0] = state.s.into();
    if n == 0 && kode == 2 {
        ans[0] = (state.s + xln).into();
    }

    if mm != 1 {
        /* generate higher derivatives, j > n */

        state.tol = wdtol / 5.0;

        state.j = 1;
        while state.j < mm as isize {
            state.t /= x;
            state.s = state.t;
            state.tols = state.t * state.tol;
            state.den = x;

            state.i = 1;
            while state.i <= state.nn {
                state.den += 1.0;
                state.trm[state.i] /= state.den;
                state.s += state.trm[state.i];
                if state.trm[state.i] < state.tols {
                    break;
                }

                state.i += 1;
            }
            ans[state.j as usize] = state.s.into();

            state.j += 1;
        }
    }
}

fn l10(
    state: &mut DPsiState,
    x: f64,
    kode: i32,
    ans: &mut [Real64],
    ierr: &mut i32,
    bvalues: &[f64],
    wdtol: f64,
    mm: usize,
) {
    state.tss = (-state.t).exp();
    state.tt = 0.5 / state.xdmy;
    state.t1 = state.tt;
    state.tst = wdtol * state.tt;
    if state.nn != 0 {
        state.t1 = state.tt + 1.0 / state._fn as f64;
    }
    state.rxsq = 1.0 / (state.xdmy * state.xdmy);
    state.ta = 0.5 * state.rxsq;
    state.t = (state._fn + 1) as f64 * state.ta;
    state.s = state.t * bvalues[2];
    if state.s.abs() >= state.tst {
        state.tk = 2.0;

        state.k = 4;
        while state.k <= 22 {
            state.t = state.t
                * ((state.tk + state._fn as f64 + 1.0) / (state.tk + 1.0))
                * ((state.tk + state._fn as f64) / (state.tk + 2.0))
                * state.rxsq;
            state.trm[state.k] = state.t * bvalues[state.k - 1];
            if (state.trm[state.k]).abs() < state.tst {
                break;
            }
            state.s += state.trm[state.k];
            state.tk += 2.0;

            state.k += 1;
        }
    }
    state.s = (state.s + state.t1) * state.tss;
    if state.xinc != 0.0 {
        /* backward recur from xdmy to x */

        state.nx = state.xinc as usize;
        state.np = state.nn + 1;
        if state.nx > n_max {
            *ierr = 3;
            state.exit = true;
            return;
        } else {
            if state.nn == 0 {
                l20(state, x, kode, ans);
                if state.exit {
                    return;
                }
            }
            state.xm = state.xinc - 1.0;
            state.fx = x + state.xm;

            /* this loop should not be changed. fx is accurate when x is small */
            state.i = 1;
            while state.i <= state.nx {
                state.trmr[state.i] = state.fx.powf(-(state.np as f64));
                state.s += state.trmr[state.i];
                state.xm -= 1.0;
                state.fx = x + state.xm;

                state.i += 1;
            }
        }
    }
    ans[mm - 1] = state.s.into();
    if state._fn == 0 {
        l30(state, x, kode, ans);
        if state.exit {
            return;
        }
    }

    /* generate lower derivatives,  j < n+mm-1 */

    state.j = 2;
    while state.j <= mm as isize {
        state._fn -= 1;
        state.tss *= state.xdmy;
        state.t1 = state.tt;
        if state._fn != 0 {
            state.t1 = state.tt + 1.0 / state._fn as f64;
        }
        state.t = (state._fn + 1) as f64 * state.ta;
        state.s = state.t * bvalues[2];
        if state.s.abs() >= state.tst {
            state.tk = (4 + state._fn) as f64;

            state.k = 4;
            while state.k <= 22 {
                state.trm[state.k] = state.trm[state.k] * (state._fn + 1) as f64 / state.tk;
                if state.trm[state.k].abs() < state.tst {
                    break;
                }
                state.s += state.trm[state.k];
                state.tk += 2.0;

                state.k += 1;
            }
        }
        state.s = (state.s + state.t1) * state.tss;
        if state.xinc != 0.0 {
            if state._fn == 0 {
                l20(state, x, kode, ans);
                if state.exit {
                    return;
                }
            }
            state.xm = state.xinc - 1.0;
            state.fx = x + state.xm;

            state.i = 1;
            while state.i <= state.nx {
                state.trmr[state.i] = state.trmr[state.i] * state.fx;
                state.s += state.trmr[state.i];
                state.xm -= 1.0;
                state.fx = x + state.xm;

                state.i += 1;
            }
        }
        ans[(mm as isize - state.j) as usize] = state.s.into();
        if state._fn == 0 {
            l30(state, x, kode, ans);
            if state.exit {
                return;
            }
        }
        state.j += 1;
    }
    state.exit = true;
}

fn l20(state: &mut DPsiState, x: f64, kode: i32, ans: &mut [Real64]) {
    state.i = 1;
    while state.i <= state.nx {
        state.s += 1.0 / (x + (state.nx as f64 - state.i as f64)); /* avoid disastrous cancellation, PR#13714 */
        state.i += 1
    }
    l30(state, x, kode, ans)
}

fn l30(state: &mut DPsiState, x: f64, kode: i32, ans: &mut [Real64]) {
    if kode != 2 {
        /* always */
        ans[0] = (state.s - state.xdmln).into();
    } else if state.xdmy != x {
        state.xq = state.xdmy / x;
        ans[0] = (state.s - state.xq.ln()).into();
    }
    state.exit = true;
}

pub fn psigamma<P: Into<Positive64>, PI: Into<PositiveInteger64>>(x: P, deriv: PI) -> Real64 {
    let x = x.into().unwrap();

    /* n-th derivative of psi(x);  e.g., psigamma(x,0) == digamma(x) */
    let mut ans = 0.0;
    let mut nz = 0;
    let mut ierr = 0;
    let mut k = 0;
    let mut n = 0;

    let deriv = deriv.into().unwrap();
    n = deriv as usize;
    if n > 100 {
        warn!("deriv = {} > {} (= n_max)", n, 100);
        return f64::nan().into();
    }
    let mut ans_temp = [ans.into()];
    dpsi(x, n, 1, 1, &mut ans_temp, &mut nz, &mut ierr);
    ans = ans_temp[0].unwrap();
    if ierr != 0 {
        return f64::nan().into();
    }
    /* Now, ans ==  A := (-1)^(n+1) / gamma(n+1) * psi(n, x) */
    ans = -ans; /* = (-1)^(0+1) * gamma(0+1) * A */
    k = 1; /* = (-1)^(k+1) * gamma(k+1) * A */
    while k <= n {
        ans *= -(k as f64);
        k += 1
    }
    ans.into()
    /* = psi(n, x) */
}

pub fn digamma<P: Into<Positive64>>(x: P) -> Real64 {
    let x = x.into().unwrap();

    let mut ans = 0.0;
    let mut nz = 0;
    let mut ierr = 0;
    let mut ans_temp = [ans.into()];
    dpsi(x, 0, 1, 1, &mut ans_temp, &mut nz, &mut ierr);
    ans = ans_temp[0].unwrap();
    if ierr != 0 {
        return f64::nan().into();
    }
    (-ans).into()
}

pub fn trigamma<R: Into<Real64>>(x: R) -> Positive64 {
    let x = x.into().unwrap();

    let mut ans = 0.0;
    let mut nz = 0;
    let mut ierr = 0;
    let mut ans_temp = [ans.into()];
    dpsi(x, 1, 1, 1, &mut ans_temp, &mut nz, &mut ierr);
    ans = ans_temp[0].unwrap();
    if ierr != 0 {
        return f64::nan().into();
    }
    ans.into()
}

pub fn tetragamma<P: Into<Positive64>>(x: P) -> Real64 {
    let x = x.into().unwrap();

    let mut ans = 0.0;
    let mut nz = 0;
    let mut ierr = 0;
    let mut ans_temp = [ans.into()];
    dpsi(x, 2, 1, 1, &mut ans_temp, &mut nz, &mut ierr);
    ans = ans_temp[0].unwrap();
    if ierr != 0 {
        return f64::nan().into();
    }
    (-2.0 * ans).into()
}

pub fn pentagamma<P: Into<Positive64>>(x: P) -> Positive64 {
    let x = x.into().unwrap();

    let mut ans = 0.0;
    let mut nz = 0;
    let mut ierr = 0;
    let mut ans_temp = [ans.into()];
    dpsi(x, 3, 1, 1, &mut ans_temp, &mut nz, &mut ierr);
    ans = ans_temp[0].unwrap();
    if ierr != 0 {
        return f64::nan().into();
    }
    (6.0 * ans).into()
}
