// Translation of nmath's lgammacor
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Real64};

use crate::distribution::func::chebyshev_eval;

/// Compute the log gamma correction factor for x >= 10 so that
///
/// log(gamma(x)) = .5\*log(2*pi) + (x-.5)*log(x) -x + lgammacor(x)
///
/// \[ lgammacor(x) is called Del(x) in other contexts (e.g. dcdflib)\]
pub fn lgammacor<R: Into<Real64>>(x: R) -> Real64 {
    let x = x.into().unwrap();

    static algmcs: [f64; 15] = [
        // below, nalgm = 5 ==> only the first 5 are used!
        0.1666389480451863247205729650822e+0,
        -0.1384948176067563840732986059135e-4,
        0.9810825646924729426157171547487e-8,
        -0.1809129475572494194263306266719e-10,
        0.6221098041892605227126015543416e-13,
        -0.3399615005417721944303330599666e-15,
        0.2683181998482698748957538846666e-17,
        -0.2868042435334643284144622399999e-19,
        0.3962837061046434803679306666666e-21,
        -0.6831888753985766870111999999999e-23,
        0.1429227355942498147573333333333e-24,
        -0.3547598158101070547199999999999e-26,
        0.1025680058010470912000000000000e-27,
        -0.3401102254316748799999999999999e-29,
        0.1276642195630062933333333333333e-30,
    ];
    let mut tmp = 0.0;

    /* For IEEE double precision DBL_EPSILON = 2^-52 = 2.220446049250313e-16 :
     *   xbig = 2 ^ 26.5
     *   xmax = DBL_MAX / 48 =  2^1020 / 3 */
    const nalgm: usize = 5;
    const xbig: f64 = 94906265.62425156;
    const xmax: f64 = 3.745194030963158e306;

    if x < 10.0 {
        return f64::nan().into();
    } else if x >= xmax {
        warn!("underflow occurred in lgammacor");
    /* allow to underflow below */
    } else if x < xbig {
        tmp = 10.0 / x;
        return (chebyshev_eval(tmp * tmp * 2.0 - 1.0, &algmcs, nalgm) / x).into();
    }
    (1.0 / (x * 12.0)).into()
}
