// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use num_traits::Float;

use super::tests::*;

#[test]
fn beta_test_1() {
    beta_test_inner(1.0, 1.0);
}

#[test]
fn beta_test_2() {
    beta_test_inner(0.0, 1.0);
}

#[test]
fn beta_test_3() {
    beta_test_inner(f64::infinity(), 1.0);
}

#[test]
fn beta_test_4() {
    beta_test_inner(10000.0, 1.0);
}

#[test]
fn lbeta_test_1() {
    lbeta_test_inner(1.0, 1.0);
}

#[test]
fn lbeta_test_2() {
    lbeta_test_inner(0.0, 1.0);
}

#[test]
fn lbeta_test_3() {
    lbeta_test_inner(1.0, f64::infinity());
}

#[test]
fn lbeta_test_4() {
    lbeta_test_inner(1.0, 25.0);
}

#[test]
fn lbeta_test_5() {
    lbeta_test_inner(1e-307, 1.0);
}

#[test]
fn lbeta_test_6() {
    lbeta_test_inner(25.0, 31.0);
}

#[test]
fn choose_test_1() {
    choose_test_inner(1.0, 1.0);
}

#[test]
fn choose_test_2() {
    choose_test_inner(-1.0, 31.0);
}

#[test]
fn choose_test_3() {
    choose_test_inner(1.0, 35.0);
}

#[test]
fn choose_test_4() {
    choose_test_inner(1.0, -3.1);
}

#[test]
fn choose_test_5() {
    choose_test_inner(1.5, 35.0);
}

#[test]
fn choose_test_6() {
    choose_test_inner(41.5, 35.0);
}

#[test]
fn lchoose_test_1() {
    lchoose_test_inner(1.0, 1.0);
}

#[test]
fn lchoose_test_2() {
    lchoose_test_inner(-1.0, 1.0);
}

#[test]
fn lchoose_test_3() {
    lchoose_test_inner(1.0, 3.0);
}

#[test]
fn lchoose_test_4() {
    lchoose_test_inner(5.0, 3.0);
}

#[test]
fn lchoose_test_5() {
    lchoose_test_inner(5.2, 3.0);
}

#[test]
fn lchoose_test_6() {
    lchoose_test_inner(5.2, 9.7);
}

#[test]
fn lchoose_test_7() {
    lchoose_test_inner(1.0, -1.0);
}

#[test]
fn lchoose_test_8() {
    lchoose_test_inner(1.0, 0.0);
}

#[test]
fn lchoose_test_9() {
    lchoose_test_inner(-1.0, 3.0);
}

#[test]
fn lchoose_test_10() {
    lchoose_test_inner(2.0, 3.0);
}

#[test]
fn gamma_test_1() {
    gamma_test_inner(1.0);
}

#[test]
fn gamma_test_2() {
    gamma_test_inner(-25.9);
}

#[test]
fn gamma_test_3() {
    gamma_test_inner(100.0);
}

#[test]
fn gamma_test_4() {
    gamma_test_inner(-100.2);
}

#[test]
fn gamma_test_5() {
    gamma_test_inner(32.0);
}

#[test]
fn lgamma_test_1() {
    lgamma_test_inner(1.0);
}

#[test]
fn lgamma_test_2() {
    lgamma_test_inner(1e25);
}

#[test]
fn lgamma_test_3() {
    lgamma_test_inner(4934721.0);
}

#[test]
fn lgamma_test_4() {
    lgamma_test_inner(-21.0);
}

#[test]
fn digamma_test_1() {
    digamma_test_inner(1.0);
}

#[test]
fn digamma_test_2() {
    digamma_test_inner(465e247);
}

#[test]
fn trigamma_test_1() {
    trigamma_test_inner(1.0);
}

#[test]
fn trigamma_test_2() {
    trigamma_test_inner(-1.0);
}

#[test]
fn trigamma_test_3() {
    trigamma_test_inner(-1.2);
}

#[test]
fn psigamma_test_1() {
    psigamma_test_inner(1.0, 0.0);
}

#[test]
fn psigamma_test_2() {
    psigamma_test_inner(1.0, 10.0);
}

#[test]
fn psigamma_test_3() {
    psigamma_test_inner(528.782, 28.0);
}

#[test]
fn tetragamma_test_1() {
    tetragamma_test_inner(1.0);
}

#[test]
fn tetragamma_test_2() {
    tetragamma_test_inner(86705e125);
}

#[test]
fn pentagamma_test_1() {
    pentagamma_test_inner(1.0);
}

#[test]
fn factorial_test() {
    factorial_test_inner(1.0);
}

#[test]
fn lfactorial_test() {
    lfactorial_test_inner(1.0);
}
