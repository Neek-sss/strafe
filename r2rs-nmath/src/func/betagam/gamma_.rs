// Translation of nmath's gamma
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Real64};

use crate::{
    distribution::func::{chebyshev_eval, stirlerr},
    func::betagam::lgammacor,
    traits::TrigPI,
};

/// This function computes the value of the gamma function.
/// The accuracy of this routine compares (very) favourably
/// with those of the Sun Microsystems portable mathematical
/// library.
///
/// MM specialized the case of  n!  for n < 50 - for even better precision
pub fn gamma<R: Into<Real64>>(x: R) -> Option<Real64> {
    let x = x.into().unwrap();

    static gamcs: [f64; 42] = [
        0.8571195590989331421920062399942e-2,
        0.4415381324841006757191315771652e-2,
        0.5685043681599363378632664588789e-1,
        -0.4219835396418560501012500186624e-2,
        0.1326808181212460220584006796352e-2,
        -0.1893024529798880432523947023886e-3,
        0.3606925327441245256578082217225e-4,
        -0.6056761904460864218485548290365e-5,
        0.1055829546302283344731823509093e-5,
        -0.1811967365542384048291855891166e-6,
        0.3117724964715322277790254593169e-7,
        -0.5354219639019687140874081024347e-8,
        0.9193275519859588946887786825940e-9,
        -0.1577941280288339761767423273953e-9,
        0.2707980622934954543266540433089e-10,
        -0.4646818653825730144081661058933e-11,
        0.7973350192007419656460767175359e-12,
        -0.1368078209830916025799499172309e-12,
        0.2347319486563800657233471771688e-13,
        -0.4027432614949066932766570534699e-14,
        0.6910051747372100912138336975257e-15,
        -0.1185584500221992907052387126192e-15,
        0.2034148542496373955201026051932e-16,
        -0.3490054341717405849274012949108e-17,
        0.5987993856485305567135051066026e-18,
        -0.1027378057872228074490069778431e-18,
        0.1762702816060529824942759660748e-19,
        -0.3024320653735306260958772112042e-20,
        0.5188914660218397839717833550506e-21,
        -0.8902770842456576692449251601066e-22,
        0.1527474068493342602274596891306e-22,
        -0.2620731256187362900257328332799e-23,
        0.4496464047830538670331046570666e-24,
        -0.7714712731336877911703901525333e-25,
        0.1323635453126044036486572714666e-25,
        -0.2270999412942928816702313813333e-26,
        0.3896418998003991449320816639999e-27,
        -0.6685198115125953327792127999999e-28,
        0.1146998663140024384347613866666e-28,
        -0.1967938586345134677295103999999e-29,
        0.3376448816585338090334890666666e-30,
        -0.5793070335782135784625493333333e-31,
    ];

    let mut i = 0;
    let mut n = 0;
    let mut y = 0.0;
    let mut sinpiy = 0.0;
    let mut value = 0.0;

    /* For IEEE double precision DBL_EPSILON = 2^-52 = 2.220446049250313e-16 :
     * (xmin, xmax) are non-trivial, see ./gammalims.c
     * xsml = exp(.01)*DBL_MIN
     * dxrel = sqrt(DBL_EPSILON) = 2 ^ -26
     */
    const ngam: usize = 22;
    const xmin: f64 = -170.5674972726612;
    const xmax: f64 = 171.61447887182298;
    const xsml: f64 = 2.2474362225598545e-308;
    const dxrel: f64 = 1.490116119384765696e-8;

    /* If the argument is exactly zero or a negative integer
     * then return NaN. */
    if x == 0.0 || x < 0.0 && x == x.round() {
        return None;
    }

    y = x.abs();

    return Some(
        if y <= 10.0 {
            /* Compute gamma(x) for -10 <= x <= 10
             * Reduce the interval and find gamma(1 + y) for 0 <= y < 1
             * first of all. */

            n = x as i32; /* n = floor(x)  ==> y in [ 0, 1 ) */
            if x < 0.0 {
                n -= 1
            } /* x = 1.dddd = 1+y */
            y = x - n as f64;
            n -= 1;
            value = chebyshev_eval(y * 2.0 - 1.0, &gamcs, ngam) + 0.9375;
            if n == 0 {
                return Some(value.into());
            }

            if n < 0 {
                /* compute gamma(x) for -10 <= x < 1 */

                /* exact 0 or "-n" checked already above */

                /* The answer is less than half precision */
                /* because x too near a negative integer. */
                if x < -0.5 && (x - (x - 0.5) / x).abs() < dxrel {
                    warn!("full precision may not have been achieved in gammafn");
                }

                /* The argument is so close to 0 that the result would overflow. */
                if y < xsml {
                    warn!("value out of range in gammafn");
                    if x > 0.0 {
                        return Some(f64::infinity().into());
                    } else {
                        return Some(f64::neg_infinity().into());
                    }
                }

                n = -n;

                i = 0;
                while i < n {
                    value /= x + i as f64;
                    i += 1
                }
                value
            } else {
                /* gamma(x) for 2 <= x <= 10 */

                i = 1;
                while i <= n {
                    value *= y + i as f64;
                    i += 1
                }
                value
            }
        } else {
            /* gamma(x) for  y = |x| > 10.0 */

            if x > xmax {
                /* Overflow */
                // No warning: +Inf is the best answer
                return Some(f64::infinity().into());
            }

            if x < xmin {
                /* Underflow */
                // No warning: 0 is the best answer
                return Some(0.0.into());
            }

            if y <= 50.0 && y == y.trunc() {
                /* compute (n - 1)! */
                value = 1.0;
                i = 2;
                while (i) < y as i32 {
                    value *= i as f64;
                    i += 1
                }
            } else {
                /* normal case */
                value = ((y - 0.5) * y.ln() - y
                    + strafe_consts::LN_SQRT_2TPI
                    + (if 2.0 * y == (2.0 * y).trunc() {
                        stirlerr(y)
                    } else {
                        lgammacor(y).unwrap()
                    }))
                .exp()
            }
            if x > 0.0 {
                return Some(value.into());
            }

            if ((x - (x - 0.5).trunc()) / x).abs() < dxrel {
                /* The answer is less than half precision because */
                /* the argument is too near a negative integer. */
                warn!("full precision may not have been achieved in gammafn");
            }

            sinpiy = y.sin_pi();
            if sinpiy == 0.0 {
                /* Negative integer arg - overflow */
                warn!("value out of range in gammafn");
                return Some(f64::infinity().into());
            }

            -std::f64::consts::PI / (y * sinpiy * value)
        }
        .into(),
    );
}
