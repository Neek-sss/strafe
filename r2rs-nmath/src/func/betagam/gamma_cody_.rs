// Translation of nmath's gamma_cody
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Real64};

use crate::traits::TrigPI;

/// This routine calculates the GAMMA function for a float argument X.
/// Computation is based on an algorithm outlined in reference \[1\].
/// The program uses rational functions that approximate the GAMMA
/// function to at least 20 significant decimal digits. Coefficients
/// for the approximation over the interval (1,2) are unpublished.
/// Those for the approximation for X >= 12 are from reference \[2\].
/// The accuracy achieved depends on the arithmetic system, the
/// compiler, the intrinsic functions, and proper selection of the
/// machine-dependent constants.
///
/// ## Errors:
///
/// The program returns the value XINF for singularities or
/// when overflow would occur.  The computation is believed
/// to be free of underflow and overflow.
pub fn gamma_cody<R: Into<Real64>>(x: R) -> Real64 {
    let x = x.into().unwrap();
    /* ----------------------------------------------------------------------
    Mathematical constants
    ----------------------------------------------------------------------*/
    static sqrtpi: f64 = 0.9189385332046727417803297; /* == ??? */
    /* *******************************************************************

    Explanation of machine-dependent constants

    beta - radix for the floating-point representation
    maxexp - the smallest positive power of beta that overflows
    XBIG - the largest argument for which GAMMA(X) is representable
     in the machine, i.e., the solution to the equation
     GAMMA(XBIG) = beta**maxexp
    XINF - the largest machine representable floating-point number;
     approximately beta**maxexp
    EPS - the smallest positive floating-point number such that  1.0+EPS > 1.0
    XMININ - the smallest positive floating-point number such that
     1/XMININ is machine representable

    Approximate values for some important machines are:

    beta       maxexp      XBIG

    CRAY-1  (S.P.)       2  8191     966.961
    Cyber 180/855
    under NOS (S.P.)       2  1070     177.803
    IEEE (IBM/XT,
    SUN, etc.) (S.P.)       2   128     35.040
    IEEE (IBM/XT,
    SUN, etc.) (D.P.)       2  f64::MAX_EXP     171.624
    IBM 3033 (D.P.)      16    63     57.574
    VAX D-Format (D.P.)       2   127     34.844
    VAX G-Format (D.P.)       2  1023     171.489

    XINF  EPS     XMININ

    CRAY-1  (S.P.)  5.45E+2465   7.11E-15   1.84E-2466
    Cyber 180/855
    under NOS (S.P.)  1.26E+322    3.55E-15   3.14E-294
    IEEE (IBM/XT,
    SUN, etc.) (S.P.)  3.40E+38     1.19E-7   1.18E-38
    IEEE (IBM/XT,
    SUN, etc.) (D.P.)  1.79D+308    2.22D-16   2.23D-308
    IBM 3033 (D.P.)  7.23D+75     2.22D-16   1.39D-76
    VAX D-Format (D.P.)  1.70D+38     1.39D-17   5.88D-39
    VAX G-Format (D.P.)  8.98D+307    1.11D-16   1.12D-308

    *******************************************************************

    ----------------------------------------------------------------------
    Machine dependent parameters
    ----------------------------------------------------------------------
    */
    static xbig: f64 = 171.624;
    /* ML_POSINF ==   const double xinf = 1.79e308;*/
    /* DBL_EPSILON = const double eps = 2.22e-16;*/
    /* DBL_MIN ==   const double xminin = 2.23e-308;*/
    /*----------------------------------------------------------------------
    Numerator and denominator coefficients for rational minimax
    approximation over (1,2).
    ----------------------------------------------------------------------*/
    static p: [f64; 8] = [
        -1.71618513886549492533811,
        24.7656508055759199108314,
        -379.804256470945635097577,
        629.331155312818442661052,
        866.966202790413211295064,
        -31451.2729688483675254357,
        -36144.4134186911729807069,
        66456.1438202405440627855,
    ];
    static q: [f64; 8] = [
        -30.8402300119738975254353,
        315.350626979604161529144,
        -1015.15636749021914166146,
        -3107.77167157231109440444,
        22538.1184209801510330112,
        4755.84627752788110767815,
        -134659.959864969306392456,
        -115132.259675553483497211,
    ];
    /*----------------------------------------------------------------------
    Coefficients for minimax approximation over (12, INF).
    ----------------------------------------------------------------------*/
    static c: [f64; 7] = [
        -0.001910444077728,
        8.4171387781295e-4,
        -5.952379913043012e-4,
        7.93650793500350248e-4,
        -0.002777777777777681622553,
        0.08333333333333333331554247,
        0.0057083835261,
    ];
    /* Local variables */
    let mut i = 0; /*logical*/
    let mut n = 0;
    let mut parity = 0;
    let mut fact = 0.0;
    let mut xden = 0.0;
    let mut xnum = 0.0;
    let mut y = 0.0;
    let mut z = 0.0;
    let mut yi = 0.0;
    let mut res = 0.0;
    let mut sum = 0.0;
    let mut ysq = 0.0;
    parity = 0;
    fact = 1.0;
    n = 0;
    y = x;
    if y <= 0.0 {
        /* -------------------------------------------------------------
        Argument is negative
        ------------------------------------------------------------- */
        y = -x;
        yi = y.trunc();
        res = y - yi;
        if res != 0.0 {
            if yi != (yi * 0.5).trunc() * 2.0 {
                parity = 1
            }
            fact = -std::f64::consts::PI / res.sin_pi();
            y += 1.0
        } else {
            return f64::infinity().into();
        }
    }
    /* -----------------------------------------------------------------
    Argument is positive
    -----------------------------------------------------------------*/
    if y < 2.2204460492503131e-16 {
        /* --------------------------------------------------------------
        Argument < EPS
        -------------------------------------------------------------- */
        if y >= 2.2250738585072014e-308 {
            res = 1.0 / y
        } else {
            return f64::infinity().into();
        }
    } else if y < 12.0 {
        yi = y;
        if y < 1.0 {
            /* ---------------------------------------------------------
            EPS < argument < 1
            --------------------------------------------------------- */
            z = y;
            y += 1.0
        } else {
            /* -----------------------------------------------------------
            1 <= argument < 12, reduce argument if necessary
            ----------------------------------------------------------- */
            n = y as usize - 1;
            y -= n as f64;
            z = y - 1.0
        }
        /* ---------------------------------------------------------
        Evaluate approximation for 1.0 < argument < 2.
        ---------------------------------------------------------*/
        xnum = 0.0;
        xden = 1.0;
        i = 0;
        while i < 8 {
            xnum = (xnum + p[i]) * z;
            xden = xden * z + q[i];
            i += 1
        }
        res = xnum / xden + 1.0;
        if yi < y {
            /* --------------------------------------------------------
            Adjust result for case  0.0 < argument < 1.
            -------------------------------------------------------- */
            res /= yi
        } else if yi > y {
            /* ----------------------------------------------------------
            Adjust result for case  2. < argument < 12.
            ---------------------------------------------------------- */
            i = 0;
            while i < n {
                res *= y;
                y += 1.0;
                i += 1
            }
        }
    } else if y <= xbig {
        ysq = y * y;
        sum = c[6];
        i = 0;
        while i < 6 {
            sum = sum / ysq + c[i];
            i += 1
        }
        sum = sum / y - y + sqrtpi;
        sum += (y - 0.5) * y.ln();
        res = sum.exp()
    } else {
        return f64::infinity().into();
    }
    /* -------------------------------------------------------------
    Evaluate for argument >= 12.,
    ------------------------------------------------------------- */
    /* ----------------------------------------------------------------------
    Final adjustments and return
    ----------------------------------------------------------------------*/
    if parity != 0 {
        res = -res
    }
    if fact != 1.0 {
        res = fact / res
    }
    return res.into();
}
