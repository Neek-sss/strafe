// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal_result,
};

use crate::func::betagam::{
    beta, choose, digamma, factorial, gamma, lbeta, lchoose, lfactorial, lgamma, pentagamma,
    psigamma, tetragamma, trigamma,
};

pub fn beta_test_inner(x1: f64, x2: f64) {
    let rust_ret = beta(x1, x2);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = beta({}, {});",
            RString::from_f64(x1),
            RString::from_f64(x2),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn lbeta_test_inner(x1: f64, x2: f64) {
    let rust_ret = lbeta(x1, x2);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = lbeta({}, {});",
            RString::from_f64(x1),
            RString::from_f64(x2),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn choose_test_inner(x1: f64, x2: f64) {
    let rust_ret = choose(x1, x2);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = choose({}, {});",
            RString::from_f64(x1),
            RString::from_f64(x2),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn lchoose_test_inner(x1: f64, x2: f64) {
    let rust_ret = lchoose(x1, x2);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = lchoose({}, {});",
            RString::from_f64(x1),
            RString::from_f64(x2),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn gamma_test_inner(x: f64) {
    let rust_ret = gamma(x).unwrap();
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = gamma({});",
            RString::from_f64(x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn lgamma_test_inner(x: f64) {
    let rust_ret = lgamma(x);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = lgamma({});",
            RString::from_f64(x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn digamma_test_inner(x: f64) {
    let rust_ret = digamma(x);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = digamma({});",
            RString::from_f64(x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn trigamma_test_inner(x: f64) {
    let rust_ret = trigamma(x);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = trigamma({});",
            RString::from_f64(x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn psigamma_test_inner(x: f64, deriv: f64) {
    let rust_ret = psigamma(x, deriv);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = psigamma({}, {});",
            RString::from_f64(x),
            RString::from_f64(deriv),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn tetragamma_test_inner(x: f64) {
    tetragamma(x);
}

pub fn pentagamma_test_inner(x: f64) {
    pentagamma(x);
}

pub fn factorial_test_inner(x: f64) {
    let rust_ret = factorial(x);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = factorial({});",
            RString::from_f64(x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn lfactorial_test_inner(x: f64) {
    let rust_ret = lfactorial(x);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = lfactorial({});",
            RString::from_f64(x),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}
