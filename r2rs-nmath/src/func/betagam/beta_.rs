// Translation of nmath's beta
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Positive64, Real64};

use crate::func::betagam::{gamma, lbeta};

/// This function returns the value of the beta function
/// evaluated with arguments a and b.
pub fn beta<P1: Into<Positive64>, P2: Into<Positive64>>(a: P1, b: P2) -> Real64 {
    let a = a.into().unwrap();
    let b = b.into().unwrap();

    /* For IEEE double precision DBL_EPSILON = 2^-52 = 2.220446049250313e-16 :
     *   xmin, xmax : see ./gammalims.c
     *   lnsml = log(DBL_MIN) = log(2 ^ -1022) = -1022 * log(2)
     */
    /* NaNs propagated correctly */
    if a == 0.0 || b == 0.0 {
        return f64::infinity().into();
    } else if !a.is_finite() || !b.is_finite() {
        return 0.0.into();
    }
    if a + b < 171.61447887182298 {
        /* ~= 171.61 for IEEE */
        // return gammafn(a) * gammafn(b) / gammafn(a+b);
        /* All the terms are positive, and all can be large for large
           or small arguments.  They are never much less than one.
           gammafn(x) can still overflow for x ~ 1e-308,
           but the result would too.
        */
        (1.0 / gamma(a + b).unwrap().unwrap()
            * gamma(a).unwrap().unwrap()
            * gamma(b).unwrap().unwrap())
        .into()
    } else {
        let val = lbeta(a, b).unwrap();
        // underflow to 0 is not harmful per se;  (-999).exp() also gives no warning
        val.exp().into()
    }
}
