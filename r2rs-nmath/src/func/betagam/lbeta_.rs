// Translation of nmath's lbeta
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Positive64, Real64};

use crate::func::betagam::{gamma, lgamma, lgammacor};

/// This function returns the value of the log beta function.
///
/// log B(a,b) = log G(a) + log G(b) - log G(a+b)
///
/// ## Notes:
///
/// This routine is a translation into C of a Fortran subroutine
/// by W. Fullerton of Los Alamos Scientific Laboratory.
pub fn lbeta<P1: Into<Positive64>, P2: Into<Positive64>>(a: P1, b: P2) -> Real64 {
    let a = a.into().unwrap();
    let b = b.into().unwrap();

    let mut corr = 0.0; /* := min(a,b) */
    let mut p = 0.0; /* := max(a,b) */
    let mut q = 0.0;

    q = a;
    p = q;
    if b < p {
        p = b
    }
    if b > q {
        q = b
    }

    /* both arguments must be >= 0 */
    if p == 0.0 {
        return f64::infinity().into();
    } else if !q.is_finite() {
        /* q == +Inf */
        return f64::neg_infinity().into();
    }

    if p >= 10.0 {
        /* p and q are big. */
        corr = lgammacor(p).unwrap() + lgammacor(q).unwrap() - lgammacor(p + q).unwrap();
        q.ln() * -0.5
            + strafe_consts::LN_SQRT_2TPI
            + corr
            + (p - 0.5) * (p / (p + q)).ln()
            + q * (-p / (p + q)).ln_1p()
    } else if q >= 10.0 {
        /* p is small, but q is big. */
        corr = lgammacor(q).unwrap() - lgammacor(p + q).unwrap();
        lgamma(p).unwrap() + corr + p - p * (p + q).ln() + (q - 0.5) * (-p / (p + q)).ln_1p()
    } else if p < 1e-306 {
        lgamma(p).unwrap() + (lgamma(q).unwrap() - lgamma(p + q).unwrap())
    } else {
        (gamma(p).unwrap().unwrap() * (gamma(q).unwrap().unwrap() / gamma(p + q).unwrap().unwrap()))
            .ln()
    }
    .into()
}
/* p and q are small: p <= q < 10.0 */
/* R change for very small args */
