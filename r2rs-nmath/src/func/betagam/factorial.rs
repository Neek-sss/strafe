use strafe_type::{FloatConstraint, Positive64, Real64};

use crate::func::betagam::{gamma, lgamma};

pub fn factorial<P: Into<Positive64>>(x: P) -> Positive64 {
    gamma(x.into().unwrap() + 1.0).unwrap().unwrap().into()
}

pub fn lfactorial<R: Into<Real64>>(x: R) -> Real64 {
    lgamma(x.into().unwrap() + 1.0)
}
