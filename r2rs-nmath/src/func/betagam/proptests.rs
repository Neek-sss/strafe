// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_pos64, stat_r64, stat_rat64, strafe_default_proptest_options},
};
use strafe_type::{r64, tof64};

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn beta_test(
        x1 in stat_pos64(None, None, None),
        x2 in stat_pos64(None, None, None),
    ) {
        beta_test_inner(x1, x2);
    }

    #[test]
    fn lbeta_test(
        x1 in stat_pos64(None, None, None),
        x2 in stat_pos64(None, None, None),
    ) {
        lbeta_test_inner(x1, x2);
    }

    #[test]
    fn choose_test(
        x1 in stat_r64(None, None, None),
        x2 in stat_r64(None, None, None),
    ) {
        choose_test_inner(x1, x2);
    }

    #[test]
    fn lchoose_test(
        x1 in stat_r64(None, None, None),
        x2 in stat_r64(None, None, None),
    ) {
        lchoose_test_inner(x1, x2);
    }

    #[test]
    fn gamma_test(
        x in stat_r64(None, None, None).prop_map(|x| {
            let x = tof64!(x);
            r64!(if x <= -1.0 {
                x.abs()
            } else {
                x
            })
        }),
    ) {
        gamma_test_inner(x);
    }

    #[test]
    fn lgamma_test(
        x in stat_r64(None, None, None),
    ) {
        lgamma_test_inner(x);
    }

    #[test]
    fn digamma_test(
        x in stat_pos64(None, None, None),
    ) {
        digamma_test_inner(x);
    }

    #[test]
    fn trigamma_test(
        x in stat_r64(None, None, None),
    ) {
        trigamma_test_inner(x);
    }

    #[test]
    fn psigamma_test(
        x in stat_pos64(None, None, None),
        d in stat_rat64(None, 100.0, None),
    ) {
        psigamma_test_inner(x, d);
    }

    #[test]
    fn tetragamma_test(
        x in stat_pos64(None, None, None),
    ) {
        tetragamma_test_inner(x);
    }

    #[test]
    fn pentagamma_test(
        x in stat_pos64(None, None, None),
    ) {
        pentagamma_test_inner(x);
    }

    #[test]
    fn factorial_test(
        x in stat_pos64(None, None, None),
    ) {
        factorial_test_inner(x);
    }

    #[test]
    fn lfactorial_test(
        x in stat_r64(None, None, None),
    ) {
        lfactorial_test_inner(x);
    }
}
