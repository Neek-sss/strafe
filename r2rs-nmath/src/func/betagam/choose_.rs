// Translation of nmath's choose
//
// Copyright (C) 2020 neek-sss
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use num_traits::Float;
use strafe_type::{FloatConstraint, Real64};

use crate::func::betagam::{lbeta, lgamma, lgammafn_sign};

pub fn lfastchoose<R1: Into<Real64>, R2: Into<Real64>>(n: R1, k: R2) -> Real64 {
    let n = n.into().unwrap();
    let k = k.into().unwrap();

    (-(n + 1.0).ln() - lbeta(n - k + 1.0, k + 1.0).unwrap()).into()
}

/// Mathematically the same as lfastchoose:
/// less stable typically, but useful if n-k+1 < 0
fn lfastchoose2<R1: Into<Real64>, R2: Into<Real64>>(
    n: R1,
    k: R2,
    s_choose: &mut Option<i32>,
) -> Real64 {
    let n = n.into().unwrap();
    let k = k.into().unwrap();

    let mut r = 0.0;
    r = lgammafn_sign(n - k + 1.0, s_choose).unwrap();
    (lgamma(n + 1.0).unwrap() - lgamma(k + 1.0).unwrap() - r).into()
}

/// log(abs(choose(n,k))
///
/// These work for the *generalized* binomial theorem,
/// i.e., are also defined for non-integer n  (integer k).
///
/// We use the simple explicit product formula for  k <= k_small_max
/// and also have added statements to make sure that the symmetry
/// (n * k ) == (n * n-k)  is preserved for non-negative integer n.
pub fn lchoose<R1: Into<Real64>, R2: Into<Real64>>(n: R1, k: R2) -> Real64 {
    let mut n = n.into().unwrap();
    let mut k = k.into().unwrap();

    let k0 = k;
    k = k.round();
    if (k - k0).abs() > 1e-7 {
        warn!("({}) must be integer, rounded to {}", k0, k);
    }
    if k < 2.0 {
        if k < 0.0 {
            return f64::neg_infinity().into();
        }
        if k == 0.0 {
            return 0.0.into();
        }
        /* else: k == 1 */
        return n.abs().ln().into();
    }
    /* else: k >= 2 */
    if n < 0.0 {
        return lchoose(-n + k - 1.0, k);
    } else if !((n - n.round()).abs() > 1e-7 * n.abs().max(1.0)) {
        n = n.round();
        if n < k {
            return f64::neg_infinity().into();
        }
        /* k <= n :*/
        if n - k < 2.0 {
            return lchoose(n, n - k);
        } /* <- Symmetry */
        /* else: n >= k+2 */
        return lfastchoose(n, k);
    }
    /* else non-integer n >= 0 : */
    if n < k - 1.0 {
        let mut s = Some(0);
        return lfastchoose2(n, k, &mut s);
    }
    lfastchoose(n, k)
}

/// Binomial coefficients.
///
/// These work for the *generalized* binomial theorem,
/// i.e., are also defined for non-integer n  (integer k).
///
/// We use the simple explicit product formula for  k <= k_small_max
/// and also have added statements to make sure that the symmetry
/// (n * k ) == (n * n-k)  is preserved for non-negative integer n.
pub fn choose<R1: Into<Real64>, R2: Into<Real64>>(n: R1, k: R2) -> Real64 {
    let mut n = n.into().unwrap();
    let mut k = k.into().unwrap();

    let mut r = 0.0;
    let k0 = k;
    k = k.round();
    // R_CheckStack();
    if (k - k0).abs() > 1e-7 {
        warn!("({}) must be integer, rounded to {}", k0, k);
    }
    if k < 30.0 {
        let mut j = 0;
        /* might have got rounding errors */
        if n - k < k && n >= 0.0 && !((n - n.round()).abs() > 1e-7 * n.abs().max(1.0)) {
            k = n - k
        } /* <- Symmetry */
        if k < 0.0 {
            return 0.0.into();
        }
        if k == 0.0 {
            return 1.0.into();
        }
        r = n;
        j = 2;
        while j <= k as i32 {
            r *= (n - j as f64 + 1.0) / j as f64;
            j += 1
        }
        return if !((n - n.round()).abs() > 1e-7 * n.abs().max(1.0)) {
            r.round()
        } else {
            r
        }
        .into();
    }
    /* else: k >= 1 */
    /* else: k >= k_small_max */
    if n < 0.0 {
        r = choose(-n + k - 1.0, k).unwrap(); /* <- Symmetry, ensure k still integer */
        if k != 2.0 * (k / 2.0).floor() {
            r = -r
        }
        return r.into();
    } else if !((n - n.round()).abs() > 1e-7 * n.abs().max(1.0)) {
        n = n.round();
        if n < k {
            return 0.0.into();
        }
        if n - k < 30.0 {
            return choose(n, n - k);
        }
        return lfastchoose(n, k).unwrap().exp().round().into();
    }
    /* else non-integer n >= 0 : */
    if n < k - 1.0 {
        let mut s_choose = Some(0);
        r = lfastchoose2(n, k, &mut s_choose).unwrap();
        return (s_choose.unwrap() as f64 * r.exp()).into();
    }
    return lfastchoose(n, k).unwrap().exp().into();
}
