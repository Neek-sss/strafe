// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use strafe_testing::{
    proptest::prelude::*,
    proptest_ext::{stat_alpha64, stat_pos64, strafe_default_proptest_options},
};

use super::tests::*;

proptest! {
    #![proptest_config(ProptestConfig {
        ..strafe_default_proptest_options()
    })]

    #[test]
    fn bessel_i_test(
        x in stat_pos64(None, Some(1e5), None),
        nu in stat_alpha64(Some(1e-1), None, None),
        expo in prop::bool::ANY,
    ) {
        bessel_i_test_inner(x, nu, expo);
    }

    #[test]
    fn bessel_k_test(
        x in stat_pos64(None, Some(1e5), None),
        nu in stat_alpha64(Some(1e-1), None, None),
        expo in prop::bool::ANY,
    ) {
        bessel_k_test_inner(x, nu, expo);
    }

    #[test]
    fn bessel_y_test(
        x in stat_pos64(None, Some(1e5), None),
        nu in stat_alpha64(None, None, None),
    ) {
        bessel_y_test_inner(x, nu);
    }

    #[test]
    fn bessel_j_test(
        x in stat_pos64(None, Some(1e5), None),
        nu in stat_alpha64(None, None, None),
    ) {
        bessel_j_test_inner(x, nu);
    }
}
