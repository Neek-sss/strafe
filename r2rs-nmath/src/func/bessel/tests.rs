// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::str::FromStr;

use strafe_testing::{
    r::{RString, RTester},
    r_assert_relative_equal_result,
};
use strafe_type::{Positive64, Real64};

use crate::func::bessel::{bessel_i, bessel_j, bessel_k, bessel_y};

pub fn bessel_i_test_inner(x: Positive64, nu: Real64, expo: bool) {
    let rust_ret = bessel_i(x, nu, expo);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = besselI({}, nu = {}, expon.scaled = {});",
            RString::from_f64(x),
            RString::from_f64(nu),
            RString::from_bool(expo),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn bessel_k_test_inner(x: Positive64, nu: Real64, expo: bool) {
    let rust_ret = bessel_k(x, nu, expo);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = besselK({}, nu = {}, expon.scaled = {});",
            RString::from_f64(x),
            RString::from_f64(nu),
            RString::from_bool(expo),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn bessel_y_test_inner(x: Positive64, nu: Real64) {
    let rust_ret = bessel_y(x, nu);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = besselY({}, nu = {});",
            RString::from_f64(x),
            RString::from_f64(nu),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}

pub fn bessel_j_test_inner(x: Positive64, nu: Real64) {
    let rust_ret = bessel_j(x, nu);
    let r_ret = RTester::new()
        .set_script(&RString::from_string(format!(
            "ret = besselJ({}, nu = {});",
            RString::from_f64(x),
            RString::from_f64(nu),
        )))
        .set_display(&RString::from_str("ret").unwrap())
        .run()
        .expect("R running error")
        .as_f64();
    r_assert_relative_equal_result!(r_ret, &rust_ret);
}
