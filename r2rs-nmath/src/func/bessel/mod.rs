//! # Bessel Functions
//!
//! ## Description
//!
//! Bessel Functions of integer and fractional order, of first and second kind, J(nu) and Y(nu),
//! and Modified Bessel functions (of first and third kind), I(nu) and K(nu).
//!
//! ## Arguments
//!
//! * x: numeric, ≥ 0.
//! * nu: numeric; The order (maybe fractional and negative) of the corresponding Bessel function.
//! * expon.scaled: logical; if TRUE, the results are exponentially scaled in order to avoid
//! overflow (I(nu)) or underflow (K(nu)), respectively.
//!
//! ## Details
//!
//! If expon.scaled = TRUE, exp(-x) I(x;nu), or exp(x) K(x;nu) are returned.
//!
//! For nu < 0, formulae 9.1.2 and 9.6.2 from Abramowitz & Stegun are applied (which is probably
//! suboptimal), except for besselK which is symmetric in nu.
//!
//! The current algorithms will give warnings about accuracy loss for large arguments. In some
//! cases, these warnings are exaggerated, and the precision is perfect. For large nu, say in the
//! order of millions, the current algorithms are rarely useful.
//!
//! ## Value
//!
//! Numeric vector with the (scaled, if expon.scaled = TRUE) values of the corresponding Bessel
//! function.
//!
//! The length of the result is the maximum of the lengths of the parameters. All parameters are
//! recycled to that length.
//!
//! ## Author(s)
//!
//! Original Fortran code: W. J. Cody, Argonne National Laboratory
//! Translation to C and adaptation to R: Martin Maechler maechler@stat.math.ethz.ch.
//!
//! ## Source
//!
//! The C code is a translation of Fortran routines from http://!www.netlib.org/specfun/ribesl,
//! ../rjbesl, etc. The four source code files for bessel\[IJKY\] each contain a paragraph
//! “Acknowledgement” and “References”, a short summary of which is
//!
//! besselI
//! based on (code) by David J. Sookne, see Sookne (1973)... Modifications... An earlier version
//! was published in Cody (1983).
//!
//! besselJ
//! as besselI
//!
//! besselK
//! based on (code) by J. B. Campbell (1980)... Modifications...
//!
//! besselY
//! draws heavily on Temme's Algol program for Y... and on Campbell's programs for Y_ν(x) ....
//! ... heavily modified.
//!
//! ## References
//!
//! Abramowitz, M. and Stegun, I. A. (1972). Handbook of Mathematical Functions. Dover, New York;
//! Chapter 9: Bessel Functions of Integer Order.
//!
//! In order of “Source” citation above:
//!
//! Sockne, David J. (1973). Bessel Functions of Real Argument and Integer Order. Journal of
//! Research of the National Bureau of Standards, 77B, 125–132.
//!
//! Cody, William J. (1983). Algorithm 597: Sequence of modified Bessel functions of the first
//! kind. ACM Transactions on Mathematical Software, 9(2), 242–245. doi: 10.1145/357456.357462.
//!
//! Campbell, J.B. (1980). On Temme's algorithm for the modified Bessel function of the third kind.
//! ACM Transactions on Mathematical Software, 6(4), 581–586. doi: 10.1145/355921.355928.
//!
//! Campbell, J.B. (1979). Bessel functions J_nu(x) and Y_nu(x) of float order and float argument.
//! Computer Physics Communications, 18, 133–142. doi: 10.1016/0010-4655(79)90030-4.
//!
//! Temme, Nico M. (1976). On the numerical evaluation of the ordinary Bessel function of the
//! second kind. Journal of Computational Physics, 21, 343–350. doi: 10.1016/0021-9991(76)90032-2.
//!
//! ## See Also
//!
//! Other special mathematical functions, such as gamma, $\Gamma$(x), and beta, $\Beta$(x).
//!
//! ## Examples
//!
//! Bessel I Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_i;
//! # use strafe_plot::prelude::*;
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let nus = [0, 1, 2, 3, 4, 5, 10, 20];
//! let x = <[f64]>::sequence(0.0, 4.0, 501);
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     y_min: Some(0.0),
//!     y_max: Some(6.0),
//!     x_min: Some(0.0),
//!     x_max: Some(4.0),
//!     legend_x: 0.1,
//!     legend_y: 0.1,
//!     title: "Bessel Main Functions I_nu(x)".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! });
//!
//! for i in 0..nus.len() {
//!     let y = x
//!         .iter()
//!         .map(|x| bessel_i(x, nus[i], false).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: x.clone(),
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / nus.len() as f64),
//!         legend: true,
//!         label: format!("nu={}", nus[i]),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_i.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_i.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_i.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_i", "src/func/bessel/doctest_out/bessel_i.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel I Plot][bessel_i]")
)]
//!
//! Bessel J Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_j;
//! # use strafe_plot::prelude::{full_palette::GREY_600, *};
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let nus = [0, 1, 2, 3, 4, 5, 10, 20];
//! let x = <[f64]>::sequence(0.0, 40.0, 801);
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     y_min: Some(-0.5),
//!     y_max: Some(1.0),
//!     x_min: Some(0.0),
//!     x_max: Some(40.0),
//!     legend_x: 0.9,
//!     legend_y: 0.1,
//!     title: "Bessel Functions J_nu(x)".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! })
//! .with_plottable(HorizontalLine {
//!     y: 0.0,
//!     dash: true,
//!     color: GREY_600,
//!     ..Default::default()
//! })
//! .with_plottable(VerticalLine {
//!     x: 0.0,
//!     dash: true,
//!     color: GREY_600,
//!     ..Default::default()
//! });
//!
//! for i in 0..nus.len() {
//!     let y = x
//!         .iter()
//!         .map(|x| bessel_j(x, nus[i]).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: x.clone(),
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / nus.len() as f64),
//!         legend: true,
//!         label: format!("nu={}", nus[i]),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_j.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_j.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_j.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_j", "src/func/bessel/doctest_out/bessel_j.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel J Plot][bessel_j]")
)]
//!
//! Bessel I Negative Nu's Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_i;
//! # use strafe_plot::prelude::{full_palette::GREY_600, *};
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let xx = (2..=7).collect::<Vec<_>>();
//! let nu = <[f64]>::sequence(-10.0, 9.0, 2001);
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     y_min: Some(-50.0),
//!     y_max: Some(200.0),
//!     x_min: Some(-10.0),
//!     x_max: Some(10.0),
//!     legend_x: 0.9,
//!     legend_y: 0.1,
//!     title: "Bessel I_nu(x) for fixed x, as f(nu)".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! })
//! .with_plottable(VerticalLine {
//!     x: 0.0,
//!     dash: true,
//!     color: GREY_600,
//!     ..Default::default()
//! });
//!
//! for i in 0..xx.len() {
//!     let y = nu
//!         .iter()
//!         .map(|nu| bessel_i(xx[i], nu, false).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: nu.clone(),
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / xx.len() as f64),
//!         legend: true,
//!         label: format!("x={}", xx[i]),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_i_negative_nus.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_i_negative_nus.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_i_negative_nus.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_i_negative_nus", "src/func/bessel/doctest_out/bessel_i_negative_nus.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel I Negative Nus Plot][bessel_i_negative_nus]")
)]
//!
//! Bessel J Negative Nu's Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_j;
//! # use strafe_plot::prelude::{full_palette::GREY_600, *};
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let xx = (2..=7).collect::<Vec<_>>();
//! let nu = <[f64]>::sequence(-10.0, 9.0, 2001);
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     y_min: Some(-50.0),
//!     y_max: Some(200.0),
//!     x_min: Some(-10.0),
//!     x_max: Some(10.0),
//!     legend_x: 0.9,
//!     legend_y: 0.1,
//!     title: "Bessel J_nu(x) for fixed x, as f(nu)".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! })
//! .with_plottable(VerticalLine {
//!     x: 0.0,
//!     dash: true,
//!     color: GREY_600,
//!     ..Default::default()
//! });
//!
//! for i in 0..xx.len() {
//!     let y = nu
//!         .iter()
//!         .map(|nu| bessel_j(xx[i], nu).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: nu.clone(),
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / xx.len() as f64),
//!         legend: true,
//!         label: format!("x={}", xx[i]),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_j_negative_nus.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_j_negative_nus.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_j_negative_nus.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_j_negative_nus", "src/func/bessel/doctest_out/bessel_j_negative_nus.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel J Negative Nus Plot][bessel_j_negative_nus]")
)]
//!
//! Bessel J Near Zero Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_j;
//! # use strafe_plot::prelude::{full_palette::GREY_600, *};
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let nus = [
//!     0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 10.0, 10.5, 20.0, 20.5,
//! ];
//! let x0 = <[f64]>::sequence(-16.0, 5.0, 256)
//!     .into_iter()
//!     .map(|x| 2.0_f64.powf(x))
//!     .collect::<Vec<_>>();
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     y_min: Some(1e-40),
//!     y_max: Some(1.0),
//!     x_log: true,
//!     y_log: true,
//!     legend_x: 0.95,
//!     legend_y: 0.4,
//!     title: "Bessel Functions J_nu(x) Near 0 log-log Scale".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! });
//!
//! for i in 0..nus.len() {
//!     let y = x0
//!         .iter()
//!         .map(|x| bessel_j(x, nus[i]).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: x0.clone(),
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / nus.len() as f64),
//!         legend: true,
//!         label: format!("nu={}", nus[i]),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_j_near_zero.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_j_near_zero.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_j_near_zero.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_j_near_zero", "src/func/bessel/doctest_out/bessel_j_near_zero.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel J Near Zero Plot][bessel_j_near_zero]")
)]
//!
//! Bessel K Near Zero Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_k;
//! # use strafe_plot::prelude::{full_palette::GREY_600, *};
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let nus = [
//!     0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 10.0, 10.5, 20.0, 20.5,
//! ];
//! let x0 = <[f64]>::sequence(-10.0, 8.0, 256)
//!     .into_iter()
//!     .map(|x| 2.0_f64.powf(x))
//!     .collect::<Vec<_>>();
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     x_log: true,
//!     y_log: true,
//!     legend_x: 0.95,
//!     legend_y: 0.05,
//!     title: "Bessel Functions K_nu(x) Near 0 log-log Scale".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! });
//!
//! for i in 0..nus.len() {
//!     let y = x0
//!         .iter()
//!         .map(|x| bessel_k(x, nus[i], false).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: x0.clone(),
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / nus.len() as f64),
//!         legend: true,
//!         label: format!("nu={}", nus[i]),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_k_near_zero.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_k_near_zero.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_k_near_zero.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_k_near_zero", "src/func/bessel/doctest_out/bessel_k_near_zero.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel K Near Zero Plot][bessel_k_near_zero]")
)]
//!
//! Bessel K Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_k;
//! # use strafe_plot::prelude::{full_palette::GREY_600, *};
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let x = <[f64]>::sequence(1e-4, 40.0, 801);
//! let nus = [0, 1, 2, 3, 4, 5, 10, 20];
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     y_max: Some(1e11),
//!     y_log: true,
//!     legend_x: 0.95,
//!     legend_y: 0.05,
//!     title: "Bessel Functions K_nu(x)".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! });
//!
//! for i in 0..nus.len() {
//!     let y = x
//!         .iter()
//!         .map(|x| bessel_k(x, nus[i], false).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: x.clone(),
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / nus.len() as f64),
//!         legend: true,
//!         label: format!("nu={}", nus[i]),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_k.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_k.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_k.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_k", "src/func/bessel/doctest_out/bessel_k.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel K Plot][bessel_k]")
)]
//!
//! Bessel Y Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_y;
//! # use strafe_plot::prelude::{full_palette::GREY_600, *};
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let x = <[f64]>::sequence(1e-4, 40.0, 801);
//! let nus = [0, 1, 2, 3, 4, 5, 10, 20];
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     y_min: Some(-1.6),
//!     y_max: Some(0.6),
//!     legend_x: 0.95,
//!     legend_y: 0.5,
//!     title: "Bessel Functions Y_nu(x)".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! });
//!
//! for i in 0..nus.len() {
//!     let xx = x
//!         .iter()
//!         .filter(|&x| *x > x * 0.6)
//!         .cloned()
//!         .collect::<Vec<_>>();
//!     let y = xx
//!         .iter()
//!         .map(|x| bessel_y(x, nus[i]).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: xx,
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / nus.len() as f64),
//!         legend: true,
//!         label: format!("nu={}", nus[i]),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_y.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_y.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_y.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_y", "src/func/bessel/doctest_out/bessel_y.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel Y Plot][bessel_y]")
)]
//!
//! Bessel Y Negative Nu Plot
//! ```rust
//! # use r2rs_base::traits::StatisticalSlice;
//! # use r2rs_nmath::func::bessel_y;
//! # use strafe_plot::prelude::{full_palette::GREY_600, *};
//! # use strafe_type::FloatConstraint;
//! # use std::fs::rename;
//! let x = <[f64]>::sequence(1e-4, 40.0, 801);
//! let nus = <[f64]>::sequence(-0.1, -2.0, 19);
//!
//! let mut plot = Plot::new();
//! plot.with_options(PlotOptions {
//!     y_min: Some(-1.6),
//!     y_max: Some(0.6),
//!     x_max: Some(10.0),
//!     title: "Bessel Functions Y_nu(x) for -nu".to_string(),
//!     x_axis_label: "x".to_string(),
//!     y_axis_label: " ".to_string(),
//!     ..Default::default()
//! });
//!
//! for i in 0..nus.len() {
//!     let xx = x
//!         .iter()
//!         .filter(|&x| *x > x * 0.6)
//!         .cloned()
//!         .collect::<Vec<_>>();
//!     let y = xx
//!         .iter()
//!         .map(|x| bessel_y(x, nus[i]).unwrap())
//!         .collect::<Vec<_>>();
//!     plot.with_plottable(Line {
//!         x: xx,
//!         y,
//!         color: ViridisRGB::get_color(i as f64 / nus.len() as f64),
//!         ..Default::default()
//!     });
//! }
//!
//! let root = SVGBackend::new("bessel_y_negative_nu.svg", (1024, 768)).into_drawing_area();
//! plot.plot(&root).unwrap();
//! # drop(root);
//! # rename(
//! #     format!("bessel_y_negative_nu.svg"),
//! #     format!("src/func/bessel/doctest_out/bessel_y_negative_nu.svg"),
//! # )
//! # .unwrap();
//! ```
#![cfg_attr(feature = "doc_outputs", cfg_attr(all(), doc = embed_doc_image::embed_image!("bessel_y_negative_nu", "src/func/bessel/doctest_out/bessel_y_negative_nu.svg")))]
#![cfg_attr(
    feature = "doc_outputs",
    cfg_attr(all(), doc = "![Bessel Y Negative Nu Plot][bessel_y_negative_nu]")
)]

mod bessel_i;
mod bessel_j;
mod bessel_k;
mod bessel_y;

pub use self::{bessel_i::*, bessel_j::*, bessel_k::*, bessel_y::*};

#[cfg(test)]
mod tests;

#[cfg(all(test, feature = "enable_proptest"))]
mod proptests;

#[cfg(all(test, feature = "enable_covtest"))]
mod covtests;
