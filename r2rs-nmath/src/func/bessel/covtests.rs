// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use super::tests::*;

#[test]
fn bessel_i_test_1() {
    bessel_i_test_inner(1.0.into(), 0.5.into(), false);
}

#[test]
fn bessel_i_test_2() {
    bessel_i_test_inner(1000.0.into(), 0.5.into(), false);
}

#[test]
fn bessel_i_test_3() {
    bessel_i_test_inner(0.0.into(), 0.9.into(), false);
}

#[test]
fn bessel_i_test_4() {
    bessel_i_test_inner(1000.0.into(), 0.5.into(), true);
}

#[test]
fn bessel_i_test_5() {
    bessel_i_test_inner(1.0.into(), (-0.5).into(), false);
}

#[test]
fn bessel_i_test_6() {
    bessel_i_test_inner(1.0.into(), (-0.6).into(), false);
}

#[test]
fn bessel_i_test_7() {
    bessel_i_test_inner(1.0.into(), (-1.0).into(), false);
}

// TODO:
// #[test]
// fn bessel_i_test_8() {
//     bessel_i_test_inner(pos64!(1.0), r64!(-1.2), true);
// }

// #[test]
// fn bessel_i_test_9() {
//     bessel_i_test_inner(pos64!(1.0), r64!(-1.2), false);
// }

#[test]
fn bessel_k_test_1() {
    bessel_k_test_inner(1.0.into(), 0.5.into(), false);
}

#[test]
fn bessel_k_test_2() {
    bessel_k_test_inner(1000.0.into(), 0.5.into(), false);
}

#[test]
fn bessel_k_test_3() {
    bessel_k_test_inner(0.0.into(), 0.9.into(), false);
}

#[test]
fn bessel_k_test_4() {
    bessel_k_test_inner(1000.0.into(), 0.5.into(), true);
}

#[test]
fn bessel_k_test_5() {
    bessel_k_test_inner(1000.0.into(), (-0.5).into(), true);
}

#[test]
fn bessel_k_test_6() {
    bessel_k_test_inner(1000.0.into(), (-0.6).into(), true);
}

// #[test]
// fn bessel_k_test_7() {
//     bessel_k_test_inner(pos64!(1000.0), r64!(-1.0), true);
// }

#[test]
fn bessel_k_test_8() {
    bessel_k_test_inner(1000.0.into(), (-1.2).into(), false);
}

// #[test]
// fn bessel_k_test_9() {
//     bessel_k_test_inner(pos64!(1000.0), r64!(-1.2), true);
// }

#[test]
fn bessel_y_test_1() {
    bessel_y_test_inner(1.0.into(), 0.5.into());
}

#[test]
fn bessel_y_test_2() {
    bessel_y_test_inner(1000.0.into(), 0.5.into());
}

#[test]
fn bessel_y_test_3() {
    bessel_y_test_inner(0.0.into(), 0.9.into());
}

#[test]
fn bessel_y_test_4() {
    bessel_y_test_inner(1.0.into(), (-0.5).into());
}

#[test]
fn bessel_y_test_5() {
    bessel_y_test_inner(1.0.into(), (-0.6).into());
}

#[test]
fn bessel_y_test_6() {
    bessel_y_test_inner(1.0.into(), (-1.0).into());
}

#[test]
fn bessel_j_test_1() {
    bessel_j_test_inner(1.0.into(), 0.5.into());
}

#[test]
fn bessel_j_test_2() {
    bessel_j_test_inner(1000.0.into(), 0.5.into());
}

#[test]
fn bessel_j_test_3() {
    bessel_j_test_inner(0.0.into(), 0.9.into());
}

#[test]
fn bessel_j_test_4() {
    bessel_j_test_inner(5.0.into(), (-0.5).into());
}

#[test]
fn bessel_j_test_5() {
    bessel_j_test_inner(5.0.into(), (-0.6).into());
}

#[test]
fn bessel_j_test_6() {
    bessel_j_test_inner(5.0.into(), (-1.0).into());
}
