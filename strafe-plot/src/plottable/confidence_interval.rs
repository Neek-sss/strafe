// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::error::Error;

use plotters::prelude::{full_palette::GREY_900, DrawingBackend, RGBColor};

use crate::{
    drawing_coords::DrawingCoords,
    linear_model::draw_cis,
    plot_options::PlotOptions,
    plottable::{Plottable, PlottableValues},
};

#[derive(Clone, Debug)]
pub struct ConfidenceInterval {
    pub x: Vec<f64>,
    pub lower_y: Vec<f64>,
    pub upper_y: Vec<f64>,
    pub color: RGBColor,
    pub force_fit_all: bool,
}

impl Default for ConfidenceInterval {
    fn default() -> Self {
        Self {
            x: Vec::new(),
            lower_y: Vec::new(),
            upper_y: Vec::new(),
            color: GREY_900,
            force_fit_all: true,
        }
    }
}

impl<B: DrawingBackend> Plottable<B> for ConfidenceInterval
where
    B::ErrorType: 'static,
{
    fn plot(
        &self,
        plot_options: &PlotOptions,
        drawing_coords: &DrawingCoords,
    ) -> Result<PlottableValues, Box<dyn Error>> {
        let mut xs = self.x.clone();
        let mut lower_ys = self.lower_y.clone();
        let mut upper_ys = self.upper_y.clone();

        if plot_options.x_log {
            for x in xs.iter_mut() {
                *x = x.abs().log10();
            }
        }

        if plot_options.y_log {
            for y in lower_ys.iter_mut() {
                *y = y.abs().log10();
            }
        }

        if plot_options.y_log {
            for y in upper_ys.iter_mut() {
                *y = y.abs().log10();
            }
        }

        let coords = draw_cis(&xs, &lower_ys, &upper_ys, &drawing_coords)?;
        Ok(PlottableValues {
            polygons: vec![(coords, GREY_900, None)],
            ..Default::default()
        })
    }

    fn force_fit(&self) -> bool {
        false
    }

    fn get_x(&self) -> Vec<f64> {
        self.x.clone()
    }

    fn get_y(&self) -> Vec<f64> {
        self.lower_y
            .iter()
            .chain(self.upper_y.iter())
            .cloned()
            .collect()
    }

    fn get_legend(&self) -> bool {
        false
    }
}
