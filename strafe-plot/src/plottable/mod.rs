// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub mod confidence_interval;
pub mod horizontal_line;
pub mod line;
pub mod points;
pub mod vertical_line;

use std::error::Error;

use plotters::{backend::DrawingBackend, style::RGBColor};

use crate::{drawing_coords::DrawingCoords, prelude::PlotOptions};

#[derive(Clone, Debug)]
pub struct PlottableValues {
    pub lines: Vec<(Vec<(f64, f64)>, u32, RGBColor, Option<String>)>,
    pub points: Vec<(Vec<(f64, f64)>, u32, RGBColor, Option<String>)>,
    pub polygons: Vec<(Vec<(f64, f64)>, RGBColor, Option<String>)>,
}

impl Default for PlottableValues {
    fn default() -> Self {
        Self {
            lines: Vec::new(),
            points: Vec::new(),
            polygons: Vec::new(),
        }
    }
}

pub trait Plottable<B: DrawingBackend> {
    fn plot(
        &self,
        plot_options: &PlotOptions,
        drawing_coords: &DrawingCoords,
    ) -> Result<PlottableValues, Box<dyn Error>>;
    fn force_fit(&self) -> bool;
    fn get_x(&self) -> Vec<f64>;
    fn get_y(&self) -> Vec<f64>;
    fn get_legend(&self) -> bool;
}
