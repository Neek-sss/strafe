// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

#[derive(Clone, Debug)]
pub struct PlotOptions {
    // Draw Area Size
    pub plot_left: f64,
    pub plot_right: f64,
    pub plot_top: f64,
    pub plot_bottom: f64,

    // Graph Border Size
    pub graph_label_left: f64,
    pub graph_space_right: f64,
    pub graph_title_space_top: f64,
    pub graph_space_top: f64,
    pub graph_label_bottom: f64,

    // Plot Scalars
    pub x_plot_scalar: f64,
    pub y_plot_scalar: f64,
    pub title_scalar: f64,
    pub axis_scalar: f64,
    pub x_label_scalar: f64,
    pub y_label_scalar: f64,

    // Names
    pub title: String,
    pub x_axis_label: String,
    pub y_axis_label: String,
    pub font: String,

    // Legend
    pub legend_x: f64,
    pub legend_y: f64,
    pub legend_scalar: f64,

    // Plot Coords
    pub x_min: Option<f64>,
    pub x_max: Option<f64>,
    pub x_log: bool,
    pub y_min: Option<f64>,
    pub y_max: Option<f64>,
    pub y_log: bool,
}

impl Default for PlotOptions {
    fn default() -> Self {
        Self {
            plot_left: 0.0,
            plot_right: 1.0,
            plot_top: 0.0,
            plot_bottom: 1.0,
            graph_label_left: 0.07,
            graph_space_right: 0.01,
            graph_title_space_top: 0.05,
            graph_space_top: 0.01,
            graph_label_bottom: 0.06,
            x_plot_scalar: 0.05,
            y_plot_scalar: 0.05,
            title_scalar: 0.05,
            axis_scalar: 0.025,
            x_label_scalar: 0.015,
            y_label_scalar: 0.015,
            title: String::new(),
            x_axis_label: String::new(),
            y_axis_label: String::new(),
            font: "sans-serif".to_string(),
            legend_x: 1.0,
            legend_y: 0.5,
            legend_scalar: 0.02,
            x_min: None,
            x_max: None,
            x_log: false,
            y_min: None,
            y_max: None,
            y_log: false,
        }
    }
}
