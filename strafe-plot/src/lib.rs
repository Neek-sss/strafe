// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub use plotters;
pub mod chart;
pub mod drawing_area;
pub mod drawing_coords;
pub mod linear_model;
pub mod model_plot;
pub mod plot;
pub mod plot_options;
pub mod plottable;
pub mod prelude;
pub mod stem_leaf;
