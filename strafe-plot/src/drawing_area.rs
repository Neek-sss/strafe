// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use plotters::{
    coord::Shift,
    drawing::DrawingArea,
    element::Rectangle,
    prelude::{full_palette::GREY_200, Color, DrawingBackend, WHITE},
};

use crate::drawing_coords::DrawingCoords;

pub fn setup_drawing_area<B: DrawingBackend>(
    root: &DrawingArea<B, Shift>,
    drawing_coords: &DrawingCoords,
) {
    root.fill(&WHITE).unwrap();
    root.draw(&Rectangle::new(
        [
            (
                drawing_coords.chart_left as i32,
                drawing_coords.chart_top as i32,
            ),
            (
                drawing_coords.chart_right as i32,
                drawing_coords.chart_bottom as i32,
            ),
        ],
        GREY_200.filled(),
    ))
    .unwrap();
}
